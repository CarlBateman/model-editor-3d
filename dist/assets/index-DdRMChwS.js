(function(){const e=document.createElement("link").relList;if(e&&e.supports&&e.supports("modulepreload"))return;for(const i of document.querySelectorAll('link[rel="modulepreload"]'))n(i);new MutationObserver(i=>{for(const r of i)if(r.type==="childList")for(const o of r.addedNodes)o.tagName==="LINK"&&o.rel==="modulepreload"&&n(o)}).observe(document,{childList:!0,subtree:!0});function t(i){const r={};return i.integrity&&(r.integrity=i.integrity),i.referrerPolicy&&(r.referrerPolicy=i.referrerPolicy),i.crossOrigin==="use-credentials"?r.credentials="include":i.crossOrigin==="anonymous"?r.credentials="omit":r.credentials="same-origin",r}function n(i){if(i.ep)return;i.ep=!0;const r=t(i);fetch(i.href,r)}})();var xg=typeof globalThis<"u"?globalThis:typeof window<"u"?window:typeof global<"u"?global:typeof self<"u"?self:{};function Mg(s){return s&&s.__esModule&&Object.prototype.hasOwnProperty.call(s,"default")?s.default:s}var Ba={exports:{}};Ba.exports;(function(s,e){(function(t,n){s.exports=n()})(xg,function(){function t(h,c){var f=Object.keys(h);if(Object.getOwnPropertySymbols){var x=Object.getOwnPropertySymbols(h);c&&(x=x.filter(function(b){return Object.getOwnPropertyDescriptor(h,b).enumerable})),f.push.apply(f,x)}return f}function n(h){for(var c=1;c<arguments.length;c++){var f=arguments[c]!=null?arguments[c]:{};c%2?t(Object(f),!0).forEach(function(x){l(h,x,f[x])}):Object.getOwnPropertyDescriptors?Object.defineProperties(h,Object.getOwnPropertyDescriptors(f)):t(Object(f)).forEach(function(x){Object.defineProperty(h,x,Object.getOwnPropertyDescriptor(f,x))})}return h}function i(h){return i=typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?function(c){return typeof c}:function(c){return c&&typeof Symbol=="function"&&c.constructor===Symbol&&c!==Symbol.prototype?"symbol":typeof c},i(h)}function r(h,c){if(!(h instanceof c))throw new TypeError("Cannot call a class as a function")}function o(h,c){for(var f=0;f<c.length;f++){var x=c[f];x.enumerable=x.enumerable||!1,x.configurable=!0,"value"in x&&(x.writable=!0),Object.defineProperty(h,M(x.key),x)}}function a(h,c,f){return c&&o(h.prototype,c),Object.defineProperty(h,"prototype",{writable:!1}),h}function l(h,c,f){return(c=M(c))in h?Object.defineProperty(h,c,{value:f,enumerable:!0,configurable:!0,writable:!0}):h[c]=f,h}function u(h,c){if(typeof c!="function"&&c!==null)throw new TypeError("Super expression must either be null or a function");h.prototype=Object.create(c&&c.prototype,{constructor:{value:h,writable:!0,configurable:!0}}),Object.defineProperty(h,"prototype",{writable:!1}),c&&m(h,c)}function d(h){return d=Object.setPrototypeOf?Object.getPrototypeOf.bind():function(c){return c.__proto__||Object.getPrototypeOf(c)},d(h)}function m(h,c){return m=Object.setPrototypeOf?Object.setPrototypeOf.bind():function(f,x){return f.__proto__=x,f},m(h,c)}function p(h){if(h===void 0)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return h}function g(h){var c=function(){if(typeof Reflect>"u"||!Reflect.construct||Reflect.construct.sham)return!1;if(typeof Proxy=="function")return!0;try{return Boolean.prototype.valueOf.call(Reflect.construct(Boolean,[],function(){})),!0}catch{return!1}}();return function(){var f,x=d(h);if(c){var b=d(this).constructor;f=Reflect.construct(x,arguments,b)}else f=x.apply(this,arguments);return function(E,R){if(R&&(typeof R=="object"||typeof R=="function"))return R;if(R!==void 0)throw new TypeError("Derived constructors may only return object or undefined");return p(E)}(this,f)}}function _(){return _=typeof Reflect<"u"&&Reflect.get?Reflect.get.bind():function(h,c,f){var x=function(E,R){for(;!Object.prototype.hasOwnProperty.call(E,R)&&(E=d(E))!==null;);return E}(h,c);if(x){var b=Object.getOwnPropertyDescriptor(x,c);return b.get?b.get.call(arguments.length<3?h:f):b.value}},_.apply(this,arguments)}function M(h){var c=function(f,x){if(typeof f!="object"||f===null)return f;var b=f[Symbol.toPrimitive];if(b!==void 0){var E=b.call(f,x);if(typeof E!="object")return E;throw new TypeError("@@toPrimitive must return a primitive value.")}return String(f)}(h,"string");return typeof c=="symbol"?c:c+""}var y=function(h){return!(!h||!h.Window)&&h instanceof h.Window},v=void 0,w=void 0;function S(h){v=h;var c=h.document.createTextNode("");c.ownerDocument!==h.document&&typeof h.wrap=="function"&&h.wrap(c)===c&&(h=h.wrap(h)),w=h}function T(h){return y(h)?h:(h.ownerDocument||h).defaultView||w.window}typeof window<"u"&&window&&S(window);var z=function(h){return!!h&&i(h)==="object"},N=function(h){return typeof h=="function"},A={window:function(h){return h===w||y(h)},docFrag:function(h){return z(h)&&h.nodeType===11},object:z,func:N,number:function(h){return typeof h=="number"},bool:function(h){return typeof h=="boolean"},string:function(h){return typeof h=="string"},element:function(h){if(!h||i(h)!=="object")return!1;var c=T(h)||w;return/object|function/.test(typeof Element>"u"?"undefined":i(Element))?h instanceof Element||h instanceof c.Element:h.nodeType===1&&typeof h.nodeName=="string"},plainObject:function(h){return z(h)&&!!h.constructor&&/function Object\b/.test(h.constructor.toString())},array:function(h){return z(h)&&h.length!==void 0&&N(h.splice)}};function k(h){var c=h.interaction;if(c.prepared.name==="drag"){var f=c.prepared.axis;f==="x"?(c.coords.cur.page.y=c.coords.start.page.y,c.coords.cur.client.y=c.coords.start.client.y,c.coords.velocity.client.y=0,c.coords.velocity.page.y=0):f==="y"&&(c.coords.cur.page.x=c.coords.start.page.x,c.coords.cur.client.x=c.coords.start.client.x,c.coords.velocity.client.x=0,c.coords.velocity.page.x=0)}}function D(h){var c=h.iEvent,f=h.interaction;if(f.prepared.name==="drag"){var x=f.prepared.axis;if(x==="x"||x==="y"){var b=x==="x"?"y":"x";c.page[b]=f.coords.start.page[b],c.client[b]=f.coords.start.client[b],c.delta[b]=0}}}var C={id:"actions/drag",install:function(h){var c=h.actions,f=h.Interactable,x=h.defaults;f.prototype.draggable=C.draggable,c.map.drag=C,c.methodDict.drag="draggable",x.actions.drag=C.defaults},listeners:{"interactions:before-action-move":k,"interactions:action-resume":k,"interactions:action-move":D,"auto-start:check":function(h){var c=h.interaction,f=h.interactable,x=h.buttons,b=f.options.drag;if(b&&b.enabled&&(!c.pointerIsDown||!/mouse|pointer/.test(c.pointerType)||x&f.options.drag.mouseButtons))return h.action={name:"drag",axis:b.lockAxis==="start"?b.startAxis:b.lockAxis},!1}},draggable:function(h){return A.object(h)?(this.options.drag.enabled=h.enabled!==!1,this.setPerAction("drag",h),this.setOnEvents("drag",h),/^(xy|x|y|start)$/.test(h.lockAxis)&&(this.options.drag.lockAxis=h.lockAxis),/^(xy|x|y)$/.test(h.startAxis)&&(this.options.drag.startAxis=h.startAxis),this):A.bool(h)?(this.options.drag.enabled=h,this):this.options.drag},beforeMove:k,move:D,defaults:{startAxis:"xy",lockAxis:"xy"},getCursor:function(){return"move"},filterEventType:function(h){return h.search("drag")===0}},X=C,Y={init:function(h){var c=h;Y.document=c.document,Y.DocumentFragment=c.DocumentFragment||H,Y.SVGElement=c.SVGElement||H,Y.SVGSVGElement=c.SVGSVGElement||H,Y.SVGElementInstance=c.SVGElementInstance||H,Y.Element=c.Element||H,Y.HTMLElement=c.HTMLElement||Y.Element,Y.Event=c.Event,Y.Touch=c.Touch||H,Y.PointerEvent=c.PointerEvent||c.MSPointerEvent},document:null,DocumentFragment:null,SVGElement:null,SVGSVGElement:null,SVGElementInstance:null,Element:null,HTMLElement:null,Event:null,Touch:null,PointerEvent:null};function H(){}var K=Y,J={init:function(h){var c=K.Element,f=h.navigator||{};J.supportsTouch="ontouchstart"in h||A.func(h.DocumentTouch)&&K.document instanceof h.DocumentTouch,J.supportsPointerEvent=f.pointerEnabled!==!1&&!!K.PointerEvent,J.isIOS=/iP(hone|od|ad)/.test(f.platform),J.isIOS7=/iP(hone|od|ad)/.test(f.platform)&&/OS 7[^\d]/.test(f.appVersion),J.isIe9=/MSIE 9/.test(f.userAgent),J.isOperaMobile=f.appName==="Opera"&&J.supportsTouch&&/Presto/.test(f.userAgent),J.prefixedMatchesSelector="matches"in c.prototype?"matches":"webkitMatchesSelector"in c.prototype?"webkitMatchesSelector":"mozMatchesSelector"in c.prototype?"mozMatchesSelector":"oMatchesSelector"in c.prototype?"oMatchesSelector":"msMatchesSelector",J.pEventTypes=J.supportsPointerEvent?K.PointerEvent===h.MSPointerEvent?{up:"MSPointerUp",down:"MSPointerDown",over:"mouseover",out:"mouseout",move:"MSPointerMove",cancel:"MSPointerCancel"}:{up:"pointerup",down:"pointerdown",over:"pointerover",out:"pointerout",move:"pointermove",cancel:"pointercancel"}:null,J.wheelEvent=K.document&&"onmousewheel"in K.document?"mousewheel":"wheel"},supportsTouch:null,supportsPointerEvent:null,isIOS7:null,isIOS:null,isIe9:null,isOperaMobile:null,prefixedMatchesSelector:null,pEventTypes:null,wheelEvent:null},ce=J;function de(h,c){if(h.contains)return h.contains(c);for(;c;){if(c===h)return!0;c=c.parentNode}return!1}function Q(h,c){for(;A.element(h);){if(ve(h,c))return h;h=pe(h)}return null}function pe(h){var c=h.parentNode;if(A.docFrag(c)){for(;(c=c.host)&&A.docFrag(c););return c}return c}function ve(h,c){return w!==v&&(c=c.replace(/\/deep\//g," ")),h[ce.prefixedMatchesSelector](c)}var Ue=function(h){return h.parentNode||h.host};function Je(h,c){for(var f,x=[],b=h;(f=Ue(b))&&b!==c&&f!==b.ownerDocument;)x.unshift(b),b=f;return x}function vt(h,c,f){for(;A.element(h);){if(ve(h,c))return!0;if((h=pe(h))===f)return ve(h,c)}return!1}function ae(h){return h.correspondingUseElement||h}function we(h){var c=h instanceof K.SVGElement?h.getBoundingClientRect():h.getClientRects()[0];return c&&{left:c.left,right:c.right,top:c.top,bottom:c.bottom,width:c.width||c.right-c.left,height:c.height||c.bottom-c.top}}function ke(h){var c,f=we(h);if(!ce.isIOS7&&f){var x={x:(c=(c=T(h))||w).scrollX||c.document.documentElement.scrollLeft,y:c.scrollY||c.document.documentElement.scrollTop};f.left+=x.x,f.right+=x.x,f.top+=x.y,f.bottom+=x.y}return f}function Ee(h){for(var c=[];h;)c.push(h),h=pe(h);return c}function nt(h){return!!A.string(h)&&(K.document.querySelector(h),!0)}function be(h,c){for(var f in c)h[f]=c[f];return h}function Z(h,c,f){return h==="parent"?pe(f):h==="self"?c.getRect(f):Q(f,h)}function st(h,c,f,x){var b=h;return A.string(b)?b=Z(b,c,f):A.func(b)&&(b=b.apply(void 0,x)),A.element(b)&&(b=ke(b)),b}function le(h){return h&&{x:"x"in h?h.x:h.left,y:"y"in h?h.y:h.top}}function xe(h){return!h||"x"in h&&"y"in h||((h=be({},h)).x=h.left||0,h.y=h.top||0,h.width=h.width||(h.right||0)-h.x,h.height=h.height||(h.bottom||0)-h.y),h}function he(h,c,f){h.left&&(c.left+=f.x),h.right&&(c.right+=f.x),h.top&&(c.top+=f.y),h.bottom&&(c.bottom+=f.y),c.width=c.right-c.left,c.height=c.bottom-c.top}function Ae(h,c,f){var x=f&&h.options[f];return le(st(x&&x.origin||h.options.origin,h,c,[h&&c]))||{x:0,y:0}}function ge(h,c){var f=arguments.length>2&&arguments[2]!==void 0?arguments[2]:function(U){return!0},x=arguments.length>3?arguments[3]:void 0;if(x=x||{},A.string(h)&&h.search(" ")!==-1&&(h=Fe(h)),A.array(h))return h.forEach(function(U){return ge(U,c,f,x)}),x;if(A.object(h)&&(c=h,h=""),A.func(c)&&f(h))x[h]=x[h]||[],x[h].push(c);else if(A.array(c))for(var b=0,E=c;b<E.length;b++){var R=E[b];ge(h,R,f,x)}else if(A.object(c))for(var L in c)ge(Fe(L).map(function(U){return"".concat(h).concat(U)}),c[L],f,x);return x}function Fe(h){return h.trim().split(/ +/)}var Ge=function(h,c){return Math.sqrt(h*h+c*c)},B=["webkit","moz"];function I(h,c){h.__set||(h.__set={});var f=function(b){if(B.some(function(E){return b.indexOf(E)===0}))return 1;typeof h[b]!="function"&&b!=="__set"&&Object.defineProperty(h,b,{get:function(){return b in h.__set?h.__set[b]:h.__set[b]=c[b]},set:function(E){h.__set[b]=E},configurable:!0})};for(var x in c)f(x);return h}function $(h,c){h.page=h.page||{},h.page.x=c.page.x,h.page.y=c.page.y,h.client=h.client||{},h.client.x=c.client.x,h.client.y=c.client.y,h.timeStamp=c.timeStamp}function ue(h){h.page.x=0,h.page.y=0,h.client.x=0,h.client.y=0}function _e(h){return h instanceof K.Event||h instanceof K.Touch}function me(h,c,f){return h=h||"page",(f=f||{}).x=c[h+"X"],f.y=c[h+"Y"],f}function Ye(h,c){return c=c||{x:0,y:0},ce.isOperaMobile&&_e(h)?(me("screen",h,c),c.x+=window.scrollX,c.y+=window.scrollY):me("page",h,c),c}function Re(h){return A.number(h.pointerId)?h.pointerId:h.identifier}function Le(h,c,f){var x=c.length>1?Ce(c):c[0];Ye(x,h.page),function(b,E){E=E||{},ce.isOperaMobile&&_e(b)?me("screen",b,E):me("client",b,E)}(x,h.client),h.timeStamp=f}function tt(h){var c=[];return A.array(h)?(c[0]=h[0],c[1]=h[1]):h.type==="touchend"?h.touches.length===1?(c[0]=h.touches[0],c[1]=h.changedTouches[0]):h.touches.length===0&&(c[0]=h.changedTouches[0],c[1]=h.changedTouches[1]):(c[0]=h.touches[0],c[1]=h.touches[1]),c}function Ce(h){for(var c={pageX:0,pageY:0,clientX:0,clientY:0,screenX:0,screenY:0},f=0;f<h.length;f++){var x=h[f];for(var b in c)c[b]+=x[b]}for(var E in c)c[E]/=h.length;return c}function Xe(h){if(!h.length)return null;var c=tt(h),f=Math.min(c[0].pageX,c[1].pageX),x=Math.min(c[0].pageY,c[1].pageY),b=Math.max(c[0].pageX,c[1].pageX),E=Math.max(c[0].pageY,c[1].pageY);return{x:f,y:x,left:f,top:x,right:b,bottom:E,width:b-f,height:E-x}}function lt(h,c){var f=c+"X",x=c+"Y",b=tt(h),E=b[0][f]-b[1][f],R=b[0][x]-b[1][x];return Ge(E,R)}function je(h,c){var f=c+"X",x=c+"Y",b=tt(h),E=b[1][f]-b[0][f],R=b[1][x]-b[0][x];return 180*Math.atan2(R,E)/Math.PI}function ze(h){return A.string(h.pointerType)?h.pointerType:A.number(h.pointerType)?[void 0,void 0,"touch","pen","mouse"][h.pointerType]:/touch/.test(h.type||"")||h instanceof K.Touch?"touch":"mouse"}function Qe(h){var c=A.func(h.composedPath)?h.composedPath():h.path;return[ae(c?c[0]:h.target),ae(h.currentTarget)]}var ft=function(){function h(c){r(this,h),this.immediatePropagationStopped=!1,this.propagationStopped=!1,this._interaction=c}return a(h,[{key:"preventDefault",value:function(){}},{key:"stopPropagation",value:function(){this.propagationStopped=!0}},{key:"stopImmediatePropagation",value:function(){this.immediatePropagationStopped=this.propagationStopped=!0}}]),h}();Object.defineProperty(ft.prototype,"interaction",{get:function(){return this._interaction._proxy},set:function(){}});var V=function(h,c){for(var f=0;f<c.length;f++){var x=c[f];h.push(x)}return h},fe=function(h){return V([],h)},G=function(h,c){for(var f=0;f<h.length;f++)if(c(h[f],f,h))return f;return-1},re=function(h,c){return h[G(h,c)]},ne=function(h){u(f,h);var c=g(f);function f(x,b,E){var R;r(this,f),(R=c.call(this,b._interaction)).dropzone=void 0,R.dragEvent=void 0,R.relatedTarget=void 0,R.draggable=void 0,R.propagationStopped=!1,R.immediatePropagationStopped=!1;var L=E==="dragleave"?x.prev:x.cur,U=L.element,W=L.dropzone;return R.type=E,R.target=U,R.currentTarget=U,R.dropzone=W,R.dragEvent=b,R.relatedTarget=b.target,R.draggable=b.interactable,R.timeStamp=b.timeStamp,R}return a(f,[{key:"reject",value:function(){var x=this,b=this._interaction.dropState;if(this.type==="dropactivate"||this.dropzone&&b.cur.dropzone===this.dropzone&&b.cur.element===this.target)if(b.prev.dropzone=this.dropzone,b.prev.element=this.target,b.rejected=!0,b.events.enter=null,this.stopImmediatePropagation(),this.type==="dropactivate"){var E=b.activeDrops,R=G(E,function(U){var W=U.dropzone,F=U.element;return W===x.dropzone&&F===x.target});b.activeDrops.splice(R,1);var L=new f(b,this.dragEvent,"dropdeactivate");L.dropzone=this.dropzone,L.target=this.target,this.dropzone.fire(L)}else this.dropzone.fire(new f(b,this.dragEvent,"dragleave"))}},{key:"preventDefault",value:function(){}},{key:"stopPropagation",value:function(){this.propagationStopped=!0}},{key:"stopImmediatePropagation",value:function(){this.immediatePropagationStopped=this.propagationStopped=!0}}]),f}(ft);function Te(h,c){for(var f=0,x=h.slice();f<x.length;f++){var b=x[f],E=b.dropzone,R=b.element;c.dropzone=E,c.target=R,E.fire(c),c.propagationStopped=c.immediatePropagationStopped=!1}}function Pe(h,c){for(var f=function(E,R){for(var L=[],U=0,W=E.interactables.list;U<W.length;U++){var F=W[U];if(F.options.drop.enabled){var q=F.options.drop.accept;if(!(A.element(q)&&q!==R||A.string(q)&&!ve(R,q)||A.func(q)&&!q({dropzone:F,draggableElement:R})))for(var oe=0,Me=F.getAllElements();oe<Me.length;oe++){var ye=Me[oe];ye!==R&&L.push({dropzone:F,element:ye,rect:F.getRect(ye)})}}}return L}(h,c),x=0;x<f.length;x++){var b=f[x];b.rect=b.dropzone.getRect(b.element)}return f}function _t(h,c,f){for(var x=h.dropState,b=h.interactable,E=h.element,R=[],L=0,U=x.activeDrops;L<U.length;L++){var W=U[L],F=W.dropzone,q=W.element,oe=W.rect,Me=F.dropCheck(c,f,b,E,q,oe);R.push(Me?q:null)}var ye=function(Se){for(var Ve,Be,Ze,ht=[],yt=0;yt<Se.length;yt++){var et=Se[yt],pt=Se[Ve];if(et&&yt!==Ve)if(pt){var pn=Ue(et),Nt=Ue(pt);if(pn!==et.ownerDocument)if(Nt!==et.ownerDocument)if(pn!==Nt){ht=ht.length?ht:Je(pt);var Sn=void 0;if(pt instanceof K.HTMLElement&&et instanceof K.SVGElement&&!(et instanceof K.SVGSVGElement)){if(et===Nt)continue;Sn=et.ownerSVGElement}else Sn=et;for(var Vn=Je(Sn,pt.ownerDocument),ai=0;Vn[ai]&&Vn[ai]===ht[ai];)ai++;var Do=[Vn[ai-1],Vn[ai],ht[ai]];if(Do[0])for(var Ss=Do[0].lastChild;Ss;){if(Ss===Do[1]){Ve=yt,ht=Vn;break}if(Ss===Do[2])break;Ss=Ss.previousSibling}}else Ze=pt,(parseInt(T(Be=et).getComputedStyle(Be).zIndex,10)||0)>=(parseInt(T(Ze).getComputedStyle(Ze).zIndex,10)||0)&&(Ve=yt);else Ve=yt}else Ve=yt}return Ve}(R);return x.activeDrops[ye]||null}function St(h,c,f){var x=h.dropState,b={enter:null,leave:null,activate:null,deactivate:null,move:null,drop:null};return f.type==="dragstart"&&(b.activate=new ne(x,f,"dropactivate"),b.activate.target=null,b.activate.dropzone=null),f.type==="dragend"&&(b.deactivate=new ne(x,f,"dropdeactivate"),b.deactivate.target=null,b.deactivate.dropzone=null),x.rejected||(x.cur.element!==x.prev.element&&(x.prev.dropzone&&(b.leave=new ne(x,f,"dragleave"),f.dragLeave=b.leave.target=x.prev.element,f.prevDropzone=b.leave.dropzone=x.prev.dropzone),x.cur.dropzone&&(b.enter=new ne(x,f,"dragenter"),f.dragEnter=x.cur.element,f.dropzone=x.cur.dropzone)),f.type==="dragend"&&x.cur.dropzone&&(b.drop=new ne(x,f,"drop"),f.dropzone=x.cur.dropzone,f.relatedTarget=x.cur.element),f.type==="dragmove"&&x.cur.dropzone&&(b.move=new ne(x,f,"dropmove"),f.dropzone=x.cur.dropzone)),b}function Et(h,c){var f=h.dropState,x=f.activeDrops,b=f.cur,E=f.prev;c.leave&&E.dropzone.fire(c.leave),c.enter&&b.dropzone.fire(c.enter),c.move&&b.dropzone.fire(c.move),c.drop&&b.dropzone.fire(c.drop),c.deactivate&&Te(x,c.deactivate),f.prev.dropzone=b.dropzone,f.prev.element=b.element}function Tt(h,c){var f=h.interaction,x=h.iEvent,b=h.event;if(x.type==="dragmove"||x.type==="dragend"){var E=f.dropState;c.dynamicDrop&&(E.activeDrops=Pe(c,f.element));var R=x,L=_t(f,R,b);E.rejected=E.rejected&&!!L&&L.dropzone===E.cur.dropzone&&L.element===E.cur.element,E.cur.dropzone=L&&L.dropzone,E.cur.element=L&&L.element,E.events=St(f,0,R)}}var ct={id:"actions/drop",install:function(h){var c=h.actions,f=h.interactStatic,x=h.Interactable,b=h.defaults;h.usePlugin(X),x.prototype.dropzone=function(E){return function(R,L){if(A.object(L)){if(R.options.drop.enabled=L.enabled!==!1,L.listeners){var U=ge(L.listeners),W=Object.keys(U).reduce(function(q,oe){return q[/^(enter|leave)/.test(oe)?"drag".concat(oe):/^(activate|deactivate|move)/.test(oe)?"drop".concat(oe):oe]=U[oe],q},{}),F=R.options.drop.listeners;F&&R.off(F),R.on(W),R.options.drop.listeners=W}return A.func(L.ondrop)&&R.on("drop",L.ondrop),A.func(L.ondropactivate)&&R.on("dropactivate",L.ondropactivate),A.func(L.ondropdeactivate)&&R.on("dropdeactivate",L.ondropdeactivate),A.func(L.ondragenter)&&R.on("dragenter",L.ondragenter),A.func(L.ondragleave)&&R.on("dragleave",L.ondragleave),A.func(L.ondropmove)&&R.on("dropmove",L.ondropmove),/^(pointer|center)$/.test(L.overlap)?R.options.drop.overlap=L.overlap:A.number(L.overlap)&&(R.options.drop.overlap=Math.max(Math.min(1,L.overlap),0)),"accept"in L&&(R.options.drop.accept=L.accept),"checker"in L&&(R.options.drop.checker=L.checker),R}return A.bool(L)?(R.options.drop.enabled=L,R):R.options.drop}(this,E)},x.prototype.dropCheck=function(E,R,L,U,W,F){return function(q,oe,Me,ye,Se,Ve,Be){var Ze=!1;if(!(Be=Be||q.getRect(Ve)))return!!q.options.drop.checker&&q.options.drop.checker(oe,Me,Ze,q,Ve,ye,Se);var ht=q.options.drop.overlap;if(ht==="pointer"){var yt=Ae(ye,Se,"drag"),et=Ye(oe);et.x+=yt.x,et.y+=yt.y;var pt=et.x>Be.left&&et.x<Be.right,pn=et.y>Be.top&&et.y<Be.bottom;Ze=pt&&pn}var Nt=ye.getRect(Se);if(Nt&&ht==="center"){var Sn=Nt.left+Nt.width/2,Vn=Nt.top+Nt.height/2;Ze=Sn>=Be.left&&Sn<=Be.right&&Vn>=Be.top&&Vn<=Be.bottom}return Nt&&A.number(ht)&&(Ze=Math.max(0,Math.min(Be.right,Nt.right)-Math.max(Be.left,Nt.left))*Math.max(0,Math.min(Be.bottom,Nt.bottom)-Math.max(Be.top,Nt.top))/(Nt.width*Nt.height)>=ht),q.options.drop.checker&&(Ze=q.options.drop.checker(oe,Me,Ze,q,Ve,ye,Se)),Ze}(this,E,R,L,U,W,F)},f.dynamicDrop=function(E){return A.bool(E)?(h.dynamicDrop=E,f):h.dynamicDrop},be(c.phaselessTypes,{dragenter:!0,dragleave:!0,dropactivate:!0,dropdeactivate:!0,dropmove:!0,drop:!0}),c.methodDict.drop="dropzone",h.dynamicDrop=!1,b.actions.drop=ct.defaults},listeners:{"interactions:before-action-start":function(h){var c=h.interaction;c.prepared.name==="drag"&&(c.dropState={cur:{dropzone:null,element:null},prev:{dropzone:null,element:null},rejected:null,events:null,activeDrops:[]})},"interactions:after-action-start":function(h,c){var f=h.interaction,x=(h.event,h.iEvent);if(f.prepared.name==="drag"){var b=f.dropState;b.activeDrops=[],b.events={},b.activeDrops=Pe(c,f.element),b.events=St(f,0,x),b.events.activate&&(Te(b.activeDrops,b.events.activate),c.fire("actions/drop:start",{interaction:f,dragEvent:x}))}},"interactions:action-move":Tt,"interactions:after-action-move":function(h,c){var f=h.interaction,x=h.iEvent;if(f.prepared.name==="drag"){var b=f.dropState;Et(f,b.events),c.fire("actions/drop:move",{interaction:f,dragEvent:x}),b.events={}}},"interactions:action-end":function(h,c){if(h.interaction.prepared.name==="drag"){var f=h.interaction,x=h.iEvent;Tt(h,c),Et(f,f.dropState.events),c.fire("actions/drop:end",{interaction:f,dragEvent:x})}},"interactions:stop":function(h){var c=h.interaction;if(c.prepared.name==="drag"){var f=c.dropState;f&&(f.activeDrops=null,f.events=null,f.cur.dropzone=null,f.cur.element=null,f.prev.dropzone=null,f.prev.element=null,f.rejected=!1)}}},getActiveDrops:Pe,getDrop:_t,getDropEvents:St,fireDropEvents:Et,filterEventType:function(h){return h.search("drag")===0||h.search("drop")===0},defaults:{enabled:!1,accept:null,overlap:"pointer"}},Kt=ct;function At(h){var c=h.interaction,f=h.iEvent,x=h.phase;if(c.prepared.name==="gesture"){var b=c.pointers.map(function(W){return W.pointer}),E=x==="start",R=x==="end",L=c.interactable.options.deltaSource;if(f.touches=[b[0],b[1]],E)f.distance=lt(b,L),f.box=Xe(b),f.scale=1,f.ds=0,f.angle=je(b,L),f.da=0,c.gesture.startDistance=f.distance,c.gesture.startAngle=f.angle;else if(R||c.pointers.length<2){var U=c.prevEvent;f.distance=U.distance,f.box=U.box,f.scale=U.scale,f.ds=0,f.angle=U.angle,f.da=0}else f.distance=lt(b,L),f.box=Xe(b),f.scale=f.distance/c.gesture.startDistance,f.angle=je(b,L),f.ds=f.scale-c.gesture.scale,f.da=f.angle-c.gesture.angle;c.gesture.distance=f.distance,c.gesture.angle=f.angle,A.number(f.scale)&&f.scale!==1/0&&!isNaN(f.scale)&&(c.gesture.scale=f.scale)}}var Pn={id:"actions/gesture",before:["actions/drag","actions/resize"],install:function(h){var c=h.actions,f=h.Interactable,x=h.defaults;f.prototype.gesturable=function(b){return A.object(b)?(this.options.gesture.enabled=b.enabled!==!1,this.setPerAction("gesture",b),this.setOnEvents("gesture",b),this):A.bool(b)?(this.options.gesture.enabled=b,this):this.options.gesture},c.map.gesture=Pn,c.methodDict.gesture="gesturable",x.actions.gesture=Pn.defaults},listeners:{"interactions:action-start":At,"interactions:action-move":At,"interactions:action-end":At,"interactions:new":function(h){h.interaction.gesture={angle:0,distance:0,scale:1,startAngle:0,startDistance:0}},"auto-start:check":function(h){if(!(h.interaction.pointers.length<2)){var c=h.interactable.options.gesture;if(c&&c.enabled)return h.action={name:"gesture"},!1}}},defaults:{},getCursor:function(){return""},filterEventType:function(h){return h.search("gesture")===0}},si=Pn;function Yi(h,c,f,x,b,E,R){if(!c)return!1;if(c===!0){var L=A.number(E.width)?E.width:E.right-E.left,U=A.number(E.height)?E.height:E.bottom-E.top;if(R=Math.min(R,Math.abs((h==="left"||h==="right"?L:U)/2)),L<0&&(h==="left"?h="right":h==="right"&&(h="left")),U<0&&(h==="top"?h="bottom":h==="bottom"&&(h="top")),h==="left"){var W=L>=0?E.left:E.right;return f.x<W+R}if(h==="top"){var F=U>=0?E.top:E.bottom;return f.y<F+R}if(h==="right")return f.x>(L>=0?E.right:E.left)-R;if(h==="bottom")return f.y>(U>=0?E.bottom:E.top)-R}return!!A.element(x)&&(A.element(c)?c===x:vt(x,c,b))}function qi(h){var c=h.iEvent,f=h.interaction;if(f.prepared.name==="resize"&&f.resizeAxes){var x=c;f.interactable.options.resize.square?(f.resizeAxes==="y"?x.delta.x=x.delta.y:x.delta.y=x.delta.x,x.axes="xy"):(x.axes=f.resizeAxes,f.resizeAxes==="x"?x.delta.y=0:f.resizeAxes==="y"&&(x.delta.x=0))}}var In,Ln,Dn={id:"actions/resize",before:["actions/drag"],install:function(h){var c=h.actions,f=h.browser,x=h.Interactable,b=h.defaults;Dn.cursors=function(E){return E.isIe9?{x:"e-resize",y:"s-resize",xy:"se-resize",top:"n-resize",left:"w-resize",bottom:"s-resize",right:"e-resize",topleft:"se-resize",bottomright:"se-resize",topright:"ne-resize",bottomleft:"ne-resize"}:{x:"ew-resize",y:"ns-resize",xy:"nwse-resize",top:"ns-resize",left:"ew-resize",bottom:"ns-resize",right:"ew-resize",topleft:"nwse-resize",bottomright:"nwse-resize",topright:"nesw-resize",bottomleft:"nesw-resize"}}(f),Dn.defaultMargin=f.supportsTouch||f.supportsPointerEvent?20:10,x.prototype.resizable=function(E){return function(R,L,U){return A.object(L)?(R.options.resize.enabled=L.enabled!==!1,R.setPerAction("resize",L),R.setOnEvents("resize",L),A.string(L.axis)&&/^x$|^y$|^xy$/.test(L.axis)?R.options.resize.axis=L.axis:L.axis===null&&(R.options.resize.axis=U.defaults.actions.resize.axis),A.bool(L.preserveAspectRatio)?R.options.resize.preserveAspectRatio=L.preserveAspectRatio:A.bool(L.square)&&(R.options.resize.square=L.square),R):A.bool(L)?(R.options.resize.enabled=L,R):R.options.resize}(this,E,h)},c.map.resize=Dn,c.methodDict.resize="resizable",b.actions.resize=Dn.defaults},listeners:{"interactions:new":function(h){h.interaction.resizeAxes="xy"},"interactions:action-start":function(h){(function(c){var f=c.iEvent,x=c.interaction;if(x.prepared.name==="resize"&&x.prepared.edges){var b=f,E=x.rect;x._rects={start:be({},E),corrected:be({},E),previous:be({},E),delta:{left:0,right:0,width:0,top:0,bottom:0,height:0}},b.edges=x.prepared.edges,b.rect=x._rects.corrected,b.deltaRect=x._rects.delta}})(h),qi(h)},"interactions:action-move":function(h){(function(c){var f=c.iEvent,x=c.interaction;if(x.prepared.name==="resize"&&x.prepared.edges){var b=f,E=x.interactable.options.resize.invert,R=E==="reposition"||E==="negate",L=x.rect,U=x._rects,W=U.start,F=U.corrected,q=U.delta,oe=U.previous;if(be(oe,F),R){if(be(F,L),E==="reposition"){if(F.top>F.bottom){var Me=F.top;F.top=F.bottom,F.bottom=Me}if(F.left>F.right){var ye=F.left;F.left=F.right,F.right=ye}}}else F.top=Math.min(L.top,W.bottom),F.bottom=Math.max(L.bottom,W.top),F.left=Math.min(L.left,W.right),F.right=Math.max(L.right,W.left);for(var Se in F.width=F.right-F.left,F.height=F.bottom-F.top,F)q[Se]=F[Se]-oe[Se];b.edges=x.prepared.edges,b.rect=F,b.deltaRect=q}})(h),qi(h)},"interactions:action-end":function(h){var c=h.iEvent,f=h.interaction;if(f.prepared.name==="resize"&&f.prepared.edges){var x=c;x.edges=f.prepared.edges,x.rect=f._rects.corrected,x.deltaRect=f._rects.delta}},"auto-start:check":function(h){var c=h.interaction,f=h.interactable,x=h.element,b=h.rect,E=h.buttons;if(b){var R=be({},c.coords.cur.page),L=f.options.resize;if(L&&L.enabled&&(!c.pointerIsDown||!/mouse|pointer/.test(c.pointerType)||E&L.mouseButtons)){if(A.object(L.edges)){var U={left:!1,right:!1,top:!1,bottom:!1};for(var W in U)U[W]=Yi(W,L.edges[W],R,c._latestPointer.eventTarget,x,b,L.margin||Dn.defaultMargin);U.left=U.left&&!U.right,U.top=U.top&&!U.bottom,(U.left||U.right||U.top||U.bottom)&&(h.action={name:"resize",edges:U})}else{var F=L.axis!=="y"&&R.x>b.right-Dn.defaultMargin,q=L.axis!=="x"&&R.y>b.bottom-Dn.defaultMargin;(F||q)&&(h.action={name:"resize",axes:(F?"x":"")+(q?"y":"")})}return!h.action&&void 0}}}},defaults:{square:!1,preserveAspectRatio:!1,axis:"xy",margin:NaN,edges:null,invert:"none"},cursors:null,getCursor:function(h){var c=h.edges,f=h.axis,x=h.name,b=Dn.cursors,E=null;if(f)E=b[x+f];else if(c){for(var R="",L=0,U=["top","bottom","left","right"];L<U.length;L++){var W=U[L];c[W]&&(R+=W)}E=b[R]}return E},filterEventType:function(h){return h.search("resize")===0},defaultMargin:null},Eo=Dn,yl={id:"actions",install:function(h){h.usePlugin(si),h.usePlugin(Eo),h.usePlugin(X),h.usePlugin(Kt)}},To=0,Zn={request:function(h){return In(h)},cancel:function(h){return Ln(h)},init:function(h){if(In=h.requestAnimationFrame,Ln=h.cancelAnimationFrame,!In)for(var c=["ms","moz","webkit","o"],f=0;f<c.length;f++){var x=c[f];In=h["".concat(x,"RequestAnimationFrame")],Ln=h["".concat(x,"CancelAnimationFrame")]||h["".concat(x,"CancelRequestAnimationFrame")]}In=In&&In.bind(h),Ln=Ln&&Ln.bind(h),In||(In=function(b){var E=Date.now(),R=Math.max(0,16-(E-To)),L=h.setTimeout(function(){b(E+R)},R);return To=E+R,L},Ln=function(b){return clearTimeout(b)})}},P={defaults:{enabled:!1,margin:60,container:null,speed:300},now:Date.now,interaction:null,i:0,x:0,y:0,isScrolling:!1,prevTime:0,margin:0,speed:0,start:function(h){P.isScrolling=!0,Zn.cancel(P.i),h.autoScroll=P,P.interaction=h,P.prevTime=P.now(),P.i=Zn.request(P.scroll)},stop:function(){P.isScrolling=!1,P.interaction&&(P.interaction.autoScroll=null),Zn.cancel(P.i)},scroll:function(){var h=P.interaction,c=h.interactable,f=h.element,x=h.prepared.name,b=c.options[x].autoScroll,E=j(b.container,c,f),R=P.now(),L=(R-P.prevTime)/1e3,U=b.speed*L;if(U>=1){var W={x:P.x*U,y:P.y*U};if(W.x||W.y){var F=ie(E);A.window(E)?E.scrollBy(W.x,W.y):E&&(E.scrollLeft+=W.x,E.scrollTop+=W.y);var q=ie(E),oe={x:q.x-F.x,y:q.y-F.y};(oe.x||oe.y)&&c.fire({type:"autoscroll",target:f,interactable:c,delta:oe,interaction:h,container:E})}P.prevTime=R}P.isScrolling&&(Zn.cancel(P.i),P.i=Zn.request(P.scroll))},check:function(h,c){var f;return(f=h.options[c].autoScroll)==null?void 0:f.enabled},onInteractionMove:function(h){var c=h.interaction,f=h.pointer;if(c.interacting()&&P.check(c.interactable,c.prepared.name))if(c.simulation)P.x=P.y=0;else{var x,b,E,R,L=c.interactable,U=c.element,W=c.prepared.name,F=L.options[W].autoScroll,q=j(F.container,L,U);if(A.window(q))R=f.clientX<P.margin,x=f.clientY<P.margin,b=f.clientX>q.innerWidth-P.margin,E=f.clientY>q.innerHeight-P.margin;else{var oe=we(q);R=f.clientX<oe.left+P.margin,x=f.clientY<oe.top+P.margin,b=f.clientX>oe.right-P.margin,E=f.clientY>oe.bottom-P.margin}P.x=b?1:R?-1:0,P.y=E?1:x?-1:0,P.isScrolling||(P.margin=F.margin,P.speed=F.speed,P.start(c))}}};function j(h,c,f){return(A.string(h)?Z(h,c,f):h)||T(f)}function ie(h){return A.window(h)&&(h=window.document.body),{x:h.scrollLeft,y:h.scrollTop}}var ee={id:"auto-scroll",install:function(h){var c=h.defaults,f=h.actions;h.autoScroll=P,P.now=function(){return h.now()},f.phaselessTypes.autoscroll=!0,c.perAction.autoScroll=P.defaults},listeners:{"interactions:new":function(h){h.interaction.autoScroll=null},"interactions:destroy":function(h){h.interaction.autoScroll=null,P.stop(),P.interaction&&(P.interaction=null)},"interactions:stop":P.stop,"interactions:action-move":function(h){return P.onInteractionMove(h)}}},te=ee;function De(h,c){var f=!1;return function(){return f||(w.console.warn(c),f=!0),h.apply(this,arguments)}}function We(h,c){return h.name=c.name,h.axis=c.axis,h.edges=c.edges,h}function qe(h){return A.bool(h)?(this.options.styleCursor=h,this):h===null?(delete this.options.styleCursor,this):this.options.styleCursor}function $e(h){return A.func(h)?(this.options.actionChecker=h,this):h===null?(delete this.options.actionChecker,this):this.options.actionChecker}var rt={id:"auto-start/interactableMethods",install:function(h){var c=h.Interactable;c.prototype.getAction=function(f,x,b,E){var R=function(L,U,W,F,q){var oe=L.getRect(F),Me=U.buttons||{0:1,1:4,3:8,4:16}[U.button],ye={action:null,interactable:L,interaction:W,element:F,rect:oe,buttons:Me};return q.fire("auto-start:check",ye),ye.action}(this,x,b,E,h);return this.options.actionChecker?this.options.actionChecker(f,x,R,this,E,b):R},c.prototype.ignoreFrom=De(function(f){return this._backCompatOption("ignoreFrom",f)},"Interactable.ignoreFrom() has been deprecated. Use Interactble.draggable({ignoreFrom: newValue})."),c.prototype.allowFrom=De(function(f){return this._backCompatOption("allowFrom",f)},"Interactable.allowFrom() has been deprecated. Use Interactble.draggable({allowFrom: newValue})."),c.prototype.actionChecker=$e,c.prototype.styleCursor=qe}};function ot(h,c,f,x,b){return c.testIgnoreAllow(c.options[h.name],f,x)&&c.options[h.name].enabled&&Jt(c,f,h,b)?h:null}function gt(h,c,f,x,b,E,R){for(var L=0,U=x.length;L<U;L++){var W=x[L],F=b[L],q=W.getAction(c,f,h,F);if(q){var oe=ot(q,W,F,E,R);if(oe)return{action:oe,interactable:W,element:F}}}return{action:null,interactable:null,element:null}}function Ut(h,c,f,x,b){var E=[],R=[],L=x;function U(F){E.push(F),R.push(L)}for(;A.element(L);){E=[],R=[],b.interactables.forEachMatch(L,U);var W=gt(h,c,f,E,R,x,b);if(W.action&&!W.interactable.options[W.action.name].manualStart)return W;L=pe(L)}return{action:null,interactable:null,element:null}}function Yt(h,c,f){var x=c.action,b=c.interactable,E=c.element;x=x||{name:null},h.interactable=b,h.element=E,We(h.prepared,x),h.rect=b&&x.name?b.getRect(E):null,it(h,f),f.fire("autoStart:prepared",{interaction:h})}function Jt(h,c,f,x){var b=h.options,E=b[f.name].max,R=b[f.name].maxPerElement,L=x.autoStart.maxInteractions,U=0,W=0,F=0;if(!(E&&R&&L))return!1;for(var q=0,oe=x.interactions.list;q<oe.length;q++){var Me=oe[q],ye=Me.prepared.name;if(Me.interacting()&&(++U>=L||Me.interactable===h&&((W+=ye===f.name?1:0)>=E||Me.element===c&&(F++,ye===f.name&&F>=R))))return!1}return L>0}function Nn(h,c){return A.number(h)?(c.autoStart.maxInteractions=h,this):c.autoStart.maxInteractions}function Mt(h,c,f){var x=f.autoStart.cursorElement;x&&x!==h&&(x.style.cursor=""),h.ownerDocument.documentElement.style.cursor=c,h.style.cursor=c,f.autoStart.cursorElement=c?h:null}function it(h,c){var f=h.interactable,x=h.element,b=h.prepared;if(h.pointerType==="mouse"&&f&&f.options.styleCursor){var E="";if(b.name){var R=f.options[b.name].cursorChecker;E=A.func(R)?R(b,f,x,h._interacting):c.actions.map[b.name].getCursor(b)}Mt(h.element,E||"",c)}else c.autoStart.cursorElement&&Mt(c.autoStart.cursorElement,"",c)}var Zi={id:"auto-start/base",before:["actions"],install:function(h){var c=h.interactStatic,f=h.defaults;h.usePlugin(rt),f.base.actionChecker=null,f.base.styleCursor=!0,be(f.perAction,{manualStart:!1,max:1/0,maxPerElement:1,allowFrom:null,ignoreFrom:null,mouseButtons:1}),c.maxInteractions=function(x){return Nn(x,h)},h.autoStart={maxInteractions:1/0,withinInteractionLimit:Jt,cursorElement:null}},listeners:{"interactions:down":function(h,c){var f=h.interaction,x=h.pointer,b=h.event,E=h.eventTarget;f.interacting()||Yt(f,Ut(f,x,b,E,c),c)},"interactions:move":function(h,c){(function(f,x){var b=f.interaction,E=f.pointer,R=f.event,L=f.eventTarget;b.pointerType!=="mouse"||b.pointerIsDown||b.interacting()||Yt(b,Ut(b,E,R,L,x),x)})(h,c),function(f,x){var b=f.interaction;if(b.pointerIsDown&&!b.interacting()&&b.pointerWasMoved&&b.prepared.name){x.fire("autoStart:before-start",f);var E=b.interactable,R=b.prepared.name;R&&E&&(E.options[R].manualStart||!Jt(E,b.element,b.prepared,x)?b.stop():(b.start(b.prepared,E,b.element),it(b,x)))}}(h,c)},"interactions:stop":function(h,c){var f=h.interaction,x=f.interactable;x&&x.options.styleCursor&&Mt(f.element,"",c)}},maxInteractions:Nn,withinInteractionLimit:Jt,validateAction:ot},Pt=Zi,oi={id:"auto-start/dragAxis",listeners:{"autoStart:before-start":function(h,c){var f=h.interaction,x=h.eventTarget,b=h.dx,E=h.dy;if(f.prepared.name==="drag"){var R=Math.abs(b),L=Math.abs(E),U=f.interactable.options.drag,W=U.startAxis,F=R>L?"x":R<L?"y":"xy";if(f.prepared.axis=U.lockAxis==="start"?F[0]:U.lockAxis,F!=="xy"&&W!=="xy"&&W!==F){f.prepared.name=null;for(var q=x,oe=function(ye){if(ye!==f.interactable){var Se=f.interactable.options.drag;if(!Se.manualStart&&ye.testIgnoreAllow(Se,q,x)){var Ve=ye.getAction(f.downPointer,f.downEvent,f,q);if(Ve&&Ve.name==="drag"&&function(Be,Ze){if(!Ze)return!1;var ht=Ze.options.drag.startAxis;return Be==="xy"||ht==="xy"||ht===Be}(F,ye)&&Pt.validateAction(Ve,ye,q,x,c))return ye}}};A.element(q);){var Me=c.interactables.forEachMatch(q,oe);if(Me){f.prepared.name="drag",f.interactable=Me,f.element=q;break}q=pe(q)}}}}}};function Ir(h){var c=h.prepared&&h.prepared.name;if(!c)return null;var f=h.interactable.options;return f[c].hold||f[c].delay}var ji={id:"auto-start/hold",install:function(h){var c=h.defaults;h.usePlugin(Pt),c.perAction.hold=0,c.perAction.delay=0},listeners:{"interactions:new":function(h){h.interaction.autoStartHoldTimer=null},"autoStart:prepared":function(h){var c=h.interaction,f=Ir(c);f>0&&(c.autoStartHoldTimer=setTimeout(function(){c.start(c.prepared,c.interactable,c.element)},f))},"interactions:move":function(h){var c=h.interaction,f=h.duplicate;c.autoStartHoldTimer&&c.pointerWasMoved&&!f&&(clearTimeout(c.autoStartHoldTimer),c.autoStartHoldTimer=null)},"autoStart:before-start":function(h){var c=h.interaction;Ir(c)>0&&(c.prepared.name=null)}},getHoldDuration:Ir},vs=ji,$t={id:"auto-start",install:function(h){h.usePlugin(Pt),h.usePlugin(vs),h.usePlugin(oi)}},jn=function(h){return/^(always|never|auto)$/.test(h)?(this.options.preventDefault=h,this):A.bool(h)?(this.options.preventDefault=h?"always":"never",this):this.options.preventDefault};function _s(h){var c=h.interaction,f=h.event;c.interactable&&c.interactable.checkAndPreventDefault(f)}var dn={id:"core/interactablePreventDefault",install:function(h){var c=h.Interactable;c.prototype.preventDefault=jn,c.prototype.checkAndPreventDefault=function(f){return function(x,b,E){var R=x.options.preventDefault;if(R!=="never")if(R!=="always"){if(b.events.supportsPassive&&/^touch(start|move)$/.test(E.type)){var L=T(E.target).document,U=b.getDocOptions(L);if(!U||!U.events||U.events.passive!==!1)return}/^(mouse|pointer|touch)*(down|start)/i.test(E.type)||A.element(E.target)&&ve(E.target,"input,select,textarea,[contenteditable=true],[contenteditable=true] *")||E.preventDefault()}else E.preventDefault()}(this,h,f)},h.interactions.docEvents.push({type:"dragstart",listener:function(f){for(var x=0,b=h.interactions.list;x<b.length;x++){var E=b[x];if(E.element&&(E.element===f.target||de(E.element,f.target)))return void E.interactable.checkAndPreventDefault(f)}}})},listeners:["down","move","up","cancel"].reduce(function(h,c){return h["interactions:".concat(c)]=_s,h},{})};function Ei(h,c){if(c.phaselessTypes[h])return!0;for(var f in c.map)if(h.indexOf(f)===0&&h.substr(f.length)in c.phases)return!0;return!1}function Ti(h){var c={};for(var f in h){var x=h[f];A.plainObject(x)?c[f]=Ti(x):A.array(x)?c[f]=fe(x):c[f]=x}return c}var Lr=function(){function h(c){r(this,h),this.states=[],this.startOffset={left:0,right:0,top:0,bottom:0},this.startDelta=void 0,this.result=void 0,this.endResult=void 0,this.startEdges=void 0,this.edges=void 0,this.interaction=void 0,this.interaction=c,this.result=Ao(),this.edges={left:!1,right:!1,top:!1,bottom:!1}}return a(h,[{key:"start",value:function(c,f){var x,b,E=c.phase,R=this.interaction,L=function(W){var F=W.interactable.options[W.prepared.name],q=F.modifiers;return q&&q.length?q:["snap","snapSize","snapEdges","restrict","restrictEdges","restrictSize"].map(function(oe){var Me=F[oe];return Me&&Me.enabled&&{options:Me,methods:Me._methods}}).filter(function(oe){return!!oe})}(R);this.prepareStates(L),this.startEdges=be({},R.edges),this.edges=be({},this.startEdges),this.startOffset=(x=R.rect,b=f,x?{left:b.x-x.left,top:b.y-x.top,right:x.right-b.x,bottom:x.bottom-b.y}:{left:0,top:0,right:0,bottom:0}),this.startDelta={x:0,y:0};var U=this.fillArg({phase:E,pageCoords:f,preEnd:!1});return this.result=Ao(),this.startAll(U),this.result=this.setAll(U)}},{key:"fillArg",value:function(c){var f=this.interaction;return c.interaction=f,c.interactable=f.interactable,c.element=f.element,c.rect||(c.rect=f.rect),c.edges||(c.edges=this.startEdges),c.startOffset=this.startOffset,c}},{key:"startAll",value:function(c){for(var f=0,x=this.states;f<x.length;f++){var b=x[f];b.methods.start&&(c.state=b,b.methods.start(c))}}},{key:"setAll",value:function(c){var f=c.phase,x=c.preEnd,b=c.skipModifiers,E=c.rect,R=c.edges;c.coords=be({},c.pageCoords),c.rect=be({},E),c.edges=be({},R);for(var L=b?this.states.slice(b):this.states,U=Ao(c.coords,c.rect),W=0;W<L.length;W++){var F,q=L[W],oe=q.options,Me=be({},c.coords),ye=null;(F=q.methods)!=null&&F.set&&this.shouldDo(oe,x,f)&&(c.state=q,ye=q.methods.set(c),he(c.edges,c.rect,{x:c.coords.x-Me.x,y:c.coords.y-Me.y})),U.eventProps.push(ye)}be(this.edges,c.edges),U.delta.x=c.coords.x-c.pageCoords.x,U.delta.y=c.coords.y-c.pageCoords.y,U.rectDelta.left=c.rect.left-E.left,U.rectDelta.right=c.rect.right-E.right,U.rectDelta.top=c.rect.top-E.top,U.rectDelta.bottom=c.rect.bottom-E.bottom;var Se=this.result.coords,Ve=this.result.rect;if(Se&&Ve){var Be=U.rect.left!==Ve.left||U.rect.right!==Ve.right||U.rect.top!==Ve.top||U.rect.bottom!==Ve.bottom;U.changed=Be||Se.x!==U.coords.x||Se.y!==U.coords.y}return U}},{key:"applyToInteraction",value:function(c){var f=this.interaction,x=c.phase,b=f.coords.cur,E=f.coords.start,R=this.result,L=this.startDelta,U=R.delta;x==="start"&&be(this.startDelta,R.delta);for(var W=0,F=[[E,L],[b,U]];W<F.length;W++){var q=F[W],oe=q[0],Me=q[1];oe.page.x+=Me.x,oe.page.y+=Me.y,oe.client.x+=Me.x,oe.client.y+=Me.y}var ye=this.result.rectDelta,Se=c.rect||f.rect;Se.left+=ye.left,Se.right+=ye.right,Se.top+=ye.top,Se.bottom+=ye.bottom,Se.width=Se.right-Se.left,Se.height=Se.bottom-Se.top}},{key:"setAndApply",value:function(c){var f=this.interaction,x=c.phase,b=c.preEnd,E=c.skipModifiers,R=this.setAll(this.fillArg({preEnd:b,phase:x,pageCoords:c.modifiedCoords||f.coords.cur.page}));if(this.result=R,!R.changed&&(!E||E<this.states.length)&&f.interacting())return!1;if(c.modifiedCoords){var L=f.coords.cur.page,U={x:c.modifiedCoords.x-L.x,y:c.modifiedCoords.y-L.y};R.coords.x+=U.x,R.coords.y+=U.y,R.delta.x+=U.x,R.delta.y+=U.y}this.applyToInteraction(c)}},{key:"beforeEnd",value:function(c){var f=c.interaction,x=c.event,b=this.states;if(b&&b.length){for(var E=!1,R=0;R<b.length;R++){var L=b[R];c.state=L;var U=L.options,W=L.methods,F=W.beforeEnd&&W.beforeEnd(c);if(F)return this.endResult=F,!1;E=E||!E&&this.shouldDo(U,!0,c.phase,!0)}E&&f.move({event:x,preEnd:!0})}}},{key:"stop",value:function(c){var f=c.interaction;if(this.states&&this.states.length){var x=be({states:this.states,interactable:f.interactable,element:f.element,rect:null},c);this.fillArg(x);for(var b=0,E=this.states;b<E.length;b++){var R=E[b];x.state=R,R.methods.stop&&R.methods.stop(x)}this.states=null,this.endResult=null}}},{key:"prepareStates",value:function(c){this.states=[];for(var f=0;f<c.length;f++){var x=c[f],b=x.options,E=x.methods,R=x.name;this.states.push({options:b,methods:E,index:f,name:R})}return this.states}},{key:"restoreInteractionCoords",value:function(c){var f=c.interaction,x=f.coords,b=f.rect,E=f.modification;if(E.result){for(var R=E.startDelta,L=E.result,U=L.delta,W=L.rectDelta,F=0,q=[[x.start,R],[x.cur,U]];F<q.length;F++){var oe=q[F],Me=oe[0],ye=oe[1];Me.page.x-=ye.x,Me.page.y-=ye.y,Me.client.x-=ye.x,Me.client.y-=ye.y}b.left-=W.left,b.right-=W.right,b.top-=W.top,b.bottom-=W.bottom}}},{key:"shouldDo",value:function(c,f,x,b){return!(!c||c.enabled===!1||b&&!c.endOnly||c.endOnly&&!f||x==="start"&&!c.setStart)}},{key:"copyFrom",value:function(c){this.startOffset=c.startOffset,this.startDelta=c.startDelta,this.startEdges=c.startEdges,this.edges=c.edges,this.states=c.states.map(function(f){return Ti(f)}),this.result=Ao(be({},c.result.coords),be({},c.result.rect))}},{key:"destroy",value:function(){for(var c in this)this[c]=null}}]),h}();function Ao(h,c){return{rect:c,coords:h,delta:{x:0,y:0},rectDelta:{left:0,right:0,top:0,bottom:0},eventProps:[],changed:!0}}function Ai(h,c){var f=h.defaults,x={start:h.start,set:h.set,beforeEnd:h.beforeEnd,stop:h.stop},b=function(E){var R=E||{};for(var L in R.enabled=R.enabled!==!1,f)L in R||(R[L]=f[L]);var U={options:R,methods:x,name:c,enable:function(){return R.enabled=!0,U},disable:function(){return R.enabled=!1,U}};return U};return c&&typeof c=="string"&&(b._defaults=f,b._methods=x),b}function ys(h){var c=h.iEvent,f=h.interaction.modification.result;f&&(c.modifiers=f.eventProps)}var Am={id:"modifiers/base",before:["actions"],install:function(h){h.defaults.perAction.modifiers=[]},listeners:{"interactions:new":function(h){var c=h.interaction;c.modification=new Lr(c)},"interactions:before-action-start":function(h){var c=h.interaction,f=h.interaction.modification;f.start(h,c.coords.start.page),c.edges=f.edges,f.applyToInteraction(h)},"interactions:before-action-move":function(h){var c=h.interaction,f=c.modification,x=f.setAndApply(h);return c.edges=f.edges,x},"interactions:before-action-end":function(h){var c=h.interaction,f=c.modification,x=f.beforeEnd(h);return c.edges=f.startEdges,x},"interactions:action-start":ys,"interactions:action-move":ys,"interactions:action-end":ys,"interactions:after-action-start":function(h){return h.interaction.modification.restoreInteractionCoords(h)},"interactions:after-action-move":function(h){return h.interaction.modification.restoreInteractionCoords(h)},"interactions:stop":function(h){return h.interaction.modification.stop(h)}}},tu=Am,nu={base:{preventDefault:"auto",deltaSource:"page"},perAction:{enabled:!1,origin:{x:0,y:0}},actions:{}},xl=function(h){u(f,h);var c=g(f);function f(x,b,E,R,L,U,W){var F;r(this,f),(F=c.call(this,x)).relatedTarget=null,F.screenX=void 0,F.screenY=void 0,F.button=void 0,F.buttons=void 0,F.ctrlKey=void 0,F.shiftKey=void 0,F.altKey=void 0,F.metaKey=void 0,F.page=void 0,F.client=void 0,F.delta=void 0,F.rect=void 0,F.x0=void 0,F.y0=void 0,F.t0=void 0,F.dt=void 0,F.duration=void 0,F.clientX0=void 0,F.clientY0=void 0,F.velocity=void 0,F.speed=void 0,F.swipe=void 0,F.axes=void 0,F.preEnd=void 0,L=L||x.element;var q=x.interactable,oe=(q&&q.options||nu).deltaSource,Me=Ae(q,L,E),ye=R==="start",Se=R==="end",Ve=ye?p(F):x.prevEvent,Be=ye?x.coords.start:Se?{page:Ve.page,client:Ve.client,timeStamp:x.coords.cur.timeStamp}:x.coords.cur;return F.page=be({},Be.page),F.client=be({},Be.client),F.rect=be({},x.rect),F.timeStamp=Be.timeStamp,Se||(F.page.x-=Me.x,F.page.y-=Me.y,F.client.x-=Me.x,F.client.y-=Me.y),F.ctrlKey=b.ctrlKey,F.altKey=b.altKey,F.shiftKey=b.shiftKey,F.metaKey=b.metaKey,F.button=b.button,F.buttons=b.buttons,F.target=L,F.currentTarget=L,F.preEnd=U,F.type=W||E+(R||""),F.interactable=q,F.t0=ye?x.pointers[x.pointers.length-1].downTime:Ve.t0,F.x0=x.coords.start.page.x-Me.x,F.y0=x.coords.start.page.y-Me.y,F.clientX0=x.coords.start.client.x-Me.x,F.clientY0=x.coords.start.client.y-Me.y,F.delta=ye||Se?{x:0,y:0}:{x:F[oe].x-Ve[oe].x,y:F[oe].y-Ve[oe].y},F.dt=x.coords.delta.timeStamp,F.duration=F.timeStamp-F.t0,F.velocity=be({},x.coords.velocity[oe]),F.speed=Ge(F.velocity.x,F.velocity.y),F.swipe=Se||R==="inertiastart"?F.getSwipe():null,F}return a(f,[{key:"getSwipe",value:function(){var x=this._interaction;if(x.prevEvent.speed<600||this.timeStamp-x.prevEvent.timeStamp>150)return null;var b=180*Math.atan2(x.prevEvent.velocityY,x.prevEvent.velocityX)/Math.PI;b<0&&(b+=360);var E=112.5<=b&&b<247.5,R=202.5<=b&&b<337.5;return{up:R,down:!R&&22.5<=b&&b<157.5,left:E,right:!E&&(292.5<=b||b<67.5),angle:b,speed:x.prevEvent.speed,velocity:{x:x.prevEvent.velocityX,y:x.prevEvent.velocityY}}}},{key:"preventDefault",value:function(){}},{key:"stopImmediatePropagation",value:function(){this.immediatePropagationStopped=this.propagationStopped=!0}},{key:"stopPropagation",value:function(){this.propagationStopped=!0}}]),f}(ft);Object.defineProperties(xl.prototype,{pageX:{get:function(){return this.page.x},set:function(h){this.page.x=h}},pageY:{get:function(){return this.page.y},set:function(h){this.page.y=h}},clientX:{get:function(){return this.client.x},set:function(h){this.client.x=h}},clientY:{get:function(){return this.client.y},set:function(h){this.client.y=h}},dx:{get:function(){return this.delta.x},set:function(h){this.delta.x=h}},dy:{get:function(){return this.delta.y},set:function(h){this.delta.y=h}},velocityX:{get:function(){return this.velocity.x},set:function(h){this.velocity.x=h}},velocityY:{get:function(){return this.velocity.y},set:function(h){this.velocity.y=h}}});var Cm=a(function h(c,f,x,b,E){r(this,h),this.id=void 0,this.pointer=void 0,this.event=void 0,this.downTime=void 0,this.downTarget=void 0,this.id=c,this.pointer=f,this.event=x,this.downTime=b,this.downTarget=E}),Rm=function(h){return h.interactable="",h.element="",h.prepared="",h.pointerIsDown="",h.pointerWasMoved="",h._proxy="",h}({}),iu=function(h){return h.start="",h.move="",h.end="",h.stop="",h.interacting="",h}({}),Pm=0,Im=function(){function h(c){var f=this,x=c.pointerType,b=c.scopeFire;r(this,h),this.interactable=null,this.element=null,this.rect=null,this._rects=void 0,this.edges=null,this._scopeFire=void 0,this.prepared={name:null,axis:null,edges:null},this.pointerType=void 0,this.pointers=[],this.downEvent=null,this.downPointer={},this._latestPointer={pointer:null,event:null,eventTarget:null},this.prevEvent=null,this.pointerIsDown=!1,this.pointerWasMoved=!1,this._interacting=!1,this._ending=!1,this._stopped=!0,this._proxy=void 0,this.simulation=null,this.doMove=De(function(F){this.move(F)},"The interaction.doMove() method has been renamed to interaction.move()"),this.coords={start:{page:{x:0,y:0},client:{x:0,y:0},timeStamp:0},prev:{page:{x:0,y:0},client:{x:0,y:0},timeStamp:0},cur:{page:{x:0,y:0},client:{x:0,y:0},timeStamp:0},delta:{page:{x:0,y:0},client:{x:0,y:0},timeStamp:0},velocity:{page:{x:0,y:0},client:{x:0,y:0},timeStamp:0}},this._id=Pm++,this._scopeFire=b,this.pointerType=x;var E=this;this._proxy={};var R=function(F){Object.defineProperty(f._proxy,F,{get:function(){return E[F]}})};for(var L in Rm)R(L);var U=function(F){Object.defineProperty(f._proxy,F,{value:function(){return E[F].apply(E,arguments)}})};for(var W in iu)U(W);this._scopeFire("interactions:new",{interaction:this})}return a(h,[{key:"pointerMoveTolerance",get:function(){return 1}},{key:"pointerDown",value:function(c,f,x){var b=this.updatePointer(c,f,x,!0),E=this.pointers[b];this._scopeFire("interactions:down",{pointer:c,event:f,eventTarget:x,pointerIndex:b,pointerInfo:E,type:"down",interaction:this})}},{key:"start",value:function(c,f,x){return!(this.interacting()||!this.pointerIsDown||this.pointers.length<(c.name==="gesture"?2:1)||!f.options[c.name].enabled)&&(We(this.prepared,c),this.interactable=f,this.element=x,this.rect=f.getRect(x),this.edges=this.prepared.edges?be({},this.prepared.edges):{left:!0,right:!0,top:!0,bottom:!0},this._stopped=!1,this._interacting=this._doPhase({interaction:this,event:this.downEvent,phase:"start"})&&!this._stopped,this._interacting)}},{key:"pointerMove",value:function(c,f,x){this.simulation||this.modification&&this.modification.endResult||this.updatePointer(c,f,x,!1);var b,E,R=this.coords.cur.page.x===this.coords.prev.page.x&&this.coords.cur.page.y===this.coords.prev.page.y&&this.coords.cur.client.x===this.coords.prev.client.x&&this.coords.cur.client.y===this.coords.prev.client.y;this.pointerIsDown&&!this.pointerWasMoved&&(b=this.coords.cur.client.x-this.coords.start.client.x,E=this.coords.cur.client.y-this.coords.start.client.y,this.pointerWasMoved=Ge(b,E)>this.pointerMoveTolerance);var L,U,W,F=this.getPointerIndex(c),q={pointer:c,pointerIndex:F,pointerInfo:this.pointers[F],event:f,type:"move",eventTarget:x,dx:b,dy:E,duplicate:R,interaction:this};R||(L=this.coords.velocity,U=this.coords.delta,W=Math.max(U.timeStamp/1e3,.001),L.page.x=U.page.x/W,L.page.y=U.page.y/W,L.client.x=U.client.x/W,L.client.y=U.client.y/W,L.timeStamp=W),this._scopeFire("interactions:move",q),R||this.simulation||(this.interacting()&&(q.type=null,this.move(q)),this.pointerWasMoved&&$(this.coords.prev,this.coords.cur))}},{key:"move",value:function(c){c&&c.event||ue(this.coords.delta),(c=be({pointer:this._latestPointer.pointer,event:this._latestPointer.event,eventTarget:this._latestPointer.eventTarget,interaction:this},c||{})).phase="move",this._doPhase(c)}},{key:"pointerUp",value:function(c,f,x,b){var E=this.getPointerIndex(c);E===-1&&(E=this.updatePointer(c,f,x,!1));var R=/cancel$/i.test(f.type)?"cancel":"up";this._scopeFire("interactions:".concat(R),{pointer:c,pointerIndex:E,pointerInfo:this.pointers[E],event:f,eventTarget:x,type:R,curEventTarget:b,interaction:this}),this.simulation||this.end(f),this.removePointer(c,f)}},{key:"documentBlur",value:function(c){this.end(c),this._scopeFire("interactions:blur",{event:c,type:"blur",interaction:this})}},{key:"end",value:function(c){var f;this._ending=!0,c=c||this._latestPointer.event,this.interacting()&&(f=this._doPhase({event:c,interaction:this,phase:"end"})),this._ending=!1,f===!0&&this.stop()}},{key:"currentAction",value:function(){return this._interacting?this.prepared.name:null}},{key:"interacting",value:function(){return this._interacting}},{key:"stop",value:function(){this._scopeFire("interactions:stop",{interaction:this}),this.interactable=this.element=null,this._interacting=!1,this._stopped=!0,this.prepared.name=this.prevEvent=null}},{key:"getPointerIndex",value:function(c){var f=Re(c);return this.pointerType==="mouse"||this.pointerType==="pen"?this.pointers.length-1:G(this.pointers,function(x){return x.id===f})}},{key:"getPointerInfo",value:function(c){return this.pointers[this.getPointerIndex(c)]}},{key:"updatePointer",value:function(c,f,x,b){var E,R,L,U=Re(c),W=this.getPointerIndex(c),F=this.pointers[W];return b=b!==!1&&(b||/(down|start)$/i.test(f.type)),F?F.pointer=c:(F=new Cm(U,c,f,null,null),W=this.pointers.length,this.pointers.push(F)),Le(this.coords.cur,this.pointers.map(function(q){return q.pointer}),this._now()),E=this.coords.delta,R=this.coords.prev,L=this.coords.cur,E.page.x=L.page.x-R.page.x,E.page.y=L.page.y-R.page.y,E.client.x=L.client.x-R.client.x,E.client.y=L.client.y-R.client.y,E.timeStamp=L.timeStamp-R.timeStamp,b&&(this.pointerIsDown=!0,F.downTime=this.coords.cur.timeStamp,F.downTarget=x,I(this.downPointer,c),this.interacting()||($(this.coords.start,this.coords.cur),$(this.coords.prev,this.coords.cur),this.downEvent=f,this.pointerWasMoved=!1)),this._updateLatestPointer(c,f,x),this._scopeFire("interactions:update-pointer",{pointer:c,event:f,eventTarget:x,down:b,pointerInfo:F,pointerIndex:W,interaction:this}),W}},{key:"removePointer",value:function(c,f){var x=this.getPointerIndex(c);if(x!==-1){var b=this.pointers[x];this._scopeFire("interactions:remove-pointer",{pointer:c,event:f,eventTarget:null,pointerIndex:x,pointerInfo:b,interaction:this}),this.pointers.splice(x,1),this.pointerIsDown=!1}}},{key:"_updateLatestPointer",value:function(c,f,x){this._latestPointer.pointer=c,this._latestPointer.event=f,this._latestPointer.eventTarget=x}},{key:"destroy",value:function(){this._latestPointer.pointer=null,this._latestPointer.event=null,this._latestPointer.eventTarget=null}},{key:"_createPreparedEvent",value:function(c,f,x,b){return new xl(this,c,this.prepared.name,f,this.element,x,b)}},{key:"_fireEvent",value:function(c){var f;(f=this.interactable)==null||f.fire(c),(!this.prevEvent||c.timeStamp>=this.prevEvent.timeStamp)&&(this.prevEvent=c)}},{key:"_doPhase",value:function(c){var f=c.event,x=c.phase,b=c.preEnd,E=c.type,R=this.rect;if(R&&x==="move"&&(he(this.edges,R,this.coords.delta[this.interactable.options.deltaSource]),R.width=R.right-R.left,R.height=R.bottom-R.top),this._scopeFire("interactions:before-action-".concat(x),c)===!1)return!1;var L=c.iEvent=this._createPreparedEvent(f,x,b,E);return this._scopeFire("interactions:action-".concat(x),c),x==="start"&&(this.prevEvent=L),this._fireEvent(L),this._scopeFire("interactions:after-action-".concat(x),c),!0}},{key:"_now",value:function(){return Date.now()}}]),h}();function ru(h){su(h.interaction)}function su(h){if(!function(f){return!(!f.offset.pending.x&&!f.offset.pending.y)}(h))return!1;var c=h.offset.pending;return Ml(h.coords.cur,c),Ml(h.coords.delta,c),he(h.edges,h.rect,c),c.x=0,c.y=0,!0}function Lm(h){var c=h.x,f=h.y;this.offset.pending.x+=c,this.offset.pending.y+=f,this.offset.total.x+=c,this.offset.total.y+=f}function Ml(h,c){var f=h.page,x=h.client,b=c.x,E=c.y;f.x+=b,f.y+=E,x.x+=b,x.y+=E}iu.offsetBy="";var Dm={id:"offset",before:["modifiers","pointer-events","actions","inertia"],install:function(h){h.Interaction.prototype.offsetBy=Lm},listeners:{"interactions:new":function(h){h.interaction.offset={total:{x:0,y:0},pending:{x:0,y:0}}},"interactions:update-pointer":function(h){return function(c){c.pointerIsDown&&(Ml(c.coords.cur,c.offset.total),c.offset.pending.x=0,c.offset.pending.y=0)}(h.interaction)},"interactions:before-action-start":ru,"interactions:before-action-move":ru,"interactions:before-action-end":function(h){var c=h.interaction;if(su(c))return c.move({offset:!0}),c.end(),!1},"interactions:stop":function(h){var c=h.interaction;c.offset.total.x=0,c.offset.total.y=0,c.offset.pending.x=0,c.offset.pending.y=0}}},ou=Dm,Nm=function(){function h(c){r(this,h),this.active=!1,this.isModified=!1,this.smoothEnd=!1,this.allowResume=!1,this.modification=void 0,this.modifierCount=0,this.modifierArg=void 0,this.startCoords=void 0,this.t0=0,this.v0=0,this.te=0,this.targetOffset=void 0,this.modifiedOffset=void 0,this.currentOffset=void 0,this.lambda_v0=0,this.one_ve_v0=0,this.timeout=void 0,this.interaction=void 0,this.interaction=c}return a(h,[{key:"start",value:function(c){var f=this.interaction,x=Co(f);if(!x||!x.enabled)return!1;var b=f.coords.velocity.client,E=Ge(b.x,b.y),R=this.modification||(this.modification=new Lr(f));if(R.copyFrom(f.modification),this.t0=f._now(),this.allowResume=x.allowResume,this.v0=E,this.currentOffset={x:0,y:0},this.startCoords=f.coords.cur.page,this.modifierArg=R.fillArg({pageCoords:this.startCoords,preEnd:!0,phase:"inertiastart"}),this.t0-f.coords.cur.timeStamp<50&&E>x.minSpeed&&E>x.endSpeed)this.startInertia();else{if(R.result=R.setAll(this.modifierArg),!R.result.changed)return!1;this.startSmoothEnd()}return f.modification.result.rect=null,f.offsetBy(this.targetOffset),f._doPhase({interaction:f,event:c,phase:"inertiastart"}),f.offsetBy({x:-this.targetOffset.x,y:-this.targetOffset.y}),f.modification.result.rect=null,this.active=!0,f.simulation=this,!0}},{key:"startInertia",value:function(){var c=this,f=this.interaction.coords.velocity.client,x=Co(this.interaction),b=x.resistance,E=-Math.log(x.endSpeed/this.v0)/b;this.targetOffset={x:(f.x-E)/b,y:(f.y-E)/b},this.te=E,this.lambda_v0=b/this.v0,this.one_ve_v0=1-x.endSpeed/this.v0;var R=this.modification,L=this.modifierArg;L.pageCoords={x:this.startCoords.x+this.targetOffset.x,y:this.startCoords.y+this.targetOffset.y},R.result=R.setAll(L),R.result.changed&&(this.isModified=!0,this.modifiedOffset={x:this.targetOffset.x+R.result.delta.x,y:this.targetOffset.y+R.result.delta.y}),this.onNextFrame(function(){return c.inertiaTick()})}},{key:"startSmoothEnd",value:function(){var c=this;this.smoothEnd=!0,this.isModified=!0,this.targetOffset={x:this.modification.result.delta.x,y:this.modification.result.delta.y},this.onNextFrame(function(){return c.smoothEndTick()})}},{key:"onNextFrame",value:function(c){var f=this;this.timeout=Zn.request(function(){f.active&&c()})}},{key:"inertiaTick",value:function(){var c,f,x,b,E,R,L,U=this,W=this.interaction,F=Co(W).resistance,q=(W._now()-this.t0)/1e3;if(q<this.te){var oe,Me=1-(Math.exp(-F*q)-this.lambda_v0)/this.one_ve_v0;this.isModified?(c=0,f=0,x=this.targetOffset.x,b=this.targetOffset.y,E=this.modifiedOffset.x,R=this.modifiedOffset.y,oe={x:au(L=Me,c,x,E),y:au(L,f,b,R)}):oe={x:this.targetOffset.x*Me,y:this.targetOffset.y*Me};var ye={x:oe.x-this.currentOffset.x,y:oe.y-this.currentOffset.y};this.currentOffset.x+=ye.x,this.currentOffset.y+=ye.y,W.offsetBy(ye),W.move(),this.onNextFrame(function(){return U.inertiaTick()})}else W.offsetBy({x:this.modifiedOffset.x-this.currentOffset.x,y:this.modifiedOffset.y-this.currentOffset.y}),this.end()}},{key:"smoothEndTick",value:function(){var c=this,f=this.interaction,x=f._now()-this.t0,b=Co(f).smoothEndDuration;if(x<b){var E={x:lu(x,0,this.targetOffset.x,b),y:lu(x,0,this.targetOffset.y,b)},R={x:E.x-this.currentOffset.x,y:E.y-this.currentOffset.y};this.currentOffset.x+=R.x,this.currentOffset.y+=R.y,f.offsetBy(R),f.move({skipModifiers:this.modifierCount}),this.onNextFrame(function(){return c.smoothEndTick()})}else f.offsetBy({x:this.targetOffset.x-this.currentOffset.x,y:this.targetOffset.y-this.currentOffset.y}),this.end()}},{key:"resume",value:function(c){var f=c.pointer,x=c.event,b=c.eventTarget,E=this.interaction;E.offsetBy({x:-this.currentOffset.x,y:-this.currentOffset.y}),E.updatePointer(f,x,b,!0),E._doPhase({interaction:E,event:x,phase:"resume"}),$(E.coords.prev,E.coords.cur),this.stop()}},{key:"end",value:function(){this.interaction.move(),this.interaction.end(),this.stop()}},{key:"stop",value:function(){this.active=this.smoothEnd=!1,this.interaction.simulation=null,Zn.cancel(this.timeout)}}]),h}();function Co(h){var c=h.interactable,f=h.prepared;return c&&c.options&&f.name&&c.options[f.name].inertia}var Om={id:"inertia",before:["modifiers","actions"],install:function(h){var c=h.defaults;h.usePlugin(ou),h.usePlugin(tu),h.actions.phases.inertiastart=!0,h.actions.phases.resume=!0,c.perAction.inertia={enabled:!1,resistance:10,minSpeed:100,endSpeed:10,allowResume:!0,smoothEndDuration:300}},listeners:{"interactions:new":function(h){var c=h.interaction;c.inertia=new Nm(c)},"interactions:before-action-end":function(h){var c=h.interaction,f=h.event;return(!c._interacting||c.simulation||!c.inertia.start(f))&&null},"interactions:down":function(h){var c=h.interaction,f=h.eventTarget,x=c.inertia;if(x.active)for(var b=f;A.element(b);){if(b===c.element){x.resume(h);break}b=pe(b)}},"interactions:stop":function(h){var c=h.interaction.inertia;c.active&&c.stop()},"interactions:before-action-resume":function(h){var c=h.interaction.modification;c.stop(h),c.start(h,h.interaction.coords.cur.page),c.applyToInteraction(h)},"interactions:before-action-inertiastart":function(h){return h.interaction.modification.setAndApply(h)},"interactions:action-resume":ys,"interactions:action-inertiastart":ys,"interactions:after-action-inertiastart":function(h){return h.interaction.modification.restoreInteractionCoords(h)},"interactions:after-action-resume":function(h){return h.interaction.modification.restoreInteractionCoords(h)}}};function au(h,c,f,x){var b=1-h;return b*b*c+2*b*h*f+h*h*x}function lu(h,c,f,x){return-f*(h/=x)*(h-2)+c}var Um=Om;function cu(h,c){for(var f=0;f<c.length;f++){var x=c[f];if(h.immediatePropagationStopped)break;x(h)}}var hu=function(){function h(c){r(this,h),this.options=void 0,this.types={},this.propagationStopped=!1,this.immediatePropagationStopped=!1,this.global=void 0,this.options=be({},c||{})}return a(h,[{key:"fire",value:function(c){var f,x=this.global;(f=this.types[c.type])&&cu(c,f),!c.propagationStopped&&x&&(f=x[c.type])&&cu(c,f)}},{key:"on",value:function(c,f){var x=ge(c,f);for(c in x)this.types[c]=V(this.types[c]||[],x[c])}},{key:"off",value:function(c,f){var x=ge(c,f);for(c in x){var b=this.types[c];if(b&&b.length)for(var E=0,R=x[c];E<R.length;E++){var L=R[E],U=b.indexOf(L);U!==-1&&b.splice(U,1)}}}},{key:"getRect",value:function(c){return null}}]),h}(),Fm=function(){function h(c){r(this,h),this.currentTarget=void 0,this.originalEvent=void 0,this.type=void 0,this.originalEvent=c,I(this,c)}return a(h,[{key:"preventOriginalDefault",value:function(){this.originalEvent.preventDefault()}},{key:"stopPropagation",value:function(){this.originalEvent.stopPropagation()}},{key:"stopImmediatePropagation",value:function(){this.originalEvent.stopImmediatePropagation()}}]),h}();function xs(h){return A.object(h)?{capture:!!h.capture,passive:!!h.passive}:{capture:!!h,passive:!1}}function Ro(h,c){return h===c||(typeof h=="boolean"?!!c.capture===h&&!c.passive:!!h.capture==!!c.capture&&!!h.passive==!!c.passive)}var Bm={id:"events",install:function(h){var c,f=[],x={},b=[],E={add:R,remove:L,addDelegate:function(F,q,oe,Me,ye){var Se=xs(ye);if(!x[oe]){x[oe]=[];for(var Ve=0;Ve<b.length;Ve++){var Be=b[Ve];R(Be,oe,U),R(Be,oe,W,!0)}}var Ze=x[oe],ht=re(Ze,function(yt){return yt.selector===F&&yt.context===q});ht||(ht={selector:F,context:q,listeners:[]},Ze.push(ht)),ht.listeners.push({func:Me,options:Se})},removeDelegate:function(F,q,oe,Me,ye){var Se,Ve=xs(ye),Be=x[oe],Ze=!1;if(Be)for(Se=Be.length-1;Se>=0;Se--){var ht=Be[Se];if(ht.selector===F&&ht.context===q){for(var yt=ht.listeners,et=yt.length-1;et>=0;et--){var pt=yt[et];if(pt.func===Me&&Ro(pt.options,Ve)){yt.splice(et,1),yt.length||(Be.splice(Se,1),L(q,oe,U),L(q,oe,W,!0)),Ze=!0;break}}if(Ze)break}}},delegateListener:U,delegateUseCapture:W,delegatedEvents:x,documents:b,targets:f,supportsOptions:!1,supportsPassive:!1};function R(F,q,oe,Me){if(F.addEventListener){var ye=xs(Me),Se=re(f,function(Ve){return Ve.eventTarget===F});Se||(Se={eventTarget:F,events:{}},f.push(Se)),Se.events[q]||(Se.events[q]=[]),re(Se.events[q],function(Ve){return Ve.func===oe&&Ro(Ve.options,ye)})||(F.addEventListener(q,oe,E.supportsOptions?ye:ye.capture),Se.events[q].push({func:oe,options:ye}))}}function L(F,q,oe,Me){if(F.addEventListener&&F.removeEventListener){var ye=G(f,function(pn){return pn.eventTarget===F}),Se=f[ye];if(Se&&Se.events)if(q!=="all"){var Ve=!1,Be=Se.events[q];if(Be){if(oe==="all"){for(var Ze=Be.length-1;Ze>=0;Ze--){var ht=Be[Ze];L(F,q,ht.func,ht.options)}return}for(var yt=xs(Me),et=0;et<Be.length;et++){var pt=Be[et];if(pt.func===oe&&Ro(pt.options,yt)){F.removeEventListener(q,oe,E.supportsOptions?yt:yt.capture),Be.splice(et,1),Be.length===0&&(delete Se.events[q],Ve=!0);break}}}Ve&&!Object.keys(Se.events).length&&f.splice(ye,1)}else for(q in Se.events)Se.events.hasOwnProperty(q)&&L(F,q,"all")}}function U(F,q){for(var oe=xs(q),Me=new Fm(F),ye=x[F.type],Se=Qe(F)[0],Ve=Se;A.element(Ve);){for(var Be=0;Be<ye.length;Be++){var Ze=ye[Be],ht=Ze.selector,yt=Ze.context;if(ve(Ve,ht)&&de(yt,Se)&&de(yt,Ve)){var et=Ze.listeners;Me.currentTarget=Ve;for(var pt=0;pt<et.length;pt++){var pn=et[pt];Ro(pn.options,oe)&&pn.func(Me)}}}Ve=pe(Ve)}}function W(F){return U(F,!0)}return(c=h.document)==null||c.createElement("div").addEventListener("test",null,{get capture(){return E.supportsOptions=!0},get passive(){return E.supportsPassive=!0}}),h.events=E,E}},bl={methodOrder:["simulationResume","mouseOrPen","hasPointer","idle"],search:function(h){for(var c=0,f=bl.methodOrder;c<f.length;c++){var x=f[c],b=bl[x](h);if(b)return b}return null},simulationResume:function(h){var c=h.pointerType,f=h.eventType,x=h.eventTarget,b=h.scope;if(!/down|start/i.test(f))return null;for(var E=0,R=b.interactions.list;E<R.length;E++){var L=R[E],U=x;if(L.simulation&&L.simulation.allowResume&&L.pointerType===c)for(;U;){if(U===L.element)return L;U=pe(U)}}return null},mouseOrPen:function(h){var c,f=h.pointerId,x=h.pointerType,b=h.eventType,E=h.scope;if(x!=="mouse"&&x!=="pen")return null;for(var R=0,L=E.interactions.list;R<L.length;R++){var U=L[R];if(U.pointerType===x){if(U.simulation&&!uu(U,f))continue;if(U.interacting())return U;c||(c=U)}}if(c)return c;for(var W=0,F=E.interactions.list;W<F.length;W++){var q=F[W];if(!(q.pointerType!==x||/down/i.test(b)&&q.simulation))return q}return null},hasPointer:function(h){for(var c=h.pointerId,f=0,x=h.scope.interactions.list;f<x.length;f++){var b=x[f];if(uu(b,c))return b}return null},idle:function(h){for(var c=h.pointerType,f=0,x=h.scope.interactions.list;f<x.length;f++){var b=x[f];if(b.pointers.length===1){var E=b.interactable;if(E&&(!E.options.gesture||!E.options.gesture.enabled))continue}else if(b.pointers.length>=2)continue;if(!b.interacting()&&c===b.pointerType)return b}return null}};function uu(h,c){return h.pointers.some(function(f){return f.id===c})}var zm=bl,Sl=["pointerDown","pointerMove","pointerUp","updatePointer","removePointer","windowBlur"];function du(h,c){return function(f){var x=c.interactions.list,b=ze(f),E=Qe(f),R=E[0],L=E[1],U=[];if(/^touch/.test(f.type)){c.prevTouchTime=c.now();for(var W=0,F=f.changedTouches;W<F.length;W++){var q=F[W],oe={pointer:q,pointerId:Re(q),pointerType:b,eventType:f.type,eventTarget:R,curEventTarget:L,scope:c},Me=fu(oe);U.push([oe.pointer,oe.eventTarget,oe.curEventTarget,Me])}}else{var ye=!1;if(!ce.supportsPointerEvent&&/mouse/.test(f.type)){for(var Se=0;Se<x.length&&!ye;Se++)ye=x[Se].pointerType!=="mouse"&&x[Se].pointerIsDown;ye=ye||c.now()-c.prevTouchTime<500||f.timeStamp===0}if(!ye){var Ve={pointer:f,pointerId:Re(f),pointerType:b,eventType:f.type,curEventTarget:L,eventTarget:R,scope:c},Be=fu(Ve);U.push([Ve.pointer,Ve.eventTarget,Ve.curEventTarget,Be])}}for(var Ze=0;Ze<U.length;Ze++){var ht=U[Ze],yt=ht[0],et=ht[1],pt=ht[2];ht[3][h](yt,f,et,pt)}}}function fu(h){var c=h.pointerType,f=h.scope,x={interaction:zm.search(h),searchDetails:h};return f.fire("interactions:find",x),x.interaction||f.interactions.new({pointerType:c})}function wl(h,c){var f=h.doc,x=h.scope,b=h.options,E=x.interactions.docEvents,R=x.events,L=R[c];for(var U in x.browser.isIOS&&!b.events&&(b.events={passive:!1}),R.delegatedEvents)L(f,U,R.delegateListener),L(f,U,R.delegateUseCapture,!0);for(var W=b&&b.events,F=0;F<E.length;F++){var q=E[F];L(f,q.type,q.listener,W)}}var km={id:"core/interactions",install:function(h){for(var c={},f=0;f<Sl.length;f++){var x=Sl[f];c[x]=du(x,h)}var b,E=ce.pEventTypes;function R(){for(var L=0,U=h.interactions.list;L<U.length;L++){var W=U[L];if(W.pointerIsDown&&W.pointerType==="touch"&&!W._interacting)for(var F=function(){var Me=oe[q];h.documents.some(function(ye){return de(ye.doc,Me.downTarget)})||W.removePointer(Me.pointer,Me.event)},q=0,oe=W.pointers;q<oe.length;q++)F()}}(b=K.PointerEvent?[{type:E.down,listener:R},{type:E.down,listener:c.pointerDown},{type:E.move,listener:c.pointerMove},{type:E.up,listener:c.pointerUp},{type:E.cancel,listener:c.pointerUp}]:[{type:"mousedown",listener:c.pointerDown},{type:"mousemove",listener:c.pointerMove},{type:"mouseup",listener:c.pointerUp},{type:"touchstart",listener:R},{type:"touchstart",listener:c.pointerDown},{type:"touchmove",listener:c.pointerMove},{type:"touchend",listener:c.pointerUp},{type:"touchcancel",listener:c.pointerUp}]).push({type:"blur",listener:function(L){for(var U=0,W=h.interactions.list;U<W.length;U++)W[U].documentBlur(L)}}),h.prevTouchTime=0,h.Interaction=function(L){u(W,L);var U=g(W);function W(){return r(this,W),U.apply(this,arguments)}return a(W,[{key:"pointerMoveTolerance",get:function(){return h.interactions.pointerMoveTolerance},set:function(F){h.interactions.pointerMoveTolerance=F}},{key:"_now",value:function(){return h.now()}}]),W}(Im),h.interactions={list:[],new:function(L){L.scopeFire=function(W,F){return h.fire(W,F)};var U=new h.Interaction(L);return h.interactions.list.push(U),U},listeners:c,docEvents:b,pointerMoveTolerance:1},h.usePlugin(dn)},listeners:{"scope:add-document":function(h){return wl(h,"add")},"scope:remove-document":function(h){return wl(h,"remove")},"interactable:unset":function(h,c){for(var f=h.interactable,x=c.interactions.list.length-1;x>=0;x--){var b=c.interactions.list[x];b.interactable===f&&(b.stop(),c.fire("interactions:destroy",{interaction:b}),b.destroy(),c.interactions.list.length>2&&c.interactions.list.splice(x,1))}}},onDocSignal:wl,doOnInteractions:du,methodNames:Sl},Vm=km,Ci=function(h){return h[h.On=0]="On",h[h.Off=1]="Off",h}(Ci||{}),Hm=function(){function h(c,f,x,b){r(this,h),this.target=void 0,this.options=void 0,this._actions=void 0,this.events=new hu,this._context=void 0,this._win=void 0,this._doc=void 0,this._scopeEvents=void 0,this._actions=f.actions,this.target=c,this._context=f.context||x,this._win=T(nt(c)?this._context:c),this._doc=this._win.document,this._scopeEvents=b,this.set(f)}return a(h,[{key:"_defaults",get:function(){return{base:{},perAction:{},actions:{}}}},{key:"setOnEvents",value:function(c,f){return A.func(f.onstart)&&this.on("".concat(c,"start"),f.onstart),A.func(f.onmove)&&this.on("".concat(c,"move"),f.onmove),A.func(f.onend)&&this.on("".concat(c,"end"),f.onend),A.func(f.oninertiastart)&&this.on("".concat(c,"inertiastart"),f.oninertiastart),this}},{key:"updatePerActionListeners",value:function(c,f,x){var b,E=this,R=(b=this._actions.map[c])==null?void 0:b.filterEventType,L=function(U){return(R==null||R(U))&&Ei(U,E._actions)};(A.array(f)||A.object(f))&&this._onOff(Ci.Off,c,f,void 0,L),(A.array(x)||A.object(x))&&this._onOff(Ci.On,c,x,void 0,L)}},{key:"setPerAction",value:function(c,f){var x=this._defaults;for(var b in f){var E=b,R=this.options[c],L=f[E];E==="listeners"&&this.updatePerActionListeners(c,R.listeners,L),A.array(L)?R[E]=fe(L):A.plainObject(L)?(R[E]=be(R[E]||{},Ti(L)),A.object(x.perAction[E])&&"enabled"in x.perAction[E]&&(R[E].enabled=L.enabled!==!1)):A.bool(L)&&A.object(x.perAction[E])?R[E].enabled=L:R[E]=L}}},{key:"getRect",value:function(c){return c=c||(A.element(this.target)?this.target:null),A.string(this.target)&&(c=c||this._context.querySelector(this.target)),ke(c)}},{key:"rectChecker",value:function(c){var f=this;return A.func(c)?(this.getRect=function(x){var b=be({},c.apply(f,x));return"width"in b||(b.width=b.right-b.left,b.height=b.bottom-b.top),b},this):c===null?(delete this.getRect,this):this.getRect}},{key:"_backCompatOption",value:function(c,f){if(nt(f)||A.object(f)){for(var x in this.options[c]=f,this._actions.map)this.options[x][c]=f;return this}return this.options[c]}},{key:"origin",value:function(c){return this._backCompatOption("origin",c)}},{key:"deltaSource",value:function(c){return c==="page"||c==="client"?(this.options.deltaSource=c,this):this.options.deltaSource}},{key:"getAllElements",value:function(){var c=this.target;return A.string(c)?Array.from(this._context.querySelectorAll(c)):A.func(c)&&c.getAllElements?c.getAllElements():A.element(c)?[c]:[]}},{key:"context",value:function(){return this._context}},{key:"inContext",value:function(c){return this._context===c.ownerDocument||de(this._context,c)}},{key:"testIgnoreAllow",value:function(c,f,x){return!this.testIgnore(c.ignoreFrom,f,x)&&this.testAllow(c.allowFrom,f,x)}},{key:"testAllow",value:function(c,f,x){return!c||!!A.element(x)&&(A.string(c)?vt(x,c,f):!!A.element(c)&&de(c,x))}},{key:"testIgnore",value:function(c,f,x){return!(!c||!A.element(x))&&(A.string(c)?vt(x,c,f):!!A.element(c)&&de(c,x))}},{key:"fire",value:function(c){return this.events.fire(c),this}},{key:"_onOff",value:function(c,f,x,b,E){A.object(f)&&!A.array(f)&&(b=x,x=null);var R=ge(f,x,E);for(var L in R){L==="wheel"&&(L=ce.wheelEvent);for(var U=0,W=R[L];U<W.length;U++){var F=W[U];Ei(L,this._actions)?this.events[c===Ci.On?"on":"off"](L,F):A.string(this.target)?this._scopeEvents[c===Ci.On?"addDelegate":"removeDelegate"](this.target,this._context,L,F,b):this._scopeEvents[c===Ci.On?"add":"remove"](this.target,L,F,b)}}return this}},{key:"on",value:function(c,f,x){return this._onOff(Ci.On,c,f,x)}},{key:"off",value:function(c,f,x){return this._onOff(Ci.Off,c,f,x)}},{key:"set",value:function(c){var f=this._defaults;for(var x in A.object(c)||(c={}),this.options=Ti(f.base),this._actions.methodDict){var b=x,E=this._actions.methodDict[b];this.options[b]={},this.setPerAction(b,be(be({},f.perAction),f.actions[b])),this[E](c[b])}for(var R in c)R!=="getRect"?A.func(this[R])&&this[R](c[R]):this.rectChecker(c.getRect);return this}},{key:"unset",value:function(){if(A.string(this.target))for(var c in this._scopeEvents.delegatedEvents)for(var f=this._scopeEvents.delegatedEvents[c],x=f.length-1;x>=0;x--){var b=f[x],E=b.selector,R=b.context,L=b.listeners;E===this.target&&R===this._context&&f.splice(x,1);for(var U=L.length-1;U>=0;U--)this._scopeEvents.removeDelegate(this.target,this._context,c,L[U][0],L[U][1])}else this._scopeEvents.remove(this.target,"all")}}]),h}(),Gm=function(){function h(c){var f=this;r(this,h),this.list=[],this.selectorMap={},this.scope=void 0,this.scope=c,c.addListeners({"interactable:unset":function(x){var b=x.interactable,E=b.target,R=A.string(E)?f.selectorMap[E]:E[f.scope.id],L=G(R,function(U){return U===b});R.splice(L,1)}})}return a(h,[{key:"new",value:function(c,f){f=be(f||{},{actions:this.scope.actions});var x=new this.scope.Interactable(c,f,this.scope.document,this.scope.events);return this.scope.addDocument(x._doc),this.list.push(x),A.string(c)?(this.selectorMap[c]||(this.selectorMap[c]=[]),this.selectorMap[c].push(x)):(x.target[this.scope.id]||Object.defineProperty(c,this.scope.id,{value:[],configurable:!0}),c[this.scope.id].push(x)),this.scope.fire("interactable:new",{target:c,options:f,interactable:x,win:this.scope._win}),x}},{key:"getExisting",value:function(c,f){var x=f&&f.context||this.scope.document,b=A.string(c),E=b?this.selectorMap[c]:c[this.scope.id];if(E)return re(E,function(R){return R._context===x&&(b||R.inContext(c))})}},{key:"forEachMatch",value:function(c,f){for(var x=0,b=this.list;x<b.length;x++){var E=b[x],R=void 0;if((A.string(E.target)?A.element(c)&&ve(c,E.target):c===E.target)&&E.inContext(c)&&(R=f(E)),R!==void 0)return R}}}]),h}(),Wm=function(){function h(){var c=this;r(this,h),this.id="__interact_scope_".concat(Math.floor(100*Math.random())),this.isInitialized=!1,this.listenerMaps=[],this.browser=ce,this.defaults=Ti(nu),this.Eventable=hu,this.actions={map:{},phases:{start:!0,move:!0,end:!0},methodDict:{},phaselessTypes:{}},this.interactStatic=function(x){var b=function E(R,L){var U=x.interactables.getExisting(R,L);return U||((U=x.interactables.new(R,L)).events.global=E.globalEvents),U};return b.getPointerAverage=Ce,b.getTouchBBox=Xe,b.getTouchDistance=lt,b.getTouchAngle=je,b.getElementRect=ke,b.getElementClientRect=we,b.matchesSelector=ve,b.closest=Q,b.globalEvents={},b.version="1.10.27",b.scope=x,b.use=function(E,R){return this.scope.usePlugin(E,R),this},b.isSet=function(E,R){return!!this.scope.interactables.get(E,R&&R.context)},b.on=De(function(E,R,L){if(A.string(E)&&E.search(" ")!==-1&&(E=E.trim().split(/ +/)),A.array(E)){for(var U=0,W=E;U<W.length;U++){var F=W[U];this.on(F,R,L)}return this}if(A.object(E)){for(var q in E)this.on(q,E[q],R);return this}return Ei(E,this.scope.actions)?this.globalEvents[E]?this.globalEvents[E].push(R):this.globalEvents[E]=[R]:this.scope.events.add(this.scope.document,E,R,{options:L}),this},"The interact.on() method is being deprecated"),b.off=De(function(E,R,L){if(A.string(E)&&E.search(" ")!==-1&&(E=E.trim().split(/ +/)),A.array(E)){for(var U=0,W=E;U<W.length;U++){var F=W[U];this.off(F,R,L)}return this}if(A.object(E)){for(var q in E)this.off(q,E[q],R);return this}var oe;return Ei(E,this.scope.actions)?E in this.globalEvents&&(oe=this.globalEvents[E].indexOf(R))!==-1&&this.globalEvents[E].splice(oe,1):this.scope.events.remove(this.scope.document,E,R,L),this},"The interact.off() method is being deprecated"),b.debug=function(){return this.scope},b.supportsTouch=function(){return ce.supportsTouch},b.supportsPointerEvent=function(){return ce.supportsPointerEvent},b.stop=function(){for(var E=0,R=this.scope.interactions.list;E<R.length;E++)R[E].stop();return this},b.pointerMoveTolerance=function(E){return A.number(E)?(this.scope.interactions.pointerMoveTolerance=E,this):this.scope.interactions.pointerMoveTolerance},b.addDocument=function(E,R){this.scope.addDocument(E,R)},b.removeDocument=function(E){this.scope.removeDocument(E)},b}(this),this.InteractEvent=xl,this.Interactable=void 0,this.interactables=new Gm(this),this._win=void 0,this.document=void 0,this.window=void 0,this.documents=[],this._plugins={list:[],map:{}},this.onWindowUnload=function(x){return c.removeDocument(x.target)};var f=this;this.Interactable=function(x){u(E,x);var b=g(E);function E(){return r(this,E),b.apply(this,arguments)}return a(E,[{key:"_defaults",get:function(){return f.defaults}},{key:"set",value:function(R){return _(d(E.prototype),"set",this).call(this,R),f.fire("interactable:set",{options:R,interactable:this}),this}},{key:"unset",value:function(){_(d(E.prototype),"unset",this).call(this);var R=f.interactables.list.indexOf(this);R<0||(f.interactables.list.splice(R,1),f.fire("interactable:unset",{interactable:this}))}}]),E}(Hm)}return a(h,[{key:"addListeners",value:function(c,f){this.listenerMaps.push({id:f,map:c})}},{key:"fire",value:function(c,f){for(var x=0,b=this.listenerMaps;x<b.length;x++){var E=b[x].map[c];if(E&&E(f,this,c)===!1)return!1}}},{key:"init",value:function(c){return this.isInitialized?this:function(f,x){return f.isInitialized=!0,A.window(x)&&S(x),K.init(x),ce.init(x),Zn.init(x),f.window=x,f.document=x.document,f.usePlugin(Vm),f.usePlugin(Bm),f}(this,c)}},{key:"pluginIsInstalled",value:function(c){var f=c.id;return f?!!this._plugins.map[f]:this._plugins.list.indexOf(c)!==-1}},{key:"usePlugin",value:function(c,f){if(!this.isInitialized)return this;if(this.pluginIsInstalled(c))return this;if(c.id&&(this._plugins.map[c.id]=c),this._plugins.list.push(c),c.install&&c.install(this,f),c.listeners&&c.before){for(var x=0,b=this.listenerMaps.length,E=c.before.reduce(function(L,U){return L[U]=!0,L[pu(U)]=!0,L},{});x<b;x++){var R=this.listenerMaps[x].id;if(R&&(E[R]||E[pu(R)]))break}this.listenerMaps.splice(x,0,{id:c.id,map:c.listeners})}else c.listeners&&this.listenerMaps.push({id:c.id,map:c.listeners});return this}},{key:"addDocument",value:function(c,f){if(this.getDocIndex(c)!==-1)return!1;var x=T(c);f=f?be({},f):{},this.documents.push({doc:c,options:f}),this.events.documents.push(c),c!==this.document&&this.events.add(x,"unload",this.onWindowUnload),this.fire("scope:add-document",{doc:c,window:x,scope:this,options:f})}},{key:"removeDocument",value:function(c){var f=this.getDocIndex(c),x=T(c),b=this.documents[f].options;this.events.remove(x,"unload",this.onWindowUnload),this.documents.splice(f,1),this.events.documents.splice(f,1),this.fire("scope:remove-document",{doc:c,window:x,scope:this,options:b})}},{key:"getDocIndex",value:function(c){for(var f=0;f<this.documents.length;f++)if(this.documents[f].doc===c)return f;return-1}},{key:"getDocOptions",value:function(c){var f=this.getDocIndex(c);return f===-1?null:this.documents[f].options}},{key:"now",value:function(){return(this.window.Date||Date).now()}}]),h}();function pu(h){return h&&h.replace(/\/.*$/,"")}var mu=new Wm,fn=mu.interactStatic,Xm=typeof globalThis<"u"?globalThis:window;mu.init(Xm);var Ym=Object.freeze({__proto__:null,edgeTarget:function(){},elements:function(){},grid:function(h){var c=[["x","y"],["left","top"],["right","bottom"],["width","height"]].filter(function(x){var b=x[0],E=x[1];return b in h||E in h}),f=function(x,b){for(var E=h.range,R=h.limits,L=R===void 0?{left:-1/0,right:1/0,top:-1/0,bottom:1/0}:R,U=h.offset,W=U===void 0?{x:0,y:0}:U,F={range:E,grid:h,x:null,y:null},q=0;q<c.length;q++){var oe=c[q],Me=oe[0],ye=oe[1],Se=Math.round((x-W.x)/h[Me]),Ve=Math.round((b-W.y)/h[ye]);F[Me]=Math.max(L.left,Math.min(L.right,Se*h[Me]+W.x)),F[ye]=Math.max(L.top,Math.min(L.bottom,Ve*h[ye]+W.y))}return F};return f.grid=h,f.coordFields=c,f}}),qm={id:"snappers",install:function(h){var c=h.interactStatic;c.snappers=be(c.snappers||{},Ym),c.createSnapGrid=c.snappers.grid}},Zm=qm,jm={start:function(h){var c=h.state,f=h.rect,x=h.edges,b=h.pageCoords,E=c.options,R=E.ratio,L=E.enabled,U=c.options,W=U.equalDelta,F=U.modifiers;R==="preserve"&&(R=f.width/f.height),c.startCoords=be({},b),c.startRect=be({},f),c.ratio=R,c.equalDelta=W;var q=c.linkedEdges={top:x.top||x.left&&!x.bottom,left:x.left||x.top&&!x.right,bottom:x.bottom||x.right&&!x.top,right:x.right||x.bottom&&!x.left};if(c.xIsPrimaryAxis=!(!x.left&&!x.right),c.equalDelta){var oe=(q.left?1:-1)*(q.top?1:-1);c.edgeSign={x:oe,y:oe}}else c.edgeSign={x:q.left?-1:1,y:q.top?-1:1};if(L!==!1&&be(x,q),F!=null&&F.length){var Me=new Lr(h.interaction);Me.copyFrom(h.interaction.modification),Me.prepareStates(F),c.subModification=Me,Me.startAll(n({},h))}},set:function(h){var c=h.state,f=h.rect,x=h.coords,b=c.linkedEdges,E=be({},x),R=c.equalDelta?Km:Jm;if(be(h.edges,b),R(c,c.xIsPrimaryAxis,x,f),!c.subModification)return null;var L=be({},f);he(b,L,{x:x.x-E.x,y:x.y-E.y});var U=c.subModification.setAll(n(n({},h),{},{rect:L,edges:b,pageCoords:x,prevCoords:x,prevRect:L})),W=U.delta;return U.changed&&(R(c,Math.abs(W.x)>Math.abs(W.y),U.coords,U.rect),be(x,U.coords)),U.eventProps},defaults:{ratio:"preserve",equalDelta:!1,modifiers:[],enabled:!1}};function Km(h,c,f){var x=h.startCoords,b=h.edgeSign;c?f.y=x.y+(f.x-x.x)*b.y:f.x=x.x+(f.y-x.y)*b.x}function Jm(h,c,f,x){var b=h.startRect,E=h.startCoords,R=h.ratio,L=h.edgeSign;if(c){var U=x.width/R;f.y=E.y+(U-b.height)*L.y}else{var W=x.height*R;f.x=E.x+(W-b.width)*L.x}}var $m=Ai(jm,"aspectRatio"),gu=function(){};gu._defaults={};var Po=gu;function Ki(h,c,f){return A.func(h)?st(h,c.interactable,c.element,[f.x,f.y,c]):st(h,c.interactable,c.element)}var Io={start:function(h){var c=h.rect,f=h.startOffset,x=h.state,b=h.interaction,E=h.pageCoords,R=x.options,L=R.elementRect,U=be({left:0,top:0,right:0,bottom:0},R.offset||{});if(c&&L){var W=Ki(R.restriction,b,E);if(W){var F=W.right-W.left-c.width,q=W.bottom-W.top-c.height;F<0&&(U.left+=F,U.right+=F),q<0&&(U.top+=q,U.bottom+=q)}U.left+=f.left-c.width*L.left,U.top+=f.top-c.height*L.top,U.right+=f.right-c.width*(1-L.right),U.bottom+=f.bottom-c.height*(1-L.bottom)}x.offset=U},set:function(h){var c=h.coords,f=h.interaction,x=h.state,b=x.options,E=x.offset,R=Ki(b.restriction,f,c);if(R){var L=function(U){return!U||"left"in U&&"top"in U||((U=be({},U)).left=U.x||0,U.top=U.y||0,U.right=U.right||U.left+U.width,U.bottom=U.bottom||U.top+U.height),U}(R);c.x=Math.max(Math.min(L.right-E.right,c.x),L.left+E.left),c.y=Math.max(Math.min(L.bottom-E.bottom,c.y),L.top+E.top)}},defaults:{restriction:null,elementRect:null,offset:null,endOnly:!1,enabled:!1}},Qm=Ai(Io,"restrict"),vu={top:1/0,left:1/0,bottom:-1/0,right:-1/0},_u={top:-1/0,left:-1/0,bottom:1/0,right:1/0};function yu(h,c){for(var f=0,x=["top","left","bottom","right"];f<x.length;f++){var b=x[f];b in h||(h[b]=c[b])}return h}var Ms={noInner:vu,noOuter:_u,start:function(h){var c,f=h.interaction,x=h.startOffset,b=h.state,E=b.options;E&&(c=le(Ki(E.offset,f,f.coords.start.page))),c=c||{x:0,y:0},b.offset={top:c.y+x.top,left:c.x+x.left,bottom:c.y-x.bottom,right:c.x-x.right}},set:function(h){var c=h.coords,f=h.edges,x=h.interaction,b=h.state,E=b.offset,R=b.options;if(f){var L=be({},c),U=Ki(R.inner,x,L)||{},W=Ki(R.outer,x,L)||{};yu(U,vu),yu(W,_u),f.top?c.y=Math.min(Math.max(W.top+E.top,L.y),U.top+E.top):f.bottom&&(c.y=Math.max(Math.min(W.bottom+E.bottom,L.y),U.bottom+E.bottom)),f.left?c.x=Math.min(Math.max(W.left+E.left,L.x),U.left+E.left):f.right&&(c.x=Math.max(Math.min(W.right+E.right,L.x),U.right+E.right))}},defaults:{inner:null,outer:null,offset:null,endOnly:!1,enabled:!1}},eg=Ai(Ms,"restrictEdges"),tg=be({get elementRect(){return{top:0,left:0,bottom:1,right:1}},set elementRect(h){}},Io.defaults),ng=Ai({start:Io.start,set:Io.set,defaults:tg},"restrictRect"),ig={width:-1/0,height:-1/0},rg={width:1/0,height:1/0},sg=Ai({start:function(h){return Ms.start(h)},set:function(h){var c=h.interaction,f=h.state,x=h.rect,b=h.edges,E=f.options;if(b){var R=xe(Ki(E.min,c,h.coords))||ig,L=xe(Ki(E.max,c,h.coords))||rg;f.options={endOnly:E.endOnly,inner:be({},Ms.noInner),outer:be({},Ms.noOuter)},b.top?(f.options.inner.top=x.bottom-R.height,f.options.outer.top=x.bottom-L.height):b.bottom&&(f.options.inner.bottom=x.top+R.height,f.options.outer.bottom=x.top+L.height),b.left?(f.options.inner.left=x.right-R.width,f.options.outer.left=x.right-L.width):b.right&&(f.options.inner.right=x.left+R.width,f.options.outer.right=x.left+L.width),Ms.set(h),f.options=E}},defaults:{min:null,max:null,endOnly:!1,enabled:!1}},"restrictSize"),El={start:function(h){var c,f=h.interaction,x=h.interactable,b=h.element,E=h.rect,R=h.state,L=h.startOffset,U=R.options,W=U.offsetWithOrigin?function(oe){var Me=oe.interaction.element,ye=le(st(oe.state.options.origin,null,null,[Me])),Se=ye||Ae(oe.interactable,Me,oe.interaction.prepared.name);return Se}(h):{x:0,y:0};if(U.offset==="startCoords")c={x:f.coords.start.page.x,y:f.coords.start.page.y};else{var F=st(U.offset,x,b,[f]);(c=le(F)||{x:0,y:0}).x+=W.x,c.y+=W.y}var q=U.relativePoints;R.offsets=E&&q&&q.length?q.map(function(oe,Me){return{index:Me,relativePoint:oe,x:L.left-E.width*oe.x+c.x,y:L.top-E.height*oe.y+c.y}}):[{index:0,relativePoint:null,x:c.x,y:c.y}]},set:function(h){var c=h.interaction,f=h.coords,x=h.state,b=x.options,E=x.offsets,R=Ae(c.interactable,c.element,c.prepared.name),L=be({},f),U=[];b.offsetWithOrigin||(L.x-=R.x,L.y-=R.y);for(var W=0,F=E;W<F.length;W++)for(var q=F[W],oe=L.x-q.x,Me=L.y-q.y,ye=0,Se=b.targets.length;ye<Se;ye++){var Ve=b.targets[ye],Be=void 0;(Be=A.func(Ve)?Ve(oe,Me,c._proxy,q,ye):Ve)&&U.push({x:(A.number(Be.x)?Be.x:oe)+q.x,y:(A.number(Be.y)?Be.y:Me)+q.y,range:A.number(Be.range)?Be.range:b.range,source:Ve,index:ye,offset:q})}for(var Ze={target:null,inRange:!1,distance:0,range:0,delta:{x:0,y:0}},ht=0;ht<U.length;ht++){var yt=U[ht],et=yt.range,pt=yt.x-L.x,pn=yt.y-L.y,Nt=Ge(pt,pn),Sn=Nt<=et;et===1/0&&Ze.inRange&&Ze.range!==1/0&&(Sn=!1),Ze.target&&!(Sn?Ze.inRange&&et!==1/0?Nt/et<Ze.distance/Ze.range:et===1/0&&Ze.range!==1/0||Nt<Ze.distance:!Ze.inRange&&Nt<Ze.distance)||(Ze.target=yt,Ze.distance=Nt,Ze.range=et,Ze.inRange=Sn,Ze.delta.x=pt,Ze.delta.y=pn)}return Ze.inRange&&(f.x=Ze.target.x,f.y=Ze.target.y),x.closest=Ze,Ze},defaults:{range:1/0,targets:null,offset:null,offsetWithOrigin:!0,origin:null,relativePoints:null,endOnly:!1,enabled:!1}},og=Ai(El,"snap"),Lo={start:function(h){var c=h.state,f=h.edges,x=c.options;if(!f)return null;h.state={options:{targets:null,relativePoints:[{x:f.left?0:1,y:f.top?0:1}],offset:x.offset||"self",origin:{x:0,y:0},range:x.range}},c.targetFields=c.targetFields||[["width","height"],["x","y"]],El.start(h),c.offsets=h.state.offsets,h.state=c},set:function(h){var c=h.interaction,f=h.state,x=h.coords,b=f.options,E=f.offsets,R={x:x.x-E[0].x,y:x.y-E[0].y};f.options=be({},b),f.options.targets=[];for(var L=0,U=b.targets||[];L<U.length;L++){var W=U[L],F=void 0;if(F=A.func(W)?W(R.x,R.y,c):W){for(var q=0,oe=f.targetFields;q<oe.length;q++){var Me=oe[q],ye=Me[0],Se=Me[1];if(ye in F||Se in F){F.x=F[ye],F.y=F[Se];break}}f.options.targets.push(F)}}var Ve=El.set(h);return f.options=b,Ve},defaults:{range:1/0,targets:null,offset:null,endOnly:!1,enabled:!1}},ag=Ai(Lo,"snapSize"),Tl={aspectRatio:$m,restrictEdges:eg,restrict:Qm,restrictRect:ng,restrictSize:sg,snapEdges:Ai({start:function(h){var c=h.edges;return c?(h.state.targetFields=h.state.targetFields||[[c.left?"left":"right",c.top?"top":"bottom"]],Lo.start(h)):null},set:Lo.set,defaults:be(Ti(Lo.defaults),{targets:void 0,range:void 0,offset:{x:0,y:0}})},"snapEdges"),snap:og,snapSize:ag,spring:Po,avoid:Po,transform:Po,rubberband:Po},lg={id:"modifiers",install:function(h){var c=h.interactStatic;for(var f in h.usePlugin(tu),h.usePlugin(Zm),c.modifiers=Tl,Tl){var x=Tl[f],b=x._defaults,E=x._methods;b._methods=E,h.defaults.perAction[f]=b}}},cg=lg,xu=function(h){u(f,h);var c=g(f);function f(x,b,E,R,L,U){var W;if(r(this,f),I(p(W=c.call(this,L)),E),E!==b&&I(p(W),b),W.timeStamp=U,W.originalEvent=E,W.type=x,W.pointerId=Re(b),W.pointerType=ze(b),W.target=R,W.currentTarget=null,x==="tap"){var F=L.getPointerIndex(b);W.dt=W.timeStamp-L.pointers[F].downTime;var q=W.timeStamp-L.tapTime;W.double=!!L.prevTap&&L.prevTap.type!=="doubletap"&&L.prevTap.target===W.target&&q<500}else x==="doubletap"&&(W.dt=b.timeStamp-L.tapTime,W.double=!0);return W}return a(f,[{key:"_subtractOrigin",value:function(x){var b=x.x,E=x.y;return this.pageX-=b,this.pageY-=E,this.clientX-=b,this.clientY-=E,this}},{key:"_addOrigin",value:function(x){var b=x.x,E=x.y;return this.pageX+=b,this.pageY+=E,this.clientX+=b,this.clientY+=E,this}},{key:"preventDefault",value:function(){this.originalEvent.preventDefault()}}]),f}(ft),bs={id:"pointer-events/base",before:["inertia","modifiers","auto-start","actions"],install:function(h){h.pointerEvents=bs,h.defaults.actions.pointerEvents=bs.defaults,be(h.actions.phaselessTypes,bs.types)},listeners:{"interactions:new":function(h){var c=h.interaction;c.prevTap=null,c.tapTime=0},"interactions:update-pointer":function(h){var c=h.down,f=h.pointerInfo;!c&&f.hold||(f.hold={duration:1/0,timeout:null})},"interactions:move":function(h,c){var f=h.interaction,x=h.pointer,b=h.event,E=h.eventTarget;h.duplicate||f.pointerIsDown&&!f.pointerWasMoved||(f.pointerIsDown&&Al(h),Ri({interaction:f,pointer:x,event:b,eventTarget:E,type:"move"},c))},"interactions:down":function(h,c){(function(f,x){for(var b=f.interaction,E=f.pointer,R=f.event,L=f.eventTarget,U=f.pointerIndex,W=b.pointers[U].hold,F=Ee(L),q={interaction:b,pointer:E,event:R,eventTarget:L,type:"hold",targets:[],path:F,node:null},oe=0;oe<F.length;oe++){var Me=F[oe];q.node=Me,x.fire("pointerEvents:collect-targets",q)}if(q.targets.length){for(var ye=1/0,Se=0,Ve=q.targets;Se<Ve.length;Se++){var Be=Ve[Se].eventable.options.holdDuration;Be<ye&&(ye=Be)}W.duration=ye,W.timeout=setTimeout(function(){Ri({interaction:b,eventTarget:L,pointer:E,event:R,type:"hold"},x)},ye)}})(h,c),Ri(h,c)},"interactions:up":function(h,c){Al(h),Ri(h,c),function(f,x){var b=f.interaction,E=f.pointer,R=f.event,L=f.eventTarget;b.pointerWasMoved||Ri({interaction:b,eventTarget:L,pointer:E,event:R,type:"tap"},x)}(h,c)},"interactions:cancel":function(h,c){Al(h),Ri(h,c)}},PointerEvent:xu,fire:Ri,collectEventTargets:Mu,defaults:{holdDuration:600,ignoreFrom:null,allowFrom:null,origin:{x:0,y:0}},types:{down:!0,move:!0,up:!0,cancel:!0,tap:!0,doubletap:!0,hold:!0}};function Ri(h,c){var f=h.interaction,x=h.pointer,b=h.event,E=h.eventTarget,R=h.type,L=h.targets,U=L===void 0?Mu(h,c):L,W=new xu(R,x,b,E,f,c.now());c.fire("pointerEvents:new",{pointerEvent:W});for(var F={interaction:f,pointer:x,event:b,eventTarget:E,targets:U,type:R,pointerEvent:W},q=0;q<U.length;q++){var oe=U[q];for(var Me in oe.props||{})W[Me]=oe.props[Me];var ye=Ae(oe.eventable,oe.node);if(W._subtractOrigin(ye),W.eventable=oe.eventable,W.currentTarget=oe.node,oe.eventable.fire(W),W._addOrigin(ye),W.immediatePropagationStopped||W.propagationStopped&&q+1<U.length&&U[q+1].node!==W.currentTarget)break}if(c.fire("pointerEvents:fired",F),R==="tap"){var Se=W.double?Ri({interaction:f,pointer:x,event:b,eventTarget:E,type:"doubletap"},c):W;f.prevTap=Se,f.tapTime=Se.timeStamp}return W}function Mu(h,c){var f=h.interaction,x=h.pointer,b=h.event,E=h.eventTarget,R=h.type,L=f.getPointerIndex(x),U=f.pointers[L];if(R==="tap"&&(f.pointerWasMoved||!U||U.downTarget!==E))return[];for(var W=Ee(E),F={interaction:f,pointer:x,event:b,eventTarget:E,type:R,path:W,targets:[],node:null},q=0;q<W.length;q++){var oe=W[q];F.node=oe,c.fire("pointerEvents:collect-targets",F)}return R==="hold"&&(F.targets=F.targets.filter(function(Me){var ye,Se;return Me.eventable.options.holdDuration===((ye=f.pointers[L])==null||(Se=ye.hold)==null?void 0:Se.duration)})),F.targets}function Al(h){var c=h.interaction,f=h.pointerIndex,x=c.pointers[f].hold;x&&x.timeout&&(clearTimeout(x.timeout),x.timeout=null)}var hg=Object.freeze({__proto__:null,default:bs});function ug(h){var c=h.interaction;c.holdIntervalHandle&&(clearInterval(c.holdIntervalHandle),c.holdIntervalHandle=null)}var dg={id:"pointer-events/holdRepeat",install:function(h){h.usePlugin(bs);var c=h.pointerEvents;c.defaults.holdRepeatInterval=0,c.types.holdrepeat=h.actions.phaselessTypes.holdrepeat=!0},listeners:["move","up","cancel","endall"].reduce(function(h,c){return h["pointerEvents:".concat(c)]=ug,h},{"pointerEvents:new":function(h){var c=h.pointerEvent;c.type==="hold"&&(c.count=(c.count||0)+1)},"pointerEvents:fired":function(h,c){var f=h.interaction,x=h.pointerEvent,b=h.eventTarget,E=h.targets;if(x.type==="hold"&&E.length){var R=E[0].eventable.options.holdRepeatInterval;R<=0||(f.holdIntervalHandle=setTimeout(function(){c.pointerEvents.fire({interaction:f,eventTarget:b,type:"hold",pointer:x,event:x},c)},R))}}})},fg=dg,pg={id:"pointer-events/interactableTargets",install:function(h){var c=h.Interactable;c.prototype.pointerEvents=function(x){return be(this.events.options,x),this};var f=c.prototype._backCompatOption;c.prototype._backCompatOption=function(x,b){var E=f.call(this,x,b);return E===this&&(this.events.options[x]=b),E}},listeners:{"pointerEvents:collect-targets":function(h,c){var f=h.targets,x=h.node,b=h.type,E=h.eventTarget;c.interactables.forEachMatch(x,function(R){var L=R.events,U=L.options;L.types[b]&&L.types[b].length&&R.testIgnoreAllow(U,x,E)&&f.push({node:x,eventable:L,props:{interactable:R}})})},"interactable:new":function(h){var c=h.interactable;c.events.getRect=function(f){return c.getRect(f)}},"interactable:set":function(h,c){var f=h.interactable,x=h.options;be(f.events.options,c.pointerEvents.defaults),be(f.events.options,x.pointerEvents||{})}}},mg=pg,gg={id:"pointer-events",install:function(h){h.usePlugin(hg),h.usePlugin(fg),h.usePlugin(mg)}},vg=gg,_g={id:"reflow",install:function(h){var c=h.Interactable;h.actions.phases.reflow=!0,c.prototype.reflow=function(f){return function(x,b,E){for(var R=x.getAllElements(),L=E.window.Promise,U=L?[]:null,W=function(){var q=R[F],oe=x.getRect(q);if(!oe)return 1;var Me,ye=re(E.interactions.list,function(Be){return Be.interacting()&&Be.interactable===x&&Be.element===q&&Be.prepared.name===b.name});if(ye)ye.move(),U&&(Me=ye._reflowPromise||new L(function(Be){ye._reflowResolve=Be}));else{var Se=xe(oe),Ve=function(Be){return{coords:Be,get page(){return this.coords.page},get client(){return this.coords.client},get timeStamp(){return this.coords.timeStamp},get pageX(){return this.coords.page.x},get pageY(){return this.coords.page.y},get clientX(){return this.coords.client.x},get clientY(){return this.coords.client.y},get pointerId(){return this.coords.pointerId},get target(){return this.coords.target},get type(){return this.coords.type},get pointerType(){return this.coords.pointerType},get buttons(){return this.coords.buttons},preventDefault:function(){}}}({page:{x:Se.x,y:Se.y},client:{x:Se.x,y:Se.y},timeStamp:E.now()});Me=function(Be,Ze,ht,yt,et){var pt=Be.interactions.new({pointerType:"reflow"}),pn={interaction:pt,event:et,pointer:et,eventTarget:ht,phase:"reflow"};pt.interactable=Ze,pt.element=ht,pt.prevEvent=et,pt.updatePointer(et,et,ht,!0),ue(pt.coords.delta),We(pt.prepared,yt),pt._doPhase(pn);var Nt=Be.window,Sn=Nt.Promise,Vn=Sn?new Sn(function(ai){pt._reflowResolve=ai}):void 0;return pt._reflowPromise=Vn,pt.start(yt,Ze,ht),pt._interacting?(pt.move(pn),pt.end(et)):(pt.stop(),pt._reflowResolve()),pt.removePointer(et,et),Vn}(E,x,q,b,Ve)}U&&U.push(Me)},F=0;F<R.length&&!W();F++);return U&&L.all(U).then(function(){return x})}(this,f,h)}},listeners:{"interactions:stop":function(h,c){var f=h.interaction;f.pointerType==="reflow"&&(f._reflowResolve&&f._reflowResolve(),function(x,b){x.splice(x.indexOf(b),1)}(c.interactions.list,f))}}},yg=_g;if(fn.use(dn),fn.use(ou),fn.use(vg),fn.use(Um),fn.use(cg),fn.use($t),fn.use(yl),fn.use(te),fn.use(yg),fn.default=fn,i(s)==="object"&&s)try{s.exports=fn}catch{}return fn.default=fn,fn})})(Ba,Ba.exports);var bg=Ba.exports;const ws=Mg(bg);function Sg(s){var e=s.target,t=(parseFloat(e.getAttribute("data-x"))||0)+s.dx,n=(parseFloat(e.getAttribute("data-y"))||0)+s.dy;e.style.transform="translate("+t+"px, "+n+"px)",e.setAttribute("data-x",t),e.setAttribute("data-y",n)}function wg(s){var e=s.target,t=parseFloat(e.getAttribute("data-x"))||0,n=parseFloat(e.getAttribute("data-y"))||0;e.style.width=s.rect.width+"px",e.style.height=s.rect.height+"px",t+=s.deltaRect.left,n+=s.deltaRect.top,e.style.transform="translate("+t+"px,"+n+"px)",e.setAttribute("data-x",t),e.setAttribute("data-y",n)}function Eg(){window.dragMoveListener=Sg,window.dragResizeListener=wg,ws(".movable-box").draggable({listeners:{move:window.dragMoveListener},allowFrom:".drag-handle"}).pointerEvents({allowFrom:"*"}),ws(".resize-drag").resizable({edges:{left:!0,right:!0,bottom:!0,top:!1},listeners:{move:window.dragResizeListener},modifiers:[ws.modifiers.restrictEdges({restriction:"parent",endOnly:!0}),ws.modifiers.restrictSize({min:{width:100,height:50}})],inertia:!0}).draggable({edges:{left:!1,right:!1,bottom:!1,top:!0},listeners:{move:window.dragMoveListener},inertia:!0,modifiers:[ws.modifiers.restrictRect({restriction:"parent",endOnly:!0})]})}let Tg={setup:Eg};/**
 * @license
 * Copyright 2010-2024 Three.js Authors
 * SPDX-License-Identifier: MIT
 */const Xa="164",hr={LEFT:0,MIDDLE:1,RIGHT:2,ROTATE:0,DOLLY:1,PAN:2},ur={ROTATE:0,PAN:1,DOLLY_PAN:2,DOLLY_ROTATE:3},mf=0,gc=1,gf=2,Ag=3,Cg=0,ih=1,vf=2,Jn=3,xi=0,_n=1,Tn=2,kn=0,xr=1,za=2,vc=3,_c=4,_f=5,zi=100,yf=101,xf=102,Mf=103,bf=104,Sf=200,wf=201,Ef=202,Tf=203,ka=204,Va=205,Af=206,Cf=207,Rf=208,Pf=209,If=210,Lf=211,Df=212,Nf=213,Of=214,Uf=0,Ff=1,Bf=2,Zs=3,zf=4,kf=5,Vf=6,Hf=7,vo=0,Gf=1,Wf=2,yi=0,rh=1,sh=2,oh=3,ah=4,Xf=5,lh=6,ch=7,yc="attached",Yf="detached",Ya=300,Mi=301,Hi=302,js=303,Ks=304,us=306,Js=1e3,Bn=1001,$s=1002,nn=1003,hh=1004,Rg=1004,ts=1005,Pg=1005,Xt=1006,Ws=1007,Ig=1007,Qn=1008,Lg=1008,bi=1009,qf=1010,Zf=1011,uh=1012,dh=1013,wr=1014,zn=1015,Fn=1016,fh=1017,ph=1018,ds=1020,jf=35902,Kf=1021,Jf=1022,Cn=1023,$f=1024,Qf=1025,Mr=1026,as=1027,mh=1028,gh=1029,ep=1030,vh=1031,_h=1033,Pa=33776,Ia=33777,La=33778,Da=33779,xc=35840,Mc=35841,bc=35842,Sc=35843,wc=36196,Ec=37492,Tc=37496,Ac=37808,Cc=37809,Rc=37810,Pc=37811,Ic=37812,Lc=37813,Dc=37814,Nc=37815,Oc=37816,Uc=37817,Fc=37818,Bc=37819,zc=37820,kc=37821,Na=36492,Vc=36494,Hc=36495,tp=36283,Gc=36284,Wc=36285,Xc=36286,np=2200,ip=2201,rp=2202,Qs=2300,eo=2301,Oa=2302,gr=2400,vr=2401,to=2402,qa=2500,yh=2501,Dg=0,Ng=1,Og=2,sp=3200,xh=3201,Gi=0,op=1,pi="",Un="srgb",wi="srgb-linear",Za="display-p3",_o="display-p3-linear",no="linear",It="srgb",io="rec709",ro="p3",Ug=0,dr=7680,Fg=7681,Bg=7682,zg=7683,kg=34055,Vg=34056,Hg=5386,Gg=512,Wg=513,Xg=514,Yg=515,qg=516,Zg=517,jg=518,Yc=519,ap=512,lp=513,cp=514,Mh=515,hp=516,up=517,dp=518,fp=519,so=35044,Kg=35048,Jg=35040,$g=35045,Qg=35049,ev=35041,tv=35046,nv=35050,iv=35042,rv="100",qc="300 es",ei=2e3,oo=2001;class ni{addEventListener(e,t){this._listeners===void 0&&(this._listeners={});const n=this._listeners;n[e]===void 0&&(n[e]=[]),n[e].indexOf(t)===-1&&n[e].push(t)}hasEventListener(e,t){if(this._listeners===void 0)return!1;const n=this._listeners;return n[e]!==void 0&&n[e].indexOf(t)!==-1}removeEventListener(e,t){if(this._listeners===void 0)return;const i=this._listeners[e];if(i!==void 0){const r=i.indexOf(t);r!==-1&&i.splice(r,1)}}dispatchEvent(e){if(this._listeners===void 0)return;const n=this._listeners[e.type];if(n!==void 0){e.target=this;const i=n.slice(0);for(let r=0,o=i.length;r<o;r++)i[r].call(this,e);e.target=null}}}const rn=["00","01","02","03","04","05","06","07","08","09","0a","0b","0c","0d","0e","0f","10","11","12","13","14","15","16","17","18","19","1a","1b","1c","1d","1e","1f","20","21","22","23","24","25","26","27","28","29","2a","2b","2c","2d","2e","2f","30","31","32","33","34","35","36","37","38","39","3a","3b","3c","3d","3e","3f","40","41","42","43","44","45","46","47","48","49","4a","4b","4c","4d","4e","4f","50","51","52","53","54","55","56","57","58","59","5a","5b","5c","5d","5e","5f","60","61","62","63","64","65","66","67","68","69","6a","6b","6c","6d","6e","6f","70","71","72","73","74","75","76","77","78","79","7a","7b","7c","7d","7e","7f","80","81","82","83","84","85","86","87","88","89","8a","8b","8c","8d","8e","8f","90","91","92","93","94","95","96","97","98","99","9a","9b","9c","9d","9e","9f","a0","a1","a2","a3","a4","a5","a6","a7","a8","a9","aa","ab","ac","ad","ae","af","b0","b1","b2","b3","b4","b5","b6","b7","b8","b9","ba","bb","bc","bd","be","bf","c0","c1","c2","c3","c4","c5","c6","c7","c8","c9","ca","cb","cc","cd","ce","cf","d0","d1","d2","d3","d4","d5","d6","d7","d8","d9","da","db","dc","dd","de","df","e0","e1","e2","e3","e4","e5","e6","e7","e8","e9","ea","eb","ec","ed","ee","ef","f0","f1","f2","f3","f4","f5","f6","f7","f8","f9","fa","fb","fc","fd","fe","ff"];let bu=1234567;const br=Math.PI/180,ls=180/Math.PI;function Rn(){const s=Math.random()*4294967295|0,e=Math.random()*4294967295|0,t=Math.random()*4294967295|0,n=Math.random()*4294967295|0;return(rn[s&255]+rn[s>>8&255]+rn[s>>16&255]+rn[s>>24&255]+"-"+rn[e&255]+rn[e>>8&255]+"-"+rn[e>>16&15|64]+rn[e>>24&255]+"-"+rn[t&63|128]+rn[t>>8&255]+"-"+rn[t>>16&255]+rn[t>>24&255]+rn[n&255]+rn[n>>8&255]+rn[n>>16&255]+rn[n>>24&255]).toLowerCase()}function kt(s,e,t){return Math.max(e,Math.min(t,s))}function bh(s,e){return(s%e+e)%e}function sv(s,e,t,n,i){return n+(s-e)*(i-n)/(t-e)}function ov(s,e,t){return s!==e?(t-s)/(e-s):0}function Xs(s,e,t){return(1-t)*s+t*e}function av(s,e,t,n){return Xs(s,e,1-Math.exp(-t*n))}function lv(s,e=1){return e-Math.abs(bh(s,e*2)-e)}function cv(s,e,t){return s<=e?0:s>=t?1:(s=(s-e)/(t-e),s*s*(3-2*s))}function hv(s,e,t){return s<=e?0:s>=t?1:(s=(s-e)/(t-e),s*s*s*(s*(s*6-15)+10))}function uv(s,e){return s+Math.floor(Math.random()*(e-s+1))}function dv(s,e){return s+Math.random()*(e-s)}function fv(s){return s*(.5-Math.random())}function pv(s){s!==void 0&&(bu=s);let e=bu+=1831565813;return e=Math.imul(e^e>>>15,e|1),e^=e+Math.imul(e^e>>>7,e|61),((e^e>>>14)>>>0)/4294967296}function mv(s){return s*br}function gv(s){return s*ls}function vv(s){return(s&s-1)===0&&s!==0}function _v(s){return Math.pow(2,Math.ceil(Math.log(s)/Math.LN2))}function yv(s){return Math.pow(2,Math.floor(Math.log(s)/Math.LN2))}function xv(s,e,t,n,i){const r=Math.cos,o=Math.sin,a=r(t/2),l=o(t/2),u=r((e+n)/2),d=o((e+n)/2),m=r((e-n)/2),p=o((e-n)/2),g=r((n-e)/2),_=o((n-e)/2);switch(i){case"XYX":s.set(a*d,l*m,l*p,a*u);break;case"YZY":s.set(l*p,a*d,l*m,a*u);break;case"ZXZ":s.set(l*m,l*p,a*d,a*u);break;case"XZX":s.set(a*d,l*_,l*g,a*u);break;case"YXY":s.set(l*g,a*d,l*_,a*u);break;case"ZYZ":s.set(l*_,l*g,a*d,a*u);break;default:console.warn("THREE.MathUtils: .setQuaternionFromProperEuler() encountered an unknown order: "+i)}}function vn(s,e){switch(e.constructor){case Float32Array:return s;case Uint32Array:return s/4294967295;case Uint16Array:return s/65535;case Uint8Array:return s/255;case Int32Array:return Math.max(s/2147483647,-1);case Int16Array:return Math.max(s/32767,-1);case Int8Array:return Math.max(s/127,-1);default:throw new Error("Invalid component type.")}}function dt(s,e){switch(e.constructor){case Float32Array:return s;case Uint32Array:return Math.round(s*4294967295);case Uint16Array:return Math.round(s*65535);case Uint8Array:return Math.round(s*255);case Int32Array:return Math.round(s*2147483647);case Int16Array:return Math.round(s*32767);case Int8Array:return Math.round(s*127);default:throw new Error("Invalid component type.")}}const pp={DEG2RAD:br,RAD2DEG:ls,generateUUID:Rn,clamp:kt,euclideanModulo:bh,mapLinear:sv,inverseLerp:ov,lerp:Xs,damp:av,pingpong:lv,smoothstep:cv,smootherstep:hv,randInt:uv,randFloat:dv,randFloatSpread:fv,seededRandom:pv,degToRad:mv,radToDeg:gv,isPowerOfTwo:vv,ceilPowerOfTwo:_v,floorPowerOfTwo:yv,setQuaternionFromProperEuler:xv,normalize:dt,denormalize:vn};class se{constructor(e=0,t=0){se.prototype.isVector2=!0,this.x=e,this.y=t}get width(){return this.x}set width(e){this.x=e}get height(){return this.y}set height(e){this.y=e}set(e,t){return this.x=e,this.y=t,this}setScalar(e){return this.x=e,this.y=e,this}setX(e){return this.x=e,this}setY(e){return this.y=e,this}setComponent(e,t){switch(e){case 0:this.x=t;break;case 1:this.y=t;break;default:throw new Error("index is out of range: "+e)}return this}getComponent(e){switch(e){case 0:return this.x;case 1:return this.y;default:throw new Error("index is out of range: "+e)}}clone(){return new this.constructor(this.x,this.y)}copy(e){return this.x=e.x,this.y=e.y,this}add(e){return this.x+=e.x,this.y+=e.y,this}addScalar(e){return this.x+=e,this.y+=e,this}addVectors(e,t){return this.x=e.x+t.x,this.y=e.y+t.y,this}addScaledVector(e,t){return this.x+=e.x*t,this.y+=e.y*t,this}sub(e){return this.x-=e.x,this.y-=e.y,this}subScalar(e){return this.x-=e,this.y-=e,this}subVectors(e,t){return this.x=e.x-t.x,this.y=e.y-t.y,this}multiply(e){return this.x*=e.x,this.y*=e.y,this}multiplyScalar(e){return this.x*=e,this.y*=e,this}divide(e){return this.x/=e.x,this.y/=e.y,this}divideScalar(e){return this.multiplyScalar(1/e)}applyMatrix3(e){const t=this.x,n=this.y,i=e.elements;return this.x=i[0]*t+i[3]*n+i[6],this.y=i[1]*t+i[4]*n+i[7],this}min(e){return this.x=Math.min(this.x,e.x),this.y=Math.min(this.y,e.y),this}max(e){return this.x=Math.max(this.x,e.x),this.y=Math.max(this.y,e.y),this}clamp(e,t){return this.x=Math.max(e.x,Math.min(t.x,this.x)),this.y=Math.max(e.y,Math.min(t.y,this.y)),this}clampScalar(e,t){return this.x=Math.max(e,Math.min(t,this.x)),this.y=Math.max(e,Math.min(t,this.y)),this}clampLength(e,t){const n=this.length();return this.divideScalar(n||1).multiplyScalar(Math.max(e,Math.min(t,n)))}floor(){return this.x=Math.floor(this.x),this.y=Math.floor(this.y),this}ceil(){return this.x=Math.ceil(this.x),this.y=Math.ceil(this.y),this}round(){return this.x=Math.round(this.x),this.y=Math.round(this.y),this}roundToZero(){return this.x=Math.trunc(this.x),this.y=Math.trunc(this.y),this}negate(){return this.x=-this.x,this.y=-this.y,this}dot(e){return this.x*e.x+this.y*e.y}cross(e){return this.x*e.y-this.y*e.x}lengthSq(){return this.x*this.x+this.y*this.y}length(){return Math.sqrt(this.x*this.x+this.y*this.y)}manhattanLength(){return Math.abs(this.x)+Math.abs(this.y)}normalize(){return this.divideScalar(this.length()||1)}angle(){return Math.atan2(-this.y,-this.x)+Math.PI}angleTo(e){const t=Math.sqrt(this.lengthSq()*e.lengthSq());if(t===0)return Math.PI/2;const n=this.dot(e)/t;return Math.acos(kt(n,-1,1))}distanceTo(e){return Math.sqrt(this.distanceToSquared(e))}distanceToSquared(e){const t=this.x-e.x,n=this.y-e.y;return t*t+n*n}manhattanDistanceTo(e){return Math.abs(this.x-e.x)+Math.abs(this.y-e.y)}setLength(e){return this.normalize().multiplyScalar(e)}lerp(e,t){return this.x+=(e.x-this.x)*t,this.y+=(e.y-this.y)*t,this}lerpVectors(e,t,n){return this.x=e.x+(t.x-e.x)*n,this.y=e.y+(t.y-e.y)*n,this}equals(e){return e.x===this.x&&e.y===this.y}fromArray(e,t=0){return this.x=e[t],this.y=e[t+1],this}toArray(e=[],t=0){return e[t]=this.x,e[t+1]=this.y,e}fromBufferAttribute(e,t){return this.x=e.getX(t),this.y=e.getY(t),this}rotateAround(e,t){const n=Math.cos(t),i=Math.sin(t),r=this.x-e.x,o=this.y-e.y;return this.x=r*n-o*i+e.x,this.y=r*i+o*n+e.y,this}random(){return this.x=Math.random(),this.y=Math.random(),this}*[Symbol.iterator](){yield this.x,yield this.y}}class ut{constructor(e,t,n,i,r,o,a,l,u){ut.prototype.isMatrix3=!0,this.elements=[1,0,0,0,1,0,0,0,1],e!==void 0&&this.set(e,t,n,i,r,o,a,l,u)}set(e,t,n,i,r,o,a,l,u){const d=this.elements;return d[0]=e,d[1]=i,d[2]=a,d[3]=t,d[4]=r,d[5]=l,d[6]=n,d[7]=o,d[8]=u,this}identity(){return this.set(1,0,0,0,1,0,0,0,1),this}copy(e){const t=this.elements,n=e.elements;return t[0]=n[0],t[1]=n[1],t[2]=n[2],t[3]=n[3],t[4]=n[4],t[5]=n[5],t[6]=n[6],t[7]=n[7],t[8]=n[8],this}extractBasis(e,t,n){return e.setFromMatrix3Column(this,0),t.setFromMatrix3Column(this,1),n.setFromMatrix3Column(this,2),this}setFromMatrix4(e){const t=e.elements;return this.set(t[0],t[4],t[8],t[1],t[5],t[9],t[2],t[6],t[10]),this}multiply(e){return this.multiplyMatrices(this,e)}premultiply(e){return this.multiplyMatrices(e,this)}multiplyMatrices(e,t){const n=e.elements,i=t.elements,r=this.elements,o=n[0],a=n[3],l=n[6],u=n[1],d=n[4],m=n[7],p=n[2],g=n[5],_=n[8],M=i[0],y=i[3],v=i[6],w=i[1],S=i[4],T=i[7],z=i[2],N=i[5],A=i[8];return r[0]=o*M+a*w+l*z,r[3]=o*y+a*S+l*N,r[6]=o*v+a*T+l*A,r[1]=u*M+d*w+m*z,r[4]=u*y+d*S+m*N,r[7]=u*v+d*T+m*A,r[2]=p*M+g*w+_*z,r[5]=p*y+g*S+_*N,r[8]=p*v+g*T+_*A,this}multiplyScalar(e){const t=this.elements;return t[0]*=e,t[3]*=e,t[6]*=e,t[1]*=e,t[4]*=e,t[7]*=e,t[2]*=e,t[5]*=e,t[8]*=e,this}determinant(){const e=this.elements,t=e[0],n=e[1],i=e[2],r=e[3],o=e[4],a=e[5],l=e[6],u=e[7],d=e[8];return t*o*d-t*a*u-n*r*d+n*a*l+i*r*u-i*o*l}invert(){const e=this.elements,t=e[0],n=e[1],i=e[2],r=e[3],o=e[4],a=e[5],l=e[6],u=e[7],d=e[8],m=d*o-a*u,p=a*l-d*r,g=u*r-o*l,_=t*m+n*p+i*g;if(_===0)return this.set(0,0,0,0,0,0,0,0,0);const M=1/_;return e[0]=m*M,e[1]=(i*u-d*n)*M,e[2]=(a*n-i*o)*M,e[3]=p*M,e[4]=(d*t-i*l)*M,e[5]=(i*r-a*t)*M,e[6]=g*M,e[7]=(n*l-u*t)*M,e[8]=(o*t-n*r)*M,this}transpose(){let e;const t=this.elements;return e=t[1],t[1]=t[3],t[3]=e,e=t[2],t[2]=t[6],t[6]=e,e=t[5],t[5]=t[7],t[7]=e,this}getNormalMatrix(e){return this.setFromMatrix4(e).invert().transpose()}transposeIntoArray(e){const t=this.elements;return e[0]=t[0],e[1]=t[3],e[2]=t[6],e[3]=t[1],e[4]=t[4],e[5]=t[7],e[6]=t[2],e[7]=t[5],e[8]=t[8],this}setUvTransform(e,t,n,i,r,o,a){const l=Math.cos(r),u=Math.sin(r);return this.set(n*l,n*u,-n*(l*o+u*a)+o+e,-i*u,i*l,-i*(-u*o+l*a)+a+t,0,0,1),this}scale(e,t){return this.premultiply(Cl.makeScale(e,t)),this}rotate(e){return this.premultiply(Cl.makeRotation(-e)),this}translate(e,t){return this.premultiply(Cl.makeTranslation(e,t)),this}makeTranslation(e,t){return e.isVector2?this.set(1,0,e.x,0,1,e.y,0,0,1):this.set(1,0,e,0,1,t,0,0,1),this}makeRotation(e){const t=Math.cos(e),n=Math.sin(e);return this.set(t,-n,0,n,t,0,0,0,1),this}makeScale(e,t){return this.set(e,0,0,0,t,0,0,0,1),this}equals(e){const t=this.elements,n=e.elements;for(let i=0;i<9;i++)if(t[i]!==n[i])return!1;return!0}fromArray(e,t=0){for(let n=0;n<9;n++)this.elements[n]=e[n+t];return this}toArray(e=[],t=0){const n=this.elements;return e[t]=n[0],e[t+1]=n[1],e[t+2]=n[2],e[t+3]=n[3],e[t+4]=n[4],e[t+5]=n[5],e[t+6]=n[6],e[t+7]=n[7],e[t+8]=n[8],e}clone(){return new this.constructor().fromArray(this.elements)}}const Cl=new ut;function mp(s){for(let e=s.length-1;e>=0;--e)if(s[e]>=65535)return!0;return!1}const Mv={Int8Array,Uint8Array,Uint8ClampedArray,Int16Array,Uint16Array,Int32Array,Uint32Array,Float32Array,Float64Array};function ns(s,e){return new Mv[s](e)}function ao(s){return document.createElementNS("http://www.w3.org/1999/xhtml",s)}function gp(){const s=ao("canvas");return s.style.display="block",s}const Su={};function vp(s){s in Su||(Su[s]=!0,console.warn(s))}const wu=new ut().set(.8224621,.177538,0,.0331941,.9668058,0,.0170827,.0723974,.9105199),Eu=new ut().set(1.2249401,-.2249404,0,-.0420569,1.0420571,0,-.0196376,-.0786361,1.0982735),No={[wi]:{transfer:no,primaries:io,toReference:s=>s,fromReference:s=>s},[Un]:{transfer:It,primaries:io,toReference:s=>s.convertSRGBToLinear(),fromReference:s=>s.convertLinearToSRGB()},[_o]:{transfer:no,primaries:ro,toReference:s=>s.applyMatrix3(Eu),fromReference:s=>s.applyMatrix3(wu)},[Za]:{transfer:It,primaries:ro,toReference:s=>s.convertSRGBToLinear().applyMatrix3(Eu),fromReference:s=>s.applyMatrix3(wu).convertLinearToSRGB()}},bv=new Set([wi,_o]),wt={enabled:!0,_workingColorSpace:wi,get workingColorSpace(){return this._workingColorSpace},set workingColorSpace(s){if(!bv.has(s))throw new Error(`Unsupported working color space, "${s}".`);this._workingColorSpace=s},convert:function(s,e,t){if(this.enabled===!1||e===t||!e||!t)return s;const n=No[e].toReference,i=No[t].fromReference;return i(n(s))},fromWorkingColorSpace:function(s,e){return this.convert(s,this._workingColorSpace,e)},toWorkingColorSpace:function(s,e){return this.convert(s,e,this._workingColorSpace)},getPrimaries:function(s){return No[s].primaries},getTransfer:function(s){return s===pi?no:No[s].transfer}};function os(s){return s<.04045?s*.0773993808:Math.pow(s*.9478672986+.0521327014,2.4)}function Rl(s){return s<.0031308?s*12.92:1.055*Math.pow(s,.41666)-.055}let Dr;class _p{static getDataURL(e){if(/^data:/i.test(e.src)||typeof HTMLCanvasElement>"u")return e.src;let t;if(e instanceof HTMLCanvasElement)t=e;else{Dr===void 0&&(Dr=ao("canvas")),Dr.width=e.width,Dr.height=e.height;const n=Dr.getContext("2d");e instanceof ImageData?n.putImageData(e,0,0):n.drawImage(e,0,0,e.width,e.height),t=Dr}return t.width>2048||t.height>2048?(console.warn("THREE.ImageUtils.getDataURL: Image converted to jpg for performance reasons",e),t.toDataURL("image/jpeg",.6)):t.toDataURL("image/png")}static sRGBToLinear(e){if(typeof HTMLImageElement<"u"&&e instanceof HTMLImageElement||typeof HTMLCanvasElement<"u"&&e instanceof HTMLCanvasElement||typeof ImageBitmap<"u"&&e instanceof ImageBitmap){const t=ao("canvas");t.width=e.width,t.height=e.height;const n=t.getContext("2d");n.drawImage(e,0,0,e.width,e.height);const i=n.getImageData(0,0,e.width,e.height),r=i.data;for(let o=0;o<r.length;o++)r[o]=os(r[o]/255)*255;return n.putImageData(i,0,0),t}else if(e.data){const t=e.data.slice(0);for(let n=0;n<t.length;n++)t instanceof Uint8Array||t instanceof Uint8ClampedArray?t[n]=Math.floor(os(t[n]/255)*255):t[n]=os(t[n]);return{data:t,width:e.width,height:e.height}}else return console.warn("THREE.ImageUtils.sRGBToLinear(): Unsupported image type. No color space conversion applied."),e}}let Sv=0;class _r{constructor(e=null){this.isSource=!0,Object.defineProperty(this,"id",{value:Sv++}),this.uuid=Rn(),this.data=e,this.dataReady=!0,this.version=0}set needsUpdate(e){e===!0&&this.version++}toJSON(e){const t=e===void 0||typeof e=="string";if(!t&&e.images[this.uuid]!==void 0)return e.images[this.uuid];const n={uuid:this.uuid,url:""},i=this.data;if(i!==null){let r;if(Array.isArray(i)){r=[];for(let o=0,a=i.length;o<a;o++)i[o].isDataTexture?r.push(Pl(i[o].image)):r.push(Pl(i[o]))}else r=Pl(i);n.url=r}return t||(e.images[this.uuid]=n),n}}function Pl(s){return typeof HTMLImageElement<"u"&&s instanceof HTMLImageElement||typeof HTMLCanvasElement<"u"&&s instanceof HTMLCanvasElement||typeof ImageBitmap<"u"&&s instanceof ImageBitmap?_p.getDataURL(s):s.data?{data:Array.from(s.data),width:s.width,height:s.height,type:s.data.constructor.name}:(console.warn("THREE.Texture: Unable to serialize Texture."),{})}let wv=0;class Vt extends ni{constructor(e=Vt.DEFAULT_IMAGE,t=Vt.DEFAULT_MAPPING,n=Bn,i=Bn,r=Xt,o=Qn,a=Cn,l=bi,u=Vt.DEFAULT_ANISOTROPY,d=pi){super(),this.isTexture=!0,Object.defineProperty(this,"id",{value:wv++}),this.uuid=Rn(),this.name="",this.source=new _r(e),this.mipmaps=[],this.mapping=t,this.channel=0,this.wrapS=n,this.wrapT=i,this.magFilter=r,this.minFilter=o,this.anisotropy=u,this.format=a,this.internalFormat=null,this.type=l,this.offset=new se(0,0),this.repeat=new se(1,1),this.center=new se(0,0),this.rotation=0,this.matrixAutoUpdate=!0,this.matrix=new ut,this.generateMipmaps=!0,this.premultiplyAlpha=!1,this.flipY=!0,this.unpackAlignment=4,this.colorSpace=d,this.userData={},this.version=0,this.onUpdate=null,this.isRenderTargetTexture=!1,this.pmremVersion=0}get image(){return this.source.data}set image(e=null){this.source.data=e}updateMatrix(){this.matrix.setUvTransform(this.offset.x,this.offset.y,this.repeat.x,this.repeat.y,this.rotation,this.center.x,this.center.y)}clone(){return new this.constructor().copy(this)}copy(e){return this.name=e.name,this.source=e.source,this.mipmaps=e.mipmaps.slice(0),this.mapping=e.mapping,this.channel=e.channel,this.wrapS=e.wrapS,this.wrapT=e.wrapT,this.magFilter=e.magFilter,this.minFilter=e.minFilter,this.anisotropy=e.anisotropy,this.format=e.format,this.internalFormat=e.internalFormat,this.type=e.type,this.offset.copy(e.offset),this.repeat.copy(e.repeat),this.center.copy(e.center),this.rotation=e.rotation,this.matrixAutoUpdate=e.matrixAutoUpdate,this.matrix.copy(e.matrix),this.generateMipmaps=e.generateMipmaps,this.premultiplyAlpha=e.premultiplyAlpha,this.flipY=e.flipY,this.unpackAlignment=e.unpackAlignment,this.colorSpace=e.colorSpace,this.userData=JSON.parse(JSON.stringify(e.userData)),this.needsUpdate=!0,this}toJSON(e){const t=e===void 0||typeof e=="string";if(!t&&e.textures[this.uuid]!==void 0)return e.textures[this.uuid];const n={metadata:{version:4.6,type:"Texture",generator:"Texture.toJSON"},uuid:this.uuid,name:this.name,image:this.source.toJSON(e).uuid,mapping:this.mapping,channel:this.channel,repeat:[this.repeat.x,this.repeat.y],offset:[this.offset.x,this.offset.y],center:[this.center.x,this.center.y],rotation:this.rotation,wrap:[this.wrapS,this.wrapT],format:this.format,internalFormat:this.internalFormat,type:this.type,colorSpace:this.colorSpace,minFilter:this.minFilter,magFilter:this.magFilter,anisotropy:this.anisotropy,flipY:this.flipY,generateMipmaps:this.generateMipmaps,premultiplyAlpha:this.premultiplyAlpha,unpackAlignment:this.unpackAlignment};return Object.keys(this.userData).length>0&&(n.userData=this.userData),t||(e.textures[this.uuid]=n),n}dispose(){this.dispatchEvent({type:"dispose"})}transformUv(e){if(this.mapping!==Ya)return e;if(e.applyMatrix3(this.matrix),e.x<0||e.x>1)switch(this.wrapS){case Js:e.x=e.x-Math.floor(e.x);break;case Bn:e.x=e.x<0?0:1;break;case $s:Math.abs(Math.floor(e.x)%2)===1?e.x=Math.ceil(e.x)-e.x:e.x=e.x-Math.floor(e.x);break}if(e.y<0||e.y>1)switch(this.wrapT){case Js:e.y=e.y-Math.floor(e.y);break;case Bn:e.y=e.y<0?0:1;break;case $s:Math.abs(Math.floor(e.y)%2)===1?e.y=Math.ceil(e.y)-e.y:e.y=e.y-Math.floor(e.y);break}return this.flipY&&(e.y=1-e.y),e}set needsUpdate(e){e===!0&&(this.version++,this.source.needsUpdate=!0)}set needsPMREMUpdate(e){e===!0&&this.pmremVersion++}}Vt.DEFAULT_IMAGE=null;Vt.DEFAULT_MAPPING=Ya;Vt.DEFAULT_ANISOTROPY=1;class Ct{constructor(e=0,t=0,n=0,i=1){Ct.prototype.isVector4=!0,this.x=e,this.y=t,this.z=n,this.w=i}get width(){return this.z}set width(e){this.z=e}get height(){return this.w}set height(e){this.w=e}set(e,t,n,i){return this.x=e,this.y=t,this.z=n,this.w=i,this}setScalar(e){return this.x=e,this.y=e,this.z=e,this.w=e,this}setX(e){return this.x=e,this}setY(e){return this.y=e,this}setZ(e){return this.z=e,this}setW(e){return this.w=e,this}setComponent(e,t){switch(e){case 0:this.x=t;break;case 1:this.y=t;break;case 2:this.z=t;break;case 3:this.w=t;break;default:throw new Error("index is out of range: "+e)}return this}getComponent(e){switch(e){case 0:return this.x;case 1:return this.y;case 2:return this.z;case 3:return this.w;default:throw new Error("index is out of range: "+e)}}clone(){return new this.constructor(this.x,this.y,this.z,this.w)}copy(e){return this.x=e.x,this.y=e.y,this.z=e.z,this.w=e.w!==void 0?e.w:1,this}add(e){return this.x+=e.x,this.y+=e.y,this.z+=e.z,this.w+=e.w,this}addScalar(e){return this.x+=e,this.y+=e,this.z+=e,this.w+=e,this}addVectors(e,t){return this.x=e.x+t.x,this.y=e.y+t.y,this.z=e.z+t.z,this.w=e.w+t.w,this}addScaledVector(e,t){return this.x+=e.x*t,this.y+=e.y*t,this.z+=e.z*t,this.w+=e.w*t,this}sub(e){return this.x-=e.x,this.y-=e.y,this.z-=e.z,this.w-=e.w,this}subScalar(e){return this.x-=e,this.y-=e,this.z-=e,this.w-=e,this}subVectors(e,t){return this.x=e.x-t.x,this.y=e.y-t.y,this.z=e.z-t.z,this.w=e.w-t.w,this}multiply(e){return this.x*=e.x,this.y*=e.y,this.z*=e.z,this.w*=e.w,this}multiplyScalar(e){return this.x*=e,this.y*=e,this.z*=e,this.w*=e,this}applyMatrix4(e){const t=this.x,n=this.y,i=this.z,r=this.w,o=e.elements;return this.x=o[0]*t+o[4]*n+o[8]*i+o[12]*r,this.y=o[1]*t+o[5]*n+o[9]*i+o[13]*r,this.z=o[2]*t+o[6]*n+o[10]*i+o[14]*r,this.w=o[3]*t+o[7]*n+o[11]*i+o[15]*r,this}divideScalar(e){return this.multiplyScalar(1/e)}setAxisAngleFromQuaternion(e){this.w=2*Math.acos(e.w);const t=Math.sqrt(1-e.w*e.w);return t<1e-4?(this.x=1,this.y=0,this.z=0):(this.x=e.x/t,this.y=e.y/t,this.z=e.z/t),this}setAxisAngleFromRotationMatrix(e){let t,n,i,r;const l=e.elements,u=l[0],d=l[4],m=l[8],p=l[1],g=l[5],_=l[9],M=l[2],y=l[6],v=l[10];if(Math.abs(d-p)<.01&&Math.abs(m-M)<.01&&Math.abs(_-y)<.01){if(Math.abs(d+p)<.1&&Math.abs(m+M)<.1&&Math.abs(_+y)<.1&&Math.abs(u+g+v-3)<.1)return this.set(1,0,0,0),this;t=Math.PI;const S=(u+1)/2,T=(g+1)/2,z=(v+1)/2,N=(d+p)/4,A=(m+M)/4,k=(_+y)/4;return S>T&&S>z?S<.01?(n=0,i=.707106781,r=.707106781):(n=Math.sqrt(S),i=N/n,r=A/n):T>z?T<.01?(n=.707106781,i=0,r=.707106781):(i=Math.sqrt(T),n=N/i,r=k/i):z<.01?(n=.707106781,i=.707106781,r=0):(r=Math.sqrt(z),n=A/r,i=k/r),this.set(n,i,r,t),this}let w=Math.sqrt((y-_)*(y-_)+(m-M)*(m-M)+(p-d)*(p-d));return Math.abs(w)<.001&&(w=1),this.x=(y-_)/w,this.y=(m-M)/w,this.z=(p-d)/w,this.w=Math.acos((u+g+v-1)/2),this}min(e){return this.x=Math.min(this.x,e.x),this.y=Math.min(this.y,e.y),this.z=Math.min(this.z,e.z),this.w=Math.min(this.w,e.w),this}max(e){return this.x=Math.max(this.x,e.x),this.y=Math.max(this.y,e.y),this.z=Math.max(this.z,e.z),this.w=Math.max(this.w,e.w),this}clamp(e,t){return this.x=Math.max(e.x,Math.min(t.x,this.x)),this.y=Math.max(e.y,Math.min(t.y,this.y)),this.z=Math.max(e.z,Math.min(t.z,this.z)),this.w=Math.max(e.w,Math.min(t.w,this.w)),this}clampScalar(e,t){return this.x=Math.max(e,Math.min(t,this.x)),this.y=Math.max(e,Math.min(t,this.y)),this.z=Math.max(e,Math.min(t,this.z)),this.w=Math.max(e,Math.min(t,this.w)),this}clampLength(e,t){const n=this.length();return this.divideScalar(n||1).multiplyScalar(Math.max(e,Math.min(t,n)))}floor(){return this.x=Math.floor(this.x),this.y=Math.floor(this.y),this.z=Math.floor(this.z),this.w=Math.floor(this.w),this}ceil(){return this.x=Math.ceil(this.x),this.y=Math.ceil(this.y),this.z=Math.ceil(this.z),this.w=Math.ceil(this.w),this}round(){return this.x=Math.round(this.x),this.y=Math.round(this.y),this.z=Math.round(this.z),this.w=Math.round(this.w),this}roundToZero(){return this.x=Math.trunc(this.x),this.y=Math.trunc(this.y),this.z=Math.trunc(this.z),this.w=Math.trunc(this.w),this}negate(){return this.x=-this.x,this.y=-this.y,this.z=-this.z,this.w=-this.w,this}dot(e){return this.x*e.x+this.y*e.y+this.z*e.z+this.w*e.w}lengthSq(){return this.x*this.x+this.y*this.y+this.z*this.z+this.w*this.w}length(){return Math.sqrt(this.x*this.x+this.y*this.y+this.z*this.z+this.w*this.w)}manhattanLength(){return Math.abs(this.x)+Math.abs(this.y)+Math.abs(this.z)+Math.abs(this.w)}normalize(){return this.divideScalar(this.length()||1)}setLength(e){return this.normalize().multiplyScalar(e)}lerp(e,t){return this.x+=(e.x-this.x)*t,this.y+=(e.y-this.y)*t,this.z+=(e.z-this.z)*t,this.w+=(e.w-this.w)*t,this}lerpVectors(e,t,n){return this.x=e.x+(t.x-e.x)*n,this.y=e.y+(t.y-e.y)*n,this.z=e.z+(t.z-e.z)*n,this.w=e.w+(t.w-e.w)*n,this}equals(e){return e.x===this.x&&e.y===this.y&&e.z===this.z&&e.w===this.w}fromArray(e,t=0){return this.x=e[t],this.y=e[t+1],this.z=e[t+2],this.w=e[t+3],this}toArray(e=[],t=0){return e[t]=this.x,e[t+1]=this.y,e[t+2]=this.z,e[t+3]=this.w,e}fromBufferAttribute(e,t){return this.x=e.getX(t),this.y=e.getY(t),this.z=e.getZ(t),this.w=e.getW(t),this}random(){return this.x=Math.random(),this.y=Math.random(),this.z=Math.random(),this.w=Math.random(),this}*[Symbol.iterator](){yield this.x,yield this.y,yield this.z,yield this.w}}class yp extends ni{constructor(e=1,t=1,n={}){super(),this.isRenderTarget=!0,this.width=e,this.height=t,this.depth=1,this.scissor=new Ct(0,0,e,t),this.scissorTest=!1,this.viewport=new Ct(0,0,e,t);const i={width:e,height:t,depth:1};n=Object.assign({generateMipmaps:!1,internalFormat:null,minFilter:Xt,depthBuffer:!0,stencilBuffer:!1,resolveDepthBuffer:!0,resolveStencilBuffer:!0,depthTexture:null,samples:0,count:1},n);const r=new Vt(i,n.mapping,n.wrapS,n.wrapT,n.magFilter,n.minFilter,n.format,n.type,n.anisotropy,n.colorSpace);r.flipY=!1,r.generateMipmaps=n.generateMipmaps,r.internalFormat=n.internalFormat,this.textures=[];const o=n.count;for(let a=0;a<o;a++)this.textures[a]=r.clone(),this.textures[a].isRenderTargetTexture=!0;this.depthBuffer=n.depthBuffer,this.stencilBuffer=n.stencilBuffer,this.resolveDepthBuffer=n.resolveDepthBuffer,this.resolveStencilBuffer=n.resolveStencilBuffer,this.depthTexture=n.depthTexture,this.samples=n.samples}get texture(){return this.textures[0]}set texture(e){this.textures[0]=e}setSize(e,t,n=1){if(this.width!==e||this.height!==t||this.depth!==n){this.width=e,this.height=t,this.depth=n;for(let i=0,r=this.textures.length;i<r;i++)this.textures[i].image.width=e,this.textures[i].image.height=t,this.textures[i].image.depth=n;this.dispose()}this.viewport.set(0,0,e,t),this.scissor.set(0,0,e,t)}clone(){return new this.constructor().copy(this)}copy(e){this.width=e.width,this.height=e.height,this.depth=e.depth,this.scissor.copy(e.scissor),this.scissorTest=e.scissorTest,this.viewport.copy(e.viewport),this.textures.length=0;for(let n=0,i=e.textures.length;n<i;n++)this.textures[n]=e.textures[n].clone(),this.textures[n].isRenderTargetTexture=!0;const t=Object.assign({},e.texture.image);return this.texture.source=new _r(t),this.depthBuffer=e.depthBuffer,this.stencilBuffer=e.stencilBuffer,this.resolveDepthBuffer=e.resolveDepthBuffer,this.resolveStencilBuffer=e.resolveStencilBuffer,e.depthTexture!==null&&(this.depthTexture=e.depthTexture.clone()),this.samples=e.samples,this}dispose(){this.dispatchEvent({type:"dispose"})}}class qt extends yp{constructor(e=1,t=1,n={}){super(e,t,n),this.isWebGLRenderTarget=!0}}class ja extends Vt{constructor(e=null,t=1,n=1,i=1){super(null),this.isDataArrayTexture=!0,this.image={data:e,width:t,height:n,depth:i},this.magFilter=nn,this.minFilter=nn,this.wrapR=Bn,this.generateMipmaps=!1,this.flipY=!1,this.unpackAlignment=1}}class Ev extends qt{constructor(e=1,t=1,n=1,i={}){super(e,t,i),this.isWebGLArrayRenderTarget=!0,this.depth=n,this.texture=new ja(null,e,t,n),this.texture.isRenderTargetTexture=!0}}class Sh extends Vt{constructor(e=null,t=1,n=1,i=1){super(null),this.isData3DTexture=!0,this.image={data:e,width:t,height:n,depth:i},this.magFilter=nn,this.minFilter=nn,this.wrapR=Bn,this.generateMipmaps=!1,this.flipY=!1,this.unpackAlignment=1}}class Tv extends qt{constructor(e=1,t=1,n=1,i={}){super(e,t,i),this.isWebGL3DRenderTarget=!0,this.depth=n,this.texture=new Sh(null,e,t,n),this.texture.isRenderTargetTexture=!0}}class Dt{constructor(e=0,t=0,n=0,i=1){this.isQuaternion=!0,this._x=e,this._y=t,this._z=n,this._w=i}static slerpFlat(e,t,n,i,r,o,a){let l=n[i+0],u=n[i+1],d=n[i+2],m=n[i+3];const p=r[o+0],g=r[o+1],_=r[o+2],M=r[o+3];if(a===0){e[t+0]=l,e[t+1]=u,e[t+2]=d,e[t+3]=m;return}if(a===1){e[t+0]=p,e[t+1]=g,e[t+2]=_,e[t+3]=M;return}if(m!==M||l!==p||u!==g||d!==_){let y=1-a;const v=l*p+u*g+d*_+m*M,w=v>=0?1:-1,S=1-v*v;if(S>Number.EPSILON){const z=Math.sqrt(S),N=Math.atan2(z,v*w);y=Math.sin(y*N)/z,a=Math.sin(a*N)/z}const T=a*w;if(l=l*y+p*T,u=u*y+g*T,d=d*y+_*T,m=m*y+M*T,y===1-a){const z=1/Math.sqrt(l*l+u*u+d*d+m*m);l*=z,u*=z,d*=z,m*=z}}e[t]=l,e[t+1]=u,e[t+2]=d,e[t+3]=m}static multiplyQuaternionsFlat(e,t,n,i,r,o){const a=n[i],l=n[i+1],u=n[i+2],d=n[i+3],m=r[o],p=r[o+1],g=r[o+2],_=r[o+3];return e[t]=a*_+d*m+l*g-u*p,e[t+1]=l*_+d*p+u*m-a*g,e[t+2]=u*_+d*g+a*p-l*m,e[t+3]=d*_-a*m-l*p-u*g,e}get x(){return this._x}set x(e){this._x=e,this._onChangeCallback()}get y(){return this._y}set y(e){this._y=e,this._onChangeCallback()}get z(){return this._z}set z(e){this._z=e,this._onChangeCallback()}get w(){return this._w}set w(e){this._w=e,this._onChangeCallback()}set(e,t,n,i){return this._x=e,this._y=t,this._z=n,this._w=i,this._onChangeCallback(),this}clone(){return new this.constructor(this._x,this._y,this._z,this._w)}copy(e){return this._x=e.x,this._y=e.y,this._z=e.z,this._w=e.w,this._onChangeCallback(),this}setFromEuler(e,t=!0){const n=e._x,i=e._y,r=e._z,o=e._order,a=Math.cos,l=Math.sin,u=a(n/2),d=a(i/2),m=a(r/2),p=l(n/2),g=l(i/2),_=l(r/2);switch(o){case"XYZ":this._x=p*d*m+u*g*_,this._y=u*g*m-p*d*_,this._z=u*d*_+p*g*m,this._w=u*d*m-p*g*_;break;case"YXZ":this._x=p*d*m+u*g*_,this._y=u*g*m-p*d*_,this._z=u*d*_-p*g*m,this._w=u*d*m+p*g*_;break;case"ZXY":this._x=p*d*m-u*g*_,this._y=u*g*m+p*d*_,this._z=u*d*_+p*g*m,this._w=u*d*m-p*g*_;break;case"ZYX":this._x=p*d*m-u*g*_,this._y=u*g*m+p*d*_,this._z=u*d*_-p*g*m,this._w=u*d*m+p*g*_;break;case"YZX":this._x=p*d*m+u*g*_,this._y=u*g*m+p*d*_,this._z=u*d*_-p*g*m,this._w=u*d*m-p*g*_;break;case"XZY":this._x=p*d*m-u*g*_,this._y=u*g*m-p*d*_,this._z=u*d*_+p*g*m,this._w=u*d*m+p*g*_;break;default:console.warn("THREE.Quaternion: .setFromEuler() encountered an unknown order: "+o)}return t===!0&&this._onChangeCallback(),this}setFromAxisAngle(e,t){const n=t/2,i=Math.sin(n);return this._x=e.x*i,this._y=e.y*i,this._z=e.z*i,this._w=Math.cos(n),this._onChangeCallback(),this}setFromRotationMatrix(e){const t=e.elements,n=t[0],i=t[4],r=t[8],o=t[1],a=t[5],l=t[9],u=t[2],d=t[6],m=t[10],p=n+a+m;if(p>0){const g=.5/Math.sqrt(p+1);this._w=.25/g,this._x=(d-l)*g,this._y=(r-u)*g,this._z=(o-i)*g}else if(n>a&&n>m){const g=2*Math.sqrt(1+n-a-m);this._w=(d-l)/g,this._x=.25*g,this._y=(i+o)/g,this._z=(r+u)/g}else if(a>m){const g=2*Math.sqrt(1+a-n-m);this._w=(r-u)/g,this._x=(i+o)/g,this._y=.25*g,this._z=(l+d)/g}else{const g=2*Math.sqrt(1+m-n-a);this._w=(o-i)/g,this._x=(r+u)/g,this._y=(l+d)/g,this._z=.25*g}return this._onChangeCallback(),this}setFromUnitVectors(e,t){let n=e.dot(t)+1;return n<Number.EPSILON?(n=0,Math.abs(e.x)>Math.abs(e.z)?(this._x=-e.y,this._y=e.x,this._z=0,this._w=n):(this._x=0,this._y=-e.z,this._z=e.y,this._w=n)):(this._x=e.y*t.z-e.z*t.y,this._y=e.z*t.x-e.x*t.z,this._z=e.x*t.y-e.y*t.x,this._w=n),this.normalize()}angleTo(e){return 2*Math.acos(Math.abs(kt(this.dot(e),-1,1)))}rotateTowards(e,t){const n=this.angleTo(e);if(n===0)return this;const i=Math.min(1,t/n);return this.slerp(e,i),this}identity(){return this.set(0,0,0,1)}invert(){return this.conjugate()}conjugate(){return this._x*=-1,this._y*=-1,this._z*=-1,this._onChangeCallback(),this}dot(e){return this._x*e._x+this._y*e._y+this._z*e._z+this._w*e._w}lengthSq(){return this._x*this._x+this._y*this._y+this._z*this._z+this._w*this._w}length(){return Math.sqrt(this._x*this._x+this._y*this._y+this._z*this._z+this._w*this._w)}normalize(){let e=this.length();return e===0?(this._x=0,this._y=0,this._z=0,this._w=1):(e=1/e,this._x=this._x*e,this._y=this._y*e,this._z=this._z*e,this._w=this._w*e),this._onChangeCallback(),this}multiply(e){return this.multiplyQuaternions(this,e)}premultiply(e){return this.multiplyQuaternions(e,this)}multiplyQuaternions(e,t){const n=e._x,i=e._y,r=e._z,o=e._w,a=t._x,l=t._y,u=t._z,d=t._w;return this._x=n*d+o*a+i*u-r*l,this._y=i*d+o*l+r*a-n*u,this._z=r*d+o*u+n*l-i*a,this._w=o*d-n*a-i*l-r*u,this._onChangeCallback(),this}slerp(e,t){if(t===0)return this;if(t===1)return this.copy(e);const n=this._x,i=this._y,r=this._z,o=this._w;let a=o*e._w+n*e._x+i*e._y+r*e._z;if(a<0?(this._w=-e._w,this._x=-e._x,this._y=-e._y,this._z=-e._z,a=-a):this.copy(e),a>=1)return this._w=o,this._x=n,this._y=i,this._z=r,this;const l=1-a*a;if(l<=Number.EPSILON){const g=1-t;return this._w=g*o+t*this._w,this._x=g*n+t*this._x,this._y=g*i+t*this._y,this._z=g*r+t*this._z,this.normalize(),this}const u=Math.sqrt(l),d=Math.atan2(u,a),m=Math.sin((1-t)*d)/u,p=Math.sin(t*d)/u;return this._w=o*m+this._w*p,this._x=n*m+this._x*p,this._y=i*m+this._y*p,this._z=r*m+this._z*p,this._onChangeCallback(),this}slerpQuaternions(e,t,n){return this.copy(e).slerp(t,n)}random(){const e=2*Math.PI*Math.random(),t=2*Math.PI*Math.random(),n=Math.random(),i=Math.sqrt(1-n),r=Math.sqrt(n);return this.set(i*Math.sin(e),i*Math.cos(e),r*Math.sin(t),r*Math.cos(t))}equals(e){return e._x===this._x&&e._y===this._y&&e._z===this._z&&e._w===this._w}fromArray(e,t=0){return this._x=e[t],this._y=e[t+1],this._z=e[t+2],this._w=e[t+3],this._onChangeCallback(),this}toArray(e=[],t=0){return e[t]=this._x,e[t+1]=this._y,e[t+2]=this._z,e[t+3]=this._w,e}fromBufferAttribute(e,t){return this._x=e.getX(t),this._y=e.getY(t),this._z=e.getZ(t),this._w=e.getW(t),this._onChangeCallback(),this}toJSON(){return this.toArray()}_onChange(e){return this._onChangeCallback=e,this}_onChangeCallback(){}*[Symbol.iterator](){yield this._x,yield this._y,yield this._z,yield this._w}}class O{constructor(e=0,t=0,n=0){O.prototype.isVector3=!0,this.x=e,this.y=t,this.z=n}set(e,t,n){return n===void 0&&(n=this.z),this.x=e,this.y=t,this.z=n,this}setScalar(e){return this.x=e,this.y=e,this.z=e,this}setX(e){return this.x=e,this}setY(e){return this.y=e,this}setZ(e){return this.z=e,this}setComponent(e,t){switch(e){case 0:this.x=t;break;case 1:this.y=t;break;case 2:this.z=t;break;default:throw new Error("index is out of range: "+e)}return this}getComponent(e){switch(e){case 0:return this.x;case 1:return this.y;case 2:return this.z;default:throw new Error("index is out of range: "+e)}}clone(){return new this.constructor(this.x,this.y,this.z)}copy(e){return this.x=e.x,this.y=e.y,this.z=e.z,this}add(e){return this.x+=e.x,this.y+=e.y,this.z+=e.z,this}addScalar(e){return this.x+=e,this.y+=e,this.z+=e,this}addVectors(e,t){return this.x=e.x+t.x,this.y=e.y+t.y,this.z=e.z+t.z,this}addScaledVector(e,t){return this.x+=e.x*t,this.y+=e.y*t,this.z+=e.z*t,this}sub(e){return this.x-=e.x,this.y-=e.y,this.z-=e.z,this}subScalar(e){return this.x-=e,this.y-=e,this.z-=e,this}subVectors(e,t){return this.x=e.x-t.x,this.y=e.y-t.y,this.z=e.z-t.z,this}multiply(e){return this.x*=e.x,this.y*=e.y,this.z*=e.z,this}multiplyScalar(e){return this.x*=e,this.y*=e,this.z*=e,this}multiplyVectors(e,t){return this.x=e.x*t.x,this.y=e.y*t.y,this.z=e.z*t.z,this}applyEuler(e){return this.applyQuaternion(Tu.setFromEuler(e))}applyAxisAngle(e,t){return this.applyQuaternion(Tu.setFromAxisAngle(e,t))}applyMatrix3(e){const t=this.x,n=this.y,i=this.z,r=e.elements;return this.x=r[0]*t+r[3]*n+r[6]*i,this.y=r[1]*t+r[4]*n+r[7]*i,this.z=r[2]*t+r[5]*n+r[8]*i,this}applyNormalMatrix(e){return this.applyMatrix3(e).normalize()}applyMatrix4(e){const t=this.x,n=this.y,i=this.z,r=e.elements,o=1/(r[3]*t+r[7]*n+r[11]*i+r[15]);return this.x=(r[0]*t+r[4]*n+r[8]*i+r[12])*o,this.y=(r[1]*t+r[5]*n+r[9]*i+r[13])*o,this.z=(r[2]*t+r[6]*n+r[10]*i+r[14])*o,this}applyQuaternion(e){const t=this.x,n=this.y,i=this.z,r=e.x,o=e.y,a=e.z,l=e.w,u=2*(o*i-a*n),d=2*(a*t-r*i),m=2*(r*n-o*t);return this.x=t+l*u+o*m-a*d,this.y=n+l*d+a*u-r*m,this.z=i+l*m+r*d-o*u,this}project(e){return this.applyMatrix4(e.matrixWorldInverse).applyMatrix4(e.projectionMatrix)}unproject(e){return this.applyMatrix4(e.projectionMatrixInverse).applyMatrix4(e.matrixWorld)}transformDirection(e){const t=this.x,n=this.y,i=this.z,r=e.elements;return this.x=r[0]*t+r[4]*n+r[8]*i,this.y=r[1]*t+r[5]*n+r[9]*i,this.z=r[2]*t+r[6]*n+r[10]*i,this.normalize()}divide(e){return this.x/=e.x,this.y/=e.y,this.z/=e.z,this}divideScalar(e){return this.multiplyScalar(1/e)}min(e){return this.x=Math.min(this.x,e.x),this.y=Math.min(this.y,e.y),this.z=Math.min(this.z,e.z),this}max(e){return this.x=Math.max(this.x,e.x),this.y=Math.max(this.y,e.y),this.z=Math.max(this.z,e.z),this}clamp(e,t){return this.x=Math.max(e.x,Math.min(t.x,this.x)),this.y=Math.max(e.y,Math.min(t.y,this.y)),this.z=Math.max(e.z,Math.min(t.z,this.z)),this}clampScalar(e,t){return this.x=Math.max(e,Math.min(t,this.x)),this.y=Math.max(e,Math.min(t,this.y)),this.z=Math.max(e,Math.min(t,this.z)),this}clampLength(e,t){const n=this.length();return this.divideScalar(n||1).multiplyScalar(Math.max(e,Math.min(t,n)))}floor(){return this.x=Math.floor(this.x),this.y=Math.floor(this.y),this.z=Math.floor(this.z),this}ceil(){return this.x=Math.ceil(this.x),this.y=Math.ceil(this.y),this.z=Math.ceil(this.z),this}round(){return this.x=Math.round(this.x),this.y=Math.round(this.y),this.z=Math.round(this.z),this}roundToZero(){return this.x=Math.trunc(this.x),this.y=Math.trunc(this.y),this.z=Math.trunc(this.z),this}negate(){return this.x=-this.x,this.y=-this.y,this.z=-this.z,this}dot(e){return this.x*e.x+this.y*e.y+this.z*e.z}lengthSq(){return this.x*this.x+this.y*this.y+this.z*this.z}length(){return Math.sqrt(this.x*this.x+this.y*this.y+this.z*this.z)}manhattanLength(){return Math.abs(this.x)+Math.abs(this.y)+Math.abs(this.z)}normalize(){return this.divideScalar(this.length()||1)}setLength(e){return this.normalize().multiplyScalar(e)}lerp(e,t){return this.x+=(e.x-this.x)*t,this.y+=(e.y-this.y)*t,this.z+=(e.z-this.z)*t,this}lerpVectors(e,t,n){return this.x=e.x+(t.x-e.x)*n,this.y=e.y+(t.y-e.y)*n,this.z=e.z+(t.z-e.z)*n,this}cross(e){return this.crossVectors(this,e)}crossVectors(e,t){const n=e.x,i=e.y,r=e.z,o=t.x,a=t.y,l=t.z;return this.x=i*l-r*a,this.y=r*o-n*l,this.z=n*a-i*o,this}projectOnVector(e){const t=e.lengthSq();if(t===0)return this.set(0,0,0);const n=e.dot(this)/t;return this.copy(e).multiplyScalar(n)}projectOnPlane(e){return Il.copy(this).projectOnVector(e),this.sub(Il)}reflect(e){return this.sub(Il.copy(e).multiplyScalar(2*this.dot(e)))}angleTo(e){const t=Math.sqrt(this.lengthSq()*e.lengthSq());if(t===0)return Math.PI/2;const n=this.dot(e)/t;return Math.acos(kt(n,-1,1))}distanceTo(e){return Math.sqrt(this.distanceToSquared(e))}distanceToSquared(e){const t=this.x-e.x,n=this.y-e.y,i=this.z-e.z;return t*t+n*n+i*i}manhattanDistanceTo(e){return Math.abs(this.x-e.x)+Math.abs(this.y-e.y)+Math.abs(this.z-e.z)}setFromSpherical(e){return this.setFromSphericalCoords(e.radius,e.phi,e.theta)}setFromSphericalCoords(e,t,n){const i=Math.sin(t)*e;return this.x=i*Math.sin(n),this.y=Math.cos(t)*e,this.z=i*Math.cos(n),this}setFromCylindrical(e){return this.setFromCylindricalCoords(e.radius,e.theta,e.y)}setFromCylindricalCoords(e,t,n){return this.x=e*Math.sin(t),this.y=n,this.z=e*Math.cos(t),this}setFromMatrixPosition(e){const t=e.elements;return this.x=t[12],this.y=t[13],this.z=t[14],this}setFromMatrixScale(e){const t=this.setFromMatrixColumn(e,0).length(),n=this.setFromMatrixColumn(e,1).length(),i=this.setFromMatrixColumn(e,2).length();return this.x=t,this.y=n,this.z=i,this}setFromMatrixColumn(e,t){return this.fromArray(e.elements,t*4)}setFromMatrix3Column(e,t){return this.fromArray(e.elements,t*3)}setFromEuler(e){return this.x=e._x,this.y=e._y,this.z=e._z,this}setFromColor(e){return this.x=e.r,this.y=e.g,this.z=e.b,this}equals(e){return e.x===this.x&&e.y===this.y&&e.z===this.z}fromArray(e,t=0){return this.x=e[t],this.y=e[t+1],this.z=e[t+2],this}toArray(e=[],t=0){return e[t]=this.x,e[t+1]=this.y,e[t+2]=this.z,e}fromBufferAttribute(e,t){return this.x=e.getX(t),this.y=e.getY(t),this.z=e.getZ(t),this}random(){return this.x=Math.random(),this.y=Math.random(),this.z=Math.random(),this}randomDirection(){const e=Math.random()*Math.PI*2,t=Math.random()*2-1,n=Math.sqrt(1-t*t);return this.x=n*Math.cos(e),this.y=t,this.z=n*Math.sin(e),this}*[Symbol.iterator](){yield this.x,yield this.y,yield this.z}}const Il=new O,Tu=new Dt;class yn{constructor(e=new O(1/0,1/0,1/0),t=new O(-1/0,-1/0,-1/0)){this.isBox3=!0,this.min=e,this.max=t}set(e,t){return this.min.copy(e),this.max.copy(t),this}setFromArray(e){this.makeEmpty();for(let t=0,n=e.length;t<n;t+=3)this.expandByPoint(Hn.fromArray(e,t));return this}setFromBufferAttribute(e){this.makeEmpty();for(let t=0,n=e.count;t<n;t++)this.expandByPoint(Hn.fromBufferAttribute(e,t));return this}setFromPoints(e){this.makeEmpty();for(let t=0,n=e.length;t<n;t++)this.expandByPoint(e[t]);return this}setFromCenterAndSize(e,t){const n=Hn.copy(t).multiplyScalar(.5);return this.min.copy(e).sub(n),this.max.copy(e).add(n),this}setFromObject(e,t=!1){return this.makeEmpty(),this.expandByObject(e,t)}clone(){return new this.constructor().copy(this)}copy(e){return this.min.copy(e.min),this.max.copy(e.max),this}makeEmpty(){return this.min.x=this.min.y=this.min.z=1/0,this.max.x=this.max.y=this.max.z=-1/0,this}isEmpty(){return this.max.x<this.min.x||this.max.y<this.min.y||this.max.z<this.min.z}getCenter(e){return this.isEmpty()?e.set(0,0,0):e.addVectors(this.min,this.max).multiplyScalar(.5)}getSize(e){return this.isEmpty()?e.set(0,0,0):e.subVectors(this.max,this.min)}expandByPoint(e){return this.min.min(e),this.max.max(e),this}expandByVector(e){return this.min.sub(e),this.max.add(e),this}expandByScalar(e){return this.min.addScalar(-e),this.max.addScalar(e),this}expandByObject(e,t=!1){e.updateWorldMatrix(!1,!1);const n=e.geometry;if(n!==void 0){const r=n.getAttribute("position");if(t===!0&&r!==void 0&&e.isInstancedMesh!==!0)for(let o=0,a=r.count;o<a;o++)e.isMesh===!0?e.getVertexPosition(o,Hn):Hn.fromBufferAttribute(r,o),Hn.applyMatrix4(e.matrixWorld),this.expandByPoint(Hn);else e.boundingBox!==void 0?(e.boundingBox===null&&e.computeBoundingBox(),Oo.copy(e.boundingBox)):(n.boundingBox===null&&n.computeBoundingBox(),Oo.copy(n.boundingBox)),Oo.applyMatrix4(e.matrixWorld),this.union(Oo)}const i=e.children;for(let r=0,o=i.length;r<o;r++)this.expandByObject(i[r],t);return this}containsPoint(e){return!(e.x<this.min.x||e.x>this.max.x||e.y<this.min.y||e.y>this.max.y||e.z<this.min.z||e.z>this.max.z)}containsBox(e){return this.min.x<=e.min.x&&e.max.x<=this.max.x&&this.min.y<=e.min.y&&e.max.y<=this.max.y&&this.min.z<=e.min.z&&e.max.z<=this.max.z}getParameter(e,t){return t.set((e.x-this.min.x)/(this.max.x-this.min.x),(e.y-this.min.y)/(this.max.y-this.min.y),(e.z-this.min.z)/(this.max.z-this.min.z))}intersectsBox(e){return!(e.max.x<this.min.x||e.min.x>this.max.x||e.max.y<this.min.y||e.min.y>this.max.y||e.max.z<this.min.z||e.min.z>this.max.z)}intersectsSphere(e){return this.clampPoint(e.center,Hn),Hn.distanceToSquared(e.center)<=e.radius*e.radius}intersectsPlane(e){let t,n;return e.normal.x>0?(t=e.normal.x*this.min.x,n=e.normal.x*this.max.x):(t=e.normal.x*this.max.x,n=e.normal.x*this.min.x),e.normal.y>0?(t+=e.normal.y*this.min.y,n+=e.normal.y*this.max.y):(t+=e.normal.y*this.max.y,n+=e.normal.y*this.min.y),e.normal.z>0?(t+=e.normal.z*this.min.z,n+=e.normal.z*this.max.z):(t+=e.normal.z*this.max.z,n+=e.normal.z*this.min.z),t<=-e.constant&&n>=-e.constant}intersectsTriangle(e){if(this.isEmpty())return!1;this.getCenter(Es),Uo.subVectors(this.max,Es),Nr.subVectors(e.a,Es),Or.subVectors(e.b,Es),Ur.subVectors(e.c,Es),Pi.subVectors(Or,Nr),Ii.subVectors(Ur,Or),Ji.subVectors(Nr,Ur);let t=[0,-Pi.z,Pi.y,0,-Ii.z,Ii.y,0,-Ji.z,Ji.y,Pi.z,0,-Pi.x,Ii.z,0,-Ii.x,Ji.z,0,-Ji.x,-Pi.y,Pi.x,0,-Ii.y,Ii.x,0,-Ji.y,Ji.x,0];return!Ll(t,Nr,Or,Ur,Uo)||(t=[1,0,0,0,1,0,0,0,1],!Ll(t,Nr,Or,Ur,Uo))?!1:(Fo.crossVectors(Pi,Ii),t=[Fo.x,Fo.y,Fo.z],Ll(t,Nr,Or,Ur,Uo))}clampPoint(e,t){return t.copy(e).clamp(this.min,this.max)}distanceToPoint(e){return this.clampPoint(e,Hn).distanceTo(e)}getBoundingSphere(e){return this.isEmpty()?e.makeEmpty():(this.getCenter(e.center),e.radius=this.getSize(Hn).length()*.5),e}intersect(e){return this.min.max(e.min),this.max.min(e.max),this.isEmpty()&&this.makeEmpty(),this}union(e){return this.min.min(e.min),this.max.max(e.max),this}applyMatrix4(e){return this.isEmpty()?this:(li[0].set(this.min.x,this.min.y,this.min.z).applyMatrix4(e),li[1].set(this.min.x,this.min.y,this.max.z).applyMatrix4(e),li[2].set(this.min.x,this.max.y,this.min.z).applyMatrix4(e),li[3].set(this.min.x,this.max.y,this.max.z).applyMatrix4(e),li[4].set(this.max.x,this.min.y,this.min.z).applyMatrix4(e),li[5].set(this.max.x,this.min.y,this.max.z).applyMatrix4(e),li[6].set(this.max.x,this.max.y,this.min.z).applyMatrix4(e),li[7].set(this.max.x,this.max.y,this.max.z).applyMatrix4(e),this.setFromPoints(li),this)}translate(e){return this.min.add(e),this.max.add(e),this}equals(e){return e.min.equals(this.min)&&e.max.equals(this.max)}}const li=[new O,new O,new O,new O,new O,new O,new O,new O],Hn=new O,Oo=new yn,Nr=new O,Or=new O,Ur=new O,Pi=new O,Ii=new O,Ji=new O,Es=new O,Uo=new O,Fo=new O,$i=new O;function Ll(s,e,t,n,i){for(let r=0,o=s.length-3;r<=o;r+=3){$i.fromArray(s,r);const a=i.x*Math.abs($i.x)+i.y*Math.abs($i.y)+i.z*Math.abs($i.z),l=e.dot($i),u=t.dot($i),d=n.dot($i);if(Math.max(-Math.max(l,u,d),Math.min(l,u,d))>a)return!1}return!0}const Av=new yn,Ts=new O,Dl=new O;class cn{constructor(e=new O,t=-1){this.isSphere=!0,this.center=e,this.radius=t}set(e,t){return this.center.copy(e),this.radius=t,this}setFromPoints(e,t){const n=this.center;t!==void 0?n.copy(t):Av.setFromPoints(e).getCenter(n);let i=0;for(let r=0,o=e.length;r<o;r++)i=Math.max(i,n.distanceToSquared(e[r]));return this.radius=Math.sqrt(i),this}copy(e){return this.center.copy(e.center),this.radius=e.radius,this}isEmpty(){return this.radius<0}makeEmpty(){return this.center.set(0,0,0),this.radius=-1,this}containsPoint(e){return e.distanceToSquared(this.center)<=this.radius*this.radius}distanceToPoint(e){return e.distanceTo(this.center)-this.radius}intersectsSphere(e){const t=this.radius+e.radius;return e.center.distanceToSquared(this.center)<=t*t}intersectsBox(e){return e.intersectsSphere(this)}intersectsPlane(e){return Math.abs(e.distanceToPoint(this.center))<=this.radius}clampPoint(e,t){const n=this.center.distanceToSquared(e);return t.copy(e),n>this.radius*this.radius&&(t.sub(this.center).normalize(),t.multiplyScalar(this.radius).add(this.center)),t}getBoundingBox(e){return this.isEmpty()?(e.makeEmpty(),e):(e.set(this.center,this.center),e.expandByScalar(this.radius),e)}applyMatrix4(e){return this.center.applyMatrix4(e),this.radius=this.radius*e.getMaxScaleOnAxis(),this}translate(e){return this.center.add(e),this}expandByPoint(e){if(this.isEmpty())return this.center.copy(e),this.radius=0,this;Ts.subVectors(e,this.center);const t=Ts.lengthSq();if(t>this.radius*this.radius){const n=Math.sqrt(t),i=(n-this.radius)*.5;this.center.addScaledVector(Ts,i/n),this.radius+=i}return this}union(e){return e.isEmpty()?this:this.isEmpty()?(this.copy(e),this):(this.center.equals(e.center)===!0?this.radius=Math.max(this.radius,e.radius):(Dl.subVectors(e.center,this.center).setLength(e.radius),this.expandByPoint(Ts.copy(e.center).add(Dl)),this.expandByPoint(Ts.copy(e.center).sub(Dl))),this)}equals(e){return e.center.equals(this.center)&&e.radius===this.radius}clone(){return new this.constructor().copy(this)}}const ci=new O,Nl=new O,Bo=new O,Li=new O,Ol=new O,zo=new O,Ul=new O;class Ar{constructor(e=new O,t=new O(0,0,-1)){this.origin=e,this.direction=t}set(e,t){return this.origin.copy(e),this.direction.copy(t),this}copy(e){return this.origin.copy(e.origin),this.direction.copy(e.direction),this}at(e,t){return t.copy(this.origin).addScaledVector(this.direction,e)}lookAt(e){return this.direction.copy(e).sub(this.origin).normalize(),this}recast(e){return this.origin.copy(this.at(e,ci)),this}closestPointToPoint(e,t){t.subVectors(e,this.origin);const n=t.dot(this.direction);return n<0?t.copy(this.origin):t.copy(this.origin).addScaledVector(this.direction,n)}distanceToPoint(e){return Math.sqrt(this.distanceSqToPoint(e))}distanceSqToPoint(e){const t=ci.subVectors(e,this.origin).dot(this.direction);return t<0?this.origin.distanceToSquared(e):(ci.copy(this.origin).addScaledVector(this.direction,t),ci.distanceToSquared(e))}distanceSqToSegment(e,t,n,i){Nl.copy(e).add(t).multiplyScalar(.5),Bo.copy(t).sub(e).normalize(),Li.copy(this.origin).sub(Nl);const r=e.distanceTo(t)*.5,o=-this.direction.dot(Bo),a=Li.dot(this.direction),l=-Li.dot(Bo),u=Li.lengthSq(),d=Math.abs(1-o*o);let m,p,g,_;if(d>0)if(m=o*l-a,p=o*a-l,_=r*d,m>=0)if(p>=-_)if(p<=_){const M=1/d;m*=M,p*=M,g=m*(m+o*p+2*a)+p*(o*m+p+2*l)+u}else p=r,m=Math.max(0,-(o*p+a)),g=-m*m+p*(p+2*l)+u;else p=-r,m=Math.max(0,-(o*p+a)),g=-m*m+p*(p+2*l)+u;else p<=-_?(m=Math.max(0,-(-o*r+a)),p=m>0?-r:Math.min(Math.max(-r,-l),r),g=-m*m+p*(p+2*l)+u):p<=_?(m=0,p=Math.min(Math.max(-r,-l),r),g=p*(p+2*l)+u):(m=Math.max(0,-(o*r+a)),p=m>0?r:Math.min(Math.max(-r,-l),r),g=-m*m+p*(p+2*l)+u);else p=o>0?-r:r,m=Math.max(0,-(o*p+a)),g=-m*m+p*(p+2*l)+u;return n&&n.copy(this.origin).addScaledVector(this.direction,m),i&&i.copy(Nl).addScaledVector(Bo,p),g}intersectSphere(e,t){ci.subVectors(e.center,this.origin);const n=ci.dot(this.direction),i=ci.dot(ci)-n*n,r=e.radius*e.radius;if(i>r)return null;const o=Math.sqrt(r-i),a=n-o,l=n+o;return l<0?null:a<0?this.at(l,t):this.at(a,t)}intersectsSphere(e){return this.distanceSqToPoint(e.center)<=e.radius*e.radius}distanceToPlane(e){const t=e.normal.dot(this.direction);if(t===0)return e.distanceToPoint(this.origin)===0?0:null;const n=-(this.origin.dot(e.normal)+e.constant)/t;return n>=0?n:null}intersectPlane(e,t){const n=this.distanceToPlane(e);return n===null?null:this.at(n,t)}intersectsPlane(e){const t=e.distanceToPoint(this.origin);return t===0||e.normal.dot(this.direction)*t<0}intersectBox(e,t){let n,i,r,o,a,l;const u=1/this.direction.x,d=1/this.direction.y,m=1/this.direction.z,p=this.origin;return u>=0?(n=(e.min.x-p.x)*u,i=(e.max.x-p.x)*u):(n=(e.max.x-p.x)*u,i=(e.min.x-p.x)*u),d>=0?(r=(e.min.y-p.y)*d,o=(e.max.y-p.y)*d):(r=(e.max.y-p.y)*d,o=(e.min.y-p.y)*d),n>o||r>i||((r>n||isNaN(n))&&(n=r),(o<i||isNaN(i))&&(i=o),m>=0?(a=(e.min.z-p.z)*m,l=(e.max.z-p.z)*m):(a=(e.max.z-p.z)*m,l=(e.min.z-p.z)*m),n>l||a>i)||((a>n||n!==n)&&(n=a),(l<i||i!==i)&&(i=l),i<0)?null:this.at(n>=0?n:i,t)}intersectsBox(e){return this.intersectBox(e,ci)!==null}intersectTriangle(e,t,n,i,r){Ol.subVectors(t,e),zo.subVectors(n,e),Ul.crossVectors(Ol,zo);let o=this.direction.dot(Ul),a;if(o>0){if(i)return null;a=1}else if(o<0)a=-1,o=-o;else return null;Li.subVectors(this.origin,e);const l=a*this.direction.dot(zo.crossVectors(Li,zo));if(l<0)return null;const u=a*this.direction.dot(Ol.cross(Li));if(u<0||l+u>o)return null;const d=-a*Li.dot(Ul);return d<0?null:this.at(d/o,r)}applyMatrix4(e){return this.origin.applyMatrix4(e),this.direction.transformDirection(e),this}equals(e){return e.origin.equals(this.origin)&&e.direction.equals(this.direction)}clone(){return new this.constructor().copy(this)}}class Ke{constructor(e,t,n,i,r,o,a,l,u,d,m,p,g,_,M,y){Ke.prototype.isMatrix4=!0,this.elements=[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1],e!==void 0&&this.set(e,t,n,i,r,o,a,l,u,d,m,p,g,_,M,y)}set(e,t,n,i,r,o,a,l,u,d,m,p,g,_,M,y){const v=this.elements;return v[0]=e,v[4]=t,v[8]=n,v[12]=i,v[1]=r,v[5]=o,v[9]=a,v[13]=l,v[2]=u,v[6]=d,v[10]=m,v[14]=p,v[3]=g,v[7]=_,v[11]=M,v[15]=y,this}identity(){return this.set(1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1),this}clone(){return new Ke().fromArray(this.elements)}copy(e){const t=this.elements,n=e.elements;return t[0]=n[0],t[1]=n[1],t[2]=n[2],t[3]=n[3],t[4]=n[4],t[5]=n[5],t[6]=n[6],t[7]=n[7],t[8]=n[8],t[9]=n[9],t[10]=n[10],t[11]=n[11],t[12]=n[12],t[13]=n[13],t[14]=n[14],t[15]=n[15],this}copyPosition(e){const t=this.elements,n=e.elements;return t[12]=n[12],t[13]=n[13],t[14]=n[14],this}setFromMatrix3(e){const t=e.elements;return this.set(t[0],t[3],t[6],0,t[1],t[4],t[7],0,t[2],t[5],t[8],0,0,0,0,1),this}extractBasis(e,t,n){return e.setFromMatrixColumn(this,0),t.setFromMatrixColumn(this,1),n.setFromMatrixColumn(this,2),this}makeBasis(e,t,n){return this.set(e.x,t.x,n.x,0,e.y,t.y,n.y,0,e.z,t.z,n.z,0,0,0,0,1),this}extractRotation(e){const t=this.elements,n=e.elements,i=1/Fr.setFromMatrixColumn(e,0).length(),r=1/Fr.setFromMatrixColumn(e,1).length(),o=1/Fr.setFromMatrixColumn(e,2).length();return t[0]=n[0]*i,t[1]=n[1]*i,t[2]=n[2]*i,t[3]=0,t[4]=n[4]*r,t[5]=n[5]*r,t[6]=n[6]*r,t[7]=0,t[8]=n[8]*o,t[9]=n[9]*o,t[10]=n[10]*o,t[11]=0,t[12]=0,t[13]=0,t[14]=0,t[15]=1,this}makeRotationFromEuler(e){const t=this.elements,n=e.x,i=e.y,r=e.z,o=Math.cos(n),a=Math.sin(n),l=Math.cos(i),u=Math.sin(i),d=Math.cos(r),m=Math.sin(r);if(e.order==="XYZ"){const p=o*d,g=o*m,_=a*d,M=a*m;t[0]=l*d,t[4]=-l*m,t[8]=u,t[1]=g+_*u,t[5]=p-M*u,t[9]=-a*l,t[2]=M-p*u,t[6]=_+g*u,t[10]=o*l}else if(e.order==="YXZ"){const p=l*d,g=l*m,_=u*d,M=u*m;t[0]=p+M*a,t[4]=_*a-g,t[8]=o*u,t[1]=o*m,t[5]=o*d,t[9]=-a,t[2]=g*a-_,t[6]=M+p*a,t[10]=o*l}else if(e.order==="ZXY"){const p=l*d,g=l*m,_=u*d,M=u*m;t[0]=p-M*a,t[4]=-o*m,t[8]=_+g*a,t[1]=g+_*a,t[5]=o*d,t[9]=M-p*a,t[2]=-o*u,t[6]=a,t[10]=o*l}else if(e.order==="ZYX"){const p=o*d,g=o*m,_=a*d,M=a*m;t[0]=l*d,t[4]=_*u-g,t[8]=p*u+M,t[1]=l*m,t[5]=M*u+p,t[9]=g*u-_,t[2]=-u,t[6]=a*l,t[10]=o*l}else if(e.order==="YZX"){const p=o*l,g=o*u,_=a*l,M=a*u;t[0]=l*d,t[4]=M-p*m,t[8]=_*m+g,t[1]=m,t[5]=o*d,t[9]=-a*d,t[2]=-u*d,t[6]=g*m+_,t[10]=p-M*m}else if(e.order==="XZY"){const p=o*l,g=o*u,_=a*l,M=a*u;t[0]=l*d,t[4]=-m,t[8]=u*d,t[1]=p*m+M,t[5]=o*d,t[9]=g*m-_,t[2]=_*m-g,t[6]=a*d,t[10]=M*m+p}return t[3]=0,t[7]=0,t[11]=0,t[12]=0,t[13]=0,t[14]=0,t[15]=1,this}makeRotationFromQuaternion(e){return this.compose(Cv,e,Rv)}lookAt(e,t,n){const i=this.elements;return wn.subVectors(e,t),wn.lengthSq()===0&&(wn.z=1),wn.normalize(),Di.crossVectors(n,wn),Di.lengthSq()===0&&(Math.abs(n.z)===1?wn.x+=1e-4:wn.z+=1e-4,wn.normalize(),Di.crossVectors(n,wn)),Di.normalize(),ko.crossVectors(wn,Di),i[0]=Di.x,i[4]=ko.x,i[8]=wn.x,i[1]=Di.y,i[5]=ko.y,i[9]=wn.y,i[2]=Di.z,i[6]=ko.z,i[10]=wn.z,this}multiply(e){return this.multiplyMatrices(this,e)}premultiply(e){return this.multiplyMatrices(e,this)}multiplyMatrices(e,t){const n=e.elements,i=t.elements,r=this.elements,o=n[0],a=n[4],l=n[8],u=n[12],d=n[1],m=n[5],p=n[9],g=n[13],_=n[2],M=n[6],y=n[10],v=n[14],w=n[3],S=n[7],T=n[11],z=n[15],N=i[0],A=i[4],k=i[8],D=i[12],C=i[1],X=i[5],Y=i[9],H=i[13],K=i[2],J=i[6],ce=i[10],de=i[14],Q=i[3],pe=i[7],ve=i[11],Ue=i[15];return r[0]=o*N+a*C+l*K+u*Q,r[4]=o*A+a*X+l*J+u*pe,r[8]=o*k+a*Y+l*ce+u*ve,r[12]=o*D+a*H+l*de+u*Ue,r[1]=d*N+m*C+p*K+g*Q,r[5]=d*A+m*X+p*J+g*pe,r[9]=d*k+m*Y+p*ce+g*ve,r[13]=d*D+m*H+p*de+g*Ue,r[2]=_*N+M*C+y*K+v*Q,r[6]=_*A+M*X+y*J+v*pe,r[10]=_*k+M*Y+y*ce+v*ve,r[14]=_*D+M*H+y*de+v*Ue,r[3]=w*N+S*C+T*K+z*Q,r[7]=w*A+S*X+T*J+z*pe,r[11]=w*k+S*Y+T*ce+z*ve,r[15]=w*D+S*H+T*de+z*Ue,this}multiplyScalar(e){const t=this.elements;return t[0]*=e,t[4]*=e,t[8]*=e,t[12]*=e,t[1]*=e,t[5]*=e,t[9]*=e,t[13]*=e,t[2]*=e,t[6]*=e,t[10]*=e,t[14]*=e,t[3]*=e,t[7]*=e,t[11]*=e,t[15]*=e,this}determinant(){const e=this.elements,t=e[0],n=e[4],i=e[8],r=e[12],o=e[1],a=e[5],l=e[9],u=e[13],d=e[2],m=e[6],p=e[10],g=e[14],_=e[3],M=e[7],y=e[11],v=e[15];return _*(+r*l*m-i*u*m-r*a*p+n*u*p+i*a*g-n*l*g)+M*(+t*l*g-t*u*p+r*o*p-i*o*g+i*u*d-r*l*d)+y*(+t*u*m-t*a*g-r*o*m+n*o*g+r*a*d-n*u*d)+v*(-i*a*d-t*l*m+t*a*p+i*o*m-n*o*p+n*l*d)}transpose(){const e=this.elements;let t;return t=e[1],e[1]=e[4],e[4]=t,t=e[2],e[2]=e[8],e[8]=t,t=e[6],e[6]=e[9],e[9]=t,t=e[3],e[3]=e[12],e[12]=t,t=e[7],e[7]=e[13],e[13]=t,t=e[11],e[11]=e[14],e[14]=t,this}setPosition(e,t,n){const i=this.elements;return e.isVector3?(i[12]=e.x,i[13]=e.y,i[14]=e.z):(i[12]=e,i[13]=t,i[14]=n),this}invert(){const e=this.elements,t=e[0],n=e[1],i=e[2],r=e[3],o=e[4],a=e[5],l=e[6],u=e[7],d=e[8],m=e[9],p=e[10],g=e[11],_=e[12],M=e[13],y=e[14],v=e[15],w=m*y*u-M*p*u+M*l*g-a*y*g-m*l*v+a*p*v,S=_*p*u-d*y*u-_*l*g+o*y*g+d*l*v-o*p*v,T=d*M*u-_*m*u+_*a*g-o*M*g-d*a*v+o*m*v,z=_*m*l-d*M*l-_*a*p+o*M*p+d*a*y-o*m*y,N=t*w+n*S+i*T+r*z;if(N===0)return this.set(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);const A=1/N;return e[0]=w*A,e[1]=(M*p*r-m*y*r-M*i*g+n*y*g+m*i*v-n*p*v)*A,e[2]=(a*y*r-M*l*r+M*i*u-n*y*u-a*i*v+n*l*v)*A,e[3]=(m*l*r-a*p*r-m*i*u+n*p*u+a*i*g-n*l*g)*A,e[4]=S*A,e[5]=(d*y*r-_*p*r+_*i*g-t*y*g-d*i*v+t*p*v)*A,e[6]=(_*l*r-o*y*r-_*i*u+t*y*u+o*i*v-t*l*v)*A,e[7]=(o*p*r-d*l*r+d*i*u-t*p*u-o*i*g+t*l*g)*A,e[8]=T*A,e[9]=(_*m*r-d*M*r-_*n*g+t*M*g+d*n*v-t*m*v)*A,e[10]=(o*M*r-_*a*r+_*n*u-t*M*u-o*n*v+t*a*v)*A,e[11]=(d*a*r-o*m*r-d*n*u+t*m*u+o*n*g-t*a*g)*A,e[12]=z*A,e[13]=(d*M*i-_*m*i+_*n*p-t*M*p-d*n*y+t*m*y)*A,e[14]=(_*a*i-o*M*i-_*n*l+t*M*l+o*n*y-t*a*y)*A,e[15]=(o*m*i-d*a*i+d*n*l-t*m*l-o*n*p+t*a*p)*A,this}scale(e){const t=this.elements,n=e.x,i=e.y,r=e.z;return t[0]*=n,t[4]*=i,t[8]*=r,t[1]*=n,t[5]*=i,t[9]*=r,t[2]*=n,t[6]*=i,t[10]*=r,t[3]*=n,t[7]*=i,t[11]*=r,this}getMaxScaleOnAxis(){const e=this.elements,t=e[0]*e[0]+e[1]*e[1]+e[2]*e[2],n=e[4]*e[4]+e[5]*e[5]+e[6]*e[6],i=e[8]*e[8]+e[9]*e[9]+e[10]*e[10];return Math.sqrt(Math.max(t,n,i))}makeTranslation(e,t,n){return e.isVector3?this.set(1,0,0,e.x,0,1,0,e.y,0,0,1,e.z,0,0,0,1):this.set(1,0,0,e,0,1,0,t,0,0,1,n,0,0,0,1),this}makeRotationX(e){const t=Math.cos(e),n=Math.sin(e);return this.set(1,0,0,0,0,t,-n,0,0,n,t,0,0,0,0,1),this}makeRotationY(e){const t=Math.cos(e),n=Math.sin(e);return this.set(t,0,n,0,0,1,0,0,-n,0,t,0,0,0,0,1),this}makeRotationZ(e){const t=Math.cos(e),n=Math.sin(e);return this.set(t,-n,0,0,n,t,0,0,0,0,1,0,0,0,0,1),this}makeRotationAxis(e,t){const n=Math.cos(t),i=Math.sin(t),r=1-n,o=e.x,a=e.y,l=e.z,u=r*o,d=r*a;return this.set(u*o+n,u*a-i*l,u*l+i*a,0,u*a+i*l,d*a+n,d*l-i*o,0,u*l-i*a,d*l+i*o,r*l*l+n,0,0,0,0,1),this}makeScale(e,t,n){return this.set(e,0,0,0,0,t,0,0,0,0,n,0,0,0,0,1),this}makeShear(e,t,n,i,r,o){return this.set(1,n,r,0,e,1,o,0,t,i,1,0,0,0,0,1),this}compose(e,t,n){const i=this.elements,r=t._x,o=t._y,a=t._z,l=t._w,u=r+r,d=o+o,m=a+a,p=r*u,g=r*d,_=r*m,M=o*d,y=o*m,v=a*m,w=l*u,S=l*d,T=l*m,z=n.x,N=n.y,A=n.z;return i[0]=(1-(M+v))*z,i[1]=(g+T)*z,i[2]=(_-S)*z,i[3]=0,i[4]=(g-T)*N,i[5]=(1-(p+v))*N,i[6]=(y+w)*N,i[7]=0,i[8]=(_+S)*A,i[9]=(y-w)*A,i[10]=(1-(p+M))*A,i[11]=0,i[12]=e.x,i[13]=e.y,i[14]=e.z,i[15]=1,this}decompose(e,t,n){const i=this.elements;let r=Fr.set(i[0],i[1],i[2]).length();const o=Fr.set(i[4],i[5],i[6]).length(),a=Fr.set(i[8],i[9],i[10]).length();this.determinant()<0&&(r=-r),e.x=i[12],e.y=i[13],e.z=i[14],Gn.copy(this);const u=1/r,d=1/o,m=1/a;return Gn.elements[0]*=u,Gn.elements[1]*=u,Gn.elements[2]*=u,Gn.elements[4]*=d,Gn.elements[5]*=d,Gn.elements[6]*=d,Gn.elements[8]*=m,Gn.elements[9]*=m,Gn.elements[10]*=m,t.setFromRotationMatrix(Gn),n.x=r,n.y=o,n.z=a,this}makePerspective(e,t,n,i,r,o,a=ei){const l=this.elements,u=2*r/(t-e),d=2*r/(n-i),m=(t+e)/(t-e),p=(n+i)/(n-i);let g,_;if(a===ei)g=-(o+r)/(o-r),_=-2*o*r/(o-r);else if(a===oo)g=-o/(o-r),_=-o*r/(o-r);else throw new Error("THREE.Matrix4.makePerspective(): Invalid coordinate system: "+a);return l[0]=u,l[4]=0,l[8]=m,l[12]=0,l[1]=0,l[5]=d,l[9]=p,l[13]=0,l[2]=0,l[6]=0,l[10]=g,l[14]=_,l[3]=0,l[7]=0,l[11]=-1,l[15]=0,this}makeOrthographic(e,t,n,i,r,o,a=ei){const l=this.elements,u=1/(t-e),d=1/(n-i),m=1/(o-r),p=(t+e)*u,g=(n+i)*d;let _,M;if(a===ei)_=(o+r)*m,M=-2*m;else if(a===oo)_=r*m,M=-1*m;else throw new Error("THREE.Matrix4.makeOrthographic(): Invalid coordinate system: "+a);return l[0]=2*u,l[4]=0,l[8]=0,l[12]=-p,l[1]=0,l[5]=2*d,l[9]=0,l[13]=-g,l[2]=0,l[6]=0,l[10]=M,l[14]=-_,l[3]=0,l[7]=0,l[11]=0,l[15]=1,this}equals(e){const t=this.elements,n=e.elements;for(let i=0;i<16;i++)if(t[i]!==n[i])return!1;return!0}fromArray(e,t=0){for(let n=0;n<16;n++)this.elements[n]=e[n+t];return this}toArray(e=[],t=0){const n=this.elements;return e[t]=n[0],e[t+1]=n[1],e[t+2]=n[2],e[t+3]=n[3],e[t+4]=n[4],e[t+5]=n[5],e[t+6]=n[6],e[t+7]=n[7],e[t+8]=n[8],e[t+9]=n[9],e[t+10]=n[10],e[t+11]=n[11],e[t+12]=n[12],e[t+13]=n[13],e[t+14]=n[14],e[t+15]=n[15],e}}const Fr=new O,Gn=new Ke,Cv=new O(0,0,0),Rv=new O(1,1,1),Di=new O,ko=new O,wn=new O,Au=new Ke,Cu=new Dt;class xn{constructor(e=0,t=0,n=0,i=xn.DEFAULT_ORDER){this.isEuler=!0,this._x=e,this._y=t,this._z=n,this._order=i}get x(){return this._x}set x(e){this._x=e,this._onChangeCallback()}get y(){return this._y}set y(e){this._y=e,this._onChangeCallback()}get z(){return this._z}set z(e){this._z=e,this._onChangeCallback()}get order(){return this._order}set order(e){this._order=e,this._onChangeCallback()}set(e,t,n,i=this._order){return this._x=e,this._y=t,this._z=n,this._order=i,this._onChangeCallback(),this}clone(){return new this.constructor(this._x,this._y,this._z,this._order)}copy(e){return this._x=e._x,this._y=e._y,this._z=e._z,this._order=e._order,this._onChangeCallback(),this}setFromRotationMatrix(e,t=this._order,n=!0){const i=e.elements,r=i[0],o=i[4],a=i[8],l=i[1],u=i[5],d=i[9],m=i[2],p=i[6],g=i[10];switch(t){case"XYZ":this._y=Math.asin(kt(a,-1,1)),Math.abs(a)<.9999999?(this._x=Math.atan2(-d,g),this._z=Math.atan2(-o,r)):(this._x=Math.atan2(p,u),this._z=0);break;case"YXZ":this._x=Math.asin(-kt(d,-1,1)),Math.abs(d)<.9999999?(this._y=Math.atan2(a,g),this._z=Math.atan2(l,u)):(this._y=Math.atan2(-m,r),this._z=0);break;case"ZXY":this._x=Math.asin(kt(p,-1,1)),Math.abs(p)<.9999999?(this._y=Math.atan2(-m,g),this._z=Math.atan2(-o,u)):(this._y=0,this._z=Math.atan2(l,r));break;case"ZYX":this._y=Math.asin(-kt(m,-1,1)),Math.abs(m)<.9999999?(this._x=Math.atan2(p,g),this._z=Math.atan2(l,r)):(this._x=0,this._z=Math.atan2(-o,u));break;case"YZX":this._z=Math.asin(kt(l,-1,1)),Math.abs(l)<.9999999?(this._x=Math.atan2(-d,u),this._y=Math.atan2(-m,r)):(this._x=0,this._y=Math.atan2(a,g));break;case"XZY":this._z=Math.asin(-kt(o,-1,1)),Math.abs(o)<.9999999?(this._x=Math.atan2(p,u),this._y=Math.atan2(a,r)):(this._x=Math.atan2(-d,g),this._y=0);break;default:console.warn("THREE.Euler: .setFromRotationMatrix() encountered an unknown order: "+t)}return this._order=t,n===!0&&this._onChangeCallback(),this}setFromQuaternion(e,t,n){return Au.makeRotationFromQuaternion(e),this.setFromRotationMatrix(Au,t,n)}setFromVector3(e,t=this._order){return this.set(e.x,e.y,e.z,t)}reorder(e){return Cu.setFromEuler(this),this.setFromQuaternion(Cu,e)}equals(e){return e._x===this._x&&e._y===this._y&&e._z===this._z&&e._order===this._order}fromArray(e){return this._x=e[0],this._y=e[1],this._z=e[2],e[3]!==void 0&&(this._order=e[3]),this._onChangeCallback(),this}toArray(e=[],t=0){return e[t]=this._x,e[t+1]=this._y,e[t+2]=this._z,e[t+3]=this._order,e}_onChange(e){return this._onChangeCallback=e,this}_onChangeCallback(){}*[Symbol.iterator](){yield this._x,yield this._y,yield this._z,yield this._order}}xn.DEFAULT_ORDER="XYZ";class Ka{constructor(){this.mask=1}set(e){this.mask=(1<<e|0)>>>0}enable(e){this.mask|=1<<e|0}enableAll(){this.mask=-1}toggle(e){this.mask^=1<<e|0}disable(e){this.mask&=~(1<<e|0)}disableAll(){this.mask=0}test(e){return(this.mask&e.mask)!==0}isEnabled(e){return(this.mask&(1<<e|0))!==0}}let Pv=0;const Ru=new O,Br=new Dt,hi=new Ke,Vo=new O,As=new O,Iv=new O,Lv=new Dt,Pu=new O(1,0,0),Iu=new O(0,1,0),Lu=new O(0,0,1),Du={type:"added"},Dv={type:"removed"},zr={type:"childadded",child:null},Fl={type:"childremoved",child:null};class xt extends ni{constructor(){super(),this.isObject3D=!0,Object.defineProperty(this,"id",{value:Pv++}),this.uuid=Rn(),this.name="",this.type="Object3D",this.parent=null,this.children=[],this.up=xt.DEFAULT_UP.clone();const e=new O,t=new xn,n=new Dt,i=new O(1,1,1);function r(){n.setFromEuler(t,!1)}function o(){t.setFromQuaternion(n,void 0,!1)}t._onChange(r),n._onChange(o),Object.defineProperties(this,{position:{configurable:!0,enumerable:!0,value:e},rotation:{configurable:!0,enumerable:!0,value:t},quaternion:{configurable:!0,enumerable:!0,value:n},scale:{configurable:!0,enumerable:!0,value:i},modelViewMatrix:{value:new Ke},normalMatrix:{value:new ut}}),this.matrix=new Ke,this.matrixWorld=new Ke,this.matrixAutoUpdate=xt.DEFAULT_MATRIX_AUTO_UPDATE,this.matrixWorldAutoUpdate=xt.DEFAULT_MATRIX_WORLD_AUTO_UPDATE,this.matrixWorldNeedsUpdate=!1,this.layers=new Ka,this.visible=!0,this.castShadow=!1,this.receiveShadow=!1,this.frustumCulled=!0,this.renderOrder=0,this.animations=[],this.userData={}}onBeforeShadow(){}onAfterShadow(){}onBeforeRender(){}onAfterRender(){}applyMatrix4(e){this.matrixAutoUpdate&&this.updateMatrix(),this.matrix.premultiply(e),this.matrix.decompose(this.position,this.quaternion,this.scale)}applyQuaternion(e){return this.quaternion.premultiply(e),this}setRotationFromAxisAngle(e,t){this.quaternion.setFromAxisAngle(e,t)}setRotationFromEuler(e){this.quaternion.setFromEuler(e,!0)}setRotationFromMatrix(e){this.quaternion.setFromRotationMatrix(e)}setRotationFromQuaternion(e){this.quaternion.copy(e)}rotateOnAxis(e,t){return Br.setFromAxisAngle(e,t),this.quaternion.multiply(Br),this}rotateOnWorldAxis(e,t){return Br.setFromAxisAngle(e,t),this.quaternion.premultiply(Br),this}rotateX(e){return this.rotateOnAxis(Pu,e)}rotateY(e){return this.rotateOnAxis(Iu,e)}rotateZ(e){return this.rotateOnAxis(Lu,e)}translateOnAxis(e,t){return Ru.copy(e).applyQuaternion(this.quaternion),this.position.add(Ru.multiplyScalar(t)),this}translateX(e){return this.translateOnAxis(Pu,e)}translateY(e){return this.translateOnAxis(Iu,e)}translateZ(e){return this.translateOnAxis(Lu,e)}localToWorld(e){return this.updateWorldMatrix(!0,!1),e.applyMatrix4(this.matrixWorld)}worldToLocal(e){return this.updateWorldMatrix(!0,!1),e.applyMatrix4(hi.copy(this.matrixWorld).invert())}lookAt(e,t,n){e.isVector3?Vo.copy(e):Vo.set(e,t,n);const i=this.parent;this.updateWorldMatrix(!0,!1),As.setFromMatrixPosition(this.matrixWorld),this.isCamera||this.isLight?hi.lookAt(As,Vo,this.up):hi.lookAt(Vo,As,this.up),this.quaternion.setFromRotationMatrix(hi),i&&(hi.extractRotation(i.matrixWorld),Br.setFromRotationMatrix(hi),this.quaternion.premultiply(Br.invert()))}add(e){if(arguments.length>1){for(let t=0;t<arguments.length;t++)this.add(arguments[t]);return this}return e===this?(console.error("THREE.Object3D.add: object can't be added as a child of itself.",e),this):(e&&e.isObject3D?(e.removeFromParent(),e.parent=this,this.children.push(e),e.dispatchEvent(Du),zr.child=e,this.dispatchEvent(zr),zr.child=null):console.error("THREE.Object3D.add: object not an instance of THREE.Object3D.",e),this)}remove(e){if(arguments.length>1){for(let n=0;n<arguments.length;n++)this.remove(arguments[n]);return this}const t=this.children.indexOf(e);return t!==-1&&(e.parent=null,this.children.splice(t,1),e.dispatchEvent(Dv),Fl.child=e,this.dispatchEvent(Fl),Fl.child=null),this}removeFromParent(){const e=this.parent;return e!==null&&e.remove(this),this}clear(){return this.remove(...this.children)}attach(e){return this.updateWorldMatrix(!0,!1),hi.copy(this.matrixWorld).invert(),e.parent!==null&&(e.parent.updateWorldMatrix(!0,!1),hi.multiply(e.parent.matrixWorld)),e.applyMatrix4(hi),e.removeFromParent(),e.parent=this,this.children.push(e),e.updateWorldMatrix(!1,!0),e.dispatchEvent(Du),zr.child=e,this.dispatchEvent(zr),zr.child=null,this}getObjectById(e){return this.getObjectByProperty("id",e)}getObjectByName(e){return this.getObjectByProperty("name",e)}getObjectByProperty(e,t){if(this[e]===t)return this;for(let n=0,i=this.children.length;n<i;n++){const o=this.children[n].getObjectByProperty(e,t);if(o!==void 0)return o}}getObjectsByProperty(e,t,n=[]){this[e]===t&&n.push(this);const i=this.children;for(let r=0,o=i.length;r<o;r++)i[r].getObjectsByProperty(e,t,n);return n}getWorldPosition(e){return this.updateWorldMatrix(!0,!1),e.setFromMatrixPosition(this.matrixWorld)}getWorldQuaternion(e){return this.updateWorldMatrix(!0,!1),this.matrixWorld.decompose(As,e,Iv),e}getWorldScale(e){return this.updateWorldMatrix(!0,!1),this.matrixWorld.decompose(As,Lv,e),e}getWorldDirection(e){this.updateWorldMatrix(!0,!1);const t=this.matrixWorld.elements;return e.set(t[8],t[9],t[10]).normalize()}raycast(){}traverse(e){e(this);const t=this.children;for(let n=0,i=t.length;n<i;n++)t[n].traverse(e)}traverseVisible(e){if(this.visible===!1)return;e(this);const t=this.children;for(let n=0,i=t.length;n<i;n++)t[n].traverseVisible(e)}traverseAncestors(e){const t=this.parent;t!==null&&(e(t),t.traverseAncestors(e))}updateMatrix(){this.matrix.compose(this.position,this.quaternion,this.scale),this.matrixWorldNeedsUpdate=!0}updateMatrixWorld(e){this.matrixAutoUpdate&&this.updateMatrix(),(this.matrixWorldNeedsUpdate||e)&&(this.parent===null?this.matrixWorld.copy(this.matrix):this.matrixWorld.multiplyMatrices(this.parent.matrixWorld,this.matrix),this.matrixWorldNeedsUpdate=!1,e=!0);const t=this.children;for(let n=0,i=t.length;n<i;n++){const r=t[n];(r.matrixWorldAutoUpdate===!0||e===!0)&&r.updateMatrixWorld(e)}}updateWorldMatrix(e,t){const n=this.parent;if(e===!0&&n!==null&&n.matrixWorldAutoUpdate===!0&&n.updateWorldMatrix(!0,!1),this.matrixAutoUpdate&&this.updateMatrix(),this.parent===null?this.matrixWorld.copy(this.matrix):this.matrixWorld.multiplyMatrices(this.parent.matrixWorld,this.matrix),t===!0){const i=this.children;for(let r=0,o=i.length;r<o;r++){const a=i[r];a.matrixWorldAutoUpdate===!0&&a.updateWorldMatrix(!1,!0)}}}toJSON(e){const t=e===void 0||typeof e=="string",n={};t&&(e={geometries:{},materials:{},textures:{},images:{},shapes:{},skeletons:{},animations:{},nodes:{}},n.metadata={version:4.6,type:"Object",generator:"Object3D.toJSON"});const i={};i.uuid=this.uuid,i.type=this.type,this.name!==""&&(i.name=this.name),this.castShadow===!0&&(i.castShadow=!0),this.receiveShadow===!0&&(i.receiveShadow=!0),this.visible===!1&&(i.visible=!1),this.frustumCulled===!1&&(i.frustumCulled=!1),this.renderOrder!==0&&(i.renderOrder=this.renderOrder),Object.keys(this.userData).length>0&&(i.userData=this.userData),i.layers=this.layers.mask,i.matrix=this.matrix.toArray(),i.up=this.up.toArray(),this.matrixAutoUpdate===!1&&(i.matrixAutoUpdate=!1),this.isInstancedMesh&&(i.type="InstancedMesh",i.count=this.count,i.instanceMatrix=this.instanceMatrix.toJSON(),this.instanceColor!==null&&(i.instanceColor=this.instanceColor.toJSON())),this.isBatchedMesh&&(i.type="BatchedMesh",i.perObjectFrustumCulled=this.perObjectFrustumCulled,i.sortObjects=this.sortObjects,i.drawRanges=this._drawRanges,i.reservedRanges=this._reservedRanges,i.visibility=this._visibility,i.active=this._active,i.bounds=this._bounds.map(a=>({boxInitialized:a.boxInitialized,boxMin:a.box.min.toArray(),boxMax:a.box.max.toArray(),sphereInitialized:a.sphereInitialized,sphereRadius:a.sphere.radius,sphereCenter:a.sphere.center.toArray()})),i.maxGeometryCount=this._maxGeometryCount,i.maxVertexCount=this._maxVertexCount,i.maxIndexCount=this._maxIndexCount,i.geometryInitialized=this._geometryInitialized,i.geometryCount=this._geometryCount,i.matricesTexture=this._matricesTexture.toJSON(e),this.boundingSphere!==null&&(i.boundingSphere={center:i.boundingSphere.center.toArray(),radius:i.boundingSphere.radius}),this.boundingBox!==null&&(i.boundingBox={min:i.boundingBox.min.toArray(),max:i.boundingBox.max.toArray()}));function r(a,l){return a[l.uuid]===void 0&&(a[l.uuid]=l.toJSON(e)),l.uuid}if(this.isScene)this.background&&(this.background.isColor?i.background=this.background.toJSON():this.background.isTexture&&(i.background=this.background.toJSON(e).uuid)),this.environment&&this.environment.isTexture&&this.environment.isRenderTargetTexture!==!0&&(i.environment=this.environment.toJSON(e).uuid);else if(this.isMesh||this.isLine||this.isPoints){i.geometry=r(e.geometries,this.geometry);const a=this.geometry.parameters;if(a!==void 0&&a.shapes!==void 0){const l=a.shapes;if(Array.isArray(l))for(let u=0,d=l.length;u<d;u++){const m=l[u];r(e.shapes,m)}else r(e.shapes,l)}}if(this.isSkinnedMesh&&(i.bindMode=this.bindMode,i.bindMatrix=this.bindMatrix.toArray(),this.skeleton!==void 0&&(r(e.skeletons,this.skeleton),i.skeleton=this.skeleton.uuid)),this.material!==void 0)if(Array.isArray(this.material)){const a=[];for(let l=0,u=this.material.length;l<u;l++)a.push(r(e.materials,this.material[l]));i.material=a}else i.material=r(e.materials,this.material);if(this.children.length>0){i.children=[];for(let a=0;a<this.children.length;a++)i.children.push(this.children[a].toJSON(e).object)}if(this.animations.length>0){i.animations=[];for(let a=0;a<this.animations.length;a++){const l=this.animations[a];i.animations.push(r(e.animations,l))}}if(t){const a=o(e.geometries),l=o(e.materials),u=o(e.textures),d=o(e.images),m=o(e.shapes),p=o(e.skeletons),g=o(e.animations),_=o(e.nodes);a.length>0&&(n.geometries=a),l.length>0&&(n.materials=l),u.length>0&&(n.textures=u),d.length>0&&(n.images=d),m.length>0&&(n.shapes=m),p.length>0&&(n.skeletons=p),g.length>0&&(n.animations=g),_.length>0&&(n.nodes=_)}return n.object=i,n;function o(a){const l=[];for(const u in a){const d=a[u];delete d.metadata,l.push(d)}return l}}clone(e){return new this.constructor().copy(this,e)}copy(e,t=!0){if(this.name=e.name,this.up.copy(e.up),this.position.copy(e.position),this.rotation.order=e.rotation.order,this.quaternion.copy(e.quaternion),this.scale.copy(e.scale),this.matrix.copy(e.matrix),this.matrixWorld.copy(e.matrixWorld),this.matrixAutoUpdate=e.matrixAutoUpdate,this.matrixWorldAutoUpdate=e.matrixWorldAutoUpdate,this.matrixWorldNeedsUpdate=e.matrixWorldNeedsUpdate,this.layers.mask=e.layers.mask,this.visible=e.visible,this.castShadow=e.castShadow,this.receiveShadow=e.receiveShadow,this.frustumCulled=e.frustumCulled,this.renderOrder=e.renderOrder,this.animations=e.animations.slice(),this.userData=JSON.parse(JSON.stringify(e.userData)),t===!0)for(let n=0;n<e.children.length;n++){const i=e.children[n];this.add(i.clone())}return this}}xt.DEFAULT_UP=new O(0,1,0);xt.DEFAULT_MATRIX_AUTO_UPDATE=!0;xt.DEFAULT_MATRIX_WORLD_AUTO_UPDATE=!0;const Wn=new O,ui=new O,Bl=new O,di=new O,kr=new O,Vr=new O,Nu=new O,zl=new O,kl=new O,Vl=new O;class An{constructor(e=new O,t=new O,n=new O){this.a=e,this.b=t,this.c=n}static getNormal(e,t,n,i){i.subVectors(n,t),Wn.subVectors(e,t),i.cross(Wn);const r=i.lengthSq();return r>0?i.multiplyScalar(1/Math.sqrt(r)):i.set(0,0,0)}static getBarycoord(e,t,n,i,r){Wn.subVectors(i,t),ui.subVectors(n,t),Bl.subVectors(e,t);const o=Wn.dot(Wn),a=Wn.dot(ui),l=Wn.dot(Bl),u=ui.dot(ui),d=ui.dot(Bl),m=o*u-a*a;if(m===0)return r.set(0,0,0),null;const p=1/m,g=(u*l-a*d)*p,_=(o*d-a*l)*p;return r.set(1-g-_,_,g)}static containsPoint(e,t,n,i){return this.getBarycoord(e,t,n,i,di)===null?!1:di.x>=0&&di.y>=0&&di.x+di.y<=1}static getInterpolation(e,t,n,i,r,o,a,l){return this.getBarycoord(e,t,n,i,di)===null?(l.x=0,l.y=0,"z"in l&&(l.z=0),"w"in l&&(l.w=0),null):(l.setScalar(0),l.addScaledVector(r,di.x),l.addScaledVector(o,di.y),l.addScaledVector(a,di.z),l)}static isFrontFacing(e,t,n,i){return Wn.subVectors(n,t),ui.subVectors(e,t),Wn.cross(ui).dot(i)<0}set(e,t,n){return this.a.copy(e),this.b.copy(t),this.c.copy(n),this}setFromPointsAndIndices(e,t,n,i){return this.a.copy(e[t]),this.b.copy(e[n]),this.c.copy(e[i]),this}setFromAttributeAndIndices(e,t,n,i){return this.a.fromBufferAttribute(e,t),this.b.fromBufferAttribute(e,n),this.c.fromBufferAttribute(e,i),this}clone(){return new this.constructor().copy(this)}copy(e){return this.a.copy(e.a),this.b.copy(e.b),this.c.copy(e.c),this}getArea(){return Wn.subVectors(this.c,this.b),ui.subVectors(this.a,this.b),Wn.cross(ui).length()*.5}getMidpoint(e){return e.addVectors(this.a,this.b).add(this.c).multiplyScalar(1/3)}getNormal(e){return An.getNormal(this.a,this.b,this.c,e)}getPlane(e){return e.setFromCoplanarPoints(this.a,this.b,this.c)}getBarycoord(e,t){return An.getBarycoord(e,this.a,this.b,this.c,t)}getInterpolation(e,t,n,i,r){return An.getInterpolation(e,this.a,this.b,this.c,t,n,i,r)}containsPoint(e){return An.containsPoint(e,this.a,this.b,this.c)}isFrontFacing(e){return An.isFrontFacing(this.a,this.b,this.c,e)}intersectsBox(e){return e.intersectsTriangle(this)}closestPointToPoint(e,t){const n=this.a,i=this.b,r=this.c;let o,a;kr.subVectors(i,n),Vr.subVectors(r,n),zl.subVectors(e,n);const l=kr.dot(zl),u=Vr.dot(zl);if(l<=0&&u<=0)return t.copy(n);kl.subVectors(e,i);const d=kr.dot(kl),m=Vr.dot(kl);if(d>=0&&m<=d)return t.copy(i);const p=l*m-d*u;if(p<=0&&l>=0&&d<=0)return o=l/(l-d),t.copy(n).addScaledVector(kr,o);Vl.subVectors(e,r);const g=kr.dot(Vl),_=Vr.dot(Vl);if(_>=0&&g<=_)return t.copy(r);const M=g*u-l*_;if(M<=0&&u>=0&&_<=0)return a=u/(u-_),t.copy(n).addScaledVector(Vr,a);const y=d*_-g*m;if(y<=0&&m-d>=0&&g-_>=0)return Nu.subVectors(r,i),a=(m-d)/(m-d+(g-_)),t.copy(i).addScaledVector(Nu,a);const v=1/(y+M+p);return o=M*v,a=p*v,t.copy(n).addScaledVector(kr,o).addScaledVector(Vr,a)}equals(e){return e.a.equals(this.a)&&e.b.equals(this.b)&&e.c.equals(this.c)}}const xp={aliceblue:15792383,antiquewhite:16444375,aqua:65535,aquamarine:8388564,azure:15794175,beige:16119260,bisque:16770244,black:0,blanchedalmond:16772045,blue:255,blueviolet:9055202,brown:10824234,burlywood:14596231,cadetblue:6266528,chartreuse:8388352,chocolate:13789470,coral:16744272,cornflowerblue:6591981,cornsilk:16775388,crimson:14423100,cyan:65535,darkblue:139,darkcyan:35723,darkgoldenrod:12092939,darkgray:11119017,darkgreen:25600,darkgrey:11119017,darkkhaki:12433259,darkmagenta:9109643,darkolivegreen:5597999,darkorange:16747520,darkorchid:10040012,darkred:9109504,darksalmon:15308410,darkseagreen:9419919,darkslateblue:4734347,darkslategray:3100495,darkslategrey:3100495,darkturquoise:52945,darkviolet:9699539,deeppink:16716947,deepskyblue:49151,dimgray:6908265,dimgrey:6908265,dodgerblue:2003199,firebrick:11674146,floralwhite:16775920,forestgreen:2263842,fuchsia:16711935,gainsboro:14474460,ghostwhite:16316671,gold:16766720,goldenrod:14329120,gray:8421504,green:32768,greenyellow:11403055,grey:8421504,honeydew:15794160,hotpink:16738740,indianred:13458524,indigo:4915330,ivory:16777200,khaki:15787660,lavender:15132410,lavenderblush:16773365,lawngreen:8190976,lemonchiffon:16775885,lightblue:11393254,lightcoral:15761536,lightcyan:14745599,lightgoldenrodyellow:16448210,lightgray:13882323,lightgreen:9498256,lightgrey:13882323,lightpink:16758465,lightsalmon:16752762,lightseagreen:2142890,lightskyblue:8900346,lightslategray:7833753,lightslategrey:7833753,lightsteelblue:11584734,lightyellow:16777184,lime:65280,limegreen:3329330,linen:16445670,magenta:16711935,maroon:8388608,mediumaquamarine:6737322,mediumblue:205,mediumorchid:12211667,mediumpurple:9662683,mediumseagreen:3978097,mediumslateblue:8087790,mediumspringgreen:64154,mediumturquoise:4772300,mediumvioletred:13047173,midnightblue:1644912,mintcream:16121850,mistyrose:16770273,moccasin:16770229,navajowhite:16768685,navy:128,oldlace:16643558,olive:8421376,olivedrab:7048739,orange:16753920,orangered:16729344,orchid:14315734,palegoldenrod:15657130,palegreen:10025880,paleturquoise:11529966,palevioletred:14381203,papayawhip:16773077,peachpuff:16767673,peru:13468991,pink:16761035,plum:14524637,powderblue:11591910,purple:8388736,rebeccapurple:6697881,red:16711680,rosybrown:12357519,royalblue:4286945,saddlebrown:9127187,salmon:16416882,sandybrown:16032864,seagreen:3050327,seashell:16774638,sienna:10506797,silver:12632256,skyblue:8900331,slateblue:6970061,slategray:7372944,slategrey:7372944,snow:16775930,springgreen:65407,steelblue:4620980,tan:13808780,teal:32896,thistle:14204888,tomato:16737095,turquoise:4251856,violet:15631086,wheat:16113331,white:16777215,whitesmoke:16119285,yellow:16776960,yellowgreen:10145074},Ni={h:0,s:0,l:0},Ho={h:0,s:0,l:0};function Hl(s,e,t){return t<0&&(t+=1),t>1&&(t-=1),t<1/6?s+(e-s)*6*t:t<1/2?e:t<2/3?s+(e-s)*6*(2/3-t):s}class Oe{constructor(e,t,n){return this.isColor=!0,this.r=1,this.g=1,this.b=1,this.set(e,t,n)}set(e,t,n){if(t===void 0&&n===void 0){const i=e;i&&i.isColor?this.copy(i):typeof i=="number"?this.setHex(i):typeof i=="string"&&this.setStyle(i)}else this.setRGB(e,t,n);return this}setScalar(e){return this.r=e,this.g=e,this.b=e,this}setHex(e,t=Un){return e=Math.floor(e),this.r=(e>>16&255)/255,this.g=(e>>8&255)/255,this.b=(e&255)/255,wt.toWorkingColorSpace(this,t),this}setRGB(e,t,n,i=wt.workingColorSpace){return this.r=e,this.g=t,this.b=n,wt.toWorkingColorSpace(this,i),this}setHSL(e,t,n,i=wt.workingColorSpace){if(e=bh(e,1),t=kt(t,0,1),n=kt(n,0,1),t===0)this.r=this.g=this.b=n;else{const r=n<=.5?n*(1+t):n+t-n*t,o=2*n-r;this.r=Hl(o,r,e+1/3),this.g=Hl(o,r,e),this.b=Hl(o,r,e-1/3)}return wt.toWorkingColorSpace(this,i),this}setStyle(e,t=Un){function n(r){r!==void 0&&parseFloat(r)<1&&console.warn("THREE.Color: Alpha component of "+e+" will be ignored.")}let i;if(i=/^(\w+)\(([^\)]*)\)/.exec(e)){let r;const o=i[1],a=i[2];switch(o){case"rgb":case"rgba":if(r=/^\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*(?:,\s*(\d*\.?\d+)\s*)?$/.exec(a))return n(r[4]),this.setRGB(Math.min(255,parseInt(r[1],10))/255,Math.min(255,parseInt(r[2],10))/255,Math.min(255,parseInt(r[3],10))/255,t);if(r=/^\s*(\d+)\%\s*,\s*(\d+)\%\s*,\s*(\d+)\%\s*(?:,\s*(\d*\.?\d+)\s*)?$/.exec(a))return n(r[4]),this.setRGB(Math.min(100,parseInt(r[1],10))/100,Math.min(100,parseInt(r[2],10))/100,Math.min(100,parseInt(r[3],10))/100,t);break;case"hsl":case"hsla":if(r=/^\s*(\d*\.?\d+)\s*,\s*(\d*\.?\d+)\%\s*,\s*(\d*\.?\d+)\%\s*(?:,\s*(\d*\.?\d+)\s*)?$/.exec(a))return n(r[4]),this.setHSL(parseFloat(r[1])/360,parseFloat(r[2])/100,parseFloat(r[3])/100,t);break;default:console.warn("THREE.Color: Unknown color model "+e)}}else if(i=/^\#([A-Fa-f\d]+)$/.exec(e)){const r=i[1],o=r.length;if(o===3)return this.setRGB(parseInt(r.charAt(0),16)/15,parseInt(r.charAt(1),16)/15,parseInt(r.charAt(2),16)/15,t);if(o===6)return this.setHex(parseInt(r,16),t);console.warn("THREE.Color: Invalid hex color "+e)}else if(e&&e.length>0)return this.setColorName(e,t);return this}setColorName(e,t=Un){const n=xp[e.toLowerCase()];return n!==void 0?this.setHex(n,t):console.warn("THREE.Color: Unknown color "+e),this}clone(){return new this.constructor(this.r,this.g,this.b)}copy(e){return this.r=e.r,this.g=e.g,this.b=e.b,this}copySRGBToLinear(e){return this.r=os(e.r),this.g=os(e.g),this.b=os(e.b),this}copyLinearToSRGB(e){return this.r=Rl(e.r),this.g=Rl(e.g),this.b=Rl(e.b),this}convertSRGBToLinear(){return this.copySRGBToLinear(this),this}convertLinearToSRGB(){return this.copyLinearToSRGB(this),this}getHex(e=Un){return wt.fromWorkingColorSpace(sn.copy(this),e),Math.round(kt(sn.r*255,0,255))*65536+Math.round(kt(sn.g*255,0,255))*256+Math.round(kt(sn.b*255,0,255))}getHexString(e=Un){return("000000"+this.getHex(e).toString(16)).slice(-6)}getHSL(e,t=wt.workingColorSpace){wt.fromWorkingColorSpace(sn.copy(this),t);const n=sn.r,i=sn.g,r=sn.b,o=Math.max(n,i,r),a=Math.min(n,i,r);let l,u;const d=(a+o)/2;if(a===o)l=0,u=0;else{const m=o-a;switch(u=d<=.5?m/(o+a):m/(2-o-a),o){case n:l=(i-r)/m+(i<r?6:0);break;case i:l=(r-n)/m+2;break;case r:l=(n-i)/m+4;break}l/=6}return e.h=l,e.s=u,e.l=d,e}getRGB(e,t=wt.workingColorSpace){return wt.fromWorkingColorSpace(sn.copy(this),t),e.r=sn.r,e.g=sn.g,e.b=sn.b,e}getStyle(e=Un){wt.fromWorkingColorSpace(sn.copy(this),e);const t=sn.r,n=sn.g,i=sn.b;return e!==Un?`color(${e} ${t.toFixed(3)} ${n.toFixed(3)} ${i.toFixed(3)})`:`rgb(${Math.round(t*255)},${Math.round(n*255)},${Math.round(i*255)})`}offsetHSL(e,t,n){return this.getHSL(Ni),this.setHSL(Ni.h+e,Ni.s+t,Ni.l+n)}add(e){return this.r+=e.r,this.g+=e.g,this.b+=e.b,this}addColors(e,t){return this.r=e.r+t.r,this.g=e.g+t.g,this.b=e.b+t.b,this}addScalar(e){return this.r+=e,this.g+=e,this.b+=e,this}sub(e){return this.r=Math.max(0,this.r-e.r),this.g=Math.max(0,this.g-e.g),this.b=Math.max(0,this.b-e.b),this}multiply(e){return this.r*=e.r,this.g*=e.g,this.b*=e.b,this}multiplyScalar(e){return this.r*=e,this.g*=e,this.b*=e,this}lerp(e,t){return this.r+=(e.r-this.r)*t,this.g+=(e.g-this.g)*t,this.b+=(e.b-this.b)*t,this}lerpColors(e,t,n){return this.r=e.r+(t.r-e.r)*n,this.g=e.g+(t.g-e.g)*n,this.b=e.b+(t.b-e.b)*n,this}lerpHSL(e,t){this.getHSL(Ni),e.getHSL(Ho);const n=Xs(Ni.h,Ho.h,t),i=Xs(Ni.s,Ho.s,t),r=Xs(Ni.l,Ho.l,t);return this.setHSL(n,i,r),this}setFromVector3(e){return this.r=e.x,this.g=e.y,this.b=e.z,this}applyMatrix3(e){const t=this.r,n=this.g,i=this.b,r=e.elements;return this.r=r[0]*t+r[3]*n+r[6]*i,this.g=r[1]*t+r[4]*n+r[7]*i,this.b=r[2]*t+r[5]*n+r[8]*i,this}equals(e){return e.r===this.r&&e.g===this.g&&e.b===this.b}fromArray(e,t=0){return this.r=e[t],this.g=e[t+1],this.b=e[t+2],this}toArray(e=[],t=0){return e[t]=this.r,e[t+1]=this.g,e[t+2]=this.b,e}fromBufferAttribute(e,t){return this.r=e.getX(t),this.g=e.getY(t),this.b=e.getZ(t),this}toJSON(){return this.getHex()}*[Symbol.iterator](){yield this.r,yield this.g,yield this.b}}const sn=new Oe;Oe.NAMES=xp;let Nv=0;class hn extends ni{constructor(){super(),this.isMaterial=!0,Object.defineProperty(this,"id",{value:Nv++}),this.uuid=Rn(),this.name="",this.type="Material",this.blending=xr,this.side=xi,this.vertexColors=!1,this.opacity=1,this.transparent=!1,this.alphaHash=!1,this.blendSrc=ka,this.blendDst=Va,this.blendEquation=zi,this.blendSrcAlpha=null,this.blendDstAlpha=null,this.blendEquationAlpha=null,this.blendColor=new Oe(0,0,0),this.blendAlpha=0,this.depthFunc=Zs,this.depthTest=!0,this.depthWrite=!0,this.stencilWriteMask=255,this.stencilFunc=Yc,this.stencilRef=0,this.stencilFuncMask=255,this.stencilFail=dr,this.stencilZFail=dr,this.stencilZPass=dr,this.stencilWrite=!1,this.clippingPlanes=null,this.clipIntersection=!1,this.clipShadows=!1,this.shadowSide=null,this.colorWrite=!0,this.precision=null,this.polygonOffset=!1,this.polygonOffsetFactor=0,this.polygonOffsetUnits=0,this.dithering=!1,this.alphaToCoverage=!1,this.premultipliedAlpha=!1,this.forceSinglePass=!1,this.visible=!0,this.toneMapped=!0,this.userData={},this.version=0,this._alphaTest=0}get alphaTest(){return this._alphaTest}set alphaTest(e){this._alphaTest>0!=e>0&&this.version++,this._alphaTest=e}onBuild(){}onBeforeRender(){}onBeforeCompile(){}customProgramCacheKey(){return this.onBeforeCompile.toString()}setValues(e){if(e!==void 0)for(const t in e){const n=e[t];if(n===void 0){console.warn(`THREE.Material: parameter '${t}' has value of undefined.`);continue}const i=this[t];if(i===void 0){console.warn(`THREE.Material: '${t}' is not a property of THREE.${this.type}.`);continue}i&&i.isColor?i.set(n):i&&i.isVector3&&n&&n.isVector3?i.copy(n):this[t]=n}}toJSON(e){const t=e===void 0||typeof e=="string";t&&(e={textures:{},images:{}});const n={metadata:{version:4.6,type:"Material",generator:"Material.toJSON"}};n.uuid=this.uuid,n.type=this.type,this.name!==""&&(n.name=this.name),this.color&&this.color.isColor&&(n.color=this.color.getHex()),this.roughness!==void 0&&(n.roughness=this.roughness),this.metalness!==void 0&&(n.metalness=this.metalness),this.sheen!==void 0&&(n.sheen=this.sheen),this.sheenColor&&this.sheenColor.isColor&&(n.sheenColor=this.sheenColor.getHex()),this.sheenRoughness!==void 0&&(n.sheenRoughness=this.sheenRoughness),this.emissive&&this.emissive.isColor&&(n.emissive=this.emissive.getHex()),this.emissiveIntensity!==void 0&&this.emissiveIntensity!==1&&(n.emissiveIntensity=this.emissiveIntensity),this.specular&&this.specular.isColor&&(n.specular=this.specular.getHex()),this.specularIntensity!==void 0&&(n.specularIntensity=this.specularIntensity),this.specularColor&&this.specularColor.isColor&&(n.specularColor=this.specularColor.getHex()),this.shininess!==void 0&&(n.shininess=this.shininess),this.clearcoat!==void 0&&(n.clearcoat=this.clearcoat),this.clearcoatRoughness!==void 0&&(n.clearcoatRoughness=this.clearcoatRoughness),this.clearcoatMap&&this.clearcoatMap.isTexture&&(n.clearcoatMap=this.clearcoatMap.toJSON(e).uuid),this.clearcoatRoughnessMap&&this.clearcoatRoughnessMap.isTexture&&(n.clearcoatRoughnessMap=this.clearcoatRoughnessMap.toJSON(e).uuid),this.clearcoatNormalMap&&this.clearcoatNormalMap.isTexture&&(n.clearcoatNormalMap=this.clearcoatNormalMap.toJSON(e).uuid,n.clearcoatNormalScale=this.clearcoatNormalScale.toArray()),this.dispersion!==void 0&&(n.dispersion=this.dispersion),this.iridescence!==void 0&&(n.iridescence=this.iridescence),this.iridescenceIOR!==void 0&&(n.iridescenceIOR=this.iridescenceIOR),this.iridescenceThicknessRange!==void 0&&(n.iridescenceThicknessRange=this.iridescenceThicknessRange),this.iridescenceMap&&this.iridescenceMap.isTexture&&(n.iridescenceMap=this.iridescenceMap.toJSON(e).uuid),this.iridescenceThicknessMap&&this.iridescenceThicknessMap.isTexture&&(n.iridescenceThicknessMap=this.iridescenceThicknessMap.toJSON(e).uuid),this.anisotropy!==void 0&&(n.anisotropy=this.anisotropy),this.anisotropyRotation!==void 0&&(n.anisotropyRotation=this.anisotropyRotation),this.anisotropyMap&&this.anisotropyMap.isTexture&&(n.anisotropyMap=this.anisotropyMap.toJSON(e).uuid),this.map&&this.map.isTexture&&(n.map=this.map.toJSON(e).uuid),this.matcap&&this.matcap.isTexture&&(n.matcap=this.matcap.toJSON(e).uuid),this.alphaMap&&this.alphaMap.isTexture&&(n.alphaMap=this.alphaMap.toJSON(e).uuid),this.lightMap&&this.lightMap.isTexture&&(n.lightMap=this.lightMap.toJSON(e).uuid,n.lightMapIntensity=this.lightMapIntensity),this.aoMap&&this.aoMap.isTexture&&(n.aoMap=this.aoMap.toJSON(e).uuid,n.aoMapIntensity=this.aoMapIntensity),this.bumpMap&&this.bumpMap.isTexture&&(n.bumpMap=this.bumpMap.toJSON(e).uuid,n.bumpScale=this.bumpScale),this.normalMap&&this.normalMap.isTexture&&(n.normalMap=this.normalMap.toJSON(e).uuid,n.normalMapType=this.normalMapType,n.normalScale=this.normalScale.toArray()),this.displacementMap&&this.displacementMap.isTexture&&(n.displacementMap=this.displacementMap.toJSON(e).uuid,n.displacementScale=this.displacementScale,n.displacementBias=this.displacementBias),this.roughnessMap&&this.roughnessMap.isTexture&&(n.roughnessMap=this.roughnessMap.toJSON(e).uuid),this.metalnessMap&&this.metalnessMap.isTexture&&(n.metalnessMap=this.metalnessMap.toJSON(e).uuid),this.emissiveMap&&this.emissiveMap.isTexture&&(n.emissiveMap=this.emissiveMap.toJSON(e).uuid),this.specularMap&&this.specularMap.isTexture&&(n.specularMap=this.specularMap.toJSON(e).uuid),this.specularIntensityMap&&this.specularIntensityMap.isTexture&&(n.specularIntensityMap=this.specularIntensityMap.toJSON(e).uuid),this.specularColorMap&&this.specularColorMap.isTexture&&(n.specularColorMap=this.specularColorMap.toJSON(e).uuid),this.envMap&&this.envMap.isTexture&&(n.envMap=this.envMap.toJSON(e).uuid,this.combine!==void 0&&(n.combine=this.combine)),this.envMapRotation!==void 0&&(n.envMapRotation=this.envMapRotation.toArray()),this.envMapIntensity!==void 0&&(n.envMapIntensity=this.envMapIntensity),this.reflectivity!==void 0&&(n.reflectivity=this.reflectivity),this.refractionRatio!==void 0&&(n.refractionRatio=this.refractionRatio),this.gradientMap&&this.gradientMap.isTexture&&(n.gradientMap=this.gradientMap.toJSON(e).uuid),this.transmission!==void 0&&(n.transmission=this.transmission),this.transmissionMap&&this.transmissionMap.isTexture&&(n.transmissionMap=this.transmissionMap.toJSON(e).uuid),this.thickness!==void 0&&(n.thickness=this.thickness),this.thicknessMap&&this.thicknessMap.isTexture&&(n.thicknessMap=this.thicknessMap.toJSON(e).uuid),this.attenuationDistance!==void 0&&this.attenuationDistance!==1/0&&(n.attenuationDistance=this.attenuationDistance),this.attenuationColor!==void 0&&(n.attenuationColor=this.attenuationColor.getHex()),this.size!==void 0&&(n.size=this.size),this.shadowSide!==null&&(n.shadowSide=this.shadowSide),this.sizeAttenuation!==void 0&&(n.sizeAttenuation=this.sizeAttenuation),this.blending!==xr&&(n.blending=this.blending),this.side!==xi&&(n.side=this.side),this.vertexColors===!0&&(n.vertexColors=!0),this.opacity<1&&(n.opacity=this.opacity),this.transparent===!0&&(n.transparent=!0),this.blendSrc!==ka&&(n.blendSrc=this.blendSrc),this.blendDst!==Va&&(n.blendDst=this.blendDst),this.blendEquation!==zi&&(n.blendEquation=this.blendEquation),this.blendSrcAlpha!==null&&(n.blendSrcAlpha=this.blendSrcAlpha),this.blendDstAlpha!==null&&(n.blendDstAlpha=this.blendDstAlpha),this.blendEquationAlpha!==null&&(n.blendEquationAlpha=this.blendEquationAlpha),this.blendColor&&this.blendColor.isColor&&(n.blendColor=this.blendColor.getHex()),this.blendAlpha!==0&&(n.blendAlpha=this.blendAlpha),this.depthFunc!==Zs&&(n.depthFunc=this.depthFunc),this.depthTest===!1&&(n.depthTest=this.depthTest),this.depthWrite===!1&&(n.depthWrite=this.depthWrite),this.colorWrite===!1&&(n.colorWrite=this.colorWrite),this.stencilWriteMask!==255&&(n.stencilWriteMask=this.stencilWriteMask),this.stencilFunc!==Yc&&(n.stencilFunc=this.stencilFunc),this.stencilRef!==0&&(n.stencilRef=this.stencilRef),this.stencilFuncMask!==255&&(n.stencilFuncMask=this.stencilFuncMask),this.stencilFail!==dr&&(n.stencilFail=this.stencilFail),this.stencilZFail!==dr&&(n.stencilZFail=this.stencilZFail),this.stencilZPass!==dr&&(n.stencilZPass=this.stencilZPass),this.stencilWrite===!0&&(n.stencilWrite=this.stencilWrite),this.rotation!==void 0&&this.rotation!==0&&(n.rotation=this.rotation),this.polygonOffset===!0&&(n.polygonOffset=!0),this.polygonOffsetFactor!==0&&(n.polygonOffsetFactor=this.polygonOffsetFactor),this.polygonOffsetUnits!==0&&(n.polygonOffsetUnits=this.polygonOffsetUnits),this.linewidth!==void 0&&this.linewidth!==1&&(n.linewidth=this.linewidth),this.dashSize!==void 0&&(n.dashSize=this.dashSize),this.gapSize!==void 0&&(n.gapSize=this.gapSize),this.scale!==void 0&&(n.scale=this.scale),this.dithering===!0&&(n.dithering=!0),this.alphaTest>0&&(n.alphaTest=this.alphaTest),this.alphaHash===!0&&(n.alphaHash=!0),this.alphaToCoverage===!0&&(n.alphaToCoverage=!0),this.premultipliedAlpha===!0&&(n.premultipliedAlpha=!0),this.forceSinglePass===!0&&(n.forceSinglePass=!0),this.wireframe===!0&&(n.wireframe=!0),this.wireframeLinewidth>1&&(n.wireframeLinewidth=this.wireframeLinewidth),this.wireframeLinecap!=="round"&&(n.wireframeLinecap=this.wireframeLinecap),this.wireframeLinejoin!=="round"&&(n.wireframeLinejoin=this.wireframeLinejoin),this.flatShading===!0&&(n.flatShading=!0),this.visible===!1&&(n.visible=!1),this.toneMapped===!1&&(n.toneMapped=!1),this.fog===!1&&(n.fog=!1),Object.keys(this.userData).length>0&&(n.userData=this.userData);function i(r){const o=[];for(const a in r){const l=r[a];delete l.metadata,o.push(l)}return o}if(t){const r=i(e.textures),o=i(e.images);r.length>0&&(n.textures=r),o.length>0&&(n.images=o)}return n}clone(){return new this.constructor().copy(this)}copy(e){this.name=e.name,this.blending=e.blending,this.side=e.side,this.vertexColors=e.vertexColors,this.opacity=e.opacity,this.transparent=e.transparent,this.blendSrc=e.blendSrc,this.blendDst=e.blendDst,this.blendEquation=e.blendEquation,this.blendSrcAlpha=e.blendSrcAlpha,this.blendDstAlpha=e.blendDstAlpha,this.blendEquationAlpha=e.blendEquationAlpha,this.blendColor.copy(e.blendColor),this.blendAlpha=e.blendAlpha,this.depthFunc=e.depthFunc,this.depthTest=e.depthTest,this.depthWrite=e.depthWrite,this.stencilWriteMask=e.stencilWriteMask,this.stencilFunc=e.stencilFunc,this.stencilRef=e.stencilRef,this.stencilFuncMask=e.stencilFuncMask,this.stencilFail=e.stencilFail,this.stencilZFail=e.stencilZFail,this.stencilZPass=e.stencilZPass,this.stencilWrite=e.stencilWrite;const t=e.clippingPlanes;let n=null;if(t!==null){const i=t.length;n=new Array(i);for(let r=0;r!==i;++r)n[r]=t[r].clone()}return this.clippingPlanes=n,this.clipIntersection=e.clipIntersection,this.clipShadows=e.clipShadows,this.shadowSide=e.shadowSide,this.colorWrite=e.colorWrite,this.precision=e.precision,this.polygonOffset=e.polygonOffset,this.polygonOffsetFactor=e.polygonOffsetFactor,this.polygonOffsetUnits=e.polygonOffsetUnits,this.dithering=e.dithering,this.alphaTest=e.alphaTest,this.alphaHash=e.alphaHash,this.alphaToCoverage=e.alphaToCoverage,this.premultipliedAlpha=e.premultipliedAlpha,this.forceSinglePass=e.forceSinglePass,this.visible=e.visible,this.toneMapped=e.toneMapped,this.userData=JSON.parse(JSON.stringify(e.userData)),this}dispose(){this.dispatchEvent({type:"dispose"})}set needsUpdate(e){e===!0&&this.version++}}class ii extends hn{constructor(e){super(),this.isMeshBasicMaterial=!0,this.type="MeshBasicMaterial",this.color=new Oe(16777215),this.map=null,this.lightMap=null,this.lightMapIntensity=1,this.aoMap=null,this.aoMapIntensity=1,this.specularMap=null,this.alphaMap=null,this.envMap=null,this.envMapRotation=new xn,this.combine=vo,this.reflectivity=1,this.refractionRatio=.98,this.wireframe=!1,this.wireframeLinewidth=1,this.wireframeLinecap="round",this.wireframeLinejoin="round",this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.color.copy(e.color),this.map=e.map,this.lightMap=e.lightMap,this.lightMapIntensity=e.lightMapIntensity,this.aoMap=e.aoMap,this.aoMapIntensity=e.aoMapIntensity,this.specularMap=e.specularMap,this.alphaMap=e.alphaMap,this.envMap=e.envMap,this.envMapRotation.copy(e.envMapRotation),this.combine=e.combine,this.reflectivity=e.reflectivity,this.refractionRatio=e.refractionRatio,this.wireframe=e.wireframe,this.wireframeLinewidth=e.wireframeLinewidth,this.wireframeLinecap=e.wireframeLinecap,this.wireframeLinejoin=e.wireframeLinejoin,this.fog=e.fog,this}}const mi=Ov();function Ov(){const s=new ArrayBuffer(4),e=new Float32Array(s),t=new Uint32Array(s),n=new Uint32Array(512),i=new Uint32Array(512);for(let l=0;l<256;++l){const u=l-127;u<-27?(n[l]=0,n[l|256]=32768,i[l]=24,i[l|256]=24):u<-14?(n[l]=1024>>-u-14,n[l|256]=1024>>-u-14|32768,i[l]=-u-1,i[l|256]=-u-1):u<=15?(n[l]=u+15<<10,n[l|256]=u+15<<10|32768,i[l]=13,i[l|256]=13):u<128?(n[l]=31744,n[l|256]=64512,i[l]=24,i[l|256]=24):(n[l]=31744,n[l|256]=64512,i[l]=13,i[l|256]=13)}const r=new Uint32Array(2048),o=new Uint32Array(64),a=new Uint32Array(64);for(let l=1;l<1024;++l){let u=l<<13,d=0;for(;!(u&8388608);)u<<=1,d-=8388608;u&=-8388609,d+=947912704,r[l]=u|d}for(let l=1024;l<2048;++l)r[l]=939524096+(l-1024<<13);for(let l=1;l<31;++l)o[l]=l<<23;o[31]=1199570944,o[32]=2147483648;for(let l=33;l<63;++l)o[l]=2147483648+(l-32<<23);o[63]=3347054592;for(let l=1;l<64;++l)l!==32&&(a[l]=1024);return{floatView:e,uint32View:t,baseTable:n,shiftTable:i,mantissaTable:r,exponentTable:o,offsetTable:a}}function Mn(s){Math.abs(s)>65504&&console.warn("THREE.DataUtils.toHalfFloat(): Value out of range."),s=kt(s,-65504,65504),mi.floatView[0]=s;const e=mi.uint32View[0],t=e>>23&511;return mi.baseTable[t]+((e&8388607)>>mi.shiftTable[t])}function zs(s){const e=s>>10;return mi.uint32View[0]=mi.mantissaTable[mi.offsetTable[e]+(s&1023)]+mi.exponentTable[e],mi.floatView[0]}const Uv={toHalfFloat:Mn,fromHalfFloat:zs},Gt=new O,Go=new se;class Rt{constructor(e,t,n=!1){if(Array.isArray(e))throw new TypeError("THREE.BufferAttribute: array should be a Typed Array.");this.isBufferAttribute=!0,this.name="",this.array=e,this.itemSize=t,this.count=e!==void 0?e.length/t:0,this.normalized=n,this.usage=so,this._updateRange={offset:0,count:-1},this.updateRanges=[],this.gpuType=zn,this.version=0}onUploadCallback(){}set needsUpdate(e){e===!0&&this.version++}get updateRange(){return vp("THREE.BufferAttribute: updateRange() is deprecated and will be removed in r169. Use addUpdateRange() instead."),this._updateRange}setUsage(e){return this.usage=e,this}addUpdateRange(e,t){this.updateRanges.push({start:e,count:t})}clearUpdateRanges(){this.updateRanges.length=0}copy(e){return this.name=e.name,this.array=new e.array.constructor(e.array),this.itemSize=e.itemSize,this.count=e.count,this.normalized=e.normalized,this.usage=e.usage,this.gpuType=e.gpuType,this}copyAt(e,t,n){e*=this.itemSize,n*=t.itemSize;for(let i=0,r=this.itemSize;i<r;i++)this.array[e+i]=t.array[n+i];return this}copyArray(e){return this.array.set(e),this}applyMatrix3(e){if(this.itemSize===2)for(let t=0,n=this.count;t<n;t++)Go.fromBufferAttribute(this,t),Go.applyMatrix3(e),this.setXY(t,Go.x,Go.y);else if(this.itemSize===3)for(let t=0,n=this.count;t<n;t++)Gt.fromBufferAttribute(this,t),Gt.applyMatrix3(e),this.setXYZ(t,Gt.x,Gt.y,Gt.z);return this}applyMatrix4(e){for(let t=0,n=this.count;t<n;t++)Gt.fromBufferAttribute(this,t),Gt.applyMatrix4(e),this.setXYZ(t,Gt.x,Gt.y,Gt.z);return this}applyNormalMatrix(e){for(let t=0,n=this.count;t<n;t++)Gt.fromBufferAttribute(this,t),Gt.applyNormalMatrix(e),this.setXYZ(t,Gt.x,Gt.y,Gt.z);return this}transformDirection(e){for(let t=0,n=this.count;t<n;t++)Gt.fromBufferAttribute(this,t),Gt.transformDirection(e),this.setXYZ(t,Gt.x,Gt.y,Gt.z);return this}set(e,t=0){return this.array.set(e,t),this}getComponent(e,t){let n=this.array[e*this.itemSize+t];return this.normalized&&(n=vn(n,this.array)),n}setComponent(e,t,n){return this.normalized&&(n=dt(n,this.array)),this.array[e*this.itemSize+t]=n,this}getX(e){let t=this.array[e*this.itemSize];return this.normalized&&(t=vn(t,this.array)),t}setX(e,t){return this.normalized&&(t=dt(t,this.array)),this.array[e*this.itemSize]=t,this}getY(e){let t=this.array[e*this.itemSize+1];return this.normalized&&(t=vn(t,this.array)),t}setY(e,t){return this.normalized&&(t=dt(t,this.array)),this.array[e*this.itemSize+1]=t,this}getZ(e){let t=this.array[e*this.itemSize+2];return this.normalized&&(t=vn(t,this.array)),t}setZ(e,t){return this.normalized&&(t=dt(t,this.array)),this.array[e*this.itemSize+2]=t,this}getW(e){let t=this.array[e*this.itemSize+3];return this.normalized&&(t=vn(t,this.array)),t}setW(e,t){return this.normalized&&(t=dt(t,this.array)),this.array[e*this.itemSize+3]=t,this}setXY(e,t,n){return e*=this.itemSize,this.normalized&&(t=dt(t,this.array),n=dt(n,this.array)),this.array[e+0]=t,this.array[e+1]=n,this}setXYZ(e,t,n,i){return e*=this.itemSize,this.normalized&&(t=dt(t,this.array),n=dt(n,this.array),i=dt(i,this.array)),this.array[e+0]=t,this.array[e+1]=n,this.array[e+2]=i,this}setXYZW(e,t,n,i,r){return e*=this.itemSize,this.normalized&&(t=dt(t,this.array),n=dt(n,this.array),i=dt(i,this.array),r=dt(r,this.array)),this.array[e+0]=t,this.array[e+1]=n,this.array[e+2]=i,this.array[e+3]=r,this}onUpload(e){return this.onUploadCallback=e,this}clone(){return new this.constructor(this.array,this.itemSize).copy(this)}toJSON(){const e={itemSize:this.itemSize,type:this.array.constructor.name,array:Array.from(this.array),normalized:this.normalized};return this.name!==""&&(e.name=this.name),this.usage!==so&&(e.usage=this.usage),e}}class Fv extends Rt{constructor(e,t,n){super(new Int8Array(e),t,n)}}class Bv extends Rt{constructor(e,t,n){super(new Uint8Array(e),t,n)}}class zv extends Rt{constructor(e,t,n){super(new Uint8ClampedArray(e),t,n)}}class kv extends Rt{constructor(e,t,n){super(new Int16Array(e),t,n)}}class wh extends Rt{constructor(e,t,n){super(new Uint16Array(e),t,n)}}class Vv extends Rt{constructor(e,t,n){super(new Int32Array(e),t,n)}}class Eh extends Rt{constructor(e,t,n){super(new Uint32Array(e),t,n)}}class Hv extends Rt{constructor(e,t,n){super(new Uint16Array(e),t,n),this.isFloat16BufferAttribute=!0}getX(e){let t=zs(this.array[e*this.itemSize]);return this.normalized&&(t=vn(t,this.array)),t}setX(e,t){return this.normalized&&(t=dt(t,this.array)),this.array[e*this.itemSize]=Mn(t),this}getY(e){let t=zs(this.array[e*this.itemSize+1]);return this.normalized&&(t=vn(t,this.array)),t}setY(e,t){return this.normalized&&(t=dt(t,this.array)),this.array[e*this.itemSize+1]=Mn(t),this}getZ(e){let t=zs(this.array[e*this.itemSize+2]);return this.normalized&&(t=vn(t,this.array)),t}setZ(e,t){return this.normalized&&(t=dt(t,this.array)),this.array[e*this.itemSize+2]=Mn(t),this}getW(e){let t=zs(this.array[e*this.itemSize+3]);return this.normalized&&(t=vn(t,this.array)),t}setW(e,t){return this.normalized&&(t=dt(t,this.array)),this.array[e*this.itemSize+3]=Mn(t),this}setXY(e,t,n){return e*=this.itemSize,this.normalized&&(t=dt(t,this.array),n=dt(n,this.array)),this.array[e+0]=Mn(t),this.array[e+1]=Mn(n),this}setXYZ(e,t,n,i){return e*=this.itemSize,this.normalized&&(t=dt(t,this.array),n=dt(n,this.array),i=dt(i,this.array)),this.array[e+0]=Mn(t),this.array[e+1]=Mn(n),this.array[e+2]=Mn(i),this}setXYZW(e,t,n,i,r){return e*=this.itemSize,this.normalized&&(t=dt(t,this.array),n=dt(n,this.array),i=dt(i,this.array),r=dt(r,this.array)),this.array[e+0]=Mn(t),this.array[e+1]=Mn(n),this.array[e+2]=Mn(i),this.array[e+3]=Mn(r),this}}class He extends Rt{constructor(e,t,n){super(new Float32Array(e),t,n)}}let Gv=0;const On=new Ke,Gl=new xt,Hr=new O,En=new yn,Cs=new yn,Qt=new O;class at extends ni{constructor(){super(),this.isBufferGeometry=!0,Object.defineProperty(this,"id",{value:Gv++}),this.uuid=Rn(),this.name="",this.type="BufferGeometry",this.index=null,this.attributes={},this.morphAttributes={},this.morphTargetsRelative=!1,this.groups=[],this.boundingBox=null,this.boundingSphere=null,this.drawRange={start:0,count:1/0},this.userData={}}getIndex(){return this.index}setIndex(e){return Array.isArray(e)?this.index=new(mp(e)?Eh:wh)(e,1):this.index=e,this}getAttribute(e){return this.attributes[e]}setAttribute(e,t){return this.attributes[e]=t,this}deleteAttribute(e){return delete this.attributes[e],this}hasAttribute(e){return this.attributes[e]!==void 0}addGroup(e,t,n=0){this.groups.push({start:e,count:t,materialIndex:n})}clearGroups(){this.groups=[]}setDrawRange(e,t){this.drawRange.start=e,this.drawRange.count=t}applyMatrix4(e){const t=this.attributes.position;t!==void 0&&(t.applyMatrix4(e),t.needsUpdate=!0);const n=this.attributes.normal;if(n!==void 0){const r=new ut().getNormalMatrix(e);n.applyNormalMatrix(r),n.needsUpdate=!0}const i=this.attributes.tangent;return i!==void 0&&(i.transformDirection(e),i.needsUpdate=!0),this.boundingBox!==null&&this.computeBoundingBox(),this.boundingSphere!==null&&this.computeBoundingSphere(),this}applyQuaternion(e){return On.makeRotationFromQuaternion(e),this.applyMatrix4(On),this}rotateX(e){return On.makeRotationX(e),this.applyMatrix4(On),this}rotateY(e){return On.makeRotationY(e),this.applyMatrix4(On),this}rotateZ(e){return On.makeRotationZ(e),this.applyMatrix4(On),this}translate(e,t,n){return On.makeTranslation(e,t,n),this.applyMatrix4(On),this}scale(e,t,n){return On.makeScale(e,t,n),this.applyMatrix4(On),this}lookAt(e){return Gl.lookAt(e),Gl.updateMatrix(),this.applyMatrix4(Gl.matrix),this}center(){return this.computeBoundingBox(),this.boundingBox.getCenter(Hr).negate(),this.translate(Hr.x,Hr.y,Hr.z),this}setFromPoints(e){const t=[];for(let n=0,i=e.length;n<i;n++){const r=e[n];t.push(r.x,r.y,r.z||0)}return this.setAttribute("position",new He(t,3)),this}computeBoundingBox(){this.boundingBox===null&&(this.boundingBox=new yn);const e=this.attributes.position,t=this.morphAttributes.position;if(e&&e.isGLBufferAttribute){console.error("THREE.BufferGeometry.computeBoundingBox(): GLBufferAttribute requires a manual bounding box.",this),this.boundingBox.set(new O(-1/0,-1/0,-1/0),new O(1/0,1/0,1/0));return}if(e!==void 0){if(this.boundingBox.setFromBufferAttribute(e),t)for(let n=0,i=t.length;n<i;n++){const r=t[n];En.setFromBufferAttribute(r),this.morphTargetsRelative?(Qt.addVectors(this.boundingBox.min,En.min),this.boundingBox.expandByPoint(Qt),Qt.addVectors(this.boundingBox.max,En.max),this.boundingBox.expandByPoint(Qt)):(this.boundingBox.expandByPoint(En.min),this.boundingBox.expandByPoint(En.max))}}else this.boundingBox.makeEmpty();(isNaN(this.boundingBox.min.x)||isNaN(this.boundingBox.min.y)||isNaN(this.boundingBox.min.z))&&console.error('THREE.BufferGeometry.computeBoundingBox(): Computed min/max have NaN values. The "position" attribute is likely to have NaN values.',this)}computeBoundingSphere(){this.boundingSphere===null&&(this.boundingSphere=new cn);const e=this.attributes.position,t=this.morphAttributes.position;if(e&&e.isGLBufferAttribute){console.error("THREE.BufferGeometry.computeBoundingSphere(): GLBufferAttribute requires a manual bounding sphere.",this),this.boundingSphere.set(new O,1/0);return}if(e){const n=this.boundingSphere.center;if(En.setFromBufferAttribute(e),t)for(let r=0,o=t.length;r<o;r++){const a=t[r];Cs.setFromBufferAttribute(a),this.morphTargetsRelative?(Qt.addVectors(En.min,Cs.min),En.expandByPoint(Qt),Qt.addVectors(En.max,Cs.max),En.expandByPoint(Qt)):(En.expandByPoint(Cs.min),En.expandByPoint(Cs.max))}En.getCenter(n);let i=0;for(let r=0,o=e.count;r<o;r++)Qt.fromBufferAttribute(e,r),i=Math.max(i,n.distanceToSquared(Qt));if(t)for(let r=0,o=t.length;r<o;r++){const a=t[r],l=this.morphTargetsRelative;for(let u=0,d=a.count;u<d;u++)Qt.fromBufferAttribute(a,u),l&&(Hr.fromBufferAttribute(e,u),Qt.add(Hr)),i=Math.max(i,n.distanceToSquared(Qt))}this.boundingSphere.radius=Math.sqrt(i),isNaN(this.boundingSphere.radius)&&console.error('THREE.BufferGeometry.computeBoundingSphere(): Computed radius is NaN. The "position" attribute is likely to have NaN values.',this)}}computeTangents(){const e=this.index,t=this.attributes;if(e===null||t.position===void 0||t.normal===void 0||t.uv===void 0){console.error("THREE.BufferGeometry: .computeTangents() failed. Missing required attributes (index, position, normal or uv)");return}const n=t.position,i=t.normal,r=t.uv;this.hasAttribute("tangent")===!1&&this.setAttribute("tangent",new Rt(new Float32Array(4*n.count),4));const o=this.getAttribute("tangent"),a=[],l=[];for(let k=0;k<n.count;k++)a[k]=new O,l[k]=new O;const u=new O,d=new O,m=new O,p=new se,g=new se,_=new se,M=new O,y=new O;function v(k,D,C){u.fromBufferAttribute(n,k),d.fromBufferAttribute(n,D),m.fromBufferAttribute(n,C),p.fromBufferAttribute(r,k),g.fromBufferAttribute(r,D),_.fromBufferAttribute(r,C),d.sub(u),m.sub(u),g.sub(p),_.sub(p);const X=1/(g.x*_.y-_.x*g.y);isFinite(X)&&(M.copy(d).multiplyScalar(_.y).addScaledVector(m,-g.y).multiplyScalar(X),y.copy(m).multiplyScalar(g.x).addScaledVector(d,-_.x).multiplyScalar(X),a[k].add(M),a[D].add(M),a[C].add(M),l[k].add(y),l[D].add(y),l[C].add(y))}let w=this.groups;w.length===0&&(w=[{start:0,count:e.count}]);for(let k=0,D=w.length;k<D;++k){const C=w[k],X=C.start,Y=C.count;for(let H=X,K=X+Y;H<K;H+=3)v(e.getX(H+0),e.getX(H+1),e.getX(H+2))}const S=new O,T=new O,z=new O,N=new O;function A(k){z.fromBufferAttribute(i,k),N.copy(z);const D=a[k];S.copy(D),S.sub(z.multiplyScalar(z.dot(D))).normalize(),T.crossVectors(N,D);const X=T.dot(l[k])<0?-1:1;o.setXYZW(k,S.x,S.y,S.z,X)}for(let k=0,D=w.length;k<D;++k){const C=w[k],X=C.start,Y=C.count;for(let H=X,K=X+Y;H<K;H+=3)A(e.getX(H+0)),A(e.getX(H+1)),A(e.getX(H+2))}}computeVertexNormals(){const e=this.index,t=this.getAttribute("position");if(t!==void 0){let n=this.getAttribute("normal");if(n===void 0)n=new Rt(new Float32Array(t.count*3),3),this.setAttribute("normal",n);else for(let p=0,g=n.count;p<g;p++)n.setXYZ(p,0,0,0);const i=new O,r=new O,o=new O,a=new O,l=new O,u=new O,d=new O,m=new O;if(e)for(let p=0,g=e.count;p<g;p+=3){const _=e.getX(p+0),M=e.getX(p+1),y=e.getX(p+2);i.fromBufferAttribute(t,_),r.fromBufferAttribute(t,M),o.fromBufferAttribute(t,y),d.subVectors(o,r),m.subVectors(i,r),d.cross(m),a.fromBufferAttribute(n,_),l.fromBufferAttribute(n,M),u.fromBufferAttribute(n,y),a.add(d),l.add(d),u.add(d),n.setXYZ(_,a.x,a.y,a.z),n.setXYZ(M,l.x,l.y,l.z),n.setXYZ(y,u.x,u.y,u.z)}else for(let p=0,g=t.count;p<g;p+=3)i.fromBufferAttribute(t,p+0),r.fromBufferAttribute(t,p+1),o.fromBufferAttribute(t,p+2),d.subVectors(o,r),m.subVectors(i,r),d.cross(m),n.setXYZ(p+0,d.x,d.y,d.z),n.setXYZ(p+1,d.x,d.y,d.z),n.setXYZ(p+2,d.x,d.y,d.z);this.normalizeNormals(),n.needsUpdate=!0}}normalizeNormals(){const e=this.attributes.normal;for(let t=0,n=e.count;t<n;t++)Qt.fromBufferAttribute(e,t),Qt.normalize(),e.setXYZ(t,Qt.x,Qt.y,Qt.z)}toNonIndexed(){function e(a,l){const u=a.array,d=a.itemSize,m=a.normalized,p=new u.constructor(l.length*d);let g=0,_=0;for(let M=0,y=l.length;M<y;M++){a.isInterleavedBufferAttribute?g=l[M]*a.data.stride+a.offset:g=l[M]*d;for(let v=0;v<d;v++)p[_++]=u[g++]}return new Rt(p,d,m)}if(this.index===null)return console.warn("THREE.BufferGeometry.toNonIndexed(): BufferGeometry is already non-indexed."),this;const t=new at,n=this.index.array,i=this.attributes;for(const a in i){const l=i[a],u=e(l,n);t.setAttribute(a,u)}const r=this.morphAttributes;for(const a in r){const l=[],u=r[a];for(let d=0,m=u.length;d<m;d++){const p=u[d],g=e(p,n);l.push(g)}t.morphAttributes[a]=l}t.morphTargetsRelative=this.morphTargetsRelative;const o=this.groups;for(let a=0,l=o.length;a<l;a++){const u=o[a];t.addGroup(u.start,u.count,u.materialIndex)}return t}toJSON(){const e={metadata:{version:4.6,type:"BufferGeometry",generator:"BufferGeometry.toJSON"}};if(e.uuid=this.uuid,e.type=this.type,this.name!==""&&(e.name=this.name),Object.keys(this.userData).length>0&&(e.userData=this.userData),this.parameters!==void 0){const l=this.parameters;for(const u in l)l[u]!==void 0&&(e[u]=l[u]);return e}e.data={attributes:{}};const t=this.index;t!==null&&(e.data.index={type:t.array.constructor.name,array:Array.prototype.slice.call(t.array)});const n=this.attributes;for(const l in n){const u=n[l];e.data.attributes[l]=u.toJSON(e.data)}const i={};let r=!1;for(const l in this.morphAttributes){const u=this.morphAttributes[l],d=[];for(let m=0,p=u.length;m<p;m++){const g=u[m];d.push(g.toJSON(e.data))}d.length>0&&(i[l]=d,r=!0)}r&&(e.data.morphAttributes=i,e.data.morphTargetsRelative=this.morphTargetsRelative);const o=this.groups;o.length>0&&(e.data.groups=JSON.parse(JSON.stringify(o)));const a=this.boundingSphere;return a!==null&&(e.data.boundingSphere={center:a.center.toArray(),radius:a.radius}),e}clone(){return new this.constructor().copy(this)}copy(e){this.index=null,this.attributes={},this.morphAttributes={},this.groups=[],this.boundingBox=null,this.boundingSphere=null;const t={};this.name=e.name;const n=e.index;n!==null&&this.setIndex(n.clone(t));const i=e.attributes;for(const u in i){const d=i[u];this.setAttribute(u,d.clone(t))}const r=e.morphAttributes;for(const u in r){const d=[],m=r[u];for(let p=0,g=m.length;p<g;p++)d.push(m[p].clone(t));this.morphAttributes[u]=d}this.morphTargetsRelative=e.morphTargetsRelative;const o=e.groups;for(let u=0,d=o.length;u<d;u++){const m=o[u];this.addGroup(m.start,m.count,m.materialIndex)}const a=e.boundingBox;a!==null&&(this.boundingBox=a.clone());const l=e.boundingSphere;return l!==null&&(this.boundingSphere=l.clone()),this.drawRange.start=e.drawRange.start,this.drawRange.count=e.drawRange.count,this.userData=e.userData,this}dispose(){this.dispatchEvent({type:"dispose"})}}const Ou=new Ke,Qi=new Ar,Wo=new cn,Uu=new O,Gr=new O,Wr=new O,Xr=new O,Wl=new O,Xo=new O,Yo=new se,qo=new se,Zo=new se,Fu=new O,Bu=new O,zu=new O,jo=new O,Ko=new O;class Ie extends xt{constructor(e=new at,t=new ii){super(),this.isMesh=!0,this.type="Mesh",this.geometry=e,this.material=t,this.updateMorphTargets()}copy(e,t){return super.copy(e,t),e.morphTargetInfluences!==void 0&&(this.morphTargetInfluences=e.morphTargetInfluences.slice()),e.morphTargetDictionary!==void 0&&(this.morphTargetDictionary=Object.assign({},e.morphTargetDictionary)),this.material=Array.isArray(e.material)?e.material.slice():e.material,this.geometry=e.geometry,this}updateMorphTargets(){const t=this.geometry.morphAttributes,n=Object.keys(t);if(n.length>0){const i=t[n[0]];if(i!==void 0){this.morphTargetInfluences=[],this.morphTargetDictionary={};for(let r=0,o=i.length;r<o;r++){const a=i[r].name||String(r);this.morphTargetInfluences.push(0),this.morphTargetDictionary[a]=r}}}}getVertexPosition(e,t){const n=this.geometry,i=n.attributes.position,r=n.morphAttributes.position,o=n.morphTargetsRelative;t.fromBufferAttribute(i,e);const a=this.morphTargetInfluences;if(r&&a){Xo.set(0,0,0);for(let l=0,u=r.length;l<u;l++){const d=a[l],m=r[l];d!==0&&(Wl.fromBufferAttribute(m,e),o?Xo.addScaledVector(Wl,d):Xo.addScaledVector(Wl.sub(t),d))}t.add(Xo)}return t}raycast(e,t){const n=this.geometry,i=this.material,r=this.matrixWorld;i!==void 0&&(n.boundingSphere===null&&n.computeBoundingSphere(),Wo.copy(n.boundingSphere),Wo.applyMatrix4(r),Qi.copy(e.ray).recast(e.near),!(Wo.containsPoint(Qi.origin)===!1&&(Qi.intersectSphere(Wo,Uu)===null||Qi.origin.distanceToSquared(Uu)>(e.far-e.near)**2))&&(Ou.copy(r).invert(),Qi.copy(e.ray).applyMatrix4(Ou),!(n.boundingBox!==null&&Qi.intersectsBox(n.boundingBox)===!1)&&this._computeIntersections(e,t,Qi)))}_computeIntersections(e,t,n){let i;const r=this.geometry,o=this.material,a=r.index,l=r.attributes.position,u=r.attributes.uv,d=r.attributes.uv1,m=r.attributes.normal,p=r.groups,g=r.drawRange;if(a!==null)if(Array.isArray(o))for(let _=0,M=p.length;_<M;_++){const y=p[_],v=o[y.materialIndex],w=Math.max(y.start,g.start),S=Math.min(a.count,Math.min(y.start+y.count,g.start+g.count));for(let T=w,z=S;T<z;T+=3){const N=a.getX(T),A=a.getX(T+1),k=a.getX(T+2);i=Jo(this,v,e,n,u,d,m,N,A,k),i&&(i.faceIndex=Math.floor(T/3),i.face.materialIndex=y.materialIndex,t.push(i))}}else{const _=Math.max(0,g.start),M=Math.min(a.count,g.start+g.count);for(let y=_,v=M;y<v;y+=3){const w=a.getX(y),S=a.getX(y+1),T=a.getX(y+2);i=Jo(this,o,e,n,u,d,m,w,S,T),i&&(i.faceIndex=Math.floor(y/3),t.push(i))}}else if(l!==void 0)if(Array.isArray(o))for(let _=0,M=p.length;_<M;_++){const y=p[_],v=o[y.materialIndex],w=Math.max(y.start,g.start),S=Math.min(l.count,Math.min(y.start+y.count,g.start+g.count));for(let T=w,z=S;T<z;T+=3){const N=T,A=T+1,k=T+2;i=Jo(this,v,e,n,u,d,m,N,A,k),i&&(i.faceIndex=Math.floor(T/3),i.face.materialIndex=y.materialIndex,t.push(i))}}else{const _=Math.max(0,g.start),M=Math.min(l.count,g.start+g.count);for(let y=_,v=M;y<v;y+=3){const w=y,S=y+1,T=y+2;i=Jo(this,o,e,n,u,d,m,w,S,T),i&&(i.faceIndex=Math.floor(y/3),t.push(i))}}}}function Wv(s,e,t,n,i,r,o,a){let l;if(e.side===_n?l=n.intersectTriangle(o,r,i,!0,a):l=n.intersectTriangle(i,r,o,e.side===xi,a),l===null)return null;Ko.copy(a),Ko.applyMatrix4(s.matrixWorld);const u=t.ray.origin.distanceTo(Ko);return u<t.near||u>t.far?null:{distance:u,point:Ko.clone(),object:s}}function Jo(s,e,t,n,i,r,o,a,l,u){s.getVertexPosition(a,Gr),s.getVertexPosition(l,Wr),s.getVertexPosition(u,Xr);const d=Wv(s,e,t,n,Gr,Wr,Xr,jo);if(d){i&&(Yo.fromBufferAttribute(i,a),qo.fromBufferAttribute(i,l),Zo.fromBufferAttribute(i,u),d.uv=An.getInterpolation(jo,Gr,Wr,Xr,Yo,qo,Zo,new se)),r&&(Yo.fromBufferAttribute(r,a),qo.fromBufferAttribute(r,l),Zo.fromBufferAttribute(r,u),d.uv1=An.getInterpolation(jo,Gr,Wr,Xr,Yo,qo,Zo,new se)),o&&(Fu.fromBufferAttribute(o,a),Bu.fromBufferAttribute(o,l),zu.fromBufferAttribute(o,u),d.normal=An.getInterpolation(jo,Gr,Wr,Xr,Fu,Bu,zu,new O),d.normal.dot(n.direction)>0&&d.normal.multiplyScalar(-1));const m={a,b:l,c:u,normal:new O,materialIndex:0};An.getNormal(Gr,Wr,Xr,m.normal),d.face=m}return d}class zt extends at{constructor(e=1,t=1,n=1,i=1,r=1,o=1){super(),this.type="BoxGeometry",this.parameters={width:e,height:t,depth:n,widthSegments:i,heightSegments:r,depthSegments:o};const a=this;i=Math.floor(i),r=Math.floor(r),o=Math.floor(o);const l=[],u=[],d=[],m=[];let p=0,g=0;_("z","y","x",-1,-1,n,t,e,o,r,0),_("z","y","x",1,-1,n,t,-e,o,r,1),_("x","z","y",1,1,e,n,t,i,o,2),_("x","z","y",1,-1,e,n,-t,i,o,3),_("x","y","z",1,-1,e,t,n,i,r,4),_("x","y","z",-1,-1,e,t,-n,i,r,5),this.setIndex(l),this.setAttribute("position",new He(u,3)),this.setAttribute("normal",new He(d,3)),this.setAttribute("uv",new He(m,2));function _(M,y,v,w,S,T,z,N,A,k,D){const C=T/A,X=z/k,Y=T/2,H=z/2,K=N/2,J=A+1,ce=k+1;let de=0,Q=0;const pe=new O;for(let ve=0;ve<ce;ve++){const Ue=ve*X-H;for(let Je=0;Je<J;Je++){const vt=Je*C-Y;pe[M]=vt*w,pe[y]=Ue*S,pe[v]=K,u.push(pe.x,pe.y,pe.z),pe[M]=0,pe[y]=0,pe[v]=N>0?1:-1,d.push(pe.x,pe.y,pe.z),m.push(Je/A),m.push(1-ve/k),de+=1}}for(let ve=0;ve<k;ve++)for(let Ue=0;Ue<A;Ue++){const Je=p+Ue+J*ve,vt=p+Ue+J*(ve+1),ae=p+(Ue+1)+J*(ve+1),we=p+(Ue+1)+J*ve;l.push(Je,vt,we),l.push(vt,ae,we),Q+=6}a.addGroup(g,Q,D),g+=Q,p+=de}}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}static fromJSON(e){return new zt(e.width,e.height,e.depth,e.widthSegments,e.heightSegments,e.depthSegments)}}function cs(s){const e={};for(const t in s){e[t]={};for(const n in s[t]){const i=s[t][n];i&&(i.isColor||i.isMatrix3||i.isMatrix4||i.isVector2||i.isVector3||i.isVector4||i.isTexture||i.isQuaternion)?i.isRenderTargetTexture?(console.warn("UniformsUtils: Textures of render targets cannot be cloned via cloneUniforms() or mergeUniforms()."),e[t][n]=null):e[t][n]=i.clone():Array.isArray(i)?e[t][n]=i.slice():e[t][n]=i}}return e}function gn(s){const e={};for(let t=0;t<s.length;t++){const n=cs(s[t]);for(const i in n)e[i]=n[i]}return e}function Xv(s){const e=[];for(let t=0;t<s.length;t++)e.push(s[t].clone());return e}function Mp(s){const e=s.getRenderTarget();return e===null?s.outputColorSpace:e.isXRRenderTarget===!0?e.texture.colorSpace:wt.workingColorSpace}const yo={clone:cs,merge:gn};var Yv=`void main() {
	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}`,qv=`void main() {
	gl_FragColor = vec4( 1.0, 0.0, 0.0, 1.0 );
}`;class tn extends hn{constructor(e){super(),this.isShaderMaterial=!0,this.type="ShaderMaterial",this.defines={},this.uniforms={},this.uniformsGroups=[],this.vertexShader=Yv,this.fragmentShader=qv,this.linewidth=1,this.wireframe=!1,this.wireframeLinewidth=1,this.fog=!1,this.lights=!1,this.clipping=!1,this.forceSinglePass=!0,this.extensions={clipCullDistance:!1,multiDraw:!1},this.defaultAttributeValues={color:[1,1,1],uv:[0,0],uv1:[0,0]},this.index0AttributeName=void 0,this.uniformsNeedUpdate=!1,this.glslVersion=null,e!==void 0&&this.setValues(e)}copy(e){return super.copy(e),this.fragmentShader=e.fragmentShader,this.vertexShader=e.vertexShader,this.uniforms=cs(e.uniforms),this.uniformsGroups=Xv(e.uniformsGroups),this.defines=Object.assign({},e.defines),this.wireframe=e.wireframe,this.wireframeLinewidth=e.wireframeLinewidth,this.fog=e.fog,this.lights=e.lights,this.clipping=e.clipping,this.extensions=Object.assign({},e.extensions),this.glslVersion=e.glslVersion,this}toJSON(e){const t=super.toJSON(e);t.glslVersion=this.glslVersion,t.uniforms={};for(const i in this.uniforms){const o=this.uniforms[i].value;o&&o.isTexture?t.uniforms[i]={type:"t",value:o.toJSON(e).uuid}:o&&o.isColor?t.uniforms[i]={type:"c",value:o.getHex()}:o&&o.isVector2?t.uniforms[i]={type:"v2",value:o.toArray()}:o&&o.isVector3?t.uniforms[i]={type:"v3",value:o.toArray()}:o&&o.isVector4?t.uniforms[i]={type:"v4",value:o.toArray()}:o&&o.isMatrix3?t.uniforms[i]={type:"m3",value:o.toArray()}:o&&o.isMatrix4?t.uniforms[i]={type:"m4",value:o.toArray()}:t.uniforms[i]={value:o}}Object.keys(this.defines).length>0&&(t.defines=this.defines),t.vertexShader=this.vertexShader,t.fragmentShader=this.fragmentShader,t.lights=this.lights,t.clipping=this.clipping;const n={};for(const i in this.extensions)this.extensions[i]===!0&&(n[i]=!0);return Object.keys(n).length>0&&(t.extensions=n),t}}class Ja extends xt{constructor(){super(),this.isCamera=!0,this.type="Camera",this.matrixWorldInverse=new Ke,this.projectionMatrix=new Ke,this.projectionMatrixInverse=new Ke,this.coordinateSystem=ei}copy(e,t){return super.copy(e,t),this.matrixWorldInverse.copy(e.matrixWorldInverse),this.projectionMatrix.copy(e.projectionMatrix),this.projectionMatrixInverse.copy(e.projectionMatrixInverse),this.coordinateSystem=e.coordinateSystem,this}getWorldDirection(e){return super.getWorldDirection(e).negate()}updateMatrixWorld(e){super.updateMatrixWorld(e),this.matrixWorldInverse.copy(this.matrixWorld).invert()}updateWorldMatrix(e,t){super.updateWorldMatrix(e,t),this.matrixWorldInverse.copy(this.matrixWorld).invert()}clone(){return new this.constructor().copy(this)}}const Oi=new O,ku=new se,Vu=new se;class en extends Ja{constructor(e=50,t=1,n=.1,i=2e3){super(),this.isPerspectiveCamera=!0,this.type="PerspectiveCamera",this.fov=e,this.zoom=1,this.near=n,this.far=i,this.focus=10,this.aspect=t,this.view=null,this.filmGauge=35,this.filmOffset=0,this.updateProjectionMatrix()}copy(e,t){return super.copy(e,t),this.fov=e.fov,this.zoom=e.zoom,this.near=e.near,this.far=e.far,this.focus=e.focus,this.aspect=e.aspect,this.view=e.view===null?null:Object.assign({},e.view),this.filmGauge=e.filmGauge,this.filmOffset=e.filmOffset,this}setFocalLength(e){const t=.5*this.getFilmHeight()/e;this.fov=ls*2*Math.atan(t),this.updateProjectionMatrix()}getFocalLength(){const e=Math.tan(br*.5*this.fov);return .5*this.getFilmHeight()/e}getEffectiveFOV(){return ls*2*Math.atan(Math.tan(br*.5*this.fov)/this.zoom)}getFilmWidth(){return this.filmGauge*Math.min(this.aspect,1)}getFilmHeight(){return this.filmGauge/Math.max(this.aspect,1)}getViewBounds(e,t,n){Oi.set(-1,-1,.5).applyMatrix4(this.projectionMatrixInverse),t.set(Oi.x,Oi.y).multiplyScalar(-e/Oi.z),Oi.set(1,1,.5).applyMatrix4(this.projectionMatrixInverse),n.set(Oi.x,Oi.y).multiplyScalar(-e/Oi.z)}getViewSize(e,t){return this.getViewBounds(e,ku,Vu),t.subVectors(Vu,ku)}setViewOffset(e,t,n,i,r,o){this.aspect=e/t,this.view===null&&(this.view={enabled:!0,fullWidth:1,fullHeight:1,offsetX:0,offsetY:0,width:1,height:1}),this.view.enabled=!0,this.view.fullWidth=e,this.view.fullHeight=t,this.view.offsetX=n,this.view.offsetY=i,this.view.width=r,this.view.height=o,this.updateProjectionMatrix()}clearViewOffset(){this.view!==null&&(this.view.enabled=!1),this.updateProjectionMatrix()}updateProjectionMatrix(){const e=this.near;let t=e*Math.tan(br*.5*this.fov)/this.zoom,n=2*t,i=this.aspect*n,r=-.5*i;const o=this.view;if(this.view!==null&&this.view.enabled){const l=o.fullWidth,u=o.fullHeight;r+=o.offsetX*i/l,t-=o.offsetY*n/u,i*=o.width/l,n*=o.height/u}const a=this.filmOffset;a!==0&&(r+=e*a/this.getFilmWidth()),this.projectionMatrix.makePerspective(r,r+i,t,t-n,e,this.far,this.coordinateSystem),this.projectionMatrixInverse.copy(this.projectionMatrix).invert()}toJSON(e){const t=super.toJSON(e);return t.object.fov=this.fov,t.object.zoom=this.zoom,t.object.near=this.near,t.object.far=this.far,t.object.focus=this.focus,t.object.aspect=this.aspect,this.view!==null&&(t.object.view=Object.assign({},this.view)),t.object.filmGauge=this.filmGauge,t.object.filmOffset=this.filmOffset,t}}const Yr=-90,qr=1;class bp extends xt{constructor(e,t,n){super(),this.type="CubeCamera",this.renderTarget=n,this.coordinateSystem=null,this.activeMipmapLevel=0;const i=new en(Yr,qr,e,t);i.layers=this.layers,this.add(i);const r=new en(Yr,qr,e,t);r.layers=this.layers,this.add(r);const o=new en(Yr,qr,e,t);o.layers=this.layers,this.add(o);const a=new en(Yr,qr,e,t);a.layers=this.layers,this.add(a);const l=new en(Yr,qr,e,t);l.layers=this.layers,this.add(l);const u=new en(Yr,qr,e,t);u.layers=this.layers,this.add(u)}updateCoordinateSystem(){const e=this.coordinateSystem,t=this.children.concat(),[n,i,r,o,a,l]=t;for(const u of t)this.remove(u);if(e===ei)n.up.set(0,1,0),n.lookAt(1,0,0),i.up.set(0,1,0),i.lookAt(-1,0,0),r.up.set(0,0,-1),r.lookAt(0,1,0),o.up.set(0,0,1),o.lookAt(0,-1,0),a.up.set(0,1,0),a.lookAt(0,0,1),l.up.set(0,1,0),l.lookAt(0,0,-1);else if(e===oo)n.up.set(0,-1,0),n.lookAt(-1,0,0),i.up.set(0,-1,0),i.lookAt(1,0,0),r.up.set(0,0,1),r.lookAt(0,1,0),o.up.set(0,0,-1),o.lookAt(0,-1,0),a.up.set(0,-1,0),a.lookAt(0,0,1),l.up.set(0,-1,0),l.lookAt(0,0,-1);else throw new Error("THREE.CubeCamera.updateCoordinateSystem(): Invalid coordinate system: "+e);for(const u of t)this.add(u),u.updateMatrixWorld()}update(e,t){this.parent===null&&this.updateMatrixWorld();const{renderTarget:n,activeMipmapLevel:i}=this;this.coordinateSystem!==e.coordinateSystem&&(this.coordinateSystem=e.coordinateSystem,this.updateCoordinateSystem());const[r,o,a,l,u,d]=this.children,m=e.getRenderTarget(),p=e.getActiveCubeFace(),g=e.getActiveMipmapLevel(),_=e.xr.enabled;e.xr.enabled=!1;const M=n.texture.generateMipmaps;n.texture.generateMipmaps=!1,e.setRenderTarget(n,0,i),e.render(t,r),e.setRenderTarget(n,1,i),e.render(t,o),e.setRenderTarget(n,2,i),e.render(t,a),e.setRenderTarget(n,3,i),e.render(t,l),e.setRenderTarget(n,4,i),e.render(t,u),n.texture.generateMipmaps=M,e.setRenderTarget(n,5,i),e.render(t,d),e.setRenderTarget(m,p,g),e.xr.enabled=_,n.texture.needsPMREMUpdate=!0}}class xo extends Vt{constructor(e,t,n,i,r,o,a,l,u,d){e=e!==void 0?e:[],t=t!==void 0?t:Mi,super(e,t,n,i,r,o,a,l,u,d),this.isCubeTexture=!0,this.flipY=!1}get images(){return this.image}set images(e){this.image=e}}class Sp extends qt{constructor(e=1,t={}){super(e,e,t),this.isWebGLCubeRenderTarget=!0;const n={width:e,height:e,depth:1},i=[n,n,n,n,n,n];this.texture=new xo(i,t.mapping,t.wrapS,t.wrapT,t.magFilter,t.minFilter,t.format,t.type,t.anisotropy,t.colorSpace),this.texture.isRenderTargetTexture=!0,this.texture.generateMipmaps=t.generateMipmaps!==void 0?t.generateMipmaps:!1,this.texture.minFilter=t.minFilter!==void 0?t.minFilter:Xt}fromEquirectangularTexture(e,t){this.texture.type=t.type,this.texture.colorSpace=t.colorSpace,this.texture.generateMipmaps=t.generateMipmaps,this.texture.minFilter=t.minFilter,this.texture.magFilter=t.magFilter;const n={uniforms:{tEquirect:{value:null}},vertexShader:`

				varying vec3 vWorldDirection;

				vec3 transformDirection( in vec3 dir, in mat4 matrix ) {

					return normalize( ( matrix * vec4( dir, 0.0 ) ).xyz );

				}

				void main() {

					vWorldDirection = transformDirection( position, modelMatrix );

					#include <begin_vertex>
					#include <project_vertex>

				}
			`,fragmentShader:`

				uniform sampler2D tEquirect;

				varying vec3 vWorldDirection;

				#include <common>

				void main() {

					vec3 direction = normalize( vWorldDirection );

					vec2 sampleUV = equirectUv( direction );

					gl_FragColor = texture2D( tEquirect, sampleUV );

				}
			`},i=new zt(5,5,5),r=new tn({name:"CubemapFromEquirect",uniforms:cs(n.uniforms),vertexShader:n.vertexShader,fragmentShader:n.fragmentShader,side:_n,blending:kn});r.uniforms.tEquirect.value=t;const o=new Ie(i,r),a=t.minFilter;return t.minFilter===Qn&&(t.minFilter=Xt),new bp(1,10,this).update(e,o),t.minFilter=a,o.geometry.dispose(),o.material.dispose(),this}clear(e,t,n,i){const r=e.getRenderTarget();for(let o=0;o<6;o++)e.setRenderTarget(this,o),e.clear(t,n,i);e.setRenderTarget(r)}}const Xl=new O,Zv=new O,jv=new ut;class $n{constructor(e=new O(1,0,0),t=0){this.isPlane=!0,this.normal=e,this.constant=t}set(e,t){return this.normal.copy(e),this.constant=t,this}setComponents(e,t,n,i){return this.normal.set(e,t,n),this.constant=i,this}setFromNormalAndCoplanarPoint(e,t){return this.normal.copy(e),this.constant=-t.dot(this.normal),this}setFromCoplanarPoints(e,t,n){const i=Xl.subVectors(n,t).cross(Zv.subVectors(e,t)).normalize();return this.setFromNormalAndCoplanarPoint(i,e),this}copy(e){return this.normal.copy(e.normal),this.constant=e.constant,this}normalize(){const e=1/this.normal.length();return this.normal.multiplyScalar(e),this.constant*=e,this}negate(){return this.constant*=-1,this.normal.negate(),this}distanceToPoint(e){return this.normal.dot(e)+this.constant}distanceToSphere(e){return this.distanceToPoint(e.center)-e.radius}projectPoint(e,t){return t.copy(e).addScaledVector(this.normal,-this.distanceToPoint(e))}intersectLine(e,t){const n=e.delta(Xl),i=this.normal.dot(n);if(i===0)return this.distanceToPoint(e.start)===0?t.copy(e.start):null;const r=-(e.start.dot(this.normal)+this.constant)/i;return r<0||r>1?null:t.copy(e.start).addScaledVector(n,r)}intersectsLine(e){const t=this.distanceToPoint(e.start),n=this.distanceToPoint(e.end);return t<0&&n>0||n<0&&t>0}intersectsBox(e){return e.intersectsPlane(this)}intersectsSphere(e){return e.intersectsPlane(this)}coplanarPoint(e){return e.copy(this.normal).multiplyScalar(-this.constant)}applyMatrix4(e,t){const n=t||jv.getNormalMatrix(e),i=this.coplanarPoint(Xl).applyMatrix4(e),r=this.normal.applyMatrix3(n).normalize();return this.constant=-i.dot(r),this}translate(e){return this.constant-=e.dot(this.normal),this}equals(e){return e.normal.equals(this.normal)&&e.constant===this.constant}clone(){return new this.constructor().copy(this)}}const er=new cn,$o=new O;class Mo{constructor(e=new $n,t=new $n,n=new $n,i=new $n,r=new $n,o=new $n){this.planes=[e,t,n,i,r,o]}set(e,t,n,i,r,o){const a=this.planes;return a[0].copy(e),a[1].copy(t),a[2].copy(n),a[3].copy(i),a[4].copy(r),a[5].copy(o),this}copy(e){const t=this.planes;for(let n=0;n<6;n++)t[n].copy(e.planes[n]);return this}setFromProjectionMatrix(e,t=ei){const n=this.planes,i=e.elements,r=i[0],o=i[1],a=i[2],l=i[3],u=i[4],d=i[5],m=i[6],p=i[7],g=i[8],_=i[9],M=i[10],y=i[11],v=i[12],w=i[13],S=i[14],T=i[15];if(n[0].setComponents(l-r,p-u,y-g,T-v).normalize(),n[1].setComponents(l+r,p+u,y+g,T+v).normalize(),n[2].setComponents(l+o,p+d,y+_,T+w).normalize(),n[3].setComponents(l-o,p-d,y-_,T-w).normalize(),n[4].setComponents(l-a,p-m,y-M,T-S).normalize(),t===ei)n[5].setComponents(l+a,p+m,y+M,T+S).normalize();else if(t===oo)n[5].setComponents(a,m,M,S).normalize();else throw new Error("THREE.Frustum.setFromProjectionMatrix(): Invalid coordinate system: "+t);return this}intersectsObject(e){if(e.boundingSphere!==void 0)e.boundingSphere===null&&e.computeBoundingSphere(),er.copy(e.boundingSphere).applyMatrix4(e.matrixWorld);else{const t=e.geometry;t.boundingSphere===null&&t.computeBoundingSphere(),er.copy(t.boundingSphere).applyMatrix4(e.matrixWorld)}return this.intersectsSphere(er)}intersectsSprite(e){return er.center.set(0,0,0),er.radius=.7071067811865476,er.applyMatrix4(e.matrixWorld),this.intersectsSphere(er)}intersectsSphere(e){const t=this.planes,n=e.center,i=-e.radius;for(let r=0;r<6;r++)if(t[r].distanceToPoint(n)<i)return!1;return!0}intersectsBox(e){const t=this.planes;for(let n=0;n<6;n++){const i=t[n];if($o.x=i.normal.x>0?e.max.x:e.min.x,$o.y=i.normal.y>0?e.max.y:e.min.y,$o.z=i.normal.z>0?e.max.z:e.min.z,i.distanceToPoint($o)<0)return!1}return!0}containsPoint(e){const t=this.planes;for(let n=0;n<6;n++)if(t[n].distanceToPoint(e)<0)return!1;return!0}clone(){return new this.constructor().copy(this)}}function wp(){let s=null,e=!1,t=null,n=null;function i(r,o){t(r,o),n=s.requestAnimationFrame(i)}return{start:function(){e!==!0&&t!==null&&(n=s.requestAnimationFrame(i),e=!0)},stop:function(){s.cancelAnimationFrame(n),e=!1},setAnimationLoop:function(r){t=r},setContext:function(r){s=r}}}function Kv(s){const e=new WeakMap;function t(a,l){const u=a.array,d=a.usage,m=u.byteLength,p=s.createBuffer();s.bindBuffer(l,p),s.bufferData(l,u,d),a.onUploadCallback();let g;if(u instanceof Float32Array)g=s.FLOAT;else if(u instanceof Uint16Array)a.isFloat16BufferAttribute?g=s.HALF_FLOAT:g=s.UNSIGNED_SHORT;else if(u instanceof Int16Array)g=s.SHORT;else if(u instanceof Uint32Array)g=s.UNSIGNED_INT;else if(u instanceof Int32Array)g=s.INT;else if(u instanceof Int8Array)g=s.BYTE;else if(u instanceof Uint8Array)g=s.UNSIGNED_BYTE;else if(u instanceof Uint8ClampedArray)g=s.UNSIGNED_BYTE;else throw new Error("THREE.WebGLAttributes: Unsupported buffer data format: "+u);return{buffer:p,type:g,bytesPerElement:u.BYTES_PER_ELEMENT,version:a.version,size:m}}function n(a,l,u){const d=l.array,m=l._updateRange,p=l.updateRanges;if(s.bindBuffer(u,a),m.count===-1&&p.length===0&&s.bufferSubData(u,0,d),p.length!==0){for(let g=0,_=p.length;g<_;g++){const M=p[g];s.bufferSubData(u,M.start*d.BYTES_PER_ELEMENT,d,M.start,M.count)}l.clearUpdateRanges()}m.count!==-1&&(s.bufferSubData(u,m.offset*d.BYTES_PER_ELEMENT,d,m.offset,m.count),m.count=-1),l.onUploadCallback()}function i(a){return a.isInterleavedBufferAttribute&&(a=a.data),e.get(a)}function r(a){a.isInterleavedBufferAttribute&&(a=a.data);const l=e.get(a);l&&(s.deleteBuffer(l.buffer),e.delete(a))}function o(a,l){if(a.isGLBufferAttribute){const d=e.get(a);(!d||d.version<a.version)&&e.set(a,{buffer:a.buffer,type:a.type,bytesPerElement:a.elementSize,version:a.version});return}a.isInterleavedBufferAttribute&&(a=a.data);const u=e.get(a);if(u===void 0)e.set(a,t(a,l));else if(u.version<a.version){if(u.size!==a.array.byteLength)throw new Error("THREE.WebGLAttributes: The size of the buffer attribute's array buffer does not match the original size. Resizing buffer attributes is not supported.");n(u.buffer,a,l),u.version=a.version}}return{get:i,remove:r,update:o}}class Cr extends at{constructor(e=1,t=1,n=1,i=1){super(),this.type="PlaneGeometry",this.parameters={width:e,height:t,widthSegments:n,heightSegments:i};const r=e/2,o=t/2,a=Math.floor(n),l=Math.floor(i),u=a+1,d=l+1,m=e/a,p=t/l,g=[],_=[],M=[],y=[];for(let v=0;v<d;v++){const w=v*p-o;for(let S=0;S<u;S++){const T=S*m-r;_.push(T,-w,0),M.push(0,0,1),y.push(S/a),y.push(1-v/l)}}for(let v=0;v<l;v++)for(let w=0;w<a;w++){const S=w+u*v,T=w+u*(v+1),z=w+1+u*(v+1),N=w+1+u*v;g.push(S,T,N),g.push(T,z,N)}this.setIndex(g),this.setAttribute("position",new He(_,3)),this.setAttribute("normal",new He(M,3)),this.setAttribute("uv",new He(y,2))}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}static fromJSON(e){return new Cr(e.width,e.height,e.widthSegments,e.heightSegments)}}var Jv=`#ifdef USE_ALPHAHASH
	if ( diffuseColor.a < getAlphaHashThreshold( vPosition ) ) discard;
#endif`,$v=`#ifdef USE_ALPHAHASH
	const float ALPHA_HASH_SCALE = 0.05;
	float hash2D( vec2 value ) {
		return fract( 1.0e4 * sin( 17.0 * value.x + 0.1 * value.y ) * ( 0.1 + abs( sin( 13.0 * value.y + value.x ) ) ) );
	}
	float hash3D( vec3 value ) {
		return hash2D( vec2( hash2D( value.xy ), value.z ) );
	}
	float getAlphaHashThreshold( vec3 position ) {
		float maxDeriv = max(
			length( dFdx( position.xyz ) ),
			length( dFdy( position.xyz ) )
		);
		float pixScale = 1.0 / ( ALPHA_HASH_SCALE * maxDeriv );
		vec2 pixScales = vec2(
			exp2( floor( log2( pixScale ) ) ),
			exp2( ceil( log2( pixScale ) ) )
		);
		vec2 alpha = vec2(
			hash3D( floor( pixScales.x * position.xyz ) ),
			hash3D( floor( pixScales.y * position.xyz ) )
		);
		float lerpFactor = fract( log2( pixScale ) );
		float x = ( 1.0 - lerpFactor ) * alpha.x + lerpFactor * alpha.y;
		float a = min( lerpFactor, 1.0 - lerpFactor );
		vec3 cases = vec3(
			x * x / ( 2.0 * a * ( 1.0 - a ) ),
			( x - 0.5 * a ) / ( 1.0 - a ),
			1.0 - ( ( 1.0 - x ) * ( 1.0 - x ) / ( 2.0 * a * ( 1.0 - a ) ) )
		);
		float threshold = ( x < ( 1.0 - a ) )
			? ( ( x < a ) ? cases.x : cases.y )
			: cases.z;
		return clamp( threshold , 1.0e-6, 1.0 );
	}
#endif`,Qv=`#ifdef USE_ALPHAMAP
	diffuseColor.a *= texture2D( alphaMap, vAlphaMapUv ).g;
#endif`,e0=`#ifdef USE_ALPHAMAP
	uniform sampler2D alphaMap;
#endif`,t0=`#ifdef USE_ALPHATEST
	#ifdef ALPHA_TO_COVERAGE
	diffuseColor.a = smoothstep( alphaTest, alphaTest + fwidth( diffuseColor.a ), diffuseColor.a );
	if ( diffuseColor.a == 0.0 ) discard;
	#else
	if ( diffuseColor.a < alphaTest ) discard;
	#endif
#endif`,n0=`#ifdef USE_ALPHATEST
	uniform float alphaTest;
#endif`,i0=`#ifdef USE_AOMAP
	float ambientOcclusion = ( texture2D( aoMap, vAoMapUv ).r - 1.0 ) * aoMapIntensity + 1.0;
	reflectedLight.indirectDiffuse *= ambientOcclusion;
	#if defined( USE_CLEARCOAT ) 
		clearcoatSpecularIndirect *= ambientOcclusion;
	#endif
	#if defined( USE_SHEEN ) 
		sheenSpecularIndirect *= ambientOcclusion;
	#endif
	#if defined( USE_ENVMAP ) && defined( STANDARD )
		float dotNV = saturate( dot( geometryNormal, geometryViewDir ) );
		reflectedLight.indirectSpecular *= computeSpecularOcclusion( dotNV, ambientOcclusion, material.roughness );
	#endif
#endif`,r0=`#ifdef USE_AOMAP
	uniform sampler2D aoMap;
	uniform float aoMapIntensity;
#endif`,s0=`#ifdef USE_BATCHING
	attribute float batchId;
	uniform highp sampler2D batchingTexture;
	mat4 getBatchingMatrix( const in float i ) {
		int size = textureSize( batchingTexture, 0 ).x;
		int j = int( i ) * 4;
		int x = j % size;
		int y = j / size;
		vec4 v1 = texelFetch( batchingTexture, ivec2( x, y ), 0 );
		vec4 v2 = texelFetch( batchingTexture, ivec2( x + 1, y ), 0 );
		vec4 v3 = texelFetch( batchingTexture, ivec2( x + 2, y ), 0 );
		vec4 v4 = texelFetch( batchingTexture, ivec2( x + 3, y ), 0 );
		return mat4( v1, v2, v3, v4 );
	}
#endif`,o0=`#ifdef USE_BATCHING
	mat4 batchingMatrix = getBatchingMatrix( batchId );
#endif`,a0=`vec3 transformed = vec3( position );
#ifdef USE_ALPHAHASH
	vPosition = vec3( position );
#endif`,l0=`vec3 objectNormal = vec3( normal );
#ifdef USE_TANGENT
	vec3 objectTangent = vec3( tangent.xyz );
#endif`,c0=`float G_BlinnPhong_Implicit( ) {
	return 0.25;
}
float D_BlinnPhong( const in float shininess, const in float dotNH ) {
	return RECIPROCAL_PI * ( shininess * 0.5 + 1.0 ) * pow( dotNH, shininess );
}
vec3 BRDF_BlinnPhong( const in vec3 lightDir, const in vec3 viewDir, const in vec3 normal, const in vec3 specularColor, const in float shininess ) {
	vec3 halfDir = normalize( lightDir + viewDir );
	float dotNH = saturate( dot( normal, halfDir ) );
	float dotVH = saturate( dot( viewDir, halfDir ) );
	vec3 F = F_Schlick( specularColor, 1.0, dotVH );
	float G = G_BlinnPhong_Implicit( );
	float D = D_BlinnPhong( shininess, dotNH );
	return F * ( G * D );
} // validated`,h0=`#ifdef USE_IRIDESCENCE
	const mat3 XYZ_TO_REC709 = mat3(
		 3.2404542, -0.9692660,  0.0556434,
		-1.5371385,  1.8760108, -0.2040259,
		-0.4985314,  0.0415560,  1.0572252
	);
	vec3 Fresnel0ToIor( vec3 fresnel0 ) {
		vec3 sqrtF0 = sqrt( fresnel0 );
		return ( vec3( 1.0 ) + sqrtF0 ) / ( vec3( 1.0 ) - sqrtF0 );
	}
	vec3 IorToFresnel0( vec3 transmittedIor, float incidentIor ) {
		return pow2( ( transmittedIor - vec3( incidentIor ) ) / ( transmittedIor + vec3( incidentIor ) ) );
	}
	float IorToFresnel0( float transmittedIor, float incidentIor ) {
		return pow2( ( transmittedIor - incidentIor ) / ( transmittedIor + incidentIor ));
	}
	vec3 evalSensitivity( float OPD, vec3 shift ) {
		float phase = 2.0 * PI * OPD * 1.0e-9;
		vec3 val = vec3( 5.4856e-13, 4.4201e-13, 5.2481e-13 );
		vec3 pos = vec3( 1.6810e+06, 1.7953e+06, 2.2084e+06 );
		vec3 var = vec3( 4.3278e+09, 9.3046e+09, 6.6121e+09 );
		vec3 xyz = val * sqrt( 2.0 * PI * var ) * cos( pos * phase + shift ) * exp( - pow2( phase ) * var );
		xyz.x += 9.7470e-14 * sqrt( 2.0 * PI * 4.5282e+09 ) * cos( 2.2399e+06 * phase + shift[ 0 ] ) * exp( - 4.5282e+09 * pow2( phase ) );
		xyz /= 1.0685e-7;
		vec3 rgb = XYZ_TO_REC709 * xyz;
		return rgb;
	}
	vec3 evalIridescence( float outsideIOR, float eta2, float cosTheta1, float thinFilmThickness, vec3 baseF0 ) {
		vec3 I;
		float iridescenceIOR = mix( outsideIOR, eta2, smoothstep( 0.0, 0.03, thinFilmThickness ) );
		float sinTheta2Sq = pow2( outsideIOR / iridescenceIOR ) * ( 1.0 - pow2( cosTheta1 ) );
		float cosTheta2Sq = 1.0 - sinTheta2Sq;
		if ( cosTheta2Sq < 0.0 ) {
			return vec3( 1.0 );
		}
		float cosTheta2 = sqrt( cosTheta2Sq );
		float R0 = IorToFresnel0( iridescenceIOR, outsideIOR );
		float R12 = F_Schlick( R0, 1.0, cosTheta1 );
		float T121 = 1.0 - R12;
		float phi12 = 0.0;
		if ( iridescenceIOR < outsideIOR ) phi12 = PI;
		float phi21 = PI - phi12;
		vec3 baseIOR = Fresnel0ToIor( clamp( baseF0, 0.0, 0.9999 ) );		vec3 R1 = IorToFresnel0( baseIOR, iridescenceIOR );
		vec3 R23 = F_Schlick( R1, 1.0, cosTheta2 );
		vec3 phi23 = vec3( 0.0 );
		if ( baseIOR[ 0 ] < iridescenceIOR ) phi23[ 0 ] = PI;
		if ( baseIOR[ 1 ] < iridescenceIOR ) phi23[ 1 ] = PI;
		if ( baseIOR[ 2 ] < iridescenceIOR ) phi23[ 2 ] = PI;
		float OPD = 2.0 * iridescenceIOR * thinFilmThickness * cosTheta2;
		vec3 phi = vec3( phi21 ) + phi23;
		vec3 R123 = clamp( R12 * R23, 1e-5, 0.9999 );
		vec3 r123 = sqrt( R123 );
		vec3 Rs = pow2( T121 ) * R23 / ( vec3( 1.0 ) - R123 );
		vec3 C0 = R12 + Rs;
		I = C0;
		vec3 Cm = Rs - T121;
		for ( int m = 1; m <= 2; ++ m ) {
			Cm *= r123;
			vec3 Sm = 2.0 * evalSensitivity( float( m ) * OPD, float( m ) * phi );
			I += Cm * Sm;
		}
		return max( I, vec3( 0.0 ) );
	}
#endif`,u0=`#ifdef USE_BUMPMAP
	uniform sampler2D bumpMap;
	uniform float bumpScale;
	vec2 dHdxy_fwd() {
		vec2 dSTdx = dFdx( vBumpMapUv );
		vec2 dSTdy = dFdy( vBumpMapUv );
		float Hll = bumpScale * texture2D( bumpMap, vBumpMapUv ).x;
		float dBx = bumpScale * texture2D( bumpMap, vBumpMapUv + dSTdx ).x - Hll;
		float dBy = bumpScale * texture2D( bumpMap, vBumpMapUv + dSTdy ).x - Hll;
		return vec2( dBx, dBy );
	}
	vec3 perturbNormalArb( vec3 surf_pos, vec3 surf_norm, vec2 dHdxy, float faceDirection ) {
		vec3 vSigmaX = normalize( dFdx( surf_pos.xyz ) );
		vec3 vSigmaY = normalize( dFdy( surf_pos.xyz ) );
		vec3 vN = surf_norm;
		vec3 R1 = cross( vSigmaY, vN );
		vec3 R2 = cross( vN, vSigmaX );
		float fDet = dot( vSigmaX, R1 ) * faceDirection;
		vec3 vGrad = sign( fDet ) * ( dHdxy.x * R1 + dHdxy.y * R2 );
		return normalize( abs( fDet ) * surf_norm - vGrad );
	}
#endif`,d0=`#if NUM_CLIPPING_PLANES > 0
	vec4 plane;
	#ifdef ALPHA_TO_COVERAGE
		float distanceToPlane, distanceGradient;
		float clipOpacity = 1.0;
		#pragma unroll_loop_start
		for ( int i = 0; i < UNION_CLIPPING_PLANES; i ++ ) {
			plane = clippingPlanes[ i ];
			distanceToPlane = - dot( vClipPosition, plane.xyz ) + plane.w;
			distanceGradient = fwidth( distanceToPlane ) / 2.0;
			clipOpacity *= smoothstep( - distanceGradient, distanceGradient, distanceToPlane );
			if ( clipOpacity == 0.0 ) discard;
		}
		#pragma unroll_loop_end
		#if UNION_CLIPPING_PLANES < NUM_CLIPPING_PLANES
			float unionClipOpacity = 1.0;
			#pragma unroll_loop_start
			for ( int i = UNION_CLIPPING_PLANES; i < NUM_CLIPPING_PLANES; i ++ ) {
				plane = clippingPlanes[ i ];
				distanceToPlane = - dot( vClipPosition, plane.xyz ) + plane.w;
				distanceGradient = fwidth( distanceToPlane ) / 2.0;
				unionClipOpacity *= 1.0 - smoothstep( - distanceGradient, distanceGradient, distanceToPlane );
			}
			#pragma unroll_loop_end
			clipOpacity *= 1.0 - unionClipOpacity;
		#endif
		diffuseColor.a *= clipOpacity;
		if ( diffuseColor.a == 0.0 ) discard;
	#else
		#pragma unroll_loop_start
		for ( int i = 0; i < UNION_CLIPPING_PLANES; i ++ ) {
			plane = clippingPlanes[ i ];
			if ( dot( vClipPosition, plane.xyz ) > plane.w ) discard;
		}
		#pragma unroll_loop_end
		#if UNION_CLIPPING_PLANES < NUM_CLIPPING_PLANES
			bool clipped = true;
			#pragma unroll_loop_start
			for ( int i = UNION_CLIPPING_PLANES; i < NUM_CLIPPING_PLANES; i ++ ) {
				plane = clippingPlanes[ i ];
				clipped = ( dot( vClipPosition, plane.xyz ) > plane.w ) && clipped;
			}
			#pragma unroll_loop_end
			if ( clipped ) discard;
		#endif
	#endif
#endif`,f0=`#if NUM_CLIPPING_PLANES > 0
	varying vec3 vClipPosition;
	uniform vec4 clippingPlanes[ NUM_CLIPPING_PLANES ];
#endif`,p0=`#if NUM_CLIPPING_PLANES > 0
	varying vec3 vClipPosition;
#endif`,m0=`#if NUM_CLIPPING_PLANES > 0
	vClipPosition = - mvPosition.xyz;
#endif`,g0=`#if defined( USE_COLOR_ALPHA )
	diffuseColor *= vColor;
#elif defined( USE_COLOR )
	diffuseColor.rgb *= vColor;
#endif`,v0=`#if defined( USE_COLOR_ALPHA )
	varying vec4 vColor;
#elif defined( USE_COLOR )
	varying vec3 vColor;
#endif`,_0=`#if defined( USE_COLOR_ALPHA )
	varying vec4 vColor;
#elif defined( USE_COLOR ) || defined( USE_INSTANCING_COLOR )
	varying vec3 vColor;
#endif`,y0=`#if defined( USE_COLOR_ALPHA )
	vColor = vec4( 1.0 );
#elif defined( USE_COLOR ) || defined( USE_INSTANCING_COLOR )
	vColor = vec3( 1.0 );
#endif
#ifdef USE_COLOR
	vColor *= color;
#endif
#ifdef USE_INSTANCING_COLOR
	vColor.xyz *= instanceColor.xyz;
#endif`,x0=`#define PI 3.141592653589793
#define PI2 6.283185307179586
#define PI_HALF 1.5707963267948966
#define RECIPROCAL_PI 0.3183098861837907
#define RECIPROCAL_PI2 0.15915494309189535
#define EPSILON 1e-6
#ifndef saturate
#define saturate( a ) clamp( a, 0.0, 1.0 )
#endif
#define whiteComplement( a ) ( 1.0 - saturate( a ) )
float pow2( const in float x ) { return x*x; }
vec3 pow2( const in vec3 x ) { return x*x; }
float pow3( const in float x ) { return x*x*x; }
float pow4( const in float x ) { float x2 = x*x; return x2*x2; }
float max3( const in vec3 v ) { return max( max( v.x, v.y ), v.z ); }
float average( const in vec3 v ) { return dot( v, vec3( 0.3333333 ) ); }
highp float rand( const in vec2 uv ) {
	const highp float a = 12.9898, b = 78.233, c = 43758.5453;
	highp float dt = dot( uv.xy, vec2( a,b ) ), sn = mod( dt, PI );
	return fract( sin( sn ) * c );
}
#ifdef HIGH_PRECISION
	float precisionSafeLength( vec3 v ) { return length( v ); }
#else
	float precisionSafeLength( vec3 v ) {
		float maxComponent = max3( abs( v ) );
		return length( v / maxComponent ) * maxComponent;
	}
#endif
struct IncidentLight {
	vec3 color;
	vec3 direction;
	bool visible;
};
struct ReflectedLight {
	vec3 directDiffuse;
	vec3 directSpecular;
	vec3 indirectDiffuse;
	vec3 indirectSpecular;
};
#ifdef USE_ALPHAHASH
	varying vec3 vPosition;
#endif
vec3 transformDirection( in vec3 dir, in mat4 matrix ) {
	return normalize( ( matrix * vec4( dir, 0.0 ) ).xyz );
}
vec3 inverseTransformDirection( in vec3 dir, in mat4 matrix ) {
	return normalize( ( vec4( dir, 0.0 ) * matrix ).xyz );
}
mat3 transposeMat3( const in mat3 m ) {
	mat3 tmp;
	tmp[ 0 ] = vec3( m[ 0 ].x, m[ 1 ].x, m[ 2 ].x );
	tmp[ 1 ] = vec3( m[ 0 ].y, m[ 1 ].y, m[ 2 ].y );
	tmp[ 2 ] = vec3( m[ 0 ].z, m[ 1 ].z, m[ 2 ].z );
	return tmp;
}
float luminance( const in vec3 rgb ) {
	const vec3 weights = vec3( 0.2126729, 0.7151522, 0.0721750 );
	return dot( weights, rgb );
}
bool isPerspectiveMatrix( mat4 m ) {
	return m[ 2 ][ 3 ] == - 1.0;
}
vec2 equirectUv( in vec3 dir ) {
	float u = atan( dir.z, dir.x ) * RECIPROCAL_PI2 + 0.5;
	float v = asin( clamp( dir.y, - 1.0, 1.0 ) ) * RECIPROCAL_PI + 0.5;
	return vec2( u, v );
}
vec3 BRDF_Lambert( const in vec3 diffuseColor ) {
	return RECIPROCAL_PI * diffuseColor;
}
vec3 F_Schlick( const in vec3 f0, const in float f90, const in float dotVH ) {
	float fresnel = exp2( ( - 5.55473 * dotVH - 6.98316 ) * dotVH );
	return f0 * ( 1.0 - fresnel ) + ( f90 * fresnel );
}
float F_Schlick( const in float f0, const in float f90, const in float dotVH ) {
	float fresnel = exp2( ( - 5.55473 * dotVH - 6.98316 ) * dotVH );
	return f0 * ( 1.0 - fresnel ) + ( f90 * fresnel );
} // validated`,M0=`#ifdef ENVMAP_TYPE_CUBE_UV
	#define cubeUV_minMipLevel 4.0
	#define cubeUV_minTileSize 16.0
	float getFace( vec3 direction ) {
		vec3 absDirection = abs( direction );
		float face = - 1.0;
		if ( absDirection.x > absDirection.z ) {
			if ( absDirection.x > absDirection.y )
				face = direction.x > 0.0 ? 0.0 : 3.0;
			else
				face = direction.y > 0.0 ? 1.0 : 4.0;
		} else {
			if ( absDirection.z > absDirection.y )
				face = direction.z > 0.0 ? 2.0 : 5.0;
			else
				face = direction.y > 0.0 ? 1.0 : 4.0;
		}
		return face;
	}
	vec2 getUV( vec3 direction, float face ) {
		vec2 uv;
		if ( face == 0.0 ) {
			uv = vec2( direction.z, direction.y ) / abs( direction.x );
		} else if ( face == 1.0 ) {
			uv = vec2( - direction.x, - direction.z ) / abs( direction.y );
		} else if ( face == 2.0 ) {
			uv = vec2( - direction.x, direction.y ) / abs( direction.z );
		} else if ( face == 3.0 ) {
			uv = vec2( - direction.z, direction.y ) / abs( direction.x );
		} else if ( face == 4.0 ) {
			uv = vec2( - direction.x, direction.z ) / abs( direction.y );
		} else {
			uv = vec2( direction.x, direction.y ) / abs( direction.z );
		}
		return 0.5 * ( uv + 1.0 );
	}
	vec3 bilinearCubeUV( sampler2D envMap, vec3 direction, float mipInt ) {
		float face = getFace( direction );
		float filterInt = max( cubeUV_minMipLevel - mipInt, 0.0 );
		mipInt = max( mipInt, cubeUV_minMipLevel );
		float faceSize = exp2( mipInt );
		highp vec2 uv = getUV( direction, face ) * ( faceSize - 2.0 ) + 1.0;
		if ( face > 2.0 ) {
			uv.y += faceSize;
			face -= 3.0;
		}
		uv.x += face * faceSize;
		uv.x += filterInt * 3.0 * cubeUV_minTileSize;
		uv.y += 4.0 * ( exp2( CUBEUV_MAX_MIP ) - faceSize );
		uv.x *= CUBEUV_TEXEL_WIDTH;
		uv.y *= CUBEUV_TEXEL_HEIGHT;
		#ifdef texture2DGradEXT
			return texture2DGradEXT( envMap, uv, vec2( 0.0 ), vec2( 0.0 ) ).rgb;
		#else
			return texture2D( envMap, uv ).rgb;
		#endif
	}
	#define cubeUV_r0 1.0
	#define cubeUV_m0 - 2.0
	#define cubeUV_r1 0.8
	#define cubeUV_m1 - 1.0
	#define cubeUV_r4 0.4
	#define cubeUV_m4 2.0
	#define cubeUV_r5 0.305
	#define cubeUV_m5 3.0
	#define cubeUV_r6 0.21
	#define cubeUV_m6 4.0
	float roughnessToMip( float roughness ) {
		float mip = 0.0;
		if ( roughness >= cubeUV_r1 ) {
			mip = ( cubeUV_r0 - roughness ) * ( cubeUV_m1 - cubeUV_m0 ) / ( cubeUV_r0 - cubeUV_r1 ) + cubeUV_m0;
		} else if ( roughness >= cubeUV_r4 ) {
			mip = ( cubeUV_r1 - roughness ) * ( cubeUV_m4 - cubeUV_m1 ) / ( cubeUV_r1 - cubeUV_r4 ) + cubeUV_m1;
		} else if ( roughness >= cubeUV_r5 ) {
			mip = ( cubeUV_r4 - roughness ) * ( cubeUV_m5 - cubeUV_m4 ) / ( cubeUV_r4 - cubeUV_r5 ) + cubeUV_m4;
		} else if ( roughness >= cubeUV_r6 ) {
			mip = ( cubeUV_r5 - roughness ) * ( cubeUV_m6 - cubeUV_m5 ) / ( cubeUV_r5 - cubeUV_r6 ) + cubeUV_m5;
		} else {
			mip = - 2.0 * log2( 1.16 * roughness );		}
		return mip;
	}
	vec4 textureCubeUV( sampler2D envMap, vec3 sampleDir, float roughness ) {
		float mip = clamp( roughnessToMip( roughness ), cubeUV_m0, CUBEUV_MAX_MIP );
		float mipF = fract( mip );
		float mipInt = floor( mip );
		vec3 color0 = bilinearCubeUV( envMap, sampleDir, mipInt );
		if ( mipF == 0.0 ) {
			return vec4( color0, 1.0 );
		} else {
			vec3 color1 = bilinearCubeUV( envMap, sampleDir, mipInt + 1.0 );
			return vec4( mix( color0, color1, mipF ), 1.0 );
		}
	}
#endif`,b0=`vec3 transformedNormal = objectNormal;
#ifdef USE_TANGENT
	vec3 transformedTangent = objectTangent;
#endif
#ifdef USE_BATCHING
	mat3 bm = mat3( batchingMatrix );
	transformedNormal /= vec3( dot( bm[ 0 ], bm[ 0 ] ), dot( bm[ 1 ], bm[ 1 ] ), dot( bm[ 2 ], bm[ 2 ] ) );
	transformedNormal = bm * transformedNormal;
	#ifdef USE_TANGENT
		transformedTangent = bm * transformedTangent;
	#endif
#endif
#ifdef USE_INSTANCING
	mat3 im = mat3( instanceMatrix );
	transformedNormal /= vec3( dot( im[ 0 ], im[ 0 ] ), dot( im[ 1 ], im[ 1 ] ), dot( im[ 2 ], im[ 2 ] ) );
	transformedNormal = im * transformedNormal;
	#ifdef USE_TANGENT
		transformedTangent = im * transformedTangent;
	#endif
#endif
transformedNormal = normalMatrix * transformedNormal;
#ifdef FLIP_SIDED
	transformedNormal = - transformedNormal;
#endif
#ifdef USE_TANGENT
	transformedTangent = ( modelViewMatrix * vec4( transformedTangent, 0.0 ) ).xyz;
	#ifdef FLIP_SIDED
		transformedTangent = - transformedTangent;
	#endif
#endif`,S0=`#ifdef USE_DISPLACEMENTMAP
	uniform sampler2D displacementMap;
	uniform float displacementScale;
	uniform float displacementBias;
#endif`,w0=`#ifdef USE_DISPLACEMENTMAP
	transformed += normalize( objectNormal ) * ( texture2D( displacementMap, vDisplacementMapUv ).x * displacementScale + displacementBias );
#endif`,E0=`#ifdef USE_EMISSIVEMAP
	vec4 emissiveColor = texture2D( emissiveMap, vEmissiveMapUv );
	totalEmissiveRadiance *= emissiveColor.rgb;
#endif`,T0=`#ifdef USE_EMISSIVEMAP
	uniform sampler2D emissiveMap;
#endif`,A0="gl_FragColor = linearToOutputTexel( gl_FragColor );",C0=`
const mat3 LINEAR_SRGB_TO_LINEAR_DISPLAY_P3 = mat3(
	vec3( 0.8224621, 0.177538, 0.0 ),
	vec3( 0.0331941, 0.9668058, 0.0 ),
	vec3( 0.0170827, 0.0723974, 0.9105199 )
);
const mat3 LINEAR_DISPLAY_P3_TO_LINEAR_SRGB = mat3(
	vec3( 1.2249401, - 0.2249404, 0.0 ),
	vec3( - 0.0420569, 1.0420571, 0.0 ),
	vec3( - 0.0196376, - 0.0786361, 1.0982735 )
);
vec4 LinearSRGBToLinearDisplayP3( in vec4 value ) {
	return vec4( value.rgb * LINEAR_SRGB_TO_LINEAR_DISPLAY_P3, value.a );
}
vec4 LinearDisplayP3ToLinearSRGB( in vec4 value ) {
	return vec4( value.rgb * LINEAR_DISPLAY_P3_TO_LINEAR_SRGB, value.a );
}
vec4 LinearTransferOETF( in vec4 value ) {
	return value;
}
vec4 sRGBTransferOETF( in vec4 value ) {
	return vec4( mix( pow( value.rgb, vec3( 0.41666 ) ) * 1.055 - vec3( 0.055 ), value.rgb * 12.92, vec3( lessThanEqual( value.rgb, vec3( 0.0031308 ) ) ) ), value.a );
}
vec4 LinearToLinear( in vec4 value ) {
	return value;
}
vec4 LinearTosRGB( in vec4 value ) {
	return sRGBTransferOETF( value );
}`,R0=`#ifdef USE_ENVMAP
	#ifdef ENV_WORLDPOS
		vec3 cameraToFrag;
		if ( isOrthographic ) {
			cameraToFrag = normalize( vec3( - viewMatrix[ 0 ][ 2 ], - viewMatrix[ 1 ][ 2 ], - viewMatrix[ 2 ][ 2 ] ) );
		} else {
			cameraToFrag = normalize( vWorldPosition - cameraPosition );
		}
		vec3 worldNormal = inverseTransformDirection( normal, viewMatrix );
		#ifdef ENVMAP_MODE_REFLECTION
			vec3 reflectVec = reflect( cameraToFrag, worldNormal );
		#else
			vec3 reflectVec = refract( cameraToFrag, worldNormal, refractionRatio );
		#endif
	#else
		vec3 reflectVec = vReflect;
	#endif
	#ifdef ENVMAP_TYPE_CUBE
		vec4 envColor = textureCube( envMap, envMapRotation * vec3( flipEnvMap * reflectVec.x, reflectVec.yz ) );
	#else
		vec4 envColor = vec4( 0.0 );
	#endif
	#ifdef ENVMAP_BLENDING_MULTIPLY
		outgoingLight = mix( outgoingLight, outgoingLight * envColor.xyz, specularStrength * reflectivity );
	#elif defined( ENVMAP_BLENDING_MIX )
		outgoingLight = mix( outgoingLight, envColor.xyz, specularStrength * reflectivity );
	#elif defined( ENVMAP_BLENDING_ADD )
		outgoingLight += envColor.xyz * specularStrength * reflectivity;
	#endif
#endif`,P0=`#ifdef USE_ENVMAP
	uniform float envMapIntensity;
	uniform float flipEnvMap;
	uniform mat3 envMapRotation;
	#ifdef ENVMAP_TYPE_CUBE
		uniform samplerCube envMap;
	#else
		uniform sampler2D envMap;
	#endif
	
#endif`,I0=`#ifdef USE_ENVMAP
	uniform float reflectivity;
	#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG ) || defined( LAMBERT )
		#define ENV_WORLDPOS
	#endif
	#ifdef ENV_WORLDPOS
		varying vec3 vWorldPosition;
		uniform float refractionRatio;
	#else
		varying vec3 vReflect;
	#endif
#endif`,L0=`#ifdef USE_ENVMAP
	#if defined( USE_BUMPMAP ) || defined( USE_NORMALMAP ) || defined( PHONG ) || defined( LAMBERT )
		#define ENV_WORLDPOS
	#endif
	#ifdef ENV_WORLDPOS
		
		varying vec3 vWorldPosition;
	#else
		varying vec3 vReflect;
		uniform float refractionRatio;
	#endif
#endif`,D0=`#ifdef USE_ENVMAP
	#ifdef ENV_WORLDPOS
		vWorldPosition = worldPosition.xyz;
	#else
		vec3 cameraToVertex;
		if ( isOrthographic ) {
			cameraToVertex = normalize( vec3( - viewMatrix[ 0 ][ 2 ], - viewMatrix[ 1 ][ 2 ], - viewMatrix[ 2 ][ 2 ] ) );
		} else {
			cameraToVertex = normalize( worldPosition.xyz - cameraPosition );
		}
		vec3 worldNormal = inverseTransformDirection( transformedNormal, viewMatrix );
		#ifdef ENVMAP_MODE_REFLECTION
			vReflect = reflect( cameraToVertex, worldNormal );
		#else
			vReflect = refract( cameraToVertex, worldNormal, refractionRatio );
		#endif
	#endif
#endif`,N0=`#ifdef USE_FOG
	vFogDepth = - mvPosition.z;
#endif`,O0=`#ifdef USE_FOG
	varying float vFogDepth;
#endif`,U0=`#ifdef USE_FOG
	#ifdef FOG_EXP2
		float fogFactor = 1.0 - exp( - fogDensity * fogDensity * vFogDepth * vFogDepth );
	#else
		float fogFactor = smoothstep( fogNear, fogFar, vFogDepth );
	#endif
	gl_FragColor.rgb = mix( gl_FragColor.rgb, fogColor, fogFactor );
#endif`,F0=`#ifdef USE_FOG
	uniform vec3 fogColor;
	varying float vFogDepth;
	#ifdef FOG_EXP2
		uniform float fogDensity;
	#else
		uniform float fogNear;
		uniform float fogFar;
	#endif
#endif`,B0=`#ifdef USE_GRADIENTMAP
	uniform sampler2D gradientMap;
#endif
vec3 getGradientIrradiance( vec3 normal, vec3 lightDirection ) {
	float dotNL = dot( normal, lightDirection );
	vec2 coord = vec2( dotNL * 0.5 + 0.5, 0.0 );
	#ifdef USE_GRADIENTMAP
		return vec3( texture2D( gradientMap, coord ).r );
	#else
		vec2 fw = fwidth( coord ) * 0.5;
		return mix( vec3( 0.7 ), vec3( 1.0 ), smoothstep( 0.7 - fw.x, 0.7 + fw.x, coord.x ) );
	#endif
}`,z0=`#ifdef USE_LIGHTMAP
	uniform sampler2D lightMap;
	uniform float lightMapIntensity;
#endif`,k0=`LambertMaterial material;
material.diffuseColor = diffuseColor.rgb;
material.specularStrength = specularStrength;`,V0=`varying vec3 vViewPosition;
struct LambertMaterial {
	vec3 diffuseColor;
	float specularStrength;
};
void RE_Direct_Lambert( const in IncidentLight directLight, const in vec3 geometryPosition, const in vec3 geometryNormal, const in vec3 geometryViewDir, const in vec3 geometryClearcoatNormal, const in LambertMaterial material, inout ReflectedLight reflectedLight ) {
	float dotNL = saturate( dot( geometryNormal, directLight.direction ) );
	vec3 irradiance = dotNL * directLight.color;
	reflectedLight.directDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
void RE_IndirectDiffuse_Lambert( const in vec3 irradiance, const in vec3 geometryPosition, const in vec3 geometryNormal, const in vec3 geometryViewDir, const in vec3 geometryClearcoatNormal, const in LambertMaterial material, inout ReflectedLight reflectedLight ) {
	reflectedLight.indirectDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
#define RE_Direct				RE_Direct_Lambert
#define RE_IndirectDiffuse		RE_IndirectDiffuse_Lambert`,H0=`uniform bool receiveShadow;
uniform vec3 ambientLightColor;
#if defined( USE_LIGHT_PROBES )
	uniform vec3 lightProbe[ 9 ];
#endif
vec3 shGetIrradianceAt( in vec3 normal, in vec3 shCoefficients[ 9 ] ) {
	float x = normal.x, y = normal.y, z = normal.z;
	vec3 result = shCoefficients[ 0 ] * 0.886227;
	result += shCoefficients[ 1 ] * 2.0 * 0.511664 * y;
	result += shCoefficients[ 2 ] * 2.0 * 0.511664 * z;
	result += shCoefficients[ 3 ] * 2.0 * 0.511664 * x;
	result += shCoefficients[ 4 ] * 2.0 * 0.429043 * x * y;
	result += shCoefficients[ 5 ] * 2.0 * 0.429043 * y * z;
	result += shCoefficients[ 6 ] * ( 0.743125 * z * z - 0.247708 );
	result += shCoefficients[ 7 ] * 2.0 * 0.429043 * x * z;
	result += shCoefficients[ 8 ] * 0.429043 * ( x * x - y * y );
	return result;
}
vec3 getLightProbeIrradiance( const in vec3 lightProbe[ 9 ], const in vec3 normal ) {
	vec3 worldNormal = inverseTransformDirection( normal, viewMatrix );
	vec3 irradiance = shGetIrradianceAt( worldNormal, lightProbe );
	return irradiance;
}
vec3 getAmbientLightIrradiance( const in vec3 ambientLightColor ) {
	vec3 irradiance = ambientLightColor;
	return irradiance;
}
float getDistanceAttenuation( const in float lightDistance, const in float cutoffDistance, const in float decayExponent ) {
	#if defined ( LEGACY_LIGHTS )
		if ( cutoffDistance > 0.0 && decayExponent > 0.0 ) {
			return pow( saturate( - lightDistance / cutoffDistance + 1.0 ), decayExponent );
		}
		return 1.0;
	#else
		float distanceFalloff = 1.0 / max( pow( lightDistance, decayExponent ), 0.01 );
		if ( cutoffDistance > 0.0 ) {
			distanceFalloff *= pow2( saturate( 1.0 - pow4( lightDistance / cutoffDistance ) ) );
		}
		return distanceFalloff;
	#endif
}
float getSpotAttenuation( const in float coneCosine, const in float penumbraCosine, const in float angleCosine ) {
	return smoothstep( coneCosine, penumbraCosine, angleCosine );
}
#if NUM_DIR_LIGHTS > 0
	struct DirectionalLight {
		vec3 direction;
		vec3 color;
	};
	uniform DirectionalLight directionalLights[ NUM_DIR_LIGHTS ];
	void getDirectionalLightInfo( const in DirectionalLight directionalLight, out IncidentLight light ) {
		light.color = directionalLight.color;
		light.direction = directionalLight.direction;
		light.visible = true;
	}
#endif
#if NUM_POINT_LIGHTS > 0
	struct PointLight {
		vec3 position;
		vec3 color;
		float distance;
		float decay;
	};
	uniform PointLight pointLights[ NUM_POINT_LIGHTS ];
	void getPointLightInfo( const in PointLight pointLight, const in vec3 geometryPosition, out IncidentLight light ) {
		vec3 lVector = pointLight.position - geometryPosition;
		light.direction = normalize( lVector );
		float lightDistance = length( lVector );
		light.color = pointLight.color;
		light.color *= getDistanceAttenuation( lightDistance, pointLight.distance, pointLight.decay );
		light.visible = ( light.color != vec3( 0.0 ) );
	}
#endif
#if NUM_SPOT_LIGHTS > 0
	struct SpotLight {
		vec3 position;
		vec3 direction;
		vec3 color;
		float distance;
		float decay;
		float coneCos;
		float penumbraCos;
	};
	uniform SpotLight spotLights[ NUM_SPOT_LIGHTS ];
	void getSpotLightInfo( const in SpotLight spotLight, const in vec3 geometryPosition, out IncidentLight light ) {
		vec3 lVector = spotLight.position - geometryPosition;
		light.direction = normalize( lVector );
		float angleCos = dot( light.direction, spotLight.direction );
		float spotAttenuation = getSpotAttenuation( spotLight.coneCos, spotLight.penumbraCos, angleCos );
		if ( spotAttenuation > 0.0 ) {
			float lightDistance = length( lVector );
			light.color = spotLight.color * spotAttenuation;
			light.color *= getDistanceAttenuation( lightDistance, spotLight.distance, spotLight.decay );
			light.visible = ( light.color != vec3( 0.0 ) );
		} else {
			light.color = vec3( 0.0 );
			light.visible = false;
		}
	}
#endif
#if NUM_RECT_AREA_LIGHTS > 0
	struct RectAreaLight {
		vec3 color;
		vec3 position;
		vec3 halfWidth;
		vec3 halfHeight;
	};
	uniform sampler2D ltc_1;	uniform sampler2D ltc_2;
	uniform RectAreaLight rectAreaLights[ NUM_RECT_AREA_LIGHTS ];
#endif
#if NUM_HEMI_LIGHTS > 0
	struct HemisphereLight {
		vec3 direction;
		vec3 skyColor;
		vec3 groundColor;
	};
	uniform HemisphereLight hemisphereLights[ NUM_HEMI_LIGHTS ];
	vec3 getHemisphereLightIrradiance( const in HemisphereLight hemiLight, const in vec3 normal ) {
		float dotNL = dot( normal, hemiLight.direction );
		float hemiDiffuseWeight = 0.5 * dotNL + 0.5;
		vec3 irradiance = mix( hemiLight.groundColor, hemiLight.skyColor, hemiDiffuseWeight );
		return irradiance;
	}
#endif`,G0=`#ifdef USE_ENVMAP
	vec3 getIBLIrradiance( const in vec3 normal ) {
		#ifdef ENVMAP_TYPE_CUBE_UV
			vec3 worldNormal = inverseTransformDirection( normal, viewMatrix );
			vec4 envMapColor = textureCubeUV( envMap, envMapRotation * worldNormal, 1.0 );
			return PI * envMapColor.rgb * envMapIntensity;
		#else
			return vec3( 0.0 );
		#endif
	}
	vec3 getIBLRadiance( const in vec3 viewDir, const in vec3 normal, const in float roughness ) {
		#ifdef ENVMAP_TYPE_CUBE_UV
			vec3 reflectVec = reflect( - viewDir, normal );
			reflectVec = normalize( mix( reflectVec, normal, roughness * roughness) );
			reflectVec = inverseTransformDirection( reflectVec, viewMatrix );
			vec4 envMapColor = textureCubeUV( envMap, envMapRotation * reflectVec, roughness );
			return envMapColor.rgb * envMapIntensity;
		#else
			return vec3( 0.0 );
		#endif
	}
	#ifdef USE_ANISOTROPY
		vec3 getIBLAnisotropyRadiance( const in vec3 viewDir, const in vec3 normal, const in float roughness, const in vec3 bitangent, const in float anisotropy ) {
			#ifdef ENVMAP_TYPE_CUBE_UV
				vec3 bentNormal = cross( bitangent, viewDir );
				bentNormal = normalize( cross( bentNormal, bitangent ) );
				bentNormal = normalize( mix( bentNormal, normal, pow2( pow2( 1.0 - anisotropy * ( 1.0 - roughness ) ) ) ) );
				return getIBLRadiance( viewDir, bentNormal, roughness );
			#else
				return vec3( 0.0 );
			#endif
		}
	#endif
#endif`,W0=`ToonMaterial material;
material.diffuseColor = diffuseColor.rgb;`,X0=`varying vec3 vViewPosition;
struct ToonMaterial {
	vec3 diffuseColor;
};
void RE_Direct_Toon( const in IncidentLight directLight, const in vec3 geometryPosition, const in vec3 geometryNormal, const in vec3 geometryViewDir, const in vec3 geometryClearcoatNormal, const in ToonMaterial material, inout ReflectedLight reflectedLight ) {
	vec3 irradiance = getGradientIrradiance( geometryNormal, directLight.direction ) * directLight.color;
	reflectedLight.directDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
void RE_IndirectDiffuse_Toon( const in vec3 irradiance, const in vec3 geometryPosition, const in vec3 geometryNormal, const in vec3 geometryViewDir, const in vec3 geometryClearcoatNormal, const in ToonMaterial material, inout ReflectedLight reflectedLight ) {
	reflectedLight.indirectDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
#define RE_Direct				RE_Direct_Toon
#define RE_IndirectDiffuse		RE_IndirectDiffuse_Toon`,Y0=`BlinnPhongMaterial material;
material.diffuseColor = diffuseColor.rgb;
material.specularColor = specular;
material.specularShininess = shininess;
material.specularStrength = specularStrength;`,q0=`varying vec3 vViewPosition;
struct BlinnPhongMaterial {
	vec3 diffuseColor;
	vec3 specularColor;
	float specularShininess;
	float specularStrength;
};
void RE_Direct_BlinnPhong( const in IncidentLight directLight, const in vec3 geometryPosition, const in vec3 geometryNormal, const in vec3 geometryViewDir, const in vec3 geometryClearcoatNormal, const in BlinnPhongMaterial material, inout ReflectedLight reflectedLight ) {
	float dotNL = saturate( dot( geometryNormal, directLight.direction ) );
	vec3 irradiance = dotNL * directLight.color;
	reflectedLight.directDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
	reflectedLight.directSpecular += irradiance * BRDF_BlinnPhong( directLight.direction, geometryViewDir, geometryNormal, material.specularColor, material.specularShininess ) * material.specularStrength;
}
void RE_IndirectDiffuse_BlinnPhong( const in vec3 irradiance, const in vec3 geometryPosition, const in vec3 geometryNormal, const in vec3 geometryViewDir, const in vec3 geometryClearcoatNormal, const in BlinnPhongMaterial material, inout ReflectedLight reflectedLight ) {
	reflectedLight.indirectDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
#define RE_Direct				RE_Direct_BlinnPhong
#define RE_IndirectDiffuse		RE_IndirectDiffuse_BlinnPhong`,Z0=`PhysicalMaterial material;
material.diffuseColor = diffuseColor.rgb * ( 1.0 - metalnessFactor );
vec3 dxy = max( abs( dFdx( nonPerturbedNormal ) ), abs( dFdy( nonPerturbedNormal ) ) );
float geometryRoughness = max( max( dxy.x, dxy.y ), dxy.z );
material.roughness = max( roughnessFactor, 0.0525 );material.roughness += geometryRoughness;
material.roughness = min( material.roughness, 1.0 );
#ifdef IOR
	material.ior = ior;
	#ifdef USE_SPECULAR
		float specularIntensityFactor = specularIntensity;
		vec3 specularColorFactor = specularColor;
		#ifdef USE_SPECULAR_COLORMAP
			specularColorFactor *= texture2D( specularColorMap, vSpecularColorMapUv ).rgb;
		#endif
		#ifdef USE_SPECULAR_INTENSITYMAP
			specularIntensityFactor *= texture2D( specularIntensityMap, vSpecularIntensityMapUv ).a;
		#endif
		material.specularF90 = mix( specularIntensityFactor, 1.0, metalnessFactor );
	#else
		float specularIntensityFactor = 1.0;
		vec3 specularColorFactor = vec3( 1.0 );
		material.specularF90 = 1.0;
	#endif
	material.specularColor = mix( min( pow2( ( material.ior - 1.0 ) / ( material.ior + 1.0 ) ) * specularColorFactor, vec3( 1.0 ) ) * specularIntensityFactor, diffuseColor.rgb, metalnessFactor );
#else
	material.specularColor = mix( vec3( 0.04 ), diffuseColor.rgb, metalnessFactor );
	material.specularF90 = 1.0;
#endif
#ifdef USE_CLEARCOAT
	material.clearcoat = clearcoat;
	material.clearcoatRoughness = clearcoatRoughness;
	material.clearcoatF0 = vec3( 0.04 );
	material.clearcoatF90 = 1.0;
	#ifdef USE_CLEARCOATMAP
		material.clearcoat *= texture2D( clearcoatMap, vClearcoatMapUv ).x;
	#endif
	#ifdef USE_CLEARCOAT_ROUGHNESSMAP
		material.clearcoatRoughness *= texture2D( clearcoatRoughnessMap, vClearcoatRoughnessMapUv ).y;
	#endif
	material.clearcoat = saturate( material.clearcoat );	material.clearcoatRoughness = max( material.clearcoatRoughness, 0.0525 );
	material.clearcoatRoughness += geometryRoughness;
	material.clearcoatRoughness = min( material.clearcoatRoughness, 1.0 );
#endif
#ifdef USE_DISPERSION
	material.dispersion = dispersion;
#endif
#ifdef USE_IRIDESCENCE
	material.iridescence = iridescence;
	material.iridescenceIOR = iridescenceIOR;
	#ifdef USE_IRIDESCENCEMAP
		material.iridescence *= texture2D( iridescenceMap, vIridescenceMapUv ).r;
	#endif
	#ifdef USE_IRIDESCENCE_THICKNESSMAP
		material.iridescenceThickness = (iridescenceThicknessMaximum - iridescenceThicknessMinimum) * texture2D( iridescenceThicknessMap, vIridescenceThicknessMapUv ).g + iridescenceThicknessMinimum;
	#else
		material.iridescenceThickness = iridescenceThicknessMaximum;
	#endif
#endif
#ifdef USE_SHEEN
	material.sheenColor = sheenColor;
	#ifdef USE_SHEEN_COLORMAP
		material.sheenColor *= texture2D( sheenColorMap, vSheenColorMapUv ).rgb;
	#endif
	material.sheenRoughness = clamp( sheenRoughness, 0.07, 1.0 );
	#ifdef USE_SHEEN_ROUGHNESSMAP
		material.sheenRoughness *= texture2D( sheenRoughnessMap, vSheenRoughnessMapUv ).a;
	#endif
#endif
#ifdef USE_ANISOTROPY
	#ifdef USE_ANISOTROPYMAP
		mat2 anisotropyMat = mat2( anisotropyVector.x, anisotropyVector.y, - anisotropyVector.y, anisotropyVector.x );
		vec3 anisotropyPolar = texture2D( anisotropyMap, vAnisotropyMapUv ).rgb;
		vec2 anisotropyV = anisotropyMat * normalize( 2.0 * anisotropyPolar.rg - vec2( 1.0 ) ) * anisotropyPolar.b;
	#else
		vec2 anisotropyV = anisotropyVector;
	#endif
	material.anisotropy = length( anisotropyV );
	if( material.anisotropy == 0.0 ) {
		anisotropyV = vec2( 1.0, 0.0 );
	} else {
		anisotropyV /= material.anisotropy;
		material.anisotropy = saturate( material.anisotropy );
	}
	material.alphaT = mix( pow2( material.roughness ), 1.0, pow2( material.anisotropy ) );
	material.anisotropyT = tbn[ 0 ] * anisotropyV.x + tbn[ 1 ] * anisotropyV.y;
	material.anisotropyB = tbn[ 1 ] * anisotropyV.x - tbn[ 0 ] * anisotropyV.y;
#endif`,j0=`struct PhysicalMaterial {
	vec3 diffuseColor;
	float roughness;
	vec3 specularColor;
	float specularF90;
	float dispersion;
	#ifdef USE_CLEARCOAT
		float clearcoat;
		float clearcoatRoughness;
		vec3 clearcoatF0;
		float clearcoatF90;
	#endif
	#ifdef USE_IRIDESCENCE
		float iridescence;
		float iridescenceIOR;
		float iridescenceThickness;
		vec3 iridescenceFresnel;
		vec3 iridescenceF0;
	#endif
	#ifdef USE_SHEEN
		vec3 sheenColor;
		float sheenRoughness;
	#endif
	#ifdef IOR
		float ior;
	#endif
	#ifdef USE_TRANSMISSION
		float transmission;
		float transmissionAlpha;
		float thickness;
		float attenuationDistance;
		vec3 attenuationColor;
	#endif
	#ifdef USE_ANISOTROPY
		float anisotropy;
		float alphaT;
		vec3 anisotropyT;
		vec3 anisotropyB;
	#endif
};
vec3 clearcoatSpecularDirect = vec3( 0.0 );
vec3 clearcoatSpecularIndirect = vec3( 0.0 );
vec3 sheenSpecularDirect = vec3( 0.0 );
vec3 sheenSpecularIndirect = vec3(0.0 );
vec3 Schlick_to_F0( const in vec3 f, const in float f90, const in float dotVH ) {
    float x = clamp( 1.0 - dotVH, 0.0, 1.0 );
    float x2 = x * x;
    float x5 = clamp( x * x2 * x2, 0.0, 0.9999 );
    return ( f - vec3( f90 ) * x5 ) / ( 1.0 - x5 );
}
float V_GGX_SmithCorrelated( const in float alpha, const in float dotNL, const in float dotNV ) {
	float a2 = pow2( alpha );
	float gv = dotNL * sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNV ) );
	float gl = dotNV * sqrt( a2 + ( 1.0 - a2 ) * pow2( dotNL ) );
	return 0.5 / max( gv + gl, EPSILON );
}
float D_GGX( const in float alpha, const in float dotNH ) {
	float a2 = pow2( alpha );
	float denom = pow2( dotNH ) * ( a2 - 1.0 ) + 1.0;
	return RECIPROCAL_PI * a2 / pow2( denom );
}
#ifdef USE_ANISOTROPY
	float V_GGX_SmithCorrelated_Anisotropic( const in float alphaT, const in float alphaB, const in float dotTV, const in float dotBV, const in float dotTL, const in float dotBL, const in float dotNV, const in float dotNL ) {
		float gv = dotNL * length( vec3( alphaT * dotTV, alphaB * dotBV, dotNV ) );
		float gl = dotNV * length( vec3( alphaT * dotTL, alphaB * dotBL, dotNL ) );
		float v = 0.5 / ( gv + gl );
		return saturate(v);
	}
	float D_GGX_Anisotropic( const in float alphaT, const in float alphaB, const in float dotNH, const in float dotTH, const in float dotBH ) {
		float a2 = alphaT * alphaB;
		highp vec3 v = vec3( alphaB * dotTH, alphaT * dotBH, a2 * dotNH );
		highp float v2 = dot( v, v );
		float w2 = a2 / v2;
		return RECIPROCAL_PI * a2 * pow2 ( w2 );
	}
#endif
#ifdef USE_CLEARCOAT
	vec3 BRDF_GGX_Clearcoat( const in vec3 lightDir, const in vec3 viewDir, const in vec3 normal, const in PhysicalMaterial material) {
		vec3 f0 = material.clearcoatF0;
		float f90 = material.clearcoatF90;
		float roughness = material.clearcoatRoughness;
		float alpha = pow2( roughness );
		vec3 halfDir = normalize( lightDir + viewDir );
		float dotNL = saturate( dot( normal, lightDir ) );
		float dotNV = saturate( dot( normal, viewDir ) );
		float dotNH = saturate( dot( normal, halfDir ) );
		float dotVH = saturate( dot( viewDir, halfDir ) );
		vec3 F = F_Schlick( f0, f90, dotVH );
		float V = V_GGX_SmithCorrelated( alpha, dotNL, dotNV );
		float D = D_GGX( alpha, dotNH );
		return F * ( V * D );
	}
#endif
vec3 BRDF_GGX( const in vec3 lightDir, const in vec3 viewDir, const in vec3 normal, const in PhysicalMaterial material ) {
	vec3 f0 = material.specularColor;
	float f90 = material.specularF90;
	float roughness = material.roughness;
	float alpha = pow2( roughness );
	vec3 halfDir = normalize( lightDir + viewDir );
	float dotNL = saturate( dot( normal, lightDir ) );
	float dotNV = saturate( dot( normal, viewDir ) );
	float dotNH = saturate( dot( normal, halfDir ) );
	float dotVH = saturate( dot( viewDir, halfDir ) );
	vec3 F = F_Schlick( f0, f90, dotVH );
	#ifdef USE_IRIDESCENCE
		F = mix( F, material.iridescenceFresnel, material.iridescence );
	#endif
	#ifdef USE_ANISOTROPY
		float dotTL = dot( material.anisotropyT, lightDir );
		float dotTV = dot( material.anisotropyT, viewDir );
		float dotTH = dot( material.anisotropyT, halfDir );
		float dotBL = dot( material.anisotropyB, lightDir );
		float dotBV = dot( material.anisotropyB, viewDir );
		float dotBH = dot( material.anisotropyB, halfDir );
		float V = V_GGX_SmithCorrelated_Anisotropic( material.alphaT, alpha, dotTV, dotBV, dotTL, dotBL, dotNV, dotNL );
		float D = D_GGX_Anisotropic( material.alphaT, alpha, dotNH, dotTH, dotBH );
	#else
		float V = V_GGX_SmithCorrelated( alpha, dotNL, dotNV );
		float D = D_GGX( alpha, dotNH );
	#endif
	return F * ( V * D );
}
vec2 LTC_Uv( const in vec3 N, const in vec3 V, const in float roughness ) {
	const float LUT_SIZE = 64.0;
	const float LUT_SCALE = ( LUT_SIZE - 1.0 ) / LUT_SIZE;
	const float LUT_BIAS = 0.5 / LUT_SIZE;
	float dotNV = saturate( dot( N, V ) );
	vec2 uv = vec2( roughness, sqrt( 1.0 - dotNV ) );
	uv = uv * LUT_SCALE + LUT_BIAS;
	return uv;
}
float LTC_ClippedSphereFormFactor( const in vec3 f ) {
	float l = length( f );
	return max( ( l * l + f.z ) / ( l + 1.0 ), 0.0 );
}
vec3 LTC_EdgeVectorFormFactor( const in vec3 v1, const in vec3 v2 ) {
	float x = dot( v1, v2 );
	float y = abs( x );
	float a = 0.8543985 + ( 0.4965155 + 0.0145206 * y ) * y;
	float b = 3.4175940 + ( 4.1616724 + y ) * y;
	float v = a / b;
	float theta_sintheta = ( x > 0.0 ) ? v : 0.5 * inversesqrt( max( 1.0 - x * x, 1e-7 ) ) - v;
	return cross( v1, v2 ) * theta_sintheta;
}
vec3 LTC_Evaluate( const in vec3 N, const in vec3 V, const in vec3 P, const in mat3 mInv, const in vec3 rectCoords[ 4 ] ) {
	vec3 v1 = rectCoords[ 1 ] - rectCoords[ 0 ];
	vec3 v2 = rectCoords[ 3 ] - rectCoords[ 0 ];
	vec3 lightNormal = cross( v1, v2 );
	if( dot( lightNormal, P - rectCoords[ 0 ] ) < 0.0 ) return vec3( 0.0 );
	vec3 T1, T2;
	T1 = normalize( V - N * dot( V, N ) );
	T2 = - cross( N, T1 );
	mat3 mat = mInv * transposeMat3( mat3( T1, T2, N ) );
	vec3 coords[ 4 ];
	coords[ 0 ] = mat * ( rectCoords[ 0 ] - P );
	coords[ 1 ] = mat * ( rectCoords[ 1 ] - P );
	coords[ 2 ] = mat * ( rectCoords[ 2 ] - P );
	coords[ 3 ] = mat * ( rectCoords[ 3 ] - P );
	coords[ 0 ] = normalize( coords[ 0 ] );
	coords[ 1 ] = normalize( coords[ 1 ] );
	coords[ 2 ] = normalize( coords[ 2 ] );
	coords[ 3 ] = normalize( coords[ 3 ] );
	vec3 vectorFormFactor = vec3( 0.0 );
	vectorFormFactor += LTC_EdgeVectorFormFactor( coords[ 0 ], coords[ 1 ] );
	vectorFormFactor += LTC_EdgeVectorFormFactor( coords[ 1 ], coords[ 2 ] );
	vectorFormFactor += LTC_EdgeVectorFormFactor( coords[ 2 ], coords[ 3 ] );
	vectorFormFactor += LTC_EdgeVectorFormFactor( coords[ 3 ], coords[ 0 ] );
	float result = LTC_ClippedSphereFormFactor( vectorFormFactor );
	return vec3( result );
}
#if defined( USE_SHEEN )
float D_Charlie( float roughness, float dotNH ) {
	float alpha = pow2( roughness );
	float invAlpha = 1.0 / alpha;
	float cos2h = dotNH * dotNH;
	float sin2h = max( 1.0 - cos2h, 0.0078125 );
	return ( 2.0 + invAlpha ) * pow( sin2h, invAlpha * 0.5 ) / ( 2.0 * PI );
}
float V_Neubelt( float dotNV, float dotNL ) {
	return saturate( 1.0 / ( 4.0 * ( dotNL + dotNV - dotNL * dotNV ) ) );
}
vec3 BRDF_Sheen( const in vec3 lightDir, const in vec3 viewDir, const in vec3 normal, vec3 sheenColor, const in float sheenRoughness ) {
	vec3 halfDir = normalize( lightDir + viewDir );
	float dotNL = saturate( dot( normal, lightDir ) );
	float dotNV = saturate( dot( normal, viewDir ) );
	float dotNH = saturate( dot( normal, halfDir ) );
	float D = D_Charlie( sheenRoughness, dotNH );
	float V = V_Neubelt( dotNV, dotNL );
	return sheenColor * ( D * V );
}
#endif
float IBLSheenBRDF( const in vec3 normal, const in vec3 viewDir, const in float roughness ) {
	float dotNV = saturate( dot( normal, viewDir ) );
	float r2 = roughness * roughness;
	float a = roughness < 0.25 ? -339.2 * r2 + 161.4 * roughness - 25.9 : -8.48 * r2 + 14.3 * roughness - 9.95;
	float b = roughness < 0.25 ? 44.0 * r2 - 23.7 * roughness + 3.26 : 1.97 * r2 - 3.27 * roughness + 0.72;
	float DG = exp( a * dotNV + b ) + ( roughness < 0.25 ? 0.0 : 0.1 * ( roughness - 0.25 ) );
	return saturate( DG * RECIPROCAL_PI );
}
vec2 DFGApprox( const in vec3 normal, const in vec3 viewDir, const in float roughness ) {
	float dotNV = saturate( dot( normal, viewDir ) );
	const vec4 c0 = vec4( - 1, - 0.0275, - 0.572, 0.022 );
	const vec4 c1 = vec4( 1, 0.0425, 1.04, - 0.04 );
	vec4 r = roughness * c0 + c1;
	float a004 = min( r.x * r.x, exp2( - 9.28 * dotNV ) ) * r.x + r.y;
	vec2 fab = vec2( - 1.04, 1.04 ) * a004 + r.zw;
	return fab;
}
vec3 EnvironmentBRDF( const in vec3 normal, const in vec3 viewDir, const in vec3 specularColor, const in float specularF90, const in float roughness ) {
	vec2 fab = DFGApprox( normal, viewDir, roughness );
	return specularColor * fab.x + specularF90 * fab.y;
}
#ifdef USE_IRIDESCENCE
void computeMultiscatteringIridescence( const in vec3 normal, const in vec3 viewDir, const in vec3 specularColor, const in float specularF90, const in float iridescence, const in vec3 iridescenceF0, const in float roughness, inout vec3 singleScatter, inout vec3 multiScatter ) {
#else
void computeMultiscattering( const in vec3 normal, const in vec3 viewDir, const in vec3 specularColor, const in float specularF90, const in float roughness, inout vec3 singleScatter, inout vec3 multiScatter ) {
#endif
	vec2 fab = DFGApprox( normal, viewDir, roughness );
	#ifdef USE_IRIDESCENCE
		vec3 Fr = mix( specularColor, iridescenceF0, iridescence );
	#else
		vec3 Fr = specularColor;
	#endif
	vec3 FssEss = Fr * fab.x + specularF90 * fab.y;
	float Ess = fab.x + fab.y;
	float Ems = 1.0 - Ess;
	vec3 Favg = Fr + ( 1.0 - Fr ) * 0.047619;	vec3 Fms = FssEss * Favg / ( 1.0 - Ems * Favg );
	singleScatter += FssEss;
	multiScatter += Fms * Ems;
}
#if NUM_RECT_AREA_LIGHTS > 0
	void RE_Direct_RectArea_Physical( const in RectAreaLight rectAreaLight, const in vec3 geometryPosition, const in vec3 geometryNormal, const in vec3 geometryViewDir, const in vec3 geometryClearcoatNormal, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {
		vec3 normal = geometryNormal;
		vec3 viewDir = geometryViewDir;
		vec3 position = geometryPosition;
		vec3 lightPos = rectAreaLight.position;
		vec3 halfWidth = rectAreaLight.halfWidth;
		vec3 halfHeight = rectAreaLight.halfHeight;
		vec3 lightColor = rectAreaLight.color;
		float roughness = material.roughness;
		vec3 rectCoords[ 4 ];
		rectCoords[ 0 ] = lightPos + halfWidth - halfHeight;		rectCoords[ 1 ] = lightPos - halfWidth - halfHeight;
		rectCoords[ 2 ] = lightPos - halfWidth + halfHeight;
		rectCoords[ 3 ] = lightPos + halfWidth + halfHeight;
		vec2 uv = LTC_Uv( normal, viewDir, roughness );
		vec4 t1 = texture2D( ltc_1, uv );
		vec4 t2 = texture2D( ltc_2, uv );
		mat3 mInv = mat3(
			vec3( t1.x, 0, t1.y ),
			vec3(    0, 1,    0 ),
			vec3( t1.z, 0, t1.w )
		);
		vec3 fresnel = ( material.specularColor * t2.x + ( vec3( 1.0 ) - material.specularColor ) * t2.y );
		reflectedLight.directSpecular += lightColor * fresnel * LTC_Evaluate( normal, viewDir, position, mInv, rectCoords );
		reflectedLight.directDiffuse += lightColor * material.diffuseColor * LTC_Evaluate( normal, viewDir, position, mat3( 1.0 ), rectCoords );
	}
#endif
void RE_Direct_Physical( const in IncidentLight directLight, const in vec3 geometryPosition, const in vec3 geometryNormal, const in vec3 geometryViewDir, const in vec3 geometryClearcoatNormal, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {
	float dotNL = saturate( dot( geometryNormal, directLight.direction ) );
	vec3 irradiance = dotNL * directLight.color;
	#ifdef USE_CLEARCOAT
		float dotNLcc = saturate( dot( geometryClearcoatNormal, directLight.direction ) );
		vec3 ccIrradiance = dotNLcc * directLight.color;
		clearcoatSpecularDirect += ccIrradiance * BRDF_GGX_Clearcoat( directLight.direction, geometryViewDir, geometryClearcoatNormal, material );
	#endif
	#ifdef USE_SHEEN
		sheenSpecularDirect += irradiance * BRDF_Sheen( directLight.direction, geometryViewDir, geometryNormal, material.sheenColor, material.sheenRoughness );
	#endif
	reflectedLight.directSpecular += irradiance * BRDF_GGX( directLight.direction, geometryViewDir, geometryNormal, material );
	reflectedLight.directDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
void RE_IndirectDiffuse_Physical( const in vec3 irradiance, const in vec3 geometryPosition, const in vec3 geometryNormal, const in vec3 geometryViewDir, const in vec3 geometryClearcoatNormal, const in PhysicalMaterial material, inout ReflectedLight reflectedLight ) {
	reflectedLight.indirectDiffuse += irradiance * BRDF_Lambert( material.diffuseColor );
}
void RE_IndirectSpecular_Physical( const in vec3 radiance, const in vec3 irradiance, const in vec3 clearcoatRadiance, const in vec3 geometryPosition, const in vec3 geometryNormal, const in vec3 geometryViewDir, const in vec3 geometryClearcoatNormal, const in PhysicalMaterial material, inout ReflectedLight reflectedLight) {
	#ifdef USE_CLEARCOAT
		clearcoatSpecularIndirect += clearcoatRadiance * EnvironmentBRDF( geometryClearcoatNormal, geometryViewDir, material.clearcoatF0, material.clearcoatF90, material.clearcoatRoughness );
	#endif
	#ifdef USE_SHEEN
		sheenSpecularIndirect += irradiance * material.sheenColor * IBLSheenBRDF( geometryNormal, geometryViewDir, material.sheenRoughness );
	#endif
	vec3 singleScattering = vec3( 0.0 );
	vec3 multiScattering = vec3( 0.0 );
	vec3 cosineWeightedIrradiance = irradiance * RECIPROCAL_PI;
	#ifdef USE_IRIDESCENCE
		computeMultiscatteringIridescence( geometryNormal, geometryViewDir, material.specularColor, material.specularF90, material.iridescence, material.iridescenceFresnel, material.roughness, singleScattering, multiScattering );
	#else
		computeMultiscattering( geometryNormal, geometryViewDir, material.specularColor, material.specularF90, material.roughness, singleScattering, multiScattering );
	#endif
	vec3 totalScattering = singleScattering + multiScattering;
	vec3 diffuse = material.diffuseColor * ( 1.0 - max( max( totalScattering.r, totalScattering.g ), totalScattering.b ) );
	reflectedLight.indirectSpecular += radiance * singleScattering;
	reflectedLight.indirectSpecular += multiScattering * cosineWeightedIrradiance;
	reflectedLight.indirectDiffuse += diffuse * cosineWeightedIrradiance;
}
#define RE_Direct				RE_Direct_Physical
#define RE_Direct_RectArea		RE_Direct_RectArea_Physical
#define RE_IndirectDiffuse		RE_IndirectDiffuse_Physical
#define RE_IndirectSpecular		RE_IndirectSpecular_Physical
float computeSpecularOcclusion( const in float dotNV, const in float ambientOcclusion, const in float roughness ) {
	return saturate( pow( dotNV + ambientOcclusion, exp2( - 16.0 * roughness - 1.0 ) ) - 1.0 + ambientOcclusion );
}`,K0=`
vec3 geometryPosition = - vViewPosition;
vec3 geometryNormal = normal;
vec3 geometryViewDir = ( isOrthographic ) ? vec3( 0, 0, 1 ) : normalize( vViewPosition );
vec3 geometryClearcoatNormal = vec3( 0.0 );
#ifdef USE_CLEARCOAT
	geometryClearcoatNormal = clearcoatNormal;
#endif
#ifdef USE_IRIDESCENCE
	float dotNVi = saturate( dot( normal, geometryViewDir ) );
	if ( material.iridescenceThickness == 0.0 ) {
		material.iridescence = 0.0;
	} else {
		material.iridescence = saturate( material.iridescence );
	}
	if ( material.iridescence > 0.0 ) {
		material.iridescenceFresnel = evalIridescence( 1.0, material.iridescenceIOR, dotNVi, material.iridescenceThickness, material.specularColor );
		material.iridescenceF0 = Schlick_to_F0( material.iridescenceFresnel, 1.0, dotNVi );
	}
#endif
IncidentLight directLight;
#if ( NUM_POINT_LIGHTS > 0 ) && defined( RE_Direct )
	PointLight pointLight;
	#if defined( USE_SHADOWMAP ) && NUM_POINT_LIGHT_SHADOWS > 0
	PointLightShadow pointLightShadow;
	#endif
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_POINT_LIGHTS; i ++ ) {
		pointLight = pointLights[ i ];
		getPointLightInfo( pointLight, geometryPosition, directLight );
		#if defined( USE_SHADOWMAP ) && ( UNROLLED_LOOP_INDEX < NUM_POINT_LIGHT_SHADOWS )
		pointLightShadow = pointLightShadows[ i ];
		directLight.color *= ( directLight.visible && receiveShadow ) ? getPointShadow( pointShadowMap[ i ], pointLightShadow.shadowMapSize, pointLightShadow.shadowBias, pointLightShadow.shadowRadius, vPointShadowCoord[ i ], pointLightShadow.shadowCameraNear, pointLightShadow.shadowCameraFar ) : 1.0;
		#endif
		RE_Direct( directLight, geometryPosition, geometryNormal, geometryViewDir, geometryClearcoatNormal, material, reflectedLight );
	}
	#pragma unroll_loop_end
#endif
#if ( NUM_SPOT_LIGHTS > 0 ) && defined( RE_Direct )
	SpotLight spotLight;
	vec4 spotColor;
	vec3 spotLightCoord;
	bool inSpotLightMap;
	#if defined( USE_SHADOWMAP ) && NUM_SPOT_LIGHT_SHADOWS > 0
	SpotLightShadow spotLightShadow;
	#endif
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_SPOT_LIGHTS; i ++ ) {
		spotLight = spotLights[ i ];
		getSpotLightInfo( spotLight, geometryPosition, directLight );
		#if ( UNROLLED_LOOP_INDEX < NUM_SPOT_LIGHT_SHADOWS_WITH_MAPS )
		#define SPOT_LIGHT_MAP_INDEX UNROLLED_LOOP_INDEX
		#elif ( UNROLLED_LOOP_INDEX < NUM_SPOT_LIGHT_SHADOWS )
		#define SPOT_LIGHT_MAP_INDEX NUM_SPOT_LIGHT_MAPS
		#else
		#define SPOT_LIGHT_MAP_INDEX ( UNROLLED_LOOP_INDEX - NUM_SPOT_LIGHT_SHADOWS + NUM_SPOT_LIGHT_SHADOWS_WITH_MAPS )
		#endif
		#if ( SPOT_LIGHT_MAP_INDEX < NUM_SPOT_LIGHT_MAPS )
			spotLightCoord = vSpotLightCoord[ i ].xyz / vSpotLightCoord[ i ].w;
			inSpotLightMap = all( lessThan( abs( spotLightCoord * 2. - 1. ), vec3( 1.0 ) ) );
			spotColor = texture2D( spotLightMap[ SPOT_LIGHT_MAP_INDEX ], spotLightCoord.xy );
			directLight.color = inSpotLightMap ? directLight.color * spotColor.rgb : directLight.color;
		#endif
		#undef SPOT_LIGHT_MAP_INDEX
		#if defined( USE_SHADOWMAP ) && ( UNROLLED_LOOP_INDEX < NUM_SPOT_LIGHT_SHADOWS )
		spotLightShadow = spotLightShadows[ i ];
		directLight.color *= ( directLight.visible && receiveShadow ) ? getShadow( spotShadowMap[ i ], spotLightShadow.shadowMapSize, spotLightShadow.shadowBias, spotLightShadow.shadowRadius, vSpotLightCoord[ i ] ) : 1.0;
		#endif
		RE_Direct( directLight, geometryPosition, geometryNormal, geometryViewDir, geometryClearcoatNormal, material, reflectedLight );
	}
	#pragma unroll_loop_end
#endif
#if ( NUM_DIR_LIGHTS > 0 ) && defined( RE_Direct )
	DirectionalLight directionalLight;
	#if defined( USE_SHADOWMAP ) && NUM_DIR_LIGHT_SHADOWS > 0
	DirectionalLightShadow directionalLightShadow;
	#endif
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_DIR_LIGHTS; i ++ ) {
		directionalLight = directionalLights[ i ];
		getDirectionalLightInfo( directionalLight, directLight );
		#if defined( USE_SHADOWMAP ) && ( UNROLLED_LOOP_INDEX < NUM_DIR_LIGHT_SHADOWS )
		directionalLightShadow = directionalLightShadows[ i ];
		directLight.color *= ( directLight.visible && receiveShadow ) ? getShadow( directionalShadowMap[ i ], directionalLightShadow.shadowMapSize, directionalLightShadow.shadowBias, directionalLightShadow.shadowRadius, vDirectionalShadowCoord[ i ] ) : 1.0;
		#endif
		RE_Direct( directLight, geometryPosition, geometryNormal, geometryViewDir, geometryClearcoatNormal, material, reflectedLight );
	}
	#pragma unroll_loop_end
#endif
#if ( NUM_RECT_AREA_LIGHTS > 0 ) && defined( RE_Direct_RectArea )
	RectAreaLight rectAreaLight;
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_RECT_AREA_LIGHTS; i ++ ) {
		rectAreaLight = rectAreaLights[ i ];
		RE_Direct_RectArea( rectAreaLight, geometryPosition, geometryNormal, geometryViewDir, geometryClearcoatNormal, material, reflectedLight );
	}
	#pragma unroll_loop_end
#endif
#if defined( RE_IndirectDiffuse )
	vec3 iblIrradiance = vec3( 0.0 );
	vec3 irradiance = getAmbientLightIrradiance( ambientLightColor );
	#if defined( USE_LIGHT_PROBES )
		irradiance += getLightProbeIrradiance( lightProbe, geometryNormal );
	#endif
	#if ( NUM_HEMI_LIGHTS > 0 )
		#pragma unroll_loop_start
		for ( int i = 0; i < NUM_HEMI_LIGHTS; i ++ ) {
			irradiance += getHemisphereLightIrradiance( hemisphereLights[ i ], geometryNormal );
		}
		#pragma unroll_loop_end
	#endif
#endif
#if defined( RE_IndirectSpecular )
	vec3 radiance = vec3( 0.0 );
	vec3 clearcoatRadiance = vec3( 0.0 );
#endif`,J0=`#if defined( RE_IndirectDiffuse )
	#ifdef USE_LIGHTMAP
		vec4 lightMapTexel = texture2D( lightMap, vLightMapUv );
		vec3 lightMapIrradiance = lightMapTexel.rgb * lightMapIntensity;
		irradiance += lightMapIrradiance;
	#endif
	#if defined( USE_ENVMAP ) && defined( STANDARD ) && defined( ENVMAP_TYPE_CUBE_UV )
		iblIrradiance += getIBLIrradiance( geometryNormal );
	#endif
#endif
#if defined( USE_ENVMAP ) && defined( RE_IndirectSpecular )
	#ifdef USE_ANISOTROPY
		radiance += getIBLAnisotropyRadiance( geometryViewDir, geometryNormal, material.roughness, material.anisotropyB, material.anisotropy );
	#else
		radiance += getIBLRadiance( geometryViewDir, geometryNormal, material.roughness );
	#endif
	#ifdef USE_CLEARCOAT
		clearcoatRadiance += getIBLRadiance( geometryViewDir, geometryClearcoatNormal, material.clearcoatRoughness );
	#endif
#endif`,$0=`#if defined( RE_IndirectDiffuse )
	RE_IndirectDiffuse( irradiance, geometryPosition, geometryNormal, geometryViewDir, geometryClearcoatNormal, material, reflectedLight );
#endif
#if defined( RE_IndirectSpecular )
	RE_IndirectSpecular( radiance, iblIrradiance, clearcoatRadiance, geometryPosition, geometryNormal, geometryViewDir, geometryClearcoatNormal, material, reflectedLight );
#endif`,Q0=`#if defined( USE_LOGDEPTHBUF )
	gl_FragDepth = vIsPerspective == 0.0 ? gl_FragCoord.z : log2( vFragDepth ) * logDepthBufFC * 0.5;
#endif`,e_=`#if defined( USE_LOGDEPTHBUF )
	uniform float logDepthBufFC;
	varying float vFragDepth;
	varying float vIsPerspective;
#endif`,t_=`#ifdef USE_LOGDEPTHBUF
	varying float vFragDepth;
	varying float vIsPerspective;
#endif`,n_=`#ifdef USE_LOGDEPTHBUF
	vFragDepth = 1.0 + gl_Position.w;
	vIsPerspective = float( isPerspectiveMatrix( projectionMatrix ) );
#endif`,i_=`#ifdef USE_MAP
	vec4 sampledDiffuseColor = texture2D( map, vMapUv );
	#ifdef DECODE_VIDEO_TEXTURE
		sampledDiffuseColor = vec4( mix( pow( sampledDiffuseColor.rgb * 0.9478672986 + vec3( 0.0521327014 ), vec3( 2.4 ) ), sampledDiffuseColor.rgb * 0.0773993808, vec3( lessThanEqual( sampledDiffuseColor.rgb, vec3( 0.04045 ) ) ) ), sampledDiffuseColor.w );
	
	#endif
	diffuseColor *= sampledDiffuseColor;
#endif`,r_=`#ifdef USE_MAP
	uniform sampler2D map;
#endif`,s_=`#if defined( USE_MAP ) || defined( USE_ALPHAMAP )
	#if defined( USE_POINTS_UV )
		vec2 uv = vUv;
	#else
		vec2 uv = ( uvTransform * vec3( gl_PointCoord.x, 1.0 - gl_PointCoord.y, 1 ) ).xy;
	#endif
#endif
#ifdef USE_MAP
	diffuseColor *= texture2D( map, uv );
#endif
#ifdef USE_ALPHAMAP
	diffuseColor.a *= texture2D( alphaMap, uv ).g;
#endif`,o_=`#if defined( USE_POINTS_UV )
	varying vec2 vUv;
#else
	#if defined( USE_MAP ) || defined( USE_ALPHAMAP )
		uniform mat3 uvTransform;
	#endif
#endif
#ifdef USE_MAP
	uniform sampler2D map;
#endif
#ifdef USE_ALPHAMAP
	uniform sampler2D alphaMap;
#endif`,a_=`float metalnessFactor = metalness;
#ifdef USE_METALNESSMAP
	vec4 texelMetalness = texture2D( metalnessMap, vMetalnessMapUv );
	metalnessFactor *= texelMetalness.b;
#endif`,l_=`#ifdef USE_METALNESSMAP
	uniform sampler2D metalnessMap;
#endif`,c_=`#ifdef USE_INSTANCING_MORPH
	float morphTargetInfluences[MORPHTARGETS_COUNT];
	float morphTargetBaseInfluence = texelFetch( morphTexture, ivec2( 0, gl_InstanceID ), 0 ).r;
	for ( int i = 0; i < MORPHTARGETS_COUNT; i ++ ) {
		morphTargetInfluences[i] =  texelFetch( morphTexture, ivec2( i + 1, gl_InstanceID ), 0 ).r;
	}
#endif`,h_=`#if defined( USE_MORPHCOLORS ) && defined( MORPHTARGETS_TEXTURE )
	vColor *= morphTargetBaseInfluence;
	for ( int i = 0; i < MORPHTARGETS_COUNT; i ++ ) {
		#if defined( USE_COLOR_ALPHA )
			if ( morphTargetInfluences[ i ] != 0.0 ) vColor += getMorph( gl_VertexID, i, 2 ) * morphTargetInfluences[ i ];
		#elif defined( USE_COLOR )
			if ( morphTargetInfluences[ i ] != 0.0 ) vColor += getMorph( gl_VertexID, i, 2 ).rgb * morphTargetInfluences[ i ];
		#endif
	}
#endif`,u_=`#ifdef USE_MORPHNORMALS
	objectNormal *= morphTargetBaseInfluence;
	#ifdef MORPHTARGETS_TEXTURE
		for ( int i = 0; i < MORPHTARGETS_COUNT; i ++ ) {
			if ( morphTargetInfluences[ i ] != 0.0 ) objectNormal += getMorph( gl_VertexID, i, 1 ).xyz * morphTargetInfluences[ i ];
		}
	#else
		objectNormal += morphNormal0 * morphTargetInfluences[ 0 ];
		objectNormal += morphNormal1 * morphTargetInfluences[ 1 ];
		objectNormal += morphNormal2 * morphTargetInfluences[ 2 ];
		objectNormal += morphNormal3 * morphTargetInfluences[ 3 ];
	#endif
#endif`,d_=`#ifdef USE_MORPHTARGETS
	#ifndef USE_INSTANCING_MORPH
		uniform float morphTargetBaseInfluence;
	#endif
	#ifdef MORPHTARGETS_TEXTURE
		#ifndef USE_INSTANCING_MORPH
			uniform float morphTargetInfluences[ MORPHTARGETS_COUNT ];
		#endif
		uniform sampler2DArray morphTargetsTexture;
		uniform ivec2 morphTargetsTextureSize;
		vec4 getMorph( const in int vertexIndex, const in int morphTargetIndex, const in int offset ) {
			int texelIndex = vertexIndex * MORPHTARGETS_TEXTURE_STRIDE + offset;
			int y = texelIndex / morphTargetsTextureSize.x;
			int x = texelIndex - y * morphTargetsTextureSize.x;
			ivec3 morphUV = ivec3( x, y, morphTargetIndex );
			return texelFetch( morphTargetsTexture, morphUV, 0 );
		}
	#else
		#ifndef USE_MORPHNORMALS
			uniform float morphTargetInfluences[ 8 ];
		#else
			uniform float morphTargetInfluences[ 4 ];
		#endif
	#endif
#endif`,f_=`#ifdef USE_MORPHTARGETS
	transformed *= morphTargetBaseInfluence;
	#ifdef MORPHTARGETS_TEXTURE
		for ( int i = 0; i < MORPHTARGETS_COUNT; i ++ ) {
			if ( morphTargetInfluences[ i ] != 0.0 ) transformed += getMorph( gl_VertexID, i, 0 ).xyz * morphTargetInfluences[ i ];
		}
	#else
		transformed += morphTarget0 * morphTargetInfluences[ 0 ];
		transformed += morphTarget1 * morphTargetInfluences[ 1 ];
		transformed += morphTarget2 * morphTargetInfluences[ 2 ];
		transformed += morphTarget3 * morphTargetInfluences[ 3 ];
		#ifndef USE_MORPHNORMALS
			transformed += morphTarget4 * morphTargetInfluences[ 4 ];
			transformed += morphTarget5 * morphTargetInfluences[ 5 ];
			transformed += morphTarget6 * morphTargetInfluences[ 6 ];
			transformed += morphTarget7 * morphTargetInfluences[ 7 ];
		#endif
	#endif
#endif`,p_=`float faceDirection = gl_FrontFacing ? 1.0 : - 1.0;
#ifdef FLAT_SHADED
	vec3 fdx = dFdx( vViewPosition );
	vec3 fdy = dFdy( vViewPosition );
	vec3 normal = normalize( cross( fdx, fdy ) );
#else
	vec3 normal = normalize( vNormal );
	#ifdef DOUBLE_SIDED
		normal *= faceDirection;
	#endif
#endif
#if defined( USE_NORMALMAP_TANGENTSPACE ) || defined( USE_CLEARCOAT_NORMALMAP ) || defined( USE_ANISOTROPY )
	#ifdef USE_TANGENT
		mat3 tbn = mat3( normalize( vTangent ), normalize( vBitangent ), normal );
	#else
		mat3 tbn = getTangentFrame( - vViewPosition, normal,
		#if defined( USE_NORMALMAP )
			vNormalMapUv
		#elif defined( USE_CLEARCOAT_NORMALMAP )
			vClearcoatNormalMapUv
		#else
			vUv
		#endif
		);
	#endif
	#if defined( DOUBLE_SIDED ) && ! defined( FLAT_SHADED )
		tbn[0] *= faceDirection;
		tbn[1] *= faceDirection;
	#endif
#endif
#ifdef USE_CLEARCOAT_NORMALMAP
	#ifdef USE_TANGENT
		mat3 tbn2 = mat3( normalize( vTangent ), normalize( vBitangent ), normal );
	#else
		mat3 tbn2 = getTangentFrame( - vViewPosition, normal, vClearcoatNormalMapUv );
	#endif
	#if defined( DOUBLE_SIDED ) && ! defined( FLAT_SHADED )
		tbn2[0] *= faceDirection;
		tbn2[1] *= faceDirection;
	#endif
#endif
vec3 nonPerturbedNormal = normal;`,m_=`#ifdef USE_NORMALMAP_OBJECTSPACE
	normal = texture2D( normalMap, vNormalMapUv ).xyz * 2.0 - 1.0;
	#ifdef FLIP_SIDED
		normal = - normal;
	#endif
	#ifdef DOUBLE_SIDED
		normal = normal * faceDirection;
	#endif
	normal = normalize( normalMatrix * normal );
#elif defined( USE_NORMALMAP_TANGENTSPACE )
	vec3 mapN = texture2D( normalMap, vNormalMapUv ).xyz * 2.0 - 1.0;
	mapN.xy *= normalScale;
	normal = normalize( tbn * mapN );
#elif defined( USE_BUMPMAP )
	normal = perturbNormalArb( - vViewPosition, normal, dHdxy_fwd(), faceDirection );
#endif`,g_=`#ifndef FLAT_SHADED
	varying vec3 vNormal;
	#ifdef USE_TANGENT
		varying vec3 vTangent;
		varying vec3 vBitangent;
	#endif
#endif`,v_=`#ifndef FLAT_SHADED
	varying vec3 vNormal;
	#ifdef USE_TANGENT
		varying vec3 vTangent;
		varying vec3 vBitangent;
	#endif
#endif`,__=`#ifndef FLAT_SHADED
	vNormal = normalize( transformedNormal );
	#ifdef USE_TANGENT
		vTangent = normalize( transformedTangent );
		vBitangent = normalize( cross( vNormal, vTangent ) * tangent.w );
	#endif
#endif`,y_=`#ifdef USE_NORMALMAP
	uniform sampler2D normalMap;
	uniform vec2 normalScale;
#endif
#ifdef USE_NORMALMAP_OBJECTSPACE
	uniform mat3 normalMatrix;
#endif
#if ! defined ( USE_TANGENT ) && ( defined ( USE_NORMALMAP_TANGENTSPACE ) || defined ( USE_CLEARCOAT_NORMALMAP ) || defined( USE_ANISOTROPY ) )
	mat3 getTangentFrame( vec3 eye_pos, vec3 surf_norm, vec2 uv ) {
		vec3 q0 = dFdx( eye_pos.xyz );
		vec3 q1 = dFdy( eye_pos.xyz );
		vec2 st0 = dFdx( uv.st );
		vec2 st1 = dFdy( uv.st );
		vec3 N = surf_norm;
		vec3 q1perp = cross( q1, N );
		vec3 q0perp = cross( N, q0 );
		vec3 T = q1perp * st0.x + q0perp * st1.x;
		vec3 B = q1perp * st0.y + q0perp * st1.y;
		float det = max( dot( T, T ), dot( B, B ) );
		float scale = ( det == 0.0 ) ? 0.0 : inversesqrt( det );
		return mat3( T * scale, B * scale, N );
	}
#endif`,x_=`#ifdef USE_CLEARCOAT
	vec3 clearcoatNormal = nonPerturbedNormal;
#endif`,M_=`#ifdef USE_CLEARCOAT_NORMALMAP
	vec3 clearcoatMapN = texture2D( clearcoatNormalMap, vClearcoatNormalMapUv ).xyz * 2.0 - 1.0;
	clearcoatMapN.xy *= clearcoatNormalScale;
	clearcoatNormal = normalize( tbn2 * clearcoatMapN );
#endif`,b_=`#ifdef USE_CLEARCOATMAP
	uniform sampler2D clearcoatMap;
#endif
#ifdef USE_CLEARCOAT_NORMALMAP
	uniform sampler2D clearcoatNormalMap;
	uniform vec2 clearcoatNormalScale;
#endif
#ifdef USE_CLEARCOAT_ROUGHNESSMAP
	uniform sampler2D clearcoatRoughnessMap;
#endif`,S_=`#ifdef USE_IRIDESCENCEMAP
	uniform sampler2D iridescenceMap;
#endif
#ifdef USE_IRIDESCENCE_THICKNESSMAP
	uniform sampler2D iridescenceThicknessMap;
#endif`,w_=`#ifdef OPAQUE
diffuseColor.a = 1.0;
#endif
#ifdef USE_TRANSMISSION
diffuseColor.a *= material.transmissionAlpha;
#endif
gl_FragColor = vec4( outgoingLight, diffuseColor.a );`,E_=`vec3 packNormalToRGB( const in vec3 normal ) {
	return normalize( normal ) * 0.5 + 0.5;
}
vec3 unpackRGBToNormal( const in vec3 rgb ) {
	return 2.0 * rgb.xyz - 1.0;
}
const float PackUpscale = 256. / 255.;const float UnpackDownscale = 255. / 256.;
const vec3 PackFactors = vec3( 256. * 256. * 256., 256. * 256., 256. );
const vec4 UnpackFactors = UnpackDownscale / vec4( PackFactors, 1. );
const float ShiftRight8 = 1. / 256.;
vec4 packDepthToRGBA( const in float v ) {
	vec4 r = vec4( fract( v * PackFactors ), v );
	r.yzw -= r.xyz * ShiftRight8;	return r * PackUpscale;
}
float unpackRGBAToDepth( const in vec4 v ) {
	return dot( v, UnpackFactors );
}
vec2 packDepthToRG( in highp float v ) {
	return packDepthToRGBA( v ).yx;
}
float unpackRGToDepth( const in highp vec2 v ) {
	return unpackRGBAToDepth( vec4( v.xy, 0.0, 0.0 ) );
}
vec4 pack2HalfToRGBA( vec2 v ) {
	vec4 r = vec4( v.x, fract( v.x * 255.0 ), v.y, fract( v.y * 255.0 ) );
	return vec4( r.x - r.y / 255.0, r.y, r.z - r.w / 255.0, r.w );
}
vec2 unpackRGBATo2Half( vec4 v ) {
	return vec2( v.x + ( v.y / 255.0 ), v.z + ( v.w / 255.0 ) );
}
float viewZToOrthographicDepth( const in float viewZ, const in float near, const in float far ) {
	return ( viewZ + near ) / ( near - far );
}
float orthographicDepthToViewZ( const in float depth, const in float near, const in float far ) {
	return depth * ( near - far ) - near;
}
float viewZToPerspectiveDepth( const in float viewZ, const in float near, const in float far ) {
	return ( ( near + viewZ ) * far ) / ( ( far - near ) * viewZ );
}
float perspectiveDepthToViewZ( const in float depth, const in float near, const in float far ) {
	return ( near * far ) / ( ( far - near ) * depth - far );
}`,T_=`#ifdef PREMULTIPLIED_ALPHA
	gl_FragColor.rgb *= gl_FragColor.a;
#endif`,A_=`vec4 mvPosition = vec4( transformed, 1.0 );
#ifdef USE_BATCHING
	mvPosition = batchingMatrix * mvPosition;
#endif
#ifdef USE_INSTANCING
	mvPosition = instanceMatrix * mvPosition;
#endif
mvPosition = modelViewMatrix * mvPosition;
gl_Position = projectionMatrix * mvPosition;`,C_=`#ifdef DITHERING
	gl_FragColor.rgb = dithering( gl_FragColor.rgb );
#endif`,R_=`#ifdef DITHERING
	vec3 dithering( vec3 color ) {
		float grid_position = rand( gl_FragCoord.xy );
		vec3 dither_shift_RGB = vec3( 0.25 / 255.0, -0.25 / 255.0, 0.25 / 255.0 );
		dither_shift_RGB = mix( 2.0 * dither_shift_RGB, -2.0 * dither_shift_RGB, grid_position );
		return color + dither_shift_RGB;
	}
#endif`,P_=`float roughnessFactor = roughness;
#ifdef USE_ROUGHNESSMAP
	vec4 texelRoughness = texture2D( roughnessMap, vRoughnessMapUv );
	roughnessFactor *= texelRoughness.g;
#endif`,I_=`#ifdef USE_ROUGHNESSMAP
	uniform sampler2D roughnessMap;
#endif`,L_=`#if NUM_SPOT_LIGHT_COORDS > 0
	varying vec4 vSpotLightCoord[ NUM_SPOT_LIGHT_COORDS ];
#endif
#if NUM_SPOT_LIGHT_MAPS > 0
	uniform sampler2D spotLightMap[ NUM_SPOT_LIGHT_MAPS ];
#endif
#ifdef USE_SHADOWMAP
	#if NUM_DIR_LIGHT_SHADOWS > 0
		uniform sampler2D directionalShadowMap[ NUM_DIR_LIGHT_SHADOWS ];
		varying vec4 vDirectionalShadowCoord[ NUM_DIR_LIGHT_SHADOWS ];
		struct DirectionalLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
		};
		uniform DirectionalLightShadow directionalLightShadows[ NUM_DIR_LIGHT_SHADOWS ];
	#endif
	#if NUM_SPOT_LIGHT_SHADOWS > 0
		uniform sampler2D spotShadowMap[ NUM_SPOT_LIGHT_SHADOWS ];
		struct SpotLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
		};
		uniform SpotLightShadow spotLightShadows[ NUM_SPOT_LIGHT_SHADOWS ];
	#endif
	#if NUM_POINT_LIGHT_SHADOWS > 0
		uniform sampler2D pointShadowMap[ NUM_POINT_LIGHT_SHADOWS ];
		varying vec4 vPointShadowCoord[ NUM_POINT_LIGHT_SHADOWS ];
		struct PointLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
			float shadowCameraNear;
			float shadowCameraFar;
		};
		uniform PointLightShadow pointLightShadows[ NUM_POINT_LIGHT_SHADOWS ];
	#endif
	float texture2DCompare( sampler2D depths, vec2 uv, float compare ) {
		return step( compare, unpackRGBAToDepth( texture2D( depths, uv ) ) );
	}
	vec2 texture2DDistribution( sampler2D shadow, vec2 uv ) {
		return unpackRGBATo2Half( texture2D( shadow, uv ) );
	}
	float VSMShadow (sampler2D shadow, vec2 uv, float compare ){
		float occlusion = 1.0;
		vec2 distribution = texture2DDistribution( shadow, uv );
		float hard_shadow = step( compare , distribution.x );
		if (hard_shadow != 1.0 ) {
			float distance = compare - distribution.x ;
			float variance = max( 0.00000, distribution.y * distribution.y );
			float softness_probability = variance / (variance + distance * distance );			softness_probability = clamp( ( softness_probability - 0.3 ) / ( 0.95 - 0.3 ), 0.0, 1.0 );			occlusion = clamp( max( hard_shadow, softness_probability ), 0.0, 1.0 );
		}
		return occlusion;
	}
	float getShadow( sampler2D shadowMap, vec2 shadowMapSize, float shadowBias, float shadowRadius, vec4 shadowCoord ) {
		float shadow = 1.0;
		shadowCoord.xyz /= shadowCoord.w;
		shadowCoord.z += shadowBias;
		bool inFrustum = shadowCoord.x >= 0.0 && shadowCoord.x <= 1.0 && shadowCoord.y >= 0.0 && shadowCoord.y <= 1.0;
		bool frustumTest = inFrustum && shadowCoord.z <= 1.0;
		if ( frustumTest ) {
		#if defined( SHADOWMAP_TYPE_PCF )
			vec2 texelSize = vec2( 1.0 ) / shadowMapSize;
			float dx0 = - texelSize.x * shadowRadius;
			float dy0 = - texelSize.y * shadowRadius;
			float dx1 = + texelSize.x * shadowRadius;
			float dy1 = + texelSize.y * shadowRadius;
			float dx2 = dx0 / 2.0;
			float dy2 = dy0 / 2.0;
			float dx3 = dx1 / 2.0;
			float dy3 = dy1 / 2.0;
			shadow = (
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, dy0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, dy0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx2, dy2 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy2 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx3, dy2 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, 0.0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx2, 0.0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy, shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx3, 0.0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, 0.0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx2, dy3 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy3 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx3, dy3 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx0, dy1 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( 0.0, dy1 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, shadowCoord.xy + vec2( dx1, dy1 ), shadowCoord.z )
			) * ( 1.0 / 17.0 );
		#elif defined( SHADOWMAP_TYPE_PCF_SOFT )
			vec2 texelSize = vec2( 1.0 ) / shadowMapSize;
			float dx = texelSize.x;
			float dy = texelSize.y;
			vec2 uv = shadowCoord.xy;
			vec2 f = fract( uv * shadowMapSize + 0.5 );
			uv -= f * texelSize;
			shadow = (
				texture2DCompare( shadowMap, uv, shadowCoord.z ) +
				texture2DCompare( shadowMap, uv + vec2( dx, 0.0 ), shadowCoord.z ) +
				texture2DCompare( shadowMap, uv + vec2( 0.0, dy ), shadowCoord.z ) +
				texture2DCompare( shadowMap, uv + texelSize, shadowCoord.z ) +
				mix( texture2DCompare( shadowMap, uv + vec2( -dx, 0.0 ), shadowCoord.z ),
					 texture2DCompare( shadowMap, uv + vec2( 2.0 * dx, 0.0 ), shadowCoord.z ),
					 f.x ) +
				mix( texture2DCompare( shadowMap, uv + vec2( -dx, dy ), shadowCoord.z ),
					 texture2DCompare( shadowMap, uv + vec2( 2.0 * dx, dy ), shadowCoord.z ),
					 f.x ) +
				mix( texture2DCompare( shadowMap, uv + vec2( 0.0, -dy ), shadowCoord.z ),
					 texture2DCompare( shadowMap, uv + vec2( 0.0, 2.0 * dy ), shadowCoord.z ),
					 f.y ) +
				mix( texture2DCompare( shadowMap, uv + vec2( dx, -dy ), shadowCoord.z ),
					 texture2DCompare( shadowMap, uv + vec2( dx, 2.0 * dy ), shadowCoord.z ),
					 f.y ) +
				mix( mix( texture2DCompare( shadowMap, uv + vec2( -dx, -dy ), shadowCoord.z ),
						  texture2DCompare( shadowMap, uv + vec2( 2.0 * dx, -dy ), shadowCoord.z ),
						  f.x ),
					 mix( texture2DCompare( shadowMap, uv + vec2( -dx, 2.0 * dy ), shadowCoord.z ),
						  texture2DCompare( shadowMap, uv + vec2( 2.0 * dx, 2.0 * dy ), shadowCoord.z ),
						  f.x ),
					 f.y )
			) * ( 1.0 / 9.0 );
		#elif defined( SHADOWMAP_TYPE_VSM )
			shadow = VSMShadow( shadowMap, shadowCoord.xy, shadowCoord.z );
		#else
			shadow = texture2DCompare( shadowMap, shadowCoord.xy, shadowCoord.z );
		#endif
		}
		return shadow;
	}
	vec2 cubeToUV( vec3 v, float texelSizeY ) {
		vec3 absV = abs( v );
		float scaleToCube = 1.0 / max( absV.x, max( absV.y, absV.z ) );
		absV *= scaleToCube;
		v *= scaleToCube * ( 1.0 - 2.0 * texelSizeY );
		vec2 planar = v.xy;
		float almostATexel = 1.5 * texelSizeY;
		float almostOne = 1.0 - almostATexel;
		if ( absV.z >= almostOne ) {
			if ( v.z > 0.0 )
				planar.x = 4.0 - v.x;
		} else if ( absV.x >= almostOne ) {
			float signX = sign( v.x );
			planar.x = v.z * signX + 2.0 * signX;
		} else if ( absV.y >= almostOne ) {
			float signY = sign( v.y );
			planar.x = v.x + 2.0 * signY + 2.0;
			planar.y = v.z * signY - 2.0;
		}
		return vec2( 0.125, 0.25 ) * planar + vec2( 0.375, 0.75 );
	}
	float getPointShadow( sampler2D shadowMap, vec2 shadowMapSize, float shadowBias, float shadowRadius, vec4 shadowCoord, float shadowCameraNear, float shadowCameraFar ) {
		float shadow = 1.0;
		vec3 lightToPosition = shadowCoord.xyz;
		
		float lightToPositionLength = length( lightToPosition );
		if ( lightToPositionLength - shadowCameraFar <= 0.0 && lightToPositionLength - shadowCameraNear >= 0.0 ) {
			float dp = ( lightToPositionLength - shadowCameraNear ) / ( shadowCameraFar - shadowCameraNear );			dp += shadowBias;
			vec3 bd3D = normalize( lightToPosition );
			vec2 texelSize = vec2( 1.0 ) / ( shadowMapSize * vec2( 4.0, 2.0 ) );
			#if defined( SHADOWMAP_TYPE_PCF ) || defined( SHADOWMAP_TYPE_PCF_SOFT ) || defined( SHADOWMAP_TYPE_VSM )
				vec2 offset = vec2( - 1, 1 ) * shadowRadius * texelSize.y;
				shadow = (
					texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xyy, texelSize.y ), dp ) +
					texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yyy, texelSize.y ), dp ) +
					texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xyx, texelSize.y ), dp ) +
					texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yyx, texelSize.y ), dp ) +
					texture2DCompare( shadowMap, cubeToUV( bd3D, texelSize.y ), dp ) +
					texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xxy, texelSize.y ), dp ) +
					texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yxy, texelSize.y ), dp ) +
					texture2DCompare( shadowMap, cubeToUV( bd3D + offset.xxx, texelSize.y ), dp ) +
					texture2DCompare( shadowMap, cubeToUV( bd3D + offset.yxx, texelSize.y ), dp )
				) * ( 1.0 / 9.0 );
			#else
				shadow = texture2DCompare( shadowMap, cubeToUV( bd3D, texelSize.y ), dp );
			#endif
		}
		return shadow;
	}
#endif`,D_=`#if NUM_SPOT_LIGHT_COORDS > 0
	uniform mat4 spotLightMatrix[ NUM_SPOT_LIGHT_COORDS ];
	varying vec4 vSpotLightCoord[ NUM_SPOT_LIGHT_COORDS ];
#endif
#ifdef USE_SHADOWMAP
	#if NUM_DIR_LIGHT_SHADOWS > 0
		uniform mat4 directionalShadowMatrix[ NUM_DIR_LIGHT_SHADOWS ];
		varying vec4 vDirectionalShadowCoord[ NUM_DIR_LIGHT_SHADOWS ];
		struct DirectionalLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
		};
		uniform DirectionalLightShadow directionalLightShadows[ NUM_DIR_LIGHT_SHADOWS ];
	#endif
	#if NUM_SPOT_LIGHT_SHADOWS > 0
		struct SpotLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
		};
		uniform SpotLightShadow spotLightShadows[ NUM_SPOT_LIGHT_SHADOWS ];
	#endif
	#if NUM_POINT_LIGHT_SHADOWS > 0
		uniform mat4 pointShadowMatrix[ NUM_POINT_LIGHT_SHADOWS ];
		varying vec4 vPointShadowCoord[ NUM_POINT_LIGHT_SHADOWS ];
		struct PointLightShadow {
			float shadowBias;
			float shadowNormalBias;
			float shadowRadius;
			vec2 shadowMapSize;
			float shadowCameraNear;
			float shadowCameraFar;
		};
		uniform PointLightShadow pointLightShadows[ NUM_POINT_LIGHT_SHADOWS ];
	#endif
#endif`,N_=`#if ( defined( USE_SHADOWMAP ) && ( NUM_DIR_LIGHT_SHADOWS > 0 || NUM_POINT_LIGHT_SHADOWS > 0 ) ) || ( NUM_SPOT_LIGHT_COORDS > 0 )
	vec3 shadowWorldNormal = inverseTransformDirection( transformedNormal, viewMatrix );
	vec4 shadowWorldPosition;
#endif
#if defined( USE_SHADOWMAP )
	#if NUM_DIR_LIGHT_SHADOWS > 0
		#pragma unroll_loop_start
		for ( int i = 0; i < NUM_DIR_LIGHT_SHADOWS; i ++ ) {
			shadowWorldPosition = worldPosition + vec4( shadowWorldNormal * directionalLightShadows[ i ].shadowNormalBias, 0 );
			vDirectionalShadowCoord[ i ] = directionalShadowMatrix[ i ] * shadowWorldPosition;
		}
		#pragma unroll_loop_end
	#endif
	#if NUM_POINT_LIGHT_SHADOWS > 0
		#pragma unroll_loop_start
		for ( int i = 0; i < NUM_POINT_LIGHT_SHADOWS; i ++ ) {
			shadowWorldPosition = worldPosition + vec4( shadowWorldNormal * pointLightShadows[ i ].shadowNormalBias, 0 );
			vPointShadowCoord[ i ] = pointShadowMatrix[ i ] * shadowWorldPosition;
		}
		#pragma unroll_loop_end
	#endif
#endif
#if NUM_SPOT_LIGHT_COORDS > 0
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_SPOT_LIGHT_COORDS; i ++ ) {
		shadowWorldPosition = worldPosition;
		#if ( defined( USE_SHADOWMAP ) && UNROLLED_LOOP_INDEX < NUM_SPOT_LIGHT_SHADOWS )
			shadowWorldPosition.xyz += shadowWorldNormal * spotLightShadows[ i ].shadowNormalBias;
		#endif
		vSpotLightCoord[ i ] = spotLightMatrix[ i ] * shadowWorldPosition;
	}
	#pragma unroll_loop_end
#endif`,O_=`float getShadowMask() {
	float shadow = 1.0;
	#ifdef USE_SHADOWMAP
	#if NUM_DIR_LIGHT_SHADOWS > 0
	DirectionalLightShadow directionalLight;
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_DIR_LIGHT_SHADOWS; i ++ ) {
		directionalLight = directionalLightShadows[ i ];
		shadow *= receiveShadow ? getShadow( directionalShadowMap[ i ], directionalLight.shadowMapSize, directionalLight.shadowBias, directionalLight.shadowRadius, vDirectionalShadowCoord[ i ] ) : 1.0;
	}
	#pragma unroll_loop_end
	#endif
	#if NUM_SPOT_LIGHT_SHADOWS > 0
	SpotLightShadow spotLight;
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_SPOT_LIGHT_SHADOWS; i ++ ) {
		spotLight = spotLightShadows[ i ];
		shadow *= receiveShadow ? getShadow( spotShadowMap[ i ], spotLight.shadowMapSize, spotLight.shadowBias, spotLight.shadowRadius, vSpotLightCoord[ i ] ) : 1.0;
	}
	#pragma unroll_loop_end
	#endif
	#if NUM_POINT_LIGHT_SHADOWS > 0
	PointLightShadow pointLight;
	#pragma unroll_loop_start
	for ( int i = 0; i < NUM_POINT_LIGHT_SHADOWS; i ++ ) {
		pointLight = pointLightShadows[ i ];
		shadow *= receiveShadow ? getPointShadow( pointShadowMap[ i ], pointLight.shadowMapSize, pointLight.shadowBias, pointLight.shadowRadius, vPointShadowCoord[ i ], pointLight.shadowCameraNear, pointLight.shadowCameraFar ) : 1.0;
	}
	#pragma unroll_loop_end
	#endif
	#endif
	return shadow;
}`,U_=`#ifdef USE_SKINNING
	mat4 boneMatX = getBoneMatrix( skinIndex.x );
	mat4 boneMatY = getBoneMatrix( skinIndex.y );
	mat4 boneMatZ = getBoneMatrix( skinIndex.z );
	mat4 boneMatW = getBoneMatrix( skinIndex.w );
#endif`,F_=`#ifdef USE_SKINNING
	uniform mat4 bindMatrix;
	uniform mat4 bindMatrixInverse;
	uniform highp sampler2D boneTexture;
	mat4 getBoneMatrix( const in float i ) {
		int size = textureSize( boneTexture, 0 ).x;
		int j = int( i ) * 4;
		int x = j % size;
		int y = j / size;
		vec4 v1 = texelFetch( boneTexture, ivec2( x, y ), 0 );
		vec4 v2 = texelFetch( boneTexture, ivec2( x + 1, y ), 0 );
		vec4 v3 = texelFetch( boneTexture, ivec2( x + 2, y ), 0 );
		vec4 v4 = texelFetch( boneTexture, ivec2( x + 3, y ), 0 );
		return mat4( v1, v2, v3, v4 );
	}
#endif`,B_=`#ifdef USE_SKINNING
	vec4 skinVertex = bindMatrix * vec4( transformed, 1.0 );
	vec4 skinned = vec4( 0.0 );
	skinned += boneMatX * skinVertex * skinWeight.x;
	skinned += boneMatY * skinVertex * skinWeight.y;
	skinned += boneMatZ * skinVertex * skinWeight.z;
	skinned += boneMatW * skinVertex * skinWeight.w;
	transformed = ( bindMatrixInverse * skinned ).xyz;
#endif`,z_=`#ifdef USE_SKINNING
	mat4 skinMatrix = mat4( 0.0 );
	skinMatrix += skinWeight.x * boneMatX;
	skinMatrix += skinWeight.y * boneMatY;
	skinMatrix += skinWeight.z * boneMatZ;
	skinMatrix += skinWeight.w * boneMatW;
	skinMatrix = bindMatrixInverse * skinMatrix * bindMatrix;
	objectNormal = vec4( skinMatrix * vec4( objectNormal, 0.0 ) ).xyz;
	#ifdef USE_TANGENT
		objectTangent = vec4( skinMatrix * vec4( objectTangent, 0.0 ) ).xyz;
	#endif
#endif`,k_=`float specularStrength;
#ifdef USE_SPECULARMAP
	vec4 texelSpecular = texture2D( specularMap, vSpecularMapUv );
	specularStrength = texelSpecular.r;
#else
	specularStrength = 1.0;
#endif`,V_=`#ifdef USE_SPECULARMAP
	uniform sampler2D specularMap;
#endif`,H_=`#if defined( TONE_MAPPING )
	gl_FragColor.rgb = toneMapping( gl_FragColor.rgb );
#endif`,G_=`#ifndef saturate
#define saturate( a ) clamp( a, 0.0, 1.0 )
#endif
uniform float toneMappingExposure;
vec3 LinearToneMapping( vec3 color ) {
	return saturate( toneMappingExposure * color );
}
vec3 ReinhardToneMapping( vec3 color ) {
	color *= toneMappingExposure;
	return saturate( color / ( vec3( 1.0 ) + color ) );
}
vec3 OptimizedCineonToneMapping( vec3 color ) {
	color *= toneMappingExposure;
	color = max( vec3( 0.0 ), color - 0.004 );
	return pow( ( color * ( 6.2 * color + 0.5 ) ) / ( color * ( 6.2 * color + 1.7 ) + 0.06 ), vec3( 2.2 ) );
}
vec3 RRTAndODTFit( vec3 v ) {
	vec3 a = v * ( v + 0.0245786 ) - 0.000090537;
	vec3 b = v * ( 0.983729 * v + 0.4329510 ) + 0.238081;
	return a / b;
}
vec3 ACESFilmicToneMapping( vec3 color ) {
	const mat3 ACESInputMat = mat3(
		vec3( 0.59719, 0.07600, 0.02840 ),		vec3( 0.35458, 0.90834, 0.13383 ),
		vec3( 0.04823, 0.01566, 0.83777 )
	);
	const mat3 ACESOutputMat = mat3(
		vec3(  1.60475, -0.10208, -0.00327 ),		vec3( -0.53108,  1.10813, -0.07276 ),
		vec3( -0.07367, -0.00605,  1.07602 )
	);
	color *= toneMappingExposure / 0.6;
	color = ACESInputMat * color;
	color = RRTAndODTFit( color );
	color = ACESOutputMat * color;
	return saturate( color );
}
const mat3 LINEAR_REC2020_TO_LINEAR_SRGB = mat3(
	vec3( 1.6605, - 0.1246, - 0.0182 ),
	vec3( - 0.5876, 1.1329, - 0.1006 ),
	vec3( - 0.0728, - 0.0083, 1.1187 )
);
const mat3 LINEAR_SRGB_TO_LINEAR_REC2020 = mat3(
	vec3( 0.6274, 0.0691, 0.0164 ),
	vec3( 0.3293, 0.9195, 0.0880 ),
	vec3( 0.0433, 0.0113, 0.8956 )
);
vec3 agxDefaultContrastApprox( vec3 x ) {
	vec3 x2 = x * x;
	vec3 x4 = x2 * x2;
	return + 15.5 * x4 * x2
		- 40.14 * x4 * x
		+ 31.96 * x4
		- 6.868 * x2 * x
		+ 0.4298 * x2
		+ 0.1191 * x
		- 0.00232;
}
vec3 AgXToneMapping( vec3 color ) {
	const mat3 AgXInsetMatrix = mat3(
		vec3( 0.856627153315983, 0.137318972929847, 0.11189821299995 ),
		vec3( 0.0951212405381588, 0.761241990602591, 0.0767994186031903 ),
		vec3( 0.0482516061458583, 0.101439036467562, 0.811302368396859 )
	);
	const mat3 AgXOutsetMatrix = mat3(
		vec3( 1.1271005818144368, - 0.1413297634984383, - 0.14132976349843826 ),
		vec3( - 0.11060664309660323, 1.157823702216272, - 0.11060664309660294 ),
		vec3( - 0.016493938717834573, - 0.016493938717834257, 1.2519364065950405 )
	);
	const float AgxMinEv = - 12.47393;	const float AgxMaxEv = 4.026069;
	color *= toneMappingExposure;
	color = LINEAR_SRGB_TO_LINEAR_REC2020 * color;
	color = AgXInsetMatrix * color;
	color = max( color, 1e-10 );	color = log2( color );
	color = ( color - AgxMinEv ) / ( AgxMaxEv - AgxMinEv );
	color = clamp( color, 0.0, 1.0 );
	color = agxDefaultContrastApprox( color );
	color = AgXOutsetMatrix * color;
	color = pow( max( vec3( 0.0 ), color ), vec3( 2.2 ) );
	color = LINEAR_REC2020_TO_LINEAR_SRGB * color;
	color = clamp( color, 0.0, 1.0 );
	return color;
}
vec3 NeutralToneMapping( vec3 color ) {
	const float StartCompression = 0.8 - 0.04;
	const float Desaturation = 0.15;
	color *= toneMappingExposure;
	float x = min( color.r, min( color.g, color.b ) );
	float offset = x < 0.08 ? x - 6.25 * x * x : 0.04;
	color -= offset;
	float peak = max( color.r, max( color.g, color.b ) );
	if ( peak < StartCompression ) return color;
	float d = 1. - StartCompression;
	float newPeak = 1. - d * d / ( peak + d - StartCompression );
	color *= newPeak / peak;
	float g = 1. - 1. / ( Desaturation * ( peak - newPeak ) + 1. );
	return mix( color, vec3( newPeak ), g );
}
vec3 CustomToneMapping( vec3 color ) { return color; }`,W_=`#ifdef USE_TRANSMISSION
	material.transmission = transmission;
	material.transmissionAlpha = 1.0;
	material.thickness = thickness;
	material.attenuationDistance = attenuationDistance;
	material.attenuationColor = attenuationColor;
	#ifdef USE_TRANSMISSIONMAP
		material.transmission *= texture2D( transmissionMap, vTransmissionMapUv ).r;
	#endif
	#ifdef USE_THICKNESSMAP
		material.thickness *= texture2D( thicknessMap, vThicknessMapUv ).g;
	#endif
	vec3 pos = vWorldPosition;
	vec3 v = normalize( cameraPosition - pos );
	vec3 n = inverseTransformDirection( normal, viewMatrix );
	vec4 transmitted = getIBLVolumeRefraction(
		n, v, material.roughness, material.diffuseColor, material.specularColor, material.specularF90,
		pos, modelMatrix, viewMatrix, projectionMatrix, material.dispersion, material.ior, material.thickness,
		material.attenuationColor, material.attenuationDistance );
	material.transmissionAlpha = mix( material.transmissionAlpha, transmitted.a, material.transmission );
	totalDiffuse = mix( totalDiffuse, transmitted.rgb, material.transmission );
#endif`,X_=`#ifdef USE_TRANSMISSION
	uniform float transmission;
	uniform float thickness;
	uniform float attenuationDistance;
	uniform vec3 attenuationColor;
	#ifdef USE_TRANSMISSIONMAP
		uniform sampler2D transmissionMap;
	#endif
	#ifdef USE_THICKNESSMAP
		uniform sampler2D thicknessMap;
	#endif
	uniform vec2 transmissionSamplerSize;
	uniform sampler2D transmissionSamplerMap;
	uniform mat4 modelMatrix;
	uniform mat4 projectionMatrix;
	varying vec3 vWorldPosition;
	float w0( float a ) {
		return ( 1.0 / 6.0 ) * ( a * ( a * ( - a + 3.0 ) - 3.0 ) + 1.0 );
	}
	float w1( float a ) {
		return ( 1.0 / 6.0 ) * ( a *  a * ( 3.0 * a - 6.0 ) + 4.0 );
	}
	float w2( float a ){
		return ( 1.0 / 6.0 ) * ( a * ( a * ( - 3.0 * a + 3.0 ) + 3.0 ) + 1.0 );
	}
	float w3( float a ) {
		return ( 1.0 / 6.0 ) * ( a * a * a );
	}
	float g0( float a ) {
		return w0( a ) + w1( a );
	}
	float g1( float a ) {
		return w2( a ) + w3( a );
	}
	float h0( float a ) {
		return - 1.0 + w1( a ) / ( w0( a ) + w1( a ) );
	}
	float h1( float a ) {
		return 1.0 + w3( a ) / ( w2( a ) + w3( a ) );
	}
	vec4 bicubic( sampler2D tex, vec2 uv, vec4 texelSize, float lod ) {
		uv = uv * texelSize.zw + 0.5;
		vec2 iuv = floor( uv );
		vec2 fuv = fract( uv );
		float g0x = g0( fuv.x );
		float g1x = g1( fuv.x );
		float h0x = h0( fuv.x );
		float h1x = h1( fuv.x );
		float h0y = h0( fuv.y );
		float h1y = h1( fuv.y );
		vec2 p0 = ( vec2( iuv.x + h0x, iuv.y + h0y ) - 0.5 ) * texelSize.xy;
		vec2 p1 = ( vec2( iuv.x + h1x, iuv.y + h0y ) - 0.5 ) * texelSize.xy;
		vec2 p2 = ( vec2( iuv.x + h0x, iuv.y + h1y ) - 0.5 ) * texelSize.xy;
		vec2 p3 = ( vec2( iuv.x + h1x, iuv.y + h1y ) - 0.5 ) * texelSize.xy;
		return g0( fuv.y ) * ( g0x * textureLod( tex, p0, lod ) + g1x * textureLod( tex, p1, lod ) ) +
			g1( fuv.y ) * ( g0x * textureLod( tex, p2, lod ) + g1x * textureLod( tex, p3, lod ) );
	}
	vec4 textureBicubic( sampler2D sampler, vec2 uv, float lod ) {
		vec2 fLodSize = vec2( textureSize( sampler, int( lod ) ) );
		vec2 cLodSize = vec2( textureSize( sampler, int( lod + 1.0 ) ) );
		vec2 fLodSizeInv = 1.0 / fLodSize;
		vec2 cLodSizeInv = 1.0 / cLodSize;
		vec4 fSample = bicubic( sampler, uv, vec4( fLodSizeInv, fLodSize ), floor( lod ) );
		vec4 cSample = bicubic( sampler, uv, vec4( cLodSizeInv, cLodSize ), ceil( lod ) );
		return mix( fSample, cSample, fract( lod ) );
	}
	vec3 getVolumeTransmissionRay( const in vec3 n, const in vec3 v, const in float thickness, const in float ior, const in mat4 modelMatrix ) {
		vec3 refractionVector = refract( - v, normalize( n ), 1.0 / ior );
		vec3 modelScale;
		modelScale.x = length( vec3( modelMatrix[ 0 ].xyz ) );
		modelScale.y = length( vec3( modelMatrix[ 1 ].xyz ) );
		modelScale.z = length( vec3( modelMatrix[ 2 ].xyz ) );
		return normalize( refractionVector ) * thickness * modelScale;
	}
	float applyIorToRoughness( const in float roughness, const in float ior ) {
		return roughness * clamp( ior * 2.0 - 2.0, 0.0, 1.0 );
	}
	vec4 getTransmissionSample( const in vec2 fragCoord, const in float roughness, const in float ior ) {
		float lod = log2( transmissionSamplerSize.x ) * applyIorToRoughness( roughness, ior );
		return textureBicubic( transmissionSamplerMap, fragCoord.xy, lod );
	}
	vec3 volumeAttenuation( const in float transmissionDistance, const in vec3 attenuationColor, const in float attenuationDistance ) {
		if ( isinf( attenuationDistance ) ) {
			return vec3( 1.0 );
		} else {
			vec3 attenuationCoefficient = -log( attenuationColor ) / attenuationDistance;
			vec3 transmittance = exp( - attenuationCoefficient * transmissionDistance );			return transmittance;
		}
	}
	vec4 getIBLVolumeRefraction( const in vec3 n, const in vec3 v, const in float roughness, const in vec3 diffuseColor,
		const in vec3 specularColor, const in float specularF90, const in vec3 position, const in mat4 modelMatrix,
		const in mat4 viewMatrix, const in mat4 projMatrix, const in float dispersion, const in float ior, const in float thickness,
		const in vec3 attenuationColor, const in float attenuationDistance ) {
		vec4 transmittedLight;
		vec3 transmittance;
		#ifdef USE_DISPERSION
			float halfSpread = ( ior - 1.0 ) * 0.025 * dispersion;
			vec3 iors = vec3( ior - halfSpread, ior, ior + halfSpread );
			for ( int i = 0; i < 3; i ++ ) {
				vec3 transmissionRay = getVolumeTransmissionRay( n, v, thickness, iors[ i ], modelMatrix );
				vec3 refractedRayExit = position + transmissionRay;
		
				vec4 ndcPos = projMatrix * viewMatrix * vec4( refractedRayExit, 1.0 );
				vec2 refractionCoords = ndcPos.xy / ndcPos.w;
				refractionCoords += 1.0;
				refractionCoords /= 2.0;
		
				vec4 transmissionSample = getTransmissionSample( refractionCoords, roughness, iors[ i ] );
				transmittedLight[ i ] = transmissionSample[ i ];
				transmittedLight.a += transmissionSample.a;
				transmittance[ i ] = diffuseColor[ i ] * volumeAttenuation( length( transmissionRay ), attenuationColor, attenuationDistance )[ i ];
			}
			transmittedLight.a /= 3.0;
		
		#else
		
			vec3 transmissionRay = getVolumeTransmissionRay( n, v, thickness, ior, modelMatrix );
			vec3 refractedRayExit = position + transmissionRay;
			vec4 ndcPos = projMatrix * viewMatrix * vec4( refractedRayExit, 1.0 );
			vec2 refractionCoords = ndcPos.xy / ndcPos.w;
			refractionCoords += 1.0;
			refractionCoords /= 2.0;
			transmittedLight = getTransmissionSample( refractionCoords, roughness, ior );
			transmittance = diffuseColor * volumeAttenuation( length( transmissionRay ), attenuationColor, attenuationDistance );
		
		#endif
		vec3 attenuatedColor = transmittance * transmittedLight.rgb;
		vec3 F = EnvironmentBRDF( n, v, specularColor, specularF90, roughness );
		float transmittanceFactor = ( transmittance.r + transmittance.g + transmittance.b ) / 3.0;
		return vec4( ( 1.0 - F ) * attenuatedColor, 1.0 - ( 1.0 - transmittedLight.a ) * transmittanceFactor );
	}
#endif`,Y_=`#if defined( USE_UV ) || defined( USE_ANISOTROPY )
	varying vec2 vUv;
#endif
#ifdef USE_MAP
	varying vec2 vMapUv;
#endif
#ifdef USE_ALPHAMAP
	varying vec2 vAlphaMapUv;
#endif
#ifdef USE_LIGHTMAP
	varying vec2 vLightMapUv;
#endif
#ifdef USE_AOMAP
	varying vec2 vAoMapUv;
#endif
#ifdef USE_BUMPMAP
	varying vec2 vBumpMapUv;
#endif
#ifdef USE_NORMALMAP
	varying vec2 vNormalMapUv;
#endif
#ifdef USE_EMISSIVEMAP
	varying vec2 vEmissiveMapUv;
#endif
#ifdef USE_METALNESSMAP
	varying vec2 vMetalnessMapUv;
#endif
#ifdef USE_ROUGHNESSMAP
	varying vec2 vRoughnessMapUv;
#endif
#ifdef USE_ANISOTROPYMAP
	varying vec2 vAnisotropyMapUv;
#endif
#ifdef USE_CLEARCOATMAP
	varying vec2 vClearcoatMapUv;
#endif
#ifdef USE_CLEARCOAT_NORMALMAP
	varying vec2 vClearcoatNormalMapUv;
#endif
#ifdef USE_CLEARCOAT_ROUGHNESSMAP
	varying vec2 vClearcoatRoughnessMapUv;
#endif
#ifdef USE_IRIDESCENCEMAP
	varying vec2 vIridescenceMapUv;
#endif
#ifdef USE_IRIDESCENCE_THICKNESSMAP
	varying vec2 vIridescenceThicknessMapUv;
#endif
#ifdef USE_SHEEN_COLORMAP
	varying vec2 vSheenColorMapUv;
#endif
#ifdef USE_SHEEN_ROUGHNESSMAP
	varying vec2 vSheenRoughnessMapUv;
#endif
#ifdef USE_SPECULARMAP
	varying vec2 vSpecularMapUv;
#endif
#ifdef USE_SPECULAR_COLORMAP
	varying vec2 vSpecularColorMapUv;
#endif
#ifdef USE_SPECULAR_INTENSITYMAP
	varying vec2 vSpecularIntensityMapUv;
#endif
#ifdef USE_TRANSMISSIONMAP
	uniform mat3 transmissionMapTransform;
	varying vec2 vTransmissionMapUv;
#endif
#ifdef USE_THICKNESSMAP
	uniform mat3 thicknessMapTransform;
	varying vec2 vThicknessMapUv;
#endif`,q_=`#if defined( USE_UV ) || defined( USE_ANISOTROPY )
	varying vec2 vUv;
#endif
#ifdef USE_MAP
	uniform mat3 mapTransform;
	varying vec2 vMapUv;
#endif
#ifdef USE_ALPHAMAP
	uniform mat3 alphaMapTransform;
	varying vec2 vAlphaMapUv;
#endif
#ifdef USE_LIGHTMAP
	uniform mat3 lightMapTransform;
	varying vec2 vLightMapUv;
#endif
#ifdef USE_AOMAP
	uniform mat3 aoMapTransform;
	varying vec2 vAoMapUv;
#endif
#ifdef USE_BUMPMAP
	uniform mat3 bumpMapTransform;
	varying vec2 vBumpMapUv;
#endif
#ifdef USE_NORMALMAP
	uniform mat3 normalMapTransform;
	varying vec2 vNormalMapUv;
#endif
#ifdef USE_DISPLACEMENTMAP
	uniform mat3 displacementMapTransform;
	varying vec2 vDisplacementMapUv;
#endif
#ifdef USE_EMISSIVEMAP
	uniform mat3 emissiveMapTransform;
	varying vec2 vEmissiveMapUv;
#endif
#ifdef USE_METALNESSMAP
	uniform mat3 metalnessMapTransform;
	varying vec2 vMetalnessMapUv;
#endif
#ifdef USE_ROUGHNESSMAP
	uniform mat3 roughnessMapTransform;
	varying vec2 vRoughnessMapUv;
#endif
#ifdef USE_ANISOTROPYMAP
	uniform mat3 anisotropyMapTransform;
	varying vec2 vAnisotropyMapUv;
#endif
#ifdef USE_CLEARCOATMAP
	uniform mat3 clearcoatMapTransform;
	varying vec2 vClearcoatMapUv;
#endif
#ifdef USE_CLEARCOAT_NORMALMAP
	uniform mat3 clearcoatNormalMapTransform;
	varying vec2 vClearcoatNormalMapUv;
#endif
#ifdef USE_CLEARCOAT_ROUGHNESSMAP
	uniform mat3 clearcoatRoughnessMapTransform;
	varying vec2 vClearcoatRoughnessMapUv;
#endif
#ifdef USE_SHEEN_COLORMAP
	uniform mat3 sheenColorMapTransform;
	varying vec2 vSheenColorMapUv;
#endif
#ifdef USE_SHEEN_ROUGHNESSMAP
	uniform mat3 sheenRoughnessMapTransform;
	varying vec2 vSheenRoughnessMapUv;
#endif
#ifdef USE_IRIDESCENCEMAP
	uniform mat3 iridescenceMapTransform;
	varying vec2 vIridescenceMapUv;
#endif
#ifdef USE_IRIDESCENCE_THICKNESSMAP
	uniform mat3 iridescenceThicknessMapTransform;
	varying vec2 vIridescenceThicknessMapUv;
#endif
#ifdef USE_SPECULARMAP
	uniform mat3 specularMapTransform;
	varying vec2 vSpecularMapUv;
#endif
#ifdef USE_SPECULAR_COLORMAP
	uniform mat3 specularColorMapTransform;
	varying vec2 vSpecularColorMapUv;
#endif
#ifdef USE_SPECULAR_INTENSITYMAP
	uniform mat3 specularIntensityMapTransform;
	varying vec2 vSpecularIntensityMapUv;
#endif
#ifdef USE_TRANSMISSIONMAP
	uniform mat3 transmissionMapTransform;
	varying vec2 vTransmissionMapUv;
#endif
#ifdef USE_THICKNESSMAP
	uniform mat3 thicknessMapTransform;
	varying vec2 vThicknessMapUv;
#endif`,Z_=`#if defined( USE_UV ) || defined( USE_ANISOTROPY )
	vUv = vec3( uv, 1 ).xy;
#endif
#ifdef USE_MAP
	vMapUv = ( mapTransform * vec3( MAP_UV, 1 ) ).xy;
#endif
#ifdef USE_ALPHAMAP
	vAlphaMapUv = ( alphaMapTransform * vec3( ALPHAMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_LIGHTMAP
	vLightMapUv = ( lightMapTransform * vec3( LIGHTMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_AOMAP
	vAoMapUv = ( aoMapTransform * vec3( AOMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_BUMPMAP
	vBumpMapUv = ( bumpMapTransform * vec3( BUMPMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_NORMALMAP
	vNormalMapUv = ( normalMapTransform * vec3( NORMALMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_DISPLACEMENTMAP
	vDisplacementMapUv = ( displacementMapTransform * vec3( DISPLACEMENTMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_EMISSIVEMAP
	vEmissiveMapUv = ( emissiveMapTransform * vec3( EMISSIVEMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_METALNESSMAP
	vMetalnessMapUv = ( metalnessMapTransform * vec3( METALNESSMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_ROUGHNESSMAP
	vRoughnessMapUv = ( roughnessMapTransform * vec3( ROUGHNESSMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_ANISOTROPYMAP
	vAnisotropyMapUv = ( anisotropyMapTransform * vec3( ANISOTROPYMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_CLEARCOATMAP
	vClearcoatMapUv = ( clearcoatMapTransform * vec3( CLEARCOATMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_CLEARCOAT_NORMALMAP
	vClearcoatNormalMapUv = ( clearcoatNormalMapTransform * vec3( CLEARCOAT_NORMALMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_CLEARCOAT_ROUGHNESSMAP
	vClearcoatRoughnessMapUv = ( clearcoatRoughnessMapTransform * vec3( CLEARCOAT_ROUGHNESSMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_IRIDESCENCEMAP
	vIridescenceMapUv = ( iridescenceMapTransform * vec3( IRIDESCENCEMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_IRIDESCENCE_THICKNESSMAP
	vIridescenceThicknessMapUv = ( iridescenceThicknessMapTransform * vec3( IRIDESCENCE_THICKNESSMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_SHEEN_COLORMAP
	vSheenColorMapUv = ( sheenColorMapTransform * vec3( SHEEN_COLORMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_SHEEN_ROUGHNESSMAP
	vSheenRoughnessMapUv = ( sheenRoughnessMapTransform * vec3( SHEEN_ROUGHNESSMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_SPECULARMAP
	vSpecularMapUv = ( specularMapTransform * vec3( SPECULARMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_SPECULAR_COLORMAP
	vSpecularColorMapUv = ( specularColorMapTransform * vec3( SPECULAR_COLORMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_SPECULAR_INTENSITYMAP
	vSpecularIntensityMapUv = ( specularIntensityMapTransform * vec3( SPECULAR_INTENSITYMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_TRANSMISSIONMAP
	vTransmissionMapUv = ( transmissionMapTransform * vec3( TRANSMISSIONMAP_UV, 1 ) ).xy;
#endif
#ifdef USE_THICKNESSMAP
	vThicknessMapUv = ( thicknessMapTransform * vec3( THICKNESSMAP_UV, 1 ) ).xy;
#endif`,j_=`#if defined( USE_ENVMAP ) || defined( DISTANCE ) || defined ( USE_SHADOWMAP ) || defined ( USE_TRANSMISSION ) || NUM_SPOT_LIGHT_COORDS > 0
	vec4 worldPosition = vec4( transformed, 1.0 );
	#ifdef USE_BATCHING
		worldPosition = batchingMatrix * worldPosition;
	#endif
	#ifdef USE_INSTANCING
		worldPosition = instanceMatrix * worldPosition;
	#endif
	worldPosition = modelMatrix * worldPosition;
#endif`;const K_=`varying vec2 vUv;
uniform mat3 uvTransform;
void main() {
	vUv = ( uvTransform * vec3( uv, 1 ) ).xy;
	gl_Position = vec4( position.xy, 1.0, 1.0 );
}`,J_=`uniform sampler2D t2D;
uniform float backgroundIntensity;
varying vec2 vUv;
void main() {
	vec4 texColor = texture2D( t2D, vUv );
	#ifdef DECODE_VIDEO_TEXTURE
		texColor = vec4( mix( pow( texColor.rgb * 0.9478672986 + vec3( 0.0521327014 ), vec3( 2.4 ) ), texColor.rgb * 0.0773993808, vec3( lessThanEqual( texColor.rgb, vec3( 0.04045 ) ) ) ), texColor.w );
	#endif
	texColor.rgb *= backgroundIntensity;
	gl_FragColor = texColor;
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
}`,$_=`varying vec3 vWorldDirection;
#include <common>
void main() {
	vWorldDirection = transformDirection( position, modelMatrix );
	#include <begin_vertex>
	#include <project_vertex>
	gl_Position.z = gl_Position.w;
}`,Q_=`#ifdef ENVMAP_TYPE_CUBE
	uniform samplerCube envMap;
#elif defined( ENVMAP_TYPE_CUBE_UV )
	uniform sampler2D envMap;
#endif
uniform float flipEnvMap;
uniform float backgroundBlurriness;
uniform float backgroundIntensity;
uniform mat3 backgroundRotation;
varying vec3 vWorldDirection;
#include <cube_uv_reflection_fragment>
void main() {
	#ifdef ENVMAP_TYPE_CUBE
		vec4 texColor = textureCube( envMap, backgroundRotation * vec3( flipEnvMap * vWorldDirection.x, vWorldDirection.yz ) );
	#elif defined( ENVMAP_TYPE_CUBE_UV )
		vec4 texColor = textureCubeUV( envMap, backgroundRotation * vWorldDirection, backgroundBlurriness );
	#else
		vec4 texColor = vec4( 0.0, 0.0, 0.0, 1.0 );
	#endif
	texColor.rgb *= backgroundIntensity;
	gl_FragColor = texColor;
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
}`,ey=`varying vec3 vWorldDirection;
#include <common>
void main() {
	vWorldDirection = transformDirection( position, modelMatrix );
	#include <begin_vertex>
	#include <project_vertex>
	gl_Position.z = gl_Position.w;
}`,ty=`uniform samplerCube tCube;
uniform float tFlip;
uniform float opacity;
varying vec3 vWorldDirection;
void main() {
	vec4 texColor = textureCube( tCube, vec3( tFlip * vWorldDirection.x, vWorldDirection.yz ) );
	gl_FragColor = texColor;
	gl_FragColor.a *= opacity;
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
}`,ny=`#include <common>
#include <batching_pars_vertex>
#include <uv_pars_vertex>
#include <displacementmap_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
varying vec2 vHighPrecisionZW;
void main() {
	#include <uv_vertex>
	#include <batching_vertex>
	#include <skinbase_vertex>
	#include <morphinstance_vertex>
	#ifdef USE_DISPLACEMENTMAP
		#include <beginnormal_vertex>
		#include <morphnormal_vertex>
		#include <skinnormal_vertex>
	#endif
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	vHighPrecisionZW = gl_Position.zw;
}`,iy=`#if DEPTH_PACKING == 3200
	uniform float opacity;
#endif
#include <common>
#include <packing>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <alphahash_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
varying vec2 vHighPrecisionZW;
void main() {
	vec4 diffuseColor = vec4( 1.0 );
	#include <clipping_planes_fragment>
	#if DEPTH_PACKING == 3200
		diffuseColor.a = opacity;
	#endif
	#include <map_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <alphahash_fragment>
	#include <logdepthbuf_fragment>
	float fragCoordZ = 0.5 * vHighPrecisionZW[0] / vHighPrecisionZW[1] + 0.5;
	#if DEPTH_PACKING == 3200
		gl_FragColor = vec4( vec3( 1.0 - fragCoordZ ), opacity );
	#elif DEPTH_PACKING == 3201
		gl_FragColor = packDepthToRGBA( fragCoordZ );
	#endif
}`,ry=`#define DISTANCE
varying vec3 vWorldPosition;
#include <common>
#include <batching_pars_vertex>
#include <uv_pars_vertex>
#include <displacementmap_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <batching_vertex>
	#include <skinbase_vertex>
	#include <morphinstance_vertex>
	#ifdef USE_DISPLACEMENTMAP
		#include <beginnormal_vertex>
		#include <morphnormal_vertex>
		#include <skinnormal_vertex>
	#endif
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <worldpos_vertex>
	#include <clipping_planes_vertex>
	vWorldPosition = worldPosition.xyz;
}`,sy=`#define DISTANCE
uniform vec3 referencePosition;
uniform float nearDistance;
uniform float farDistance;
varying vec3 vWorldPosition;
#include <common>
#include <packing>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <alphahash_pars_fragment>
#include <clipping_planes_pars_fragment>
void main () {
	vec4 diffuseColor = vec4( 1.0 );
	#include <clipping_planes_fragment>
	#include <map_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <alphahash_fragment>
	float dist = length( vWorldPosition - referencePosition );
	dist = ( dist - nearDistance ) / ( farDistance - nearDistance );
	dist = saturate( dist );
	gl_FragColor = packDepthToRGBA( dist );
}`,oy=`varying vec3 vWorldDirection;
#include <common>
void main() {
	vWorldDirection = transformDirection( position, modelMatrix );
	#include <begin_vertex>
	#include <project_vertex>
}`,ay=`uniform sampler2D tEquirect;
varying vec3 vWorldDirection;
#include <common>
void main() {
	vec3 direction = normalize( vWorldDirection );
	vec2 sampleUV = equirectUv( direction );
	gl_FragColor = texture2D( tEquirect, sampleUV );
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
}`,ly=`uniform float scale;
attribute float lineDistance;
varying float vLineDistance;
#include <common>
#include <uv_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	vLineDistance = scale * lineDistance;
	#include <uv_vertex>
	#include <color_vertex>
	#include <morphinstance_vertex>
	#include <morphcolor_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	#include <fog_vertex>
}`,cy=`uniform vec3 diffuse;
uniform float opacity;
uniform float dashSize;
uniform float totalSize;
varying float vLineDistance;
#include <common>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <fog_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <clipping_planes_fragment>
	if ( mod( vLineDistance, totalSize ) > dashSize ) {
		discard;
	}
	vec3 outgoingLight = vec3( 0.0 );
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	outgoingLight = diffuseColor.rgb;
	#include <opaque_fragment>
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
}`,hy=`#include <common>
#include <batching_pars_vertex>
#include <uv_pars_vertex>
#include <envmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <color_vertex>
	#include <morphinstance_vertex>
	#include <morphcolor_vertex>
	#include <batching_vertex>
	#if defined ( USE_ENVMAP ) || defined ( USE_SKINNING )
		#include <beginnormal_vertex>
		#include <morphnormal_vertex>
		#include <skinbase_vertex>
		#include <skinnormal_vertex>
		#include <defaultnormal_vertex>
	#endif
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	#include <worldpos_vertex>
	#include <envmap_vertex>
	#include <fog_vertex>
}`,uy=`uniform vec3 diffuse;
uniform float opacity;
#ifndef FLAT_SHADED
	varying vec3 vNormal;
#endif
#include <common>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <alphahash_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <envmap_common_pars_fragment>
#include <envmap_pars_fragment>
#include <fog_pars_fragment>
#include <specularmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <clipping_planes_fragment>
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <alphahash_fragment>
	#include <specularmap_fragment>
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	#ifdef USE_LIGHTMAP
		vec4 lightMapTexel = texture2D( lightMap, vLightMapUv );
		reflectedLight.indirectDiffuse += lightMapTexel.rgb * lightMapIntensity * RECIPROCAL_PI;
	#else
		reflectedLight.indirectDiffuse += vec3( 1.0 );
	#endif
	#include <aomap_fragment>
	reflectedLight.indirectDiffuse *= diffuseColor.rgb;
	vec3 outgoingLight = reflectedLight.indirectDiffuse;
	#include <envmap_fragment>
	#include <opaque_fragment>
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,dy=`#define LAMBERT
varying vec3 vViewPosition;
#include <common>
#include <batching_pars_vertex>
#include <uv_pars_vertex>
#include <displacementmap_pars_vertex>
#include <envmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <color_vertex>
	#include <morphinstance_vertex>
	#include <morphcolor_vertex>
	#include <batching_vertex>
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	vViewPosition = - mvPosition.xyz;
	#include <worldpos_vertex>
	#include <envmap_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>
}`,fy=`#define LAMBERT
uniform vec3 diffuse;
uniform vec3 emissive;
uniform float opacity;
#include <common>
#include <packing>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <alphahash_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <emissivemap_pars_fragment>
#include <envmap_common_pars_fragment>
#include <envmap_pars_fragment>
#include <fog_pars_fragment>
#include <bsdfs>
#include <lights_pars_begin>
#include <normal_pars_fragment>
#include <lights_lambert_pars_fragment>
#include <shadowmap_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <specularmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <clipping_planes_fragment>
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	vec3 totalEmissiveRadiance = emissive;
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <alphahash_fragment>
	#include <specularmap_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	#include <emissivemap_fragment>
	#include <lights_lambert_fragment>
	#include <lights_fragment_begin>
	#include <lights_fragment_maps>
	#include <lights_fragment_end>
	#include <aomap_fragment>
	vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + totalEmissiveRadiance;
	#include <envmap_fragment>
	#include <opaque_fragment>
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,py=`#define MATCAP
varying vec3 vViewPosition;
#include <common>
#include <batching_pars_vertex>
#include <uv_pars_vertex>
#include <color_pars_vertex>
#include <displacementmap_pars_vertex>
#include <fog_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <color_vertex>
	#include <morphinstance_vertex>
	#include <morphcolor_vertex>
	#include <batching_vertex>
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	#include <fog_vertex>
	vViewPosition = - mvPosition.xyz;
}`,my=`#define MATCAP
uniform vec3 diffuse;
uniform float opacity;
uniform sampler2D matcap;
varying vec3 vViewPosition;
#include <common>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <alphahash_pars_fragment>
#include <fog_pars_fragment>
#include <normal_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <clipping_planes_fragment>
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <alphahash_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	vec3 viewDir = normalize( vViewPosition );
	vec3 x = normalize( vec3( viewDir.z, 0.0, - viewDir.x ) );
	vec3 y = cross( viewDir, x );
	vec2 uv = vec2( dot( x, normal ), dot( y, normal ) ) * 0.495 + 0.5;
	#ifdef USE_MATCAP
		vec4 matcapColor = texture2D( matcap, uv );
	#else
		vec4 matcapColor = vec4( vec3( mix( 0.2, 0.8, uv.y ) ), 1.0 );
	#endif
	vec3 outgoingLight = diffuseColor.rgb * matcapColor.rgb;
	#include <opaque_fragment>
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,gy=`#define NORMAL
#if defined( FLAT_SHADED ) || defined( USE_BUMPMAP ) || defined( USE_NORMALMAP_TANGENTSPACE )
	varying vec3 vViewPosition;
#endif
#include <common>
#include <batching_pars_vertex>
#include <uv_pars_vertex>
#include <displacementmap_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <batching_vertex>
	#include <beginnormal_vertex>
	#include <morphinstance_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
#if defined( FLAT_SHADED ) || defined( USE_BUMPMAP ) || defined( USE_NORMALMAP_TANGENTSPACE )
	vViewPosition = - mvPosition.xyz;
#endif
}`,vy=`#define NORMAL
uniform float opacity;
#if defined( FLAT_SHADED ) || defined( USE_BUMPMAP ) || defined( USE_NORMALMAP_TANGENTSPACE )
	varying vec3 vViewPosition;
#endif
#include <packing>
#include <uv_pars_fragment>
#include <normal_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	vec4 diffuseColor = vec4( 0.0, 0.0, 0.0, opacity );
	#include <clipping_planes_fragment>
	#include <logdepthbuf_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	gl_FragColor = vec4( packNormalToRGB( normal ), diffuseColor.a );
	#ifdef OPAQUE
		gl_FragColor.a = 1.0;
	#endif
}`,_y=`#define PHONG
varying vec3 vViewPosition;
#include <common>
#include <batching_pars_vertex>
#include <uv_pars_vertex>
#include <displacementmap_pars_vertex>
#include <envmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <color_vertex>
	#include <morphcolor_vertex>
	#include <batching_vertex>
	#include <beginnormal_vertex>
	#include <morphinstance_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	vViewPosition = - mvPosition.xyz;
	#include <worldpos_vertex>
	#include <envmap_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>
}`,yy=`#define PHONG
uniform vec3 diffuse;
uniform vec3 emissive;
uniform vec3 specular;
uniform float shininess;
uniform float opacity;
#include <common>
#include <packing>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <alphahash_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <emissivemap_pars_fragment>
#include <envmap_common_pars_fragment>
#include <envmap_pars_fragment>
#include <fog_pars_fragment>
#include <bsdfs>
#include <lights_pars_begin>
#include <normal_pars_fragment>
#include <lights_phong_pars_fragment>
#include <shadowmap_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <specularmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <clipping_planes_fragment>
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	vec3 totalEmissiveRadiance = emissive;
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <alphahash_fragment>
	#include <specularmap_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	#include <emissivemap_fragment>
	#include <lights_phong_fragment>
	#include <lights_fragment_begin>
	#include <lights_fragment_maps>
	#include <lights_fragment_end>
	#include <aomap_fragment>
	vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + reflectedLight.directSpecular + reflectedLight.indirectSpecular + totalEmissiveRadiance;
	#include <envmap_fragment>
	#include <opaque_fragment>
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,xy=`#define STANDARD
varying vec3 vViewPosition;
#ifdef USE_TRANSMISSION
	varying vec3 vWorldPosition;
#endif
#include <common>
#include <batching_pars_vertex>
#include <uv_pars_vertex>
#include <displacementmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <color_vertex>
	#include <morphinstance_vertex>
	#include <morphcolor_vertex>
	#include <batching_vertex>
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	vViewPosition = - mvPosition.xyz;
	#include <worldpos_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>
#ifdef USE_TRANSMISSION
	vWorldPosition = worldPosition.xyz;
#endif
}`,My=`#define STANDARD
#ifdef PHYSICAL
	#define IOR
	#define USE_SPECULAR
#endif
uniform vec3 diffuse;
uniform vec3 emissive;
uniform float roughness;
uniform float metalness;
uniform float opacity;
#ifdef IOR
	uniform float ior;
#endif
#ifdef USE_SPECULAR
	uniform float specularIntensity;
	uniform vec3 specularColor;
	#ifdef USE_SPECULAR_COLORMAP
		uniform sampler2D specularColorMap;
	#endif
	#ifdef USE_SPECULAR_INTENSITYMAP
		uniform sampler2D specularIntensityMap;
	#endif
#endif
#ifdef USE_CLEARCOAT
	uniform float clearcoat;
	uniform float clearcoatRoughness;
#endif
#ifdef USE_DISPERSION
	uniform float dispersion;
#endif
#ifdef USE_IRIDESCENCE
	uniform float iridescence;
	uniform float iridescenceIOR;
	uniform float iridescenceThicknessMinimum;
	uniform float iridescenceThicknessMaximum;
#endif
#ifdef USE_SHEEN
	uniform vec3 sheenColor;
	uniform float sheenRoughness;
	#ifdef USE_SHEEN_COLORMAP
		uniform sampler2D sheenColorMap;
	#endif
	#ifdef USE_SHEEN_ROUGHNESSMAP
		uniform sampler2D sheenRoughnessMap;
	#endif
#endif
#ifdef USE_ANISOTROPY
	uniform vec2 anisotropyVector;
	#ifdef USE_ANISOTROPYMAP
		uniform sampler2D anisotropyMap;
	#endif
#endif
varying vec3 vViewPosition;
#include <common>
#include <packing>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <alphahash_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <emissivemap_pars_fragment>
#include <iridescence_fragment>
#include <cube_uv_reflection_fragment>
#include <envmap_common_pars_fragment>
#include <envmap_physical_pars_fragment>
#include <fog_pars_fragment>
#include <lights_pars_begin>
#include <normal_pars_fragment>
#include <lights_physical_pars_fragment>
#include <transmission_pars_fragment>
#include <shadowmap_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <clearcoat_pars_fragment>
#include <iridescence_pars_fragment>
#include <roughnessmap_pars_fragment>
#include <metalnessmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <clipping_planes_fragment>
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	vec3 totalEmissiveRadiance = emissive;
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <alphahash_fragment>
	#include <roughnessmap_fragment>
	#include <metalnessmap_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	#include <clearcoat_normal_fragment_begin>
	#include <clearcoat_normal_fragment_maps>
	#include <emissivemap_fragment>
	#include <lights_physical_fragment>
	#include <lights_fragment_begin>
	#include <lights_fragment_maps>
	#include <lights_fragment_end>
	#include <aomap_fragment>
	vec3 totalDiffuse = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse;
	vec3 totalSpecular = reflectedLight.directSpecular + reflectedLight.indirectSpecular;
	#include <transmission_fragment>
	vec3 outgoingLight = totalDiffuse + totalSpecular + totalEmissiveRadiance;
	#ifdef USE_SHEEN
		float sheenEnergyComp = 1.0 - 0.157 * max3( material.sheenColor );
		outgoingLight = outgoingLight * sheenEnergyComp + sheenSpecularDirect + sheenSpecularIndirect;
	#endif
	#ifdef USE_CLEARCOAT
		float dotNVcc = saturate( dot( geometryClearcoatNormal, geometryViewDir ) );
		vec3 Fcc = F_Schlick( material.clearcoatF0, material.clearcoatF90, dotNVcc );
		outgoingLight = outgoingLight * ( 1.0 - material.clearcoat * Fcc ) + ( clearcoatSpecularDirect + clearcoatSpecularIndirect ) * material.clearcoat;
	#endif
	#include <opaque_fragment>
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,by=`#define TOON
varying vec3 vViewPosition;
#include <common>
#include <batching_pars_vertex>
#include <uv_pars_vertex>
#include <displacementmap_pars_vertex>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <normal_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <shadowmap_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	#include <color_vertex>
	#include <morphinstance_vertex>
	#include <morphcolor_vertex>
	#include <batching_vertex>
	#include <beginnormal_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <normal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <displacementmap_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	vViewPosition = - mvPosition.xyz;
	#include <worldpos_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>
}`,Sy=`#define TOON
uniform vec3 diffuse;
uniform vec3 emissive;
uniform float opacity;
#include <common>
#include <packing>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <alphahash_pars_fragment>
#include <aomap_pars_fragment>
#include <lightmap_pars_fragment>
#include <emissivemap_pars_fragment>
#include <gradientmap_pars_fragment>
#include <fog_pars_fragment>
#include <bsdfs>
#include <lights_pars_begin>
#include <normal_pars_fragment>
#include <lights_toon_pars_fragment>
#include <shadowmap_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <clipping_planes_fragment>
	ReflectedLight reflectedLight = ReflectedLight( vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ), vec3( 0.0 ) );
	vec3 totalEmissiveRadiance = emissive;
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <alphahash_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
	#include <emissivemap_fragment>
	#include <lights_toon_fragment>
	#include <lights_fragment_begin>
	#include <lights_fragment_maps>
	#include <lights_fragment_end>
	#include <aomap_fragment>
	vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + totalEmissiveRadiance;
	#include <opaque_fragment>
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
	#include <dithering_fragment>
}`,wy=`uniform float size;
uniform float scale;
#include <common>
#include <color_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
#ifdef USE_POINTS_UV
	varying vec2 vUv;
	uniform mat3 uvTransform;
#endif
void main() {
	#ifdef USE_POINTS_UV
		vUv = ( uvTransform * vec3( uv, 1 ) ).xy;
	#endif
	#include <color_vertex>
	#include <morphinstance_vertex>
	#include <morphcolor_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <project_vertex>
	gl_PointSize = size;
	#ifdef USE_SIZEATTENUATION
		bool isPerspective = isPerspectiveMatrix( projectionMatrix );
		if ( isPerspective ) gl_PointSize *= ( scale / - mvPosition.z );
	#endif
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	#include <worldpos_vertex>
	#include <fog_vertex>
}`,Ey=`uniform vec3 diffuse;
uniform float opacity;
#include <common>
#include <color_pars_fragment>
#include <map_particle_pars_fragment>
#include <alphatest_pars_fragment>
#include <alphahash_pars_fragment>
#include <fog_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <clipping_planes_fragment>
	vec3 outgoingLight = vec3( 0.0 );
	#include <logdepthbuf_fragment>
	#include <map_particle_fragment>
	#include <color_fragment>
	#include <alphatest_fragment>
	#include <alphahash_fragment>
	outgoingLight = diffuseColor.rgb;
	#include <opaque_fragment>
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
	#include <fog_fragment>
	#include <premultiplied_alpha_fragment>
}`,Ty=`#include <common>
#include <batching_pars_vertex>
#include <fog_pars_vertex>
#include <morphtarget_pars_vertex>
#include <skinning_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <shadowmap_pars_vertex>
void main() {
	#include <batching_vertex>
	#include <beginnormal_vertex>
	#include <morphinstance_vertex>
	#include <morphnormal_vertex>
	#include <skinbase_vertex>
	#include <skinnormal_vertex>
	#include <defaultnormal_vertex>
	#include <begin_vertex>
	#include <morphtarget_vertex>
	#include <skinning_vertex>
	#include <project_vertex>
	#include <logdepthbuf_vertex>
	#include <worldpos_vertex>
	#include <shadowmap_vertex>
	#include <fog_vertex>
}`,Ay=`uniform vec3 color;
uniform float opacity;
#include <common>
#include <packing>
#include <fog_pars_fragment>
#include <bsdfs>
#include <lights_pars_begin>
#include <logdepthbuf_pars_fragment>
#include <shadowmap_pars_fragment>
#include <shadowmask_pars_fragment>
void main() {
	#include <logdepthbuf_fragment>
	gl_FragColor = vec4( color, opacity * ( 1.0 - getShadowMask() ) );
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
	#include <fog_fragment>
}`,Cy=`uniform float rotation;
uniform vec2 center;
#include <common>
#include <uv_pars_vertex>
#include <fog_pars_vertex>
#include <logdepthbuf_pars_vertex>
#include <clipping_planes_pars_vertex>
void main() {
	#include <uv_vertex>
	vec4 mvPosition = modelViewMatrix * vec4( 0.0, 0.0, 0.0, 1.0 );
	vec2 scale;
	scale.x = length( vec3( modelMatrix[ 0 ].x, modelMatrix[ 0 ].y, modelMatrix[ 0 ].z ) );
	scale.y = length( vec3( modelMatrix[ 1 ].x, modelMatrix[ 1 ].y, modelMatrix[ 1 ].z ) );
	#ifndef USE_SIZEATTENUATION
		bool isPerspective = isPerspectiveMatrix( projectionMatrix );
		if ( isPerspective ) scale *= - mvPosition.z;
	#endif
	vec2 alignedPosition = ( position.xy - ( center - vec2( 0.5 ) ) ) * scale;
	vec2 rotatedPosition;
	rotatedPosition.x = cos( rotation ) * alignedPosition.x - sin( rotation ) * alignedPosition.y;
	rotatedPosition.y = sin( rotation ) * alignedPosition.x + cos( rotation ) * alignedPosition.y;
	mvPosition.xy += rotatedPosition;
	gl_Position = projectionMatrix * mvPosition;
	#include <logdepthbuf_vertex>
	#include <clipping_planes_vertex>
	#include <fog_vertex>
}`,Ry=`uniform vec3 diffuse;
uniform float opacity;
#include <common>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <alphatest_pars_fragment>
#include <alphahash_pars_fragment>
#include <fog_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
void main() {
	vec4 diffuseColor = vec4( diffuse, opacity );
	#include <clipping_planes_fragment>
	vec3 outgoingLight = vec3( 0.0 );
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <alphahash_fragment>
	outgoingLight = diffuseColor.rgb;
	#include <opaque_fragment>
	#include <tonemapping_fragment>
	#include <colorspace_fragment>
	#include <fog_fragment>
}`,mt={alphahash_fragment:Jv,alphahash_pars_fragment:$v,alphamap_fragment:Qv,alphamap_pars_fragment:e0,alphatest_fragment:t0,alphatest_pars_fragment:n0,aomap_fragment:i0,aomap_pars_fragment:r0,batching_pars_vertex:s0,batching_vertex:o0,begin_vertex:a0,beginnormal_vertex:l0,bsdfs:c0,iridescence_fragment:h0,bumpmap_pars_fragment:u0,clipping_planes_fragment:d0,clipping_planes_pars_fragment:f0,clipping_planes_pars_vertex:p0,clipping_planes_vertex:m0,color_fragment:g0,color_pars_fragment:v0,color_pars_vertex:_0,color_vertex:y0,common:x0,cube_uv_reflection_fragment:M0,defaultnormal_vertex:b0,displacementmap_pars_vertex:S0,displacementmap_vertex:w0,emissivemap_fragment:E0,emissivemap_pars_fragment:T0,colorspace_fragment:A0,colorspace_pars_fragment:C0,envmap_fragment:R0,envmap_common_pars_fragment:P0,envmap_pars_fragment:I0,envmap_pars_vertex:L0,envmap_physical_pars_fragment:G0,envmap_vertex:D0,fog_vertex:N0,fog_pars_vertex:O0,fog_fragment:U0,fog_pars_fragment:F0,gradientmap_pars_fragment:B0,lightmap_pars_fragment:z0,lights_lambert_fragment:k0,lights_lambert_pars_fragment:V0,lights_pars_begin:H0,lights_toon_fragment:W0,lights_toon_pars_fragment:X0,lights_phong_fragment:Y0,lights_phong_pars_fragment:q0,lights_physical_fragment:Z0,lights_physical_pars_fragment:j0,lights_fragment_begin:K0,lights_fragment_maps:J0,lights_fragment_end:$0,logdepthbuf_fragment:Q0,logdepthbuf_pars_fragment:e_,logdepthbuf_pars_vertex:t_,logdepthbuf_vertex:n_,map_fragment:i_,map_pars_fragment:r_,map_particle_fragment:s_,map_particle_pars_fragment:o_,metalnessmap_fragment:a_,metalnessmap_pars_fragment:l_,morphinstance_vertex:c_,morphcolor_vertex:h_,morphnormal_vertex:u_,morphtarget_pars_vertex:d_,morphtarget_vertex:f_,normal_fragment_begin:p_,normal_fragment_maps:m_,normal_pars_fragment:g_,normal_pars_vertex:v_,normal_vertex:__,normalmap_pars_fragment:y_,clearcoat_normal_fragment_begin:x_,clearcoat_normal_fragment_maps:M_,clearcoat_pars_fragment:b_,iridescence_pars_fragment:S_,opaque_fragment:w_,packing:E_,premultiplied_alpha_fragment:T_,project_vertex:A_,dithering_fragment:C_,dithering_pars_fragment:R_,roughnessmap_fragment:P_,roughnessmap_pars_fragment:I_,shadowmap_pars_fragment:L_,shadowmap_pars_vertex:D_,shadowmap_vertex:N_,shadowmask_pars_fragment:O_,skinbase_vertex:U_,skinning_pars_vertex:F_,skinning_vertex:B_,skinnormal_vertex:z_,specularmap_fragment:k_,specularmap_pars_fragment:V_,tonemapping_fragment:H_,tonemapping_pars_fragment:G_,transmission_fragment:W_,transmission_pars_fragment:X_,uv_pars_fragment:Y_,uv_pars_vertex:q_,uv_vertex:Z_,worldpos_vertex:j_,background_vert:K_,background_frag:J_,backgroundCube_vert:$_,backgroundCube_frag:Q_,cube_vert:ey,cube_frag:ty,depth_vert:ny,depth_frag:iy,distanceRGBA_vert:ry,distanceRGBA_frag:sy,equirect_vert:oy,equirect_frag:ay,linedashed_vert:ly,linedashed_frag:cy,meshbasic_vert:hy,meshbasic_frag:uy,meshlambert_vert:dy,meshlambert_frag:fy,meshmatcap_vert:py,meshmatcap_frag:my,meshnormal_vert:gy,meshnormal_frag:vy,meshphong_vert:_y,meshphong_frag:yy,meshphysical_vert:xy,meshphysical_frag:My,meshtoon_vert:by,meshtoon_frag:Sy,points_vert:wy,points_frag:Ey,shadow_vert:Ty,shadow_frag:Ay,sprite_vert:Cy,sprite_frag:Ry},Ne={common:{diffuse:{value:new Oe(16777215)},opacity:{value:1},map:{value:null},mapTransform:{value:new ut},alphaMap:{value:null},alphaMapTransform:{value:new ut},alphaTest:{value:0}},specularmap:{specularMap:{value:null},specularMapTransform:{value:new ut}},envmap:{envMap:{value:null},envMapRotation:{value:new ut},flipEnvMap:{value:-1},reflectivity:{value:1},ior:{value:1.5},refractionRatio:{value:.98}},aomap:{aoMap:{value:null},aoMapIntensity:{value:1},aoMapTransform:{value:new ut}},lightmap:{lightMap:{value:null},lightMapIntensity:{value:1},lightMapTransform:{value:new ut}},bumpmap:{bumpMap:{value:null},bumpMapTransform:{value:new ut},bumpScale:{value:1}},normalmap:{normalMap:{value:null},normalMapTransform:{value:new ut},normalScale:{value:new se(1,1)}},displacementmap:{displacementMap:{value:null},displacementMapTransform:{value:new ut},displacementScale:{value:1},displacementBias:{value:0}},emissivemap:{emissiveMap:{value:null},emissiveMapTransform:{value:new ut}},metalnessmap:{metalnessMap:{value:null},metalnessMapTransform:{value:new ut}},roughnessmap:{roughnessMap:{value:null},roughnessMapTransform:{value:new ut}},gradientmap:{gradientMap:{value:null}},fog:{fogDensity:{value:25e-5},fogNear:{value:1},fogFar:{value:2e3},fogColor:{value:new Oe(16777215)}},lights:{ambientLightColor:{value:[]},lightProbe:{value:[]},directionalLights:{value:[],properties:{direction:{},color:{}}},directionalLightShadows:{value:[],properties:{shadowBias:{},shadowNormalBias:{},shadowRadius:{},shadowMapSize:{}}},directionalShadowMap:{value:[]},directionalShadowMatrix:{value:[]},spotLights:{value:[],properties:{color:{},position:{},direction:{},distance:{},coneCos:{},penumbraCos:{},decay:{}}},spotLightShadows:{value:[],properties:{shadowBias:{},shadowNormalBias:{},shadowRadius:{},shadowMapSize:{}}},spotLightMap:{value:[]},spotShadowMap:{value:[]},spotLightMatrix:{value:[]},pointLights:{value:[],properties:{color:{},position:{},decay:{},distance:{}}},pointLightShadows:{value:[],properties:{shadowBias:{},shadowNormalBias:{},shadowRadius:{},shadowMapSize:{},shadowCameraNear:{},shadowCameraFar:{}}},pointShadowMap:{value:[]},pointShadowMatrix:{value:[]},hemisphereLights:{value:[],properties:{direction:{},skyColor:{},groundColor:{}}},rectAreaLights:{value:[],properties:{color:{},position:{},width:{},height:{}}},ltc_1:{value:null},ltc_2:{value:null}},points:{diffuse:{value:new Oe(16777215)},opacity:{value:1},size:{value:1},scale:{value:1},map:{value:null},alphaMap:{value:null},alphaMapTransform:{value:new ut},alphaTest:{value:0},uvTransform:{value:new ut}},sprite:{diffuse:{value:new Oe(16777215)},opacity:{value:1},center:{value:new se(.5,.5)},rotation:{value:0},map:{value:null},mapTransform:{value:new ut},alphaMap:{value:null},alphaMapTransform:{value:new ut},alphaTest:{value:0}}},Xn={basic:{uniforms:gn([Ne.common,Ne.specularmap,Ne.envmap,Ne.aomap,Ne.lightmap,Ne.fog]),vertexShader:mt.meshbasic_vert,fragmentShader:mt.meshbasic_frag},lambert:{uniforms:gn([Ne.common,Ne.specularmap,Ne.envmap,Ne.aomap,Ne.lightmap,Ne.emissivemap,Ne.bumpmap,Ne.normalmap,Ne.displacementmap,Ne.fog,Ne.lights,{emissive:{value:new Oe(0)}}]),vertexShader:mt.meshlambert_vert,fragmentShader:mt.meshlambert_frag},phong:{uniforms:gn([Ne.common,Ne.specularmap,Ne.envmap,Ne.aomap,Ne.lightmap,Ne.emissivemap,Ne.bumpmap,Ne.normalmap,Ne.displacementmap,Ne.fog,Ne.lights,{emissive:{value:new Oe(0)},specular:{value:new Oe(1118481)},shininess:{value:30}}]),vertexShader:mt.meshphong_vert,fragmentShader:mt.meshphong_frag},standard:{uniforms:gn([Ne.common,Ne.envmap,Ne.aomap,Ne.lightmap,Ne.emissivemap,Ne.bumpmap,Ne.normalmap,Ne.displacementmap,Ne.roughnessmap,Ne.metalnessmap,Ne.fog,Ne.lights,{emissive:{value:new Oe(0)},roughness:{value:1},metalness:{value:0},envMapIntensity:{value:1}}]),vertexShader:mt.meshphysical_vert,fragmentShader:mt.meshphysical_frag},toon:{uniforms:gn([Ne.common,Ne.aomap,Ne.lightmap,Ne.emissivemap,Ne.bumpmap,Ne.normalmap,Ne.displacementmap,Ne.gradientmap,Ne.fog,Ne.lights,{emissive:{value:new Oe(0)}}]),vertexShader:mt.meshtoon_vert,fragmentShader:mt.meshtoon_frag},matcap:{uniforms:gn([Ne.common,Ne.bumpmap,Ne.normalmap,Ne.displacementmap,Ne.fog,{matcap:{value:null}}]),vertexShader:mt.meshmatcap_vert,fragmentShader:mt.meshmatcap_frag},points:{uniforms:gn([Ne.points,Ne.fog]),vertexShader:mt.points_vert,fragmentShader:mt.points_frag},dashed:{uniforms:gn([Ne.common,Ne.fog,{scale:{value:1},dashSize:{value:1},totalSize:{value:2}}]),vertexShader:mt.linedashed_vert,fragmentShader:mt.linedashed_frag},depth:{uniforms:gn([Ne.common,Ne.displacementmap]),vertexShader:mt.depth_vert,fragmentShader:mt.depth_frag},normal:{uniforms:gn([Ne.common,Ne.bumpmap,Ne.normalmap,Ne.displacementmap,{opacity:{value:1}}]),vertexShader:mt.meshnormal_vert,fragmentShader:mt.meshnormal_frag},sprite:{uniforms:gn([Ne.sprite,Ne.fog]),vertexShader:mt.sprite_vert,fragmentShader:mt.sprite_frag},background:{uniforms:{uvTransform:{value:new ut},t2D:{value:null},backgroundIntensity:{value:1}},vertexShader:mt.background_vert,fragmentShader:mt.background_frag},backgroundCube:{uniforms:{envMap:{value:null},flipEnvMap:{value:-1},backgroundBlurriness:{value:0},backgroundIntensity:{value:1},backgroundRotation:{value:new ut}},vertexShader:mt.backgroundCube_vert,fragmentShader:mt.backgroundCube_frag},cube:{uniforms:{tCube:{value:null},tFlip:{value:-1},opacity:{value:1}},vertexShader:mt.cube_vert,fragmentShader:mt.cube_frag},equirect:{uniforms:{tEquirect:{value:null}},vertexShader:mt.equirect_vert,fragmentShader:mt.equirect_frag},distanceRGBA:{uniforms:gn([Ne.common,Ne.displacementmap,{referencePosition:{value:new O},nearDistance:{value:1},farDistance:{value:1e3}}]),vertexShader:mt.distanceRGBA_vert,fragmentShader:mt.distanceRGBA_frag},shadow:{uniforms:gn([Ne.lights,Ne.fog,{color:{value:new Oe(0)},opacity:{value:1}}]),vertexShader:mt.shadow_vert,fragmentShader:mt.shadow_frag}};Xn.physical={uniforms:gn([Xn.standard.uniforms,{clearcoat:{value:0},clearcoatMap:{value:null},clearcoatMapTransform:{value:new ut},clearcoatNormalMap:{value:null},clearcoatNormalMapTransform:{value:new ut},clearcoatNormalScale:{value:new se(1,1)},clearcoatRoughness:{value:0},clearcoatRoughnessMap:{value:null},clearcoatRoughnessMapTransform:{value:new ut},dispersion:{value:0},iridescence:{value:0},iridescenceMap:{value:null},iridescenceMapTransform:{value:new ut},iridescenceIOR:{value:1.3},iridescenceThicknessMinimum:{value:100},iridescenceThicknessMaximum:{value:400},iridescenceThicknessMap:{value:null},iridescenceThicknessMapTransform:{value:new ut},sheen:{value:0},sheenColor:{value:new Oe(0)},sheenColorMap:{value:null},sheenColorMapTransform:{value:new ut},sheenRoughness:{value:1},sheenRoughnessMap:{value:null},sheenRoughnessMapTransform:{value:new ut},transmission:{value:0},transmissionMap:{value:null},transmissionMapTransform:{value:new ut},transmissionSamplerSize:{value:new se},transmissionSamplerMap:{value:null},thickness:{value:0},thicknessMap:{value:null},thicknessMapTransform:{value:new ut},attenuationDistance:{value:0},attenuationColor:{value:new Oe(0)},specularColor:{value:new Oe(1,1,1)},specularColorMap:{value:null},specularColorMapTransform:{value:new ut},specularIntensity:{value:1},specularIntensityMap:{value:null},specularIntensityMapTransform:{value:new ut},anisotropyVector:{value:new se},anisotropyMap:{value:null},anisotropyMapTransform:{value:new ut}}]),vertexShader:mt.meshphysical_vert,fragmentShader:mt.meshphysical_frag};const Qo={r:0,b:0,g:0},tr=new xn,Py=new Ke;function Iy(s,e,t,n,i,r,o){const a=new Oe(0);let l=r===!0?0:1,u,d,m=null,p=0,g=null;function _(w){let S=w.isScene===!0?w.background:null;return S&&S.isTexture&&(S=(w.backgroundBlurriness>0?t:e).get(S)),S}function M(w){let S=!1;const T=_(w);T===null?v(a,l):T&&T.isColor&&(v(T,1),S=!0);const z=s.xr.getEnvironmentBlendMode();z==="additive"?n.buffers.color.setClear(0,0,0,1,o):z==="alpha-blend"&&n.buffers.color.setClear(0,0,0,0,o),(s.autoClear||S)&&s.clear(s.autoClearColor,s.autoClearDepth,s.autoClearStencil)}function y(w,S){const T=_(S);T&&(T.isCubeTexture||T.mapping===us)?(d===void 0&&(d=new Ie(new zt(1,1,1),new tn({name:"BackgroundCubeMaterial",uniforms:cs(Xn.backgroundCube.uniforms),vertexShader:Xn.backgroundCube.vertexShader,fragmentShader:Xn.backgroundCube.fragmentShader,side:_n,depthTest:!1,depthWrite:!1,fog:!1})),d.geometry.deleteAttribute("normal"),d.geometry.deleteAttribute("uv"),d.onBeforeRender=function(z,N,A){this.matrixWorld.copyPosition(A.matrixWorld)},Object.defineProperty(d.material,"envMap",{get:function(){return this.uniforms.envMap.value}}),i.update(d)),tr.copy(S.backgroundRotation),tr.x*=-1,tr.y*=-1,tr.z*=-1,T.isCubeTexture&&T.isRenderTargetTexture===!1&&(tr.y*=-1,tr.z*=-1),d.material.uniforms.envMap.value=T,d.material.uniforms.flipEnvMap.value=T.isCubeTexture&&T.isRenderTargetTexture===!1?-1:1,d.material.uniforms.backgroundBlurriness.value=S.backgroundBlurriness,d.material.uniforms.backgroundIntensity.value=S.backgroundIntensity,d.material.uniforms.backgroundRotation.value.setFromMatrix4(Py.makeRotationFromEuler(tr)),d.material.toneMapped=wt.getTransfer(T.colorSpace)!==It,(m!==T||p!==T.version||g!==s.toneMapping)&&(d.material.needsUpdate=!0,m=T,p=T.version,g=s.toneMapping),d.layers.enableAll(),w.unshift(d,d.geometry,d.material,0,0,null)):T&&T.isTexture&&(u===void 0&&(u=new Ie(new Cr(2,2),new tn({name:"BackgroundMaterial",uniforms:cs(Xn.background.uniforms),vertexShader:Xn.background.vertexShader,fragmentShader:Xn.background.fragmentShader,side:xi,depthTest:!1,depthWrite:!1,fog:!1})),u.geometry.deleteAttribute("normal"),Object.defineProperty(u.material,"map",{get:function(){return this.uniforms.t2D.value}}),i.update(u)),u.material.uniforms.t2D.value=T,u.material.uniforms.backgroundIntensity.value=S.backgroundIntensity,u.material.toneMapped=wt.getTransfer(T.colorSpace)!==It,T.matrixAutoUpdate===!0&&T.updateMatrix(),u.material.uniforms.uvTransform.value.copy(T.matrix),(m!==T||p!==T.version||g!==s.toneMapping)&&(u.material.needsUpdate=!0,m=T,p=T.version,g=s.toneMapping),u.layers.enableAll(),w.unshift(u,u.geometry,u.material,0,0,null))}function v(w,S){w.getRGB(Qo,Mp(s)),n.buffers.color.setClear(Qo.r,Qo.g,Qo.b,S,o)}return{getClearColor:function(){return a},setClearColor:function(w,S=1){a.set(w),l=S,v(a,l)},getClearAlpha:function(){return l},setClearAlpha:function(w){l=w,v(a,l)},render:M,addToRenderList:y}}function Ly(s,e){const t=s.getParameter(s.MAX_VERTEX_ATTRIBS),n={},i=p(null);let r=i,o=!1;function a(C,X,Y,H,K){let J=!1;const ce=m(H,Y,X);r!==ce&&(r=ce,u(r.object)),J=g(C,H,Y,K),J&&_(C,H,Y,K),K!==null&&e.update(K,s.ELEMENT_ARRAY_BUFFER),(J||o)&&(o=!1,T(C,X,Y,H),K!==null&&s.bindBuffer(s.ELEMENT_ARRAY_BUFFER,e.get(K).buffer))}function l(){return s.createVertexArray()}function u(C){return s.bindVertexArray(C)}function d(C){return s.deleteVertexArray(C)}function m(C,X,Y){const H=Y.wireframe===!0;let K=n[C.id];K===void 0&&(K={},n[C.id]=K);let J=K[X.id];J===void 0&&(J={},K[X.id]=J);let ce=J[H];return ce===void 0&&(ce=p(l()),J[H]=ce),ce}function p(C){const X=[],Y=[],H=[];for(let K=0;K<t;K++)X[K]=0,Y[K]=0,H[K]=0;return{geometry:null,program:null,wireframe:!1,newAttributes:X,enabledAttributes:Y,attributeDivisors:H,object:C,attributes:{},index:null}}function g(C,X,Y,H){const K=r.attributes,J=X.attributes;let ce=0;const de=Y.getAttributes();for(const Q in de)if(de[Q].location>=0){const ve=K[Q];let Ue=J[Q];if(Ue===void 0&&(Q==="instanceMatrix"&&C.instanceMatrix&&(Ue=C.instanceMatrix),Q==="instanceColor"&&C.instanceColor&&(Ue=C.instanceColor)),ve===void 0||ve.attribute!==Ue||Ue&&ve.data!==Ue.data)return!0;ce++}return r.attributesNum!==ce||r.index!==H}function _(C,X,Y,H){const K={},J=X.attributes;let ce=0;const de=Y.getAttributes();for(const Q in de)if(de[Q].location>=0){let ve=J[Q];ve===void 0&&(Q==="instanceMatrix"&&C.instanceMatrix&&(ve=C.instanceMatrix),Q==="instanceColor"&&C.instanceColor&&(ve=C.instanceColor));const Ue={};Ue.attribute=ve,ve&&ve.data&&(Ue.data=ve.data),K[Q]=Ue,ce++}r.attributes=K,r.attributesNum=ce,r.index=H}function M(){const C=r.newAttributes;for(let X=0,Y=C.length;X<Y;X++)C[X]=0}function y(C){v(C,0)}function v(C,X){const Y=r.newAttributes,H=r.enabledAttributes,K=r.attributeDivisors;Y[C]=1,H[C]===0&&(s.enableVertexAttribArray(C),H[C]=1),K[C]!==X&&(s.vertexAttribDivisor(C,X),K[C]=X)}function w(){const C=r.newAttributes,X=r.enabledAttributes;for(let Y=0,H=X.length;Y<H;Y++)X[Y]!==C[Y]&&(s.disableVertexAttribArray(Y),X[Y]=0)}function S(C,X,Y,H,K,J,ce){ce===!0?s.vertexAttribIPointer(C,X,Y,K,J):s.vertexAttribPointer(C,X,Y,H,K,J)}function T(C,X,Y,H){M();const K=H.attributes,J=Y.getAttributes(),ce=X.defaultAttributeValues;for(const de in J){const Q=J[de];if(Q.location>=0){let pe=K[de];if(pe===void 0&&(de==="instanceMatrix"&&C.instanceMatrix&&(pe=C.instanceMatrix),de==="instanceColor"&&C.instanceColor&&(pe=C.instanceColor)),pe!==void 0){const ve=pe.normalized,Ue=pe.itemSize,Je=e.get(pe);if(Je===void 0)continue;const vt=Je.buffer,ae=Je.type,we=Je.bytesPerElement,ke=ae===s.INT||ae===s.UNSIGNED_INT||pe.gpuType===dh;if(pe.isInterleavedBufferAttribute){const Ee=pe.data,nt=Ee.stride,be=pe.offset;if(Ee.isInstancedInterleavedBuffer){for(let Z=0;Z<Q.locationSize;Z++)v(Q.location+Z,Ee.meshPerAttribute);C.isInstancedMesh!==!0&&H._maxInstanceCount===void 0&&(H._maxInstanceCount=Ee.meshPerAttribute*Ee.count)}else for(let Z=0;Z<Q.locationSize;Z++)y(Q.location+Z);s.bindBuffer(s.ARRAY_BUFFER,vt);for(let Z=0;Z<Q.locationSize;Z++)S(Q.location+Z,Ue/Q.locationSize,ae,ve,nt*we,(be+Ue/Q.locationSize*Z)*we,ke)}else{if(pe.isInstancedBufferAttribute){for(let Ee=0;Ee<Q.locationSize;Ee++)v(Q.location+Ee,pe.meshPerAttribute);C.isInstancedMesh!==!0&&H._maxInstanceCount===void 0&&(H._maxInstanceCount=pe.meshPerAttribute*pe.count)}else for(let Ee=0;Ee<Q.locationSize;Ee++)y(Q.location+Ee);s.bindBuffer(s.ARRAY_BUFFER,vt);for(let Ee=0;Ee<Q.locationSize;Ee++)S(Q.location+Ee,Ue/Q.locationSize,ae,ve,Ue*we,Ue/Q.locationSize*Ee*we,ke)}}else if(ce!==void 0){const ve=ce[de];if(ve!==void 0)switch(ve.length){case 2:s.vertexAttrib2fv(Q.location,ve);break;case 3:s.vertexAttrib3fv(Q.location,ve);break;case 4:s.vertexAttrib4fv(Q.location,ve);break;default:s.vertexAttrib1fv(Q.location,ve)}}}}w()}function z(){k();for(const C in n){const X=n[C];for(const Y in X){const H=X[Y];for(const K in H)d(H[K].object),delete H[K];delete X[Y]}delete n[C]}}function N(C){if(n[C.id]===void 0)return;const X=n[C.id];for(const Y in X){const H=X[Y];for(const K in H)d(H[K].object),delete H[K];delete X[Y]}delete n[C.id]}function A(C){for(const X in n){const Y=n[X];if(Y[C.id]===void 0)continue;const H=Y[C.id];for(const K in H)d(H[K].object),delete H[K];delete Y[C.id]}}function k(){D(),o=!0,r!==i&&(r=i,u(r.object))}function D(){i.geometry=null,i.program=null,i.wireframe=!1}return{setup:a,reset:k,resetDefaultState:D,dispose:z,releaseStatesOfGeometry:N,releaseStatesOfProgram:A,initAttributes:M,enableAttribute:y,disableUnusedAttributes:w}}function Dy(s,e,t){let n;function i(u){n=u}function r(u,d){s.drawArrays(n,u,d),t.update(d,n,1)}function o(u,d,m){m!==0&&(s.drawArraysInstanced(n,u,d,m),t.update(d,n,m))}function a(u,d,m){if(m===0)return;const p=e.get("WEBGL_multi_draw");if(p===null)for(let g=0;g<m;g++)this.render(u[g],d[g]);else{p.multiDrawArraysWEBGL(n,u,0,d,0,m);let g=0;for(let _=0;_<m;_++)g+=d[_];t.update(g,n,1)}}function l(u,d,m,p){if(m===0)return;const g=e.get("WEBGL_multi_draw");if(g===null)for(let _=0;_<u.length;_++)o(u[_],d[_],p[_]);else{g.multiDrawArraysInstancedWEBGL(n,u,0,d,0,p,0,m);let _=0;for(let M=0;M<m;M++)_+=d[M];for(let M=0;M<p.length;M++)t.update(_,n,p[M])}}this.setMode=i,this.render=r,this.renderInstances=o,this.renderMultiDraw=a,this.renderMultiDrawInstances=l}function Ny(s,e,t,n){let i;function r(){if(i!==void 0)return i;if(e.has("EXT_texture_filter_anisotropic")===!0){const N=e.get("EXT_texture_filter_anisotropic");i=s.getParameter(N.MAX_TEXTURE_MAX_ANISOTROPY_EXT)}else i=0;return i}function o(N){return!(N!==Cn&&n.convert(N)!==s.getParameter(s.IMPLEMENTATION_COLOR_READ_FORMAT))}function a(N){const A=N===Fn&&(e.has("EXT_color_buffer_half_float")||e.has("EXT_color_buffer_float"));return!(N!==bi&&n.convert(N)!==s.getParameter(s.IMPLEMENTATION_COLOR_READ_TYPE)&&N!==zn&&!A)}function l(N){if(N==="highp"){if(s.getShaderPrecisionFormat(s.VERTEX_SHADER,s.HIGH_FLOAT).precision>0&&s.getShaderPrecisionFormat(s.FRAGMENT_SHADER,s.HIGH_FLOAT).precision>0)return"highp";N="mediump"}return N==="mediump"&&s.getShaderPrecisionFormat(s.VERTEX_SHADER,s.MEDIUM_FLOAT).precision>0&&s.getShaderPrecisionFormat(s.FRAGMENT_SHADER,s.MEDIUM_FLOAT).precision>0?"mediump":"lowp"}let u=t.precision!==void 0?t.precision:"highp";const d=l(u);d!==u&&(console.warn("THREE.WebGLRenderer:",u,"not supported, using",d,"instead."),u=d);const m=t.logarithmicDepthBuffer===!0,p=s.getParameter(s.MAX_TEXTURE_IMAGE_UNITS),g=s.getParameter(s.MAX_VERTEX_TEXTURE_IMAGE_UNITS),_=s.getParameter(s.MAX_TEXTURE_SIZE),M=s.getParameter(s.MAX_CUBE_MAP_TEXTURE_SIZE),y=s.getParameter(s.MAX_VERTEX_ATTRIBS),v=s.getParameter(s.MAX_VERTEX_UNIFORM_VECTORS),w=s.getParameter(s.MAX_VARYING_VECTORS),S=s.getParameter(s.MAX_FRAGMENT_UNIFORM_VECTORS),T=g>0,z=s.getParameter(s.MAX_SAMPLES);return{isWebGL2:!0,getMaxAnisotropy:r,getMaxPrecision:l,textureFormatReadable:o,textureTypeReadable:a,precision:u,logarithmicDepthBuffer:m,maxTextures:p,maxVertexTextures:g,maxTextureSize:_,maxCubemapSize:M,maxAttributes:y,maxVertexUniforms:v,maxVaryings:w,maxFragmentUniforms:S,vertexTextures:T,maxSamples:z}}function Oy(s){const e=this;let t=null,n=0,i=!1,r=!1;const o=new $n,a=new ut,l={value:null,needsUpdate:!1};this.uniform=l,this.numPlanes=0,this.numIntersection=0,this.init=function(m,p){const g=m.length!==0||p||n!==0||i;return i=p,n=m.length,g},this.beginShadows=function(){r=!0,d(null)},this.endShadows=function(){r=!1},this.setGlobalState=function(m,p){t=d(m,p,0)},this.setState=function(m,p,g){const _=m.clippingPlanes,M=m.clipIntersection,y=m.clipShadows,v=s.get(m);if(!i||_===null||_.length===0||r&&!y)r?d(null):u();else{const w=r?0:n,S=w*4;let T=v.clippingState||null;l.value=T,T=d(_,p,S,g);for(let z=0;z!==S;++z)T[z]=t[z];v.clippingState=T,this.numIntersection=M?this.numPlanes:0,this.numPlanes+=w}};function u(){l.value!==t&&(l.value=t,l.needsUpdate=n>0),e.numPlanes=n,e.numIntersection=0}function d(m,p,g,_){const M=m!==null?m.length:0;let y=null;if(M!==0){if(y=l.value,_!==!0||y===null){const v=g+M*4,w=p.matrixWorldInverse;a.getNormalMatrix(w),(y===null||y.length<v)&&(y=new Float32Array(v));for(let S=0,T=g;S!==M;++S,T+=4)o.copy(m[S]).applyMatrix4(w,a),o.normal.toArray(y,T),y[T+3]=o.constant}l.value=y,l.needsUpdate=!0}return e.numPlanes=M,e.numIntersection=0,y}}function Uy(s){let e=new WeakMap;function t(o,a){return a===js?o.mapping=Mi:a===Ks&&(o.mapping=Hi),o}function n(o){if(o&&o.isTexture){const a=o.mapping;if(a===js||a===Ks)if(e.has(o)){const l=e.get(o).texture;return t(l,o.mapping)}else{const l=o.image;if(l&&l.height>0){const u=new Sp(l.height);return u.fromEquirectangularTexture(s,o),e.set(o,u),o.addEventListener("dispose",i),t(u.texture,o.mapping)}else return null}}return o}function i(o){const a=o.target;a.removeEventListener("dispose",i);const l=e.get(a);l!==void 0&&(e.delete(a),l.dispose())}function r(){e=new WeakMap}return{get:n,dispose:r}}class bo extends Ja{constructor(e=-1,t=1,n=1,i=-1,r=.1,o=2e3){super(),this.isOrthographicCamera=!0,this.type="OrthographicCamera",this.zoom=1,this.view=null,this.left=e,this.right=t,this.top=n,this.bottom=i,this.near=r,this.far=o,this.updateProjectionMatrix()}copy(e,t){return super.copy(e,t),this.left=e.left,this.right=e.right,this.top=e.top,this.bottom=e.bottom,this.near=e.near,this.far=e.far,this.zoom=e.zoom,this.view=e.view===null?null:Object.assign({},e.view),this}setViewOffset(e,t,n,i,r,o){this.view===null&&(this.view={enabled:!0,fullWidth:1,fullHeight:1,offsetX:0,offsetY:0,width:1,height:1}),this.view.enabled=!0,this.view.fullWidth=e,this.view.fullHeight=t,this.view.offsetX=n,this.view.offsetY=i,this.view.width=r,this.view.height=o,this.updateProjectionMatrix()}clearViewOffset(){this.view!==null&&(this.view.enabled=!1),this.updateProjectionMatrix()}updateProjectionMatrix(){const e=(this.right-this.left)/(2*this.zoom),t=(this.top-this.bottom)/(2*this.zoom),n=(this.right+this.left)/2,i=(this.top+this.bottom)/2;let r=n-e,o=n+e,a=i+t,l=i-t;if(this.view!==null&&this.view.enabled){const u=(this.right-this.left)/this.view.fullWidth/this.zoom,d=(this.top-this.bottom)/this.view.fullHeight/this.zoom;r+=u*this.view.offsetX,o=r+u*this.view.width,a-=d*this.view.offsetY,l=a-d*this.view.height}this.projectionMatrix.makeOrthographic(r,o,a,l,this.near,this.far,this.coordinateSystem),this.projectionMatrixInverse.copy(this.projectionMatrix).invert()}toJSON(e){const t=super.toJSON(e);return t.object.zoom=this.zoom,t.object.left=this.left,t.object.right=this.right,t.object.top=this.top,t.object.bottom=this.bottom,t.object.near=this.near,t.object.far=this.far,this.view!==null&&(t.object.view=Object.assign({},this.view)),t}}const is=4,Hu=[.125,.215,.35,.446,.526,.582],mr=20,Yl=new bo,Gu=new Oe;let ql=null,Zl=0,jl=0,Kl=!1;const fr=(1+Math.sqrt(5))/2,Zr=1/fr,Wu=[new O(-fr,Zr,0),new O(fr,Zr,0),new O(-Zr,0,fr),new O(Zr,0,fr),new O(0,fr,-Zr),new O(0,fr,Zr),new O(-1,1,-1),new O(1,1,-1),new O(-1,1,1),new O(1,1,1)];class Zc{constructor(e){this._renderer=e,this._pingPongRenderTarget=null,this._lodMax=0,this._cubeSize=0,this._lodPlanes=[],this._sizeLods=[],this._sigmas=[],this._blurMaterial=null,this._cubemapMaterial=null,this._equirectMaterial=null,this._compileMaterial(this._blurMaterial)}fromScene(e,t=0,n=.1,i=100){ql=this._renderer.getRenderTarget(),Zl=this._renderer.getActiveCubeFace(),jl=this._renderer.getActiveMipmapLevel(),Kl=this._renderer.xr.enabled,this._renderer.xr.enabled=!1,this._setSize(256);const r=this._allocateTargets();return r.depthBuffer=!0,this._sceneToCubeUV(e,n,i,r),t>0&&this._blur(r,0,0,t),this._applyPMREM(r),this._cleanup(r),r}fromEquirectangular(e,t=null){return this._fromTexture(e,t)}fromCubemap(e,t=null){return this._fromTexture(e,t)}compileCubemapShader(){this._cubemapMaterial===null&&(this._cubemapMaterial=qu(),this._compileMaterial(this._cubemapMaterial))}compileEquirectangularShader(){this._equirectMaterial===null&&(this._equirectMaterial=Yu(),this._compileMaterial(this._equirectMaterial))}dispose(){this._dispose(),this._cubemapMaterial!==null&&this._cubemapMaterial.dispose(),this._equirectMaterial!==null&&this._equirectMaterial.dispose()}_setSize(e){this._lodMax=Math.floor(Math.log2(e)),this._cubeSize=Math.pow(2,this._lodMax)}_dispose(){this._blurMaterial!==null&&this._blurMaterial.dispose(),this._pingPongRenderTarget!==null&&this._pingPongRenderTarget.dispose();for(let e=0;e<this._lodPlanes.length;e++)this._lodPlanes[e].dispose()}_cleanup(e){this._renderer.setRenderTarget(ql,Zl,jl),this._renderer.xr.enabled=Kl,e.scissorTest=!1,ea(e,0,0,e.width,e.height)}_fromTexture(e,t){e.mapping===Mi||e.mapping===Hi?this._setSize(e.image.length===0?16:e.image[0].width||e.image[0].image.width):this._setSize(e.image.width/4),ql=this._renderer.getRenderTarget(),Zl=this._renderer.getActiveCubeFace(),jl=this._renderer.getActiveMipmapLevel(),Kl=this._renderer.xr.enabled,this._renderer.xr.enabled=!1;const n=t||this._allocateTargets();return this._textureToCubeUV(e,n),this._applyPMREM(n),this._cleanup(n),n}_allocateTargets(){const e=3*Math.max(this._cubeSize,112),t=4*this._cubeSize,n={magFilter:Xt,minFilter:Xt,generateMipmaps:!1,type:Fn,format:Cn,colorSpace:wi,depthBuffer:!1},i=Xu(e,t,n);if(this._pingPongRenderTarget===null||this._pingPongRenderTarget.width!==e||this._pingPongRenderTarget.height!==t){this._pingPongRenderTarget!==null&&this._dispose(),this._pingPongRenderTarget=Xu(e,t,n);const{_lodMax:r}=this;({sizeLods:this._sizeLods,lodPlanes:this._lodPlanes,sigmas:this._sigmas}=Fy(r)),this._blurMaterial=By(r,e,t)}return i}_compileMaterial(e){const t=new Ie(this._lodPlanes[0],e);this._renderer.compile(t,Yl)}_sceneToCubeUV(e,t,n,i){const a=new en(90,1,t,n),l=[1,-1,1,1,1,1],u=[1,1,1,-1,-1,-1],d=this._renderer,m=d.autoClear,p=d.toneMapping;d.getClearColor(Gu),d.toneMapping=yi,d.autoClear=!1;const g=new ii({name:"PMREM.Background",side:_n,depthWrite:!1,depthTest:!1}),_=new Ie(new zt,g);let M=!1;const y=e.background;y?y.isColor&&(g.color.copy(y),e.background=null,M=!0):(g.color.copy(Gu),M=!0);for(let v=0;v<6;v++){const w=v%3;w===0?(a.up.set(0,l[v],0),a.lookAt(u[v],0,0)):w===1?(a.up.set(0,0,l[v]),a.lookAt(0,u[v],0)):(a.up.set(0,l[v],0),a.lookAt(0,0,u[v]));const S=this._cubeSize;ea(i,w*S,v>2?S:0,S,S),d.setRenderTarget(i),M&&d.render(_,a),d.render(e,a)}_.geometry.dispose(),_.material.dispose(),d.toneMapping=p,d.autoClear=m,e.background=y}_textureToCubeUV(e,t){const n=this._renderer,i=e.mapping===Mi||e.mapping===Hi;i?(this._cubemapMaterial===null&&(this._cubemapMaterial=qu()),this._cubemapMaterial.uniforms.flipEnvMap.value=e.isRenderTargetTexture===!1?-1:1):this._equirectMaterial===null&&(this._equirectMaterial=Yu());const r=i?this._cubemapMaterial:this._equirectMaterial,o=new Ie(this._lodPlanes[0],r),a=r.uniforms;a.envMap.value=e;const l=this._cubeSize;ea(t,0,0,3*l,2*l),n.setRenderTarget(t),n.render(o,Yl)}_applyPMREM(e){const t=this._renderer,n=t.autoClear;t.autoClear=!1;const i=this._lodPlanes.length;for(let r=1;r<i;r++){const o=Math.sqrt(this._sigmas[r]*this._sigmas[r]-this._sigmas[r-1]*this._sigmas[r-1]),a=Wu[(i-r-1)%Wu.length];this._blur(e,r-1,r,o,a)}t.autoClear=n}_blur(e,t,n,i,r){const o=this._pingPongRenderTarget;this._halfBlur(e,o,t,n,i,"latitudinal",r),this._halfBlur(o,e,n,n,i,"longitudinal",r)}_halfBlur(e,t,n,i,r,o,a){const l=this._renderer,u=this._blurMaterial;o!=="latitudinal"&&o!=="longitudinal"&&console.error("blur direction must be either latitudinal or longitudinal!");const d=3,m=new Ie(this._lodPlanes[i],u),p=u.uniforms,g=this._sizeLods[n]-1,_=isFinite(r)?Math.PI/(2*g):2*Math.PI/(2*mr-1),M=r/_,y=isFinite(r)?1+Math.floor(d*M):mr;y>mr&&console.warn(`sigmaRadians, ${r}, is too large and will clip, as it requested ${y} samples when the maximum is set to ${mr}`);const v=[];let w=0;for(let A=0;A<mr;++A){const k=A/M,D=Math.exp(-k*k/2);v.push(D),A===0?w+=D:A<y&&(w+=2*D)}for(let A=0;A<v.length;A++)v[A]=v[A]/w;p.envMap.value=e.texture,p.samples.value=y,p.weights.value=v,p.latitudinal.value=o==="latitudinal",a&&(p.poleAxis.value=a);const{_lodMax:S}=this;p.dTheta.value=_,p.mipInt.value=S-n;const T=this._sizeLods[i],z=3*T*(i>S-is?i-S+is:0),N=4*(this._cubeSize-T);ea(t,z,N,3*T,2*T),l.setRenderTarget(t),l.render(m,Yl)}}function Fy(s){const e=[],t=[],n=[];let i=s;const r=s-is+1+Hu.length;for(let o=0;o<r;o++){const a=Math.pow(2,i);t.push(a);let l=1/a;o>s-is?l=Hu[o-s+is-1]:o===0&&(l=0),n.push(l);const u=1/(a-2),d=-u,m=1+u,p=[d,d,m,d,m,m,d,d,m,m,d,m],g=6,_=6,M=3,y=2,v=1,w=new Float32Array(M*_*g),S=new Float32Array(y*_*g),T=new Float32Array(v*_*g);for(let N=0;N<g;N++){const A=N%3*2/3-1,k=N>2?0:-1,D=[A,k,0,A+2/3,k,0,A+2/3,k+1,0,A,k,0,A+2/3,k+1,0,A,k+1,0];w.set(D,M*_*N),S.set(p,y*_*N);const C=[N,N,N,N,N,N];T.set(C,v*_*N)}const z=new at;z.setAttribute("position",new Rt(w,M)),z.setAttribute("uv",new Rt(S,y)),z.setAttribute("faceIndex",new Rt(T,v)),e.push(z),i>is&&i--}return{lodPlanes:e,sizeLods:t,sigmas:n}}function Xu(s,e,t){const n=new qt(s,e,t);return n.texture.mapping=us,n.texture.name="PMREM.cubeUv",n.scissorTest=!0,n}function ea(s,e,t,n,i){s.viewport.set(e,t,n,i),s.scissor.set(e,t,n,i)}function By(s,e,t){const n=new Float32Array(mr),i=new O(0,1,0);return new tn({name:"SphericalGaussianBlur",defines:{n:mr,CUBEUV_TEXEL_WIDTH:1/e,CUBEUV_TEXEL_HEIGHT:1/t,CUBEUV_MAX_MIP:`${s}.0`},uniforms:{envMap:{value:null},samples:{value:1},weights:{value:n},latitudinal:{value:!1},dTheta:{value:0},mipInt:{value:0},poleAxis:{value:i}},vertexShader:Th(),fragmentShader:`

			precision mediump float;
			precision mediump int;

			varying vec3 vOutputDirection;

			uniform sampler2D envMap;
			uniform int samples;
			uniform float weights[ n ];
			uniform bool latitudinal;
			uniform float dTheta;
			uniform float mipInt;
			uniform vec3 poleAxis;

			#define ENVMAP_TYPE_CUBE_UV
			#include <cube_uv_reflection_fragment>

			vec3 getSample( float theta, vec3 axis ) {

				float cosTheta = cos( theta );
				// Rodrigues' axis-angle rotation
				vec3 sampleDirection = vOutputDirection * cosTheta
					+ cross( axis, vOutputDirection ) * sin( theta )
					+ axis * dot( axis, vOutputDirection ) * ( 1.0 - cosTheta );

				return bilinearCubeUV( envMap, sampleDirection, mipInt );

			}

			void main() {

				vec3 axis = latitudinal ? poleAxis : cross( poleAxis, vOutputDirection );

				if ( all( equal( axis, vec3( 0.0 ) ) ) ) {

					axis = vec3( vOutputDirection.z, 0.0, - vOutputDirection.x );

				}

				axis = normalize( axis );

				gl_FragColor = vec4( 0.0, 0.0, 0.0, 1.0 );
				gl_FragColor.rgb += weights[ 0 ] * getSample( 0.0, axis );

				for ( int i = 1; i < n; i++ ) {

					if ( i >= samples ) {

						break;

					}

					float theta = dTheta * float( i );
					gl_FragColor.rgb += weights[ i ] * getSample( -1.0 * theta, axis );
					gl_FragColor.rgb += weights[ i ] * getSample( theta, axis );

				}

			}
		`,blending:kn,depthTest:!1,depthWrite:!1})}function Yu(){return new tn({name:"EquirectangularToCubeUV",uniforms:{envMap:{value:null}},vertexShader:Th(),fragmentShader:`

			precision mediump float;
			precision mediump int;

			varying vec3 vOutputDirection;

			uniform sampler2D envMap;

			#include <common>

			void main() {

				vec3 outputDirection = normalize( vOutputDirection );
				vec2 uv = equirectUv( outputDirection );

				gl_FragColor = vec4( texture2D ( envMap, uv ).rgb, 1.0 );

			}
		`,blending:kn,depthTest:!1,depthWrite:!1})}function qu(){return new tn({name:"CubemapToCubeUV",uniforms:{envMap:{value:null},flipEnvMap:{value:-1}},vertexShader:Th(),fragmentShader:`

			precision mediump float;
			precision mediump int;

			uniform float flipEnvMap;

			varying vec3 vOutputDirection;

			uniform samplerCube envMap;

			void main() {

				gl_FragColor = textureCube( envMap, vec3( flipEnvMap * vOutputDirection.x, vOutputDirection.yz ) );

			}
		`,blending:kn,depthTest:!1,depthWrite:!1})}function Th(){return`

		precision mediump float;
		precision mediump int;

		attribute float faceIndex;

		varying vec3 vOutputDirection;

		// RH coordinate system; PMREM face-indexing convention
		vec3 getDirection( vec2 uv, float face ) {

			uv = 2.0 * uv - 1.0;

			vec3 direction = vec3( uv, 1.0 );

			if ( face == 0.0 ) {

				direction = direction.zyx; // ( 1, v, u ) pos x

			} else if ( face == 1.0 ) {

				direction = direction.xzy;
				direction.xz *= -1.0; // ( -u, 1, -v ) pos y

			} else if ( face == 2.0 ) {

				direction.x *= -1.0; // ( -u, v, 1 ) pos z

			} else if ( face == 3.0 ) {

				direction = direction.zyx;
				direction.xz *= -1.0; // ( -1, v, -u ) neg x

			} else if ( face == 4.0 ) {

				direction = direction.xzy;
				direction.xy *= -1.0; // ( -u, -1, v ) neg y

			} else if ( face == 5.0 ) {

				direction.z *= -1.0; // ( u, v, -1 ) neg z

			}

			return direction;

		}

		void main() {

			vOutputDirection = getDirection( uv, faceIndex );
			gl_Position = vec4( position, 1.0 );

		}
	`}function zy(s){let e=new WeakMap,t=null;function n(a){if(a&&a.isTexture){const l=a.mapping,u=l===js||l===Ks,d=l===Mi||l===Hi;if(u||d){let m=e.get(a);const p=m!==void 0?m.texture.pmremVersion:0;if(a.isRenderTargetTexture&&a.pmremVersion!==p)return t===null&&(t=new Zc(s)),m=u?t.fromEquirectangular(a,m):t.fromCubemap(a,m),m.texture.pmremVersion=a.pmremVersion,e.set(a,m),m.texture;if(m!==void 0)return m.texture;{const g=a.image;return u&&g&&g.height>0||d&&g&&i(g)?(t===null&&(t=new Zc(s)),m=u?t.fromEquirectangular(a):t.fromCubemap(a),m.texture.pmremVersion=a.pmremVersion,e.set(a,m),a.addEventListener("dispose",r),m.texture):null}}}return a}function i(a){let l=0;const u=6;for(let d=0;d<u;d++)a[d]!==void 0&&l++;return l===u}function r(a){const l=a.target;l.removeEventListener("dispose",r);const u=e.get(l);u!==void 0&&(e.delete(l),u.dispose())}function o(){e=new WeakMap,t!==null&&(t.dispose(),t=null)}return{get:n,dispose:o}}function ky(s){const e={};function t(n){if(e[n]!==void 0)return e[n];let i;switch(n){case"WEBGL_depth_texture":i=s.getExtension("WEBGL_depth_texture")||s.getExtension("MOZ_WEBGL_depth_texture")||s.getExtension("WEBKIT_WEBGL_depth_texture");break;case"EXT_texture_filter_anisotropic":i=s.getExtension("EXT_texture_filter_anisotropic")||s.getExtension("MOZ_EXT_texture_filter_anisotropic")||s.getExtension("WEBKIT_EXT_texture_filter_anisotropic");break;case"WEBGL_compressed_texture_s3tc":i=s.getExtension("WEBGL_compressed_texture_s3tc")||s.getExtension("MOZ_WEBGL_compressed_texture_s3tc")||s.getExtension("WEBKIT_WEBGL_compressed_texture_s3tc");break;case"WEBGL_compressed_texture_pvrtc":i=s.getExtension("WEBGL_compressed_texture_pvrtc")||s.getExtension("WEBKIT_WEBGL_compressed_texture_pvrtc");break;default:i=s.getExtension(n)}return e[n]=i,i}return{has:function(n){return t(n)!==null},init:function(){t("EXT_color_buffer_float"),t("WEBGL_clip_cull_distance"),t("OES_texture_float_linear"),t("EXT_color_buffer_half_float"),t("WEBGL_multisampled_render_to_texture"),t("WEBGL_render_shared_exponent")},get:function(n){const i=t(n);return i===null&&console.warn("THREE.WebGLRenderer: "+n+" extension not supported."),i}}}function Vy(s,e,t,n){const i={},r=new WeakMap;function o(m){const p=m.target;p.index!==null&&e.remove(p.index);for(const _ in p.attributes)e.remove(p.attributes[_]);for(const _ in p.morphAttributes){const M=p.morphAttributes[_];for(let y=0,v=M.length;y<v;y++)e.remove(M[y])}p.removeEventListener("dispose",o),delete i[p.id];const g=r.get(p);g&&(e.remove(g),r.delete(p)),n.releaseStatesOfGeometry(p),p.isInstancedBufferGeometry===!0&&delete p._maxInstanceCount,t.memory.geometries--}function a(m,p){return i[p.id]===!0||(p.addEventListener("dispose",o),i[p.id]=!0,t.memory.geometries++),p}function l(m){const p=m.attributes;for(const _ in p)e.update(p[_],s.ARRAY_BUFFER);const g=m.morphAttributes;for(const _ in g){const M=g[_];for(let y=0,v=M.length;y<v;y++)e.update(M[y],s.ARRAY_BUFFER)}}function u(m){const p=[],g=m.index,_=m.attributes.position;let M=0;if(g!==null){const w=g.array;M=g.version;for(let S=0,T=w.length;S<T;S+=3){const z=w[S+0],N=w[S+1],A=w[S+2];p.push(z,N,N,A,A,z)}}else if(_!==void 0){const w=_.array;M=_.version;for(let S=0,T=w.length/3-1;S<T;S+=3){const z=S+0,N=S+1,A=S+2;p.push(z,N,N,A,A,z)}}else return;const y=new(mp(p)?Eh:wh)(p,1);y.version=M;const v=r.get(m);v&&e.remove(v),r.set(m,y)}function d(m){const p=r.get(m);if(p){const g=m.index;g!==null&&p.version<g.version&&u(m)}else u(m);return r.get(m)}return{get:a,update:l,getWireframeAttribute:d}}function Hy(s,e,t){let n;function i(p){n=p}let r,o;function a(p){r=p.type,o=p.bytesPerElement}function l(p,g){s.drawElements(n,g,r,p*o),t.update(g,n,1)}function u(p,g,_){_!==0&&(s.drawElementsInstanced(n,g,r,p*o,_),t.update(g,n,_))}function d(p,g,_){if(_===0)return;const M=e.get("WEBGL_multi_draw");if(M===null)for(let y=0;y<_;y++)this.render(p[y]/o,g[y]);else{M.multiDrawElementsWEBGL(n,g,0,r,p,0,_);let y=0;for(let v=0;v<_;v++)y+=g[v];t.update(y,n,1)}}function m(p,g,_,M){if(_===0)return;const y=e.get("WEBGL_multi_draw");if(y===null)for(let v=0;v<p.length;v++)u(p[v]/o,g[v],M[v]);else{y.multiDrawElementsInstancedWEBGL(n,g,0,r,p,0,M,0,_);let v=0;for(let w=0;w<_;w++)v+=g[w];for(let w=0;w<M.length;w++)t.update(v,n,M[w])}}this.setMode=i,this.setIndex=a,this.render=l,this.renderInstances=u,this.renderMultiDraw=d,this.renderMultiDrawInstances=m}function Gy(s){const e={geometries:0,textures:0},t={frame:0,calls:0,triangles:0,points:0,lines:0};function n(r,o,a){switch(t.calls++,o){case s.TRIANGLES:t.triangles+=a*(r/3);break;case s.LINES:t.lines+=a*(r/2);break;case s.LINE_STRIP:t.lines+=a*(r-1);break;case s.LINE_LOOP:t.lines+=a*r;break;case s.POINTS:t.points+=a*r;break;default:console.error("THREE.WebGLInfo: Unknown draw mode:",o);break}}function i(){t.calls=0,t.triangles=0,t.points=0,t.lines=0}return{memory:e,render:t,programs:null,autoReset:!0,reset:i,update:n}}function Wy(s,e,t){const n=new WeakMap,i=new Ct;function r(o,a,l){const u=o.morphTargetInfluences,d=a.morphAttributes.position||a.morphAttributes.normal||a.morphAttributes.color,m=d!==void 0?d.length:0;let p=n.get(a);if(p===void 0||p.count!==m){let C=function(){k.dispose(),n.delete(a),a.removeEventListener("dispose",C)};var g=C;p!==void 0&&p.texture.dispose();const _=a.morphAttributes.position!==void 0,M=a.morphAttributes.normal!==void 0,y=a.morphAttributes.color!==void 0,v=a.morphAttributes.position||[],w=a.morphAttributes.normal||[],S=a.morphAttributes.color||[];let T=0;_===!0&&(T=1),M===!0&&(T=2),y===!0&&(T=3);let z=a.attributes.position.count*T,N=1;z>e.maxTextureSize&&(N=Math.ceil(z/e.maxTextureSize),z=e.maxTextureSize);const A=new Float32Array(z*N*4*m),k=new ja(A,z,N,m);k.type=zn,k.needsUpdate=!0;const D=T*4;for(let X=0;X<m;X++){const Y=v[X],H=w[X],K=S[X],J=z*N*4*X;for(let ce=0;ce<Y.count;ce++){const de=ce*D;_===!0&&(i.fromBufferAttribute(Y,ce),A[J+de+0]=i.x,A[J+de+1]=i.y,A[J+de+2]=i.z,A[J+de+3]=0),M===!0&&(i.fromBufferAttribute(H,ce),A[J+de+4]=i.x,A[J+de+5]=i.y,A[J+de+6]=i.z,A[J+de+7]=0),y===!0&&(i.fromBufferAttribute(K,ce),A[J+de+8]=i.x,A[J+de+9]=i.y,A[J+de+10]=i.z,A[J+de+11]=K.itemSize===4?i.w:1)}}p={count:m,texture:k,size:new se(z,N)},n.set(a,p),a.addEventListener("dispose",C)}if(o.isInstancedMesh===!0&&o.morphTexture!==null)l.getUniforms().setValue(s,"morphTexture",o.morphTexture,t);else{let _=0;for(let y=0;y<u.length;y++)_+=u[y];const M=a.morphTargetsRelative?1:1-_;l.getUniforms().setValue(s,"morphTargetBaseInfluence",M),l.getUniforms().setValue(s,"morphTargetInfluences",u)}l.getUniforms().setValue(s,"morphTargetsTexture",p.texture,t),l.getUniforms().setValue(s,"morphTargetsTextureSize",p.size)}return{update:r}}function Xy(s,e,t,n){let i=new WeakMap;function r(l){const u=n.render.frame,d=l.geometry,m=e.get(l,d);if(i.get(m)!==u&&(e.update(m),i.set(m,u)),l.isInstancedMesh&&(l.hasEventListener("dispose",a)===!1&&l.addEventListener("dispose",a),i.get(l)!==u&&(t.update(l.instanceMatrix,s.ARRAY_BUFFER),l.instanceColor!==null&&t.update(l.instanceColor,s.ARRAY_BUFFER),i.set(l,u))),l.isSkinnedMesh){const p=l.skeleton;i.get(p)!==u&&(p.update(),i.set(p,u))}return m}function o(){i=new WeakMap}function a(l){const u=l.target;u.removeEventListener("dispose",a),t.remove(u.instanceMatrix),u.instanceColor!==null&&t.remove(u.instanceColor)}return{update:r,dispose:o}}class Ah extends Vt{constructor(e,t,n,i,r,o,a,l,u,d){if(d=d!==void 0?d:Mr,d!==Mr&&d!==as)throw new Error("DepthTexture format must be either THREE.DepthFormat or THREE.DepthStencilFormat");n===void 0&&d===Mr&&(n=wr),n===void 0&&d===as&&(n=ds),super(null,i,r,o,a,l,d,n,u),this.isDepthTexture=!0,this.image={width:e,height:t},this.magFilter=a!==void 0?a:nn,this.minFilter=l!==void 0?l:nn,this.flipY=!1,this.generateMipmaps=!1,this.compareFunction=null}copy(e){return super.copy(e),this.compareFunction=e.compareFunction,this}toJSON(e){const t=super.toJSON(e);return this.compareFunction!==null&&(t.compareFunction=this.compareFunction),t}}const Ep=new Vt,Tp=new Ah(1,1);Tp.compareFunction=Mh;const Ap=new ja,Cp=new Sh,Rp=new xo,Zu=[],ju=[],Ku=new Float32Array(16),Ju=new Float32Array(9),$u=new Float32Array(4);function fs(s,e,t){const n=s[0];if(n<=0||n>0)return s;const i=e*t;let r=Zu[i];if(r===void 0&&(r=new Float32Array(i),Zu[i]=r),e!==0){n.toArray(r,0);for(let o=1,a=0;o!==e;++o)a+=t,s[o].toArray(r,a)}return r}function Zt(s,e){if(s.length!==e.length)return!1;for(let t=0,n=s.length;t<n;t++)if(s[t]!==e[t])return!1;return!0}function jt(s,e){for(let t=0,n=e.length;t<n;t++)s[t]=e[t]}function $a(s,e){let t=ju[e];t===void 0&&(t=new Int32Array(e),ju[e]=t);for(let n=0;n!==e;++n)t[n]=s.allocateTextureUnit();return t}function Yy(s,e){const t=this.cache;t[0]!==e&&(s.uniform1f(this.addr,e),t[0]=e)}function qy(s,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y)&&(s.uniform2f(this.addr,e.x,e.y),t[0]=e.x,t[1]=e.y);else{if(Zt(t,e))return;s.uniform2fv(this.addr,e),jt(t,e)}}function Zy(s,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z)&&(s.uniform3f(this.addr,e.x,e.y,e.z),t[0]=e.x,t[1]=e.y,t[2]=e.z);else if(e.r!==void 0)(t[0]!==e.r||t[1]!==e.g||t[2]!==e.b)&&(s.uniform3f(this.addr,e.r,e.g,e.b),t[0]=e.r,t[1]=e.g,t[2]=e.b);else{if(Zt(t,e))return;s.uniform3fv(this.addr,e),jt(t,e)}}function jy(s,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z||t[3]!==e.w)&&(s.uniform4f(this.addr,e.x,e.y,e.z,e.w),t[0]=e.x,t[1]=e.y,t[2]=e.z,t[3]=e.w);else{if(Zt(t,e))return;s.uniform4fv(this.addr,e),jt(t,e)}}function Ky(s,e){const t=this.cache,n=e.elements;if(n===void 0){if(Zt(t,e))return;s.uniformMatrix2fv(this.addr,!1,e),jt(t,e)}else{if(Zt(t,n))return;$u.set(n),s.uniformMatrix2fv(this.addr,!1,$u),jt(t,n)}}function Jy(s,e){const t=this.cache,n=e.elements;if(n===void 0){if(Zt(t,e))return;s.uniformMatrix3fv(this.addr,!1,e),jt(t,e)}else{if(Zt(t,n))return;Ju.set(n),s.uniformMatrix3fv(this.addr,!1,Ju),jt(t,n)}}function $y(s,e){const t=this.cache,n=e.elements;if(n===void 0){if(Zt(t,e))return;s.uniformMatrix4fv(this.addr,!1,e),jt(t,e)}else{if(Zt(t,n))return;Ku.set(n),s.uniformMatrix4fv(this.addr,!1,Ku),jt(t,n)}}function Qy(s,e){const t=this.cache;t[0]!==e&&(s.uniform1i(this.addr,e),t[0]=e)}function ex(s,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y)&&(s.uniform2i(this.addr,e.x,e.y),t[0]=e.x,t[1]=e.y);else{if(Zt(t,e))return;s.uniform2iv(this.addr,e),jt(t,e)}}function tx(s,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z)&&(s.uniform3i(this.addr,e.x,e.y,e.z),t[0]=e.x,t[1]=e.y,t[2]=e.z);else{if(Zt(t,e))return;s.uniform3iv(this.addr,e),jt(t,e)}}function nx(s,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z||t[3]!==e.w)&&(s.uniform4i(this.addr,e.x,e.y,e.z,e.w),t[0]=e.x,t[1]=e.y,t[2]=e.z,t[3]=e.w);else{if(Zt(t,e))return;s.uniform4iv(this.addr,e),jt(t,e)}}function ix(s,e){const t=this.cache;t[0]!==e&&(s.uniform1ui(this.addr,e),t[0]=e)}function rx(s,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y)&&(s.uniform2ui(this.addr,e.x,e.y),t[0]=e.x,t[1]=e.y);else{if(Zt(t,e))return;s.uniform2uiv(this.addr,e),jt(t,e)}}function sx(s,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z)&&(s.uniform3ui(this.addr,e.x,e.y,e.z),t[0]=e.x,t[1]=e.y,t[2]=e.z);else{if(Zt(t,e))return;s.uniform3uiv(this.addr,e),jt(t,e)}}function ox(s,e){const t=this.cache;if(e.x!==void 0)(t[0]!==e.x||t[1]!==e.y||t[2]!==e.z||t[3]!==e.w)&&(s.uniform4ui(this.addr,e.x,e.y,e.z,e.w),t[0]=e.x,t[1]=e.y,t[2]=e.z,t[3]=e.w);else{if(Zt(t,e))return;s.uniform4uiv(this.addr,e),jt(t,e)}}function ax(s,e,t){const n=this.cache,i=t.allocateTextureUnit();n[0]!==i&&(s.uniform1i(this.addr,i),n[0]=i);const r=this.type===s.SAMPLER_2D_SHADOW?Tp:Ep;t.setTexture2D(e||r,i)}function lx(s,e,t){const n=this.cache,i=t.allocateTextureUnit();n[0]!==i&&(s.uniform1i(this.addr,i),n[0]=i),t.setTexture3D(e||Cp,i)}function cx(s,e,t){const n=this.cache,i=t.allocateTextureUnit();n[0]!==i&&(s.uniform1i(this.addr,i),n[0]=i),t.setTextureCube(e||Rp,i)}function hx(s,e,t){const n=this.cache,i=t.allocateTextureUnit();n[0]!==i&&(s.uniform1i(this.addr,i),n[0]=i),t.setTexture2DArray(e||Ap,i)}function ux(s){switch(s){case 5126:return Yy;case 35664:return qy;case 35665:return Zy;case 35666:return jy;case 35674:return Ky;case 35675:return Jy;case 35676:return $y;case 5124:case 35670:return Qy;case 35667:case 35671:return ex;case 35668:case 35672:return tx;case 35669:case 35673:return nx;case 5125:return ix;case 36294:return rx;case 36295:return sx;case 36296:return ox;case 35678:case 36198:case 36298:case 36306:case 35682:return ax;case 35679:case 36299:case 36307:return lx;case 35680:case 36300:case 36308:case 36293:return cx;case 36289:case 36303:case 36311:case 36292:return hx}}function dx(s,e){s.uniform1fv(this.addr,e)}function fx(s,e){const t=fs(e,this.size,2);s.uniform2fv(this.addr,t)}function px(s,e){const t=fs(e,this.size,3);s.uniform3fv(this.addr,t)}function mx(s,e){const t=fs(e,this.size,4);s.uniform4fv(this.addr,t)}function gx(s,e){const t=fs(e,this.size,4);s.uniformMatrix2fv(this.addr,!1,t)}function vx(s,e){const t=fs(e,this.size,9);s.uniformMatrix3fv(this.addr,!1,t)}function _x(s,e){const t=fs(e,this.size,16);s.uniformMatrix4fv(this.addr,!1,t)}function yx(s,e){s.uniform1iv(this.addr,e)}function xx(s,e){s.uniform2iv(this.addr,e)}function Mx(s,e){s.uniform3iv(this.addr,e)}function bx(s,e){s.uniform4iv(this.addr,e)}function Sx(s,e){s.uniform1uiv(this.addr,e)}function wx(s,e){s.uniform2uiv(this.addr,e)}function Ex(s,e){s.uniform3uiv(this.addr,e)}function Tx(s,e){s.uniform4uiv(this.addr,e)}function Ax(s,e,t){const n=this.cache,i=e.length,r=$a(t,i);Zt(n,r)||(s.uniform1iv(this.addr,r),jt(n,r));for(let o=0;o!==i;++o)t.setTexture2D(e[o]||Ep,r[o])}function Cx(s,e,t){const n=this.cache,i=e.length,r=$a(t,i);Zt(n,r)||(s.uniform1iv(this.addr,r),jt(n,r));for(let o=0;o!==i;++o)t.setTexture3D(e[o]||Cp,r[o])}function Rx(s,e,t){const n=this.cache,i=e.length,r=$a(t,i);Zt(n,r)||(s.uniform1iv(this.addr,r),jt(n,r));for(let o=0;o!==i;++o)t.setTextureCube(e[o]||Rp,r[o])}function Px(s,e,t){const n=this.cache,i=e.length,r=$a(t,i);Zt(n,r)||(s.uniform1iv(this.addr,r),jt(n,r));for(let o=0;o!==i;++o)t.setTexture2DArray(e[o]||Ap,r[o])}function Ix(s){switch(s){case 5126:return dx;case 35664:return fx;case 35665:return px;case 35666:return mx;case 35674:return gx;case 35675:return vx;case 35676:return _x;case 5124:case 35670:return yx;case 35667:case 35671:return xx;case 35668:case 35672:return Mx;case 35669:case 35673:return bx;case 5125:return Sx;case 36294:return wx;case 36295:return Ex;case 36296:return Tx;case 35678:case 36198:case 36298:case 36306:case 35682:return Ax;case 35679:case 36299:case 36307:return Cx;case 35680:case 36300:case 36308:case 36293:return Rx;case 36289:case 36303:case 36311:case 36292:return Px}}class Lx{constructor(e,t,n){this.id=e,this.addr=n,this.cache=[],this.type=t.type,this.setValue=ux(t.type)}}class Dx{constructor(e,t,n){this.id=e,this.addr=n,this.cache=[],this.type=t.type,this.size=t.size,this.setValue=Ix(t.type)}}class Nx{constructor(e){this.id=e,this.seq=[],this.map={}}setValue(e,t,n){const i=this.seq;for(let r=0,o=i.length;r!==o;++r){const a=i[r];a.setValue(e,t[a.id],n)}}}const Jl=/(\w+)(\])?(\[|\.)?/g;function Qu(s,e){s.seq.push(e),s.map[e.id]=e}function Ox(s,e,t){const n=s.name,i=n.length;for(Jl.lastIndex=0;;){const r=Jl.exec(n),o=Jl.lastIndex;let a=r[1];const l=r[2]==="]",u=r[3];if(l&&(a=a|0),u===void 0||u==="["&&o+2===i){Qu(t,u===void 0?new Lx(a,s,e):new Dx(a,s,e));break}else{let m=t.map[a];m===void 0&&(m=new Nx(a),Qu(t,m)),t=m}}}class Ua{constructor(e,t){this.seq=[],this.map={};const n=e.getProgramParameter(t,e.ACTIVE_UNIFORMS);for(let i=0;i<n;++i){const r=e.getActiveUniform(t,i),o=e.getUniformLocation(t,r.name);Ox(r,o,this)}}setValue(e,t,n,i){const r=this.map[t];r!==void 0&&r.setValue(e,n,i)}setOptional(e,t,n){const i=t[n];i!==void 0&&this.setValue(e,n,i)}static upload(e,t,n,i){for(let r=0,o=t.length;r!==o;++r){const a=t[r],l=n[a.id];l.needsUpdate!==!1&&a.setValue(e,l.value,i)}}static seqWithValue(e,t){const n=[];for(let i=0,r=e.length;i!==r;++i){const o=e[i];o.id in t&&n.push(o)}return n}}function ed(s,e,t){const n=s.createShader(e);return s.shaderSource(n,t),s.compileShader(n),n}const Ux=37297;let Fx=0;function Bx(s,e){const t=s.split(`
`),n=[],i=Math.max(e-6,0),r=Math.min(e+6,t.length);for(let o=i;o<r;o++){const a=o+1;n.push(`${a===e?">":" "} ${a}: ${t[o]}`)}return n.join(`
`)}function zx(s){const e=wt.getPrimaries(wt.workingColorSpace),t=wt.getPrimaries(s);let n;switch(e===t?n="":e===ro&&t===io?n="LinearDisplayP3ToLinearSRGB":e===io&&t===ro&&(n="LinearSRGBToLinearDisplayP3"),s){case wi:case _o:return[n,"LinearTransferOETF"];case Un:case Za:return[n,"sRGBTransferOETF"];default:return console.warn("THREE.WebGLProgram: Unsupported color space:",s),[n,"LinearTransferOETF"]}}function td(s,e,t){const n=s.getShaderParameter(e,s.COMPILE_STATUS),i=s.getShaderInfoLog(e).trim();if(n&&i==="")return"";const r=/ERROR: 0:(\d+)/.exec(i);if(r){const o=parseInt(r[1]);return t.toUpperCase()+`

`+i+`

`+Bx(s.getShaderSource(e),o)}else return i}function kx(s,e){const t=zx(e);return`vec4 ${s}( vec4 value ) { return ${t[0]}( ${t[1]}( value ) ); }`}function Vx(s,e){let t;switch(e){case rh:t="Linear";break;case sh:t="Reinhard";break;case oh:t="OptimizedCineon";break;case ah:t="ACESFilmic";break;case lh:t="AgX";break;case ch:t="Neutral";break;case Xf:t="Custom";break;default:console.warn("THREE.WebGLProgram: Unsupported toneMapping:",e),t="Linear"}return"vec3 "+s+"( vec3 color ) { return "+t+"ToneMapping( color ); }"}function Hx(s){return[s.extensionClipCullDistance?"#extension GL_ANGLE_clip_cull_distance : require":"",s.extensionMultiDraw?"#extension GL_ANGLE_multi_draw : require":""].filter(ks).join(`
`)}function Gx(s){const e=[];for(const t in s){const n=s[t];n!==!1&&e.push("#define "+t+" "+n)}return e.join(`
`)}function Wx(s,e){const t={},n=s.getProgramParameter(e,s.ACTIVE_ATTRIBUTES);for(let i=0;i<n;i++){const r=s.getActiveAttrib(e,i),o=r.name;let a=1;r.type===s.FLOAT_MAT2&&(a=2),r.type===s.FLOAT_MAT3&&(a=3),r.type===s.FLOAT_MAT4&&(a=4),t[o]={type:r.type,location:s.getAttribLocation(e,o),locationSize:a}}return t}function ks(s){return s!==""}function nd(s,e){const t=e.numSpotLightShadows+e.numSpotLightMaps-e.numSpotLightShadowsWithMaps;return s.replace(/NUM_DIR_LIGHTS/g,e.numDirLights).replace(/NUM_SPOT_LIGHTS/g,e.numSpotLights).replace(/NUM_SPOT_LIGHT_MAPS/g,e.numSpotLightMaps).replace(/NUM_SPOT_LIGHT_COORDS/g,t).replace(/NUM_RECT_AREA_LIGHTS/g,e.numRectAreaLights).replace(/NUM_POINT_LIGHTS/g,e.numPointLights).replace(/NUM_HEMI_LIGHTS/g,e.numHemiLights).replace(/NUM_DIR_LIGHT_SHADOWS/g,e.numDirLightShadows).replace(/NUM_SPOT_LIGHT_SHADOWS_WITH_MAPS/g,e.numSpotLightShadowsWithMaps).replace(/NUM_SPOT_LIGHT_SHADOWS/g,e.numSpotLightShadows).replace(/NUM_POINT_LIGHT_SHADOWS/g,e.numPointLightShadows)}function id(s,e){return s.replace(/NUM_CLIPPING_PLANES/g,e.numClippingPlanes).replace(/UNION_CLIPPING_PLANES/g,e.numClippingPlanes-e.numClipIntersection)}const Xx=/^[ \t]*#include +<([\w\d./]+)>/gm;function jc(s){return s.replace(Xx,qx)}const Yx=new Map;function qx(s,e){let t=mt[e];if(t===void 0){const n=Yx.get(e);if(n!==void 0)t=mt[n],console.warn('THREE.WebGLRenderer: Shader chunk "%s" has been deprecated. Use "%s" instead.',e,n);else throw new Error("Can not resolve #include <"+e+">")}return jc(t)}const Zx=/#pragma unroll_loop_start\s+for\s*\(\s*int\s+i\s*=\s*(\d+)\s*;\s*i\s*<\s*(\d+)\s*;\s*i\s*\+\+\s*\)\s*{([\s\S]+?)}\s+#pragma unroll_loop_end/g;function rd(s){return s.replace(Zx,jx)}function jx(s,e,t,n){let i="";for(let r=parseInt(e);r<parseInt(t);r++)i+=n.replace(/\[\s*i\s*\]/g,"[ "+r+" ]").replace(/UNROLLED_LOOP_INDEX/g,r);return i}function sd(s){let e=`precision ${s.precision} float;
	precision ${s.precision} int;
	precision ${s.precision} sampler2D;
	precision ${s.precision} samplerCube;
	precision ${s.precision} sampler3D;
	precision ${s.precision} sampler2DArray;
	precision ${s.precision} sampler2DShadow;
	precision ${s.precision} samplerCubeShadow;
	precision ${s.precision} sampler2DArrayShadow;
	precision ${s.precision} isampler2D;
	precision ${s.precision} isampler3D;
	precision ${s.precision} isamplerCube;
	precision ${s.precision} isampler2DArray;
	precision ${s.precision} usampler2D;
	precision ${s.precision} usampler3D;
	precision ${s.precision} usamplerCube;
	precision ${s.precision} usampler2DArray;
	`;return s.precision==="highp"?e+=`
#define HIGH_PRECISION`:s.precision==="mediump"?e+=`
#define MEDIUM_PRECISION`:s.precision==="lowp"&&(e+=`
#define LOW_PRECISION`),e}function Kx(s){let e="SHADOWMAP_TYPE_BASIC";return s.shadowMapType===ih?e="SHADOWMAP_TYPE_PCF":s.shadowMapType===vf?e="SHADOWMAP_TYPE_PCF_SOFT":s.shadowMapType===Jn&&(e="SHADOWMAP_TYPE_VSM"),e}function Jx(s){let e="ENVMAP_TYPE_CUBE";if(s.envMap)switch(s.envMapMode){case Mi:case Hi:e="ENVMAP_TYPE_CUBE";break;case us:e="ENVMAP_TYPE_CUBE_UV";break}return e}function $x(s){let e="ENVMAP_MODE_REFLECTION";if(s.envMap)switch(s.envMapMode){case Hi:e="ENVMAP_MODE_REFRACTION";break}return e}function Qx(s){let e="ENVMAP_BLENDING_NONE";if(s.envMap)switch(s.combine){case vo:e="ENVMAP_BLENDING_MULTIPLY";break;case Gf:e="ENVMAP_BLENDING_MIX";break;case Wf:e="ENVMAP_BLENDING_ADD";break}return e}function eM(s){const e=s.envMapCubeUVHeight;if(e===null)return null;const t=Math.log2(e)-2,n=1/e;return{texelWidth:1/(3*Math.max(Math.pow(2,t),7*16)),texelHeight:n,maxMip:t}}function tM(s,e,t,n){const i=s.getContext(),r=t.defines;let o=t.vertexShader,a=t.fragmentShader;const l=Kx(t),u=Jx(t),d=$x(t),m=Qx(t),p=eM(t),g=Hx(t),_=Gx(r),M=i.createProgram();let y,v,w=t.glslVersion?"#version "+t.glslVersion+`
`:"";t.isRawShaderMaterial?(y=["#define SHADER_TYPE "+t.shaderType,"#define SHADER_NAME "+t.shaderName,_].filter(ks).join(`
`),y.length>0&&(y+=`
`),v=["#define SHADER_TYPE "+t.shaderType,"#define SHADER_NAME "+t.shaderName,_].filter(ks).join(`
`),v.length>0&&(v+=`
`)):(y=[sd(t),"#define SHADER_TYPE "+t.shaderType,"#define SHADER_NAME "+t.shaderName,_,t.extensionClipCullDistance?"#define USE_CLIP_DISTANCE":"",t.batching?"#define USE_BATCHING":"",t.instancing?"#define USE_INSTANCING":"",t.instancingColor?"#define USE_INSTANCING_COLOR":"",t.instancingMorph?"#define USE_INSTANCING_MORPH":"",t.useFog&&t.fog?"#define USE_FOG":"",t.useFog&&t.fogExp2?"#define FOG_EXP2":"",t.map?"#define USE_MAP":"",t.envMap?"#define USE_ENVMAP":"",t.envMap?"#define "+d:"",t.lightMap?"#define USE_LIGHTMAP":"",t.aoMap?"#define USE_AOMAP":"",t.bumpMap?"#define USE_BUMPMAP":"",t.normalMap?"#define USE_NORMALMAP":"",t.normalMapObjectSpace?"#define USE_NORMALMAP_OBJECTSPACE":"",t.normalMapTangentSpace?"#define USE_NORMALMAP_TANGENTSPACE":"",t.displacementMap?"#define USE_DISPLACEMENTMAP":"",t.emissiveMap?"#define USE_EMISSIVEMAP":"",t.anisotropy?"#define USE_ANISOTROPY":"",t.anisotropyMap?"#define USE_ANISOTROPYMAP":"",t.clearcoatMap?"#define USE_CLEARCOATMAP":"",t.clearcoatRoughnessMap?"#define USE_CLEARCOAT_ROUGHNESSMAP":"",t.clearcoatNormalMap?"#define USE_CLEARCOAT_NORMALMAP":"",t.iridescenceMap?"#define USE_IRIDESCENCEMAP":"",t.iridescenceThicknessMap?"#define USE_IRIDESCENCE_THICKNESSMAP":"",t.specularMap?"#define USE_SPECULARMAP":"",t.specularColorMap?"#define USE_SPECULAR_COLORMAP":"",t.specularIntensityMap?"#define USE_SPECULAR_INTENSITYMAP":"",t.roughnessMap?"#define USE_ROUGHNESSMAP":"",t.metalnessMap?"#define USE_METALNESSMAP":"",t.alphaMap?"#define USE_ALPHAMAP":"",t.alphaHash?"#define USE_ALPHAHASH":"",t.transmission?"#define USE_TRANSMISSION":"",t.transmissionMap?"#define USE_TRANSMISSIONMAP":"",t.thicknessMap?"#define USE_THICKNESSMAP":"",t.sheenColorMap?"#define USE_SHEEN_COLORMAP":"",t.sheenRoughnessMap?"#define USE_SHEEN_ROUGHNESSMAP":"",t.mapUv?"#define MAP_UV "+t.mapUv:"",t.alphaMapUv?"#define ALPHAMAP_UV "+t.alphaMapUv:"",t.lightMapUv?"#define LIGHTMAP_UV "+t.lightMapUv:"",t.aoMapUv?"#define AOMAP_UV "+t.aoMapUv:"",t.emissiveMapUv?"#define EMISSIVEMAP_UV "+t.emissiveMapUv:"",t.bumpMapUv?"#define BUMPMAP_UV "+t.bumpMapUv:"",t.normalMapUv?"#define NORMALMAP_UV "+t.normalMapUv:"",t.displacementMapUv?"#define DISPLACEMENTMAP_UV "+t.displacementMapUv:"",t.metalnessMapUv?"#define METALNESSMAP_UV "+t.metalnessMapUv:"",t.roughnessMapUv?"#define ROUGHNESSMAP_UV "+t.roughnessMapUv:"",t.anisotropyMapUv?"#define ANISOTROPYMAP_UV "+t.anisotropyMapUv:"",t.clearcoatMapUv?"#define CLEARCOATMAP_UV "+t.clearcoatMapUv:"",t.clearcoatNormalMapUv?"#define CLEARCOAT_NORMALMAP_UV "+t.clearcoatNormalMapUv:"",t.clearcoatRoughnessMapUv?"#define CLEARCOAT_ROUGHNESSMAP_UV "+t.clearcoatRoughnessMapUv:"",t.iridescenceMapUv?"#define IRIDESCENCEMAP_UV "+t.iridescenceMapUv:"",t.iridescenceThicknessMapUv?"#define IRIDESCENCE_THICKNESSMAP_UV "+t.iridescenceThicknessMapUv:"",t.sheenColorMapUv?"#define SHEEN_COLORMAP_UV "+t.sheenColorMapUv:"",t.sheenRoughnessMapUv?"#define SHEEN_ROUGHNESSMAP_UV "+t.sheenRoughnessMapUv:"",t.specularMapUv?"#define SPECULARMAP_UV "+t.specularMapUv:"",t.specularColorMapUv?"#define SPECULAR_COLORMAP_UV "+t.specularColorMapUv:"",t.specularIntensityMapUv?"#define SPECULAR_INTENSITYMAP_UV "+t.specularIntensityMapUv:"",t.transmissionMapUv?"#define TRANSMISSIONMAP_UV "+t.transmissionMapUv:"",t.thicknessMapUv?"#define THICKNESSMAP_UV "+t.thicknessMapUv:"",t.vertexTangents&&t.flatShading===!1?"#define USE_TANGENT":"",t.vertexColors?"#define USE_COLOR":"",t.vertexAlphas?"#define USE_COLOR_ALPHA":"",t.vertexUv1s?"#define USE_UV1":"",t.vertexUv2s?"#define USE_UV2":"",t.vertexUv3s?"#define USE_UV3":"",t.pointsUvs?"#define USE_POINTS_UV":"",t.flatShading?"#define FLAT_SHADED":"",t.skinning?"#define USE_SKINNING":"",t.morphTargets?"#define USE_MORPHTARGETS":"",t.morphNormals&&t.flatShading===!1?"#define USE_MORPHNORMALS":"",t.morphColors?"#define USE_MORPHCOLORS":"",t.morphTargetsCount>0?"#define MORPHTARGETS_TEXTURE":"",t.morphTargetsCount>0?"#define MORPHTARGETS_TEXTURE_STRIDE "+t.morphTextureStride:"",t.morphTargetsCount>0?"#define MORPHTARGETS_COUNT "+t.morphTargetsCount:"",t.doubleSided?"#define DOUBLE_SIDED":"",t.flipSided?"#define FLIP_SIDED":"",t.shadowMapEnabled?"#define USE_SHADOWMAP":"",t.shadowMapEnabled?"#define "+l:"",t.sizeAttenuation?"#define USE_SIZEATTENUATION":"",t.numLightProbes>0?"#define USE_LIGHT_PROBES":"",t.useLegacyLights?"#define LEGACY_LIGHTS":"",t.logarithmicDepthBuffer?"#define USE_LOGDEPTHBUF":"","uniform mat4 modelMatrix;","uniform mat4 modelViewMatrix;","uniform mat4 projectionMatrix;","uniform mat4 viewMatrix;","uniform mat3 normalMatrix;","uniform vec3 cameraPosition;","uniform bool isOrthographic;","#ifdef USE_INSTANCING","	attribute mat4 instanceMatrix;","#endif","#ifdef USE_INSTANCING_COLOR","	attribute vec3 instanceColor;","#endif","#ifdef USE_INSTANCING_MORPH","	uniform sampler2D morphTexture;","#endif","attribute vec3 position;","attribute vec3 normal;","attribute vec2 uv;","#ifdef USE_UV1","	attribute vec2 uv1;","#endif","#ifdef USE_UV2","	attribute vec2 uv2;","#endif","#ifdef USE_UV3","	attribute vec2 uv3;","#endif","#ifdef USE_TANGENT","	attribute vec4 tangent;","#endif","#if defined( USE_COLOR_ALPHA )","	attribute vec4 color;","#elif defined( USE_COLOR )","	attribute vec3 color;","#endif","#if ( defined( USE_MORPHTARGETS ) && ! defined( MORPHTARGETS_TEXTURE ) )","	attribute vec3 morphTarget0;","	attribute vec3 morphTarget1;","	attribute vec3 morphTarget2;","	attribute vec3 morphTarget3;","	#ifdef USE_MORPHNORMALS","		attribute vec3 morphNormal0;","		attribute vec3 morphNormal1;","		attribute vec3 morphNormal2;","		attribute vec3 morphNormal3;","	#else","		attribute vec3 morphTarget4;","		attribute vec3 morphTarget5;","		attribute vec3 morphTarget6;","		attribute vec3 morphTarget7;","	#endif","#endif","#ifdef USE_SKINNING","	attribute vec4 skinIndex;","	attribute vec4 skinWeight;","#endif",`
`].filter(ks).join(`
`),v=[sd(t),"#define SHADER_TYPE "+t.shaderType,"#define SHADER_NAME "+t.shaderName,_,t.useFog&&t.fog?"#define USE_FOG":"",t.useFog&&t.fogExp2?"#define FOG_EXP2":"",t.alphaToCoverage?"#define ALPHA_TO_COVERAGE":"",t.map?"#define USE_MAP":"",t.matcap?"#define USE_MATCAP":"",t.envMap?"#define USE_ENVMAP":"",t.envMap?"#define "+u:"",t.envMap?"#define "+d:"",t.envMap?"#define "+m:"",p?"#define CUBEUV_TEXEL_WIDTH "+p.texelWidth:"",p?"#define CUBEUV_TEXEL_HEIGHT "+p.texelHeight:"",p?"#define CUBEUV_MAX_MIP "+p.maxMip+".0":"",t.lightMap?"#define USE_LIGHTMAP":"",t.aoMap?"#define USE_AOMAP":"",t.bumpMap?"#define USE_BUMPMAP":"",t.normalMap?"#define USE_NORMALMAP":"",t.normalMapObjectSpace?"#define USE_NORMALMAP_OBJECTSPACE":"",t.normalMapTangentSpace?"#define USE_NORMALMAP_TANGENTSPACE":"",t.emissiveMap?"#define USE_EMISSIVEMAP":"",t.anisotropy?"#define USE_ANISOTROPY":"",t.anisotropyMap?"#define USE_ANISOTROPYMAP":"",t.clearcoat?"#define USE_CLEARCOAT":"",t.clearcoatMap?"#define USE_CLEARCOATMAP":"",t.clearcoatRoughnessMap?"#define USE_CLEARCOAT_ROUGHNESSMAP":"",t.clearcoatNormalMap?"#define USE_CLEARCOAT_NORMALMAP":"",t.dispersion?"#define USE_DISPERSION":"",t.iridescence?"#define USE_IRIDESCENCE":"",t.iridescenceMap?"#define USE_IRIDESCENCEMAP":"",t.iridescenceThicknessMap?"#define USE_IRIDESCENCE_THICKNESSMAP":"",t.specularMap?"#define USE_SPECULARMAP":"",t.specularColorMap?"#define USE_SPECULAR_COLORMAP":"",t.specularIntensityMap?"#define USE_SPECULAR_INTENSITYMAP":"",t.roughnessMap?"#define USE_ROUGHNESSMAP":"",t.metalnessMap?"#define USE_METALNESSMAP":"",t.alphaMap?"#define USE_ALPHAMAP":"",t.alphaTest?"#define USE_ALPHATEST":"",t.alphaHash?"#define USE_ALPHAHASH":"",t.sheen?"#define USE_SHEEN":"",t.sheenColorMap?"#define USE_SHEEN_COLORMAP":"",t.sheenRoughnessMap?"#define USE_SHEEN_ROUGHNESSMAP":"",t.transmission?"#define USE_TRANSMISSION":"",t.transmissionMap?"#define USE_TRANSMISSIONMAP":"",t.thicknessMap?"#define USE_THICKNESSMAP":"",t.vertexTangents&&t.flatShading===!1?"#define USE_TANGENT":"",t.vertexColors||t.instancingColor?"#define USE_COLOR":"",t.vertexAlphas?"#define USE_COLOR_ALPHA":"",t.vertexUv1s?"#define USE_UV1":"",t.vertexUv2s?"#define USE_UV2":"",t.vertexUv3s?"#define USE_UV3":"",t.pointsUvs?"#define USE_POINTS_UV":"",t.gradientMap?"#define USE_GRADIENTMAP":"",t.flatShading?"#define FLAT_SHADED":"",t.doubleSided?"#define DOUBLE_SIDED":"",t.flipSided?"#define FLIP_SIDED":"",t.shadowMapEnabled?"#define USE_SHADOWMAP":"",t.shadowMapEnabled?"#define "+l:"",t.premultipliedAlpha?"#define PREMULTIPLIED_ALPHA":"",t.numLightProbes>0?"#define USE_LIGHT_PROBES":"",t.useLegacyLights?"#define LEGACY_LIGHTS":"",t.decodeVideoTexture?"#define DECODE_VIDEO_TEXTURE":"",t.logarithmicDepthBuffer?"#define USE_LOGDEPTHBUF":"","uniform mat4 viewMatrix;","uniform vec3 cameraPosition;","uniform bool isOrthographic;",t.toneMapping!==yi?"#define TONE_MAPPING":"",t.toneMapping!==yi?mt.tonemapping_pars_fragment:"",t.toneMapping!==yi?Vx("toneMapping",t.toneMapping):"",t.dithering?"#define DITHERING":"",t.opaque?"#define OPAQUE":"",mt.colorspace_pars_fragment,kx("linearToOutputTexel",t.outputColorSpace),t.useDepthPacking?"#define DEPTH_PACKING "+t.depthPacking:"",`
`].filter(ks).join(`
`)),o=jc(o),o=nd(o,t),o=id(o,t),a=jc(a),a=nd(a,t),a=id(a,t),o=rd(o),a=rd(a),t.isRawShaderMaterial!==!0&&(w=`#version 300 es
`,y=[g,"#define attribute in","#define varying out","#define texture2D texture"].join(`
`)+`
`+y,v=["#define varying in",t.glslVersion===qc?"":"layout(location = 0) out highp vec4 pc_fragColor;",t.glslVersion===qc?"":"#define gl_FragColor pc_fragColor","#define gl_FragDepthEXT gl_FragDepth","#define texture2D texture","#define textureCube texture","#define texture2DProj textureProj","#define texture2DLodEXT textureLod","#define texture2DProjLodEXT textureProjLod","#define textureCubeLodEXT textureLod","#define texture2DGradEXT textureGrad","#define texture2DProjGradEXT textureProjGrad","#define textureCubeGradEXT textureGrad"].join(`
`)+`
`+v);const S=w+y+o,T=w+v+a,z=ed(i,i.VERTEX_SHADER,S),N=ed(i,i.FRAGMENT_SHADER,T);i.attachShader(M,z),i.attachShader(M,N),t.index0AttributeName!==void 0?i.bindAttribLocation(M,0,t.index0AttributeName):t.morphTargets===!0&&i.bindAttribLocation(M,0,"position"),i.linkProgram(M);function A(X){if(s.debug.checkShaderErrors){const Y=i.getProgramInfoLog(M).trim(),H=i.getShaderInfoLog(z).trim(),K=i.getShaderInfoLog(N).trim();let J=!0,ce=!0;if(i.getProgramParameter(M,i.LINK_STATUS)===!1)if(J=!1,typeof s.debug.onShaderError=="function")s.debug.onShaderError(i,M,z,N);else{const de=td(i,z,"vertex"),Q=td(i,N,"fragment");console.error("THREE.WebGLProgram: Shader Error "+i.getError()+" - VALIDATE_STATUS "+i.getProgramParameter(M,i.VALIDATE_STATUS)+`

Material Name: `+X.name+`
Material Type: `+X.type+`

Program Info Log: `+Y+`
`+de+`
`+Q)}else Y!==""?console.warn("THREE.WebGLProgram: Program Info Log:",Y):(H===""||K==="")&&(ce=!1);ce&&(X.diagnostics={runnable:J,programLog:Y,vertexShader:{log:H,prefix:y},fragmentShader:{log:K,prefix:v}})}i.deleteShader(z),i.deleteShader(N),k=new Ua(i,M),D=Wx(i,M)}let k;this.getUniforms=function(){return k===void 0&&A(this),k};let D;this.getAttributes=function(){return D===void 0&&A(this),D};let C=t.rendererExtensionParallelShaderCompile===!1;return this.isReady=function(){return C===!1&&(C=i.getProgramParameter(M,Ux)),C},this.destroy=function(){n.releaseStatesOfProgram(this),i.deleteProgram(M),this.program=void 0},this.type=t.shaderType,this.name=t.shaderName,this.id=Fx++,this.cacheKey=e,this.usedTimes=1,this.program=M,this.vertexShader=z,this.fragmentShader=N,this}let nM=0;class iM{constructor(){this.shaderCache=new Map,this.materialCache=new Map}update(e){const t=e.vertexShader,n=e.fragmentShader,i=this._getShaderStage(t),r=this._getShaderStage(n),o=this._getShaderCacheForMaterial(e);return o.has(i)===!1&&(o.add(i),i.usedTimes++),o.has(r)===!1&&(o.add(r),r.usedTimes++),this}remove(e){const t=this.materialCache.get(e);for(const n of t)n.usedTimes--,n.usedTimes===0&&this.shaderCache.delete(n.code);return this.materialCache.delete(e),this}getVertexShaderID(e){return this._getShaderStage(e.vertexShader).id}getFragmentShaderID(e){return this._getShaderStage(e.fragmentShader).id}dispose(){this.shaderCache.clear(),this.materialCache.clear()}_getShaderCacheForMaterial(e){const t=this.materialCache;let n=t.get(e);return n===void 0&&(n=new Set,t.set(e,n)),n}_getShaderStage(e){const t=this.shaderCache;let n=t.get(e);return n===void 0&&(n=new rM(e),t.set(e,n)),n}}class rM{constructor(e){this.id=nM++,this.code=e,this.usedTimes=0}}function sM(s,e,t,n,i,r,o){const a=new Ka,l=new iM,u=new Set,d=[],m=i.logarithmicDepthBuffer,p=i.vertexTextures;let g=i.precision;const _={MeshDepthMaterial:"depth",MeshDistanceMaterial:"distanceRGBA",MeshNormalMaterial:"normal",MeshBasicMaterial:"basic",MeshLambertMaterial:"lambert",MeshPhongMaterial:"phong",MeshToonMaterial:"toon",MeshStandardMaterial:"physical",MeshPhysicalMaterial:"physical",MeshMatcapMaterial:"matcap",LineBasicMaterial:"basic",LineDashedMaterial:"dashed",PointsMaterial:"points",ShadowMaterial:"shadow",SpriteMaterial:"sprite"};function M(D){return u.add(D),D===0?"uv":`uv${D}`}function y(D,C,X,Y,H){const K=Y.fog,J=H.geometry,ce=D.isMeshStandardMaterial?Y.environment:null,de=(D.isMeshStandardMaterial?t:e).get(D.envMap||ce),Q=de&&de.mapping===us?de.image.height:null,pe=_[D.type];D.precision!==null&&(g=i.getMaxPrecision(D.precision),g!==D.precision&&console.warn("THREE.WebGLProgram.getParameters:",D.precision,"not supported, using",g,"instead."));const ve=J.morphAttributes.position||J.morphAttributes.normal||J.morphAttributes.color,Ue=ve!==void 0?ve.length:0;let Je=0;J.morphAttributes.position!==void 0&&(Je=1),J.morphAttributes.normal!==void 0&&(Je=2),J.morphAttributes.color!==void 0&&(Je=3);let vt,ae,we,ke;if(pe){const ct=Xn[pe];vt=ct.vertexShader,ae=ct.fragmentShader}else vt=D.vertexShader,ae=D.fragmentShader,l.update(D),we=l.getVertexShaderID(D),ke=l.getFragmentShaderID(D);const Ee=s.getRenderTarget(),nt=H.isInstancedMesh===!0,be=H.isBatchedMesh===!0,Z=!!D.map,st=!!D.matcap,le=!!de,xe=!!D.aoMap,he=!!D.lightMap,Ae=!!D.bumpMap,ge=!!D.normalMap,Fe=!!D.displacementMap,Ge=!!D.emissiveMap,B=!!D.metalnessMap,I=!!D.roughnessMap,$=D.anisotropy>0,ue=D.clearcoat>0,_e=D.dispersion>0,me=D.iridescence>0,Ye=D.sheen>0,Re=D.transmission>0,Le=$&&!!D.anisotropyMap,tt=ue&&!!D.clearcoatMap,Ce=ue&&!!D.clearcoatNormalMap,Xe=ue&&!!D.clearcoatRoughnessMap,lt=me&&!!D.iridescenceMap,je=me&&!!D.iridescenceThicknessMap,ze=Ye&&!!D.sheenColorMap,Qe=Ye&&!!D.sheenRoughnessMap,ft=!!D.specularMap,V=!!D.specularColorMap,fe=!!D.specularIntensityMap,G=Re&&!!D.transmissionMap,re=Re&&!!D.thicknessMap,ne=!!D.gradientMap,Te=!!D.alphaMap,Pe=D.alphaTest>0,_t=!!D.alphaHash,St=!!D.extensions;let Et=yi;D.toneMapped&&(Ee===null||Ee.isXRRenderTarget===!0)&&(Et=s.toneMapping);const Tt={shaderID:pe,shaderType:D.type,shaderName:D.name,vertexShader:vt,fragmentShader:ae,defines:D.defines,customVertexShaderID:we,customFragmentShaderID:ke,isRawShaderMaterial:D.isRawShaderMaterial===!0,glslVersion:D.glslVersion,precision:g,batching:be,instancing:nt,instancingColor:nt&&H.instanceColor!==null,instancingMorph:nt&&H.morphTexture!==null,supportsVertexTextures:p,outputColorSpace:Ee===null?s.outputColorSpace:Ee.isXRRenderTarget===!0?Ee.texture.colorSpace:wi,alphaToCoverage:!!D.alphaToCoverage,map:Z,matcap:st,envMap:le,envMapMode:le&&de.mapping,envMapCubeUVHeight:Q,aoMap:xe,lightMap:he,bumpMap:Ae,normalMap:ge,displacementMap:p&&Fe,emissiveMap:Ge,normalMapObjectSpace:ge&&D.normalMapType===op,normalMapTangentSpace:ge&&D.normalMapType===Gi,metalnessMap:B,roughnessMap:I,anisotropy:$,anisotropyMap:Le,clearcoat:ue,clearcoatMap:tt,clearcoatNormalMap:Ce,clearcoatRoughnessMap:Xe,dispersion:_e,iridescence:me,iridescenceMap:lt,iridescenceThicknessMap:je,sheen:Ye,sheenColorMap:ze,sheenRoughnessMap:Qe,specularMap:ft,specularColorMap:V,specularIntensityMap:fe,transmission:Re,transmissionMap:G,thicknessMap:re,gradientMap:ne,opaque:D.transparent===!1&&D.blending===xr&&D.alphaToCoverage===!1,alphaMap:Te,alphaTest:Pe,alphaHash:_t,combine:D.combine,mapUv:Z&&M(D.map.channel),aoMapUv:xe&&M(D.aoMap.channel),lightMapUv:he&&M(D.lightMap.channel),bumpMapUv:Ae&&M(D.bumpMap.channel),normalMapUv:ge&&M(D.normalMap.channel),displacementMapUv:Fe&&M(D.displacementMap.channel),emissiveMapUv:Ge&&M(D.emissiveMap.channel),metalnessMapUv:B&&M(D.metalnessMap.channel),roughnessMapUv:I&&M(D.roughnessMap.channel),anisotropyMapUv:Le&&M(D.anisotropyMap.channel),clearcoatMapUv:tt&&M(D.clearcoatMap.channel),clearcoatNormalMapUv:Ce&&M(D.clearcoatNormalMap.channel),clearcoatRoughnessMapUv:Xe&&M(D.clearcoatRoughnessMap.channel),iridescenceMapUv:lt&&M(D.iridescenceMap.channel),iridescenceThicknessMapUv:je&&M(D.iridescenceThicknessMap.channel),sheenColorMapUv:ze&&M(D.sheenColorMap.channel),sheenRoughnessMapUv:Qe&&M(D.sheenRoughnessMap.channel),specularMapUv:ft&&M(D.specularMap.channel),specularColorMapUv:V&&M(D.specularColorMap.channel),specularIntensityMapUv:fe&&M(D.specularIntensityMap.channel),transmissionMapUv:G&&M(D.transmissionMap.channel),thicknessMapUv:re&&M(D.thicknessMap.channel),alphaMapUv:Te&&M(D.alphaMap.channel),vertexTangents:!!J.attributes.tangent&&(ge||$),vertexColors:D.vertexColors,vertexAlphas:D.vertexColors===!0&&!!J.attributes.color&&J.attributes.color.itemSize===4,pointsUvs:H.isPoints===!0&&!!J.attributes.uv&&(Z||Te),fog:!!K,useFog:D.fog===!0,fogExp2:!!K&&K.isFogExp2,flatShading:D.flatShading===!0,sizeAttenuation:D.sizeAttenuation===!0,logarithmicDepthBuffer:m,skinning:H.isSkinnedMesh===!0,morphTargets:J.morphAttributes.position!==void 0,morphNormals:J.morphAttributes.normal!==void 0,morphColors:J.morphAttributes.color!==void 0,morphTargetsCount:Ue,morphTextureStride:Je,numDirLights:C.directional.length,numPointLights:C.point.length,numSpotLights:C.spot.length,numSpotLightMaps:C.spotLightMap.length,numRectAreaLights:C.rectArea.length,numHemiLights:C.hemi.length,numDirLightShadows:C.directionalShadowMap.length,numPointLightShadows:C.pointShadowMap.length,numSpotLightShadows:C.spotShadowMap.length,numSpotLightShadowsWithMaps:C.numSpotLightShadowsWithMaps,numLightProbes:C.numLightProbes,numClippingPlanes:o.numPlanes,numClipIntersection:o.numIntersection,dithering:D.dithering,shadowMapEnabled:s.shadowMap.enabled&&X.length>0,shadowMapType:s.shadowMap.type,toneMapping:Et,useLegacyLights:s._useLegacyLights,decodeVideoTexture:Z&&D.map.isVideoTexture===!0&&wt.getTransfer(D.map.colorSpace)===It,premultipliedAlpha:D.premultipliedAlpha,doubleSided:D.side===Tn,flipSided:D.side===_n,useDepthPacking:D.depthPacking>=0,depthPacking:D.depthPacking||0,index0AttributeName:D.index0AttributeName,extensionClipCullDistance:St&&D.extensions.clipCullDistance===!0&&n.has("WEBGL_clip_cull_distance"),extensionMultiDraw:St&&D.extensions.multiDraw===!0&&n.has("WEBGL_multi_draw"),rendererExtensionParallelShaderCompile:n.has("KHR_parallel_shader_compile"),customProgramCacheKey:D.customProgramCacheKey()};return Tt.vertexUv1s=u.has(1),Tt.vertexUv2s=u.has(2),Tt.vertexUv3s=u.has(3),u.clear(),Tt}function v(D){const C=[];if(D.shaderID?C.push(D.shaderID):(C.push(D.customVertexShaderID),C.push(D.customFragmentShaderID)),D.defines!==void 0)for(const X in D.defines)C.push(X),C.push(D.defines[X]);return D.isRawShaderMaterial===!1&&(w(C,D),S(C,D),C.push(s.outputColorSpace)),C.push(D.customProgramCacheKey),C.join()}function w(D,C){D.push(C.precision),D.push(C.outputColorSpace),D.push(C.envMapMode),D.push(C.envMapCubeUVHeight),D.push(C.mapUv),D.push(C.alphaMapUv),D.push(C.lightMapUv),D.push(C.aoMapUv),D.push(C.bumpMapUv),D.push(C.normalMapUv),D.push(C.displacementMapUv),D.push(C.emissiveMapUv),D.push(C.metalnessMapUv),D.push(C.roughnessMapUv),D.push(C.anisotropyMapUv),D.push(C.clearcoatMapUv),D.push(C.clearcoatNormalMapUv),D.push(C.clearcoatRoughnessMapUv),D.push(C.iridescenceMapUv),D.push(C.iridescenceThicknessMapUv),D.push(C.sheenColorMapUv),D.push(C.sheenRoughnessMapUv),D.push(C.specularMapUv),D.push(C.specularColorMapUv),D.push(C.specularIntensityMapUv),D.push(C.transmissionMapUv),D.push(C.thicknessMapUv),D.push(C.combine),D.push(C.fogExp2),D.push(C.sizeAttenuation),D.push(C.morphTargetsCount),D.push(C.morphAttributeCount),D.push(C.numDirLights),D.push(C.numPointLights),D.push(C.numSpotLights),D.push(C.numSpotLightMaps),D.push(C.numHemiLights),D.push(C.numRectAreaLights),D.push(C.numDirLightShadows),D.push(C.numPointLightShadows),D.push(C.numSpotLightShadows),D.push(C.numSpotLightShadowsWithMaps),D.push(C.numLightProbes),D.push(C.shadowMapType),D.push(C.toneMapping),D.push(C.numClippingPlanes),D.push(C.numClipIntersection),D.push(C.depthPacking)}function S(D,C){a.disableAll(),C.supportsVertexTextures&&a.enable(0),C.instancing&&a.enable(1),C.instancingColor&&a.enable(2),C.instancingMorph&&a.enable(3),C.matcap&&a.enable(4),C.envMap&&a.enable(5),C.normalMapObjectSpace&&a.enable(6),C.normalMapTangentSpace&&a.enable(7),C.clearcoat&&a.enable(8),C.iridescence&&a.enable(9),C.alphaTest&&a.enable(10),C.vertexColors&&a.enable(11),C.vertexAlphas&&a.enable(12),C.vertexUv1s&&a.enable(13),C.vertexUv2s&&a.enable(14),C.vertexUv3s&&a.enable(15),C.vertexTangents&&a.enable(16),C.anisotropy&&a.enable(17),C.alphaHash&&a.enable(18),C.batching&&a.enable(19),C.dispersion&&a.enable(20),D.push(a.mask),a.disableAll(),C.fog&&a.enable(0),C.useFog&&a.enable(1),C.flatShading&&a.enable(2),C.logarithmicDepthBuffer&&a.enable(3),C.skinning&&a.enable(4),C.morphTargets&&a.enable(5),C.morphNormals&&a.enable(6),C.morphColors&&a.enable(7),C.premultipliedAlpha&&a.enable(8),C.shadowMapEnabled&&a.enable(9),C.useLegacyLights&&a.enable(10),C.doubleSided&&a.enable(11),C.flipSided&&a.enable(12),C.useDepthPacking&&a.enable(13),C.dithering&&a.enable(14),C.transmission&&a.enable(15),C.sheen&&a.enable(16),C.opaque&&a.enable(17),C.pointsUvs&&a.enable(18),C.decodeVideoTexture&&a.enable(19),C.alphaToCoverage&&a.enable(20),D.push(a.mask)}function T(D){const C=_[D.type];let X;if(C){const Y=Xn[C];X=yo.clone(Y.uniforms)}else X=D.uniforms;return X}function z(D,C){let X;for(let Y=0,H=d.length;Y<H;Y++){const K=d[Y];if(K.cacheKey===C){X=K,++X.usedTimes;break}}return X===void 0&&(X=new tM(s,C,D,r),d.push(X)),X}function N(D){if(--D.usedTimes===0){const C=d.indexOf(D);d[C]=d[d.length-1],d.pop(),D.destroy()}}function A(D){l.remove(D)}function k(){l.dispose()}return{getParameters:y,getProgramCacheKey:v,getUniforms:T,acquireProgram:z,releaseProgram:N,releaseShaderCache:A,programs:d,dispose:k}}function oM(){let s=new WeakMap;function e(r){let o=s.get(r);return o===void 0&&(o={},s.set(r,o)),o}function t(r){s.delete(r)}function n(r,o,a){s.get(r)[o]=a}function i(){s=new WeakMap}return{get:e,remove:t,update:n,dispose:i}}function aM(s,e){return s.groupOrder!==e.groupOrder?s.groupOrder-e.groupOrder:s.renderOrder!==e.renderOrder?s.renderOrder-e.renderOrder:s.material.id!==e.material.id?s.material.id-e.material.id:s.z!==e.z?s.z-e.z:s.id-e.id}function od(s,e){return s.groupOrder!==e.groupOrder?s.groupOrder-e.groupOrder:s.renderOrder!==e.renderOrder?s.renderOrder-e.renderOrder:s.z!==e.z?e.z-s.z:s.id-e.id}function ad(){const s=[];let e=0;const t=[],n=[],i=[];function r(){e=0,t.length=0,n.length=0,i.length=0}function o(m,p,g,_,M,y){let v=s[e];return v===void 0?(v={id:m.id,object:m,geometry:p,material:g,groupOrder:_,renderOrder:m.renderOrder,z:M,group:y},s[e]=v):(v.id=m.id,v.object=m,v.geometry=p,v.material=g,v.groupOrder=_,v.renderOrder=m.renderOrder,v.z=M,v.group=y),e++,v}function a(m,p,g,_,M,y){const v=o(m,p,g,_,M,y);g.transmission>0?n.push(v):g.transparent===!0?i.push(v):t.push(v)}function l(m,p,g,_,M,y){const v=o(m,p,g,_,M,y);g.transmission>0?n.unshift(v):g.transparent===!0?i.unshift(v):t.unshift(v)}function u(m,p){t.length>1&&t.sort(m||aM),n.length>1&&n.sort(p||od),i.length>1&&i.sort(p||od)}function d(){for(let m=e,p=s.length;m<p;m++){const g=s[m];if(g.id===null)break;g.id=null,g.object=null,g.geometry=null,g.material=null,g.group=null}}return{opaque:t,transmissive:n,transparent:i,init:r,push:a,unshift:l,finish:d,sort:u}}function lM(){let s=new WeakMap;function e(n,i){const r=s.get(n);let o;return r===void 0?(o=new ad,s.set(n,[o])):i>=r.length?(o=new ad,r.push(o)):o=r[i],o}function t(){s=new WeakMap}return{get:e,dispose:t}}function cM(){const s={};return{get:function(e){if(s[e.id]!==void 0)return s[e.id];let t;switch(e.type){case"DirectionalLight":t={direction:new O,color:new Oe};break;case"SpotLight":t={position:new O,direction:new O,color:new Oe,distance:0,coneCos:0,penumbraCos:0,decay:0};break;case"PointLight":t={position:new O,color:new Oe,distance:0,decay:0};break;case"HemisphereLight":t={direction:new O,skyColor:new Oe,groundColor:new Oe};break;case"RectAreaLight":t={color:new Oe,position:new O,halfWidth:new O,halfHeight:new O};break}return s[e.id]=t,t}}}function hM(){const s={};return{get:function(e){if(s[e.id]!==void 0)return s[e.id];let t;switch(e.type){case"DirectionalLight":t={shadowBias:0,shadowNormalBias:0,shadowRadius:1,shadowMapSize:new se};break;case"SpotLight":t={shadowBias:0,shadowNormalBias:0,shadowRadius:1,shadowMapSize:new se};break;case"PointLight":t={shadowBias:0,shadowNormalBias:0,shadowRadius:1,shadowMapSize:new se,shadowCameraNear:1,shadowCameraFar:1e3};break}return s[e.id]=t,t}}}let uM=0;function dM(s,e){return(e.castShadow?2:0)-(s.castShadow?2:0)+(e.map?1:0)-(s.map?1:0)}function fM(s){const e=new cM,t=hM(),n={version:0,hash:{directionalLength:-1,pointLength:-1,spotLength:-1,rectAreaLength:-1,hemiLength:-1,numDirectionalShadows:-1,numPointShadows:-1,numSpotShadows:-1,numSpotMaps:-1,numLightProbes:-1},ambient:[0,0,0],probe:[],directional:[],directionalShadow:[],directionalShadowMap:[],directionalShadowMatrix:[],spot:[],spotLightMap:[],spotShadow:[],spotShadowMap:[],spotLightMatrix:[],rectArea:[],rectAreaLTC1:null,rectAreaLTC2:null,point:[],pointShadow:[],pointShadowMap:[],pointShadowMatrix:[],hemi:[],numSpotLightShadowsWithMaps:0,numLightProbes:0};for(let u=0;u<9;u++)n.probe.push(new O);const i=new O,r=new Ke,o=new Ke;function a(u,d){let m=0,p=0,g=0;for(let X=0;X<9;X++)n.probe[X].set(0,0,0);let _=0,M=0,y=0,v=0,w=0,S=0,T=0,z=0,N=0,A=0,k=0;u.sort(dM);const D=d===!0?Math.PI:1;for(let X=0,Y=u.length;X<Y;X++){const H=u[X],K=H.color,J=H.intensity,ce=H.distance,de=H.shadow&&H.shadow.map?H.shadow.map.texture:null;if(H.isAmbientLight)m+=K.r*J*D,p+=K.g*J*D,g+=K.b*J*D;else if(H.isLightProbe){for(let Q=0;Q<9;Q++)n.probe[Q].addScaledVector(H.sh.coefficients[Q],J);k++}else if(H.isDirectionalLight){const Q=e.get(H);if(Q.color.copy(H.color).multiplyScalar(H.intensity*D),H.castShadow){const pe=H.shadow,ve=t.get(H);ve.shadowBias=pe.bias,ve.shadowNormalBias=pe.normalBias,ve.shadowRadius=pe.radius,ve.shadowMapSize=pe.mapSize,n.directionalShadow[_]=ve,n.directionalShadowMap[_]=de,n.directionalShadowMatrix[_]=H.shadow.matrix,S++}n.directional[_]=Q,_++}else if(H.isSpotLight){const Q=e.get(H);Q.position.setFromMatrixPosition(H.matrixWorld),Q.color.copy(K).multiplyScalar(J*D),Q.distance=ce,Q.coneCos=Math.cos(H.angle),Q.penumbraCos=Math.cos(H.angle*(1-H.penumbra)),Q.decay=H.decay,n.spot[y]=Q;const pe=H.shadow;if(H.map&&(n.spotLightMap[N]=H.map,N++,pe.updateMatrices(H),H.castShadow&&A++),n.spotLightMatrix[y]=pe.matrix,H.castShadow){const ve=t.get(H);ve.shadowBias=pe.bias,ve.shadowNormalBias=pe.normalBias,ve.shadowRadius=pe.radius,ve.shadowMapSize=pe.mapSize,n.spotShadow[y]=ve,n.spotShadowMap[y]=de,z++}y++}else if(H.isRectAreaLight){const Q=e.get(H);Q.color.copy(K).multiplyScalar(J),Q.halfWidth.set(H.width*.5,0,0),Q.halfHeight.set(0,H.height*.5,0),n.rectArea[v]=Q,v++}else if(H.isPointLight){const Q=e.get(H);if(Q.color.copy(H.color).multiplyScalar(H.intensity*D),Q.distance=H.distance,Q.decay=H.decay,H.castShadow){const pe=H.shadow,ve=t.get(H);ve.shadowBias=pe.bias,ve.shadowNormalBias=pe.normalBias,ve.shadowRadius=pe.radius,ve.shadowMapSize=pe.mapSize,ve.shadowCameraNear=pe.camera.near,ve.shadowCameraFar=pe.camera.far,n.pointShadow[M]=ve,n.pointShadowMap[M]=de,n.pointShadowMatrix[M]=H.shadow.matrix,T++}n.point[M]=Q,M++}else if(H.isHemisphereLight){const Q=e.get(H);Q.skyColor.copy(H.color).multiplyScalar(J*D),Q.groundColor.copy(H.groundColor).multiplyScalar(J*D),n.hemi[w]=Q,w++}}v>0&&(s.has("OES_texture_float_linear")===!0?(n.rectAreaLTC1=Ne.LTC_FLOAT_1,n.rectAreaLTC2=Ne.LTC_FLOAT_2):(n.rectAreaLTC1=Ne.LTC_HALF_1,n.rectAreaLTC2=Ne.LTC_HALF_2)),n.ambient[0]=m,n.ambient[1]=p,n.ambient[2]=g;const C=n.hash;(C.directionalLength!==_||C.pointLength!==M||C.spotLength!==y||C.rectAreaLength!==v||C.hemiLength!==w||C.numDirectionalShadows!==S||C.numPointShadows!==T||C.numSpotShadows!==z||C.numSpotMaps!==N||C.numLightProbes!==k)&&(n.directional.length=_,n.spot.length=y,n.rectArea.length=v,n.point.length=M,n.hemi.length=w,n.directionalShadow.length=S,n.directionalShadowMap.length=S,n.pointShadow.length=T,n.pointShadowMap.length=T,n.spotShadow.length=z,n.spotShadowMap.length=z,n.directionalShadowMatrix.length=S,n.pointShadowMatrix.length=T,n.spotLightMatrix.length=z+N-A,n.spotLightMap.length=N,n.numSpotLightShadowsWithMaps=A,n.numLightProbes=k,C.directionalLength=_,C.pointLength=M,C.spotLength=y,C.rectAreaLength=v,C.hemiLength=w,C.numDirectionalShadows=S,C.numPointShadows=T,C.numSpotShadows=z,C.numSpotMaps=N,C.numLightProbes=k,n.version=uM++)}function l(u,d){let m=0,p=0,g=0,_=0,M=0;const y=d.matrixWorldInverse;for(let v=0,w=u.length;v<w;v++){const S=u[v];if(S.isDirectionalLight){const T=n.directional[m];T.direction.setFromMatrixPosition(S.matrixWorld),i.setFromMatrixPosition(S.target.matrixWorld),T.direction.sub(i),T.direction.transformDirection(y),m++}else if(S.isSpotLight){const T=n.spot[g];T.position.setFromMatrixPosition(S.matrixWorld),T.position.applyMatrix4(y),T.direction.setFromMatrixPosition(S.matrixWorld),i.setFromMatrixPosition(S.target.matrixWorld),T.direction.sub(i),T.direction.transformDirection(y),g++}else if(S.isRectAreaLight){const T=n.rectArea[_];T.position.setFromMatrixPosition(S.matrixWorld),T.position.applyMatrix4(y),o.identity(),r.copy(S.matrixWorld),r.premultiply(y),o.extractRotation(r),T.halfWidth.set(S.width*.5,0,0),T.halfHeight.set(0,S.height*.5,0),T.halfWidth.applyMatrix4(o),T.halfHeight.applyMatrix4(o),_++}else if(S.isPointLight){const T=n.point[p];T.position.setFromMatrixPosition(S.matrixWorld),T.position.applyMatrix4(y),p++}else if(S.isHemisphereLight){const T=n.hemi[M];T.direction.setFromMatrixPosition(S.matrixWorld),T.direction.transformDirection(y),M++}}}return{setup:a,setupView:l,state:n}}function ld(s){const e=new fM(s),t=[],n=[];function i(d){u.camera=d,t.length=0,n.length=0}function r(d){t.push(d)}function o(d){n.push(d)}function a(d){e.setup(t,d)}function l(d){e.setupView(t,d)}const u={lightsArray:t,shadowsArray:n,camera:null,lights:e,transmissionRenderTarget:{}};return{init:i,state:u,setupLights:a,setupLightsView:l,pushLight:r,pushShadow:o}}function pM(s){let e=new WeakMap;function t(i,r=0){const o=e.get(i);let a;return o===void 0?(a=new ld(s),e.set(i,[a])):r>=o.length?(a=new ld(s),o.push(a)):a=o[r],a}function n(){e=new WeakMap}return{get:t,dispose:n}}class Qa extends hn{constructor(e){super(),this.isMeshDepthMaterial=!0,this.type="MeshDepthMaterial",this.depthPacking=sp,this.map=null,this.alphaMap=null,this.displacementMap=null,this.displacementScale=1,this.displacementBias=0,this.wireframe=!1,this.wireframeLinewidth=1,this.setValues(e)}copy(e){return super.copy(e),this.depthPacking=e.depthPacking,this.map=e.map,this.alphaMap=e.alphaMap,this.displacementMap=e.displacementMap,this.displacementScale=e.displacementScale,this.displacementBias=e.displacementBias,this.wireframe=e.wireframe,this.wireframeLinewidth=e.wireframeLinewidth,this}}class Ch extends hn{constructor(e){super(),this.isMeshDistanceMaterial=!0,this.type="MeshDistanceMaterial",this.map=null,this.alphaMap=null,this.displacementMap=null,this.displacementScale=1,this.displacementBias=0,this.setValues(e)}copy(e){return super.copy(e),this.map=e.map,this.alphaMap=e.alphaMap,this.displacementMap=e.displacementMap,this.displacementScale=e.displacementScale,this.displacementBias=e.displacementBias,this}}const mM=`void main() {
	gl_Position = vec4( position, 1.0 );
}`,gM=`uniform sampler2D shadow_pass;
uniform vec2 resolution;
uniform float radius;
#include <packing>
void main() {
	const float samples = float( VSM_SAMPLES );
	float mean = 0.0;
	float squared_mean = 0.0;
	float uvStride = samples <= 1.0 ? 0.0 : 2.0 / ( samples - 1.0 );
	float uvStart = samples <= 1.0 ? 0.0 : - 1.0;
	for ( float i = 0.0; i < samples; i ++ ) {
		float uvOffset = uvStart + i * uvStride;
		#ifdef HORIZONTAL_PASS
			vec2 distribution = unpackRGBATo2Half( texture2D( shadow_pass, ( gl_FragCoord.xy + vec2( uvOffset, 0.0 ) * radius ) / resolution ) );
			mean += distribution.x;
			squared_mean += distribution.y * distribution.y + distribution.x * distribution.x;
		#else
			float depth = unpackRGBAToDepth( texture2D( shadow_pass, ( gl_FragCoord.xy + vec2( 0.0, uvOffset ) * radius ) / resolution ) );
			mean += depth;
			squared_mean += depth * depth;
		#endif
	}
	mean = mean / samples;
	squared_mean = squared_mean / samples;
	float std_dev = sqrt( squared_mean - mean * mean );
	gl_FragColor = pack2HalfToRGBA( vec2( mean, std_dev ) );
}`;function vM(s,e,t){let n=new Mo;const i=new se,r=new se,o=new Ct,a=new Qa({depthPacking:xh}),l=new Ch,u={},d=t.maxTextureSize,m={[xi]:_n,[_n]:xi,[Tn]:Tn},p=new tn({defines:{VSM_SAMPLES:8},uniforms:{shadow_pass:{value:null},resolution:{value:new se},radius:{value:4}},vertexShader:mM,fragmentShader:gM}),g=p.clone();g.defines.HORIZONTAL_PASS=1;const _=new at;_.setAttribute("position",new Rt(new Float32Array([-1,-1,.5,3,-1,.5,-1,3,.5]),3));const M=new Ie(_,p),y=this;this.enabled=!1,this.autoUpdate=!0,this.needsUpdate=!1,this.type=ih;let v=this.type;this.render=function(N,A,k){if(y.enabled===!1||y.autoUpdate===!1&&y.needsUpdate===!1||N.length===0)return;const D=s.getRenderTarget(),C=s.getActiveCubeFace(),X=s.getActiveMipmapLevel(),Y=s.state;Y.setBlending(kn),Y.buffers.color.setClear(1,1,1,1),Y.buffers.depth.setTest(!0),Y.setScissorTest(!1);const H=v!==Jn&&this.type===Jn,K=v===Jn&&this.type!==Jn;for(let J=0,ce=N.length;J<ce;J++){const de=N[J],Q=de.shadow;if(Q===void 0){console.warn("THREE.WebGLShadowMap:",de,"has no shadow.");continue}if(Q.autoUpdate===!1&&Q.needsUpdate===!1)continue;i.copy(Q.mapSize);const pe=Q.getFrameExtents();if(i.multiply(pe),r.copy(Q.mapSize),(i.x>d||i.y>d)&&(i.x>d&&(r.x=Math.floor(d/pe.x),i.x=r.x*pe.x,Q.mapSize.x=r.x),i.y>d&&(r.y=Math.floor(d/pe.y),i.y=r.y*pe.y,Q.mapSize.y=r.y)),Q.map===null||H===!0||K===!0){const Ue=this.type!==Jn?{minFilter:nn,magFilter:nn}:{};Q.map!==null&&Q.map.dispose(),Q.map=new qt(i.x,i.y,Ue),Q.map.texture.name=de.name+".shadowMap",Q.camera.updateProjectionMatrix()}s.setRenderTarget(Q.map),s.clear();const ve=Q.getViewportCount();for(let Ue=0;Ue<ve;Ue++){const Je=Q.getViewport(Ue);o.set(r.x*Je.x,r.y*Je.y,r.x*Je.z,r.y*Je.w),Y.viewport(o),Q.updateMatrices(de,Ue),n=Q.getFrustum(),T(A,k,Q.camera,de,this.type)}Q.isPointLightShadow!==!0&&this.type===Jn&&w(Q,k),Q.needsUpdate=!1}v=this.type,y.needsUpdate=!1,s.setRenderTarget(D,C,X)};function w(N,A){const k=e.update(M);p.defines.VSM_SAMPLES!==N.blurSamples&&(p.defines.VSM_SAMPLES=N.blurSamples,g.defines.VSM_SAMPLES=N.blurSamples,p.needsUpdate=!0,g.needsUpdate=!0),N.mapPass===null&&(N.mapPass=new qt(i.x,i.y)),p.uniforms.shadow_pass.value=N.map.texture,p.uniforms.resolution.value=N.mapSize,p.uniforms.radius.value=N.radius,s.setRenderTarget(N.mapPass),s.clear(),s.renderBufferDirect(A,null,k,p,M,null),g.uniforms.shadow_pass.value=N.mapPass.texture,g.uniforms.resolution.value=N.mapSize,g.uniforms.radius.value=N.radius,s.setRenderTarget(N.map),s.clear(),s.renderBufferDirect(A,null,k,g,M,null)}function S(N,A,k,D){let C=null;const X=k.isPointLight===!0?N.customDistanceMaterial:N.customDepthMaterial;if(X!==void 0)C=X;else if(C=k.isPointLight===!0?l:a,s.localClippingEnabled&&A.clipShadows===!0&&Array.isArray(A.clippingPlanes)&&A.clippingPlanes.length!==0||A.displacementMap&&A.displacementScale!==0||A.alphaMap&&A.alphaTest>0||A.map&&A.alphaTest>0){const Y=C.uuid,H=A.uuid;let K=u[Y];K===void 0&&(K={},u[Y]=K);let J=K[H];J===void 0&&(J=C.clone(),K[H]=J,A.addEventListener("dispose",z)),C=J}if(C.visible=A.visible,C.wireframe=A.wireframe,D===Jn?C.side=A.shadowSide!==null?A.shadowSide:A.side:C.side=A.shadowSide!==null?A.shadowSide:m[A.side],C.alphaMap=A.alphaMap,C.alphaTest=A.alphaTest,C.map=A.map,C.clipShadows=A.clipShadows,C.clippingPlanes=A.clippingPlanes,C.clipIntersection=A.clipIntersection,C.displacementMap=A.displacementMap,C.displacementScale=A.displacementScale,C.displacementBias=A.displacementBias,C.wireframeLinewidth=A.wireframeLinewidth,C.linewidth=A.linewidth,k.isPointLight===!0&&C.isMeshDistanceMaterial===!0){const Y=s.properties.get(C);Y.light=k}return C}function T(N,A,k,D,C){if(N.visible===!1)return;if(N.layers.test(A.layers)&&(N.isMesh||N.isLine||N.isPoints)&&(N.castShadow||N.receiveShadow&&C===Jn)&&(!N.frustumCulled||n.intersectsObject(N))){N.modelViewMatrix.multiplyMatrices(k.matrixWorldInverse,N.matrixWorld);const H=e.update(N),K=N.material;if(Array.isArray(K)){const J=H.groups;for(let ce=0,de=J.length;ce<de;ce++){const Q=J[ce],pe=K[Q.materialIndex];if(pe&&pe.visible){const ve=S(N,pe,D,C);N.onBeforeShadow(s,N,A,k,H,ve,Q),s.renderBufferDirect(k,null,H,ve,N,Q),N.onAfterShadow(s,N,A,k,H,ve,Q)}}}else if(K.visible){const J=S(N,K,D,C);N.onBeforeShadow(s,N,A,k,H,J,null),s.renderBufferDirect(k,null,H,J,N,null),N.onAfterShadow(s,N,A,k,H,J,null)}}const Y=N.children;for(let H=0,K=Y.length;H<K;H++)T(Y[H],A,k,D,C)}function z(N){N.target.removeEventListener("dispose",z);for(const k in u){const D=u[k],C=N.target.uuid;C in D&&(D[C].dispose(),delete D[C])}}}function _M(s){function e(){let G=!1;const re=new Ct;let ne=null;const Te=new Ct(0,0,0,0);return{setMask:function(Pe){ne!==Pe&&!G&&(s.colorMask(Pe,Pe,Pe,Pe),ne=Pe)},setLocked:function(Pe){G=Pe},setClear:function(Pe,_t,St,Et,Tt){Tt===!0&&(Pe*=Et,_t*=Et,St*=Et),re.set(Pe,_t,St,Et),Te.equals(re)===!1&&(s.clearColor(Pe,_t,St,Et),Te.copy(re))},reset:function(){G=!1,ne=null,Te.set(-1,0,0,0)}}}function t(){let G=!1,re=null,ne=null,Te=null;return{setTest:function(Pe){Pe?ke(s.DEPTH_TEST):Ee(s.DEPTH_TEST)},setMask:function(Pe){re!==Pe&&!G&&(s.depthMask(Pe),re=Pe)},setFunc:function(Pe){if(ne!==Pe){switch(Pe){case Uf:s.depthFunc(s.NEVER);break;case Ff:s.depthFunc(s.ALWAYS);break;case Bf:s.depthFunc(s.LESS);break;case Zs:s.depthFunc(s.LEQUAL);break;case zf:s.depthFunc(s.EQUAL);break;case kf:s.depthFunc(s.GEQUAL);break;case Vf:s.depthFunc(s.GREATER);break;case Hf:s.depthFunc(s.NOTEQUAL);break;default:s.depthFunc(s.LEQUAL)}ne=Pe}},setLocked:function(Pe){G=Pe},setClear:function(Pe){Te!==Pe&&(s.clearDepth(Pe),Te=Pe)},reset:function(){G=!1,re=null,ne=null,Te=null}}}function n(){let G=!1,re=null,ne=null,Te=null,Pe=null,_t=null,St=null,Et=null,Tt=null;return{setTest:function(ct){G||(ct?ke(s.STENCIL_TEST):Ee(s.STENCIL_TEST))},setMask:function(ct){re!==ct&&!G&&(s.stencilMask(ct),re=ct)},setFunc:function(ct,Kt,At){(ne!==ct||Te!==Kt||Pe!==At)&&(s.stencilFunc(ct,Kt,At),ne=ct,Te=Kt,Pe=At)},setOp:function(ct,Kt,At){(_t!==ct||St!==Kt||Et!==At)&&(s.stencilOp(ct,Kt,At),_t=ct,St=Kt,Et=At)},setLocked:function(ct){G=ct},setClear:function(ct){Tt!==ct&&(s.clearStencil(ct),Tt=ct)},reset:function(){G=!1,re=null,ne=null,Te=null,Pe=null,_t=null,St=null,Et=null,Tt=null}}}const i=new e,r=new t,o=new n,a=new WeakMap,l=new WeakMap;let u={},d={},m=new WeakMap,p=[],g=null,_=!1,M=null,y=null,v=null,w=null,S=null,T=null,z=null,N=new Oe(0,0,0),A=0,k=!1,D=null,C=null,X=null,Y=null,H=null;const K=s.getParameter(s.MAX_COMBINED_TEXTURE_IMAGE_UNITS);let J=!1,ce=0;const de=s.getParameter(s.VERSION);de.indexOf("WebGL")!==-1?(ce=parseFloat(/^WebGL (\d)/.exec(de)[1]),J=ce>=1):de.indexOf("OpenGL ES")!==-1&&(ce=parseFloat(/^OpenGL ES (\d)/.exec(de)[1]),J=ce>=2);let Q=null,pe={};const ve=s.getParameter(s.SCISSOR_BOX),Ue=s.getParameter(s.VIEWPORT),Je=new Ct().fromArray(ve),vt=new Ct().fromArray(Ue);function ae(G,re,ne,Te){const Pe=new Uint8Array(4),_t=s.createTexture();s.bindTexture(G,_t),s.texParameteri(G,s.TEXTURE_MIN_FILTER,s.NEAREST),s.texParameteri(G,s.TEXTURE_MAG_FILTER,s.NEAREST);for(let St=0;St<ne;St++)G===s.TEXTURE_3D||G===s.TEXTURE_2D_ARRAY?s.texImage3D(re,0,s.RGBA,1,1,Te,0,s.RGBA,s.UNSIGNED_BYTE,Pe):s.texImage2D(re+St,0,s.RGBA,1,1,0,s.RGBA,s.UNSIGNED_BYTE,Pe);return _t}const we={};we[s.TEXTURE_2D]=ae(s.TEXTURE_2D,s.TEXTURE_2D,1),we[s.TEXTURE_CUBE_MAP]=ae(s.TEXTURE_CUBE_MAP,s.TEXTURE_CUBE_MAP_POSITIVE_X,6),we[s.TEXTURE_2D_ARRAY]=ae(s.TEXTURE_2D_ARRAY,s.TEXTURE_2D_ARRAY,1,1),we[s.TEXTURE_3D]=ae(s.TEXTURE_3D,s.TEXTURE_3D,1,1),i.setClear(0,0,0,1),r.setClear(1),o.setClear(0),ke(s.DEPTH_TEST),r.setFunc(Zs),Ae(!1),ge(gc),ke(s.CULL_FACE),xe(kn);function ke(G){u[G]!==!0&&(s.enable(G),u[G]=!0)}function Ee(G){u[G]!==!1&&(s.disable(G),u[G]=!1)}function nt(G,re){return d[G]!==re?(s.bindFramebuffer(G,re),d[G]=re,G===s.DRAW_FRAMEBUFFER&&(d[s.FRAMEBUFFER]=re),G===s.FRAMEBUFFER&&(d[s.DRAW_FRAMEBUFFER]=re),!0):!1}function be(G,re){let ne=p,Te=!1;if(G){ne=m.get(re),ne===void 0&&(ne=[],m.set(re,ne));const Pe=G.textures;if(ne.length!==Pe.length||ne[0]!==s.COLOR_ATTACHMENT0){for(let _t=0,St=Pe.length;_t<St;_t++)ne[_t]=s.COLOR_ATTACHMENT0+_t;ne.length=Pe.length,Te=!0}}else ne[0]!==s.BACK&&(ne[0]=s.BACK,Te=!0);Te&&s.drawBuffers(ne)}function Z(G){return g!==G?(s.useProgram(G),g=G,!0):!1}const st={[zi]:s.FUNC_ADD,[yf]:s.FUNC_SUBTRACT,[xf]:s.FUNC_REVERSE_SUBTRACT};st[Mf]=s.MIN,st[bf]=s.MAX;const le={[Sf]:s.ZERO,[wf]:s.ONE,[Ef]:s.SRC_COLOR,[ka]:s.SRC_ALPHA,[If]:s.SRC_ALPHA_SATURATE,[Rf]:s.DST_COLOR,[Af]:s.DST_ALPHA,[Tf]:s.ONE_MINUS_SRC_COLOR,[Va]:s.ONE_MINUS_SRC_ALPHA,[Pf]:s.ONE_MINUS_DST_COLOR,[Cf]:s.ONE_MINUS_DST_ALPHA,[Lf]:s.CONSTANT_COLOR,[Df]:s.ONE_MINUS_CONSTANT_COLOR,[Nf]:s.CONSTANT_ALPHA,[Of]:s.ONE_MINUS_CONSTANT_ALPHA};function xe(G,re,ne,Te,Pe,_t,St,Et,Tt,ct){if(G===kn){_===!0&&(Ee(s.BLEND),_=!1);return}if(_===!1&&(ke(s.BLEND),_=!0),G!==_f){if(G!==M||ct!==k){if((y!==zi||S!==zi)&&(s.blendEquation(s.FUNC_ADD),y=zi,S=zi),ct)switch(G){case xr:s.blendFuncSeparate(s.ONE,s.ONE_MINUS_SRC_ALPHA,s.ONE,s.ONE_MINUS_SRC_ALPHA);break;case za:s.blendFunc(s.ONE,s.ONE);break;case vc:s.blendFuncSeparate(s.ZERO,s.ONE_MINUS_SRC_COLOR,s.ZERO,s.ONE);break;case _c:s.blendFuncSeparate(s.ZERO,s.SRC_COLOR,s.ZERO,s.SRC_ALPHA);break;default:console.error("THREE.WebGLState: Invalid blending: ",G);break}else switch(G){case xr:s.blendFuncSeparate(s.SRC_ALPHA,s.ONE_MINUS_SRC_ALPHA,s.ONE,s.ONE_MINUS_SRC_ALPHA);break;case za:s.blendFunc(s.SRC_ALPHA,s.ONE);break;case vc:s.blendFuncSeparate(s.ZERO,s.ONE_MINUS_SRC_COLOR,s.ZERO,s.ONE);break;case _c:s.blendFunc(s.ZERO,s.SRC_COLOR);break;default:console.error("THREE.WebGLState: Invalid blending: ",G);break}v=null,w=null,T=null,z=null,N.set(0,0,0),A=0,M=G,k=ct}return}Pe=Pe||re,_t=_t||ne,St=St||Te,(re!==y||Pe!==S)&&(s.blendEquationSeparate(st[re],st[Pe]),y=re,S=Pe),(ne!==v||Te!==w||_t!==T||St!==z)&&(s.blendFuncSeparate(le[ne],le[Te],le[_t],le[St]),v=ne,w=Te,T=_t,z=St),(Et.equals(N)===!1||Tt!==A)&&(s.blendColor(Et.r,Et.g,Et.b,Tt),N.copy(Et),A=Tt),M=G,k=!1}function he(G,re){G.side===Tn?Ee(s.CULL_FACE):ke(s.CULL_FACE);let ne=G.side===_n;re&&(ne=!ne),Ae(ne),G.blending===xr&&G.transparent===!1?xe(kn):xe(G.blending,G.blendEquation,G.blendSrc,G.blendDst,G.blendEquationAlpha,G.blendSrcAlpha,G.blendDstAlpha,G.blendColor,G.blendAlpha,G.premultipliedAlpha),r.setFunc(G.depthFunc),r.setTest(G.depthTest),r.setMask(G.depthWrite),i.setMask(G.colorWrite);const Te=G.stencilWrite;o.setTest(Te),Te&&(o.setMask(G.stencilWriteMask),o.setFunc(G.stencilFunc,G.stencilRef,G.stencilFuncMask),o.setOp(G.stencilFail,G.stencilZFail,G.stencilZPass)),Ge(G.polygonOffset,G.polygonOffsetFactor,G.polygonOffsetUnits),G.alphaToCoverage===!0?ke(s.SAMPLE_ALPHA_TO_COVERAGE):Ee(s.SAMPLE_ALPHA_TO_COVERAGE)}function Ae(G){D!==G&&(G?s.frontFace(s.CW):s.frontFace(s.CCW),D=G)}function ge(G){G!==mf?(ke(s.CULL_FACE),G!==C&&(G===gc?s.cullFace(s.BACK):G===gf?s.cullFace(s.FRONT):s.cullFace(s.FRONT_AND_BACK))):Ee(s.CULL_FACE),C=G}function Fe(G){G!==X&&(J&&s.lineWidth(G),X=G)}function Ge(G,re,ne){G?(ke(s.POLYGON_OFFSET_FILL),(Y!==re||H!==ne)&&(s.polygonOffset(re,ne),Y=re,H=ne)):Ee(s.POLYGON_OFFSET_FILL)}function B(G){G?ke(s.SCISSOR_TEST):Ee(s.SCISSOR_TEST)}function I(G){G===void 0&&(G=s.TEXTURE0+K-1),Q!==G&&(s.activeTexture(G),Q=G)}function $(G,re,ne){ne===void 0&&(Q===null?ne=s.TEXTURE0+K-1:ne=Q);let Te=pe[ne];Te===void 0&&(Te={type:void 0,texture:void 0},pe[ne]=Te),(Te.type!==G||Te.texture!==re)&&(Q!==ne&&(s.activeTexture(ne),Q=ne),s.bindTexture(G,re||we[G]),Te.type=G,Te.texture=re)}function ue(){const G=pe[Q];G!==void 0&&G.type!==void 0&&(s.bindTexture(G.type,null),G.type=void 0,G.texture=void 0)}function _e(){try{s.compressedTexImage2D.apply(s,arguments)}catch(G){console.error("THREE.WebGLState:",G)}}function me(){try{s.compressedTexImage3D.apply(s,arguments)}catch(G){console.error("THREE.WebGLState:",G)}}function Ye(){try{s.texSubImage2D.apply(s,arguments)}catch(G){console.error("THREE.WebGLState:",G)}}function Re(){try{s.texSubImage3D.apply(s,arguments)}catch(G){console.error("THREE.WebGLState:",G)}}function Le(){try{s.compressedTexSubImage2D.apply(s,arguments)}catch(G){console.error("THREE.WebGLState:",G)}}function tt(){try{s.compressedTexSubImage3D.apply(s,arguments)}catch(G){console.error("THREE.WebGLState:",G)}}function Ce(){try{s.texStorage2D.apply(s,arguments)}catch(G){console.error("THREE.WebGLState:",G)}}function Xe(){try{s.texStorage3D.apply(s,arguments)}catch(G){console.error("THREE.WebGLState:",G)}}function lt(){try{s.texImage2D.apply(s,arguments)}catch(G){console.error("THREE.WebGLState:",G)}}function je(){try{s.texImage3D.apply(s,arguments)}catch(G){console.error("THREE.WebGLState:",G)}}function ze(G){Je.equals(G)===!1&&(s.scissor(G.x,G.y,G.z,G.w),Je.copy(G))}function Qe(G){vt.equals(G)===!1&&(s.viewport(G.x,G.y,G.z,G.w),vt.copy(G))}function ft(G,re){let ne=l.get(re);ne===void 0&&(ne=new WeakMap,l.set(re,ne));let Te=ne.get(G);Te===void 0&&(Te=s.getUniformBlockIndex(re,G.name),ne.set(G,Te))}function V(G,re){const Te=l.get(re).get(G);a.get(re)!==Te&&(s.uniformBlockBinding(re,Te,G.__bindingPointIndex),a.set(re,Te))}function fe(){s.disable(s.BLEND),s.disable(s.CULL_FACE),s.disable(s.DEPTH_TEST),s.disable(s.POLYGON_OFFSET_FILL),s.disable(s.SCISSOR_TEST),s.disable(s.STENCIL_TEST),s.disable(s.SAMPLE_ALPHA_TO_COVERAGE),s.blendEquation(s.FUNC_ADD),s.blendFunc(s.ONE,s.ZERO),s.blendFuncSeparate(s.ONE,s.ZERO,s.ONE,s.ZERO),s.blendColor(0,0,0,0),s.colorMask(!0,!0,!0,!0),s.clearColor(0,0,0,0),s.depthMask(!0),s.depthFunc(s.LESS),s.clearDepth(1),s.stencilMask(4294967295),s.stencilFunc(s.ALWAYS,0,4294967295),s.stencilOp(s.KEEP,s.KEEP,s.KEEP),s.clearStencil(0),s.cullFace(s.BACK),s.frontFace(s.CCW),s.polygonOffset(0,0),s.activeTexture(s.TEXTURE0),s.bindFramebuffer(s.FRAMEBUFFER,null),s.bindFramebuffer(s.DRAW_FRAMEBUFFER,null),s.bindFramebuffer(s.READ_FRAMEBUFFER,null),s.useProgram(null),s.lineWidth(1),s.scissor(0,0,s.canvas.width,s.canvas.height),s.viewport(0,0,s.canvas.width,s.canvas.height),u={},Q=null,pe={},d={},m=new WeakMap,p=[],g=null,_=!1,M=null,y=null,v=null,w=null,S=null,T=null,z=null,N=new Oe(0,0,0),A=0,k=!1,D=null,C=null,X=null,Y=null,H=null,Je.set(0,0,s.canvas.width,s.canvas.height),vt.set(0,0,s.canvas.width,s.canvas.height),i.reset(),r.reset(),o.reset()}return{buffers:{color:i,depth:r,stencil:o},enable:ke,disable:Ee,bindFramebuffer:nt,drawBuffers:be,useProgram:Z,setBlending:xe,setMaterial:he,setFlipSided:Ae,setCullFace:ge,setLineWidth:Fe,setPolygonOffset:Ge,setScissorTest:B,activeTexture:I,bindTexture:$,unbindTexture:ue,compressedTexImage2D:_e,compressedTexImage3D:me,texImage2D:lt,texImage3D:je,updateUBOMapping:ft,uniformBlockBinding:V,texStorage2D:Ce,texStorage3D:Xe,texSubImage2D:Ye,texSubImage3D:Re,compressedTexSubImage2D:Le,compressedTexSubImage3D:tt,scissor:ze,viewport:Qe,reset:fe}}function yM(s,e,t,n,i,r,o){const a=e.has("WEBGL_multisampled_render_to_texture")?e.get("WEBGL_multisampled_render_to_texture"):null,l=typeof navigator>"u"?!1:/OculusBrowser/g.test(navigator.userAgent),u=new se,d=new WeakMap;let m;const p=new WeakMap;let g=!1;try{g=typeof OffscreenCanvas<"u"&&new OffscreenCanvas(1,1).getContext("2d")!==null}catch{}function _(B,I){return g?new OffscreenCanvas(B,I):ao("canvas")}function M(B,I,$){let ue=1;const _e=Ge(B);if((_e.width>$||_e.height>$)&&(ue=$/Math.max(_e.width,_e.height)),ue<1)if(typeof HTMLImageElement<"u"&&B instanceof HTMLImageElement||typeof HTMLCanvasElement<"u"&&B instanceof HTMLCanvasElement||typeof ImageBitmap<"u"&&B instanceof ImageBitmap||typeof VideoFrame<"u"&&B instanceof VideoFrame){const me=Math.floor(ue*_e.width),Ye=Math.floor(ue*_e.height);m===void 0&&(m=_(me,Ye));const Re=I?_(me,Ye):m;return Re.width=me,Re.height=Ye,Re.getContext("2d").drawImage(B,0,0,me,Ye),console.warn("THREE.WebGLRenderer: Texture has been resized from ("+_e.width+"x"+_e.height+") to ("+me+"x"+Ye+")."),Re}else return"data"in B&&console.warn("THREE.WebGLRenderer: Image in DataTexture is too big ("+_e.width+"x"+_e.height+")."),B;return B}function y(B){return B.generateMipmaps&&B.minFilter!==nn&&B.minFilter!==Xt}function v(B){s.generateMipmap(B)}function w(B,I,$,ue,_e=!1){if(B!==null){if(s[B]!==void 0)return s[B];console.warn("THREE.WebGLRenderer: Attempt to use non-existing WebGL internal format '"+B+"'")}let me=I;if(I===s.RED&&($===s.FLOAT&&(me=s.R32F),$===s.HALF_FLOAT&&(me=s.R16F),$===s.UNSIGNED_BYTE&&(me=s.R8)),I===s.RED_INTEGER&&($===s.UNSIGNED_BYTE&&(me=s.R8UI),$===s.UNSIGNED_SHORT&&(me=s.R16UI),$===s.UNSIGNED_INT&&(me=s.R32UI),$===s.BYTE&&(me=s.R8I),$===s.SHORT&&(me=s.R16I),$===s.INT&&(me=s.R32I)),I===s.RG&&($===s.FLOAT&&(me=s.RG32F),$===s.HALF_FLOAT&&(me=s.RG16F),$===s.UNSIGNED_BYTE&&(me=s.RG8)),I===s.RG_INTEGER&&($===s.UNSIGNED_BYTE&&(me=s.RG8UI),$===s.UNSIGNED_SHORT&&(me=s.RG16UI),$===s.UNSIGNED_INT&&(me=s.RG32UI),$===s.BYTE&&(me=s.RG8I),$===s.SHORT&&(me=s.RG16I),$===s.INT&&(me=s.RG32I)),I===s.RGB&&$===s.UNSIGNED_INT_5_9_9_9_REV&&(me=s.RGB9_E5),I===s.RGBA){const Ye=_e?no:wt.getTransfer(ue);$===s.FLOAT&&(me=s.RGBA32F),$===s.HALF_FLOAT&&(me=s.RGBA16F),$===s.UNSIGNED_BYTE&&(me=Ye===It?s.SRGB8_ALPHA8:s.RGBA8),$===s.UNSIGNED_SHORT_4_4_4_4&&(me=s.RGBA4),$===s.UNSIGNED_SHORT_5_5_5_1&&(me=s.RGB5_A1)}return(me===s.R16F||me===s.R32F||me===s.RG16F||me===s.RG32F||me===s.RGBA16F||me===s.RGBA32F)&&e.get("EXT_color_buffer_float"),me}function S(B,I){return y(B)===!0||B.isFramebufferTexture&&B.minFilter!==nn&&B.minFilter!==Xt?Math.log2(Math.max(I.width,I.height))+1:B.mipmaps!==void 0&&B.mipmaps.length>0?B.mipmaps.length:B.isCompressedTexture&&Array.isArray(B.image)?I.mipmaps.length:1}function T(B){const I=B.target;I.removeEventListener("dispose",T),N(I),I.isVideoTexture&&d.delete(I)}function z(B){const I=B.target;I.removeEventListener("dispose",z),k(I)}function N(B){const I=n.get(B);if(I.__webglInit===void 0)return;const $=B.source,ue=p.get($);if(ue){const _e=ue[I.__cacheKey];_e.usedTimes--,_e.usedTimes===0&&A(B),Object.keys(ue).length===0&&p.delete($)}n.remove(B)}function A(B){const I=n.get(B);s.deleteTexture(I.__webglTexture);const $=B.source,ue=p.get($);delete ue[I.__cacheKey],o.memory.textures--}function k(B){const I=n.get(B);if(B.depthTexture&&B.depthTexture.dispose(),B.isWebGLCubeRenderTarget)for(let ue=0;ue<6;ue++){if(Array.isArray(I.__webglFramebuffer[ue]))for(let _e=0;_e<I.__webglFramebuffer[ue].length;_e++)s.deleteFramebuffer(I.__webglFramebuffer[ue][_e]);else s.deleteFramebuffer(I.__webglFramebuffer[ue]);I.__webglDepthbuffer&&s.deleteRenderbuffer(I.__webglDepthbuffer[ue])}else{if(Array.isArray(I.__webglFramebuffer))for(let ue=0;ue<I.__webglFramebuffer.length;ue++)s.deleteFramebuffer(I.__webglFramebuffer[ue]);else s.deleteFramebuffer(I.__webglFramebuffer);if(I.__webglDepthbuffer&&s.deleteRenderbuffer(I.__webglDepthbuffer),I.__webglMultisampledFramebuffer&&s.deleteFramebuffer(I.__webglMultisampledFramebuffer),I.__webglColorRenderbuffer)for(let ue=0;ue<I.__webglColorRenderbuffer.length;ue++)I.__webglColorRenderbuffer[ue]&&s.deleteRenderbuffer(I.__webglColorRenderbuffer[ue]);I.__webglDepthRenderbuffer&&s.deleteRenderbuffer(I.__webglDepthRenderbuffer)}const $=B.textures;for(let ue=0,_e=$.length;ue<_e;ue++){const me=n.get($[ue]);me.__webglTexture&&(s.deleteTexture(me.__webglTexture),o.memory.textures--),n.remove($[ue])}n.remove(B)}let D=0;function C(){D=0}function X(){const B=D;return B>=i.maxTextures&&console.warn("THREE.WebGLTextures: Trying to use "+B+" texture units while this GPU supports only "+i.maxTextures),D+=1,B}function Y(B){const I=[];return I.push(B.wrapS),I.push(B.wrapT),I.push(B.wrapR||0),I.push(B.magFilter),I.push(B.minFilter),I.push(B.anisotropy),I.push(B.internalFormat),I.push(B.format),I.push(B.type),I.push(B.generateMipmaps),I.push(B.premultiplyAlpha),I.push(B.flipY),I.push(B.unpackAlignment),I.push(B.colorSpace),I.join()}function H(B,I){const $=n.get(B);if(B.isVideoTexture&&ge(B),B.isRenderTargetTexture===!1&&B.version>0&&$.__version!==B.version){const ue=B.image;if(ue===null)console.warn("THREE.WebGLRenderer: Texture marked for update but no image data found.");else if(ue.complete===!1)console.warn("THREE.WebGLRenderer: Texture marked for update but image is incomplete");else{Je($,B,I);return}}t.bindTexture(s.TEXTURE_2D,$.__webglTexture,s.TEXTURE0+I)}function K(B,I){const $=n.get(B);if(B.version>0&&$.__version!==B.version){Je($,B,I);return}t.bindTexture(s.TEXTURE_2D_ARRAY,$.__webglTexture,s.TEXTURE0+I)}function J(B,I){const $=n.get(B);if(B.version>0&&$.__version!==B.version){Je($,B,I);return}t.bindTexture(s.TEXTURE_3D,$.__webglTexture,s.TEXTURE0+I)}function ce(B,I){const $=n.get(B);if(B.version>0&&$.__version!==B.version){vt($,B,I);return}t.bindTexture(s.TEXTURE_CUBE_MAP,$.__webglTexture,s.TEXTURE0+I)}const de={[Js]:s.REPEAT,[Bn]:s.CLAMP_TO_EDGE,[$s]:s.MIRRORED_REPEAT},Q={[nn]:s.NEAREST,[hh]:s.NEAREST_MIPMAP_NEAREST,[ts]:s.NEAREST_MIPMAP_LINEAR,[Xt]:s.LINEAR,[Ws]:s.LINEAR_MIPMAP_NEAREST,[Qn]:s.LINEAR_MIPMAP_LINEAR},pe={[ap]:s.NEVER,[fp]:s.ALWAYS,[lp]:s.LESS,[Mh]:s.LEQUAL,[cp]:s.EQUAL,[dp]:s.GEQUAL,[hp]:s.GREATER,[up]:s.NOTEQUAL};function ve(B,I){if(I.type===zn&&e.has("OES_texture_float_linear")===!1&&(I.magFilter===Xt||I.magFilter===Ws||I.magFilter===ts||I.magFilter===Qn||I.minFilter===Xt||I.minFilter===Ws||I.minFilter===ts||I.minFilter===Qn)&&console.warn("THREE.WebGLRenderer: Unable to use linear filtering with floating point textures. OES_texture_float_linear not supported on this device."),s.texParameteri(B,s.TEXTURE_WRAP_S,de[I.wrapS]),s.texParameteri(B,s.TEXTURE_WRAP_T,de[I.wrapT]),(B===s.TEXTURE_3D||B===s.TEXTURE_2D_ARRAY)&&s.texParameteri(B,s.TEXTURE_WRAP_R,de[I.wrapR]),s.texParameteri(B,s.TEXTURE_MAG_FILTER,Q[I.magFilter]),s.texParameteri(B,s.TEXTURE_MIN_FILTER,Q[I.minFilter]),I.compareFunction&&(s.texParameteri(B,s.TEXTURE_COMPARE_MODE,s.COMPARE_REF_TO_TEXTURE),s.texParameteri(B,s.TEXTURE_COMPARE_FUNC,pe[I.compareFunction])),e.has("EXT_texture_filter_anisotropic")===!0){if(I.magFilter===nn||I.minFilter!==ts&&I.minFilter!==Qn||I.type===zn&&e.has("OES_texture_float_linear")===!1)return;if(I.anisotropy>1||n.get(I).__currentAnisotropy){const $=e.get("EXT_texture_filter_anisotropic");s.texParameterf(B,$.TEXTURE_MAX_ANISOTROPY_EXT,Math.min(I.anisotropy,i.getMaxAnisotropy())),n.get(I).__currentAnisotropy=I.anisotropy}}}function Ue(B,I){let $=!1;B.__webglInit===void 0&&(B.__webglInit=!0,I.addEventListener("dispose",T));const ue=I.source;let _e=p.get(ue);_e===void 0&&(_e={},p.set(ue,_e));const me=Y(I);if(me!==B.__cacheKey){_e[me]===void 0&&(_e[me]={texture:s.createTexture(),usedTimes:0},o.memory.textures++,$=!0),_e[me].usedTimes++;const Ye=_e[B.__cacheKey];Ye!==void 0&&(_e[B.__cacheKey].usedTimes--,Ye.usedTimes===0&&A(I)),B.__cacheKey=me,B.__webglTexture=_e[me].texture}return $}function Je(B,I,$){let ue=s.TEXTURE_2D;(I.isDataArrayTexture||I.isCompressedArrayTexture)&&(ue=s.TEXTURE_2D_ARRAY),I.isData3DTexture&&(ue=s.TEXTURE_3D);const _e=Ue(B,I),me=I.source;t.bindTexture(ue,B.__webglTexture,s.TEXTURE0+$);const Ye=n.get(me);if(me.version!==Ye.__version||_e===!0){t.activeTexture(s.TEXTURE0+$);const Re=wt.getPrimaries(wt.workingColorSpace),Le=I.colorSpace===pi?null:wt.getPrimaries(I.colorSpace),tt=I.colorSpace===pi||Re===Le?s.NONE:s.BROWSER_DEFAULT_WEBGL;s.pixelStorei(s.UNPACK_FLIP_Y_WEBGL,I.flipY),s.pixelStorei(s.UNPACK_PREMULTIPLY_ALPHA_WEBGL,I.premultiplyAlpha),s.pixelStorei(s.UNPACK_ALIGNMENT,I.unpackAlignment),s.pixelStorei(s.UNPACK_COLORSPACE_CONVERSION_WEBGL,tt);let Ce=M(I.image,!1,i.maxTextureSize);Ce=Fe(I,Ce);const Xe=r.convert(I.format,I.colorSpace),lt=r.convert(I.type);let je=w(I.internalFormat,Xe,lt,I.colorSpace,I.isVideoTexture);ve(ue,I);let ze;const Qe=I.mipmaps,ft=I.isVideoTexture!==!0,V=Ye.__version===void 0||_e===!0,fe=me.dataReady,G=S(I,Ce);if(I.isDepthTexture)je=s.DEPTH_COMPONENT16,I.type===zn?je=s.DEPTH_COMPONENT32F:I.type===wr?je=s.DEPTH_COMPONENT24:I.type===ds&&(je=s.DEPTH24_STENCIL8),V&&(ft?t.texStorage2D(s.TEXTURE_2D,1,je,Ce.width,Ce.height):t.texImage2D(s.TEXTURE_2D,0,je,Ce.width,Ce.height,0,Xe,lt,null));else if(I.isDataTexture)if(Qe.length>0){ft&&V&&t.texStorage2D(s.TEXTURE_2D,G,je,Qe[0].width,Qe[0].height);for(let re=0,ne=Qe.length;re<ne;re++)ze=Qe[re],ft?fe&&t.texSubImage2D(s.TEXTURE_2D,re,0,0,ze.width,ze.height,Xe,lt,ze.data):t.texImage2D(s.TEXTURE_2D,re,je,ze.width,ze.height,0,Xe,lt,ze.data);I.generateMipmaps=!1}else ft?(V&&t.texStorage2D(s.TEXTURE_2D,G,je,Ce.width,Ce.height),fe&&t.texSubImage2D(s.TEXTURE_2D,0,0,0,Ce.width,Ce.height,Xe,lt,Ce.data)):t.texImage2D(s.TEXTURE_2D,0,je,Ce.width,Ce.height,0,Xe,lt,Ce.data);else if(I.isCompressedTexture)if(I.isCompressedArrayTexture){ft&&V&&t.texStorage3D(s.TEXTURE_2D_ARRAY,G,je,Qe[0].width,Qe[0].height,Ce.depth);for(let re=0,ne=Qe.length;re<ne;re++)ze=Qe[re],I.format!==Cn?Xe!==null?ft?fe&&t.compressedTexSubImage3D(s.TEXTURE_2D_ARRAY,re,0,0,0,ze.width,ze.height,Ce.depth,Xe,ze.data,0,0):t.compressedTexImage3D(s.TEXTURE_2D_ARRAY,re,je,ze.width,ze.height,Ce.depth,0,ze.data,0,0):console.warn("THREE.WebGLRenderer: Attempt to load unsupported compressed texture format in .uploadTexture()"):ft?fe&&t.texSubImage3D(s.TEXTURE_2D_ARRAY,re,0,0,0,ze.width,ze.height,Ce.depth,Xe,lt,ze.data):t.texImage3D(s.TEXTURE_2D_ARRAY,re,je,ze.width,ze.height,Ce.depth,0,Xe,lt,ze.data)}else{ft&&V&&t.texStorage2D(s.TEXTURE_2D,G,je,Qe[0].width,Qe[0].height);for(let re=0,ne=Qe.length;re<ne;re++)ze=Qe[re],I.format!==Cn?Xe!==null?ft?fe&&t.compressedTexSubImage2D(s.TEXTURE_2D,re,0,0,ze.width,ze.height,Xe,ze.data):t.compressedTexImage2D(s.TEXTURE_2D,re,je,ze.width,ze.height,0,ze.data):console.warn("THREE.WebGLRenderer: Attempt to load unsupported compressed texture format in .uploadTexture()"):ft?fe&&t.texSubImage2D(s.TEXTURE_2D,re,0,0,ze.width,ze.height,Xe,lt,ze.data):t.texImage2D(s.TEXTURE_2D,re,je,ze.width,ze.height,0,Xe,lt,ze.data)}else if(I.isDataArrayTexture)ft?(V&&t.texStorage3D(s.TEXTURE_2D_ARRAY,G,je,Ce.width,Ce.height,Ce.depth),fe&&t.texSubImage3D(s.TEXTURE_2D_ARRAY,0,0,0,0,Ce.width,Ce.height,Ce.depth,Xe,lt,Ce.data)):t.texImage3D(s.TEXTURE_2D_ARRAY,0,je,Ce.width,Ce.height,Ce.depth,0,Xe,lt,Ce.data);else if(I.isData3DTexture)ft?(V&&t.texStorage3D(s.TEXTURE_3D,G,je,Ce.width,Ce.height,Ce.depth),fe&&t.texSubImage3D(s.TEXTURE_3D,0,0,0,0,Ce.width,Ce.height,Ce.depth,Xe,lt,Ce.data)):t.texImage3D(s.TEXTURE_3D,0,je,Ce.width,Ce.height,Ce.depth,0,Xe,lt,Ce.data);else if(I.isFramebufferTexture){if(V)if(ft)t.texStorage2D(s.TEXTURE_2D,G,je,Ce.width,Ce.height);else{let re=Ce.width,ne=Ce.height;for(let Te=0;Te<G;Te++)t.texImage2D(s.TEXTURE_2D,Te,je,re,ne,0,Xe,lt,null),re>>=1,ne>>=1}}else if(Qe.length>0){if(ft&&V){const re=Ge(Qe[0]);t.texStorage2D(s.TEXTURE_2D,G,je,re.width,re.height)}for(let re=0,ne=Qe.length;re<ne;re++)ze=Qe[re],ft?fe&&t.texSubImage2D(s.TEXTURE_2D,re,0,0,Xe,lt,ze):t.texImage2D(s.TEXTURE_2D,re,je,Xe,lt,ze);I.generateMipmaps=!1}else if(ft){if(V){const re=Ge(Ce);t.texStorage2D(s.TEXTURE_2D,G,je,re.width,re.height)}fe&&t.texSubImage2D(s.TEXTURE_2D,0,0,0,Xe,lt,Ce)}else t.texImage2D(s.TEXTURE_2D,0,je,Xe,lt,Ce);y(I)&&v(ue),Ye.__version=me.version,I.onUpdate&&I.onUpdate(I)}B.__version=I.version}function vt(B,I,$){if(I.image.length!==6)return;const ue=Ue(B,I),_e=I.source;t.bindTexture(s.TEXTURE_CUBE_MAP,B.__webglTexture,s.TEXTURE0+$);const me=n.get(_e);if(_e.version!==me.__version||ue===!0){t.activeTexture(s.TEXTURE0+$);const Ye=wt.getPrimaries(wt.workingColorSpace),Re=I.colorSpace===pi?null:wt.getPrimaries(I.colorSpace),Le=I.colorSpace===pi||Ye===Re?s.NONE:s.BROWSER_DEFAULT_WEBGL;s.pixelStorei(s.UNPACK_FLIP_Y_WEBGL,I.flipY),s.pixelStorei(s.UNPACK_PREMULTIPLY_ALPHA_WEBGL,I.premultiplyAlpha),s.pixelStorei(s.UNPACK_ALIGNMENT,I.unpackAlignment),s.pixelStorei(s.UNPACK_COLORSPACE_CONVERSION_WEBGL,Le);const tt=I.isCompressedTexture||I.image[0].isCompressedTexture,Ce=I.image[0]&&I.image[0].isDataTexture,Xe=[];for(let ne=0;ne<6;ne++)!tt&&!Ce?Xe[ne]=M(I.image[ne],!0,i.maxCubemapSize):Xe[ne]=Ce?I.image[ne].image:I.image[ne],Xe[ne]=Fe(I,Xe[ne]);const lt=Xe[0],je=r.convert(I.format,I.colorSpace),ze=r.convert(I.type),Qe=w(I.internalFormat,je,ze,I.colorSpace),ft=I.isVideoTexture!==!0,V=me.__version===void 0||ue===!0,fe=_e.dataReady;let G=S(I,lt);ve(s.TEXTURE_CUBE_MAP,I);let re;if(tt){ft&&V&&t.texStorage2D(s.TEXTURE_CUBE_MAP,G,Qe,lt.width,lt.height);for(let ne=0;ne<6;ne++){re=Xe[ne].mipmaps;for(let Te=0;Te<re.length;Te++){const Pe=re[Te];I.format!==Cn?je!==null?ft?fe&&t.compressedTexSubImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,Te,0,0,Pe.width,Pe.height,je,Pe.data):t.compressedTexImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,Te,Qe,Pe.width,Pe.height,0,Pe.data):console.warn("THREE.WebGLRenderer: Attempt to load unsupported compressed texture format in .setTextureCube()"):ft?fe&&t.texSubImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,Te,0,0,Pe.width,Pe.height,je,ze,Pe.data):t.texImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,Te,Qe,Pe.width,Pe.height,0,je,ze,Pe.data)}}}else{if(re=I.mipmaps,ft&&V){re.length>0&&G++;const ne=Ge(Xe[0]);t.texStorage2D(s.TEXTURE_CUBE_MAP,G,Qe,ne.width,ne.height)}for(let ne=0;ne<6;ne++)if(Ce){ft?fe&&t.texSubImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,0,0,0,Xe[ne].width,Xe[ne].height,je,ze,Xe[ne].data):t.texImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,0,Qe,Xe[ne].width,Xe[ne].height,0,je,ze,Xe[ne].data);for(let Te=0;Te<re.length;Te++){const _t=re[Te].image[ne].image;ft?fe&&t.texSubImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,Te+1,0,0,_t.width,_t.height,je,ze,_t.data):t.texImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,Te+1,Qe,_t.width,_t.height,0,je,ze,_t.data)}}else{ft?fe&&t.texSubImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,0,0,0,je,ze,Xe[ne]):t.texImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,0,Qe,je,ze,Xe[ne]);for(let Te=0;Te<re.length;Te++){const Pe=re[Te];ft?fe&&t.texSubImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,Te+1,0,0,je,ze,Pe.image[ne]):t.texImage2D(s.TEXTURE_CUBE_MAP_POSITIVE_X+ne,Te+1,Qe,je,ze,Pe.image[ne])}}}y(I)&&v(s.TEXTURE_CUBE_MAP),me.__version=_e.version,I.onUpdate&&I.onUpdate(I)}B.__version=I.version}function ae(B,I,$,ue,_e,me){const Ye=r.convert($.format,$.colorSpace),Re=r.convert($.type),Le=w($.internalFormat,Ye,Re,$.colorSpace);if(!n.get(I).__hasExternalTextures){const Ce=Math.max(1,I.width>>me),Xe=Math.max(1,I.height>>me);_e===s.TEXTURE_3D||_e===s.TEXTURE_2D_ARRAY?t.texImage3D(_e,me,Le,Ce,Xe,I.depth,0,Ye,Re,null):t.texImage2D(_e,me,Le,Ce,Xe,0,Ye,Re,null)}t.bindFramebuffer(s.FRAMEBUFFER,B),Ae(I)?a.framebufferTexture2DMultisampleEXT(s.FRAMEBUFFER,ue,_e,n.get($).__webglTexture,0,he(I)):(_e===s.TEXTURE_2D||_e>=s.TEXTURE_CUBE_MAP_POSITIVE_X&&_e<=s.TEXTURE_CUBE_MAP_NEGATIVE_Z)&&s.framebufferTexture2D(s.FRAMEBUFFER,ue,_e,n.get($).__webglTexture,me),t.bindFramebuffer(s.FRAMEBUFFER,null)}function we(B,I,$){if(s.bindRenderbuffer(s.RENDERBUFFER,B),I.depthBuffer&&!I.stencilBuffer){let ue=s.DEPTH_COMPONENT24;if($||Ae(I)){const _e=I.depthTexture;_e&&_e.isDepthTexture&&(_e.type===zn?ue=s.DEPTH_COMPONENT32F:_e.type===wr&&(ue=s.DEPTH_COMPONENT24));const me=he(I);Ae(I)?a.renderbufferStorageMultisampleEXT(s.RENDERBUFFER,me,ue,I.width,I.height):s.renderbufferStorageMultisample(s.RENDERBUFFER,me,ue,I.width,I.height)}else s.renderbufferStorage(s.RENDERBUFFER,ue,I.width,I.height);s.framebufferRenderbuffer(s.FRAMEBUFFER,s.DEPTH_ATTACHMENT,s.RENDERBUFFER,B)}else if(I.depthBuffer&&I.stencilBuffer){const ue=he(I);$&&Ae(I)===!1?s.renderbufferStorageMultisample(s.RENDERBUFFER,ue,s.DEPTH24_STENCIL8,I.width,I.height):Ae(I)?a.renderbufferStorageMultisampleEXT(s.RENDERBUFFER,ue,s.DEPTH24_STENCIL8,I.width,I.height):s.renderbufferStorage(s.RENDERBUFFER,s.DEPTH_STENCIL,I.width,I.height),s.framebufferRenderbuffer(s.FRAMEBUFFER,s.DEPTH_STENCIL_ATTACHMENT,s.RENDERBUFFER,B)}else{const ue=I.textures;for(let _e=0;_e<ue.length;_e++){const me=ue[_e],Ye=r.convert(me.format,me.colorSpace),Re=r.convert(me.type),Le=w(me.internalFormat,Ye,Re,me.colorSpace),tt=he(I);$&&Ae(I)===!1?s.renderbufferStorageMultisample(s.RENDERBUFFER,tt,Le,I.width,I.height):Ae(I)?a.renderbufferStorageMultisampleEXT(s.RENDERBUFFER,tt,Le,I.width,I.height):s.renderbufferStorage(s.RENDERBUFFER,Le,I.width,I.height)}}s.bindRenderbuffer(s.RENDERBUFFER,null)}function ke(B,I){if(I&&I.isWebGLCubeRenderTarget)throw new Error("Depth Texture with cube render targets is not supported");if(t.bindFramebuffer(s.FRAMEBUFFER,B),!(I.depthTexture&&I.depthTexture.isDepthTexture))throw new Error("renderTarget.depthTexture must be an instance of THREE.DepthTexture");(!n.get(I.depthTexture).__webglTexture||I.depthTexture.image.width!==I.width||I.depthTexture.image.height!==I.height)&&(I.depthTexture.image.width=I.width,I.depthTexture.image.height=I.height,I.depthTexture.needsUpdate=!0),H(I.depthTexture,0);const ue=n.get(I.depthTexture).__webglTexture,_e=he(I);if(I.depthTexture.format===Mr)Ae(I)?a.framebufferTexture2DMultisampleEXT(s.FRAMEBUFFER,s.DEPTH_ATTACHMENT,s.TEXTURE_2D,ue,0,_e):s.framebufferTexture2D(s.FRAMEBUFFER,s.DEPTH_ATTACHMENT,s.TEXTURE_2D,ue,0);else if(I.depthTexture.format===as)Ae(I)?a.framebufferTexture2DMultisampleEXT(s.FRAMEBUFFER,s.DEPTH_STENCIL_ATTACHMENT,s.TEXTURE_2D,ue,0,_e):s.framebufferTexture2D(s.FRAMEBUFFER,s.DEPTH_STENCIL_ATTACHMENT,s.TEXTURE_2D,ue,0);else throw new Error("Unknown depthTexture format")}function Ee(B){const I=n.get(B),$=B.isWebGLCubeRenderTarget===!0;if(B.depthTexture&&!I.__autoAllocateDepthBuffer){if($)throw new Error("target.depthTexture not supported in Cube render targets");ke(I.__webglFramebuffer,B)}else if($){I.__webglDepthbuffer=[];for(let ue=0;ue<6;ue++)t.bindFramebuffer(s.FRAMEBUFFER,I.__webglFramebuffer[ue]),I.__webglDepthbuffer[ue]=s.createRenderbuffer(),we(I.__webglDepthbuffer[ue],B,!1)}else t.bindFramebuffer(s.FRAMEBUFFER,I.__webglFramebuffer),I.__webglDepthbuffer=s.createRenderbuffer(),we(I.__webglDepthbuffer,B,!1);t.bindFramebuffer(s.FRAMEBUFFER,null)}function nt(B,I,$){const ue=n.get(B);I!==void 0&&ae(ue.__webglFramebuffer,B,B.texture,s.COLOR_ATTACHMENT0,s.TEXTURE_2D,0),$!==void 0&&Ee(B)}function be(B){const I=B.texture,$=n.get(B),ue=n.get(I);B.addEventListener("dispose",z);const _e=B.textures,me=B.isWebGLCubeRenderTarget===!0,Ye=_e.length>1;if(Ye||(ue.__webglTexture===void 0&&(ue.__webglTexture=s.createTexture()),ue.__version=I.version,o.memory.textures++),me){$.__webglFramebuffer=[];for(let Re=0;Re<6;Re++)if(I.mipmaps&&I.mipmaps.length>0){$.__webglFramebuffer[Re]=[];for(let Le=0;Le<I.mipmaps.length;Le++)$.__webglFramebuffer[Re][Le]=s.createFramebuffer()}else $.__webglFramebuffer[Re]=s.createFramebuffer()}else{if(I.mipmaps&&I.mipmaps.length>0){$.__webglFramebuffer=[];for(let Re=0;Re<I.mipmaps.length;Re++)$.__webglFramebuffer[Re]=s.createFramebuffer()}else $.__webglFramebuffer=s.createFramebuffer();if(Ye)for(let Re=0,Le=_e.length;Re<Le;Re++){const tt=n.get(_e[Re]);tt.__webglTexture===void 0&&(tt.__webglTexture=s.createTexture(),o.memory.textures++)}if(B.samples>0&&Ae(B)===!1){$.__webglMultisampledFramebuffer=s.createFramebuffer(),$.__webglColorRenderbuffer=[],t.bindFramebuffer(s.FRAMEBUFFER,$.__webglMultisampledFramebuffer);for(let Re=0;Re<_e.length;Re++){const Le=_e[Re];$.__webglColorRenderbuffer[Re]=s.createRenderbuffer(),s.bindRenderbuffer(s.RENDERBUFFER,$.__webglColorRenderbuffer[Re]);const tt=r.convert(Le.format,Le.colorSpace),Ce=r.convert(Le.type),Xe=w(Le.internalFormat,tt,Ce,Le.colorSpace,B.isXRRenderTarget===!0),lt=he(B);s.renderbufferStorageMultisample(s.RENDERBUFFER,lt,Xe,B.width,B.height),s.framebufferRenderbuffer(s.FRAMEBUFFER,s.COLOR_ATTACHMENT0+Re,s.RENDERBUFFER,$.__webglColorRenderbuffer[Re])}s.bindRenderbuffer(s.RENDERBUFFER,null),B.depthBuffer&&($.__webglDepthRenderbuffer=s.createRenderbuffer(),we($.__webglDepthRenderbuffer,B,!0)),t.bindFramebuffer(s.FRAMEBUFFER,null)}}if(me){t.bindTexture(s.TEXTURE_CUBE_MAP,ue.__webglTexture),ve(s.TEXTURE_CUBE_MAP,I);for(let Re=0;Re<6;Re++)if(I.mipmaps&&I.mipmaps.length>0)for(let Le=0;Le<I.mipmaps.length;Le++)ae($.__webglFramebuffer[Re][Le],B,I,s.COLOR_ATTACHMENT0,s.TEXTURE_CUBE_MAP_POSITIVE_X+Re,Le);else ae($.__webglFramebuffer[Re],B,I,s.COLOR_ATTACHMENT0,s.TEXTURE_CUBE_MAP_POSITIVE_X+Re,0);y(I)&&v(s.TEXTURE_CUBE_MAP),t.unbindTexture()}else if(Ye){for(let Re=0,Le=_e.length;Re<Le;Re++){const tt=_e[Re],Ce=n.get(tt);t.bindTexture(s.TEXTURE_2D,Ce.__webglTexture),ve(s.TEXTURE_2D,tt),ae($.__webglFramebuffer,B,tt,s.COLOR_ATTACHMENT0+Re,s.TEXTURE_2D,0),y(tt)&&v(s.TEXTURE_2D)}t.unbindTexture()}else{let Re=s.TEXTURE_2D;if((B.isWebGL3DRenderTarget||B.isWebGLArrayRenderTarget)&&(Re=B.isWebGL3DRenderTarget?s.TEXTURE_3D:s.TEXTURE_2D_ARRAY),t.bindTexture(Re,ue.__webglTexture),ve(Re,I),I.mipmaps&&I.mipmaps.length>0)for(let Le=0;Le<I.mipmaps.length;Le++)ae($.__webglFramebuffer[Le],B,I,s.COLOR_ATTACHMENT0,Re,Le);else ae($.__webglFramebuffer,B,I,s.COLOR_ATTACHMENT0,Re,0);y(I)&&v(Re),t.unbindTexture()}B.depthBuffer&&Ee(B)}function Z(B){const I=B.textures;for(let $=0,ue=I.length;$<ue;$++){const _e=I[$];if(y(_e)){const me=B.isWebGLCubeRenderTarget?s.TEXTURE_CUBE_MAP:s.TEXTURE_2D,Ye=n.get(_e).__webglTexture;t.bindTexture(me,Ye),v(me),t.unbindTexture()}}}const st=[],le=[];function xe(B){if(B.samples>0){if(Ae(B)===!1){const I=B.textures,$=B.width,ue=B.height;let _e=s.COLOR_BUFFER_BIT;const me=B.stencilBuffer?s.DEPTH_STENCIL_ATTACHMENT:s.DEPTH_ATTACHMENT,Ye=n.get(B),Re=I.length>1;if(Re)for(let Le=0;Le<I.length;Le++)t.bindFramebuffer(s.FRAMEBUFFER,Ye.__webglMultisampledFramebuffer),s.framebufferRenderbuffer(s.FRAMEBUFFER,s.COLOR_ATTACHMENT0+Le,s.RENDERBUFFER,null),t.bindFramebuffer(s.FRAMEBUFFER,Ye.__webglFramebuffer),s.framebufferTexture2D(s.DRAW_FRAMEBUFFER,s.COLOR_ATTACHMENT0+Le,s.TEXTURE_2D,null,0);t.bindFramebuffer(s.READ_FRAMEBUFFER,Ye.__webglMultisampledFramebuffer),t.bindFramebuffer(s.DRAW_FRAMEBUFFER,Ye.__webglFramebuffer);for(let Le=0;Le<I.length;Le++){if(B.resolveDepthBuffer&&(B.depthBuffer&&(_e|=s.DEPTH_BUFFER_BIT),B.stencilBuffer&&B.resolveStencilBuffer&&(_e|=s.STENCIL_BUFFER_BIT)),Re){s.framebufferRenderbuffer(s.READ_FRAMEBUFFER,s.COLOR_ATTACHMENT0,s.RENDERBUFFER,Ye.__webglColorRenderbuffer[Le]);const tt=n.get(I[Le]).__webglTexture;s.framebufferTexture2D(s.DRAW_FRAMEBUFFER,s.COLOR_ATTACHMENT0,s.TEXTURE_2D,tt,0)}s.blitFramebuffer(0,0,$,ue,0,0,$,ue,_e,s.NEAREST),l===!0&&(st.length=0,le.length=0,st.push(s.COLOR_ATTACHMENT0+Le),B.depthBuffer&&B.resolveDepthBuffer===!1&&(st.push(me),le.push(me),s.invalidateFramebuffer(s.DRAW_FRAMEBUFFER,le)),s.invalidateFramebuffer(s.READ_FRAMEBUFFER,st))}if(t.bindFramebuffer(s.READ_FRAMEBUFFER,null),t.bindFramebuffer(s.DRAW_FRAMEBUFFER,null),Re)for(let Le=0;Le<I.length;Le++){t.bindFramebuffer(s.FRAMEBUFFER,Ye.__webglMultisampledFramebuffer),s.framebufferRenderbuffer(s.FRAMEBUFFER,s.COLOR_ATTACHMENT0+Le,s.RENDERBUFFER,Ye.__webglColorRenderbuffer[Le]);const tt=n.get(I[Le]).__webglTexture;t.bindFramebuffer(s.FRAMEBUFFER,Ye.__webglFramebuffer),s.framebufferTexture2D(s.DRAW_FRAMEBUFFER,s.COLOR_ATTACHMENT0+Le,s.TEXTURE_2D,tt,0)}t.bindFramebuffer(s.DRAW_FRAMEBUFFER,Ye.__webglMultisampledFramebuffer)}else if(B.depthBuffer&&B.resolveDepthBuffer===!1&&l){const I=B.stencilBuffer?s.DEPTH_STENCIL_ATTACHMENT:s.DEPTH_ATTACHMENT;s.invalidateFramebuffer(s.DRAW_FRAMEBUFFER,[I])}}}function he(B){return Math.min(i.maxSamples,B.samples)}function Ae(B){const I=n.get(B);return B.samples>0&&e.has("WEBGL_multisampled_render_to_texture")===!0&&I.__useRenderToTexture!==!1}function ge(B){const I=o.render.frame;d.get(B)!==I&&(d.set(B,I),B.update())}function Fe(B,I){const $=B.colorSpace,ue=B.format,_e=B.type;return B.isCompressedTexture===!0||B.isVideoTexture===!0||$!==wi&&$!==pi&&(wt.getTransfer($)===It?(ue!==Cn||_e!==bi)&&console.warn("THREE.WebGLTextures: sRGB encoded textures have to use RGBAFormat and UnsignedByteType."):console.error("THREE.WebGLTextures: Unsupported texture color space:",$)),I}function Ge(B){return typeof HTMLImageElement<"u"&&B instanceof HTMLImageElement?(u.width=B.naturalWidth||B.width,u.height=B.naturalHeight||B.height):typeof VideoFrame<"u"&&B instanceof VideoFrame?(u.width=B.displayWidth,u.height=B.displayHeight):(u.width=B.width,u.height=B.height),u}this.allocateTextureUnit=X,this.resetTextureUnits=C,this.setTexture2D=H,this.setTexture2DArray=K,this.setTexture3D=J,this.setTextureCube=ce,this.rebindTextures=nt,this.setupRenderTarget=be,this.updateRenderTargetMipmap=Z,this.updateMultisampleRenderTarget=xe,this.setupDepthRenderbuffer=Ee,this.setupFrameBufferTexture=ae,this.useMultisampledRTT=Ae}function Pp(s,e){function t(n,i=pi){let r;const o=wt.getTransfer(i);if(n===bi)return s.UNSIGNED_BYTE;if(n===fh)return s.UNSIGNED_SHORT_4_4_4_4;if(n===ph)return s.UNSIGNED_SHORT_5_5_5_1;if(n===jf)return s.UNSIGNED_INT_5_9_9_9_REV;if(n===qf)return s.BYTE;if(n===Zf)return s.SHORT;if(n===uh)return s.UNSIGNED_SHORT;if(n===dh)return s.INT;if(n===wr)return s.UNSIGNED_INT;if(n===zn)return s.FLOAT;if(n===Fn)return s.HALF_FLOAT;if(n===Kf)return s.ALPHA;if(n===Jf)return s.RGB;if(n===Cn)return s.RGBA;if(n===$f)return s.LUMINANCE;if(n===Qf)return s.LUMINANCE_ALPHA;if(n===Mr)return s.DEPTH_COMPONENT;if(n===as)return s.DEPTH_STENCIL;if(n===mh)return s.RED;if(n===gh)return s.RED_INTEGER;if(n===ep)return s.RG;if(n===vh)return s.RG_INTEGER;if(n===_h)return s.RGBA_INTEGER;if(n===Pa||n===Ia||n===La||n===Da)if(o===It)if(r=e.get("WEBGL_compressed_texture_s3tc_srgb"),r!==null){if(n===Pa)return r.COMPRESSED_SRGB_S3TC_DXT1_EXT;if(n===Ia)return r.COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT;if(n===La)return r.COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT;if(n===Da)return r.COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT}else return null;else if(r=e.get("WEBGL_compressed_texture_s3tc"),r!==null){if(n===Pa)return r.COMPRESSED_RGB_S3TC_DXT1_EXT;if(n===Ia)return r.COMPRESSED_RGBA_S3TC_DXT1_EXT;if(n===La)return r.COMPRESSED_RGBA_S3TC_DXT3_EXT;if(n===Da)return r.COMPRESSED_RGBA_S3TC_DXT5_EXT}else return null;if(n===xc||n===Mc||n===bc||n===Sc)if(r=e.get("WEBGL_compressed_texture_pvrtc"),r!==null){if(n===xc)return r.COMPRESSED_RGB_PVRTC_4BPPV1_IMG;if(n===Mc)return r.COMPRESSED_RGB_PVRTC_2BPPV1_IMG;if(n===bc)return r.COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;if(n===Sc)return r.COMPRESSED_RGBA_PVRTC_2BPPV1_IMG}else return null;if(n===wc||n===Ec||n===Tc)if(r=e.get("WEBGL_compressed_texture_etc"),r!==null){if(n===wc||n===Ec)return o===It?r.COMPRESSED_SRGB8_ETC2:r.COMPRESSED_RGB8_ETC2;if(n===Tc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ETC2_EAC:r.COMPRESSED_RGBA8_ETC2_EAC}else return null;if(n===Ac||n===Cc||n===Rc||n===Pc||n===Ic||n===Lc||n===Dc||n===Nc||n===Oc||n===Uc||n===Fc||n===Bc||n===zc||n===kc)if(r=e.get("WEBGL_compressed_texture_astc"),r!==null){if(n===Ac)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_4x4_KHR:r.COMPRESSED_RGBA_ASTC_4x4_KHR;if(n===Cc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_5x4_KHR:r.COMPRESSED_RGBA_ASTC_5x4_KHR;if(n===Rc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_5x5_KHR:r.COMPRESSED_RGBA_ASTC_5x5_KHR;if(n===Pc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_6x5_KHR:r.COMPRESSED_RGBA_ASTC_6x5_KHR;if(n===Ic)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_6x6_KHR:r.COMPRESSED_RGBA_ASTC_6x6_KHR;if(n===Lc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_8x5_KHR:r.COMPRESSED_RGBA_ASTC_8x5_KHR;if(n===Dc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_8x6_KHR:r.COMPRESSED_RGBA_ASTC_8x6_KHR;if(n===Nc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_8x8_KHR:r.COMPRESSED_RGBA_ASTC_8x8_KHR;if(n===Oc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_10x5_KHR:r.COMPRESSED_RGBA_ASTC_10x5_KHR;if(n===Uc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_10x6_KHR:r.COMPRESSED_RGBA_ASTC_10x6_KHR;if(n===Fc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_10x8_KHR:r.COMPRESSED_RGBA_ASTC_10x8_KHR;if(n===Bc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_10x10_KHR:r.COMPRESSED_RGBA_ASTC_10x10_KHR;if(n===zc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_12x10_KHR:r.COMPRESSED_RGBA_ASTC_12x10_KHR;if(n===kc)return o===It?r.COMPRESSED_SRGB8_ALPHA8_ASTC_12x12_KHR:r.COMPRESSED_RGBA_ASTC_12x12_KHR}else return null;if(n===Na||n===Vc||n===Hc)if(r=e.get("EXT_texture_compression_bptc"),r!==null){if(n===Na)return o===It?r.COMPRESSED_SRGB_ALPHA_BPTC_UNORM_EXT:r.COMPRESSED_RGBA_BPTC_UNORM_EXT;if(n===Vc)return r.COMPRESSED_RGB_BPTC_SIGNED_FLOAT_EXT;if(n===Hc)return r.COMPRESSED_RGB_BPTC_UNSIGNED_FLOAT_EXT}else return null;if(n===tp||n===Gc||n===Wc||n===Xc)if(r=e.get("EXT_texture_compression_rgtc"),r!==null){if(n===Na)return r.COMPRESSED_RED_RGTC1_EXT;if(n===Gc)return r.COMPRESSED_SIGNED_RED_RGTC1_EXT;if(n===Wc)return r.COMPRESSED_RED_GREEN_RGTC2_EXT;if(n===Xc)return r.COMPRESSED_SIGNED_RED_GREEN_RGTC2_EXT}else return null;return n===ds?s.UNSIGNED_INT_24_8:s[n]!==void 0?s[n]:null}return{convert:t}}class Ip extends en{constructor(e=[]){super(),this.isArrayCamera=!0,this.cameras=e}}class rs extends xt{constructor(){super(),this.isGroup=!0,this.type="Group"}}const xM={type:"move"};class $l{constructor(){this._targetRay=null,this._grip=null,this._hand=null}getHandSpace(){return this._hand===null&&(this._hand=new rs,this._hand.matrixAutoUpdate=!1,this._hand.visible=!1,this._hand.joints={},this._hand.inputState={pinching:!1}),this._hand}getTargetRaySpace(){return this._targetRay===null&&(this._targetRay=new rs,this._targetRay.matrixAutoUpdate=!1,this._targetRay.visible=!1,this._targetRay.hasLinearVelocity=!1,this._targetRay.linearVelocity=new O,this._targetRay.hasAngularVelocity=!1,this._targetRay.angularVelocity=new O),this._targetRay}getGripSpace(){return this._grip===null&&(this._grip=new rs,this._grip.matrixAutoUpdate=!1,this._grip.visible=!1,this._grip.hasLinearVelocity=!1,this._grip.linearVelocity=new O,this._grip.hasAngularVelocity=!1,this._grip.angularVelocity=new O),this._grip}dispatchEvent(e){return this._targetRay!==null&&this._targetRay.dispatchEvent(e),this._grip!==null&&this._grip.dispatchEvent(e),this._hand!==null&&this._hand.dispatchEvent(e),this}connect(e){if(e&&e.hand){const t=this._hand;if(t)for(const n of e.hand.values())this._getHandJoint(t,n)}return this.dispatchEvent({type:"connected",data:e}),this}disconnect(e){return this.dispatchEvent({type:"disconnected",data:e}),this._targetRay!==null&&(this._targetRay.visible=!1),this._grip!==null&&(this._grip.visible=!1),this._hand!==null&&(this._hand.visible=!1),this}update(e,t,n){let i=null,r=null,o=null;const a=this._targetRay,l=this._grip,u=this._hand;if(e&&t.session.visibilityState!=="visible-blurred"){if(u&&e.hand){o=!0;for(const M of e.hand.values()){const y=t.getJointPose(M,n),v=this._getHandJoint(u,M);y!==null&&(v.matrix.fromArray(y.transform.matrix),v.matrix.decompose(v.position,v.rotation,v.scale),v.matrixWorldNeedsUpdate=!0,v.jointRadius=y.radius),v.visible=y!==null}const d=u.joints["index-finger-tip"],m=u.joints["thumb-tip"],p=d.position.distanceTo(m.position),g=.02,_=.005;u.inputState.pinching&&p>g+_?(u.inputState.pinching=!1,this.dispatchEvent({type:"pinchend",handedness:e.handedness,target:this})):!u.inputState.pinching&&p<=g-_&&(u.inputState.pinching=!0,this.dispatchEvent({type:"pinchstart",handedness:e.handedness,target:this}))}else l!==null&&e.gripSpace&&(r=t.getPose(e.gripSpace,n),r!==null&&(l.matrix.fromArray(r.transform.matrix),l.matrix.decompose(l.position,l.rotation,l.scale),l.matrixWorldNeedsUpdate=!0,r.linearVelocity?(l.hasLinearVelocity=!0,l.linearVelocity.copy(r.linearVelocity)):l.hasLinearVelocity=!1,r.angularVelocity?(l.hasAngularVelocity=!0,l.angularVelocity.copy(r.angularVelocity)):l.hasAngularVelocity=!1));a!==null&&(i=t.getPose(e.targetRaySpace,n),i===null&&r!==null&&(i=r),i!==null&&(a.matrix.fromArray(i.transform.matrix),a.matrix.decompose(a.position,a.rotation,a.scale),a.matrixWorldNeedsUpdate=!0,i.linearVelocity?(a.hasLinearVelocity=!0,a.linearVelocity.copy(i.linearVelocity)):a.hasLinearVelocity=!1,i.angularVelocity?(a.hasAngularVelocity=!0,a.angularVelocity.copy(i.angularVelocity)):a.hasAngularVelocity=!1,this.dispatchEvent(xM)))}return a!==null&&(a.visible=i!==null),l!==null&&(l.visible=r!==null),u!==null&&(u.visible=o!==null),this}_getHandJoint(e,t){if(e.joints[t.jointName]===void 0){const n=new rs;n.matrixAutoUpdate=!1,n.visible=!1,e.joints[t.jointName]=n,e.add(n)}return e.joints[t.jointName]}}const MM=`
void main() {

	gl_Position = vec4( position, 1.0 );

}`,bM=`
uniform sampler2DArray depthColor;
uniform float depthWidth;
uniform float depthHeight;

void main() {

	vec2 coord = vec2( gl_FragCoord.x / depthWidth, gl_FragCoord.y / depthHeight );

	if ( coord.x >= 1.0 ) {

		gl_FragDepth = texture( depthColor, vec3( coord.x - 1.0, coord.y, 1 ) ).r;

	} else {

		gl_FragDepth = texture( depthColor, vec3( coord.x, coord.y, 0 ) ).r;

	}

}`;class SM{constructor(){this.texture=null,this.mesh=null,this.depthNear=0,this.depthFar=0}init(e,t,n){if(this.texture===null){const i=new Vt,r=e.properties.get(i);r.__webglTexture=t.texture,(t.depthNear!=n.depthNear||t.depthFar!=n.depthFar)&&(this.depthNear=t.depthNear,this.depthFar=t.depthFar),this.texture=i}}render(e,t){if(this.texture!==null){if(this.mesh===null){const n=t.cameras[0].viewport,i=new tn({vertexShader:MM,fragmentShader:bM,uniforms:{depthColor:{value:this.texture},depthWidth:{value:n.z},depthHeight:{value:n.w}}});this.mesh=new Ie(new Cr(20,20),i)}e.render(this.mesh,t)}}reset(){this.texture=null,this.mesh=null}}class wM extends ni{constructor(e,t){super();const n=this;let i=null,r=1,o=null,a="local-floor",l=1,u=null,d=null,m=null,p=null,g=null,_=null;const M=new SM,y=t.getContextAttributes();let v=null,w=null;const S=[],T=[],z=new se;let N=null;const A=new en;A.layers.enable(1),A.viewport=new Ct;const k=new en;k.layers.enable(2),k.viewport=new Ct;const D=[A,k],C=new Ip;C.layers.enable(1),C.layers.enable(2);let X=null,Y=null;this.cameraAutoUpdate=!0,this.enabled=!1,this.isPresenting=!1,this.getController=function(ae){let we=S[ae];return we===void 0&&(we=new $l,S[ae]=we),we.getTargetRaySpace()},this.getControllerGrip=function(ae){let we=S[ae];return we===void 0&&(we=new $l,S[ae]=we),we.getGripSpace()},this.getHand=function(ae){let we=S[ae];return we===void 0&&(we=new $l,S[ae]=we),we.getHandSpace()};function H(ae){const we=T.indexOf(ae.inputSource);if(we===-1)return;const ke=S[we];ke!==void 0&&(ke.update(ae.inputSource,ae.frame,u||o),ke.dispatchEvent({type:ae.type,data:ae.inputSource}))}function K(){i.removeEventListener("select",H),i.removeEventListener("selectstart",H),i.removeEventListener("selectend",H),i.removeEventListener("squeeze",H),i.removeEventListener("squeezestart",H),i.removeEventListener("squeezeend",H),i.removeEventListener("end",K),i.removeEventListener("inputsourceschange",J);for(let ae=0;ae<S.length;ae++){const we=T[ae];we!==null&&(T[ae]=null,S[ae].disconnect(we))}X=null,Y=null,M.reset(),e.setRenderTarget(v),g=null,p=null,m=null,i=null,w=null,vt.stop(),n.isPresenting=!1,e.setPixelRatio(N),e.setSize(z.width,z.height,!1),n.dispatchEvent({type:"sessionend"})}this.setFramebufferScaleFactor=function(ae){r=ae,n.isPresenting===!0&&console.warn("THREE.WebXRManager: Cannot change framebuffer scale while presenting.")},this.setReferenceSpaceType=function(ae){a=ae,n.isPresenting===!0&&console.warn("THREE.WebXRManager: Cannot change reference space type while presenting.")},this.getReferenceSpace=function(){return u||o},this.setReferenceSpace=function(ae){u=ae},this.getBaseLayer=function(){return p!==null?p:g},this.getBinding=function(){return m},this.getFrame=function(){return _},this.getSession=function(){return i},this.setSession=async function(ae){if(i=ae,i!==null){if(v=e.getRenderTarget(),i.addEventListener("select",H),i.addEventListener("selectstart",H),i.addEventListener("selectend",H),i.addEventListener("squeeze",H),i.addEventListener("squeezestart",H),i.addEventListener("squeezeend",H),i.addEventListener("end",K),i.addEventListener("inputsourceschange",J),y.xrCompatible!==!0&&await t.makeXRCompatible(),N=e.getPixelRatio(),e.getSize(z),i.renderState.layers===void 0){const we={antialias:y.antialias,alpha:!0,depth:y.depth,stencil:y.stencil,framebufferScaleFactor:r};g=new XRWebGLLayer(i,t,we),i.updateRenderState({baseLayer:g}),e.setPixelRatio(1),e.setSize(g.framebufferWidth,g.framebufferHeight,!1),w=new qt(g.framebufferWidth,g.framebufferHeight,{format:Cn,type:bi,colorSpace:e.outputColorSpace,stencilBuffer:y.stencil})}else{let we=null,ke=null,Ee=null;y.depth&&(Ee=y.stencil?t.DEPTH24_STENCIL8:t.DEPTH_COMPONENT24,we=y.stencil?as:Mr,ke=y.stencil?ds:wr);const nt={colorFormat:t.RGBA8,depthFormat:Ee,scaleFactor:r};m=new XRWebGLBinding(i,t),p=m.createProjectionLayer(nt),i.updateRenderState({layers:[p]}),e.setPixelRatio(1),e.setSize(p.textureWidth,p.textureHeight,!1),w=new qt(p.textureWidth,p.textureHeight,{format:Cn,type:bi,depthTexture:new Ah(p.textureWidth,p.textureHeight,ke,void 0,void 0,void 0,void 0,void 0,void 0,we),stencilBuffer:y.stencil,colorSpace:e.outputColorSpace,samples:y.antialias?4:0,resolveDepthBuffer:p.ignoreDepthValues===!1})}w.isXRRenderTarget=!0,this.setFoveation(l),u=null,o=await i.requestReferenceSpace(a),vt.setContext(i),vt.start(),n.isPresenting=!0,n.dispatchEvent({type:"sessionstart"})}},this.getEnvironmentBlendMode=function(){if(i!==null)return i.environmentBlendMode};function J(ae){for(let we=0;we<ae.removed.length;we++){const ke=ae.removed[we],Ee=T.indexOf(ke);Ee>=0&&(T[Ee]=null,S[Ee].disconnect(ke))}for(let we=0;we<ae.added.length;we++){const ke=ae.added[we];let Ee=T.indexOf(ke);if(Ee===-1){for(let be=0;be<S.length;be++)if(be>=T.length){T.push(ke),Ee=be;break}else if(T[be]===null){T[be]=ke,Ee=be;break}if(Ee===-1)break}const nt=S[Ee];nt&&nt.connect(ke)}}const ce=new O,de=new O;function Q(ae,we,ke){ce.setFromMatrixPosition(we.matrixWorld),de.setFromMatrixPosition(ke.matrixWorld);const Ee=ce.distanceTo(de),nt=we.projectionMatrix.elements,be=ke.projectionMatrix.elements,Z=nt[14]/(nt[10]-1),st=nt[14]/(nt[10]+1),le=(nt[9]+1)/nt[5],xe=(nt[9]-1)/nt[5],he=(nt[8]-1)/nt[0],Ae=(be[8]+1)/be[0],ge=Z*he,Fe=Z*Ae,Ge=Ee/(-he+Ae),B=Ge*-he;we.matrixWorld.decompose(ae.position,ae.quaternion,ae.scale),ae.translateX(B),ae.translateZ(Ge),ae.matrixWorld.compose(ae.position,ae.quaternion,ae.scale),ae.matrixWorldInverse.copy(ae.matrixWorld).invert();const I=Z+Ge,$=st+Ge,ue=ge-B,_e=Fe+(Ee-B),me=le*st/$*I,Ye=xe*st/$*I;ae.projectionMatrix.makePerspective(ue,_e,me,Ye,I,$),ae.projectionMatrixInverse.copy(ae.projectionMatrix).invert()}function pe(ae,we){we===null?ae.matrixWorld.copy(ae.matrix):ae.matrixWorld.multiplyMatrices(we.matrixWorld,ae.matrix),ae.matrixWorldInverse.copy(ae.matrixWorld).invert()}this.updateCamera=function(ae){if(i===null)return;M.texture!==null&&(ae.near=M.depthNear,ae.far=M.depthFar),C.near=k.near=A.near=ae.near,C.far=k.far=A.far=ae.far,(X!==C.near||Y!==C.far)&&(i.updateRenderState({depthNear:C.near,depthFar:C.far}),X=C.near,Y=C.far,A.near=X,A.far=Y,k.near=X,k.far=Y,A.updateProjectionMatrix(),k.updateProjectionMatrix(),ae.updateProjectionMatrix());const we=ae.parent,ke=C.cameras;pe(C,we);for(let Ee=0;Ee<ke.length;Ee++)pe(ke[Ee],we);ke.length===2?Q(C,A,k):C.projectionMatrix.copy(A.projectionMatrix),ve(ae,C,we)};function ve(ae,we,ke){ke===null?ae.matrix.copy(we.matrixWorld):(ae.matrix.copy(ke.matrixWorld),ae.matrix.invert(),ae.matrix.multiply(we.matrixWorld)),ae.matrix.decompose(ae.position,ae.quaternion,ae.scale),ae.updateMatrixWorld(!0),ae.projectionMatrix.copy(we.projectionMatrix),ae.projectionMatrixInverse.copy(we.projectionMatrixInverse),ae.isPerspectiveCamera&&(ae.fov=ls*2*Math.atan(1/ae.projectionMatrix.elements[5]),ae.zoom=1)}this.getCamera=function(){return C},this.getFoveation=function(){if(!(p===null&&g===null))return l},this.setFoveation=function(ae){l=ae,p!==null&&(p.fixedFoveation=ae),g!==null&&g.fixedFoveation!==void 0&&(g.fixedFoveation=ae)},this.hasDepthSensing=function(){return M.texture!==null};let Ue=null;function Je(ae,we){if(d=we.getViewerPose(u||o),_=we,d!==null){const ke=d.views;g!==null&&(e.setRenderTargetFramebuffer(w,g.framebuffer),e.setRenderTarget(w));let Ee=!1;ke.length!==C.cameras.length&&(C.cameras.length=0,Ee=!0);for(let be=0;be<ke.length;be++){const Z=ke[be];let st=null;if(g!==null)st=g.getViewport(Z);else{const xe=m.getViewSubImage(p,Z);st=xe.viewport,be===0&&(e.setRenderTargetTextures(w,xe.colorTexture,p.ignoreDepthValues?void 0:xe.depthStencilTexture),e.setRenderTarget(w))}let le=D[be];le===void 0&&(le=new en,le.layers.enable(be),le.viewport=new Ct,D[be]=le),le.matrix.fromArray(Z.transform.matrix),le.matrix.decompose(le.position,le.quaternion,le.scale),le.projectionMatrix.fromArray(Z.projectionMatrix),le.projectionMatrixInverse.copy(le.projectionMatrix).invert(),le.viewport.set(st.x,st.y,st.width,st.height),be===0&&(C.matrix.copy(le.matrix),C.matrix.decompose(C.position,C.quaternion,C.scale)),Ee===!0&&C.cameras.push(le)}const nt=i.enabledFeatures;if(nt&&nt.includes("depth-sensing")){const be=m.getDepthInformation(ke[0]);be&&be.isValid&&be.texture&&M.init(e,be,i.renderState)}}for(let ke=0;ke<S.length;ke++){const Ee=T[ke],nt=S[ke];Ee!==null&&nt!==void 0&&nt.update(Ee,we,u||o)}M.render(e,C),Ue&&Ue(ae,we),we.detectedPlanes&&n.dispatchEvent({type:"planesdetected",data:we}),_=null}const vt=new wp;vt.setAnimationLoop(Je),this.setAnimationLoop=function(ae){Ue=ae},this.dispose=function(){}}}const nr=new xn,EM=new Ke;function TM(s,e){function t(y,v){y.matrixAutoUpdate===!0&&y.updateMatrix(),v.value.copy(y.matrix)}function n(y,v){v.color.getRGB(y.fogColor.value,Mp(s)),v.isFog?(y.fogNear.value=v.near,y.fogFar.value=v.far):v.isFogExp2&&(y.fogDensity.value=v.density)}function i(y,v,w,S,T){v.isMeshBasicMaterial||v.isMeshLambertMaterial?r(y,v):v.isMeshToonMaterial?(r(y,v),m(y,v)):v.isMeshPhongMaterial?(r(y,v),d(y,v)):v.isMeshStandardMaterial?(r(y,v),p(y,v),v.isMeshPhysicalMaterial&&g(y,v,T)):v.isMeshMatcapMaterial?(r(y,v),_(y,v)):v.isMeshDepthMaterial?r(y,v):v.isMeshDistanceMaterial?(r(y,v),M(y,v)):v.isMeshNormalMaterial?r(y,v):v.isLineBasicMaterial?(o(y,v),v.isLineDashedMaterial&&a(y,v)):v.isPointsMaterial?l(y,v,w,S):v.isSpriteMaterial?u(y,v):v.isShadowMaterial?(y.color.value.copy(v.color),y.opacity.value=v.opacity):v.isShaderMaterial&&(v.uniformsNeedUpdate=!1)}function r(y,v){y.opacity.value=v.opacity,v.color&&y.diffuse.value.copy(v.color),v.emissive&&y.emissive.value.copy(v.emissive).multiplyScalar(v.emissiveIntensity),v.map&&(y.map.value=v.map,t(v.map,y.mapTransform)),v.alphaMap&&(y.alphaMap.value=v.alphaMap,t(v.alphaMap,y.alphaMapTransform)),v.bumpMap&&(y.bumpMap.value=v.bumpMap,t(v.bumpMap,y.bumpMapTransform),y.bumpScale.value=v.bumpScale,v.side===_n&&(y.bumpScale.value*=-1)),v.normalMap&&(y.normalMap.value=v.normalMap,t(v.normalMap,y.normalMapTransform),y.normalScale.value.copy(v.normalScale),v.side===_n&&y.normalScale.value.negate()),v.displacementMap&&(y.displacementMap.value=v.displacementMap,t(v.displacementMap,y.displacementMapTransform),y.displacementScale.value=v.displacementScale,y.displacementBias.value=v.displacementBias),v.emissiveMap&&(y.emissiveMap.value=v.emissiveMap,t(v.emissiveMap,y.emissiveMapTransform)),v.specularMap&&(y.specularMap.value=v.specularMap,t(v.specularMap,y.specularMapTransform)),v.alphaTest>0&&(y.alphaTest.value=v.alphaTest);const w=e.get(v),S=w.envMap,T=w.envMapRotation;if(S&&(y.envMap.value=S,nr.copy(T),nr.x*=-1,nr.y*=-1,nr.z*=-1,S.isCubeTexture&&S.isRenderTargetTexture===!1&&(nr.y*=-1,nr.z*=-1),y.envMapRotation.value.setFromMatrix4(EM.makeRotationFromEuler(nr)),y.flipEnvMap.value=S.isCubeTexture&&S.isRenderTargetTexture===!1?-1:1,y.reflectivity.value=v.reflectivity,y.ior.value=v.ior,y.refractionRatio.value=v.refractionRatio),v.lightMap){y.lightMap.value=v.lightMap;const z=s._useLegacyLights===!0?Math.PI:1;y.lightMapIntensity.value=v.lightMapIntensity*z,t(v.lightMap,y.lightMapTransform)}v.aoMap&&(y.aoMap.value=v.aoMap,y.aoMapIntensity.value=v.aoMapIntensity,t(v.aoMap,y.aoMapTransform))}function o(y,v){y.diffuse.value.copy(v.color),y.opacity.value=v.opacity,v.map&&(y.map.value=v.map,t(v.map,y.mapTransform))}function a(y,v){y.dashSize.value=v.dashSize,y.totalSize.value=v.dashSize+v.gapSize,y.scale.value=v.scale}function l(y,v,w,S){y.diffuse.value.copy(v.color),y.opacity.value=v.opacity,y.size.value=v.size*w,y.scale.value=S*.5,v.map&&(y.map.value=v.map,t(v.map,y.uvTransform)),v.alphaMap&&(y.alphaMap.value=v.alphaMap,t(v.alphaMap,y.alphaMapTransform)),v.alphaTest>0&&(y.alphaTest.value=v.alphaTest)}function u(y,v){y.diffuse.value.copy(v.color),y.opacity.value=v.opacity,y.rotation.value=v.rotation,v.map&&(y.map.value=v.map,t(v.map,y.mapTransform)),v.alphaMap&&(y.alphaMap.value=v.alphaMap,t(v.alphaMap,y.alphaMapTransform)),v.alphaTest>0&&(y.alphaTest.value=v.alphaTest)}function d(y,v){y.specular.value.copy(v.specular),y.shininess.value=Math.max(v.shininess,1e-4)}function m(y,v){v.gradientMap&&(y.gradientMap.value=v.gradientMap)}function p(y,v){y.metalness.value=v.metalness,v.metalnessMap&&(y.metalnessMap.value=v.metalnessMap,t(v.metalnessMap,y.metalnessMapTransform)),y.roughness.value=v.roughness,v.roughnessMap&&(y.roughnessMap.value=v.roughnessMap,t(v.roughnessMap,y.roughnessMapTransform)),v.envMap&&(y.envMapIntensity.value=v.envMapIntensity)}function g(y,v,w){y.ior.value=v.ior,v.sheen>0&&(y.sheenColor.value.copy(v.sheenColor).multiplyScalar(v.sheen),y.sheenRoughness.value=v.sheenRoughness,v.sheenColorMap&&(y.sheenColorMap.value=v.sheenColorMap,t(v.sheenColorMap,y.sheenColorMapTransform)),v.sheenRoughnessMap&&(y.sheenRoughnessMap.value=v.sheenRoughnessMap,t(v.sheenRoughnessMap,y.sheenRoughnessMapTransform))),v.clearcoat>0&&(y.clearcoat.value=v.clearcoat,y.clearcoatRoughness.value=v.clearcoatRoughness,v.clearcoatMap&&(y.clearcoatMap.value=v.clearcoatMap,t(v.clearcoatMap,y.clearcoatMapTransform)),v.clearcoatRoughnessMap&&(y.clearcoatRoughnessMap.value=v.clearcoatRoughnessMap,t(v.clearcoatRoughnessMap,y.clearcoatRoughnessMapTransform)),v.clearcoatNormalMap&&(y.clearcoatNormalMap.value=v.clearcoatNormalMap,t(v.clearcoatNormalMap,y.clearcoatNormalMapTransform),y.clearcoatNormalScale.value.copy(v.clearcoatNormalScale),v.side===_n&&y.clearcoatNormalScale.value.negate())),v.dispersion>0&&(y.dispersion.value=v.dispersion),v.iridescence>0&&(y.iridescence.value=v.iridescence,y.iridescenceIOR.value=v.iridescenceIOR,y.iridescenceThicknessMinimum.value=v.iridescenceThicknessRange[0],y.iridescenceThicknessMaximum.value=v.iridescenceThicknessRange[1],v.iridescenceMap&&(y.iridescenceMap.value=v.iridescenceMap,t(v.iridescenceMap,y.iridescenceMapTransform)),v.iridescenceThicknessMap&&(y.iridescenceThicknessMap.value=v.iridescenceThicknessMap,t(v.iridescenceThicknessMap,y.iridescenceThicknessMapTransform))),v.transmission>0&&(y.transmission.value=v.transmission,y.transmissionSamplerMap.value=w.texture,y.transmissionSamplerSize.value.set(w.width,w.height),v.transmissionMap&&(y.transmissionMap.value=v.transmissionMap,t(v.transmissionMap,y.transmissionMapTransform)),y.thickness.value=v.thickness,v.thicknessMap&&(y.thicknessMap.value=v.thicknessMap,t(v.thicknessMap,y.thicknessMapTransform)),y.attenuationDistance.value=v.attenuationDistance,y.attenuationColor.value.copy(v.attenuationColor)),v.anisotropy>0&&(y.anisotropyVector.value.set(v.anisotropy*Math.cos(v.anisotropyRotation),v.anisotropy*Math.sin(v.anisotropyRotation)),v.anisotropyMap&&(y.anisotropyMap.value=v.anisotropyMap,t(v.anisotropyMap,y.anisotropyMapTransform))),y.specularIntensity.value=v.specularIntensity,y.specularColor.value.copy(v.specularColor),v.specularColorMap&&(y.specularColorMap.value=v.specularColorMap,t(v.specularColorMap,y.specularColorMapTransform)),v.specularIntensityMap&&(y.specularIntensityMap.value=v.specularIntensityMap,t(v.specularIntensityMap,y.specularIntensityMapTransform))}function _(y,v){v.matcap&&(y.matcap.value=v.matcap)}function M(y,v){const w=e.get(v).light;y.referencePosition.value.setFromMatrixPosition(w.matrixWorld),y.nearDistance.value=w.shadow.camera.near,y.farDistance.value=w.shadow.camera.far}return{refreshFogUniforms:n,refreshMaterialUniforms:i}}function AM(s,e,t,n){let i={},r={},o=[];const a=s.getParameter(s.MAX_UNIFORM_BUFFER_BINDINGS);function l(w,S){const T=S.program;n.uniformBlockBinding(w,T)}function u(w,S){let T=i[w.id];T===void 0&&(_(w),T=d(w),i[w.id]=T,w.addEventListener("dispose",y));const z=S.program;n.updateUBOMapping(w,z);const N=e.render.frame;r[w.id]!==N&&(p(w),r[w.id]=N)}function d(w){const S=m();w.__bindingPointIndex=S;const T=s.createBuffer(),z=w.__size,N=w.usage;return s.bindBuffer(s.UNIFORM_BUFFER,T),s.bufferData(s.UNIFORM_BUFFER,z,N),s.bindBuffer(s.UNIFORM_BUFFER,null),s.bindBufferBase(s.UNIFORM_BUFFER,S,T),T}function m(){for(let w=0;w<a;w++)if(o.indexOf(w)===-1)return o.push(w),w;return console.error("THREE.WebGLRenderer: Maximum number of simultaneously usable uniforms groups reached."),0}function p(w){const S=i[w.id],T=w.uniforms,z=w.__cache;s.bindBuffer(s.UNIFORM_BUFFER,S);for(let N=0,A=T.length;N<A;N++){const k=Array.isArray(T[N])?T[N]:[T[N]];for(let D=0,C=k.length;D<C;D++){const X=k[D];if(g(X,N,D,z)===!0){const Y=X.__offset,H=Array.isArray(X.value)?X.value:[X.value];let K=0;for(let J=0;J<H.length;J++){const ce=H[J],de=M(ce);typeof ce=="number"||typeof ce=="boolean"?(X.__data[0]=ce,s.bufferSubData(s.UNIFORM_BUFFER,Y+K,X.__data)):ce.isMatrix3?(X.__data[0]=ce.elements[0],X.__data[1]=ce.elements[1],X.__data[2]=ce.elements[2],X.__data[3]=0,X.__data[4]=ce.elements[3],X.__data[5]=ce.elements[4],X.__data[6]=ce.elements[5],X.__data[7]=0,X.__data[8]=ce.elements[6],X.__data[9]=ce.elements[7],X.__data[10]=ce.elements[8],X.__data[11]=0):(ce.toArray(X.__data,K),K+=de.storage/Float32Array.BYTES_PER_ELEMENT)}s.bufferSubData(s.UNIFORM_BUFFER,Y,X.__data)}}}s.bindBuffer(s.UNIFORM_BUFFER,null)}function g(w,S,T,z){const N=w.value,A=S+"_"+T;if(z[A]===void 0)return typeof N=="number"||typeof N=="boolean"?z[A]=N:z[A]=N.clone(),!0;{const k=z[A];if(typeof N=="number"||typeof N=="boolean"){if(k!==N)return z[A]=N,!0}else if(k.equals(N)===!1)return k.copy(N),!0}return!1}function _(w){const S=w.uniforms;let T=0;const z=16;for(let A=0,k=S.length;A<k;A++){const D=Array.isArray(S[A])?S[A]:[S[A]];for(let C=0,X=D.length;C<X;C++){const Y=D[C],H=Array.isArray(Y.value)?Y.value:[Y.value];for(let K=0,J=H.length;K<J;K++){const ce=H[K],de=M(ce),Q=T%z;Q!==0&&z-Q<de.boundary&&(T+=z-Q),Y.__data=new Float32Array(de.storage/Float32Array.BYTES_PER_ELEMENT),Y.__offset=T,T+=de.storage}}}const N=T%z;return N>0&&(T+=z-N),w.__size=T,w.__cache={},this}function M(w){const S={boundary:0,storage:0};return typeof w=="number"||typeof w=="boolean"?(S.boundary=4,S.storage=4):w.isVector2?(S.boundary=8,S.storage=8):w.isVector3||w.isColor?(S.boundary=16,S.storage=12):w.isVector4?(S.boundary=16,S.storage=16):w.isMatrix3?(S.boundary=48,S.storage=48):w.isMatrix4?(S.boundary=64,S.storage=64):w.isTexture?console.warn("THREE.WebGLRenderer: Texture samplers can not be part of an uniforms group."):console.warn("THREE.WebGLRenderer: Unsupported uniform value type.",w),S}function y(w){const S=w.target;S.removeEventListener("dispose",y);const T=o.indexOf(S.__bindingPointIndex);o.splice(T,1),s.deleteBuffer(i[S.id]),delete i[S.id],delete r[S.id]}function v(){for(const w in i)s.deleteBuffer(i[w]);o=[],i={},r={}}return{bind:l,update:u,dispose:v}}class Lp{constructor(e={}){const{canvas:t=gp(),context:n=null,depth:i=!0,stencil:r=!1,alpha:o=!1,antialias:a=!1,premultipliedAlpha:l=!0,preserveDrawingBuffer:u=!1,powerPreference:d="default",failIfMajorPerformanceCaveat:m=!1}=e;this.isWebGLRenderer=!0;let p;if(n!==null){if(typeof WebGLRenderingContext<"u"&&n instanceof WebGLRenderingContext)throw new Error("THREE.WebGLRenderer: WebGL 1 is not supported since r163.");p=n.getContextAttributes().alpha}else p=o;const g=new Uint32Array(4),_=new Int32Array(4);let M=null,y=null;const v=[],w=[];this.domElement=t,this.debug={checkShaderErrors:!0,onShaderError:null},this.autoClear=!0,this.autoClearColor=!0,this.autoClearDepth=!0,this.autoClearStencil=!0,this.sortObjects=!0,this.clippingPlanes=[],this.localClippingEnabled=!1,this._outputColorSpace=Un,this._useLegacyLights=!1,this.toneMapping=yi,this.toneMappingExposure=1;const S=this;let T=!1,z=0,N=0,A=null,k=-1,D=null;const C=new Ct,X=new Ct;let Y=null;const H=new Oe(0);let K=0,J=t.width,ce=t.height,de=1,Q=null,pe=null;const ve=new Ct(0,0,J,ce),Ue=new Ct(0,0,J,ce);let Je=!1;const vt=new Mo;let ae=!1,we=!1;const ke=new Ke,Ee=new O,nt={background:null,fog:null,environment:null,overrideMaterial:null,isScene:!0};function be(){return A===null?de:1}let Z=n;function st(P,j){return t.getContext(P,j)}try{const P={alpha:!0,depth:i,stencil:r,antialias:a,premultipliedAlpha:l,preserveDrawingBuffer:u,powerPreference:d,failIfMajorPerformanceCaveat:m};if("setAttribute"in t&&t.setAttribute("data-engine",`three.js r${Xa}`),t.addEventListener("webglcontextlost",G,!1),t.addEventListener("webglcontextrestored",re,!1),t.addEventListener("webglcontextcreationerror",ne,!1),Z===null){const j="webgl2";if(Z=st(j,P),Z===null)throw st(j)?new Error("Error creating WebGL context with your selected attributes."):new Error("Error creating WebGL context.")}}catch(P){throw console.error("THREE.WebGLRenderer: "+P.message),P}let le,xe,he,Ae,ge,Fe,Ge,B,I,$,ue,_e,me,Ye,Re,Le,tt,Ce,Xe,lt,je,ze,Qe,ft;function V(){le=new ky(Z),le.init(),ze=new Pp(Z,le),xe=new Ny(Z,le,e,ze),he=new _M(Z),Ae=new Gy(Z),ge=new oM,Fe=new yM(Z,le,he,ge,xe,ze,Ae),Ge=new Uy(S),B=new zy(S),I=new Kv(Z),Qe=new Ly(Z,I),$=new Vy(Z,I,Ae,Qe),ue=new Xy(Z,$,I,Ae),Xe=new Wy(Z,xe,Fe),Le=new Oy(ge),_e=new sM(S,Ge,B,le,xe,Qe,Le),me=new TM(S,ge),Ye=new lM,Re=new pM(le),Ce=new Iy(S,Ge,B,he,ue,p,l),tt=new vM(S,ue,xe),ft=new AM(Z,Ae,xe,he),lt=new Dy(Z,le,Ae),je=new Hy(Z,le,Ae),Ae.programs=_e.programs,S.capabilities=xe,S.extensions=le,S.properties=ge,S.renderLists=Ye,S.shadowMap=tt,S.state=he,S.info=Ae}V();const fe=new wM(S,Z);this.xr=fe,this.getContext=function(){return Z},this.getContextAttributes=function(){return Z.getContextAttributes()},this.forceContextLoss=function(){const P=le.get("WEBGL_lose_context");P&&P.loseContext()},this.forceContextRestore=function(){const P=le.get("WEBGL_lose_context");P&&P.restoreContext()},this.getPixelRatio=function(){return de},this.setPixelRatio=function(P){P!==void 0&&(de=P,this.setSize(J,ce,!1))},this.getSize=function(P){return P.set(J,ce)},this.setSize=function(P,j,ie=!0){if(fe.isPresenting){console.warn("THREE.WebGLRenderer: Can't change size while VR device is presenting.");return}J=P,ce=j,t.width=Math.floor(P*de),t.height=Math.floor(j*de),ie===!0&&(t.style.width=P+"px",t.style.height=j+"px"),this.setViewport(0,0,P,j)},this.getDrawingBufferSize=function(P){return P.set(J*de,ce*de).floor()},this.setDrawingBufferSize=function(P,j,ie){J=P,ce=j,de=ie,t.width=Math.floor(P*ie),t.height=Math.floor(j*ie),this.setViewport(0,0,P,j)},this.getCurrentViewport=function(P){return P.copy(C)},this.getViewport=function(P){return P.copy(ve)},this.setViewport=function(P,j,ie,ee){P.isVector4?ve.set(P.x,P.y,P.z,P.w):ve.set(P,j,ie,ee),he.viewport(C.copy(ve).multiplyScalar(de).round())},this.getScissor=function(P){return P.copy(Ue)},this.setScissor=function(P,j,ie,ee){P.isVector4?Ue.set(P.x,P.y,P.z,P.w):Ue.set(P,j,ie,ee),he.scissor(X.copy(Ue).multiplyScalar(de).round())},this.getScissorTest=function(){return Je},this.setScissorTest=function(P){he.setScissorTest(Je=P)},this.setOpaqueSort=function(P){Q=P},this.setTransparentSort=function(P){pe=P},this.getClearColor=function(P){return P.copy(Ce.getClearColor())},this.setClearColor=function(){Ce.setClearColor.apply(Ce,arguments)},this.getClearAlpha=function(){return Ce.getClearAlpha()},this.setClearAlpha=function(){Ce.setClearAlpha.apply(Ce,arguments)},this.clear=function(P=!0,j=!0,ie=!0){let ee=0;if(P){let te=!1;if(A!==null){const De=A.texture.format;te=De===_h||De===vh||De===gh}if(te){const De=A.texture.type,We=De===bi||De===wr||De===uh||De===ds||De===fh||De===ph,qe=Ce.getClearColor(),$e=Ce.getClearAlpha(),rt=qe.r,ot=qe.g,gt=qe.b;We?(g[0]=rt,g[1]=ot,g[2]=gt,g[3]=$e,Z.clearBufferuiv(Z.COLOR,0,g)):(_[0]=rt,_[1]=ot,_[2]=gt,_[3]=$e,Z.clearBufferiv(Z.COLOR,0,_))}else ee|=Z.COLOR_BUFFER_BIT}j&&(ee|=Z.DEPTH_BUFFER_BIT),ie&&(ee|=Z.STENCIL_BUFFER_BIT,this.state.buffers.stencil.setMask(4294967295)),Z.clear(ee)},this.clearColor=function(){this.clear(!0,!1,!1)},this.clearDepth=function(){this.clear(!1,!0,!1)},this.clearStencil=function(){this.clear(!1,!1,!0)},this.dispose=function(){t.removeEventListener("webglcontextlost",G,!1),t.removeEventListener("webglcontextrestored",re,!1),t.removeEventListener("webglcontextcreationerror",ne,!1),Ye.dispose(),Re.dispose(),ge.dispose(),Ge.dispose(),B.dispose(),ue.dispose(),Qe.dispose(),ft.dispose(),_e.dispose(),fe.dispose(),fe.removeEventListener("sessionstart",ct),fe.removeEventListener("sessionend",Kt),At.stop()};function G(P){P.preventDefault(),console.log("THREE.WebGLRenderer: Context Lost."),T=!0}function re(){console.log("THREE.WebGLRenderer: Context Restored."),T=!1;const P=Ae.autoReset,j=tt.enabled,ie=tt.autoUpdate,ee=tt.needsUpdate,te=tt.type;V(),Ae.autoReset=P,tt.enabled=j,tt.autoUpdate=ie,tt.needsUpdate=ee,tt.type=te}function ne(P){console.error("THREE.WebGLRenderer: A WebGL context could not be created. Reason: ",P.statusMessage)}function Te(P){const j=P.target;j.removeEventListener("dispose",Te),Pe(j)}function Pe(P){_t(P),ge.remove(P)}function _t(P){const j=ge.get(P).programs;j!==void 0&&(j.forEach(function(ie){_e.releaseProgram(ie)}),P.isShaderMaterial&&_e.releaseShaderCache(P))}this.renderBufferDirect=function(P,j,ie,ee,te,De){j===null&&(j=nt);const We=te.isMesh&&te.matrixWorld.determinant()<0,qe=yl(P,j,ie,ee,te);he.setMaterial(ee,We);let $e=ie.index,rt=1;if(ee.wireframe===!0){if($e=$.getWireframeAttribute(ie),$e===void 0)return;rt=2}const ot=ie.drawRange,gt=ie.attributes.position;let Ut=ot.start*rt,Yt=(ot.start+ot.count)*rt;De!==null&&(Ut=Math.max(Ut,De.start*rt),Yt=Math.min(Yt,(De.start+De.count)*rt)),$e!==null?(Ut=Math.max(Ut,0),Yt=Math.min(Yt,$e.count)):gt!=null&&(Ut=Math.max(Ut,0),Yt=Math.min(Yt,gt.count));const Jt=Yt-Ut;if(Jt<0||Jt===1/0)return;Qe.setup(te,ee,qe,ie,$e);let Nn,Mt=lt;if($e!==null&&(Nn=I.get($e),Mt=je,Mt.setIndex(Nn)),te.isMesh)ee.wireframe===!0?(he.setLineWidth(ee.wireframeLinewidth*be()),Mt.setMode(Z.LINES)):Mt.setMode(Z.TRIANGLES);else if(te.isLine){let it=ee.linewidth;it===void 0&&(it=1),he.setLineWidth(it*be()),te.isLineSegments?Mt.setMode(Z.LINES):te.isLineLoop?Mt.setMode(Z.LINE_LOOP):Mt.setMode(Z.LINE_STRIP)}else te.isPoints?Mt.setMode(Z.POINTS):te.isSprite&&Mt.setMode(Z.TRIANGLES);if(te.isBatchedMesh)te._multiDrawInstances!==null?Mt.renderMultiDrawInstances(te._multiDrawStarts,te._multiDrawCounts,te._multiDrawCount,te._multiDrawInstances):Mt.renderMultiDraw(te._multiDrawStarts,te._multiDrawCounts,te._multiDrawCount);else if(te.isInstancedMesh)Mt.renderInstances(Ut,Jt,te.count);else if(ie.isInstancedBufferGeometry){const it=ie._maxInstanceCount!==void 0?ie._maxInstanceCount:1/0,Zi=Math.min(ie.instanceCount,it);Mt.renderInstances(Ut,Jt,Zi)}else Mt.render(Ut,Jt)};function St(P,j,ie){P.transparent===!0&&P.side===Tn&&P.forceSinglePass===!1?(P.side=_n,P.needsUpdate=!0,Ln(P,j,ie),P.side=xi,P.needsUpdate=!0,Ln(P,j,ie),P.side=Tn):Ln(P,j,ie)}this.compile=function(P,j,ie=null){ie===null&&(ie=P),y=Re.get(ie),y.init(j),w.push(y),ie.traverseVisible(function(te){te.isLight&&te.layers.test(j.layers)&&(y.pushLight(te),te.castShadow&&y.pushShadow(te))}),P!==ie&&P.traverseVisible(function(te){te.isLight&&te.layers.test(j.layers)&&(y.pushLight(te),te.castShadow&&y.pushShadow(te))}),y.setupLights(S._useLegacyLights);const ee=new Set;return P.traverse(function(te){const De=te.material;if(De)if(Array.isArray(De))for(let We=0;We<De.length;We++){const qe=De[We];St(qe,ie,te),ee.add(qe)}else St(De,ie,te),ee.add(De)}),w.pop(),y=null,ee},this.compileAsync=function(P,j,ie=null){const ee=this.compile(P,j,ie);return new Promise(te=>{function De(){if(ee.forEach(function(We){ge.get(We).currentProgram.isReady()&&ee.delete(We)}),ee.size===0){te(P);return}setTimeout(De,10)}le.get("KHR_parallel_shader_compile")!==null?De():setTimeout(De,10)})};let Et=null;function Tt(P){Et&&Et(P)}function ct(){At.stop()}function Kt(){At.start()}const At=new wp;At.setAnimationLoop(Tt),typeof self<"u"&&At.setContext(self),this.setAnimationLoop=function(P){Et=P,fe.setAnimationLoop(P),P===null?At.stop():At.start()},fe.addEventListener("sessionstart",ct),fe.addEventListener("sessionend",Kt),this.render=function(P,j){if(j!==void 0&&j.isCamera!==!0){console.error("THREE.WebGLRenderer.render: camera is not an instance of THREE.Camera.");return}if(T===!0)return;P.matrixWorldAutoUpdate===!0&&P.updateMatrixWorld(),j.parent===null&&j.matrixWorldAutoUpdate===!0&&j.updateMatrixWorld(),fe.enabled===!0&&fe.isPresenting===!0&&(fe.cameraAutoUpdate===!0&&fe.updateCamera(j),j=fe.getCamera()),P.isScene===!0&&P.onBeforeRender(S,P,j,A),y=Re.get(P,w.length),y.init(j),w.push(y),ke.multiplyMatrices(j.projectionMatrix,j.matrixWorldInverse),vt.setFromProjectionMatrix(ke),we=this.localClippingEnabled,ae=Le.init(this.clippingPlanes,we),M=Ye.get(P,v.length),M.init(),v.push(M),Pn(P,j,0,S.sortObjects),M.finish(),S.sortObjects===!0&&M.sort(Q,pe);const ie=fe.enabled===!1||fe.isPresenting===!1||fe.hasDepthSensing()===!1;ie&&Ce.addToRenderList(M,P),this.info.render.frame++,ae===!0&&Le.beginShadows();const ee=y.state.shadowsArray;tt.render(ee,P,j),ae===!0&&Le.endShadows(),this.info.autoReset===!0&&this.info.reset();const te=M.opaque,De=M.transmissive;if(y.setupLights(S._useLegacyLights),j.isArrayCamera){const We=j.cameras;if(De.length>0)for(let qe=0,$e=We.length;qe<$e;qe++){const rt=We[qe];Yi(te,De,P,rt)}ie&&Ce.render(P);for(let qe=0,$e=We.length;qe<$e;qe++){const rt=We[qe];si(M,P,rt,rt.viewport)}}else De.length>0&&Yi(te,De,P,j),ie&&Ce.render(P),si(M,P,j);A!==null&&(Fe.updateMultisampleRenderTarget(A),Fe.updateRenderTargetMipmap(A)),P.isScene===!0&&P.onAfterRender(S,P,j),Qe.resetDefaultState(),k=-1,D=null,w.pop(),w.length>0?(y=w[w.length-1],ae===!0&&Le.setGlobalState(S.clippingPlanes,y.state.camera)):y=null,v.pop(),v.length>0?M=v[v.length-1]:M=null};function Pn(P,j,ie,ee){if(P.visible===!1)return;if(P.layers.test(j.layers)){if(P.isGroup)ie=P.renderOrder;else if(P.isLOD)P.autoUpdate===!0&&P.update(j);else if(P.isLight)y.pushLight(P),P.castShadow&&y.pushShadow(P);else if(P.isSprite){if(!P.frustumCulled||vt.intersectsSprite(P)){ee&&Ee.setFromMatrixPosition(P.matrixWorld).applyMatrix4(ke);const We=ue.update(P),qe=P.material;qe.visible&&M.push(P,We,qe,ie,Ee.z,null)}}else if((P.isMesh||P.isLine||P.isPoints)&&(!P.frustumCulled||vt.intersectsObject(P))){const We=ue.update(P),qe=P.material;if(ee&&(P.boundingSphere!==void 0?(P.boundingSphere===null&&P.computeBoundingSphere(),Ee.copy(P.boundingSphere.center)):(We.boundingSphere===null&&We.computeBoundingSphere(),Ee.copy(We.boundingSphere.center)),Ee.applyMatrix4(P.matrixWorld).applyMatrix4(ke)),Array.isArray(qe)){const $e=We.groups;for(let rt=0,ot=$e.length;rt<ot;rt++){const gt=$e[rt],Ut=qe[gt.materialIndex];Ut&&Ut.visible&&M.push(P,We,Ut,ie,Ee.z,gt)}}else qe.visible&&M.push(P,We,qe,ie,Ee.z,null)}}const De=P.children;for(let We=0,qe=De.length;We<qe;We++)Pn(De[We],j,ie,ee)}function si(P,j,ie,ee){const te=P.opaque,De=P.transmissive,We=P.transparent;y.setupLightsView(ie),ae===!0&&Le.setGlobalState(S.clippingPlanes,ie),ee&&he.viewport(C.copy(ee)),te.length>0&&qi(te,j,ie),De.length>0&&qi(De,j,ie),We.length>0&&qi(We,j,ie),he.buffers.depth.setTest(!0),he.buffers.depth.setMask(!0),he.buffers.color.setMask(!0),he.setPolygonOffset(!1)}function Yi(P,j,ie,ee){if((ie.isScene===!0?ie.overrideMaterial:null)!==null)return;y.state.transmissionRenderTarget[ee.id]===void 0&&(y.state.transmissionRenderTarget[ee.id]=new qt(1,1,{generateMipmaps:!0,type:le.has("EXT_color_buffer_half_float")||le.has("EXT_color_buffer_float")?Fn:bi,minFilter:Qn,samples:4,stencilBuffer:r,resolveDepthBuffer:!1,resolveStencilBuffer:!1}));const De=y.state.transmissionRenderTarget[ee.id],We=ee.viewport||C;De.setSize(We.z,We.w);const qe=S.getRenderTarget();S.setRenderTarget(De),S.getClearColor(H),K=S.getClearAlpha(),K<1&&S.setClearColor(16777215,.5),S.clear();const $e=S.toneMapping;S.toneMapping=yi;const rt=ee.viewport;if(ee.viewport!==void 0&&(ee.viewport=void 0),y.setupLightsView(ee),ae===!0&&Le.setGlobalState(S.clippingPlanes,ee),qi(P,ie,ee),Fe.updateMultisampleRenderTarget(De),Fe.updateRenderTargetMipmap(De),le.has("WEBGL_multisampled_render_to_texture")===!1){let ot=!1;for(let gt=0,Ut=j.length;gt<Ut;gt++){const Yt=j[gt],Jt=Yt.object,Nn=Yt.geometry,Mt=Yt.material,it=Yt.group;if(Mt.side===Tn&&Jt.layers.test(ee.layers)){const Zi=Mt.side;Mt.side=_n,Mt.needsUpdate=!0,In(Jt,ie,ee,Nn,Mt,it),Mt.side=Zi,Mt.needsUpdate=!0,ot=!0}}ot===!0&&(Fe.updateMultisampleRenderTarget(De),Fe.updateRenderTargetMipmap(De))}S.setRenderTarget(qe),S.setClearColor(H,K),rt!==void 0&&(ee.viewport=rt),S.toneMapping=$e}function qi(P,j,ie){const ee=j.isScene===!0?j.overrideMaterial:null;for(let te=0,De=P.length;te<De;te++){const We=P[te],qe=We.object,$e=We.geometry,rt=ee===null?We.material:ee,ot=We.group;qe.layers.test(ie.layers)&&In(qe,j,ie,$e,rt,ot)}}function In(P,j,ie,ee,te,De){P.onBeforeRender(S,j,ie,ee,te,De),P.modelViewMatrix.multiplyMatrices(ie.matrixWorldInverse,P.matrixWorld),P.normalMatrix.getNormalMatrix(P.modelViewMatrix),te.onBeforeRender(S,j,ie,ee,P,De),te.transparent===!0&&te.side===Tn&&te.forceSinglePass===!1?(te.side=_n,te.needsUpdate=!0,S.renderBufferDirect(ie,j,ee,te,P,De),te.side=xi,te.needsUpdate=!0,S.renderBufferDirect(ie,j,ee,te,P,De),te.side=Tn):S.renderBufferDirect(ie,j,ee,te,P,De),P.onAfterRender(S,j,ie,ee,te,De)}function Ln(P,j,ie){j.isScene!==!0&&(j=nt);const ee=ge.get(P),te=y.state.lights,De=y.state.shadowsArray,We=te.state.version,qe=_e.getParameters(P,te.state,De,j,ie),$e=_e.getProgramCacheKey(qe);let rt=ee.programs;ee.environment=P.isMeshStandardMaterial?j.environment:null,ee.fog=j.fog,ee.envMap=(P.isMeshStandardMaterial?B:Ge).get(P.envMap||ee.environment),ee.envMapRotation=ee.environment!==null&&P.envMap===null?j.environmentRotation:P.envMapRotation,rt===void 0&&(P.addEventListener("dispose",Te),rt=new Map,ee.programs=rt);let ot=rt.get($e);if(ot!==void 0){if(ee.currentProgram===ot&&ee.lightsStateVersion===We)return Eo(P,qe),ot}else qe.uniforms=_e.getUniforms(P),P.onBuild(ie,qe,S),P.onBeforeCompile(qe,S),ot=_e.acquireProgram(qe,$e),rt.set($e,ot),ee.uniforms=qe.uniforms;const gt=ee.uniforms;return(!P.isShaderMaterial&&!P.isRawShaderMaterial||P.clipping===!0)&&(gt.clippingPlanes=Le.uniform),Eo(P,qe),ee.needsLights=Zn(P),ee.lightsStateVersion=We,ee.needsLights&&(gt.ambientLightColor.value=te.state.ambient,gt.lightProbe.value=te.state.probe,gt.directionalLights.value=te.state.directional,gt.directionalLightShadows.value=te.state.directionalShadow,gt.spotLights.value=te.state.spot,gt.spotLightShadows.value=te.state.spotShadow,gt.rectAreaLights.value=te.state.rectArea,gt.ltc_1.value=te.state.rectAreaLTC1,gt.ltc_2.value=te.state.rectAreaLTC2,gt.pointLights.value=te.state.point,gt.pointLightShadows.value=te.state.pointShadow,gt.hemisphereLights.value=te.state.hemi,gt.directionalShadowMap.value=te.state.directionalShadowMap,gt.directionalShadowMatrix.value=te.state.directionalShadowMatrix,gt.spotShadowMap.value=te.state.spotShadowMap,gt.spotLightMatrix.value=te.state.spotLightMatrix,gt.spotLightMap.value=te.state.spotLightMap,gt.pointShadowMap.value=te.state.pointShadowMap,gt.pointShadowMatrix.value=te.state.pointShadowMatrix),ee.currentProgram=ot,ee.uniformsList=null,ot}function Dn(P){if(P.uniformsList===null){const j=P.currentProgram.getUniforms();P.uniformsList=Ua.seqWithValue(j.seq,P.uniforms)}return P.uniformsList}function Eo(P,j){const ie=ge.get(P);ie.outputColorSpace=j.outputColorSpace,ie.batching=j.batching,ie.instancing=j.instancing,ie.instancingColor=j.instancingColor,ie.instancingMorph=j.instancingMorph,ie.skinning=j.skinning,ie.morphTargets=j.morphTargets,ie.morphNormals=j.morphNormals,ie.morphColors=j.morphColors,ie.morphTargetsCount=j.morphTargetsCount,ie.numClippingPlanes=j.numClippingPlanes,ie.numIntersection=j.numClipIntersection,ie.vertexAlphas=j.vertexAlphas,ie.vertexTangents=j.vertexTangents,ie.toneMapping=j.toneMapping}function yl(P,j,ie,ee,te){j.isScene!==!0&&(j=nt),Fe.resetTextureUnits();const De=j.fog,We=ee.isMeshStandardMaterial?j.environment:null,qe=A===null?S.outputColorSpace:A.isXRRenderTarget===!0?A.texture.colorSpace:wi,$e=(ee.isMeshStandardMaterial?B:Ge).get(ee.envMap||We),rt=ee.vertexColors===!0&&!!ie.attributes.color&&ie.attributes.color.itemSize===4,ot=!!ie.attributes.tangent&&(!!ee.normalMap||ee.anisotropy>0),gt=!!ie.morphAttributes.position,Ut=!!ie.morphAttributes.normal,Yt=!!ie.morphAttributes.color;let Jt=yi;ee.toneMapped&&(A===null||A.isXRRenderTarget===!0)&&(Jt=S.toneMapping);const Nn=ie.morphAttributes.position||ie.morphAttributes.normal||ie.morphAttributes.color,Mt=Nn!==void 0?Nn.length:0,it=ge.get(ee),Zi=y.state.lights;if(ae===!0&&(we===!0||P!==D)){const dn=P===D&&ee.id===k;Le.setState(ee,P,dn)}let Pt=!1;ee.version===it.__version?(it.needsLights&&it.lightsStateVersion!==Zi.state.version||it.outputColorSpace!==qe||te.isBatchedMesh&&it.batching===!1||!te.isBatchedMesh&&it.batching===!0||te.isInstancedMesh&&it.instancing===!1||!te.isInstancedMesh&&it.instancing===!0||te.isSkinnedMesh&&it.skinning===!1||!te.isSkinnedMesh&&it.skinning===!0||te.isInstancedMesh&&it.instancingColor===!0&&te.instanceColor===null||te.isInstancedMesh&&it.instancingColor===!1&&te.instanceColor!==null||te.isInstancedMesh&&it.instancingMorph===!0&&te.morphTexture===null||te.isInstancedMesh&&it.instancingMorph===!1&&te.morphTexture!==null||it.envMap!==$e||ee.fog===!0&&it.fog!==De||it.numClippingPlanes!==void 0&&(it.numClippingPlanes!==Le.numPlanes||it.numIntersection!==Le.numIntersection)||it.vertexAlphas!==rt||it.vertexTangents!==ot||it.morphTargets!==gt||it.morphNormals!==Ut||it.morphColors!==Yt||it.toneMapping!==Jt||it.morphTargetsCount!==Mt)&&(Pt=!0):(Pt=!0,it.__version=ee.version);let oi=it.currentProgram;Pt===!0&&(oi=Ln(ee,j,te));let Ir=!1,ji=!1,vs=!1;const $t=oi.getUniforms(),jn=it.uniforms;if(he.useProgram(oi.program)&&(Ir=!0,ji=!0,vs=!0),ee.id!==k&&(k=ee.id,ji=!0),Ir||D!==P){$t.setValue(Z,"projectionMatrix",P.projectionMatrix),$t.setValue(Z,"viewMatrix",P.matrixWorldInverse);const dn=$t.map.cameraPosition;dn!==void 0&&dn.setValue(Z,Ee.setFromMatrixPosition(P.matrixWorld)),xe.logarithmicDepthBuffer&&$t.setValue(Z,"logDepthBufFC",2/(Math.log(P.far+1)/Math.LN2)),(ee.isMeshPhongMaterial||ee.isMeshToonMaterial||ee.isMeshLambertMaterial||ee.isMeshBasicMaterial||ee.isMeshStandardMaterial||ee.isShaderMaterial)&&$t.setValue(Z,"isOrthographic",P.isOrthographicCamera===!0),D!==P&&(D=P,ji=!0,vs=!0)}if(te.isSkinnedMesh){$t.setOptional(Z,te,"bindMatrix"),$t.setOptional(Z,te,"bindMatrixInverse");const dn=te.skeleton;dn&&(dn.boneTexture===null&&dn.computeBoneTexture(),$t.setValue(Z,"boneTexture",dn.boneTexture,Fe))}te.isBatchedMesh&&($t.setOptional(Z,te,"batchingTexture"),$t.setValue(Z,"batchingTexture",te._matricesTexture,Fe));const _s=ie.morphAttributes;if((_s.position!==void 0||_s.normal!==void 0||_s.color!==void 0)&&Xe.update(te,ie,oi),(ji||it.receiveShadow!==te.receiveShadow)&&(it.receiveShadow=te.receiveShadow,$t.setValue(Z,"receiveShadow",te.receiveShadow)),ee.isMeshGouraudMaterial&&ee.envMap!==null&&(jn.envMap.value=$e,jn.flipEnvMap.value=$e.isCubeTexture&&$e.isRenderTargetTexture===!1?-1:1),ee.isMeshStandardMaterial&&ee.envMap===null&&j.environment!==null&&(jn.envMapIntensity.value=j.environmentIntensity),ji&&($t.setValue(Z,"toneMappingExposure",S.toneMappingExposure),it.needsLights&&To(jn,vs),De&&ee.fog===!0&&me.refreshFogUniforms(jn,De),me.refreshMaterialUniforms(jn,ee,de,ce,y.state.transmissionRenderTarget[P.id]),Ua.upload(Z,Dn(it),jn,Fe)),ee.isShaderMaterial&&ee.uniformsNeedUpdate===!0&&(Ua.upload(Z,Dn(it),jn,Fe),ee.uniformsNeedUpdate=!1),ee.isSpriteMaterial&&$t.setValue(Z,"center",te.center),$t.setValue(Z,"modelViewMatrix",te.modelViewMatrix),$t.setValue(Z,"normalMatrix",te.normalMatrix),$t.setValue(Z,"modelMatrix",te.matrixWorld),ee.isShaderMaterial||ee.isRawShaderMaterial){const dn=ee.uniformsGroups;for(let Ei=0,Ti=dn.length;Ei<Ti;Ei++){const Lr=dn[Ei];ft.update(Lr,oi),ft.bind(Lr,oi)}}return oi}function To(P,j){P.ambientLightColor.needsUpdate=j,P.lightProbe.needsUpdate=j,P.directionalLights.needsUpdate=j,P.directionalLightShadows.needsUpdate=j,P.pointLights.needsUpdate=j,P.pointLightShadows.needsUpdate=j,P.spotLights.needsUpdate=j,P.spotLightShadows.needsUpdate=j,P.rectAreaLights.needsUpdate=j,P.hemisphereLights.needsUpdate=j}function Zn(P){return P.isMeshLambertMaterial||P.isMeshToonMaterial||P.isMeshPhongMaterial||P.isMeshStandardMaterial||P.isShadowMaterial||P.isShaderMaterial&&P.lights===!0}this.getActiveCubeFace=function(){return z},this.getActiveMipmapLevel=function(){return N},this.getRenderTarget=function(){return A},this.setRenderTargetTextures=function(P,j,ie){ge.get(P.texture).__webglTexture=j,ge.get(P.depthTexture).__webglTexture=ie;const ee=ge.get(P);ee.__hasExternalTextures=!0,ee.__autoAllocateDepthBuffer=ie===void 0,ee.__autoAllocateDepthBuffer||le.has("WEBGL_multisampled_render_to_texture")===!0&&(console.warn("THREE.WebGLRenderer: Render-to-texture extension was disabled because an external texture was provided"),ee.__useRenderToTexture=!1)},this.setRenderTargetFramebuffer=function(P,j){const ie=ge.get(P);ie.__webglFramebuffer=j,ie.__useDefaultFramebuffer=j===void 0},this.setRenderTarget=function(P,j=0,ie=0){A=P,z=j,N=ie;let ee=!0,te=null,De=!1,We=!1;if(P){const $e=ge.get(P);$e.__useDefaultFramebuffer!==void 0?(he.bindFramebuffer(Z.FRAMEBUFFER,null),ee=!1):$e.__webglFramebuffer===void 0?Fe.setupRenderTarget(P):$e.__hasExternalTextures&&Fe.rebindTextures(P,ge.get(P.texture).__webglTexture,ge.get(P.depthTexture).__webglTexture);const rt=P.texture;(rt.isData3DTexture||rt.isDataArrayTexture||rt.isCompressedArrayTexture)&&(We=!0);const ot=ge.get(P).__webglFramebuffer;P.isWebGLCubeRenderTarget?(Array.isArray(ot[j])?te=ot[j][ie]:te=ot[j],De=!0):P.samples>0&&Fe.useMultisampledRTT(P)===!1?te=ge.get(P).__webglMultisampledFramebuffer:Array.isArray(ot)?te=ot[ie]:te=ot,C.copy(P.viewport),X.copy(P.scissor),Y=P.scissorTest}else C.copy(ve).multiplyScalar(de).floor(),X.copy(Ue).multiplyScalar(de).floor(),Y=Je;if(he.bindFramebuffer(Z.FRAMEBUFFER,te)&&ee&&he.drawBuffers(P,te),he.viewport(C),he.scissor(X),he.setScissorTest(Y),De){const $e=ge.get(P.texture);Z.framebufferTexture2D(Z.FRAMEBUFFER,Z.COLOR_ATTACHMENT0,Z.TEXTURE_CUBE_MAP_POSITIVE_X+j,$e.__webglTexture,ie)}else if(We){const $e=ge.get(P.texture),rt=j||0;Z.framebufferTextureLayer(Z.FRAMEBUFFER,Z.COLOR_ATTACHMENT0,$e.__webglTexture,ie||0,rt)}k=-1},this.readRenderTargetPixels=function(P,j,ie,ee,te,De,We){if(!(P&&P.isWebGLRenderTarget)){console.error("THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not THREE.WebGLRenderTarget.");return}let qe=ge.get(P).__webglFramebuffer;if(P.isWebGLCubeRenderTarget&&We!==void 0&&(qe=qe[We]),qe){he.bindFramebuffer(Z.FRAMEBUFFER,qe);try{const $e=P.texture,rt=$e.format,ot=$e.type;if(!xe.textureFormatReadable(rt)){console.error("THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not in RGBA or implementation defined format.");return}if(!xe.textureTypeReadable(ot)){console.error("THREE.WebGLRenderer.readRenderTargetPixels: renderTarget is not in UnsignedByteType or implementation defined type.");return}j>=0&&j<=P.width-ee&&ie>=0&&ie<=P.height-te&&Z.readPixels(j,ie,ee,te,ze.convert(rt),ze.convert(ot),De)}finally{const $e=A!==null?ge.get(A).__webglFramebuffer:null;he.bindFramebuffer(Z.FRAMEBUFFER,$e)}}},this.copyFramebufferToTexture=function(P,j,ie=0){const ee=Math.pow(2,-ie),te=Math.floor(j.image.width*ee),De=Math.floor(j.image.height*ee);Fe.setTexture2D(j,0),Z.copyTexSubImage2D(Z.TEXTURE_2D,ie,0,0,P.x,P.y,te,De),he.unbindTexture()},this.copyTextureToTexture=function(P,j,ie,ee=0){const te=j.image.width,De=j.image.height,We=ze.convert(ie.format),qe=ze.convert(ie.type);Fe.setTexture2D(ie,0),Z.pixelStorei(Z.UNPACK_FLIP_Y_WEBGL,ie.flipY),Z.pixelStorei(Z.UNPACK_PREMULTIPLY_ALPHA_WEBGL,ie.premultiplyAlpha),Z.pixelStorei(Z.UNPACK_ALIGNMENT,ie.unpackAlignment),j.isDataTexture?Z.texSubImage2D(Z.TEXTURE_2D,ee,P.x,P.y,te,De,We,qe,j.image.data):j.isCompressedTexture?Z.compressedTexSubImage2D(Z.TEXTURE_2D,ee,P.x,P.y,j.mipmaps[0].width,j.mipmaps[0].height,We,j.mipmaps[0].data):Z.texSubImage2D(Z.TEXTURE_2D,ee,P.x,P.y,We,qe,j.image),ee===0&&ie.generateMipmaps&&Z.generateMipmap(Z.TEXTURE_2D),he.unbindTexture()},this.copyTextureToTexture3D=function(P,j,ie,ee,te=0){const De=P.max.x-P.min.x,We=P.max.y-P.min.y,qe=P.max.z-P.min.z,$e=ze.convert(ee.format),rt=ze.convert(ee.type);let ot;if(ee.isData3DTexture)Fe.setTexture3D(ee,0),ot=Z.TEXTURE_3D;else if(ee.isDataArrayTexture||ee.isCompressedArrayTexture)Fe.setTexture2DArray(ee,0),ot=Z.TEXTURE_2D_ARRAY;else{console.warn("THREE.WebGLRenderer.copyTextureToTexture3D: only supports THREE.DataTexture3D and THREE.DataTexture2DArray.");return}Z.pixelStorei(Z.UNPACK_FLIP_Y_WEBGL,ee.flipY),Z.pixelStorei(Z.UNPACK_PREMULTIPLY_ALPHA_WEBGL,ee.premultiplyAlpha),Z.pixelStorei(Z.UNPACK_ALIGNMENT,ee.unpackAlignment);const gt=Z.getParameter(Z.UNPACK_ROW_LENGTH),Ut=Z.getParameter(Z.UNPACK_IMAGE_HEIGHT),Yt=Z.getParameter(Z.UNPACK_SKIP_PIXELS),Jt=Z.getParameter(Z.UNPACK_SKIP_ROWS),Nn=Z.getParameter(Z.UNPACK_SKIP_IMAGES),Mt=ie.isCompressedTexture?ie.mipmaps[te]:ie.image;Z.pixelStorei(Z.UNPACK_ROW_LENGTH,Mt.width),Z.pixelStorei(Z.UNPACK_IMAGE_HEIGHT,Mt.height),Z.pixelStorei(Z.UNPACK_SKIP_PIXELS,P.min.x),Z.pixelStorei(Z.UNPACK_SKIP_ROWS,P.min.y),Z.pixelStorei(Z.UNPACK_SKIP_IMAGES,P.min.z),ie.isDataTexture||ie.isData3DTexture?Z.texSubImage3D(ot,te,j.x,j.y,j.z,De,We,qe,$e,rt,Mt.data):ee.isCompressedArrayTexture?Z.compressedTexSubImage3D(ot,te,j.x,j.y,j.z,De,We,qe,$e,Mt.data):Z.texSubImage3D(ot,te,j.x,j.y,j.z,De,We,qe,$e,rt,Mt),Z.pixelStorei(Z.UNPACK_ROW_LENGTH,gt),Z.pixelStorei(Z.UNPACK_IMAGE_HEIGHT,Ut),Z.pixelStorei(Z.UNPACK_SKIP_PIXELS,Yt),Z.pixelStorei(Z.UNPACK_SKIP_ROWS,Jt),Z.pixelStorei(Z.UNPACK_SKIP_IMAGES,Nn),te===0&&ee.generateMipmaps&&Z.generateMipmap(ot),he.unbindTexture()},this.initTexture=function(P){P.isCubeTexture?Fe.setTextureCube(P,0):P.isData3DTexture?Fe.setTexture3D(P,0):P.isDataArrayTexture||P.isCompressedArrayTexture?Fe.setTexture2DArray(P,0):Fe.setTexture2D(P,0),he.unbindTexture()},this.resetState=function(){z=0,N=0,A=null,he.reset(),Qe.reset()},typeof __THREE_DEVTOOLS__<"u"&&__THREE_DEVTOOLS__.dispatchEvent(new CustomEvent("observe",{detail:this}))}get coordinateSystem(){return ei}get outputColorSpace(){return this._outputColorSpace}set outputColorSpace(e){this._outputColorSpace=e;const t=this.getContext();t.drawingBufferColorSpace=e===Za?"display-p3":"srgb",t.unpackColorSpace=wt.workingColorSpace===_o?"display-p3":"srgb"}get useLegacyLights(){return console.warn("THREE.WebGLRenderer: The property .useLegacyLights has been deprecated. Migrate your lighting according to the following guide: https://discourse.threejs.org/t/updates-to-lighting-in-three-js-r155/53733."),this._useLegacyLights}set useLegacyLights(e){console.warn("THREE.WebGLRenderer: The property .useLegacyLights has been deprecated. Migrate your lighting according to the following guide: https://discourse.threejs.org/t/updates-to-lighting-in-three-js-r155/53733."),this._useLegacyLights=e}}class el{constructor(e,t=25e-5){this.isFogExp2=!0,this.name="",this.color=new Oe(e),this.density=t}clone(){return new el(this.color,this.density)}toJSON(){return{type:"FogExp2",name:this.name,color:this.color.getHex(),density:this.density}}}class tl{constructor(e,t=1,n=1e3){this.isFog=!0,this.name="",this.color=new Oe(e),this.near=t,this.far=n}clone(){return new tl(this.color,this.near,this.far)}toJSON(){return{type:"Fog",name:this.name,color:this.color.getHex(),near:this.near,far:this.far}}}class Rh extends xt{constructor(){super(),this.isScene=!0,this.type="Scene",this.background=null,this.environment=null,this.fog=null,this.backgroundBlurriness=0,this.backgroundIntensity=1,this.backgroundRotation=new xn,this.environmentIntensity=1,this.environmentRotation=new xn,this.overrideMaterial=null,typeof __THREE_DEVTOOLS__<"u"&&__THREE_DEVTOOLS__.dispatchEvent(new CustomEvent("observe",{detail:this}))}copy(e,t){return super.copy(e,t),e.background!==null&&(this.background=e.background.clone()),e.environment!==null&&(this.environment=e.environment.clone()),e.fog!==null&&(this.fog=e.fog.clone()),this.backgroundBlurriness=e.backgroundBlurriness,this.backgroundIntensity=e.backgroundIntensity,this.backgroundRotation.copy(e.backgroundRotation),this.environmentIntensity=e.environmentIntensity,this.environmentRotation.copy(e.environmentRotation),e.overrideMaterial!==null&&(this.overrideMaterial=e.overrideMaterial.clone()),this.matrixAutoUpdate=e.matrixAutoUpdate,this}toJSON(e){const t=super.toJSON(e);return this.fog!==null&&(t.object.fog=this.fog.toJSON()),this.backgroundBlurriness>0&&(t.object.backgroundBlurriness=this.backgroundBlurriness),this.backgroundIntensity!==1&&(t.object.backgroundIntensity=this.backgroundIntensity),t.object.backgroundRotation=this.backgroundRotation.toArray(),this.environmentIntensity!==1&&(t.object.environmentIntensity=this.environmentIntensity),t.object.environmentRotation=this.environmentRotation.toArray(),t}}class nl{constructor(e,t){this.isInterleavedBuffer=!0,this.array=e,this.stride=t,this.count=e!==void 0?e.length/t:0,this.usage=so,this._updateRange={offset:0,count:-1},this.updateRanges=[],this.version=0,this.uuid=Rn()}onUploadCallback(){}set needsUpdate(e){e===!0&&this.version++}get updateRange(){return vp("THREE.InterleavedBuffer: updateRange() is deprecated and will be removed in r169. Use addUpdateRange() instead."),this._updateRange}setUsage(e){return this.usage=e,this}addUpdateRange(e,t){this.updateRanges.push({start:e,count:t})}clearUpdateRanges(){this.updateRanges.length=0}copy(e){return this.array=new e.array.constructor(e.array),this.count=e.count,this.stride=e.stride,this.usage=e.usage,this}copyAt(e,t,n){e*=this.stride,n*=t.stride;for(let i=0,r=this.stride;i<r;i++)this.array[e+i]=t.array[n+i];return this}set(e,t=0){return this.array.set(e,t),this}clone(e){e.arrayBuffers===void 0&&(e.arrayBuffers={}),this.array.buffer._uuid===void 0&&(this.array.buffer._uuid=Rn()),e.arrayBuffers[this.array.buffer._uuid]===void 0&&(e.arrayBuffers[this.array.buffer._uuid]=this.array.slice(0).buffer);const t=new this.array.constructor(e.arrayBuffers[this.array.buffer._uuid]),n=new this.constructor(t,this.stride);return n.setUsage(this.usage),n}onUpload(e){return this.onUploadCallback=e,this}toJSON(e){return e.arrayBuffers===void 0&&(e.arrayBuffers={}),this.array.buffer._uuid===void 0&&(this.array.buffer._uuid=Rn()),e.arrayBuffers[this.array.buffer._uuid]===void 0&&(e.arrayBuffers[this.array.buffer._uuid]=Array.from(new Uint32Array(this.array.buffer))),{uuid:this.uuid,buffer:this.array.buffer._uuid,type:this.array.constructor.name,stride:this.stride}}}const mn=new O;class Er{constructor(e,t,n,i=!1){this.isInterleavedBufferAttribute=!0,this.name="",this.data=e,this.itemSize=t,this.offset=n,this.normalized=i}get count(){return this.data.count}get array(){return this.data.array}set needsUpdate(e){this.data.needsUpdate=e}applyMatrix4(e){for(let t=0,n=this.data.count;t<n;t++)mn.fromBufferAttribute(this,t),mn.applyMatrix4(e),this.setXYZ(t,mn.x,mn.y,mn.z);return this}applyNormalMatrix(e){for(let t=0,n=this.count;t<n;t++)mn.fromBufferAttribute(this,t),mn.applyNormalMatrix(e),this.setXYZ(t,mn.x,mn.y,mn.z);return this}transformDirection(e){for(let t=0,n=this.count;t<n;t++)mn.fromBufferAttribute(this,t),mn.transformDirection(e),this.setXYZ(t,mn.x,mn.y,mn.z);return this}getComponent(e,t){let n=this.array[e*this.data.stride+this.offset+t];return this.normalized&&(n=vn(n,this.array)),n}setComponent(e,t,n){return this.normalized&&(n=dt(n,this.array)),this.data.array[e*this.data.stride+this.offset+t]=n,this}setX(e,t){return this.normalized&&(t=dt(t,this.array)),this.data.array[e*this.data.stride+this.offset]=t,this}setY(e,t){return this.normalized&&(t=dt(t,this.array)),this.data.array[e*this.data.stride+this.offset+1]=t,this}setZ(e,t){return this.normalized&&(t=dt(t,this.array)),this.data.array[e*this.data.stride+this.offset+2]=t,this}setW(e,t){return this.normalized&&(t=dt(t,this.array)),this.data.array[e*this.data.stride+this.offset+3]=t,this}getX(e){let t=this.data.array[e*this.data.stride+this.offset];return this.normalized&&(t=vn(t,this.array)),t}getY(e){let t=this.data.array[e*this.data.stride+this.offset+1];return this.normalized&&(t=vn(t,this.array)),t}getZ(e){let t=this.data.array[e*this.data.stride+this.offset+2];return this.normalized&&(t=vn(t,this.array)),t}getW(e){let t=this.data.array[e*this.data.stride+this.offset+3];return this.normalized&&(t=vn(t,this.array)),t}setXY(e,t,n){return e=e*this.data.stride+this.offset,this.normalized&&(t=dt(t,this.array),n=dt(n,this.array)),this.data.array[e+0]=t,this.data.array[e+1]=n,this}setXYZ(e,t,n,i){return e=e*this.data.stride+this.offset,this.normalized&&(t=dt(t,this.array),n=dt(n,this.array),i=dt(i,this.array)),this.data.array[e+0]=t,this.data.array[e+1]=n,this.data.array[e+2]=i,this}setXYZW(e,t,n,i,r){return e=e*this.data.stride+this.offset,this.normalized&&(t=dt(t,this.array),n=dt(n,this.array),i=dt(i,this.array),r=dt(r,this.array)),this.data.array[e+0]=t,this.data.array[e+1]=n,this.data.array[e+2]=i,this.data.array[e+3]=r,this}clone(e){if(e===void 0){console.log("THREE.InterleavedBufferAttribute.clone(): Cloning an interleaved buffer attribute will de-interleave buffer data.");const t=[];for(let n=0;n<this.count;n++){const i=n*this.data.stride+this.offset;for(let r=0;r<this.itemSize;r++)t.push(this.data.array[i+r])}return new Rt(new this.array.constructor(t),this.itemSize,this.normalized)}else return e.interleavedBuffers===void 0&&(e.interleavedBuffers={}),e.interleavedBuffers[this.data.uuid]===void 0&&(e.interleavedBuffers[this.data.uuid]=this.data.clone(e)),new Er(e.interleavedBuffers[this.data.uuid],this.itemSize,this.offset,this.normalized)}toJSON(e){if(e===void 0){console.log("THREE.InterleavedBufferAttribute.toJSON(): Serializing an interleaved buffer attribute will de-interleave buffer data.");const t=[];for(let n=0;n<this.count;n++){const i=n*this.data.stride+this.offset;for(let r=0;r<this.itemSize;r++)t.push(this.data.array[i+r])}return{itemSize:this.itemSize,type:this.array.constructor.name,array:t,normalized:this.normalized}}else return e.interleavedBuffers===void 0&&(e.interleavedBuffers={}),e.interleavedBuffers[this.data.uuid]===void 0&&(e.interleavedBuffers[this.data.uuid]=this.data.toJSON(e)),{isInterleavedBufferAttribute:!0,itemSize:this.itemSize,data:this.data.uuid,offset:this.offset,normalized:this.normalized}}}class Ph extends hn{constructor(e){super(),this.isSpriteMaterial=!0,this.type="SpriteMaterial",this.color=new Oe(16777215),this.map=null,this.alphaMap=null,this.rotation=0,this.sizeAttenuation=!0,this.transparent=!0,this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.color.copy(e.color),this.map=e.map,this.alphaMap=e.alphaMap,this.rotation=e.rotation,this.sizeAttenuation=e.sizeAttenuation,this.fog=e.fog,this}}let jr;const Rs=new O,Kr=new O,Jr=new O,$r=new se,Ps=new se,Dp=new Ke,ta=new O,Is=new O,na=new O,cd=new se,Ql=new se,hd=new se;class Np extends xt{constructor(e=new Ph){if(super(),this.isSprite=!0,this.type="Sprite",jr===void 0){jr=new at;const t=new Float32Array([-.5,-.5,0,0,0,.5,-.5,0,1,0,.5,.5,0,1,1,-.5,.5,0,0,1]),n=new nl(t,5);jr.setIndex([0,1,2,0,2,3]),jr.setAttribute("position",new Er(n,3,0,!1)),jr.setAttribute("uv",new Er(n,2,3,!1))}this.geometry=jr,this.material=e,this.center=new se(.5,.5)}raycast(e,t){e.camera===null&&console.error('THREE.Sprite: "Raycaster.camera" needs to be set in order to raycast against sprites.'),Kr.setFromMatrixScale(this.matrixWorld),Dp.copy(e.camera.matrixWorld),this.modelViewMatrix.multiplyMatrices(e.camera.matrixWorldInverse,this.matrixWorld),Jr.setFromMatrixPosition(this.modelViewMatrix),e.camera.isPerspectiveCamera&&this.material.sizeAttenuation===!1&&Kr.multiplyScalar(-Jr.z);const n=this.material.rotation;let i,r;n!==0&&(r=Math.cos(n),i=Math.sin(n));const o=this.center;ia(ta.set(-.5,-.5,0),Jr,o,Kr,i,r),ia(Is.set(.5,-.5,0),Jr,o,Kr,i,r),ia(na.set(.5,.5,0),Jr,o,Kr,i,r),cd.set(0,0),Ql.set(1,0),hd.set(1,1);let a=e.ray.intersectTriangle(ta,Is,na,!1,Rs);if(a===null&&(ia(Is.set(-.5,.5,0),Jr,o,Kr,i,r),Ql.set(0,1),a=e.ray.intersectTriangle(ta,na,Is,!1,Rs),a===null))return;const l=e.ray.origin.distanceTo(Rs);l<e.near||l>e.far||t.push({distance:l,point:Rs.clone(),uv:An.getInterpolation(Rs,ta,Is,na,cd,Ql,hd,new se),face:null,object:this})}copy(e,t){return super.copy(e,t),e.center!==void 0&&this.center.copy(e.center),this.material=e.material,this}}function ia(s,e,t,n,i,r){$r.subVectors(s,t).addScalar(.5).multiply(n),i!==void 0?(Ps.x=r*$r.x-i*$r.y,Ps.y=i*$r.x+r*$r.y):Ps.copy($r),s.copy(e),s.x+=Ps.x,s.y+=Ps.y,s.applyMatrix4(Dp)}const ra=new O,ud=new O;class Op extends xt{constructor(){super(),this._currentLevel=0,this.type="LOD",Object.defineProperties(this,{levels:{enumerable:!0,value:[]},isLOD:{value:!0}}),this.autoUpdate=!0}copy(e){super.copy(e,!1);const t=e.levels;for(let n=0,i=t.length;n<i;n++){const r=t[n];this.addLevel(r.object.clone(),r.distance,r.hysteresis)}return this.autoUpdate=e.autoUpdate,this}addLevel(e,t=0,n=0){t=Math.abs(t);const i=this.levels;let r;for(r=0;r<i.length&&!(t<i[r].distance);r++);return i.splice(r,0,{distance:t,hysteresis:n,object:e}),this.add(e),this}getCurrentLevel(){return this._currentLevel}getObjectForDistance(e){const t=this.levels;if(t.length>0){let n,i;for(n=1,i=t.length;n<i;n++){let r=t[n].distance;if(t[n].object.visible&&(r-=r*t[n].hysteresis),e<r)break}return t[n-1].object}return null}raycast(e,t){if(this.levels.length>0){ra.setFromMatrixPosition(this.matrixWorld);const i=e.ray.origin.distanceTo(ra);this.getObjectForDistance(i).raycast(e,t)}}update(e){const t=this.levels;if(t.length>1){ra.setFromMatrixPosition(e.matrixWorld),ud.setFromMatrixPosition(this.matrixWorld);const n=ra.distanceTo(ud)/e.zoom;t[0].object.visible=!0;let i,r;for(i=1,r=t.length;i<r;i++){let o=t[i].distance;if(t[i].object.visible&&(o-=o*t[i].hysteresis),n>=o)t[i-1].object.visible=!1,t[i].object.visible=!0;else break}for(this._currentLevel=i-1;i<r;i++)t[i].object.visible=!1}}toJSON(e){const t=super.toJSON(e);this.autoUpdate===!1&&(t.object.autoUpdate=!1),t.object.levels=[];const n=this.levels;for(let i=0,r=n.length;i<r;i++){const o=n[i];t.object.levels.push({object:o.object.uuid,distance:o.distance,hysteresis:o.hysteresis})}return t}}const dd=new O,fd=new Ct,pd=new Ct,CM=new O,md=new Ke,sa=new O,ec=new cn,gd=new Ke,tc=new Ar;class Up extends Ie{constructor(e,t){super(e,t),this.isSkinnedMesh=!0,this.type="SkinnedMesh",this.bindMode=yc,this.bindMatrix=new Ke,this.bindMatrixInverse=new Ke,this.boundingBox=null,this.boundingSphere=null}computeBoundingBox(){const e=this.geometry;this.boundingBox===null&&(this.boundingBox=new yn),this.boundingBox.makeEmpty();const t=e.getAttribute("position");for(let n=0;n<t.count;n++)this.getVertexPosition(n,sa),this.boundingBox.expandByPoint(sa)}computeBoundingSphere(){const e=this.geometry;this.boundingSphere===null&&(this.boundingSphere=new cn),this.boundingSphere.makeEmpty();const t=e.getAttribute("position");for(let n=0;n<t.count;n++)this.getVertexPosition(n,sa),this.boundingSphere.expandByPoint(sa)}copy(e,t){return super.copy(e,t),this.bindMode=e.bindMode,this.bindMatrix.copy(e.bindMatrix),this.bindMatrixInverse.copy(e.bindMatrixInverse),this.skeleton=e.skeleton,e.boundingBox!==null&&(this.boundingBox=e.boundingBox.clone()),e.boundingSphere!==null&&(this.boundingSphere=e.boundingSphere.clone()),this}raycast(e,t){const n=this.material,i=this.matrixWorld;n!==void 0&&(this.boundingSphere===null&&this.computeBoundingSphere(),ec.copy(this.boundingSphere),ec.applyMatrix4(i),e.ray.intersectsSphere(ec)!==!1&&(gd.copy(i).invert(),tc.copy(e.ray).applyMatrix4(gd),!(this.boundingBox!==null&&tc.intersectsBox(this.boundingBox)===!1)&&this._computeIntersections(e,t,tc)))}getVertexPosition(e,t){return super.getVertexPosition(e,t),this.applyBoneTransform(e,t),t}bind(e,t){this.skeleton=e,t===void 0&&(this.updateMatrixWorld(!0),this.skeleton.calculateInverses(),t=this.matrixWorld),this.bindMatrix.copy(t),this.bindMatrixInverse.copy(t).invert()}pose(){this.skeleton.pose()}normalizeSkinWeights(){const e=new Ct,t=this.geometry.attributes.skinWeight;for(let n=0,i=t.count;n<i;n++){e.fromBufferAttribute(t,n);const r=1/e.manhattanLength();r!==1/0?e.multiplyScalar(r):e.set(1,0,0,0),t.setXYZW(n,e.x,e.y,e.z,e.w)}}updateMatrixWorld(e){super.updateMatrixWorld(e),this.bindMode===yc?this.bindMatrixInverse.copy(this.matrixWorld).invert():this.bindMode===Yf?this.bindMatrixInverse.copy(this.bindMatrix).invert():console.warn("THREE.SkinnedMesh: Unrecognized bindMode: "+this.bindMode)}applyBoneTransform(e,t){const n=this.skeleton,i=this.geometry;fd.fromBufferAttribute(i.attributes.skinIndex,e),pd.fromBufferAttribute(i.attributes.skinWeight,e),dd.copy(t).applyMatrix4(this.bindMatrix),t.set(0,0,0);for(let r=0;r<4;r++){const o=pd.getComponent(r);if(o!==0){const a=fd.getComponent(r);md.multiplyMatrices(n.bones[a].matrixWorld,n.boneInverses[a]),t.addScaledVector(CM.copy(dd).applyMatrix4(md),o)}}return t.applyMatrix4(this.bindMatrixInverse)}}class Ih extends xt{constructor(){super(),this.isBone=!0,this.type="Bone"}}class Vi extends Vt{constructor(e=null,t=1,n=1,i,r,o,a,l,u=nn,d=nn,m,p){super(null,o,a,l,u,d,i,r,m,p),this.isDataTexture=!0,this.image={data:e,width:t,height:n},this.generateMipmaps=!1,this.flipY=!1,this.unpackAlignment=1}}const vd=new Ke,RM=new Ke;class il{constructor(e=[],t=[]){this.uuid=Rn(),this.bones=e.slice(0),this.boneInverses=t,this.boneMatrices=null,this.boneTexture=null,this.init()}init(){const e=this.bones,t=this.boneInverses;if(this.boneMatrices=new Float32Array(e.length*16),t.length===0)this.calculateInverses();else if(e.length!==t.length){console.warn("THREE.Skeleton: Number of inverse bone matrices does not match amount of bones."),this.boneInverses=[];for(let n=0,i=this.bones.length;n<i;n++)this.boneInverses.push(new Ke)}}calculateInverses(){this.boneInverses.length=0;for(let e=0,t=this.bones.length;e<t;e++){const n=new Ke;this.bones[e]&&n.copy(this.bones[e].matrixWorld).invert(),this.boneInverses.push(n)}}pose(){for(let e=0,t=this.bones.length;e<t;e++){const n=this.bones[e];n&&n.matrixWorld.copy(this.boneInverses[e]).invert()}for(let e=0,t=this.bones.length;e<t;e++){const n=this.bones[e];n&&(n.parent&&n.parent.isBone?(n.matrix.copy(n.parent.matrixWorld).invert(),n.matrix.multiply(n.matrixWorld)):n.matrix.copy(n.matrixWorld),n.matrix.decompose(n.position,n.quaternion,n.scale))}}update(){const e=this.bones,t=this.boneInverses,n=this.boneMatrices,i=this.boneTexture;for(let r=0,o=e.length;r<o;r++){const a=e[r]?e[r].matrixWorld:RM;vd.multiplyMatrices(a,t[r]),vd.toArray(n,r*16)}i!==null&&(i.needsUpdate=!0)}clone(){return new il(this.bones,this.boneInverses)}computeBoneTexture(){let e=Math.sqrt(this.bones.length*4);e=Math.ceil(e/4)*4,e=Math.max(e,4);const t=new Float32Array(e*e*4);t.set(this.boneMatrices);const n=new Vi(t,e,e,Cn,zn);return n.needsUpdate=!0,this.boneMatrices=t,this.boneTexture=n,this}getBoneByName(e){for(let t=0,n=this.bones.length;t<n;t++){const i=this.bones[t];if(i.name===e)return i}}dispose(){this.boneTexture!==null&&(this.boneTexture.dispose(),this.boneTexture=null)}fromJSON(e,t){this.uuid=e.uuid;for(let n=0,i=e.bones.length;n<i;n++){const r=e.bones[n];let o=t[r];o===void 0&&(console.warn("THREE.Skeleton: No bone found with UUID:",r),o=new Ih),this.bones.push(o),this.boneInverses.push(new Ke().fromArray(e.boneInverses[n]))}return this.init(),this}toJSON(){const e={metadata:{version:4.6,type:"Skeleton",generator:"Skeleton.toJSON"},bones:[],boneInverses:[]};e.uuid=this.uuid;const t=this.bones,n=this.boneInverses;for(let i=0,r=t.length;i<r;i++){const o=t[i];e.bones.push(o.uuid);const a=n[i];e.boneInverses.push(a.toArray())}return e}}class hs extends Rt{constructor(e,t,n,i=1){super(e,t,n),this.isInstancedBufferAttribute=!0,this.meshPerAttribute=i}copy(e){return super.copy(e),this.meshPerAttribute=e.meshPerAttribute,this}toJSON(){const e=super.toJSON();return e.meshPerAttribute=this.meshPerAttribute,e.isInstancedBufferAttribute=!0,e}}const Qr=new Ke,_d=new Ke,oa=[],yd=new yn,PM=new Ke,Ls=new Ie,Ds=new cn;class Fp extends Ie{constructor(e,t,n){super(e,t),this.isInstancedMesh=!0,this.instanceMatrix=new hs(new Float32Array(n*16),16),this.instanceColor=null,this.morphTexture=null,this.count=n,this.boundingBox=null,this.boundingSphere=null;for(let i=0;i<n;i++)this.setMatrixAt(i,PM)}computeBoundingBox(){const e=this.geometry,t=this.count;this.boundingBox===null&&(this.boundingBox=new yn),e.boundingBox===null&&e.computeBoundingBox(),this.boundingBox.makeEmpty();for(let n=0;n<t;n++)this.getMatrixAt(n,Qr),yd.copy(e.boundingBox).applyMatrix4(Qr),this.boundingBox.union(yd)}computeBoundingSphere(){const e=this.geometry,t=this.count;this.boundingSphere===null&&(this.boundingSphere=new cn),e.boundingSphere===null&&e.computeBoundingSphere(),this.boundingSphere.makeEmpty();for(let n=0;n<t;n++)this.getMatrixAt(n,Qr),Ds.copy(e.boundingSphere).applyMatrix4(Qr),this.boundingSphere.union(Ds)}copy(e,t){return super.copy(e,t),this.instanceMatrix.copy(e.instanceMatrix),e.morphTexture!==null&&(this.morphTexture=e.morphTexture.clone()),e.instanceColor!==null&&(this.instanceColor=e.instanceColor.clone()),this.count=e.count,e.boundingBox!==null&&(this.boundingBox=e.boundingBox.clone()),e.boundingSphere!==null&&(this.boundingSphere=e.boundingSphere.clone()),this}getColorAt(e,t){t.fromArray(this.instanceColor.array,e*3)}getMatrixAt(e,t){t.fromArray(this.instanceMatrix.array,e*16)}getMorphAt(e,t){const n=t.morphTargetInfluences,i=this.morphTexture.source.data.data,r=n.length+1,o=e*r+1;for(let a=0;a<n.length;a++)n[a]=i[o+a]}raycast(e,t){const n=this.matrixWorld,i=this.count;if(Ls.geometry=this.geometry,Ls.material=this.material,Ls.material!==void 0&&(this.boundingSphere===null&&this.computeBoundingSphere(),Ds.copy(this.boundingSphere),Ds.applyMatrix4(n),e.ray.intersectsSphere(Ds)!==!1))for(let r=0;r<i;r++){this.getMatrixAt(r,Qr),_d.multiplyMatrices(n,Qr),Ls.matrixWorld=_d,Ls.raycast(e,oa);for(let o=0,a=oa.length;o<a;o++){const l=oa[o];l.instanceId=r,l.object=this,t.push(l)}oa.length=0}}setColorAt(e,t){this.instanceColor===null&&(this.instanceColor=new hs(new Float32Array(this.instanceMatrix.count*3),3)),t.toArray(this.instanceColor.array,e*3)}setMatrixAt(e,t){t.toArray(this.instanceMatrix.array,e*16)}setMorphAt(e,t){const n=t.morphTargetInfluences,i=n.length+1;this.morphTexture===null&&(this.morphTexture=new Vi(new Float32Array(i*this.count),i,this.count,mh,zn));const r=this.morphTexture.source.data.data;let o=0;for(let u=0;u<n.length;u++)o+=n[u];const a=this.geometry.morphTargetsRelative?1:1-o,l=i*e;r[l]=a,r.set(n,l+1)}updateMorphTargets(){}dispose(){return this.dispatchEvent({type:"dispose"}),this.morphTexture!==null&&(this.morphTexture.dispose(),this.morphTexture=null),this}}function IM(s,e){return s.z-e.z}function LM(s,e){return e.z-s.z}class DM{constructor(){this.index=0,this.pool=[],this.list=[]}push(e,t){const n=this.pool,i=this.list;this.index>=n.length&&n.push({start:-1,count:-1,z:-1});const r=n[this.index];i.push(r),this.index++,r.start=e.start,r.count=e.count,r.z=t}reset(){this.list.length=0,this.index=0}}const es="batchId",Ui=new Ke,xd=new Ke,NM=new Ke,Md=new Ke,nc=new Mo,aa=new yn,ir=new cn,Ns=new O,ic=new DM,on=new Ie,la=[];function OM(s,e,t=0){const n=e.itemSize;if(s.isInterleavedBufferAttribute||s.array.constructor!==e.array.constructor){const i=s.count;for(let r=0;r<i;r++)for(let o=0;o<n;o++)e.setComponent(r+t,o,s.getComponent(r,o))}else e.array.set(s.array,t*n);e.needsUpdate=!0}class Bp extends Ie{get maxGeometryCount(){return this._maxGeometryCount}constructor(e,t,n=t*2,i){super(new at,i),this.isBatchedMesh=!0,this.perObjectFrustumCulled=!0,this.sortObjects=!0,this.boundingBox=null,this.boundingSphere=null,this.customSort=null,this._drawRanges=[],this._reservedRanges=[],this._visibility=[],this._active=[],this._bounds=[],this._maxGeometryCount=e,this._maxVertexCount=t,this._maxIndexCount=n,this._geometryInitialized=!1,this._geometryCount=0,this._multiDrawCounts=new Int32Array(e),this._multiDrawStarts=new Int32Array(e),this._multiDrawCount=0,this._multiDrawInstances=null,this._visibilityChanged=!0,this._matricesTexture=null,this._initMatricesTexture()}_initMatricesTexture(){let e=Math.sqrt(this._maxGeometryCount*4);e=Math.ceil(e/4)*4,e=Math.max(e,4);const t=new Float32Array(e*e*4),n=new Vi(t,e,e,Cn,zn);this._matricesTexture=n}_initializeGeometry(e){const t=this.geometry,n=this._maxVertexCount,i=this._maxGeometryCount,r=this._maxIndexCount;if(this._geometryInitialized===!1){for(const a in e.attributes){const l=e.getAttribute(a),{array:u,itemSize:d,normalized:m}=l,p=new u.constructor(n*d),g=new Rt(p,d,m);t.setAttribute(a,g)}if(e.getIndex()!==null){const a=n>65536?new Uint32Array(r):new Uint16Array(r);t.setIndex(new Rt(a,1))}const o=i>65536?new Uint32Array(n):new Uint16Array(n);t.setAttribute(es,new Rt(o,1)),this._geometryInitialized=!0}}_validateGeometry(e){if(e.getAttribute(es))throw new Error(`BatchedMesh: Geometry cannot use attribute "${es}"`);const t=this.geometry;if(!!e.getIndex()!=!!t.getIndex())throw new Error('BatchedMesh: All geometries must consistently have "index".');for(const n in t.attributes){if(n===es)continue;if(!e.hasAttribute(n))throw new Error(`BatchedMesh: Added geometry missing "${n}". All geometries must have consistent attributes.`);const i=e.getAttribute(n),r=t.getAttribute(n);if(i.itemSize!==r.itemSize||i.normalized!==r.normalized)throw new Error("BatchedMesh: All attributes must have a consistent itemSize and normalized value.")}}setCustomSort(e){return this.customSort=e,this}computeBoundingBox(){this.boundingBox===null&&(this.boundingBox=new yn);const e=this._geometryCount,t=this.boundingBox,n=this._active;t.makeEmpty();for(let i=0;i<e;i++)n[i]!==!1&&(this.getMatrixAt(i,Ui),this.getBoundingBoxAt(i,aa).applyMatrix4(Ui),t.union(aa))}computeBoundingSphere(){this.boundingSphere===null&&(this.boundingSphere=new cn);const e=this._geometryCount,t=this.boundingSphere,n=this._active;t.makeEmpty();for(let i=0;i<e;i++)n[i]!==!1&&(this.getMatrixAt(i,Ui),this.getBoundingSphereAt(i,ir).applyMatrix4(Ui),t.union(ir))}addGeometry(e,t=-1,n=-1){if(this._initializeGeometry(e),this._validateGeometry(e),this._geometryCount>=this._maxGeometryCount)throw new Error("BatchedMesh: Maximum geometry count reached.");const i={vertexStart:-1,vertexCount:-1,indexStart:-1,indexCount:-1};let r=null;const o=this._reservedRanges,a=this._drawRanges,l=this._bounds;this._geometryCount!==0&&(r=o[o.length-1]),t===-1?i.vertexCount=e.getAttribute("position").count:i.vertexCount=t,r===null?i.vertexStart=0:i.vertexStart=r.vertexStart+r.vertexCount;const u=e.getIndex(),d=u!==null;if(d&&(n===-1?i.indexCount=u.count:i.indexCount=n,r===null?i.indexStart=0:i.indexStart=r.indexStart+r.indexCount),i.indexStart!==-1&&i.indexStart+i.indexCount>this._maxIndexCount||i.vertexStart+i.vertexCount>this._maxVertexCount)throw new Error("BatchedMesh: Reserved space request exceeds the maximum buffer size.");const m=this._visibility,p=this._active,g=this._matricesTexture,_=this._matricesTexture.image.data;m.push(!0),p.push(!0);const M=this._geometryCount;this._geometryCount++,NM.toArray(_,M*16),g.needsUpdate=!0,o.push(i),a.push({start:d?i.indexStart:i.vertexStart,count:-1}),l.push({boxInitialized:!1,box:new yn,sphereInitialized:!1,sphere:new cn});const y=this.geometry.getAttribute(es);for(let v=0;v<i.vertexCount;v++)y.setX(i.vertexStart+v,M);return y.needsUpdate=!0,this.setGeometryAt(M,e),M}setGeometryAt(e,t){if(e>=this._geometryCount)throw new Error("BatchedMesh: Maximum geometry count reached.");this._validateGeometry(t);const n=this.geometry,i=n.getIndex()!==null,r=n.getIndex(),o=t.getIndex(),a=this._reservedRanges[e];if(i&&o.count>a.indexCount||t.attributes.position.count>a.vertexCount)throw new Error("BatchedMesh: Reserved space not large enough for provided geometry.");const l=a.vertexStart,u=a.vertexCount;for(const g in n.attributes){if(g===es)continue;const _=t.getAttribute(g),M=n.getAttribute(g);OM(_,M,l);const y=_.itemSize;for(let v=_.count,w=u;v<w;v++){const S=l+v;for(let T=0;T<y;T++)M.setComponent(S,T,0)}M.needsUpdate=!0,M.addUpdateRange(l*y,u*y)}if(i){const g=a.indexStart;for(let _=0;_<o.count;_++)r.setX(g+_,l+o.getX(_));for(let _=o.count,M=a.indexCount;_<M;_++)r.setX(g+_,l);r.needsUpdate=!0,r.addUpdateRange(g,a.indexCount)}const d=this._bounds[e];t.boundingBox!==null?(d.box.copy(t.boundingBox),d.boxInitialized=!0):d.boxInitialized=!1,t.boundingSphere!==null?(d.sphere.copy(t.boundingSphere),d.sphereInitialized=!0):d.sphereInitialized=!1;const m=this._drawRanges[e],p=t.getAttribute("position");return m.count=i?o.count:p.count,this._visibilityChanged=!0,e}deleteGeometry(e){const t=this._active;return e>=t.length||t[e]===!1?this:(t[e]=!1,this._visibilityChanged=!0,this)}getInstanceCountAt(e){return this._multiDrawInstances===null?null:this._multiDrawInstances[e]}setInstanceCountAt(e,t){return this._multiDrawInstances===null&&(this._multiDrawInstances=new Int32Array(this._maxGeometryCount).fill(1)),this._multiDrawInstances[e]=t,e}getBoundingBoxAt(e,t){if(this._active[e]===!1)return null;const i=this._bounds[e],r=i.box,o=this.geometry;if(i.boxInitialized===!1){r.makeEmpty();const a=o.index,l=o.attributes.position,u=this._drawRanges[e];for(let d=u.start,m=u.start+u.count;d<m;d++){let p=d;a&&(p=a.getX(p)),r.expandByPoint(Ns.fromBufferAttribute(l,p))}i.boxInitialized=!0}return t.copy(r),t}getBoundingSphereAt(e,t){if(this._active[e]===!1)return null;const i=this._bounds[e],r=i.sphere,o=this.geometry;if(i.sphereInitialized===!1){r.makeEmpty(),this.getBoundingBoxAt(e,aa),aa.getCenter(r.center);const a=o.index,l=o.attributes.position,u=this._drawRanges[e];let d=0;for(let m=u.start,p=u.start+u.count;m<p;m++){let g=m;a&&(g=a.getX(g)),Ns.fromBufferAttribute(l,g),d=Math.max(d,r.center.distanceToSquared(Ns))}r.radius=Math.sqrt(d),i.sphereInitialized=!0}return t.copy(r),t}setMatrixAt(e,t){const n=this._active,i=this._matricesTexture,r=this._matricesTexture.image.data,o=this._geometryCount;return e>=o||n[e]===!1?this:(t.toArray(r,e*16),i.needsUpdate=!0,this)}getMatrixAt(e,t){const n=this._active,i=this._matricesTexture.image.data,r=this._geometryCount;return e>=r||n[e]===!1?null:t.fromArray(i,e*16)}setVisibleAt(e,t){const n=this._visibility,i=this._active,r=this._geometryCount;return e>=r||i[e]===!1||n[e]===t?this:(n[e]=t,this._visibilityChanged=!0,this)}getVisibleAt(e){const t=this._visibility,n=this._active,i=this._geometryCount;return e>=i||n[e]===!1?!1:t[e]}raycast(e,t){const n=this._visibility,i=this._active,r=this._drawRanges,o=this._geometryCount,a=this.matrixWorld,l=this.geometry;on.material=this.material,on.geometry.index=l.index,on.geometry.attributes=l.attributes,on.geometry.boundingBox===null&&(on.geometry.boundingBox=new yn),on.geometry.boundingSphere===null&&(on.geometry.boundingSphere=new cn);for(let u=0;u<o;u++){if(!n[u]||!i[u])continue;const d=r[u];on.geometry.setDrawRange(d.start,d.count),this.getMatrixAt(u,on.matrixWorld).premultiply(a),this.getBoundingBoxAt(u,on.geometry.boundingBox),this.getBoundingSphereAt(u,on.geometry.boundingSphere),on.raycast(e,la);for(let m=0,p=la.length;m<p;m++){const g=la[m];g.object=this,g.batchId=u,t.push(g)}la.length=0}on.material=null,on.geometry.index=null,on.geometry.attributes={},on.geometry.setDrawRange(0,1/0)}copy(e){return super.copy(e),this.geometry=e.geometry.clone(),this.perObjectFrustumCulled=e.perObjectFrustumCulled,this.sortObjects=e.sortObjects,this.boundingBox=e.boundingBox!==null?e.boundingBox.clone():null,this.boundingSphere=e.boundingSphere!==null?e.boundingSphere.clone():null,this._drawRanges=e._drawRanges.map(t=>({...t})),this._reservedRanges=e._reservedRanges.map(t=>({...t})),this._visibility=e._visibility.slice(),this._active=e._active.slice(),this._bounds=e._bounds.map(t=>({boxInitialized:t.boxInitialized,box:t.box.clone(),sphereInitialized:t.sphereInitialized,sphere:t.sphere.clone()})),this._maxGeometryCount=e._maxGeometryCount,this._maxVertexCount=e._maxVertexCount,this._maxIndexCount=e._maxIndexCount,this._geometryInitialized=e._geometryInitialized,this._geometryCount=e._geometryCount,this._multiDrawCounts=e._multiDrawCounts.slice(),this._multiDrawStarts=e._multiDrawStarts.slice(),this._matricesTexture=e._matricesTexture.clone(),this._matricesTexture.image.data=this._matricesTexture.image.slice(),this}dispose(){return this.geometry.dispose(),this._matricesTexture.dispose(),this._matricesTexture=null,this}onBeforeRender(e,t,n,i,r){if(!this._visibilityChanged&&!this.perObjectFrustumCulled&&!this.sortObjects)return;const o=i.getIndex(),a=o===null?1:o.array.BYTES_PER_ELEMENT,l=this._active,u=this._visibility,d=this._multiDrawStarts,m=this._multiDrawCounts,p=this._drawRanges,g=this.perObjectFrustumCulled;g&&(Md.multiplyMatrices(n.projectionMatrix,n.matrixWorldInverse).multiply(this.matrixWorld),nc.setFromProjectionMatrix(Md,e.coordinateSystem));let _=0;if(this.sortObjects){xd.copy(this.matrixWorld).invert(),Ns.setFromMatrixPosition(n.matrixWorld).applyMatrix4(xd);for(let v=0,w=u.length;v<w;v++)if(u[v]&&l[v]){this.getMatrixAt(v,Ui),this.getBoundingSphereAt(v,ir).applyMatrix4(Ui);let S=!1;if(g&&(S=!nc.intersectsSphere(ir)),!S){const T=Ns.distanceTo(ir.center);ic.push(p[v],T)}}const M=ic.list,y=this.customSort;y===null?M.sort(r.transparent?LM:IM):y.call(this,M,n);for(let v=0,w=M.length;v<w;v++){const S=M[v];d[_]=S.start*a,m[_]=S.count,_++}ic.reset()}else for(let M=0,y=u.length;M<y;M++)if(u[M]&&l[M]){let v=!1;if(g&&(this.getMatrixAt(M,Ui),this.getBoundingSphereAt(M,ir).applyMatrix4(Ui),v=!nc.intersectsSphere(ir)),!v){const w=p[M];d[_]=w.start*a,m[_]=w.count,_++}}this._multiDrawCount=_,this._visibilityChanged=!1}onBeforeShadow(e,t,n,i,r,o){this.onBeforeRender(e,null,i,r,o)}}class un extends hn{constructor(e){super(),this.isLineBasicMaterial=!0,this.type="LineBasicMaterial",this.color=new Oe(16777215),this.map=null,this.linewidth=1,this.linecap="round",this.linejoin="round",this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.color.copy(e.color),this.map=e.map,this.linewidth=e.linewidth,this.linecap=e.linecap,this.linejoin=e.linejoin,this.fog=e.fog,this}}const Ha=new O,Ga=new O,bd=new Ke,Os=new Ar,ca=new cn,rc=new O,Sd=new O;class ln extends xt{constructor(e=new at,t=new un){super(),this.isLine=!0,this.type="Line",this.geometry=e,this.material=t,this.updateMorphTargets()}copy(e,t){return super.copy(e,t),this.material=Array.isArray(e.material)?e.material.slice():e.material,this.geometry=e.geometry,this}computeLineDistances(){const e=this.geometry;if(e.index===null){const t=e.attributes.position,n=[0];for(let i=1,r=t.count;i<r;i++)Ha.fromBufferAttribute(t,i-1),Ga.fromBufferAttribute(t,i),n[i]=n[i-1],n[i]+=Ha.distanceTo(Ga);e.setAttribute("lineDistance",new He(n,1))}else console.warn("THREE.Line.computeLineDistances(): Computation only possible with non-indexed BufferGeometry.");return this}raycast(e,t){const n=this.geometry,i=this.matrixWorld,r=e.params.Line.threshold,o=n.drawRange;if(n.boundingSphere===null&&n.computeBoundingSphere(),ca.copy(n.boundingSphere),ca.applyMatrix4(i),ca.radius+=r,e.ray.intersectsSphere(ca)===!1)return;bd.copy(i).invert(),Os.copy(e.ray).applyMatrix4(bd);const a=r/((this.scale.x+this.scale.y+this.scale.z)/3),l=a*a,u=this.isLineSegments?2:1,d=n.index,p=n.attributes.position;if(d!==null){const g=Math.max(0,o.start),_=Math.min(d.count,o.start+o.count);for(let M=g,y=_-1;M<y;M+=u){const v=d.getX(M),w=d.getX(M+1),S=ha(this,e,Os,l,v,w);S&&t.push(S)}if(this.isLineLoop){const M=d.getX(_-1),y=d.getX(g),v=ha(this,e,Os,l,M,y);v&&t.push(v)}}else{const g=Math.max(0,o.start),_=Math.min(p.count,o.start+o.count);for(let M=g,y=_-1;M<y;M+=u){const v=ha(this,e,Os,l,M,M+1);v&&t.push(v)}if(this.isLineLoop){const M=ha(this,e,Os,l,_-1,g);M&&t.push(M)}}}updateMorphTargets(){const t=this.geometry.morphAttributes,n=Object.keys(t);if(n.length>0){const i=t[n[0]];if(i!==void 0){this.morphTargetInfluences=[],this.morphTargetDictionary={};for(let r=0,o=i.length;r<o;r++){const a=i[r].name||String(r);this.morphTargetInfluences.push(0),this.morphTargetDictionary[a]=r}}}}}function ha(s,e,t,n,i,r){const o=s.geometry.attributes.position;if(Ha.fromBufferAttribute(o,i),Ga.fromBufferAttribute(o,r),t.distanceSqToSegment(Ha,Ga,rc,Sd)>n)return;rc.applyMatrix4(s.matrixWorld);const l=e.ray.origin.distanceTo(rc);if(!(l<e.near||l>e.far))return{distance:l,point:Sd.clone().applyMatrix4(s.matrixWorld),index:i,face:null,faceIndex:null,object:s}}const wd=new O,Ed=new O;class ri extends ln{constructor(e,t){super(e,t),this.isLineSegments=!0,this.type="LineSegments"}computeLineDistances(){const e=this.geometry;if(e.index===null){const t=e.attributes.position,n=[];for(let i=0,r=t.count;i<r;i+=2)wd.fromBufferAttribute(t,i),Ed.fromBufferAttribute(t,i+1),n[i]=i===0?0:n[i-1],n[i+1]=n[i]+wd.distanceTo(Ed);e.setAttribute("lineDistance",new He(n,1))}else console.warn("THREE.LineSegments.computeLineDistances(): Computation only possible with non-indexed BufferGeometry.");return this}}class zp extends ln{constructor(e,t){super(e,t),this.isLineLoop=!0,this.type="LineLoop"}}class Lh extends hn{constructor(e){super(),this.isPointsMaterial=!0,this.type="PointsMaterial",this.color=new Oe(16777215),this.map=null,this.alphaMap=null,this.size=1,this.sizeAttenuation=!0,this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.color.copy(e.color),this.map=e.map,this.alphaMap=e.alphaMap,this.size=e.size,this.sizeAttenuation=e.sizeAttenuation,this.fog=e.fog,this}}const Td=new Ke,Kc=new Ar,ua=new cn,da=new O;class kp extends xt{constructor(e=new at,t=new Lh){super(),this.isPoints=!0,this.type="Points",this.geometry=e,this.material=t,this.updateMorphTargets()}copy(e,t){return super.copy(e,t),this.material=Array.isArray(e.material)?e.material.slice():e.material,this.geometry=e.geometry,this}raycast(e,t){const n=this.geometry,i=this.matrixWorld,r=e.params.Points.threshold,o=n.drawRange;if(n.boundingSphere===null&&n.computeBoundingSphere(),ua.copy(n.boundingSphere),ua.applyMatrix4(i),ua.radius+=r,e.ray.intersectsSphere(ua)===!1)return;Td.copy(i).invert(),Kc.copy(e.ray).applyMatrix4(Td);const a=r/((this.scale.x+this.scale.y+this.scale.z)/3),l=a*a,u=n.index,m=n.attributes.position;if(u!==null){const p=Math.max(0,o.start),g=Math.min(u.count,o.start+o.count);for(let _=p,M=g;_<M;_++){const y=u.getX(_);da.fromBufferAttribute(m,y),Ad(da,y,l,i,e,t,this)}}else{const p=Math.max(0,o.start),g=Math.min(m.count,o.start+o.count);for(let _=p,M=g;_<M;_++)da.fromBufferAttribute(m,_),Ad(da,_,l,i,e,t,this)}}updateMorphTargets(){const t=this.geometry.morphAttributes,n=Object.keys(t);if(n.length>0){const i=t[n[0]];if(i!==void 0){this.morphTargetInfluences=[],this.morphTargetDictionary={};for(let r=0,o=i.length;r<o;r++){const a=i[r].name||String(r);this.morphTargetInfluences.push(0),this.morphTargetDictionary[a]=r}}}}}function Ad(s,e,t,n,i,r,o){const a=Kc.distanceSqToPoint(s);if(a<t){const l=new O;Kc.closestPointToPoint(s,l),l.applyMatrix4(n);const u=i.ray.origin.distanceTo(l);if(u<i.near||u>i.far)return;r.push({distance:u,distanceToRay:Math.sqrt(a),point:l,index:e,face:null,object:o})}}class UM extends Vt{constructor(e,t,n,i,r,o,a,l,u){super(e,t,n,i,r,o,a,l,u),this.isVideoTexture=!0,this.minFilter=o!==void 0?o:Xt,this.magFilter=r!==void 0?r:Xt,this.generateMipmaps=!1;const d=this;function m(){d.needsUpdate=!0,e.requestVideoFrameCallback(m)}"requestVideoFrameCallback"in e&&e.requestVideoFrameCallback(m)}clone(){return new this.constructor(this.image).copy(this)}update(){const e=this.image;"requestVideoFrameCallback"in e===!1&&e.readyState>=e.HAVE_CURRENT_DATA&&(this.needsUpdate=!0)}}class FM extends Vt{constructor(e,t){super({width:e,height:t}),this.isFramebufferTexture=!0,this.magFilter=nn,this.minFilter=nn,this.generateMipmaps=!1,this.needsUpdate=!0}}class rl extends Vt{constructor(e,t,n,i,r,o,a,l,u,d,m,p){super(null,o,a,l,u,d,i,r,m,p),this.isCompressedTexture=!0,this.image={width:t,height:n},this.mipmaps=e,this.flipY=!1,this.generateMipmaps=!1}}class BM extends rl{constructor(e,t,n,i,r,o){super(e,t,n,r,o),this.isCompressedArrayTexture=!0,this.image.depth=i,this.wrapR=Bn}}class zM extends rl{constructor(e,t,n){super(void 0,e[0].width,e[0].height,t,n,Mi),this.isCompressedCubeTexture=!0,this.isCubeTexture=!0,this.image=e}}class kM extends Vt{constructor(e,t,n,i,r,o,a,l,u){super(e,t,n,i,r,o,a,l,u),this.isCanvasTexture=!0,this.needsUpdate=!0}}class Yn{constructor(){this.type="Curve",this.arcLengthDivisions=200}getPoint(){return console.warn("THREE.Curve: .getPoint() not implemented."),null}getPointAt(e,t){const n=this.getUtoTmapping(e);return this.getPoint(n,t)}getPoints(e=5){const t=[];for(let n=0;n<=e;n++)t.push(this.getPoint(n/e));return t}getSpacedPoints(e=5){const t=[];for(let n=0;n<=e;n++)t.push(this.getPointAt(n/e));return t}getLength(){const e=this.getLengths();return e[e.length-1]}getLengths(e=this.arcLengthDivisions){if(this.cacheArcLengths&&this.cacheArcLengths.length===e+1&&!this.needsUpdate)return this.cacheArcLengths;this.needsUpdate=!1;const t=[];let n,i=this.getPoint(0),r=0;t.push(0);for(let o=1;o<=e;o++)n=this.getPoint(o/e),r+=n.distanceTo(i),t.push(r),i=n;return this.cacheArcLengths=t,t}updateArcLengths(){this.needsUpdate=!0,this.getLengths()}getUtoTmapping(e,t){const n=this.getLengths();let i=0;const r=n.length;let o;t?o=t:o=e*n[r-1];let a=0,l=r-1,u;for(;a<=l;)if(i=Math.floor(a+(l-a)/2),u=n[i]-o,u<0)a=i+1;else if(u>0)l=i-1;else{l=i;break}if(i=l,n[i]===o)return i/(r-1);const d=n[i],p=n[i+1]-d,g=(o-d)/p;return(i+g)/(r-1)}getTangent(e,t){let i=e-1e-4,r=e+1e-4;i<0&&(i=0),r>1&&(r=1);const o=this.getPoint(i),a=this.getPoint(r),l=t||(o.isVector2?new se:new O);return l.copy(a).sub(o).normalize(),l}getTangentAt(e,t){const n=this.getUtoTmapping(e);return this.getTangent(n,t)}computeFrenetFrames(e,t){const n=new O,i=[],r=[],o=[],a=new O,l=new Ke;for(let g=0;g<=e;g++){const _=g/e;i[g]=this.getTangentAt(_,new O)}r[0]=new O,o[0]=new O;let u=Number.MAX_VALUE;const d=Math.abs(i[0].x),m=Math.abs(i[0].y),p=Math.abs(i[0].z);d<=u&&(u=d,n.set(1,0,0)),m<=u&&(u=m,n.set(0,1,0)),p<=u&&n.set(0,0,1),a.crossVectors(i[0],n).normalize(),r[0].crossVectors(i[0],a),o[0].crossVectors(i[0],r[0]);for(let g=1;g<=e;g++){if(r[g]=r[g-1].clone(),o[g]=o[g-1].clone(),a.crossVectors(i[g-1],i[g]),a.length()>Number.EPSILON){a.normalize();const _=Math.acos(kt(i[g-1].dot(i[g]),-1,1));r[g].applyMatrix4(l.makeRotationAxis(a,_))}o[g].crossVectors(i[g],r[g])}if(t===!0){let g=Math.acos(kt(r[0].dot(r[e]),-1,1));g/=e,i[0].dot(a.crossVectors(r[0],r[e]))>0&&(g=-g);for(let _=1;_<=e;_++)r[_].applyMatrix4(l.makeRotationAxis(i[_],g*_)),o[_].crossVectors(i[_],r[_])}return{tangents:i,normals:r,binormals:o}}clone(){return new this.constructor().copy(this)}copy(e){return this.arcLengthDivisions=e.arcLengthDivisions,this}toJSON(){const e={metadata:{version:4.6,type:"Curve",generator:"Curve.toJSON"}};return e.arcLengthDivisions=this.arcLengthDivisions,e.type=this.type,e}fromJSON(e){return this.arcLengthDivisions=e.arcLengthDivisions,this}}class sl extends Yn{constructor(e=0,t=0,n=1,i=1,r=0,o=Math.PI*2,a=!1,l=0){super(),this.isEllipseCurve=!0,this.type="EllipseCurve",this.aX=e,this.aY=t,this.xRadius=n,this.yRadius=i,this.aStartAngle=r,this.aEndAngle=o,this.aClockwise=a,this.aRotation=l}getPoint(e,t=new se){const n=t,i=Math.PI*2;let r=this.aEndAngle-this.aStartAngle;const o=Math.abs(r)<Number.EPSILON;for(;r<0;)r+=i;for(;r>i;)r-=i;r<Number.EPSILON&&(o?r=0:r=i),this.aClockwise===!0&&!o&&(r===i?r=-i:r=r-i);const a=this.aStartAngle+e*r;let l=this.aX+this.xRadius*Math.cos(a),u=this.aY+this.yRadius*Math.sin(a);if(this.aRotation!==0){const d=Math.cos(this.aRotation),m=Math.sin(this.aRotation),p=l-this.aX,g=u-this.aY;l=p*d-g*m+this.aX,u=p*m+g*d+this.aY}return n.set(l,u)}copy(e){return super.copy(e),this.aX=e.aX,this.aY=e.aY,this.xRadius=e.xRadius,this.yRadius=e.yRadius,this.aStartAngle=e.aStartAngle,this.aEndAngle=e.aEndAngle,this.aClockwise=e.aClockwise,this.aRotation=e.aRotation,this}toJSON(){const e=super.toJSON();return e.aX=this.aX,e.aY=this.aY,e.xRadius=this.xRadius,e.yRadius=this.yRadius,e.aStartAngle=this.aStartAngle,e.aEndAngle=this.aEndAngle,e.aClockwise=this.aClockwise,e.aRotation=this.aRotation,e}fromJSON(e){return super.fromJSON(e),this.aX=e.aX,this.aY=e.aY,this.xRadius=e.xRadius,this.yRadius=e.yRadius,this.aStartAngle=e.aStartAngle,this.aEndAngle=e.aEndAngle,this.aClockwise=e.aClockwise,this.aRotation=e.aRotation,this}}class Vp extends sl{constructor(e,t,n,i,r,o){super(e,t,n,n,i,r,o),this.isArcCurve=!0,this.type="ArcCurve"}}function Dh(){let s=0,e=0,t=0,n=0;function i(r,o,a,l){s=r,e=a,t=-3*r+3*o-2*a-l,n=2*r-2*o+a+l}return{initCatmullRom:function(r,o,a,l,u){i(o,a,u*(a-r),u*(l-o))},initNonuniformCatmullRom:function(r,o,a,l,u,d,m){let p=(o-r)/u-(a-r)/(u+d)+(a-o)/d,g=(a-o)/d-(l-o)/(d+m)+(l-a)/m;p*=d,g*=d,i(o,a,p,g)},calc:function(r){const o=r*r,a=o*r;return s+e*r+t*o+n*a}}}const fa=new O,sc=new Dh,oc=new Dh,ac=new Dh;class Hp extends Yn{constructor(e=[],t=!1,n="centripetal",i=.5){super(),this.isCatmullRomCurve3=!0,this.type="CatmullRomCurve3",this.points=e,this.closed=t,this.curveType=n,this.tension=i}getPoint(e,t=new O){const n=t,i=this.points,r=i.length,o=(r-(this.closed?0:1))*e;let a=Math.floor(o),l=o-a;this.closed?a+=a>0?0:(Math.floor(Math.abs(a)/r)+1)*r:l===0&&a===r-1&&(a=r-2,l=1);let u,d;this.closed||a>0?u=i[(a-1)%r]:(fa.subVectors(i[0],i[1]).add(i[0]),u=fa);const m=i[a%r],p=i[(a+1)%r];if(this.closed||a+2<r?d=i[(a+2)%r]:(fa.subVectors(i[r-1],i[r-2]).add(i[r-1]),d=fa),this.curveType==="centripetal"||this.curveType==="chordal"){const g=this.curveType==="chordal"?.5:.25;let _=Math.pow(u.distanceToSquared(m),g),M=Math.pow(m.distanceToSquared(p),g),y=Math.pow(p.distanceToSquared(d),g);M<1e-4&&(M=1),_<1e-4&&(_=M),y<1e-4&&(y=M),sc.initNonuniformCatmullRom(u.x,m.x,p.x,d.x,_,M,y),oc.initNonuniformCatmullRom(u.y,m.y,p.y,d.y,_,M,y),ac.initNonuniformCatmullRom(u.z,m.z,p.z,d.z,_,M,y)}else this.curveType==="catmullrom"&&(sc.initCatmullRom(u.x,m.x,p.x,d.x,this.tension),oc.initCatmullRom(u.y,m.y,p.y,d.y,this.tension),ac.initCatmullRom(u.z,m.z,p.z,d.z,this.tension));return n.set(sc.calc(l),oc.calc(l),ac.calc(l)),n}copy(e){super.copy(e),this.points=[];for(let t=0,n=e.points.length;t<n;t++){const i=e.points[t];this.points.push(i.clone())}return this.closed=e.closed,this.curveType=e.curveType,this.tension=e.tension,this}toJSON(){const e=super.toJSON();e.points=[];for(let t=0,n=this.points.length;t<n;t++){const i=this.points[t];e.points.push(i.toArray())}return e.closed=this.closed,e.curveType=this.curveType,e.tension=this.tension,e}fromJSON(e){super.fromJSON(e),this.points=[];for(let t=0,n=e.points.length;t<n;t++){const i=e.points[t];this.points.push(new O().fromArray(i))}return this.closed=e.closed,this.curveType=e.curveType,this.tension=e.tension,this}}function Cd(s,e,t,n,i){const r=(n-e)*.5,o=(i-t)*.5,a=s*s,l=s*a;return(2*t-2*n+r+o)*l+(-3*t+3*n-2*r-o)*a+r*s+t}function VM(s,e){const t=1-s;return t*t*e}function HM(s,e){return 2*(1-s)*s*e}function GM(s,e){return s*s*e}function Ys(s,e,t,n){return VM(s,e)+HM(s,t)+GM(s,n)}function WM(s,e){const t=1-s;return t*t*t*e}function XM(s,e){const t=1-s;return 3*t*t*s*e}function YM(s,e){return 3*(1-s)*s*s*e}function qM(s,e){return s*s*s*e}function qs(s,e,t,n,i){return WM(s,e)+XM(s,t)+YM(s,n)+qM(s,i)}class Nh extends Yn{constructor(e=new se,t=new se,n=new se,i=new se){super(),this.isCubicBezierCurve=!0,this.type="CubicBezierCurve",this.v0=e,this.v1=t,this.v2=n,this.v3=i}getPoint(e,t=new se){const n=t,i=this.v0,r=this.v1,o=this.v2,a=this.v3;return n.set(qs(e,i.x,r.x,o.x,a.x),qs(e,i.y,r.y,o.y,a.y)),n}copy(e){return super.copy(e),this.v0.copy(e.v0),this.v1.copy(e.v1),this.v2.copy(e.v2),this.v3.copy(e.v3),this}toJSON(){const e=super.toJSON();return e.v0=this.v0.toArray(),e.v1=this.v1.toArray(),e.v2=this.v2.toArray(),e.v3=this.v3.toArray(),e}fromJSON(e){return super.fromJSON(e),this.v0.fromArray(e.v0),this.v1.fromArray(e.v1),this.v2.fromArray(e.v2),this.v3.fromArray(e.v3),this}}class Gp extends Yn{constructor(e=new O,t=new O,n=new O,i=new O){super(),this.isCubicBezierCurve3=!0,this.type="CubicBezierCurve3",this.v0=e,this.v1=t,this.v2=n,this.v3=i}getPoint(e,t=new O){const n=t,i=this.v0,r=this.v1,o=this.v2,a=this.v3;return n.set(qs(e,i.x,r.x,o.x,a.x),qs(e,i.y,r.y,o.y,a.y),qs(e,i.z,r.z,o.z,a.z)),n}copy(e){return super.copy(e),this.v0.copy(e.v0),this.v1.copy(e.v1),this.v2.copy(e.v2),this.v3.copy(e.v3),this}toJSON(){const e=super.toJSON();return e.v0=this.v0.toArray(),e.v1=this.v1.toArray(),e.v2=this.v2.toArray(),e.v3=this.v3.toArray(),e}fromJSON(e){return super.fromJSON(e),this.v0.fromArray(e.v0),this.v1.fromArray(e.v1),this.v2.fromArray(e.v2),this.v3.fromArray(e.v3),this}}class Oh extends Yn{constructor(e=new se,t=new se){super(),this.isLineCurve=!0,this.type="LineCurve",this.v1=e,this.v2=t}getPoint(e,t=new se){const n=t;return e===1?n.copy(this.v2):(n.copy(this.v2).sub(this.v1),n.multiplyScalar(e).add(this.v1)),n}getPointAt(e,t){return this.getPoint(e,t)}getTangent(e,t=new se){return t.subVectors(this.v2,this.v1).normalize()}getTangentAt(e,t){return this.getTangent(e,t)}copy(e){return super.copy(e),this.v1.copy(e.v1),this.v2.copy(e.v2),this}toJSON(){const e=super.toJSON();return e.v1=this.v1.toArray(),e.v2=this.v2.toArray(),e}fromJSON(e){return super.fromJSON(e),this.v1.fromArray(e.v1),this.v2.fromArray(e.v2),this}}class Wp extends Yn{constructor(e=new O,t=new O){super(),this.isLineCurve3=!0,this.type="LineCurve3",this.v1=e,this.v2=t}getPoint(e,t=new O){const n=t;return e===1?n.copy(this.v2):(n.copy(this.v2).sub(this.v1),n.multiplyScalar(e).add(this.v1)),n}getPointAt(e,t){return this.getPoint(e,t)}getTangent(e,t=new O){return t.subVectors(this.v2,this.v1).normalize()}getTangentAt(e,t){return this.getTangent(e,t)}copy(e){return super.copy(e),this.v1.copy(e.v1),this.v2.copy(e.v2),this}toJSON(){const e=super.toJSON();return e.v1=this.v1.toArray(),e.v2=this.v2.toArray(),e}fromJSON(e){return super.fromJSON(e),this.v1.fromArray(e.v1),this.v2.fromArray(e.v2),this}}class Uh extends Yn{constructor(e=new se,t=new se,n=new se){super(),this.isQuadraticBezierCurve=!0,this.type="QuadraticBezierCurve",this.v0=e,this.v1=t,this.v2=n}getPoint(e,t=new se){const n=t,i=this.v0,r=this.v1,o=this.v2;return n.set(Ys(e,i.x,r.x,o.x),Ys(e,i.y,r.y,o.y)),n}copy(e){return super.copy(e),this.v0.copy(e.v0),this.v1.copy(e.v1),this.v2.copy(e.v2),this}toJSON(){const e=super.toJSON();return e.v0=this.v0.toArray(),e.v1=this.v1.toArray(),e.v2=this.v2.toArray(),e}fromJSON(e){return super.fromJSON(e),this.v0.fromArray(e.v0),this.v1.fromArray(e.v1),this.v2.fromArray(e.v2),this}}class Fh extends Yn{constructor(e=new O,t=new O,n=new O){super(),this.isQuadraticBezierCurve3=!0,this.type="QuadraticBezierCurve3",this.v0=e,this.v1=t,this.v2=n}getPoint(e,t=new O){const n=t,i=this.v0,r=this.v1,o=this.v2;return n.set(Ys(e,i.x,r.x,o.x),Ys(e,i.y,r.y,o.y),Ys(e,i.z,r.z,o.z)),n}copy(e){return super.copy(e),this.v0.copy(e.v0),this.v1.copy(e.v1),this.v2.copy(e.v2),this}toJSON(){const e=super.toJSON();return e.v0=this.v0.toArray(),e.v1=this.v1.toArray(),e.v2=this.v2.toArray(),e}fromJSON(e){return super.fromJSON(e),this.v0.fromArray(e.v0),this.v1.fromArray(e.v1),this.v2.fromArray(e.v2),this}}class Bh extends Yn{constructor(e=[]){super(),this.isSplineCurve=!0,this.type="SplineCurve",this.points=e}getPoint(e,t=new se){const n=t,i=this.points,r=(i.length-1)*e,o=Math.floor(r),a=r-o,l=i[o===0?o:o-1],u=i[o],d=i[o>i.length-2?i.length-1:o+1],m=i[o>i.length-3?i.length-1:o+2];return n.set(Cd(a,l.x,u.x,d.x,m.x),Cd(a,l.y,u.y,d.y,m.y)),n}copy(e){super.copy(e),this.points=[];for(let t=0,n=e.points.length;t<n;t++){const i=e.points[t];this.points.push(i.clone())}return this}toJSON(){const e=super.toJSON();e.points=[];for(let t=0,n=this.points.length;t<n;t++){const i=this.points[t];e.points.push(i.toArray())}return e}fromJSON(e){super.fromJSON(e),this.points=[];for(let t=0,n=e.points.length;t<n;t++){const i=e.points[t];this.points.push(new se().fromArray(i))}return this}}var Wa=Object.freeze({__proto__:null,ArcCurve:Vp,CatmullRomCurve3:Hp,CubicBezierCurve:Nh,CubicBezierCurve3:Gp,EllipseCurve:sl,LineCurve:Oh,LineCurve3:Wp,QuadraticBezierCurve:Uh,QuadraticBezierCurve3:Fh,SplineCurve:Bh});class Xp extends Yn{constructor(){super(),this.type="CurvePath",this.curves=[],this.autoClose=!1}add(e){this.curves.push(e)}closePath(){const e=this.curves[0].getPoint(0),t=this.curves[this.curves.length-1].getPoint(1);if(!e.equals(t)){const n=e.isVector2===!0?"LineCurve":"LineCurve3";this.curves.push(new Wa[n](t,e))}return this}getPoint(e,t){const n=e*this.getLength(),i=this.getCurveLengths();let r=0;for(;r<i.length;){if(i[r]>=n){const o=i[r]-n,a=this.curves[r],l=a.getLength(),u=l===0?0:1-o/l;return a.getPointAt(u,t)}r++}return null}getLength(){const e=this.getCurveLengths();return e[e.length-1]}updateArcLengths(){this.needsUpdate=!0,this.cacheLengths=null,this.getCurveLengths()}getCurveLengths(){if(this.cacheLengths&&this.cacheLengths.length===this.curves.length)return this.cacheLengths;const e=[];let t=0;for(let n=0,i=this.curves.length;n<i;n++)t+=this.curves[n].getLength(),e.push(t);return this.cacheLengths=e,e}getSpacedPoints(e=40){const t=[];for(let n=0;n<=e;n++)t.push(this.getPoint(n/e));return this.autoClose&&t.push(t[0]),t}getPoints(e=12){const t=[];let n;for(let i=0,r=this.curves;i<r.length;i++){const o=r[i],a=o.isEllipseCurve?e*2:o.isLineCurve||o.isLineCurve3?1:o.isSplineCurve?e*o.points.length:e,l=o.getPoints(a);for(let u=0;u<l.length;u++){const d=l[u];n&&n.equals(d)||(t.push(d),n=d)}}return this.autoClose&&t.length>1&&!t[t.length-1].equals(t[0])&&t.push(t[0]),t}copy(e){super.copy(e),this.curves=[];for(let t=0,n=e.curves.length;t<n;t++){const i=e.curves[t];this.curves.push(i.clone())}return this.autoClose=e.autoClose,this}toJSON(){const e=super.toJSON();e.autoClose=this.autoClose,e.curves=[];for(let t=0,n=this.curves.length;t<n;t++){const i=this.curves[t];e.curves.push(i.toJSON())}return e}fromJSON(e){super.fromJSON(e),this.autoClose=e.autoClose,this.curves=[];for(let t=0,n=e.curves.length;t<n;t++){const i=e.curves[t];this.curves.push(new Wa[i.type]().fromJSON(i))}return this}}class lo extends Xp{constructor(e){super(),this.type="Path",this.currentPoint=new se,e&&this.setFromPoints(e)}setFromPoints(e){this.moveTo(e[0].x,e[0].y);for(let t=1,n=e.length;t<n;t++)this.lineTo(e[t].x,e[t].y);return this}moveTo(e,t){return this.currentPoint.set(e,t),this}lineTo(e,t){const n=new Oh(this.currentPoint.clone(),new se(e,t));return this.curves.push(n),this.currentPoint.set(e,t),this}quadraticCurveTo(e,t,n,i){const r=new Uh(this.currentPoint.clone(),new se(e,t),new se(n,i));return this.curves.push(r),this.currentPoint.set(n,i),this}bezierCurveTo(e,t,n,i,r,o){const a=new Nh(this.currentPoint.clone(),new se(e,t),new se(n,i),new se(r,o));return this.curves.push(a),this.currentPoint.set(r,o),this}splineThru(e){const t=[this.currentPoint.clone()].concat(e),n=new Bh(t);return this.curves.push(n),this.currentPoint.copy(e[e.length-1]),this}arc(e,t,n,i,r,o){const a=this.currentPoint.x,l=this.currentPoint.y;return this.absarc(e+a,t+l,n,i,r,o),this}absarc(e,t,n,i,r,o){return this.absellipse(e,t,n,n,i,r,o),this}ellipse(e,t,n,i,r,o,a,l){const u=this.currentPoint.x,d=this.currentPoint.y;return this.absellipse(e+u,t+d,n,i,r,o,a,l),this}absellipse(e,t,n,i,r,o,a,l){const u=new sl(e,t,n,i,r,o,a,l);if(this.curves.length>0){const m=u.getPoint(0);m.equals(this.currentPoint)||this.lineTo(m.x,m.y)}this.curves.push(u);const d=u.getPoint(1);return this.currentPoint.copy(d),this}copy(e){return super.copy(e),this.currentPoint.copy(e.currentPoint),this}toJSON(){const e=super.toJSON();return e.currentPoint=this.currentPoint.toArray(),e}fromJSON(e){return super.fromJSON(e),this.currentPoint.fromArray(e.currentPoint),this}}class So extends at{constructor(e=[new se(0,-.5),new se(.5,0),new se(0,.5)],t=12,n=0,i=Math.PI*2){super(),this.type="LatheGeometry",this.parameters={points:e,segments:t,phiStart:n,phiLength:i},t=Math.floor(t),i=kt(i,0,Math.PI*2);const r=[],o=[],a=[],l=[],u=[],d=1/t,m=new O,p=new se,g=new O,_=new O,M=new O;let y=0,v=0;for(let w=0;w<=e.length-1;w++)switch(w){case 0:y=e[w+1].x-e[w].x,v=e[w+1].y-e[w].y,g.x=v*1,g.y=-y,g.z=v*0,M.copy(g),g.normalize(),l.push(g.x,g.y,g.z);break;case e.length-1:l.push(M.x,M.y,M.z);break;default:y=e[w+1].x-e[w].x,v=e[w+1].y-e[w].y,g.x=v*1,g.y=-y,g.z=v*0,_.copy(g),g.x+=M.x,g.y+=M.y,g.z+=M.z,g.normalize(),l.push(g.x,g.y,g.z),M.copy(_)}for(let w=0;w<=t;w++){const S=n+w*d*i,T=Math.sin(S),z=Math.cos(S);for(let N=0;N<=e.length-1;N++){m.x=e[N].x*T,m.y=e[N].y,m.z=e[N].x*z,o.push(m.x,m.y,m.z),p.x=w/t,p.y=N/(e.length-1),a.push(p.x,p.y);const A=l[3*N+0]*T,k=l[3*N+1],D=l[3*N+0]*z;u.push(A,k,D)}}for(let w=0;w<t;w++)for(let S=0;S<e.length-1;S++){const T=S+w*e.length,z=T,N=T+e.length,A=T+e.length+1,k=T+1;r.push(z,N,k),r.push(A,k,N)}this.setIndex(r),this.setAttribute("position",new He(o,3)),this.setAttribute("uv",new He(a,2)),this.setAttribute("normal",new He(u,3))}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}static fromJSON(e){return new So(e.points,e.segments,e.phiStart,e.phiLength)}}class ol extends So{constructor(e=1,t=1,n=4,i=8){const r=new lo;r.absarc(0,-t/2,e,Math.PI*1.5,0),r.absarc(0,t/2,e,0,Math.PI*.5),super(r.getPoints(n),i),this.type="CapsuleGeometry",this.parameters={radius:e,length:t,capSegments:n,radialSegments:i}}static fromJSON(e){return new ol(e.radius,e.length,e.capSegments,e.radialSegments)}}class al extends at{constructor(e=1,t=32,n=0,i=Math.PI*2){super(),this.type="CircleGeometry",this.parameters={radius:e,segments:t,thetaStart:n,thetaLength:i},t=Math.max(3,t);const r=[],o=[],a=[],l=[],u=new O,d=new se;o.push(0,0,0),a.push(0,0,1),l.push(.5,.5);for(let m=0,p=3;m<=t;m++,p+=3){const g=n+m/t*i;u.x=e*Math.cos(g),u.y=e*Math.sin(g),o.push(u.x,u.y,u.z),a.push(0,0,1),d.x=(o[p]/e+1)/2,d.y=(o[p+1]/e+1)/2,l.push(d.x,d.y)}for(let m=1;m<=t;m++)r.push(m,m+1,0);this.setIndex(r),this.setAttribute("position",new He(o,3)),this.setAttribute("normal",new He(a,3)),this.setAttribute("uv",new He(l,2))}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}static fromJSON(e){return new al(e.radius,e.segments,e.thetaStart,e.thetaLength)}}class Wt extends at{constructor(e=1,t=1,n=1,i=32,r=1,o=!1,a=0,l=Math.PI*2){super(),this.type="CylinderGeometry",this.parameters={radiusTop:e,radiusBottom:t,height:n,radialSegments:i,heightSegments:r,openEnded:o,thetaStart:a,thetaLength:l};const u=this;i=Math.floor(i),r=Math.floor(r);const d=[],m=[],p=[],g=[];let _=0;const M=[],y=n/2;let v=0;w(),o===!1&&(e>0&&S(!0),t>0&&S(!1)),this.setIndex(d),this.setAttribute("position",new He(m,3)),this.setAttribute("normal",new He(p,3)),this.setAttribute("uv",new He(g,2));function w(){const T=new O,z=new O;let N=0;const A=(t-e)/n;for(let k=0;k<=r;k++){const D=[],C=k/r,X=C*(t-e)+e;for(let Y=0;Y<=i;Y++){const H=Y/i,K=H*l+a,J=Math.sin(K),ce=Math.cos(K);z.x=X*J,z.y=-C*n+y,z.z=X*ce,m.push(z.x,z.y,z.z),T.set(J,A,ce).normalize(),p.push(T.x,T.y,T.z),g.push(H,1-C),D.push(_++)}M.push(D)}for(let k=0;k<i;k++)for(let D=0;D<r;D++){const C=M[D][k],X=M[D+1][k],Y=M[D+1][k+1],H=M[D][k+1];d.push(C,X,H),d.push(X,Y,H),N+=6}u.addGroup(v,N,0),v+=N}function S(T){const z=_,N=new se,A=new O;let k=0;const D=T===!0?e:t,C=T===!0?1:-1;for(let Y=1;Y<=i;Y++)m.push(0,y*C,0),p.push(0,C,0),g.push(.5,.5),_++;const X=_;for(let Y=0;Y<=i;Y++){const K=Y/i*l+a,J=Math.cos(K),ce=Math.sin(K);A.x=D*ce,A.y=y*C,A.z=D*J,m.push(A.x,A.y,A.z),p.push(0,C,0),N.x=J*.5+.5,N.y=ce*.5*C+.5,g.push(N.x,N.y),_++}for(let Y=0;Y<i;Y++){const H=z+Y,K=X+Y;T===!0?d.push(K,K+1,H):d.push(K+1,K,H),k+=3}u.addGroup(v,k,T===!0?1:2),v+=k}}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}static fromJSON(e){return new Wt(e.radiusTop,e.radiusBottom,e.height,e.radialSegments,e.heightSegments,e.openEnded,e.thetaStart,e.thetaLength)}}class ll extends Wt{constructor(e=1,t=1,n=32,i=1,r=!1,o=0,a=Math.PI*2){super(0,e,t,n,i,r,o,a),this.type="ConeGeometry",this.parameters={radius:e,height:t,radialSegments:n,heightSegments:i,openEnded:r,thetaStart:o,thetaLength:a}}static fromJSON(e){return new ll(e.radius,e.height,e.radialSegments,e.heightSegments,e.openEnded,e.thetaStart,e.thetaLength)}}class Wi extends at{constructor(e=[],t=[],n=1,i=0){super(),this.type="PolyhedronGeometry",this.parameters={vertices:e,indices:t,radius:n,detail:i};const r=[],o=[];a(i),u(n),d(),this.setAttribute("position",new He(r,3)),this.setAttribute("normal",new He(r.slice(),3)),this.setAttribute("uv",new He(o,2)),i===0?this.computeVertexNormals():this.normalizeNormals();function a(w){const S=new O,T=new O,z=new O;for(let N=0;N<t.length;N+=3)g(t[N+0],S),g(t[N+1],T),g(t[N+2],z),l(S,T,z,w)}function l(w,S,T,z){const N=z+1,A=[];for(let k=0;k<=N;k++){A[k]=[];const D=w.clone().lerp(T,k/N),C=S.clone().lerp(T,k/N),X=N-k;for(let Y=0;Y<=X;Y++)Y===0&&k===N?A[k][Y]=D:A[k][Y]=D.clone().lerp(C,Y/X)}for(let k=0;k<N;k++)for(let D=0;D<2*(N-k)-1;D++){const C=Math.floor(D/2);D%2===0?(p(A[k][C+1]),p(A[k+1][C]),p(A[k][C])):(p(A[k][C+1]),p(A[k+1][C+1]),p(A[k+1][C]))}}function u(w){const S=new O;for(let T=0;T<r.length;T+=3)S.x=r[T+0],S.y=r[T+1],S.z=r[T+2],S.normalize().multiplyScalar(w),r[T+0]=S.x,r[T+1]=S.y,r[T+2]=S.z}function d(){const w=new O;for(let S=0;S<r.length;S+=3){w.x=r[S+0],w.y=r[S+1],w.z=r[S+2];const T=y(w)/2/Math.PI+.5,z=v(w)/Math.PI+.5;o.push(T,1-z)}_(),m()}function m(){for(let w=0;w<o.length;w+=6){const S=o[w+0],T=o[w+2],z=o[w+4],N=Math.max(S,T,z),A=Math.min(S,T,z);N>.9&&A<.1&&(S<.2&&(o[w+0]+=1),T<.2&&(o[w+2]+=1),z<.2&&(o[w+4]+=1))}}function p(w){r.push(w.x,w.y,w.z)}function g(w,S){const T=w*3;S.x=e[T+0],S.y=e[T+1],S.z=e[T+2]}function _(){const w=new O,S=new O,T=new O,z=new O,N=new se,A=new se,k=new se;for(let D=0,C=0;D<r.length;D+=9,C+=6){w.set(r[D+0],r[D+1],r[D+2]),S.set(r[D+3],r[D+4],r[D+5]),T.set(r[D+6],r[D+7],r[D+8]),N.set(o[C+0],o[C+1]),A.set(o[C+2],o[C+3]),k.set(o[C+4],o[C+5]),z.copy(w).add(S).add(T).divideScalar(3);const X=y(z);M(N,C+0,w,X),M(A,C+2,S,X),M(k,C+4,T,X)}}function M(w,S,T,z){z<0&&w.x===1&&(o[S]=w.x-1),T.x===0&&T.z===0&&(o[S]=z/2/Math.PI+.5)}function y(w){return Math.atan2(w.z,-w.x)}function v(w){return Math.atan2(-w.y,Math.sqrt(w.x*w.x+w.z*w.z))}}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}static fromJSON(e){return new Wi(e.vertices,e.indices,e.radius,e.details)}}class cl extends Wi{constructor(e=1,t=0){const n=(1+Math.sqrt(5))/2,i=1/n,r=[-1,-1,-1,-1,-1,1,-1,1,-1,-1,1,1,1,-1,-1,1,-1,1,1,1,-1,1,1,1,0,-i,-n,0,-i,n,0,i,-n,0,i,n,-i,-n,0,-i,n,0,i,-n,0,i,n,0,-n,0,-i,n,0,-i,-n,0,i,n,0,i],o=[3,11,7,3,7,15,3,15,13,7,19,17,7,17,6,7,6,15,17,4,8,17,8,10,17,10,6,8,0,16,8,16,2,8,2,10,0,12,1,0,1,18,0,18,16,6,10,2,6,2,13,6,13,15,2,16,18,2,18,3,2,3,13,18,1,9,18,9,11,18,11,3,4,14,12,4,12,0,4,0,8,11,9,5,11,5,19,11,19,7,19,5,14,19,14,4,19,4,17,1,12,14,1,14,5,1,5,9];super(r,o,e,t),this.type="DodecahedronGeometry",this.parameters={radius:e,detail:t}}static fromJSON(e){return new cl(e.radius,e.detail)}}const pa=new O,ma=new O,lc=new O,ga=new An;class Yp extends at{constructor(e=null,t=1){if(super(),this.type="EdgesGeometry",this.parameters={geometry:e,thresholdAngle:t},e!==null){const i=Math.pow(10,4),r=Math.cos(br*t),o=e.getIndex(),a=e.getAttribute("position"),l=o?o.count:a.count,u=[0,0,0],d=["a","b","c"],m=new Array(3),p={},g=[];for(let _=0;_<l;_+=3){o?(u[0]=o.getX(_),u[1]=o.getX(_+1),u[2]=o.getX(_+2)):(u[0]=_,u[1]=_+1,u[2]=_+2);const{a:M,b:y,c:v}=ga;if(M.fromBufferAttribute(a,u[0]),y.fromBufferAttribute(a,u[1]),v.fromBufferAttribute(a,u[2]),ga.getNormal(lc),m[0]=`${Math.round(M.x*i)},${Math.round(M.y*i)},${Math.round(M.z*i)}`,m[1]=`${Math.round(y.x*i)},${Math.round(y.y*i)},${Math.round(y.z*i)}`,m[2]=`${Math.round(v.x*i)},${Math.round(v.y*i)},${Math.round(v.z*i)}`,!(m[0]===m[1]||m[1]===m[2]||m[2]===m[0]))for(let w=0;w<3;w++){const S=(w+1)%3,T=m[w],z=m[S],N=ga[d[w]],A=ga[d[S]],k=`${T}_${z}`,D=`${z}_${T}`;D in p&&p[D]?(lc.dot(p[D].normal)<=r&&(g.push(N.x,N.y,N.z),g.push(A.x,A.y,A.z)),p[D]=null):k in p||(p[k]={index0:u[w],index1:u[S],normal:lc.clone()})}}for(const _ in p)if(p[_]){const{index0:M,index1:y}=p[_];pa.fromBufferAttribute(a,M),ma.fromBufferAttribute(a,y),g.push(pa.x,pa.y,pa.z),g.push(ma.x,ma.y,ma.z)}this.setAttribute("position",new He(g,3))}}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}}class Sr extends lo{constructor(e){super(e),this.uuid=Rn(),this.type="Shape",this.holes=[]}getPointsHoles(e){const t=[];for(let n=0,i=this.holes.length;n<i;n++)t[n]=this.holes[n].getPoints(e);return t}extractPoints(e){return{shape:this.getPoints(e),holes:this.getPointsHoles(e)}}copy(e){super.copy(e),this.holes=[];for(let t=0,n=e.holes.length;t<n;t++){const i=e.holes[t];this.holes.push(i.clone())}return this}toJSON(){const e=super.toJSON();e.uuid=this.uuid,e.holes=[];for(let t=0,n=this.holes.length;t<n;t++){const i=this.holes[t];e.holes.push(i.toJSON())}return e}fromJSON(e){super.fromJSON(e),this.uuid=e.uuid,this.holes=[];for(let t=0,n=e.holes.length;t<n;t++){const i=e.holes[t];this.holes.push(new lo().fromJSON(i))}return this}}const ZM={triangulate:function(s,e,t=2){const n=e&&e.length,i=n?e[0]*t:s.length;let r=qp(s,0,i,t,!0);const o=[];if(!r||r.next===r.prev)return o;let a,l,u,d,m,p,g;if(n&&(r=QM(s,e,r,t)),s.length>80*t){a=u=s[0],l=d=s[1];for(let _=t;_<i;_+=t)m=s[_],p=s[_+1],m<a&&(a=m),p<l&&(l=p),m>u&&(u=m),p>d&&(d=p);g=Math.max(u-a,d-l),g=g!==0?32767/g:0}return co(r,o,t,a,l,g,0),o}};function qp(s,e,t,n,i){let r,o;if(i===hb(s,e,t,n)>0)for(r=e;r<t;r+=n)o=Rd(r,s[r],s[r+1],o);else for(r=t-n;r>=e;r-=n)o=Rd(r,s[r],s[r+1],o);return o&&hl(o,o.next)&&(uo(o),o=o.next),o}function Tr(s,e){if(!s)return s;e||(e=s);let t=s,n;do if(n=!1,!t.steiner&&(hl(t,t.next)||Ft(t.prev,t,t.next)===0)){if(uo(t),t=e=t.prev,t===t.next)break;n=!0}else t=t.next;while(n||t!==e);return e}function co(s,e,t,n,i,r,o){if(!s)return;!o&&r&&rb(s,n,i,r);let a=s,l,u;for(;s.prev!==s.next;){if(l=s.prev,u=s.next,r?KM(s,n,i,r):jM(s)){e.push(l.i/t|0),e.push(s.i/t|0),e.push(u.i/t|0),uo(s),s=u.next,a=u.next;continue}if(s=u,s===a){o?o===1?(s=JM(Tr(s),e,t),co(s,e,t,n,i,r,2)):o===2&&$M(s,e,t,n,i,r):co(Tr(s),e,t,n,i,r,1);break}}}function jM(s){const e=s.prev,t=s,n=s.next;if(Ft(e,t,n)>=0)return!1;const i=e.x,r=t.x,o=n.x,a=e.y,l=t.y,u=n.y,d=i<r?i<o?i:o:r<o?r:o,m=a<l?a<u?a:u:l<u?l:u,p=i>r?i>o?i:o:r>o?r:o,g=a>l?a>u?a:u:l>u?l:u;let _=n.next;for(;_!==e;){if(_.x>=d&&_.x<=p&&_.y>=m&&_.y<=g&&ss(i,a,r,l,o,u,_.x,_.y)&&Ft(_.prev,_,_.next)>=0)return!1;_=_.next}return!0}function KM(s,e,t,n){const i=s.prev,r=s,o=s.next;if(Ft(i,r,o)>=0)return!1;const a=i.x,l=r.x,u=o.x,d=i.y,m=r.y,p=o.y,g=a<l?a<u?a:u:l<u?l:u,_=d<m?d<p?d:p:m<p?m:p,M=a>l?a>u?a:u:l>u?l:u,y=d>m?d>p?d:p:m>p?m:p,v=Jc(g,_,e,t,n),w=Jc(M,y,e,t,n);let S=s.prevZ,T=s.nextZ;for(;S&&S.z>=v&&T&&T.z<=w;){if(S.x>=g&&S.x<=M&&S.y>=_&&S.y<=y&&S!==i&&S!==o&&ss(a,d,l,m,u,p,S.x,S.y)&&Ft(S.prev,S,S.next)>=0||(S=S.prevZ,T.x>=g&&T.x<=M&&T.y>=_&&T.y<=y&&T!==i&&T!==o&&ss(a,d,l,m,u,p,T.x,T.y)&&Ft(T.prev,T,T.next)>=0))return!1;T=T.nextZ}for(;S&&S.z>=v;){if(S.x>=g&&S.x<=M&&S.y>=_&&S.y<=y&&S!==i&&S!==o&&ss(a,d,l,m,u,p,S.x,S.y)&&Ft(S.prev,S,S.next)>=0)return!1;S=S.prevZ}for(;T&&T.z<=w;){if(T.x>=g&&T.x<=M&&T.y>=_&&T.y<=y&&T!==i&&T!==o&&ss(a,d,l,m,u,p,T.x,T.y)&&Ft(T.prev,T,T.next)>=0)return!1;T=T.nextZ}return!0}function JM(s,e,t){let n=s;do{const i=n.prev,r=n.next.next;!hl(i,r)&&Zp(i,n,n.next,r)&&ho(i,r)&&ho(r,i)&&(e.push(i.i/t|0),e.push(n.i/t|0),e.push(r.i/t|0),uo(n),uo(n.next),n=s=r),n=n.next}while(n!==s);return Tr(n)}function $M(s,e,t,n,i,r){let o=s;do{let a=o.next.next;for(;a!==o.prev;){if(o.i!==a.i&&ab(o,a)){let l=jp(o,a);o=Tr(o,o.next),l=Tr(l,l.next),co(o,e,t,n,i,r,0),co(l,e,t,n,i,r,0);return}a=a.next}o=o.next}while(o!==s)}function QM(s,e,t,n){const i=[];let r,o,a,l,u;for(r=0,o=e.length;r<o;r++)a=e[r]*n,l=r<o-1?e[r+1]*n:s.length,u=qp(s,a,l,n,!1),u===u.next&&(u.steiner=!0),i.push(ob(u));for(i.sort(eb),r=0;r<i.length;r++)t=tb(i[r],t);return t}function eb(s,e){return s.x-e.x}function tb(s,e){const t=nb(s,e);if(!t)return e;const n=jp(t,s);return Tr(n,n.next),Tr(t,t.next)}function nb(s,e){let t=e,n=-1/0,i;const r=s.x,o=s.y;do{if(o<=t.y&&o>=t.next.y&&t.next.y!==t.y){const p=t.x+(o-t.y)*(t.next.x-t.x)/(t.next.y-t.y);if(p<=r&&p>n&&(n=p,i=t.x<t.next.x?t:t.next,p===r))return i}t=t.next}while(t!==e);if(!i)return null;const a=i,l=i.x,u=i.y;let d=1/0,m;t=i;do r>=t.x&&t.x>=l&&r!==t.x&&ss(o<u?r:n,o,l,u,o<u?n:r,o,t.x,t.y)&&(m=Math.abs(o-t.y)/(r-t.x),ho(t,s)&&(m<d||m===d&&(t.x>i.x||t.x===i.x&&ib(i,t)))&&(i=t,d=m)),t=t.next;while(t!==a);return i}function ib(s,e){return Ft(s.prev,s,e.prev)<0&&Ft(e.next,s,s.next)<0}function rb(s,e,t,n){let i=s;do i.z===0&&(i.z=Jc(i.x,i.y,e,t,n)),i.prevZ=i.prev,i.nextZ=i.next,i=i.next;while(i!==s);i.prevZ.nextZ=null,i.prevZ=null,sb(i)}function sb(s){let e,t,n,i,r,o,a,l,u=1;do{for(t=s,s=null,r=null,o=0;t;){for(o++,n=t,a=0,e=0;e<u&&(a++,n=n.nextZ,!!n);e++);for(l=u;a>0||l>0&&n;)a!==0&&(l===0||!n||t.z<=n.z)?(i=t,t=t.nextZ,a--):(i=n,n=n.nextZ,l--),r?r.nextZ=i:s=i,i.prevZ=r,r=i;t=n}r.nextZ=null,u*=2}while(o>1);return s}function Jc(s,e,t,n,i){return s=(s-t)*i|0,e=(e-n)*i|0,s=(s|s<<8)&16711935,s=(s|s<<4)&252645135,s=(s|s<<2)&858993459,s=(s|s<<1)&1431655765,e=(e|e<<8)&16711935,e=(e|e<<4)&252645135,e=(e|e<<2)&858993459,e=(e|e<<1)&1431655765,s|e<<1}function ob(s){let e=s,t=s;do(e.x<t.x||e.x===t.x&&e.y<t.y)&&(t=e),e=e.next;while(e!==s);return t}function ss(s,e,t,n,i,r,o,a){return(i-o)*(e-a)>=(s-o)*(r-a)&&(s-o)*(n-a)>=(t-o)*(e-a)&&(t-o)*(r-a)>=(i-o)*(n-a)}function ab(s,e){return s.next.i!==e.i&&s.prev.i!==e.i&&!lb(s,e)&&(ho(s,e)&&ho(e,s)&&cb(s,e)&&(Ft(s.prev,s,e.prev)||Ft(s,e.prev,e))||hl(s,e)&&Ft(s.prev,s,s.next)>0&&Ft(e.prev,e,e.next)>0)}function Ft(s,e,t){return(e.y-s.y)*(t.x-e.x)-(e.x-s.x)*(t.y-e.y)}function hl(s,e){return s.x===e.x&&s.y===e.y}function Zp(s,e,t,n){const i=_a(Ft(s,e,t)),r=_a(Ft(s,e,n)),o=_a(Ft(t,n,s)),a=_a(Ft(t,n,e));return!!(i!==r&&o!==a||i===0&&va(s,t,e)||r===0&&va(s,n,e)||o===0&&va(t,s,n)||a===0&&va(t,e,n))}function va(s,e,t){return e.x<=Math.max(s.x,t.x)&&e.x>=Math.min(s.x,t.x)&&e.y<=Math.max(s.y,t.y)&&e.y>=Math.min(s.y,t.y)}function _a(s){return s>0?1:s<0?-1:0}function lb(s,e){let t=s;do{if(t.i!==s.i&&t.next.i!==s.i&&t.i!==e.i&&t.next.i!==e.i&&Zp(t,t.next,s,e))return!0;t=t.next}while(t!==s);return!1}function ho(s,e){return Ft(s.prev,s,s.next)<0?Ft(s,e,s.next)>=0&&Ft(s,s.prev,e)>=0:Ft(s,e,s.prev)<0||Ft(s,s.next,e)<0}function cb(s,e){let t=s,n=!1;const i=(s.x+e.x)/2,r=(s.y+e.y)/2;do t.y>r!=t.next.y>r&&t.next.y!==t.y&&i<(t.next.x-t.x)*(r-t.y)/(t.next.y-t.y)+t.x&&(n=!n),t=t.next;while(t!==s);return n}function jp(s,e){const t=new $c(s.i,s.x,s.y),n=new $c(e.i,e.x,e.y),i=s.next,r=e.prev;return s.next=e,e.prev=s,t.next=i,i.prev=t,n.next=t,t.prev=n,r.next=n,n.prev=r,n}function Rd(s,e,t,n){const i=new $c(s,e,t);return n?(i.next=n.next,i.prev=n,n.next.prev=i,n.next=i):(i.prev=i,i.next=i),i}function uo(s){s.next.prev=s.prev,s.prev.next=s.next,s.prevZ&&(s.prevZ.nextZ=s.nextZ),s.nextZ&&(s.nextZ.prevZ=s.prevZ)}function $c(s,e,t){this.i=s,this.x=e,this.y=t,this.prev=null,this.next=null,this.z=0,this.prevZ=null,this.nextZ=null,this.steiner=!1}function hb(s,e,t,n){let i=0;for(let r=e,o=t-n;r<t;r+=n)i+=(s[o]-s[r])*(s[r+1]+s[o+1]),o=r;return i}class ti{static area(e){const t=e.length;let n=0;for(let i=t-1,r=0;r<t;i=r++)n+=e[i].x*e[r].y-e[r].x*e[i].y;return n*.5}static isClockWise(e){return ti.area(e)<0}static triangulateShape(e,t){const n=[],i=[],r=[];Pd(e),Id(n,e);let o=e.length;t.forEach(Pd);for(let l=0;l<t.length;l++)i.push(o),o+=t[l].length,Id(n,t[l]);const a=ZM.triangulate(n,i);for(let l=0;l<a.length;l+=3)r.push(a.slice(l,l+3));return r}}function Pd(s){const e=s.length;e>2&&s[e-1].equals(s[0])&&s.pop()}function Id(s,e){for(let t=0;t<e.length;t++)s.push(e[t].x),s.push(e[t].y)}class ul extends at{constructor(e=new Sr([new se(.5,.5),new se(-.5,.5),new se(-.5,-.5),new se(.5,-.5)]),t={}){super(),this.type="ExtrudeGeometry",this.parameters={shapes:e,options:t},e=Array.isArray(e)?e:[e];const n=this,i=[],r=[];for(let a=0,l=e.length;a<l;a++){const u=e[a];o(u)}this.setAttribute("position",new He(i,3)),this.setAttribute("uv",new He(r,2)),this.computeVertexNormals();function o(a){const l=[],u=t.curveSegments!==void 0?t.curveSegments:12,d=t.steps!==void 0?t.steps:1,m=t.depth!==void 0?t.depth:1;let p=t.bevelEnabled!==void 0?t.bevelEnabled:!0,g=t.bevelThickness!==void 0?t.bevelThickness:.2,_=t.bevelSize!==void 0?t.bevelSize:g-.1,M=t.bevelOffset!==void 0?t.bevelOffset:0,y=t.bevelSegments!==void 0?t.bevelSegments:3;const v=t.extrudePath,w=t.UVGenerator!==void 0?t.UVGenerator:ub;let S,T=!1,z,N,A,k;v&&(S=v.getSpacedPoints(d),T=!0,p=!1,z=v.computeFrenetFrames(d,!1),N=new O,A=new O,k=new O),p||(y=0,g=0,_=0,M=0);const D=a.extractPoints(u);let C=D.shape;const X=D.holes;if(!ti.isClockWise(C)){C=C.reverse();for(let le=0,xe=X.length;le<xe;le++){const he=X[le];ti.isClockWise(he)&&(X[le]=he.reverse())}}const H=ti.triangulateShape(C,X),K=C;for(let le=0,xe=X.length;le<xe;le++){const he=X[le];C=C.concat(he)}function J(le,xe,he){return xe||console.error("THREE.ExtrudeGeometry: vec does not exist"),le.clone().addScaledVector(xe,he)}const ce=C.length,de=H.length;function Q(le,xe,he){let Ae,ge,Fe;const Ge=le.x-xe.x,B=le.y-xe.y,I=he.x-le.x,$=he.y-le.y,ue=Ge*Ge+B*B,_e=Ge*$-B*I;if(Math.abs(_e)>Number.EPSILON){const me=Math.sqrt(ue),Ye=Math.sqrt(I*I+$*$),Re=xe.x-B/me,Le=xe.y+Ge/me,tt=he.x-$/Ye,Ce=he.y+I/Ye,Xe=((tt-Re)*$-(Ce-Le)*I)/(Ge*$-B*I);Ae=Re+Ge*Xe-le.x,ge=Le+B*Xe-le.y;const lt=Ae*Ae+ge*ge;if(lt<=2)return new se(Ae,ge);Fe=Math.sqrt(lt/2)}else{let me=!1;Ge>Number.EPSILON?I>Number.EPSILON&&(me=!0):Ge<-Number.EPSILON?I<-Number.EPSILON&&(me=!0):Math.sign(B)===Math.sign($)&&(me=!0),me?(Ae=-B,ge=Ge,Fe=Math.sqrt(ue)):(Ae=Ge,ge=B,Fe=Math.sqrt(ue/2))}return new se(Ae/Fe,ge/Fe)}const pe=[];for(let le=0,xe=K.length,he=xe-1,Ae=le+1;le<xe;le++,he++,Ae++)he===xe&&(he=0),Ae===xe&&(Ae=0),pe[le]=Q(K[le],K[he],K[Ae]);const ve=[];let Ue,Je=pe.concat();for(let le=0,xe=X.length;le<xe;le++){const he=X[le];Ue=[];for(let Ae=0,ge=he.length,Fe=ge-1,Ge=Ae+1;Ae<ge;Ae++,Fe++,Ge++)Fe===ge&&(Fe=0),Ge===ge&&(Ge=0),Ue[Ae]=Q(he[Ae],he[Fe],he[Ge]);ve.push(Ue),Je=Je.concat(Ue)}for(let le=0;le<y;le++){const xe=le/y,he=g*Math.cos(xe*Math.PI/2),Ae=_*Math.sin(xe*Math.PI/2)+M;for(let ge=0,Fe=K.length;ge<Fe;ge++){const Ge=J(K[ge],pe[ge],Ae);Ee(Ge.x,Ge.y,-he)}for(let ge=0,Fe=X.length;ge<Fe;ge++){const Ge=X[ge];Ue=ve[ge];for(let B=0,I=Ge.length;B<I;B++){const $=J(Ge[B],Ue[B],Ae);Ee($.x,$.y,-he)}}}const vt=_+M;for(let le=0;le<ce;le++){const xe=p?J(C[le],Je[le],vt):C[le];T?(A.copy(z.normals[0]).multiplyScalar(xe.x),N.copy(z.binormals[0]).multiplyScalar(xe.y),k.copy(S[0]).add(A).add(N),Ee(k.x,k.y,k.z)):Ee(xe.x,xe.y,0)}for(let le=1;le<=d;le++)for(let xe=0;xe<ce;xe++){const he=p?J(C[xe],Je[xe],vt):C[xe];T?(A.copy(z.normals[le]).multiplyScalar(he.x),N.copy(z.binormals[le]).multiplyScalar(he.y),k.copy(S[le]).add(A).add(N),Ee(k.x,k.y,k.z)):Ee(he.x,he.y,m/d*le)}for(let le=y-1;le>=0;le--){const xe=le/y,he=g*Math.cos(xe*Math.PI/2),Ae=_*Math.sin(xe*Math.PI/2)+M;for(let ge=0,Fe=K.length;ge<Fe;ge++){const Ge=J(K[ge],pe[ge],Ae);Ee(Ge.x,Ge.y,m+he)}for(let ge=0,Fe=X.length;ge<Fe;ge++){const Ge=X[ge];Ue=ve[ge];for(let B=0,I=Ge.length;B<I;B++){const $=J(Ge[B],Ue[B],Ae);T?Ee($.x,$.y+S[d-1].y,S[d-1].x+he):Ee($.x,$.y,m+he)}}}ae(),we();function ae(){const le=i.length/3;if(p){let xe=0,he=ce*xe;for(let Ae=0;Ae<de;Ae++){const ge=H[Ae];nt(ge[2]+he,ge[1]+he,ge[0]+he)}xe=d+y*2,he=ce*xe;for(let Ae=0;Ae<de;Ae++){const ge=H[Ae];nt(ge[0]+he,ge[1]+he,ge[2]+he)}}else{for(let xe=0;xe<de;xe++){const he=H[xe];nt(he[2],he[1],he[0])}for(let xe=0;xe<de;xe++){const he=H[xe];nt(he[0]+ce*d,he[1]+ce*d,he[2]+ce*d)}}n.addGroup(le,i.length/3-le,0)}function we(){const le=i.length/3;let xe=0;ke(K,xe),xe+=K.length;for(let he=0,Ae=X.length;he<Ae;he++){const ge=X[he];ke(ge,xe),xe+=ge.length}n.addGroup(le,i.length/3-le,1)}function ke(le,xe){let he=le.length;for(;--he>=0;){const Ae=he;let ge=he-1;ge<0&&(ge=le.length-1);for(let Fe=0,Ge=d+y*2;Fe<Ge;Fe++){const B=ce*Fe,I=ce*(Fe+1),$=xe+Ae+B,ue=xe+ge+B,_e=xe+ge+I,me=xe+Ae+I;be($,ue,_e,me)}}}function Ee(le,xe,he){l.push(le),l.push(xe),l.push(he)}function nt(le,xe,he){Z(le),Z(xe),Z(he);const Ae=i.length/3,ge=w.generateTopUV(n,i,Ae-3,Ae-2,Ae-1);st(ge[0]),st(ge[1]),st(ge[2])}function be(le,xe,he,Ae){Z(le),Z(xe),Z(Ae),Z(xe),Z(he),Z(Ae);const ge=i.length/3,Fe=w.generateSideWallUV(n,i,ge-6,ge-3,ge-2,ge-1);st(Fe[0]),st(Fe[1]),st(Fe[3]),st(Fe[1]),st(Fe[2]),st(Fe[3])}function Z(le){i.push(l[le*3+0]),i.push(l[le*3+1]),i.push(l[le*3+2])}function st(le){r.push(le.x),r.push(le.y)}}}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}toJSON(){const e=super.toJSON(),t=this.parameters.shapes,n=this.parameters.options;return db(t,n,e)}static fromJSON(e,t){const n=[];for(let r=0,o=e.shapes.length;r<o;r++){const a=t[e.shapes[r]];n.push(a)}const i=e.options.extrudePath;return i!==void 0&&(e.options.extrudePath=new Wa[i.type]().fromJSON(i)),new ul(n,e.options)}}const ub={generateTopUV:function(s,e,t,n,i){const r=e[t*3],o=e[t*3+1],a=e[n*3],l=e[n*3+1],u=e[i*3],d=e[i*3+1];return[new se(r,o),new se(a,l),new se(u,d)]},generateSideWallUV:function(s,e,t,n,i,r){const o=e[t*3],a=e[t*3+1],l=e[t*3+2],u=e[n*3],d=e[n*3+1],m=e[n*3+2],p=e[i*3],g=e[i*3+1],_=e[i*3+2],M=e[r*3],y=e[r*3+1],v=e[r*3+2];return Math.abs(a-d)<Math.abs(o-u)?[new se(o,1-l),new se(u,1-m),new se(p,1-_),new se(M,1-v)]:[new se(a,1-l),new se(d,1-m),new se(g,1-_),new se(y,1-v)]}};function db(s,e,t){if(t.shapes=[],Array.isArray(s))for(let n=0,i=s.length;n<i;n++){const r=s[n];t.shapes.push(r.uuid)}else t.shapes.push(s.uuid);return t.options=Object.assign({},e),e.extrudePath!==void 0&&(t.options.extrudePath=e.extrudePath.toJSON()),t}class dl extends Wi{constructor(e=1,t=0){const n=(1+Math.sqrt(5))/2,i=[-1,n,0,1,n,0,-1,-n,0,1,-n,0,0,-1,n,0,1,n,0,-1,-n,0,1,-n,n,0,-1,n,0,1,-n,0,-1,-n,0,1],r=[0,11,5,0,5,1,0,1,7,0,7,10,0,10,11,1,5,9,5,11,4,11,10,2,10,7,6,7,1,8,3,9,4,3,4,2,3,2,6,3,6,8,3,8,9,4,9,5,2,4,11,6,2,10,8,6,7,9,8,1];super(i,r,e,t),this.type="IcosahedronGeometry",this.parameters={radius:e,detail:t}}static fromJSON(e){return new dl(e.radius,e.detail)}}class vi extends Wi{constructor(e=1,t=0){const n=[1,0,0,-1,0,0,0,1,0,0,-1,0,0,0,1,0,0,-1],i=[0,2,4,0,4,3,0,3,5,0,5,2,1,2,5,1,5,3,1,3,4,1,4,2];super(n,i,e,t),this.type="OctahedronGeometry",this.parameters={radius:e,detail:t}}static fromJSON(e){return new vi(e.radius,e.detail)}}class fl extends at{constructor(e=.5,t=1,n=32,i=1,r=0,o=Math.PI*2){super(),this.type="RingGeometry",this.parameters={innerRadius:e,outerRadius:t,thetaSegments:n,phiSegments:i,thetaStart:r,thetaLength:o},n=Math.max(3,n),i=Math.max(1,i);const a=[],l=[],u=[],d=[];let m=e;const p=(t-e)/i,g=new O,_=new se;for(let M=0;M<=i;M++){for(let y=0;y<=n;y++){const v=r+y/n*o;g.x=m*Math.cos(v),g.y=m*Math.sin(v),l.push(g.x,g.y,g.z),u.push(0,0,1),_.x=(g.x/t+1)/2,_.y=(g.y/t+1)/2,d.push(_.x,_.y)}m+=p}for(let M=0;M<i;M++){const y=M*(n+1);for(let v=0;v<n;v++){const w=v+y,S=w,T=w+n+1,z=w+n+2,N=w+1;a.push(S,T,N),a.push(T,z,N)}}this.setIndex(a),this.setAttribute("position",new He(l,3)),this.setAttribute("normal",new He(u,3)),this.setAttribute("uv",new He(d,2))}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}static fromJSON(e){return new fl(e.innerRadius,e.outerRadius,e.thetaSegments,e.phiSegments,e.thetaStart,e.thetaLength)}}class pl extends at{constructor(e=new Sr([new se(0,.5),new se(-.5,-.5),new se(.5,-.5)]),t=12){super(),this.type="ShapeGeometry",this.parameters={shapes:e,curveSegments:t};const n=[],i=[],r=[],o=[];let a=0,l=0;if(Array.isArray(e)===!1)u(e);else for(let d=0;d<e.length;d++)u(e[d]),this.addGroup(a,l,d),a+=l,l=0;this.setIndex(n),this.setAttribute("position",new He(i,3)),this.setAttribute("normal",new He(r,3)),this.setAttribute("uv",new He(o,2));function u(d){const m=i.length/3,p=d.extractPoints(t);let g=p.shape;const _=p.holes;ti.isClockWise(g)===!1&&(g=g.reverse());for(let y=0,v=_.length;y<v;y++){const w=_[y];ti.isClockWise(w)===!0&&(_[y]=w.reverse())}const M=ti.triangulateShape(g,_);for(let y=0,v=_.length;y<v;y++){const w=_[y];g=g.concat(w)}for(let y=0,v=g.length;y<v;y++){const w=g[y];i.push(w.x,w.y,0),r.push(0,0,1),o.push(w.x,w.y)}for(let y=0,v=M.length;y<v;y++){const w=M[y],S=w[0]+m,T=w[1]+m,z=w[2]+m;n.push(S,T,z),l+=3}}}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}toJSON(){const e=super.toJSON(),t=this.parameters.shapes;return fb(t,e)}static fromJSON(e,t){const n=[];for(let i=0,r=e.shapes.length;i<r;i++){const o=t[e.shapes[i]];n.push(o)}return new pl(n,e.curveSegments)}}function fb(s,e){if(e.shapes=[],Array.isArray(s))for(let t=0,n=s.length;t<n;t++){const i=s[t];e.shapes.push(i.uuid)}else e.shapes.push(s.uuid);return e}class ps extends at{constructor(e=1,t=32,n=16,i=0,r=Math.PI*2,o=0,a=Math.PI){super(),this.type="SphereGeometry",this.parameters={radius:e,widthSegments:t,heightSegments:n,phiStart:i,phiLength:r,thetaStart:o,thetaLength:a},t=Math.max(3,Math.floor(t)),n=Math.max(2,Math.floor(n));const l=Math.min(o+a,Math.PI);let u=0;const d=[],m=new O,p=new O,g=[],_=[],M=[],y=[];for(let v=0;v<=n;v++){const w=[],S=v/n;let T=0;v===0&&o===0?T=.5/t:v===n&&l===Math.PI&&(T=-.5/t);for(let z=0;z<=t;z++){const N=z/t;m.x=-e*Math.cos(i+N*r)*Math.sin(o+S*a),m.y=e*Math.cos(o+S*a),m.z=e*Math.sin(i+N*r)*Math.sin(o+S*a),_.push(m.x,m.y,m.z),p.copy(m).normalize(),M.push(p.x,p.y,p.z),y.push(N+T,1-S),w.push(u++)}d.push(w)}for(let v=0;v<n;v++)for(let w=0;w<t;w++){const S=d[v][w+1],T=d[v][w],z=d[v+1][w],N=d[v+1][w+1];(v!==0||o>0)&&g.push(S,T,N),(v!==n-1||l<Math.PI)&&g.push(T,z,N)}this.setIndex(g),this.setAttribute("position",new He(_,3)),this.setAttribute("normal",new He(M,3)),this.setAttribute("uv",new He(y,2))}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}static fromJSON(e){return new ps(e.radius,e.widthSegments,e.heightSegments,e.phiStart,e.phiLength,e.thetaStart,e.thetaLength)}}class ml extends Wi{constructor(e=1,t=0){const n=[1,1,1,-1,-1,1,-1,1,-1,1,-1,-1],i=[2,1,0,0,3,2,1,3,0,2,3,1];super(n,i,e,t),this.type="TetrahedronGeometry",this.parameters={radius:e,detail:t}}static fromJSON(e){return new ml(e.radius,e.detail)}}class gi extends at{constructor(e=1,t=.4,n=12,i=48,r=Math.PI*2){super(),this.type="TorusGeometry",this.parameters={radius:e,tube:t,radialSegments:n,tubularSegments:i,arc:r},n=Math.floor(n),i=Math.floor(i);const o=[],a=[],l=[],u=[],d=new O,m=new O,p=new O;for(let g=0;g<=n;g++)for(let _=0;_<=i;_++){const M=_/i*r,y=g/n*Math.PI*2;m.x=(e+t*Math.cos(y))*Math.cos(M),m.y=(e+t*Math.cos(y))*Math.sin(M),m.z=t*Math.sin(y),a.push(m.x,m.y,m.z),d.x=e*Math.cos(M),d.y=e*Math.sin(M),p.subVectors(m,d).normalize(),l.push(p.x,p.y,p.z),u.push(_/i),u.push(g/n)}for(let g=1;g<=n;g++)for(let _=1;_<=i;_++){const M=(i+1)*g+_-1,y=(i+1)*(g-1)+_-1,v=(i+1)*(g-1)+_,w=(i+1)*g+_;o.push(M,y,w),o.push(y,v,w)}this.setIndex(o),this.setAttribute("position",new He(a,3)),this.setAttribute("normal",new He(l,3)),this.setAttribute("uv",new He(u,2))}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}static fromJSON(e){return new gi(e.radius,e.tube,e.radialSegments,e.tubularSegments,e.arc)}}class gl extends at{constructor(e=1,t=.4,n=64,i=8,r=2,o=3){super(),this.type="TorusKnotGeometry",this.parameters={radius:e,tube:t,tubularSegments:n,radialSegments:i,p:r,q:o},n=Math.floor(n),i=Math.floor(i);const a=[],l=[],u=[],d=[],m=new O,p=new O,g=new O,_=new O,M=new O,y=new O,v=new O;for(let S=0;S<=n;++S){const T=S/n*r*Math.PI*2;w(T,r,o,e,g),w(T+.01,r,o,e,_),y.subVectors(_,g),v.addVectors(_,g),M.crossVectors(y,v),v.crossVectors(M,y),M.normalize(),v.normalize();for(let z=0;z<=i;++z){const N=z/i*Math.PI*2,A=-t*Math.cos(N),k=t*Math.sin(N);m.x=g.x+(A*v.x+k*M.x),m.y=g.y+(A*v.y+k*M.y),m.z=g.z+(A*v.z+k*M.z),l.push(m.x,m.y,m.z),p.subVectors(m,g).normalize(),u.push(p.x,p.y,p.z),d.push(S/n),d.push(z/i)}}for(let S=1;S<=n;S++)for(let T=1;T<=i;T++){const z=(i+1)*(S-1)+(T-1),N=(i+1)*S+(T-1),A=(i+1)*S+T,k=(i+1)*(S-1)+T;a.push(z,N,k),a.push(N,A,k)}this.setIndex(a),this.setAttribute("position",new He(l,3)),this.setAttribute("normal",new He(u,3)),this.setAttribute("uv",new He(d,2));function w(S,T,z,N,A){const k=Math.cos(S),D=Math.sin(S),C=z/T*S,X=Math.cos(C);A.x=N*(2+X)*.5*k,A.y=N*(2+X)*D*.5,A.z=N*Math.sin(C)*.5}}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}static fromJSON(e){return new gl(e.radius,e.tube,e.tubularSegments,e.radialSegments,e.p,e.q)}}class vl extends at{constructor(e=new Fh(new O(-1,-1,0),new O(-1,1,0),new O(1,1,0)),t=64,n=1,i=8,r=!1){super(),this.type="TubeGeometry",this.parameters={path:e,tubularSegments:t,radius:n,radialSegments:i,closed:r};const o=e.computeFrenetFrames(t,r);this.tangents=o.tangents,this.normals=o.normals,this.binormals=o.binormals;const a=new O,l=new O,u=new se;let d=new O;const m=[],p=[],g=[],_=[];M(),this.setIndex(_),this.setAttribute("position",new He(m,3)),this.setAttribute("normal",new He(p,3)),this.setAttribute("uv",new He(g,2));function M(){for(let S=0;S<t;S++)y(S);y(r===!1?t:0),w(),v()}function y(S){d=e.getPointAt(S/t,d);const T=o.normals[S],z=o.binormals[S];for(let N=0;N<=i;N++){const A=N/i*Math.PI*2,k=Math.sin(A),D=-Math.cos(A);l.x=D*T.x+k*z.x,l.y=D*T.y+k*z.y,l.z=D*T.z+k*z.z,l.normalize(),p.push(l.x,l.y,l.z),a.x=d.x+n*l.x,a.y=d.y+n*l.y,a.z=d.z+n*l.z,m.push(a.x,a.y,a.z)}}function v(){for(let S=1;S<=t;S++)for(let T=1;T<=i;T++){const z=(i+1)*(S-1)+(T-1),N=(i+1)*S+(T-1),A=(i+1)*S+T,k=(i+1)*(S-1)+T;_.push(z,N,k),_.push(N,A,k)}}function w(){for(let S=0;S<=t;S++)for(let T=0;T<=i;T++)u.x=S/t,u.y=T/i,g.push(u.x,u.y)}}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}toJSON(){const e=super.toJSON();return e.path=this.parameters.path.toJSON(),e}static fromJSON(e){return new vl(new Wa[e.path.type]().fromJSON(e.path),e.tubularSegments,e.radius,e.radialSegments,e.closed)}}class Kp extends at{constructor(e=null){if(super(),this.type="WireframeGeometry",this.parameters={geometry:e},e!==null){const t=[],n=new Set,i=new O,r=new O;if(e.index!==null){const o=e.attributes.position,a=e.index;let l=e.groups;l.length===0&&(l=[{start:0,count:a.count,materialIndex:0}]);for(let u=0,d=l.length;u<d;++u){const m=l[u],p=m.start,g=m.count;for(let _=p,M=p+g;_<M;_+=3)for(let y=0;y<3;y++){const v=a.getX(_+y),w=a.getX(_+(y+1)%3);i.fromBufferAttribute(o,v),r.fromBufferAttribute(o,w),Ld(i,r,n)===!0&&(t.push(i.x,i.y,i.z),t.push(r.x,r.y,r.z))}}}else{const o=e.attributes.position;for(let a=0,l=o.count/3;a<l;a++)for(let u=0;u<3;u++){const d=3*a+u,m=3*a+(u+1)%3;i.fromBufferAttribute(o,d),r.fromBufferAttribute(o,m),Ld(i,r,n)===!0&&(t.push(i.x,i.y,i.z),t.push(r.x,r.y,r.z))}}this.setAttribute("position",new He(t,3))}}copy(e){return super.copy(e),this.parameters=Object.assign({},e.parameters),this}}function Ld(s,e,t){const n=`${s.x},${s.y},${s.z}-${e.x},${e.y},${e.z}`,i=`${e.x},${e.y},${e.z}-${s.x},${s.y},${s.z}`;return t.has(n)===!0||t.has(i)===!0?!1:(t.add(n),t.add(i),!0)}var Dd=Object.freeze({__proto__:null,BoxGeometry:zt,CapsuleGeometry:ol,CircleGeometry:al,ConeGeometry:ll,CylinderGeometry:Wt,DodecahedronGeometry:cl,EdgesGeometry:Yp,ExtrudeGeometry:ul,IcosahedronGeometry:dl,LatheGeometry:So,OctahedronGeometry:vi,PlaneGeometry:Cr,PolyhedronGeometry:Wi,RingGeometry:fl,ShapeGeometry:pl,SphereGeometry:ps,TetrahedronGeometry:ml,TorusGeometry:gi,TorusKnotGeometry:gl,TubeGeometry:vl,WireframeGeometry:Kp});class Jp extends hn{constructor(e){super(),this.isShadowMaterial=!0,this.type="ShadowMaterial",this.color=new Oe(0),this.transparent=!0,this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.color.copy(e.color),this.fog=e.fog,this}}class zh extends tn{constructor(e){super(e),this.isRawShaderMaterial=!0,this.type="RawShaderMaterial"}}class kh extends hn{constructor(e){super(),this.isMeshStandardMaterial=!0,this.defines={STANDARD:""},this.type="MeshStandardMaterial",this.color=new Oe(16777215),this.roughness=1,this.metalness=0,this.map=null,this.lightMap=null,this.lightMapIntensity=1,this.aoMap=null,this.aoMapIntensity=1,this.emissive=new Oe(0),this.emissiveIntensity=1,this.emissiveMap=null,this.bumpMap=null,this.bumpScale=1,this.normalMap=null,this.normalMapType=Gi,this.normalScale=new se(1,1),this.displacementMap=null,this.displacementScale=1,this.displacementBias=0,this.roughnessMap=null,this.metalnessMap=null,this.alphaMap=null,this.envMap=null,this.envMapRotation=new xn,this.envMapIntensity=1,this.wireframe=!1,this.wireframeLinewidth=1,this.wireframeLinecap="round",this.wireframeLinejoin="round",this.flatShading=!1,this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.defines={STANDARD:""},this.color.copy(e.color),this.roughness=e.roughness,this.metalness=e.metalness,this.map=e.map,this.lightMap=e.lightMap,this.lightMapIntensity=e.lightMapIntensity,this.aoMap=e.aoMap,this.aoMapIntensity=e.aoMapIntensity,this.emissive.copy(e.emissive),this.emissiveMap=e.emissiveMap,this.emissiveIntensity=e.emissiveIntensity,this.bumpMap=e.bumpMap,this.bumpScale=e.bumpScale,this.normalMap=e.normalMap,this.normalMapType=e.normalMapType,this.normalScale.copy(e.normalScale),this.displacementMap=e.displacementMap,this.displacementScale=e.displacementScale,this.displacementBias=e.displacementBias,this.roughnessMap=e.roughnessMap,this.metalnessMap=e.metalnessMap,this.alphaMap=e.alphaMap,this.envMap=e.envMap,this.envMapRotation.copy(e.envMapRotation),this.envMapIntensity=e.envMapIntensity,this.wireframe=e.wireframe,this.wireframeLinewidth=e.wireframeLinewidth,this.wireframeLinecap=e.wireframeLinecap,this.wireframeLinejoin=e.wireframeLinejoin,this.flatShading=e.flatShading,this.fog=e.fog,this}}class $p extends kh{constructor(e){super(),this.isMeshPhysicalMaterial=!0,this.defines={STANDARD:"",PHYSICAL:""},this.type="MeshPhysicalMaterial",this.anisotropyRotation=0,this.anisotropyMap=null,this.clearcoatMap=null,this.clearcoatRoughness=0,this.clearcoatRoughnessMap=null,this.clearcoatNormalScale=new se(1,1),this.clearcoatNormalMap=null,this.ior=1.5,Object.defineProperty(this,"reflectivity",{get:function(){return kt(2.5*(this.ior-1)/(this.ior+1),0,1)},set:function(t){this.ior=(1+.4*t)/(1-.4*t)}}),this.iridescenceMap=null,this.iridescenceIOR=1.3,this.iridescenceThicknessRange=[100,400],this.iridescenceThicknessMap=null,this.sheenColor=new Oe(0),this.sheenColorMap=null,this.sheenRoughness=1,this.sheenRoughnessMap=null,this.transmissionMap=null,this.thickness=0,this.thicknessMap=null,this.attenuationDistance=1/0,this.attenuationColor=new Oe(1,1,1),this.specularIntensity=1,this.specularIntensityMap=null,this.specularColor=new Oe(1,1,1),this.specularColorMap=null,this._anisotropy=0,this._clearcoat=0,this._dispersion=0,this._iridescence=0,this._sheen=0,this._transmission=0,this.setValues(e)}get anisotropy(){return this._anisotropy}set anisotropy(e){this._anisotropy>0!=e>0&&this.version++,this._anisotropy=e}get clearcoat(){return this._clearcoat}set clearcoat(e){this._clearcoat>0!=e>0&&this.version++,this._clearcoat=e}get iridescence(){return this._iridescence}set iridescence(e){this._iridescence>0!=e>0&&this.version++,this._iridescence=e}get dispersion(){return this._dispersion}set dispersion(e){this._dispersion>0!=e>0&&this.version++,this._dispersion=e}get sheen(){return this._sheen}set sheen(e){this._sheen>0!=e>0&&this.version++,this._sheen=e}get transmission(){return this._transmission}set transmission(e){this._transmission>0!=e>0&&this.version++,this._transmission=e}copy(e){return super.copy(e),this.defines={STANDARD:"",PHYSICAL:""},this.anisotropy=e.anisotropy,this.anisotropyRotation=e.anisotropyRotation,this.anisotropyMap=e.anisotropyMap,this.clearcoat=e.clearcoat,this.clearcoatMap=e.clearcoatMap,this.clearcoatRoughness=e.clearcoatRoughness,this.clearcoatRoughnessMap=e.clearcoatRoughnessMap,this.clearcoatNormalMap=e.clearcoatNormalMap,this.clearcoatNormalScale.copy(e.clearcoatNormalScale),this.dispersion=e.dispersion,this.ior=e.ior,this.iridescence=e.iridescence,this.iridescenceMap=e.iridescenceMap,this.iridescenceIOR=e.iridescenceIOR,this.iridescenceThicknessRange=[...e.iridescenceThicknessRange],this.iridescenceThicknessMap=e.iridescenceThicknessMap,this.sheen=e.sheen,this.sheenColor.copy(e.sheenColor),this.sheenColorMap=e.sheenColorMap,this.sheenRoughness=e.sheenRoughness,this.sheenRoughnessMap=e.sheenRoughnessMap,this.transmission=e.transmission,this.transmissionMap=e.transmissionMap,this.thickness=e.thickness,this.thicknessMap=e.thicknessMap,this.attenuationDistance=e.attenuationDistance,this.attenuationColor.copy(e.attenuationColor),this.specularIntensity=e.specularIntensity,this.specularIntensityMap=e.specularIntensityMap,this.specularColor.copy(e.specularColor),this.specularColorMap=e.specularColorMap,this}}class Qp extends hn{constructor(e){super(),this.isMeshPhongMaterial=!0,this.type="MeshPhongMaterial",this.color=new Oe(16777215),this.specular=new Oe(1118481),this.shininess=30,this.map=null,this.lightMap=null,this.lightMapIntensity=1,this.aoMap=null,this.aoMapIntensity=1,this.emissive=new Oe(0),this.emissiveIntensity=1,this.emissiveMap=null,this.bumpMap=null,this.bumpScale=1,this.normalMap=null,this.normalMapType=Gi,this.normalScale=new se(1,1),this.displacementMap=null,this.displacementScale=1,this.displacementBias=0,this.specularMap=null,this.alphaMap=null,this.envMap=null,this.envMapRotation=new xn,this.combine=vo,this.reflectivity=1,this.refractionRatio=.98,this.wireframe=!1,this.wireframeLinewidth=1,this.wireframeLinecap="round",this.wireframeLinejoin="round",this.flatShading=!1,this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.color.copy(e.color),this.specular.copy(e.specular),this.shininess=e.shininess,this.map=e.map,this.lightMap=e.lightMap,this.lightMapIntensity=e.lightMapIntensity,this.aoMap=e.aoMap,this.aoMapIntensity=e.aoMapIntensity,this.emissive.copy(e.emissive),this.emissiveMap=e.emissiveMap,this.emissiveIntensity=e.emissiveIntensity,this.bumpMap=e.bumpMap,this.bumpScale=e.bumpScale,this.normalMap=e.normalMap,this.normalMapType=e.normalMapType,this.normalScale.copy(e.normalScale),this.displacementMap=e.displacementMap,this.displacementScale=e.displacementScale,this.displacementBias=e.displacementBias,this.specularMap=e.specularMap,this.alphaMap=e.alphaMap,this.envMap=e.envMap,this.envMapRotation.copy(e.envMapRotation),this.combine=e.combine,this.reflectivity=e.reflectivity,this.refractionRatio=e.refractionRatio,this.wireframe=e.wireframe,this.wireframeLinewidth=e.wireframeLinewidth,this.wireframeLinecap=e.wireframeLinecap,this.wireframeLinejoin=e.wireframeLinejoin,this.flatShading=e.flatShading,this.fog=e.fog,this}}class em extends hn{constructor(e){super(),this.isMeshToonMaterial=!0,this.defines={TOON:""},this.type="MeshToonMaterial",this.color=new Oe(16777215),this.map=null,this.gradientMap=null,this.lightMap=null,this.lightMapIntensity=1,this.aoMap=null,this.aoMapIntensity=1,this.emissive=new Oe(0),this.emissiveIntensity=1,this.emissiveMap=null,this.bumpMap=null,this.bumpScale=1,this.normalMap=null,this.normalMapType=Gi,this.normalScale=new se(1,1),this.displacementMap=null,this.displacementScale=1,this.displacementBias=0,this.alphaMap=null,this.wireframe=!1,this.wireframeLinewidth=1,this.wireframeLinecap="round",this.wireframeLinejoin="round",this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.color.copy(e.color),this.map=e.map,this.gradientMap=e.gradientMap,this.lightMap=e.lightMap,this.lightMapIntensity=e.lightMapIntensity,this.aoMap=e.aoMap,this.aoMapIntensity=e.aoMapIntensity,this.emissive.copy(e.emissive),this.emissiveMap=e.emissiveMap,this.emissiveIntensity=e.emissiveIntensity,this.bumpMap=e.bumpMap,this.bumpScale=e.bumpScale,this.normalMap=e.normalMap,this.normalMapType=e.normalMapType,this.normalScale.copy(e.normalScale),this.displacementMap=e.displacementMap,this.displacementScale=e.displacementScale,this.displacementBias=e.displacementBias,this.alphaMap=e.alphaMap,this.wireframe=e.wireframe,this.wireframeLinewidth=e.wireframeLinewidth,this.wireframeLinecap=e.wireframeLinecap,this.wireframeLinejoin=e.wireframeLinejoin,this.fog=e.fog,this}}class tm extends hn{constructor(e){super(),this.isMeshNormalMaterial=!0,this.type="MeshNormalMaterial",this.bumpMap=null,this.bumpScale=1,this.normalMap=null,this.normalMapType=Gi,this.normalScale=new se(1,1),this.displacementMap=null,this.displacementScale=1,this.displacementBias=0,this.wireframe=!1,this.wireframeLinewidth=1,this.flatShading=!1,this.setValues(e)}copy(e){return super.copy(e),this.bumpMap=e.bumpMap,this.bumpScale=e.bumpScale,this.normalMap=e.normalMap,this.normalMapType=e.normalMapType,this.normalScale.copy(e.normalScale),this.displacementMap=e.displacementMap,this.displacementScale=e.displacementScale,this.displacementBias=e.displacementBias,this.wireframe=e.wireframe,this.wireframeLinewidth=e.wireframeLinewidth,this.flatShading=e.flatShading,this}}class Vh extends hn{constructor(e){super(),this.isMeshLambertMaterial=!0,this.type="MeshLambertMaterial",this.color=new Oe(16777215),this.map=null,this.lightMap=null,this.lightMapIntensity=1,this.aoMap=null,this.aoMapIntensity=1,this.emissive=new Oe(0),this.emissiveIntensity=1,this.emissiveMap=null,this.bumpMap=null,this.bumpScale=1,this.normalMap=null,this.normalMapType=Gi,this.normalScale=new se(1,1),this.displacementMap=null,this.displacementScale=1,this.displacementBias=0,this.specularMap=null,this.alphaMap=null,this.envMap=null,this.envMapRotation=new xn,this.combine=vo,this.reflectivity=1,this.refractionRatio=.98,this.wireframe=!1,this.wireframeLinewidth=1,this.wireframeLinecap="round",this.wireframeLinejoin="round",this.flatShading=!1,this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.color.copy(e.color),this.map=e.map,this.lightMap=e.lightMap,this.lightMapIntensity=e.lightMapIntensity,this.aoMap=e.aoMap,this.aoMapIntensity=e.aoMapIntensity,this.emissive.copy(e.emissive),this.emissiveMap=e.emissiveMap,this.emissiveIntensity=e.emissiveIntensity,this.bumpMap=e.bumpMap,this.bumpScale=e.bumpScale,this.normalMap=e.normalMap,this.normalMapType=e.normalMapType,this.normalScale.copy(e.normalScale),this.displacementMap=e.displacementMap,this.displacementScale=e.displacementScale,this.displacementBias=e.displacementBias,this.specularMap=e.specularMap,this.alphaMap=e.alphaMap,this.envMap=e.envMap,this.envMapRotation.copy(e.envMapRotation),this.combine=e.combine,this.reflectivity=e.reflectivity,this.refractionRatio=e.refractionRatio,this.wireframe=e.wireframe,this.wireframeLinewidth=e.wireframeLinewidth,this.wireframeLinecap=e.wireframeLinecap,this.wireframeLinejoin=e.wireframeLinejoin,this.flatShading=e.flatShading,this.fog=e.fog,this}}class nm extends hn{constructor(e){super(),this.isMeshMatcapMaterial=!0,this.defines={MATCAP:""},this.type="MeshMatcapMaterial",this.color=new Oe(16777215),this.matcap=null,this.map=null,this.bumpMap=null,this.bumpScale=1,this.normalMap=null,this.normalMapType=Gi,this.normalScale=new se(1,1),this.displacementMap=null,this.displacementScale=1,this.displacementBias=0,this.alphaMap=null,this.flatShading=!1,this.fog=!0,this.setValues(e)}copy(e){return super.copy(e),this.defines={MATCAP:""},this.color.copy(e.color),this.matcap=e.matcap,this.map=e.map,this.bumpMap=e.bumpMap,this.bumpScale=e.bumpScale,this.normalMap=e.normalMap,this.normalMapType=e.normalMapType,this.normalScale.copy(e.normalScale),this.displacementMap=e.displacementMap,this.displacementScale=e.displacementScale,this.displacementBias=e.displacementBias,this.alphaMap=e.alphaMap,this.flatShading=e.flatShading,this.fog=e.fog,this}}class im extends un{constructor(e){super(),this.isLineDashedMaterial=!0,this.type="LineDashedMaterial",this.scale=1,this.dashSize=3,this.gapSize=1,this.setValues(e)}copy(e){return super.copy(e),this.scale=e.scale,this.dashSize=e.dashSize,this.gapSize=e.gapSize,this}}function yr(s,e,t){return!s||!t&&s.constructor===e?s:typeof e.BYTES_PER_ELEMENT=="number"?new e(s):Array.prototype.slice.call(s)}function rm(s){return ArrayBuffer.isView(s)&&!(s instanceof DataView)}function sm(s){function e(i,r){return s[i]-s[r]}const t=s.length,n=new Array(t);for(let i=0;i!==t;++i)n[i]=i;return n.sort(e),n}function Qc(s,e,t){const n=s.length,i=new s.constructor(n);for(let r=0,o=0;o!==n;++r){const a=t[r]*e;for(let l=0;l!==e;++l)i[o++]=s[a+l]}return i}function Hh(s,e,t,n){let i=1,r=s[0];for(;r!==void 0&&r[n]===void 0;)r=s[i++];if(r===void 0)return;let o=r[n];if(o!==void 0)if(Array.isArray(o))do o=r[n],o!==void 0&&(e.push(r.time),t.push.apply(t,o)),r=s[i++];while(r!==void 0);else if(o.toArray!==void 0)do o=r[n],o!==void 0&&(e.push(r.time),o.toArray(t,t.length)),r=s[i++];while(r!==void 0);else do o=r[n],o!==void 0&&(e.push(r.time),t.push(o)),r=s[i++];while(r!==void 0)}function pb(s,e,t,n,i=30){const r=s.clone();r.name=e;const o=[];for(let l=0;l<r.tracks.length;++l){const u=r.tracks[l],d=u.getValueSize(),m=[],p=[];for(let g=0;g<u.times.length;++g){const _=u.times[g]*i;if(!(_<t||_>=n)){m.push(u.times[g]);for(let M=0;M<d;++M)p.push(u.values[g*d+M])}}m.length!==0&&(u.times=yr(m,u.times.constructor),u.values=yr(p,u.values.constructor),o.push(u))}r.tracks=o;let a=1/0;for(let l=0;l<r.tracks.length;++l)a>r.tracks[l].times[0]&&(a=r.tracks[l].times[0]);for(let l=0;l<r.tracks.length;++l)r.tracks[l].shift(-1*a);return r.resetDuration(),r}function mb(s,e=0,t=s,n=30){n<=0&&(n=30);const i=t.tracks.length,r=e/n;for(let o=0;o<i;++o){const a=t.tracks[o],l=a.ValueTypeName;if(l==="bool"||l==="string")continue;const u=s.tracks.find(function(v){return v.name===a.name&&v.ValueTypeName===l});if(u===void 0)continue;let d=0;const m=a.getValueSize();a.createInterpolant.isInterpolantFactoryMethodGLTFCubicSpline&&(d=m/3);let p=0;const g=u.getValueSize();u.createInterpolant.isInterpolantFactoryMethodGLTFCubicSpline&&(p=g/3);const _=a.times.length-1;let M;if(r<=a.times[0]){const v=d,w=m-d;M=a.values.slice(v,w)}else if(r>=a.times[_]){const v=_*m+d,w=v+m-d;M=a.values.slice(v,w)}else{const v=a.createInterpolant(),w=d,S=m-d;v.evaluate(r),M=v.resultBuffer.slice(w,S)}l==="quaternion"&&new Dt().fromArray(M).normalize().conjugate().toArray(M);const y=u.times.length;for(let v=0;v<y;++v){const w=v*g+p;if(l==="quaternion")Dt.multiplyQuaternionsFlat(u.values,w,M,0,u.values,w);else{const S=g-p*2;for(let T=0;T<S;++T)u.values[w+T]-=M[T]}}}return s.blendMode=yh,s}const gb={convertArray:yr,isTypedArray:rm,getKeyframeOrder:sm,sortedArray:Qc,flattenJSON:Hh,subclip:pb,makeClipAdditive:mb};class wo{constructor(e,t,n,i){this.parameterPositions=e,this._cachedIndex=0,this.resultBuffer=i!==void 0?i:new t.constructor(n),this.sampleValues=t,this.valueSize=n,this.settings=null,this.DefaultSettings_={}}evaluate(e){const t=this.parameterPositions;let n=this._cachedIndex,i=t[n],r=t[n-1];e:{t:{let o;n:{i:if(!(e<i)){for(let a=n+2;;){if(i===void 0){if(e<r)break i;return n=t.length,this._cachedIndex=n,this.copySampleValue_(n-1)}if(n===a)break;if(r=i,i=t[++n],e<i)break t}o=t.length;break n}if(!(e>=r)){const a=t[1];e<a&&(n=2,r=a);for(let l=n-2;;){if(r===void 0)return this._cachedIndex=0,this.copySampleValue_(0);if(n===l)break;if(i=r,r=t[--n-1],e>=r)break t}o=n,n=0;break n}break e}for(;n<o;){const a=n+o>>>1;e<t[a]?o=a:n=a+1}if(i=t[n],r=t[n-1],r===void 0)return this._cachedIndex=0,this.copySampleValue_(0);if(i===void 0)return n=t.length,this._cachedIndex=n,this.copySampleValue_(n-1)}this._cachedIndex=n,this.intervalChanged_(n,r,i)}return this.interpolate_(n,r,e,i)}getSettings_(){return this.settings||this.DefaultSettings_}copySampleValue_(e){const t=this.resultBuffer,n=this.sampleValues,i=this.valueSize,r=e*i;for(let o=0;o!==i;++o)t[o]=n[r+o];return t}interpolate_(){throw new Error("call to abstract method")}intervalChanged_(){}}class om extends wo{constructor(e,t,n,i){super(e,t,n,i),this._weightPrev=-0,this._offsetPrev=-0,this._weightNext=-0,this._offsetNext=-0,this.DefaultSettings_={endingStart:gr,endingEnd:gr}}intervalChanged_(e,t,n){const i=this.parameterPositions;let r=e-2,o=e+1,a=i[r],l=i[o];if(a===void 0)switch(this.getSettings_().endingStart){case vr:r=e,a=2*t-n;break;case to:r=i.length-2,a=t+i[r]-i[r+1];break;default:r=e,a=n}if(l===void 0)switch(this.getSettings_().endingEnd){case vr:o=e,l=2*n-t;break;case to:o=1,l=n+i[1]-i[0];break;default:o=e-1,l=t}const u=(n-t)*.5,d=this.valueSize;this._weightPrev=u/(t-a),this._weightNext=u/(l-n),this._offsetPrev=r*d,this._offsetNext=o*d}interpolate_(e,t,n,i){const r=this.resultBuffer,o=this.sampleValues,a=this.valueSize,l=e*a,u=l-a,d=this._offsetPrev,m=this._offsetNext,p=this._weightPrev,g=this._weightNext,_=(n-t)/(i-t),M=_*_,y=M*_,v=-p*y+2*p*M-p*_,w=(1+p)*y+(-1.5-2*p)*M+(-.5+p)*_+1,S=(-1-g)*y+(1.5+g)*M+.5*_,T=g*y-g*M;for(let z=0;z!==a;++z)r[z]=v*o[d+z]+w*o[u+z]+S*o[l+z]+T*o[m+z];return r}}class Gh extends wo{constructor(e,t,n,i){super(e,t,n,i)}interpolate_(e,t,n,i){const r=this.resultBuffer,o=this.sampleValues,a=this.valueSize,l=e*a,u=l-a,d=(n-t)/(i-t),m=1-d;for(let p=0;p!==a;++p)r[p]=o[u+p]*m+o[l+p]*d;return r}}class am extends wo{constructor(e,t,n,i){super(e,t,n,i)}interpolate_(e){return this.copySampleValue_(e-1)}}class qn{constructor(e,t,n,i){if(e===void 0)throw new Error("THREE.KeyframeTrack: track name is undefined");if(t===void 0||t.length===0)throw new Error("THREE.KeyframeTrack: no keyframes in track named "+e);this.name=e,this.times=yr(t,this.TimeBufferType),this.values=yr(n,this.ValueBufferType),this.setInterpolation(i||this.DefaultInterpolation)}static toJSON(e){const t=e.constructor;let n;if(t.toJSON!==this.toJSON)n=t.toJSON(e);else{n={name:e.name,times:yr(e.times,Array),values:yr(e.values,Array)};const i=e.getInterpolation();i!==e.DefaultInterpolation&&(n.interpolation=i)}return n.type=e.ValueTypeName,n}InterpolantFactoryMethodDiscrete(e){return new am(this.times,this.values,this.getValueSize(),e)}InterpolantFactoryMethodLinear(e){return new Gh(this.times,this.values,this.getValueSize(),e)}InterpolantFactoryMethodSmooth(e){return new om(this.times,this.values,this.getValueSize(),e)}setInterpolation(e){let t;switch(e){case Qs:t=this.InterpolantFactoryMethodDiscrete;break;case eo:t=this.InterpolantFactoryMethodLinear;break;case Oa:t=this.InterpolantFactoryMethodSmooth;break}if(t===void 0){const n="unsupported interpolation for "+this.ValueTypeName+" keyframe track named "+this.name;if(this.createInterpolant===void 0)if(e!==this.DefaultInterpolation)this.setInterpolation(this.DefaultInterpolation);else throw new Error(n);return console.warn("THREE.KeyframeTrack:",n),this}return this.createInterpolant=t,this}getInterpolation(){switch(this.createInterpolant){case this.InterpolantFactoryMethodDiscrete:return Qs;case this.InterpolantFactoryMethodLinear:return eo;case this.InterpolantFactoryMethodSmooth:return Oa}}getValueSize(){return this.values.length/this.times.length}shift(e){if(e!==0){const t=this.times;for(let n=0,i=t.length;n!==i;++n)t[n]+=e}return this}scale(e){if(e!==1){const t=this.times;for(let n=0,i=t.length;n!==i;++n)t[n]*=e}return this}trim(e,t){const n=this.times,i=n.length;let r=0,o=i-1;for(;r!==i&&n[r]<e;)++r;for(;o!==-1&&n[o]>t;)--o;if(++o,r!==0||o!==i){r>=o&&(o=Math.max(o,1),r=o-1);const a=this.getValueSize();this.times=n.slice(r,o),this.values=this.values.slice(r*a,o*a)}return this}validate(){let e=!0;const t=this.getValueSize();t-Math.floor(t)!==0&&(console.error("THREE.KeyframeTrack: Invalid value size in track.",this),e=!1);const n=this.times,i=this.values,r=n.length;r===0&&(console.error("THREE.KeyframeTrack: Track is empty.",this),e=!1);let o=null;for(let a=0;a!==r;a++){const l=n[a];if(typeof l=="number"&&isNaN(l)){console.error("THREE.KeyframeTrack: Time is not a valid number.",this,a,l),e=!1;break}if(o!==null&&o>l){console.error("THREE.KeyframeTrack: Out of order keys.",this,a,l,o),e=!1;break}o=l}if(i!==void 0&&rm(i))for(let a=0,l=i.length;a!==l;++a){const u=i[a];if(isNaN(u)){console.error("THREE.KeyframeTrack: Value is not a valid number.",this,a,u),e=!1;break}}return e}optimize(){const e=this.times.slice(),t=this.values.slice(),n=this.getValueSize(),i=this.getInterpolation()===Oa,r=e.length-1;let o=1;for(let a=1;a<r;++a){let l=!1;const u=e[a],d=e[a+1];if(u!==d&&(a!==1||u!==e[0]))if(i)l=!0;else{const m=a*n,p=m-n,g=m+n;for(let _=0;_!==n;++_){const M=t[m+_];if(M!==t[p+_]||M!==t[g+_]){l=!0;break}}}if(l){if(a!==o){e[o]=e[a];const m=a*n,p=o*n;for(let g=0;g!==n;++g)t[p+g]=t[m+g]}++o}}if(r>0){e[o]=e[r];for(let a=r*n,l=o*n,u=0;u!==n;++u)t[l+u]=t[a+u];++o}return o!==e.length?(this.times=e.slice(0,o),this.values=t.slice(0,o*n)):(this.times=e,this.values=t),this}clone(){const e=this.times.slice(),t=this.values.slice(),n=this.constructor,i=new n(this.name,e,t);return i.createInterpolant=this.createInterpolant,i}}qn.prototype.TimeBufferType=Float32Array;qn.prototype.ValueBufferType=Float32Array;qn.prototype.DefaultInterpolation=eo;class Rr extends qn{}Rr.prototype.ValueTypeName="bool";Rr.prototype.ValueBufferType=Array;Rr.prototype.DefaultInterpolation=Qs;Rr.prototype.InterpolantFactoryMethodLinear=void 0;Rr.prototype.InterpolantFactoryMethodSmooth=void 0;class Wh extends qn{}Wh.prototype.ValueTypeName="color";class fo extends qn{}fo.prototype.ValueTypeName="number";class lm extends wo{constructor(e,t,n,i){super(e,t,n,i)}interpolate_(e,t,n,i){const r=this.resultBuffer,o=this.sampleValues,a=this.valueSize,l=(n-t)/(i-t);let u=e*a;for(let d=u+a;u!==d;u+=4)Dt.slerpFlat(r,0,o,u-a,o,u,l);return r}}class ms extends qn{InterpolantFactoryMethodLinear(e){return new lm(this.times,this.values,this.getValueSize(),e)}}ms.prototype.ValueTypeName="quaternion";ms.prototype.DefaultInterpolation=eo;ms.prototype.InterpolantFactoryMethodSmooth=void 0;class Pr extends qn{}Pr.prototype.ValueTypeName="string";Pr.prototype.ValueBufferType=Array;Pr.prototype.DefaultInterpolation=Qs;Pr.prototype.InterpolantFactoryMethodLinear=void 0;Pr.prototype.InterpolantFactoryMethodSmooth=void 0;class po extends qn{}po.prototype.ValueTypeName="vector";class mo{constructor(e="",t=-1,n=[],i=qa){this.name=e,this.tracks=n,this.duration=t,this.blendMode=i,this.uuid=Rn(),this.duration<0&&this.resetDuration()}static parse(e){const t=[],n=e.tracks,i=1/(e.fps||1);for(let o=0,a=n.length;o!==a;++o)t.push(_b(n[o]).scale(i));const r=new this(e.name,e.duration,t,e.blendMode);return r.uuid=e.uuid,r}static toJSON(e){const t=[],n=e.tracks,i={name:e.name,duration:e.duration,tracks:t,uuid:e.uuid,blendMode:e.blendMode};for(let r=0,o=n.length;r!==o;++r)t.push(qn.toJSON(n[r]));return i}static CreateFromMorphTargetSequence(e,t,n,i){const r=t.length,o=[];for(let a=0;a<r;a++){let l=[],u=[];l.push((a+r-1)%r,a,(a+1)%r),u.push(0,1,0);const d=sm(l);l=Qc(l,1,d),u=Qc(u,1,d),!i&&l[0]===0&&(l.push(r),u.push(u[0])),o.push(new fo(".morphTargetInfluences["+t[a].name+"]",l,u).scale(1/n))}return new this(e,-1,o)}static findByName(e,t){let n=e;if(!Array.isArray(e)){const i=e;n=i.geometry&&i.geometry.animations||i.animations}for(let i=0;i<n.length;i++)if(n[i].name===t)return n[i];return null}static CreateClipsFromMorphTargetSequences(e,t,n){const i={},r=/^([\w-]*?)([\d]+)$/;for(let a=0,l=e.length;a<l;a++){const u=e[a],d=u.name.match(r);if(d&&d.length>1){const m=d[1];let p=i[m];p||(i[m]=p=[]),p.push(u)}}const o=[];for(const a in i)o.push(this.CreateFromMorphTargetSequence(a,i[a],t,n));return o}static parseAnimation(e,t){if(!e)return console.error("THREE.AnimationClip: No animation in JSONLoader data."),null;const n=function(m,p,g,_,M){if(g.length!==0){const y=[],v=[];Hh(g,y,v,_),y.length!==0&&M.push(new m(p,y,v))}},i=[],r=e.name||"default",o=e.fps||30,a=e.blendMode;let l=e.length||-1;const u=e.hierarchy||[];for(let m=0;m<u.length;m++){const p=u[m].keys;if(!(!p||p.length===0))if(p[0].morphTargets){const g={};let _;for(_=0;_<p.length;_++)if(p[_].morphTargets)for(let M=0;M<p[_].morphTargets.length;M++)g[p[_].morphTargets[M]]=-1;for(const M in g){const y=[],v=[];for(let w=0;w!==p[_].morphTargets.length;++w){const S=p[_];y.push(S.time),v.push(S.morphTarget===M?1:0)}i.push(new fo(".morphTargetInfluence["+M+"]",y,v))}l=g.length*o}else{const g=".bones["+t[m].name+"]";n(po,g+".position",p,"pos",i),n(ms,g+".quaternion",p,"rot",i),n(po,g+".scale",p,"scl",i)}}return i.length===0?null:new this(r,l,i,a)}resetDuration(){const e=this.tracks;let t=0;for(let n=0,i=e.length;n!==i;++n){const r=this.tracks[n];t=Math.max(t,r.times[r.times.length-1])}return this.duration=t,this}trim(){for(let e=0;e<this.tracks.length;e++)this.tracks[e].trim(0,this.duration);return this}validate(){let e=!0;for(let t=0;t<this.tracks.length;t++)e=e&&this.tracks[t].validate();return e}optimize(){for(let e=0;e<this.tracks.length;e++)this.tracks[e].optimize();return this}clone(){const e=[];for(let t=0;t<this.tracks.length;t++)e.push(this.tracks[t].clone());return new this.constructor(this.name,this.duration,e,this.blendMode)}toJSON(){return this.constructor.toJSON(this)}}function vb(s){switch(s.toLowerCase()){case"scalar":case"double":case"float":case"number":case"integer":return fo;case"vector":case"vector2":case"vector3":case"vector4":return po;case"color":return Wh;case"quaternion":return ms;case"bool":case"boolean":return Rr;case"string":return Pr}throw new Error("THREE.KeyframeTrack: Unsupported typeName: "+s)}function _b(s){if(s.type===void 0)throw new Error("THREE.KeyframeTrack: track type undefined, can not parse");const e=vb(s.type);if(s.times===void 0){const t=[],n=[];Hh(s.keys,t,n,"value"),s.times=t,s.values=n}return e.parse!==void 0?e.parse(s):new e(s.name,s.times,s.values,s.interpolation)}const _i={enabled:!1,files:{},add:function(s,e){this.enabled!==!1&&(this.files[s]=e)},get:function(s){if(this.enabled!==!1)return this.files[s]},remove:function(s){delete this.files[s]},clear:function(){this.files={}}};class Xh{constructor(e,t,n){const i=this;let r=!1,o=0,a=0,l;const u=[];this.onStart=void 0,this.onLoad=e,this.onProgress=t,this.onError=n,this.itemStart=function(d){a++,r===!1&&i.onStart!==void 0&&i.onStart(d,o,a),r=!0},this.itemEnd=function(d){o++,i.onProgress!==void 0&&i.onProgress(d,o,a),o===a&&(r=!1,i.onLoad!==void 0&&i.onLoad())},this.itemError=function(d){i.onError!==void 0&&i.onError(d)},this.resolveURL=function(d){return l?l(d):d},this.setURLModifier=function(d){return l=d,this},this.addHandler=function(d,m){return u.push(d,m),this},this.removeHandler=function(d){const m=u.indexOf(d);return m!==-1&&u.splice(m,2),this},this.getHandler=function(d){for(let m=0,p=u.length;m<p;m+=2){const g=u[m],_=u[m+1];if(g.global&&(g.lastIndex=0),g.test(d))return _}return null}}}const cm=new Xh;class bn{constructor(e){this.manager=e!==void 0?e:cm,this.crossOrigin="anonymous",this.withCredentials=!1,this.path="",this.resourcePath="",this.requestHeader={}}load(){}loadAsync(e,t){const n=this;return new Promise(function(i,r){n.load(e,i,t,r)})}parse(){}setCrossOrigin(e){return this.crossOrigin=e,this}setWithCredentials(e){return this.withCredentials=e,this}setPath(e){return this.path=e,this}setResourcePath(e){return this.resourcePath=e,this}setRequestHeader(e){return this.requestHeader=e,this}}bn.DEFAULT_MATERIAL_NAME="__DEFAULT";const fi={};class yb extends Error{constructor(e,t){super(e),this.response=t}}class Si extends bn{constructor(e){super(e)}load(e,t,n,i){e===void 0&&(e=""),this.path!==void 0&&(e=this.path+e),e=this.manager.resolveURL(e);const r=_i.get(e);if(r!==void 0)return this.manager.itemStart(e),setTimeout(()=>{t&&t(r),this.manager.itemEnd(e)},0),r;if(fi[e]!==void 0){fi[e].push({onLoad:t,onProgress:n,onError:i});return}fi[e]=[],fi[e].push({onLoad:t,onProgress:n,onError:i});const o=new Request(e,{headers:new Headers(this.requestHeader),credentials:this.withCredentials?"include":"same-origin"}),a=this.mimeType,l=this.responseType;fetch(o).then(u=>{if(u.status===200||u.status===0){if(u.status===0&&console.warn("THREE.FileLoader: HTTP Status 0 received."),typeof ReadableStream>"u"||u.body===void 0||u.body.getReader===void 0)return u;const d=fi[e],m=u.body.getReader(),p=u.headers.get("X-File-Size")||u.headers.get("Content-Length"),g=p?parseInt(p):0,_=g!==0;let M=0;const y=new ReadableStream({start(v){w();function w(){m.read().then(({done:S,value:T})=>{if(S)v.close();else{M+=T.byteLength;const z=new ProgressEvent("progress",{lengthComputable:_,loaded:M,total:g});for(let N=0,A=d.length;N<A;N++){const k=d[N];k.onProgress&&k.onProgress(z)}v.enqueue(T),w()}})}}});return new Response(y)}else throw new yb(`fetch for "${u.url}" responded with ${u.status}: ${u.statusText}`,u)}).then(u=>{switch(l){case"arraybuffer":return u.arrayBuffer();case"blob":return u.blob();case"document":return u.text().then(d=>new DOMParser().parseFromString(d,a));case"json":return u.json();default:if(a===void 0)return u.text();{const m=/charset="?([^;"\s]*)"?/i.exec(a),p=m&&m[1]?m[1].toLowerCase():void 0,g=new TextDecoder(p);return u.arrayBuffer().then(_=>g.decode(_))}}}).then(u=>{_i.add(e,u);const d=fi[e];delete fi[e];for(let m=0,p=d.length;m<p;m++){const g=d[m];g.onLoad&&g.onLoad(u)}}).catch(u=>{const d=fi[e];if(d===void 0)throw this.manager.itemError(e),u;delete fi[e];for(let m=0,p=d.length;m<p;m++){const g=d[m];g.onError&&g.onError(u)}this.manager.itemError(e)}).finally(()=>{this.manager.itemEnd(e)}),this.manager.itemStart(e)}setResponseType(e){return this.responseType=e,this}setMimeType(e){return this.mimeType=e,this}}class xb extends bn{constructor(e){super(e)}load(e,t,n,i){const r=this,o=new Si(this.manager);o.setPath(this.path),o.setRequestHeader(this.requestHeader),o.setWithCredentials(this.withCredentials),o.load(e,function(a){try{t(r.parse(JSON.parse(a)))}catch(l){i?i(l):console.error(l),r.manager.itemError(e)}},n,i)}parse(e){const t=[];for(let n=0;n<e.length;n++){const i=mo.parse(e[n]);t.push(i)}return t}}class Mb extends bn{constructor(e){super(e)}load(e,t,n,i){const r=this,o=[],a=new rl,l=new Si(this.manager);l.setPath(this.path),l.setResponseType("arraybuffer"),l.setRequestHeader(this.requestHeader),l.setWithCredentials(r.withCredentials);let u=0;function d(m){l.load(e[m],function(p){const g=r.parse(p,!0);o[m]={width:g.width,height:g.height,format:g.format,mipmaps:g.mipmaps},u+=1,u===6&&(g.mipmapCount===1&&(a.minFilter=Xt),a.image=o,a.format=g.format,a.needsUpdate=!0,t&&t(a))},n,i)}if(Array.isArray(e))for(let m=0,p=e.length;m<p;++m)d(m);else l.load(e,function(m){const p=r.parse(m,!0);if(p.isCubemap){const g=p.mipmaps.length/p.mipmapCount;for(let _=0;_<g;_++){o[_]={mipmaps:[]};for(let M=0;M<p.mipmapCount;M++)o[_].mipmaps.push(p.mipmaps[_*p.mipmapCount+M]),o[_].format=p.format,o[_].width=p.width,o[_].height=p.height}a.image=o}else a.image.width=p.width,a.image.height=p.height,a.mipmaps=p.mipmaps;p.mipmapCount===1&&(a.minFilter=Xt),a.format=p.format,a.needsUpdate=!0,t&&t(a)},n,i);return a}}class go extends bn{constructor(e){super(e)}load(e,t,n,i){this.path!==void 0&&(e=this.path+e),e=this.manager.resolveURL(e);const r=this,o=_i.get(e);if(o!==void 0)return r.manager.itemStart(e),setTimeout(function(){t&&t(o),r.manager.itemEnd(e)},0),o;const a=ao("img");function l(){d(),_i.add(e,this),t&&t(this),r.manager.itemEnd(e)}function u(m){d(),i&&i(m),r.manager.itemError(e),r.manager.itemEnd(e)}function d(){a.removeEventListener("load",l,!1),a.removeEventListener("error",u,!1)}return a.addEventListener("load",l,!1),a.addEventListener("error",u,!1),e.slice(0,5)!=="data:"&&this.crossOrigin!==void 0&&(a.crossOrigin=this.crossOrigin),r.manager.itemStart(e),a.src=e,a}}class bb extends bn{constructor(e){super(e)}load(e,t,n,i){const r=new xo;r.colorSpace=Un;const o=new go(this.manager);o.setCrossOrigin(this.crossOrigin),o.setPath(this.path);let a=0;function l(u){o.load(e[u],function(d){r.images[u]=d,a++,a===6&&(r.needsUpdate=!0,t&&t(r))},void 0,i)}for(let u=0;u<e.length;++u)l(u);return r}}class Sb extends bn{constructor(e){super(e)}load(e,t,n,i){const r=this,o=new Vi,a=new Si(this.manager);return a.setResponseType("arraybuffer"),a.setRequestHeader(this.requestHeader),a.setPath(this.path),a.setWithCredentials(r.withCredentials),a.load(e,function(l){let u;try{u=r.parse(l)}catch(d){if(i!==void 0)i(d);else{console.error(d);return}}u.image!==void 0?o.image=u.image:u.data!==void 0&&(o.image.width=u.width,o.image.height=u.height,o.image.data=u.data),o.wrapS=u.wrapS!==void 0?u.wrapS:Bn,o.wrapT=u.wrapT!==void 0?u.wrapT:Bn,o.magFilter=u.magFilter!==void 0?u.magFilter:Xt,o.minFilter=u.minFilter!==void 0?u.minFilter:Xt,o.anisotropy=u.anisotropy!==void 0?u.anisotropy:1,u.colorSpace!==void 0&&(o.colorSpace=u.colorSpace),u.flipY!==void 0&&(o.flipY=u.flipY),u.format!==void 0&&(o.format=u.format),u.type!==void 0&&(o.type=u.type),u.mipmaps!==void 0&&(o.mipmaps=u.mipmaps,o.minFilter=Qn),u.mipmapCount===1&&(o.minFilter=Xt),u.generateMipmaps!==void 0&&(o.generateMipmaps=u.generateMipmaps),o.needsUpdate=!0,t&&t(o,u)},n,i),o}}class wb extends bn{constructor(e){super(e)}load(e,t,n,i){const r=new Vt,o=new go(this.manager);return o.setCrossOrigin(this.crossOrigin),o.setPath(this.path),o.load(e,function(a){r.image=a,r.needsUpdate=!0,t!==void 0&&t(r)},n,i),r}}class Xi extends xt{constructor(e,t=1){super(),this.isLight=!0,this.type="Light",this.color=new Oe(e),this.intensity=t}dispose(){}copy(e,t){return super.copy(e,t),this.color.copy(e.color),this.intensity=e.intensity,this}toJSON(e){const t=super.toJSON(e);return t.object.color=this.color.getHex(),t.object.intensity=this.intensity,this.groundColor!==void 0&&(t.object.groundColor=this.groundColor.getHex()),this.distance!==void 0&&(t.object.distance=this.distance),this.angle!==void 0&&(t.object.angle=this.angle),this.decay!==void 0&&(t.object.decay=this.decay),this.penumbra!==void 0&&(t.object.penumbra=this.penumbra),this.shadow!==void 0&&(t.object.shadow=this.shadow.toJSON()),t}}class hm extends Xi{constructor(e,t,n){super(e,n),this.isHemisphereLight=!0,this.type="HemisphereLight",this.position.copy(xt.DEFAULT_UP),this.updateMatrix(),this.groundColor=new Oe(t)}copy(e,t){return super.copy(e,t),this.groundColor.copy(e.groundColor),this}}const cc=new Ke,Nd=new O,Od=new O;class Yh{constructor(e){this.camera=e,this.bias=0,this.normalBias=0,this.radius=1,this.blurSamples=8,this.mapSize=new se(512,512),this.map=null,this.mapPass=null,this.matrix=new Ke,this.autoUpdate=!0,this.needsUpdate=!1,this._frustum=new Mo,this._frameExtents=new se(1,1),this._viewportCount=1,this._viewports=[new Ct(0,0,1,1)]}getViewportCount(){return this._viewportCount}getFrustum(){return this._frustum}updateMatrices(e){const t=this.camera,n=this.matrix;Nd.setFromMatrixPosition(e.matrixWorld),t.position.copy(Nd),Od.setFromMatrixPosition(e.target.matrixWorld),t.lookAt(Od),t.updateMatrixWorld(),cc.multiplyMatrices(t.projectionMatrix,t.matrixWorldInverse),this._frustum.setFromProjectionMatrix(cc),n.set(.5,0,0,.5,0,.5,0,.5,0,0,.5,.5,0,0,0,1),n.multiply(cc)}getViewport(e){return this._viewports[e]}getFrameExtents(){return this._frameExtents}dispose(){this.map&&this.map.dispose(),this.mapPass&&this.mapPass.dispose()}copy(e){return this.camera=e.camera.clone(),this.bias=e.bias,this.radius=e.radius,this.mapSize.copy(e.mapSize),this}clone(){return new this.constructor().copy(this)}toJSON(){const e={};return this.bias!==0&&(e.bias=this.bias),this.normalBias!==0&&(e.normalBias=this.normalBias),this.radius!==1&&(e.radius=this.radius),(this.mapSize.x!==512||this.mapSize.y!==512)&&(e.mapSize=this.mapSize.toArray()),e.camera=this.camera.toJSON(!1).object,delete e.camera.matrix,e}}class Eb extends Yh{constructor(){super(new en(50,1,.5,500)),this.isSpotLightShadow=!0,this.focus=1}updateMatrices(e){const t=this.camera,n=ls*2*e.angle*this.focus,i=this.mapSize.width/this.mapSize.height,r=e.distance||t.far;(n!==t.fov||i!==t.aspect||r!==t.far)&&(t.fov=n,t.aspect=i,t.far=r,t.updateProjectionMatrix()),super.updateMatrices(e)}copy(e){return super.copy(e),this.focus=e.focus,this}}class um extends Xi{constructor(e,t,n=0,i=Math.PI/3,r=0,o=2){super(e,t),this.isSpotLight=!0,this.type="SpotLight",this.position.copy(xt.DEFAULT_UP),this.updateMatrix(),this.target=new xt,this.distance=n,this.angle=i,this.penumbra=r,this.decay=o,this.map=null,this.shadow=new Eb}get power(){return this.intensity*Math.PI}set power(e){this.intensity=e/Math.PI}dispose(){this.shadow.dispose()}copy(e,t){return super.copy(e,t),this.distance=e.distance,this.angle=e.angle,this.penumbra=e.penumbra,this.decay=e.decay,this.target=e.target.clone(),this.shadow=e.shadow.clone(),this}}const Ud=new Ke,Us=new O,hc=new O;class Tb extends Yh{constructor(){super(new en(90,1,.5,500)),this.isPointLightShadow=!0,this._frameExtents=new se(4,2),this._viewportCount=6,this._viewports=[new Ct(2,1,1,1),new Ct(0,1,1,1),new Ct(3,1,1,1),new Ct(1,1,1,1),new Ct(3,0,1,1),new Ct(1,0,1,1)],this._cubeDirections=[new O(1,0,0),new O(-1,0,0),new O(0,0,1),new O(0,0,-1),new O(0,1,0),new O(0,-1,0)],this._cubeUps=[new O(0,1,0),new O(0,1,0),new O(0,1,0),new O(0,1,0),new O(0,0,1),new O(0,0,-1)]}updateMatrices(e,t=0){const n=this.camera,i=this.matrix,r=e.distance||n.far;r!==n.far&&(n.far=r,n.updateProjectionMatrix()),Us.setFromMatrixPosition(e.matrixWorld),n.position.copy(Us),hc.copy(n.position),hc.add(this._cubeDirections[t]),n.up.copy(this._cubeUps[t]),n.lookAt(hc),n.updateMatrixWorld(),i.makeTranslation(-Us.x,-Us.y,-Us.z),Ud.multiplyMatrices(n.projectionMatrix,n.matrixWorldInverse),this._frustum.setFromProjectionMatrix(Ud)}}class dm extends Xi{constructor(e,t,n=0,i=2){super(e,t),this.isPointLight=!0,this.type="PointLight",this.distance=n,this.decay=i,this.shadow=new Tb}get power(){return this.intensity*4*Math.PI}set power(e){this.intensity=e/(4*Math.PI)}dispose(){this.shadow.dispose()}copy(e,t){return super.copy(e,t),this.distance=e.distance,this.decay=e.decay,this.shadow=e.shadow.clone(),this}}class Ab extends Yh{constructor(){super(new bo(-5,5,5,-5,.5,500)),this.isDirectionalLightShadow=!0}}class qh extends Xi{constructor(e,t){super(e,t),this.isDirectionalLight=!0,this.type="DirectionalLight",this.position.copy(xt.DEFAULT_UP),this.updateMatrix(),this.target=new xt,this.shadow=new Ab}dispose(){this.shadow.dispose()}copy(e){return super.copy(e),this.target=e.target.clone(),this.shadow=e.shadow.clone(),this}}class fm extends Xi{constructor(e,t){super(e,t),this.isAmbientLight=!0,this.type="AmbientLight"}}class pm extends Xi{constructor(e,t,n=10,i=10){super(e,t),this.isRectAreaLight=!0,this.type="RectAreaLight",this.width=n,this.height=i}get power(){return this.intensity*this.width*this.height*Math.PI}set power(e){this.intensity=e/(this.width*this.height*Math.PI)}copy(e){return super.copy(e),this.width=e.width,this.height=e.height,this}toJSON(e){const t=super.toJSON(e);return t.object.width=this.width,t.object.height=this.height,t}}class mm{constructor(){this.isSphericalHarmonics3=!0,this.coefficients=[];for(let e=0;e<9;e++)this.coefficients.push(new O)}set(e){for(let t=0;t<9;t++)this.coefficients[t].copy(e[t]);return this}zero(){for(let e=0;e<9;e++)this.coefficients[e].set(0,0,0);return this}getAt(e,t){const n=e.x,i=e.y,r=e.z,o=this.coefficients;return t.copy(o[0]).multiplyScalar(.282095),t.addScaledVector(o[1],.488603*i),t.addScaledVector(o[2],.488603*r),t.addScaledVector(o[3],.488603*n),t.addScaledVector(o[4],1.092548*(n*i)),t.addScaledVector(o[5],1.092548*(i*r)),t.addScaledVector(o[6],.315392*(3*r*r-1)),t.addScaledVector(o[7],1.092548*(n*r)),t.addScaledVector(o[8],.546274*(n*n-i*i)),t}getIrradianceAt(e,t){const n=e.x,i=e.y,r=e.z,o=this.coefficients;return t.copy(o[0]).multiplyScalar(.886227),t.addScaledVector(o[1],2*.511664*i),t.addScaledVector(o[2],2*.511664*r),t.addScaledVector(o[3],2*.511664*n),t.addScaledVector(o[4],2*.429043*n*i),t.addScaledVector(o[5],2*.429043*i*r),t.addScaledVector(o[6],.743125*r*r-.247708),t.addScaledVector(o[7],2*.429043*n*r),t.addScaledVector(o[8],.429043*(n*n-i*i)),t}add(e){for(let t=0;t<9;t++)this.coefficients[t].add(e.coefficients[t]);return this}addScaledSH(e,t){for(let n=0;n<9;n++)this.coefficients[n].addScaledVector(e.coefficients[n],t);return this}scale(e){for(let t=0;t<9;t++)this.coefficients[t].multiplyScalar(e);return this}lerp(e,t){for(let n=0;n<9;n++)this.coefficients[n].lerp(e.coefficients[n],t);return this}equals(e){for(let t=0;t<9;t++)if(!this.coefficients[t].equals(e.coefficients[t]))return!1;return!0}copy(e){return this.set(e.coefficients)}clone(){return new this.constructor().copy(this)}fromArray(e,t=0){const n=this.coefficients;for(let i=0;i<9;i++)n[i].fromArray(e,t+i*3);return this}toArray(e=[],t=0){const n=this.coefficients;for(let i=0;i<9;i++)n[i].toArray(e,t+i*3);return e}static getBasisAt(e,t){const n=e.x,i=e.y,r=e.z;t[0]=.282095,t[1]=.488603*i,t[2]=.488603*r,t[3]=.488603*n,t[4]=1.092548*n*i,t[5]=1.092548*i*r,t[6]=.315392*(3*r*r-1),t[7]=1.092548*n*r,t[8]=.546274*(n*n-i*i)}}class gm extends Xi{constructor(e=new mm,t=1){super(void 0,t),this.isLightProbe=!0,this.sh=e}copy(e){return super.copy(e),this.sh.copy(e.sh),this}fromJSON(e){return this.intensity=e.intensity,this.sh.fromArray(e.sh),this}toJSON(e){const t=super.toJSON(e);return t.object.sh=this.sh.toArray(),t}}class _l extends bn{constructor(e){super(e),this.textures={}}load(e,t,n,i){const r=this,o=new Si(r.manager);o.setPath(r.path),o.setRequestHeader(r.requestHeader),o.setWithCredentials(r.withCredentials),o.load(e,function(a){try{t(r.parse(JSON.parse(a)))}catch(l){i?i(l):console.error(l),r.manager.itemError(e)}},n,i)}parse(e){const t=this.textures;function n(r){return t[r]===void 0&&console.warn("THREE.MaterialLoader: Undefined texture",r),t[r]}const i=_l.createMaterialFromType(e.type);if(e.uuid!==void 0&&(i.uuid=e.uuid),e.name!==void 0&&(i.name=e.name),e.color!==void 0&&i.color!==void 0&&i.color.setHex(e.color),e.roughness!==void 0&&(i.roughness=e.roughness),e.metalness!==void 0&&(i.metalness=e.metalness),e.sheen!==void 0&&(i.sheen=e.sheen),e.sheenColor!==void 0&&(i.sheenColor=new Oe().setHex(e.sheenColor)),e.sheenRoughness!==void 0&&(i.sheenRoughness=e.sheenRoughness),e.emissive!==void 0&&i.emissive!==void 0&&i.emissive.setHex(e.emissive),e.specular!==void 0&&i.specular!==void 0&&i.specular.setHex(e.specular),e.specularIntensity!==void 0&&(i.specularIntensity=e.specularIntensity),e.specularColor!==void 0&&i.specularColor!==void 0&&i.specularColor.setHex(e.specularColor),e.shininess!==void 0&&(i.shininess=e.shininess),e.clearcoat!==void 0&&(i.clearcoat=e.clearcoat),e.clearcoatRoughness!==void 0&&(i.clearcoatRoughness=e.clearcoatRoughness),e.dispersion!==void 0&&(i.dispersion=e.dispersion),e.iridescence!==void 0&&(i.iridescence=e.iridescence),e.iridescenceIOR!==void 0&&(i.iridescenceIOR=e.iridescenceIOR),e.iridescenceThicknessRange!==void 0&&(i.iridescenceThicknessRange=e.iridescenceThicknessRange),e.transmission!==void 0&&(i.transmission=e.transmission),e.thickness!==void 0&&(i.thickness=e.thickness),e.attenuationDistance!==void 0&&(i.attenuationDistance=e.attenuationDistance),e.attenuationColor!==void 0&&i.attenuationColor!==void 0&&i.attenuationColor.setHex(e.attenuationColor),e.anisotropy!==void 0&&(i.anisotropy=e.anisotropy),e.anisotropyRotation!==void 0&&(i.anisotropyRotation=e.anisotropyRotation),e.fog!==void 0&&(i.fog=e.fog),e.flatShading!==void 0&&(i.flatShading=e.flatShading),e.blending!==void 0&&(i.blending=e.blending),e.combine!==void 0&&(i.combine=e.combine),e.side!==void 0&&(i.side=e.side),e.shadowSide!==void 0&&(i.shadowSide=e.shadowSide),e.opacity!==void 0&&(i.opacity=e.opacity),e.transparent!==void 0&&(i.transparent=e.transparent),e.alphaTest!==void 0&&(i.alphaTest=e.alphaTest),e.alphaHash!==void 0&&(i.alphaHash=e.alphaHash),e.depthFunc!==void 0&&(i.depthFunc=e.depthFunc),e.depthTest!==void 0&&(i.depthTest=e.depthTest),e.depthWrite!==void 0&&(i.depthWrite=e.depthWrite),e.colorWrite!==void 0&&(i.colorWrite=e.colorWrite),e.blendSrc!==void 0&&(i.blendSrc=e.blendSrc),e.blendDst!==void 0&&(i.blendDst=e.blendDst),e.blendEquation!==void 0&&(i.blendEquation=e.blendEquation),e.blendSrcAlpha!==void 0&&(i.blendSrcAlpha=e.blendSrcAlpha),e.blendDstAlpha!==void 0&&(i.blendDstAlpha=e.blendDstAlpha),e.blendEquationAlpha!==void 0&&(i.blendEquationAlpha=e.blendEquationAlpha),e.blendColor!==void 0&&i.blendColor!==void 0&&i.blendColor.setHex(e.blendColor),e.blendAlpha!==void 0&&(i.blendAlpha=e.blendAlpha),e.stencilWriteMask!==void 0&&(i.stencilWriteMask=e.stencilWriteMask),e.stencilFunc!==void 0&&(i.stencilFunc=e.stencilFunc),e.stencilRef!==void 0&&(i.stencilRef=e.stencilRef),e.stencilFuncMask!==void 0&&(i.stencilFuncMask=e.stencilFuncMask),e.stencilFail!==void 0&&(i.stencilFail=e.stencilFail),e.stencilZFail!==void 0&&(i.stencilZFail=e.stencilZFail),e.stencilZPass!==void 0&&(i.stencilZPass=e.stencilZPass),e.stencilWrite!==void 0&&(i.stencilWrite=e.stencilWrite),e.wireframe!==void 0&&(i.wireframe=e.wireframe),e.wireframeLinewidth!==void 0&&(i.wireframeLinewidth=e.wireframeLinewidth),e.wireframeLinecap!==void 0&&(i.wireframeLinecap=e.wireframeLinecap),e.wireframeLinejoin!==void 0&&(i.wireframeLinejoin=e.wireframeLinejoin),e.rotation!==void 0&&(i.rotation=e.rotation),e.linewidth!==void 0&&(i.linewidth=e.linewidth),e.dashSize!==void 0&&(i.dashSize=e.dashSize),e.gapSize!==void 0&&(i.gapSize=e.gapSize),e.scale!==void 0&&(i.scale=e.scale),e.polygonOffset!==void 0&&(i.polygonOffset=e.polygonOffset),e.polygonOffsetFactor!==void 0&&(i.polygonOffsetFactor=e.polygonOffsetFactor),e.polygonOffsetUnits!==void 0&&(i.polygonOffsetUnits=e.polygonOffsetUnits),e.dithering!==void 0&&(i.dithering=e.dithering),e.alphaToCoverage!==void 0&&(i.alphaToCoverage=e.alphaToCoverage),e.premultipliedAlpha!==void 0&&(i.premultipliedAlpha=e.premultipliedAlpha),e.forceSinglePass!==void 0&&(i.forceSinglePass=e.forceSinglePass),e.visible!==void 0&&(i.visible=e.visible),e.toneMapped!==void 0&&(i.toneMapped=e.toneMapped),e.userData!==void 0&&(i.userData=e.userData),e.vertexColors!==void 0&&(typeof e.vertexColors=="number"?i.vertexColors=e.vertexColors>0:i.vertexColors=e.vertexColors),e.uniforms!==void 0)for(const r in e.uniforms){const o=e.uniforms[r];switch(i.uniforms[r]={},o.type){case"t":i.uniforms[r].value=n(o.value);break;case"c":i.uniforms[r].value=new Oe().setHex(o.value);break;case"v2":i.uniforms[r].value=new se().fromArray(o.value);break;case"v3":i.uniforms[r].value=new O().fromArray(o.value);break;case"v4":i.uniforms[r].value=new Ct().fromArray(o.value);break;case"m3":i.uniforms[r].value=new ut().fromArray(o.value);break;case"m4":i.uniforms[r].value=new Ke().fromArray(o.value);break;default:i.uniforms[r].value=o.value}}if(e.defines!==void 0&&(i.defines=e.defines),e.vertexShader!==void 0&&(i.vertexShader=e.vertexShader),e.fragmentShader!==void 0&&(i.fragmentShader=e.fragmentShader),e.glslVersion!==void 0&&(i.glslVersion=e.glslVersion),e.extensions!==void 0)for(const r in e.extensions)i.extensions[r]=e.extensions[r];if(e.lights!==void 0&&(i.lights=e.lights),e.clipping!==void 0&&(i.clipping=e.clipping),e.size!==void 0&&(i.size=e.size),e.sizeAttenuation!==void 0&&(i.sizeAttenuation=e.sizeAttenuation),e.map!==void 0&&(i.map=n(e.map)),e.matcap!==void 0&&(i.matcap=n(e.matcap)),e.alphaMap!==void 0&&(i.alphaMap=n(e.alphaMap)),e.bumpMap!==void 0&&(i.bumpMap=n(e.bumpMap)),e.bumpScale!==void 0&&(i.bumpScale=e.bumpScale),e.normalMap!==void 0&&(i.normalMap=n(e.normalMap)),e.normalMapType!==void 0&&(i.normalMapType=e.normalMapType),e.normalScale!==void 0){let r=e.normalScale;Array.isArray(r)===!1&&(r=[r,r]),i.normalScale=new se().fromArray(r)}return e.displacementMap!==void 0&&(i.displacementMap=n(e.displacementMap)),e.displacementScale!==void 0&&(i.displacementScale=e.displacementScale),e.displacementBias!==void 0&&(i.displacementBias=e.displacementBias),e.roughnessMap!==void 0&&(i.roughnessMap=n(e.roughnessMap)),e.metalnessMap!==void 0&&(i.metalnessMap=n(e.metalnessMap)),e.emissiveMap!==void 0&&(i.emissiveMap=n(e.emissiveMap)),e.emissiveIntensity!==void 0&&(i.emissiveIntensity=e.emissiveIntensity),e.specularMap!==void 0&&(i.specularMap=n(e.specularMap)),e.specularIntensityMap!==void 0&&(i.specularIntensityMap=n(e.specularIntensityMap)),e.specularColorMap!==void 0&&(i.specularColorMap=n(e.specularColorMap)),e.envMap!==void 0&&(i.envMap=n(e.envMap)),e.envMapRotation!==void 0&&i.envMapRotation.fromArray(e.envMapRotation),e.envMapIntensity!==void 0&&(i.envMapIntensity=e.envMapIntensity),e.reflectivity!==void 0&&(i.reflectivity=e.reflectivity),e.refractionRatio!==void 0&&(i.refractionRatio=e.refractionRatio),e.lightMap!==void 0&&(i.lightMap=n(e.lightMap)),e.lightMapIntensity!==void 0&&(i.lightMapIntensity=e.lightMapIntensity),e.aoMap!==void 0&&(i.aoMap=n(e.aoMap)),e.aoMapIntensity!==void 0&&(i.aoMapIntensity=e.aoMapIntensity),e.gradientMap!==void 0&&(i.gradientMap=n(e.gradientMap)),e.clearcoatMap!==void 0&&(i.clearcoatMap=n(e.clearcoatMap)),e.clearcoatRoughnessMap!==void 0&&(i.clearcoatRoughnessMap=n(e.clearcoatRoughnessMap)),e.clearcoatNormalMap!==void 0&&(i.clearcoatNormalMap=n(e.clearcoatNormalMap)),e.clearcoatNormalScale!==void 0&&(i.clearcoatNormalScale=new se().fromArray(e.clearcoatNormalScale)),e.iridescenceMap!==void 0&&(i.iridescenceMap=n(e.iridescenceMap)),e.iridescenceThicknessMap!==void 0&&(i.iridescenceThicknessMap=n(e.iridescenceThicknessMap)),e.transmissionMap!==void 0&&(i.transmissionMap=n(e.transmissionMap)),e.thicknessMap!==void 0&&(i.thicknessMap=n(e.thicknessMap)),e.anisotropyMap!==void 0&&(i.anisotropyMap=n(e.anisotropyMap)),e.sheenColorMap!==void 0&&(i.sheenColorMap=n(e.sheenColorMap)),e.sheenRoughnessMap!==void 0&&(i.sheenRoughnessMap=n(e.sheenRoughnessMap)),i}setTextures(e){return this.textures=e,this}static createMaterialFromType(e){const t={ShadowMaterial:Jp,SpriteMaterial:Ph,RawShaderMaterial:zh,ShaderMaterial:tn,PointsMaterial:Lh,MeshPhysicalMaterial:$p,MeshStandardMaterial:kh,MeshPhongMaterial:Qp,MeshToonMaterial:em,MeshNormalMaterial:tm,MeshLambertMaterial:Vh,MeshDepthMaterial:Qa,MeshDistanceMaterial:Ch,MeshBasicMaterial:ii,MeshMatcapMaterial:nm,LineDashedMaterial:im,LineBasicMaterial:un,Material:hn};return new t[e]}}class eh{static decodeText(e){if(typeof TextDecoder<"u")return new TextDecoder().decode(e);let t="";for(let n=0,i=e.length;n<i;n++)t+=String.fromCharCode(e[n]);try{return decodeURIComponent(escape(t))}catch{return t}}static extractUrlBase(e){const t=e.lastIndexOf("/");return t===-1?"./":e.slice(0,t+1)}static resolveURL(e,t){return typeof e!="string"||e===""?"":(/^https?:\/\//i.test(t)&&/^\//.test(e)&&(t=t.replace(/(^https?:\/\/[^\/]+).*/i,"$1")),/^(https?:)?\/\//i.test(e)||/^data:.*,.*$/i.test(e)||/^blob:.*$/i.test(e)?e:t+e)}}class vm extends at{constructor(){super(),this.isInstancedBufferGeometry=!0,this.type="InstancedBufferGeometry",this.instanceCount=1/0}copy(e){return super.copy(e),this.instanceCount=e.instanceCount,this}toJSON(){const e=super.toJSON();return e.instanceCount=this.instanceCount,e.isInstancedBufferGeometry=!0,e}}class _m extends bn{constructor(e){super(e)}load(e,t,n,i){const r=this,o=new Si(r.manager);o.setPath(r.path),o.setRequestHeader(r.requestHeader),o.setWithCredentials(r.withCredentials),o.load(e,function(a){try{t(r.parse(JSON.parse(a)))}catch(l){i?i(l):console.error(l),r.manager.itemError(e)}},n,i)}parse(e){const t={},n={};function i(g,_){if(t[_]!==void 0)return t[_];const y=g.interleavedBuffers[_],v=r(g,y.buffer),w=ns(y.type,v),S=new nl(w,y.stride);return S.uuid=y.uuid,t[_]=S,S}function r(g,_){if(n[_]!==void 0)return n[_];const y=g.arrayBuffers[_],v=new Uint32Array(y).buffer;return n[_]=v,v}const o=e.isInstancedBufferGeometry?new vm:new at,a=e.data.index;if(a!==void 0){const g=ns(a.type,a.array);o.setIndex(new Rt(g,1))}const l=e.data.attributes;for(const g in l){const _=l[g];let M;if(_.isInterleavedBufferAttribute){const y=i(e.data,_.data);M=new Er(y,_.itemSize,_.offset,_.normalized)}else{const y=ns(_.type,_.array),v=_.isInstancedBufferAttribute?hs:Rt;M=new v(y,_.itemSize,_.normalized)}_.name!==void 0&&(M.name=_.name),_.usage!==void 0&&M.setUsage(_.usage),o.setAttribute(g,M)}const u=e.data.morphAttributes;if(u)for(const g in u){const _=u[g],M=[];for(let y=0,v=_.length;y<v;y++){const w=_[y];let S;if(w.isInterleavedBufferAttribute){const T=i(e.data,w.data);S=new Er(T,w.itemSize,w.offset,w.normalized)}else{const T=ns(w.type,w.array);S=new Rt(T,w.itemSize,w.normalized)}w.name!==void 0&&(S.name=w.name),M.push(S)}o.morphAttributes[g]=M}e.data.morphTargetsRelative&&(o.morphTargetsRelative=!0);const m=e.data.groups||e.data.drawcalls||e.data.offsets;if(m!==void 0)for(let g=0,_=m.length;g!==_;++g){const M=m[g];o.addGroup(M.start,M.count,M.materialIndex)}const p=e.data.boundingSphere;if(p!==void 0){const g=new O;p.center!==void 0&&g.fromArray(p.center),o.boundingSphere=new cn(g,p.radius)}return e.name&&(o.name=e.name),e.userData&&(o.userData=e.userData),o}}class Cb extends bn{constructor(e){super(e)}load(e,t,n,i){const r=this,o=this.path===""?eh.extractUrlBase(e):this.path;this.resourcePath=this.resourcePath||o;const a=new Si(this.manager);a.setPath(this.path),a.setRequestHeader(this.requestHeader),a.setWithCredentials(this.withCredentials),a.load(e,function(l){let u=null;try{u=JSON.parse(l)}catch(m){i!==void 0&&i(m),console.error("THREE:ObjectLoader: Can't parse "+e+".",m.message);return}const d=u.metadata;if(d===void 0||d.type===void 0||d.type.toLowerCase()==="geometry"){i!==void 0&&i(new Error("THREE.ObjectLoader: Can't load "+e)),console.error("THREE.ObjectLoader: Can't load "+e);return}r.parse(u,t)},n,i)}async loadAsync(e,t){const n=this,i=this.path===""?eh.extractUrlBase(e):this.path;this.resourcePath=this.resourcePath||i;const r=new Si(this.manager);r.setPath(this.path),r.setRequestHeader(this.requestHeader),r.setWithCredentials(this.withCredentials);const o=await r.loadAsync(e,t),a=JSON.parse(o),l=a.metadata;if(l===void 0||l.type===void 0||l.type.toLowerCase()==="geometry")throw new Error("THREE.ObjectLoader: Can't load "+e);return await n.parseAsync(a)}parse(e,t){const n=this.parseAnimations(e.animations),i=this.parseShapes(e.shapes),r=this.parseGeometries(e.geometries,i),o=this.parseImages(e.images,function(){t!==void 0&&t(u)}),a=this.parseTextures(e.textures,o),l=this.parseMaterials(e.materials,a),u=this.parseObject(e.object,r,l,a,n),d=this.parseSkeletons(e.skeletons,u);if(this.bindSkeletons(u,d),t!==void 0){let m=!1;for(const p in o)if(o[p].data instanceof HTMLImageElement){m=!0;break}m===!1&&t(u)}return u}async parseAsync(e){const t=this.parseAnimations(e.animations),n=this.parseShapes(e.shapes),i=this.parseGeometries(e.geometries,n),r=await this.parseImagesAsync(e.images),o=this.parseTextures(e.textures,r),a=this.parseMaterials(e.materials,o),l=this.parseObject(e.object,i,a,o,t),u=this.parseSkeletons(e.skeletons,l);return this.bindSkeletons(l,u),l}parseShapes(e){const t={};if(e!==void 0)for(let n=0,i=e.length;n<i;n++){const r=new Sr().fromJSON(e[n]);t[r.uuid]=r}return t}parseSkeletons(e,t){const n={},i={};if(t.traverse(function(r){r.isBone&&(i[r.uuid]=r)}),e!==void 0)for(let r=0,o=e.length;r<o;r++){const a=new il().fromJSON(e[r],i);n[a.uuid]=a}return n}parseGeometries(e,t){const n={};if(e!==void 0){const i=new _m;for(let r=0,o=e.length;r<o;r++){let a;const l=e[r];switch(l.type){case"BufferGeometry":case"InstancedBufferGeometry":a=i.parse(l);break;default:l.type in Dd?a=Dd[l.type].fromJSON(l,t):console.warn(`THREE.ObjectLoader: Unsupported geometry type "${l.type}"`)}a.uuid=l.uuid,l.name!==void 0&&(a.name=l.name),l.userData!==void 0&&(a.userData=l.userData),n[l.uuid]=a}}return n}parseMaterials(e,t){const n={},i={};if(e!==void 0){const r=new _l;r.setTextures(t);for(let o=0,a=e.length;o<a;o++){const l=e[o];n[l.uuid]===void 0&&(n[l.uuid]=r.parse(l)),i[l.uuid]=n[l.uuid]}}return i}parseAnimations(e){const t={};if(e!==void 0)for(let n=0;n<e.length;n++){const i=e[n],r=mo.parse(i);t[r.uuid]=r}return t}parseImages(e,t){const n=this,i={};let r;function o(l){return n.manager.itemStart(l),r.load(l,function(){n.manager.itemEnd(l)},void 0,function(){n.manager.itemError(l),n.manager.itemEnd(l)})}function a(l){if(typeof l=="string"){const u=l,d=/^(\/\/)|([a-z]+:(\/\/)?)/i.test(u)?u:n.resourcePath+u;return o(d)}else return l.data?{data:ns(l.type,l.data),width:l.width,height:l.height}:null}if(e!==void 0&&e.length>0){const l=new Xh(t);r=new go(l),r.setCrossOrigin(this.crossOrigin);for(let u=0,d=e.length;u<d;u++){const m=e[u],p=m.url;if(Array.isArray(p)){const g=[];for(let _=0,M=p.length;_<M;_++){const y=p[_],v=a(y);v!==null&&(v instanceof HTMLImageElement?g.push(v):g.push(new Vi(v.data,v.width,v.height)))}i[m.uuid]=new _r(g)}else{const g=a(m.url);i[m.uuid]=new _r(g)}}}return i}async parseImagesAsync(e){const t=this,n={};let i;async function r(o){if(typeof o=="string"){const a=o,l=/^(\/\/)|([a-z]+:(\/\/)?)/i.test(a)?a:t.resourcePath+a;return await i.loadAsync(l)}else return o.data?{data:ns(o.type,o.data),width:o.width,height:o.height}:null}if(e!==void 0&&e.length>0){i=new go(this.manager),i.setCrossOrigin(this.crossOrigin);for(let o=0,a=e.length;o<a;o++){const l=e[o],u=l.url;if(Array.isArray(u)){const d=[];for(let m=0,p=u.length;m<p;m++){const g=u[m],_=await r(g);_!==null&&(_ instanceof HTMLImageElement?d.push(_):d.push(new Vi(_.data,_.width,_.height)))}n[l.uuid]=new _r(d)}else{const d=await r(l.url);n[l.uuid]=new _r(d)}}}return n}parseTextures(e,t){function n(r,o){return typeof r=="number"?r:(console.warn("THREE.ObjectLoader.parseTexture: Constant should be in numeric form.",r),o[r])}const i={};if(e!==void 0)for(let r=0,o=e.length;r<o;r++){const a=e[r];a.image===void 0&&console.warn('THREE.ObjectLoader: No "image" specified for',a.uuid),t[a.image]===void 0&&console.warn("THREE.ObjectLoader: Undefined image",a.image);const l=t[a.image],u=l.data;let d;Array.isArray(u)?(d=new xo,u.length===6&&(d.needsUpdate=!0)):(u&&u.data?d=new Vi:d=new Vt,u&&(d.needsUpdate=!0)),d.source=l,d.uuid=a.uuid,a.name!==void 0&&(d.name=a.name),a.mapping!==void 0&&(d.mapping=n(a.mapping,Rb)),a.channel!==void 0&&(d.channel=a.channel),a.offset!==void 0&&d.offset.fromArray(a.offset),a.repeat!==void 0&&d.repeat.fromArray(a.repeat),a.center!==void 0&&d.center.fromArray(a.center),a.rotation!==void 0&&(d.rotation=a.rotation),a.wrap!==void 0&&(d.wrapS=n(a.wrap[0],Fd),d.wrapT=n(a.wrap[1],Fd)),a.format!==void 0&&(d.format=a.format),a.internalFormat!==void 0&&(d.internalFormat=a.internalFormat),a.type!==void 0&&(d.type=a.type),a.colorSpace!==void 0&&(d.colorSpace=a.colorSpace),a.minFilter!==void 0&&(d.minFilter=n(a.minFilter,Bd)),a.magFilter!==void 0&&(d.magFilter=n(a.magFilter,Bd)),a.anisotropy!==void 0&&(d.anisotropy=a.anisotropy),a.flipY!==void 0&&(d.flipY=a.flipY),a.generateMipmaps!==void 0&&(d.generateMipmaps=a.generateMipmaps),a.premultiplyAlpha!==void 0&&(d.premultiplyAlpha=a.premultiplyAlpha),a.unpackAlignment!==void 0&&(d.unpackAlignment=a.unpackAlignment),a.compareFunction!==void 0&&(d.compareFunction=a.compareFunction),a.userData!==void 0&&(d.userData=a.userData),i[a.uuid]=d}return i}parseObject(e,t,n,i,r){let o;function a(p){return t[p]===void 0&&console.warn("THREE.ObjectLoader: Undefined geometry",p),t[p]}function l(p){if(p!==void 0){if(Array.isArray(p)){const g=[];for(let _=0,M=p.length;_<M;_++){const y=p[_];n[y]===void 0&&console.warn("THREE.ObjectLoader: Undefined material",y),g.push(n[y])}return g}return n[p]===void 0&&console.warn("THREE.ObjectLoader: Undefined material",p),n[p]}}function u(p){return i[p]===void 0&&console.warn("THREE.ObjectLoader: Undefined texture",p),i[p]}let d,m;switch(e.type){case"Scene":o=new Rh,e.background!==void 0&&(Number.isInteger(e.background)?o.background=new Oe(e.background):o.background=u(e.background)),e.environment!==void 0&&(o.environment=u(e.environment)),e.fog!==void 0&&(e.fog.type==="Fog"?o.fog=new tl(e.fog.color,e.fog.near,e.fog.far):e.fog.type==="FogExp2"&&(o.fog=new el(e.fog.color,e.fog.density)),e.fog.name!==""&&(o.fog.name=e.fog.name)),e.backgroundBlurriness!==void 0&&(o.backgroundBlurriness=e.backgroundBlurriness),e.backgroundIntensity!==void 0&&(o.backgroundIntensity=e.backgroundIntensity),e.backgroundRotation!==void 0&&o.backgroundRotation.fromArray(e.backgroundRotation),e.environmentIntensity!==void 0&&(o.environmentIntensity=e.environmentIntensity),e.environmentRotation!==void 0&&o.environmentRotation.fromArray(e.environmentRotation);break;case"PerspectiveCamera":o=new en(e.fov,e.aspect,e.near,e.far),e.focus!==void 0&&(o.focus=e.focus),e.zoom!==void 0&&(o.zoom=e.zoom),e.filmGauge!==void 0&&(o.filmGauge=e.filmGauge),e.filmOffset!==void 0&&(o.filmOffset=e.filmOffset),e.view!==void 0&&(o.view=Object.assign({},e.view));break;case"OrthographicCamera":o=new bo(e.left,e.right,e.top,e.bottom,e.near,e.far),e.zoom!==void 0&&(o.zoom=e.zoom),e.view!==void 0&&(o.view=Object.assign({},e.view));break;case"AmbientLight":o=new fm(e.color,e.intensity);break;case"DirectionalLight":o=new qh(e.color,e.intensity);break;case"PointLight":o=new dm(e.color,e.intensity,e.distance,e.decay);break;case"RectAreaLight":o=new pm(e.color,e.intensity,e.width,e.height);break;case"SpotLight":o=new um(e.color,e.intensity,e.distance,e.angle,e.penumbra,e.decay);break;case"HemisphereLight":o=new hm(e.color,e.groundColor,e.intensity);break;case"LightProbe":o=new gm().fromJSON(e);break;case"SkinnedMesh":d=a(e.geometry),m=l(e.material),o=new Up(d,m),e.bindMode!==void 0&&(o.bindMode=e.bindMode),e.bindMatrix!==void 0&&o.bindMatrix.fromArray(e.bindMatrix),e.skeleton!==void 0&&(o.skeleton=e.skeleton);break;case"Mesh":d=a(e.geometry),m=l(e.material),o=new Ie(d,m);break;case"InstancedMesh":d=a(e.geometry),m=l(e.material);const p=e.count,g=e.instanceMatrix,_=e.instanceColor;o=new Fp(d,m,p),o.instanceMatrix=new hs(new Float32Array(g.array),16),_!==void 0&&(o.instanceColor=new hs(new Float32Array(_.array),_.itemSize));break;case"BatchedMesh":d=a(e.geometry),m=l(e.material),o=new Bp(e.maxGeometryCount,e.maxVertexCount,e.maxIndexCount,m),o.geometry=d,o.perObjectFrustumCulled=e.perObjectFrustumCulled,o.sortObjects=e.sortObjects,o._drawRanges=e.drawRanges,o._reservedRanges=e.reservedRanges,o._visibility=e.visibility,o._active=e.active,o._bounds=e.bounds.map(M=>{const y=new yn;y.min.fromArray(M.boxMin),y.max.fromArray(M.boxMax);const v=new cn;return v.radius=M.sphereRadius,v.center.fromArray(M.sphereCenter),{boxInitialized:M.boxInitialized,box:y,sphereInitialized:M.sphereInitialized,sphere:v}}),o._maxGeometryCount=e.maxGeometryCount,o._maxVertexCount=e.maxVertexCount,o._maxIndexCount=e.maxIndexCount,o._geometryInitialized=e.geometryInitialized,o._geometryCount=e.geometryCount,o._matricesTexture=u(e.matricesTexture.uuid);break;case"LOD":o=new Op;break;case"Line":o=new ln(a(e.geometry),l(e.material));break;case"LineLoop":o=new zp(a(e.geometry),l(e.material));break;case"LineSegments":o=new ri(a(e.geometry),l(e.material));break;case"PointCloud":case"Points":o=new kp(a(e.geometry),l(e.material));break;case"Sprite":o=new Np(l(e.material));break;case"Group":o=new rs;break;case"Bone":o=new Ih;break;default:o=new xt}if(o.uuid=e.uuid,e.name!==void 0&&(o.name=e.name),e.matrix!==void 0?(o.matrix.fromArray(e.matrix),e.matrixAutoUpdate!==void 0&&(o.matrixAutoUpdate=e.matrixAutoUpdate),o.matrixAutoUpdate&&o.matrix.decompose(o.position,o.quaternion,o.scale)):(e.position!==void 0&&o.position.fromArray(e.position),e.rotation!==void 0&&o.rotation.fromArray(e.rotation),e.quaternion!==void 0&&o.quaternion.fromArray(e.quaternion),e.scale!==void 0&&o.scale.fromArray(e.scale)),e.up!==void 0&&o.up.fromArray(e.up),e.castShadow!==void 0&&(o.castShadow=e.castShadow),e.receiveShadow!==void 0&&(o.receiveShadow=e.receiveShadow),e.shadow&&(e.shadow.bias!==void 0&&(o.shadow.bias=e.shadow.bias),e.shadow.normalBias!==void 0&&(o.shadow.normalBias=e.shadow.normalBias),e.shadow.radius!==void 0&&(o.shadow.radius=e.shadow.radius),e.shadow.mapSize!==void 0&&o.shadow.mapSize.fromArray(e.shadow.mapSize),e.shadow.camera!==void 0&&(o.shadow.camera=this.parseObject(e.shadow.camera))),e.visible!==void 0&&(o.visible=e.visible),e.frustumCulled!==void 0&&(o.frustumCulled=e.frustumCulled),e.renderOrder!==void 0&&(o.renderOrder=e.renderOrder),e.userData!==void 0&&(o.userData=e.userData),e.layers!==void 0&&(o.layers.mask=e.layers),e.children!==void 0){const p=e.children;for(let g=0;g<p.length;g++)o.add(this.parseObject(p[g],t,n,i,r))}if(e.animations!==void 0){const p=e.animations;for(let g=0;g<p.length;g++){const _=p[g];o.animations.push(r[_])}}if(e.type==="LOD"){e.autoUpdate!==void 0&&(o.autoUpdate=e.autoUpdate);const p=e.levels;for(let g=0;g<p.length;g++){const _=p[g],M=o.getObjectByProperty("uuid",_.object);M!==void 0&&o.addLevel(M,_.distance,_.hysteresis)}}return o}bindSkeletons(e,t){Object.keys(t).length!==0&&e.traverse(function(n){if(n.isSkinnedMesh===!0&&n.skeleton!==void 0){const i=t[n.skeleton];i===void 0?console.warn("THREE.ObjectLoader: No skeleton found with UUID:",n.skeleton):n.bind(i,n.bindMatrix)}})}}const Rb={UVMapping:Ya,CubeReflectionMapping:Mi,CubeRefractionMapping:Hi,EquirectangularReflectionMapping:js,EquirectangularRefractionMapping:Ks,CubeUVReflectionMapping:us},Fd={RepeatWrapping:Js,ClampToEdgeWrapping:Bn,MirroredRepeatWrapping:$s},Bd={NearestFilter:nn,NearestMipmapNearestFilter:hh,NearestMipmapLinearFilter:ts,LinearFilter:Xt,LinearMipmapNearestFilter:Ws,LinearMipmapLinearFilter:Qn};class Pb extends bn{constructor(e){super(e),this.isImageBitmapLoader=!0,typeof createImageBitmap>"u"&&console.warn("THREE.ImageBitmapLoader: createImageBitmap() not supported."),typeof fetch>"u"&&console.warn("THREE.ImageBitmapLoader: fetch() not supported."),this.options={premultiplyAlpha:"none"}}setOptions(e){return this.options=e,this}load(e,t,n,i){e===void 0&&(e=""),this.path!==void 0&&(e=this.path+e),e=this.manager.resolveURL(e);const r=this,o=_i.get(e);if(o!==void 0){if(r.manager.itemStart(e),o.then){o.then(u=>{t&&t(u),r.manager.itemEnd(e)}).catch(u=>{i&&i(u)});return}return setTimeout(function(){t&&t(o),r.manager.itemEnd(e)},0),o}const a={};a.credentials=this.crossOrigin==="anonymous"?"same-origin":"include",a.headers=this.requestHeader;const l=fetch(e,a).then(function(u){return u.blob()}).then(function(u){return createImageBitmap(u,Object.assign(r.options,{colorSpaceConversion:"none"}))}).then(function(u){return _i.add(e,u),t&&t(u),r.manager.itemEnd(e),u}).catch(function(u){i&&i(u),_i.remove(e),r.manager.itemError(e),r.manager.itemEnd(e)});_i.add(e,l),r.manager.itemStart(e)}}let ya;class Zh{static getContext(){return ya===void 0&&(ya=new(window.AudioContext||window.webkitAudioContext)),ya}static setContext(e){ya=e}}class Ib extends bn{constructor(e){super(e)}load(e,t,n,i){const r=this,o=new Si(this.manager);o.setResponseType("arraybuffer"),o.setPath(this.path),o.setRequestHeader(this.requestHeader),o.setWithCredentials(this.withCredentials),o.load(e,function(l){try{const u=l.slice(0);Zh.getContext().decodeAudioData(u,function(m){t(m)}).catch(a)}catch(u){a(u)}},n,i);function a(l){i?i(l):console.error(l),r.manager.itemError(e)}}}const zd=new Ke,kd=new Ke,rr=new Ke;class Lb{constructor(){this.type="StereoCamera",this.aspect=1,this.eyeSep=.064,this.cameraL=new en,this.cameraL.layers.enable(1),this.cameraL.matrixAutoUpdate=!1,this.cameraR=new en,this.cameraR.layers.enable(2),this.cameraR.matrixAutoUpdate=!1,this._cache={focus:null,fov:null,aspect:null,near:null,far:null,zoom:null,eyeSep:null}}update(e){const t=this._cache;if(t.focus!==e.focus||t.fov!==e.fov||t.aspect!==e.aspect*this.aspect||t.near!==e.near||t.far!==e.far||t.zoom!==e.zoom||t.eyeSep!==this.eyeSep){t.focus=e.focus,t.fov=e.fov,t.aspect=e.aspect*this.aspect,t.near=e.near,t.far=e.far,t.zoom=e.zoom,t.eyeSep=this.eyeSep,rr.copy(e.projectionMatrix);const i=t.eyeSep/2,r=i*t.near/t.focus,o=t.near*Math.tan(br*t.fov*.5)/t.zoom;let a,l;kd.elements[12]=-i,zd.elements[12]=i,a=-o*t.aspect+r,l=o*t.aspect+r,rr.elements[0]=2*t.near/(l-a),rr.elements[8]=(l+a)/(l-a),this.cameraL.projectionMatrix.copy(rr),a=-o*t.aspect-r,l=o*t.aspect-r,rr.elements[0]=2*t.near/(l-a),rr.elements[8]=(l+a)/(l-a),this.cameraR.projectionMatrix.copy(rr)}this.cameraL.matrixWorld.copy(e.matrixWorld).multiply(kd),this.cameraR.matrixWorld.copy(e.matrixWorld).multiply(zd)}}class jh{constructor(e=!0){this.autoStart=e,this.startTime=0,this.oldTime=0,this.elapsedTime=0,this.running=!1}start(){this.startTime=Vd(),this.oldTime=this.startTime,this.elapsedTime=0,this.running=!0}stop(){this.getElapsedTime(),this.running=!1,this.autoStart=!1}getElapsedTime(){return this.getDelta(),this.elapsedTime}getDelta(){let e=0;if(this.autoStart&&!this.running)return this.start(),0;if(this.running){const t=Vd();e=(t-this.oldTime)/1e3,this.oldTime=t,this.elapsedTime+=e}return e}}function Vd(){return(typeof performance>"u"?Date:performance).now()}const sr=new O,Hd=new Dt,Db=new O,or=new O;class Nb extends xt{constructor(){super(),this.type="AudioListener",this.context=Zh.getContext(),this.gain=this.context.createGain(),this.gain.connect(this.context.destination),this.filter=null,this.timeDelta=0,this._clock=new jh}getInput(){return this.gain}removeFilter(){return this.filter!==null&&(this.gain.disconnect(this.filter),this.filter.disconnect(this.context.destination),this.gain.connect(this.context.destination),this.filter=null),this}getFilter(){return this.filter}setFilter(e){return this.filter!==null?(this.gain.disconnect(this.filter),this.filter.disconnect(this.context.destination)):this.gain.disconnect(this.context.destination),this.filter=e,this.gain.connect(this.filter),this.filter.connect(this.context.destination),this}getMasterVolume(){return this.gain.gain.value}setMasterVolume(e){return this.gain.gain.setTargetAtTime(e,this.context.currentTime,.01),this}updateMatrixWorld(e){super.updateMatrixWorld(e);const t=this.context.listener,n=this.up;if(this.timeDelta=this._clock.getDelta(),this.matrixWorld.decompose(sr,Hd,Db),or.set(0,0,-1).applyQuaternion(Hd),t.positionX){const i=this.context.currentTime+this.timeDelta;t.positionX.linearRampToValueAtTime(sr.x,i),t.positionY.linearRampToValueAtTime(sr.y,i),t.positionZ.linearRampToValueAtTime(sr.z,i),t.forwardX.linearRampToValueAtTime(or.x,i),t.forwardY.linearRampToValueAtTime(or.y,i),t.forwardZ.linearRampToValueAtTime(or.z,i),t.upX.linearRampToValueAtTime(n.x,i),t.upY.linearRampToValueAtTime(n.y,i),t.upZ.linearRampToValueAtTime(n.z,i)}else t.setPosition(sr.x,sr.y,sr.z),t.setOrientation(or.x,or.y,or.z,n.x,n.y,n.z)}}class ym extends xt{constructor(e){super(),this.type="Audio",this.listener=e,this.context=e.context,this.gain=this.context.createGain(),this.gain.connect(e.getInput()),this.autoplay=!1,this.buffer=null,this.detune=0,this.loop=!1,this.loopStart=0,this.loopEnd=0,this.offset=0,this.duration=void 0,this.playbackRate=1,this.isPlaying=!1,this.hasPlaybackControl=!0,this.source=null,this.sourceType="empty",this._startedAt=0,this._progress=0,this._connected=!1,this.filters=[]}getOutput(){return this.gain}setNodeSource(e){return this.hasPlaybackControl=!1,this.sourceType="audioNode",this.source=e,this.connect(),this}setMediaElementSource(e){return this.hasPlaybackControl=!1,this.sourceType="mediaNode",this.source=this.context.createMediaElementSource(e),this.connect(),this}setMediaStreamSource(e){return this.hasPlaybackControl=!1,this.sourceType="mediaStreamNode",this.source=this.context.createMediaStreamSource(e),this.connect(),this}setBuffer(e){return this.buffer=e,this.sourceType="buffer",this.autoplay&&this.play(),this}play(e=0){if(this.isPlaying===!0){console.warn("THREE.Audio: Audio is already playing.");return}if(this.hasPlaybackControl===!1){console.warn("THREE.Audio: this Audio has no playback control.");return}this._startedAt=this.context.currentTime+e;const t=this.context.createBufferSource();return t.buffer=this.buffer,t.loop=this.loop,t.loopStart=this.loopStart,t.loopEnd=this.loopEnd,t.onended=this.onEnded.bind(this),t.start(this._startedAt,this._progress+this.offset,this.duration),this.isPlaying=!0,this.source=t,this.setDetune(this.detune),this.setPlaybackRate(this.playbackRate),this.connect()}pause(){if(this.hasPlaybackControl===!1){console.warn("THREE.Audio: this Audio has no playback control.");return}return this.isPlaying===!0&&(this._progress+=Math.max(this.context.currentTime-this._startedAt,0)*this.playbackRate,this.loop===!0&&(this._progress=this._progress%(this.duration||this.buffer.duration)),this.source.stop(),this.source.onended=null,this.isPlaying=!1),this}stop(){if(this.hasPlaybackControl===!1){console.warn("THREE.Audio: this Audio has no playback control.");return}return this._progress=0,this.source!==null&&(this.source.stop(),this.source.onended=null),this.isPlaying=!1,this}connect(){if(this.filters.length>0){this.source.connect(this.filters[0]);for(let e=1,t=this.filters.length;e<t;e++)this.filters[e-1].connect(this.filters[e]);this.filters[this.filters.length-1].connect(this.getOutput())}else this.source.connect(this.getOutput());return this._connected=!0,this}disconnect(){if(this._connected!==!1){if(this.filters.length>0){this.source.disconnect(this.filters[0]);for(let e=1,t=this.filters.length;e<t;e++)this.filters[e-1].disconnect(this.filters[e]);this.filters[this.filters.length-1].disconnect(this.getOutput())}else this.source.disconnect(this.getOutput());return this._connected=!1,this}}getFilters(){return this.filters}setFilters(e){return e||(e=[]),this._connected===!0?(this.disconnect(),this.filters=e.slice(),this.connect()):this.filters=e.slice(),this}setDetune(e){return this.detune=e,this.isPlaying===!0&&this.source.detune!==void 0&&this.source.detune.setTargetAtTime(this.detune,this.context.currentTime,.01),this}getDetune(){return this.detune}getFilter(){return this.getFilters()[0]}setFilter(e){return this.setFilters(e?[e]:[])}setPlaybackRate(e){if(this.hasPlaybackControl===!1){console.warn("THREE.Audio: this Audio has no playback control.");return}return this.playbackRate=e,this.isPlaying===!0&&this.source.playbackRate.setTargetAtTime(this.playbackRate,this.context.currentTime,.01),this}getPlaybackRate(){return this.playbackRate}onEnded(){this.isPlaying=!1}getLoop(){return this.hasPlaybackControl===!1?(console.warn("THREE.Audio: this Audio has no playback control."),!1):this.loop}setLoop(e){if(this.hasPlaybackControl===!1){console.warn("THREE.Audio: this Audio has no playback control.");return}return this.loop=e,this.isPlaying===!0&&(this.source.loop=this.loop),this}setLoopStart(e){return this.loopStart=e,this}setLoopEnd(e){return this.loopEnd=e,this}getVolume(){return this.gain.gain.value}setVolume(e){return this.gain.gain.setTargetAtTime(e,this.context.currentTime,.01),this}}const ar=new O,Gd=new Dt,Ob=new O,lr=new O;class Ub extends ym{constructor(e){super(e),this.panner=this.context.createPanner(),this.panner.panningModel="HRTF",this.panner.connect(this.gain)}connect(){super.connect(),this.panner.connect(this.gain)}disconnect(){super.disconnect(),this.panner.disconnect(this.gain)}getOutput(){return this.panner}getRefDistance(){return this.panner.refDistance}setRefDistance(e){return this.panner.refDistance=e,this}getRolloffFactor(){return this.panner.rolloffFactor}setRolloffFactor(e){return this.panner.rolloffFactor=e,this}getDistanceModel(){return this.panner.distanceModel}setDistanceModel(e){return this.panner.distanceModel=e,this}getMaxDistance(){return this.panner.maxDistance}setMaxDistance(e){return this.panner.maxDistance=e,this}setDirectionalCone(e,t,n){return this.panner.coneInnerAngle=e,this.panner.coneOuterAngle=t,this.panner.coneOuterGain=n,this}updateMatrixWorld(e){if(super.updateMatrixWorld(e),this.hasPlaybackControl===!0&&this.isPlaying===!1)return;this.matrixWorld.decompose(ar,Gd,Ob),lr.set(0,0,1).applyQuaternion(Gd);const t=this.panner;if(t.positionX){const n=this.context.currentTime+this.listener.timeDelta;t.positionX.linearRampToValueAtTime(ar.x,n),t.positionY.linearRampToValueAtTime(ar.y,n),t.positionZ.linearRampToValueAtTime(ar.z,n),t.orientationX.linearRampToValueAtTime(lr.x,n),t.orientationY.linearRampToValueAtTime(lr.y,n),t.orientationZ.linearRampToValueAtTime(lr.z,n)}else t.setPosition(ar.x,ar.y,ar.z),t.setOrientation(lr.x,lr.y,lr.z)}}class Fb{constructor(e,t=2048){this.analyser=e.context.createAnalyser(),this.analyser.fftSize=t,this.data=new Uint8Array(this.analyser.frequencyBinCount),e.getOutput().connect(this.analyser)}getFrequencyData(){return this.analyser.getByteFrequencyData(this.data),this.data}getAverageFrequency(){let e=0;const t=this.getFrequencyData();for(let n=0;n<t.length;n++)e+=t[n];return e/t.length}}class xm{constructor(e,t,n){this.binding=e,this.valueSize=n;let i,r,o;switch(t){case"quaternion":i=this._slerp,r=this._slerpAdditive,o=this._setAdditiveIdentityQuaternion,this.buffer=new Float64Array(n*6),this._workIndex=5;break;case"string":case"bool":i=this._select,r=this._select,o=this._setAdditiveIdentityOther,this.buffer=new Array(n*5);break;default:i=this._lerp,r=this._lerpAdditive,o=this._setAdditiveIdentityNumeric,this.buffer=new Float64Array(n*5)}this._mixBufferRegion=i,this._mixBufferRegionAdditive=r,this._setIdentity=o,this._origIndex=3,this._addIndex=4,this.cumulativeWeight=0,this.cumulativeWeightAdditive=0,this.useCount=0,this.referenceCount=0}accumulate(e,t){const n=this.buffer,i=this.valueSize,r=e*i+i;let o=this.cumulativeWeight;if(o===0){for(let a=0;a!==i;++a)n[r+a]=n[a];o=t}else{o+=t;const a=t/o;this._mixBufferRegion(n,r,0,a,i)}this.cumulativeWeight=o}accumulateAdditive(e){const t=this.buffer,n=this.valueSize,i=n*this._addIndex;this.cumulativeWeightAdditive===0&&this._setIdentity(),this._mixBufferRegionAdditive(t,i,0,e,n),this.cumulativeWeightAdditive+=e}apply(e){const t=this.valueSize,n=this.buffer,i=e*t+t,r=this.cumulativeWeight,o=this.cumulativeWeightAdditive,a=this.binding;if(this.cumulativeWeight=0,this.cumulativeWeightAdditive=0,r<1){const l=t*this._origIndex;this._mixBufferRegion(n,i,l,1-r,t)}o>0&&this._mixBufferRegionAdditive(n,i,this._addIndex*t,1,t);for(let l=t,u=t+t;l!==u;++l)if(n[l]!==n[l+t]){a.setValue(n,i);break}}saveOriginalState(){const e=this.binding,t=this.buffer,n=this.valueSize,i=n*this._origIndex;e.getValue(t,i);for(let r=n,o=i;r!==o;++r)t[r]=t[i+r%n];this._setIdentity(),this.cumulativeWeight=0,this.cumulativeWeightAdditive=0}restoreOriginalState(){const e=this.valueSize*3;this.binding.setValue(this.buffer,e)}_setAdditiveIdentityNumeric(){const e=this._addIndex*this.valueSize,t=e+this.valueSize;for(let n=e;n<t;n++)this.buffer[n]=0}_setAdditiveIdentityQuaternion(){this._setAdditiveIdentityNumeric(),this.buffer[this._addIndex*this.valueSize+3]=1}_setAdditiveIdentityOther(){const e=this._origIndex*this.valueSize,t=this._addIndex*this.valueSize;for(let n=0;n<this.valueSize;n++)this.buffer[t+n]=this.buffer[e+n]}_select(e,t,n,i,r){if(i>=.5)for(let o=0;o!==r;++o)e[t+o]=e[n+o]}_slerp(e,t,n,i){Dt.slerpFlat(e,t,e,t,e,n,i)}_slerpAdditive(e,t,n,i,r){const o=this._workIndex*r;Dt.multiplyQuaternionsFlat(e,o,e,t,e,n),Dt.slerpFlat(e,t,e,t,e,o,i)}_lerp(e,t,n,i,r){const o=1-i;for(let a=0;a!==r;++a){const l=t+a;e[l]=e[l]*o+e[n+a]*i}}_lerpAdditive(e,t,n,i,r){for(let o=0;o!==r;++o){const a=t+o;e[a]=e[a]+e[n+o]*i}}}const Kh="\\[\\]\\.:\\/",Bb=new RegExp("["+Kh+"]","g"),Jh="[^"+Kh+"]",zb="[^"+Kh.replace("\\.","")+"]",kb=/((?:WC+[\/:])*)/.source.replace("WC",Jh),Vb=/(WCOD+)?/.source.replace("WCOD",zb),Hb=/(?:\.(WC+)(?:\[(.+)\])?)?/.source.replace("WC",Jh),Gb=/\.(WC+)(?:\[(.+)\])?/.source.replace("WC",Jh),Wb=new RegExp("^"+kb+Vb+Hb+Gb+"$"),Xb=["material","materials","bones","map"];class Yb{constructor(e,t,n){const i=n||bt.parseTrackName(t);this._targetGroup=e,this._bindings=e.subscribe_(t,i)}getValue(e,t){this.bind();const n=this._targetGroup.nCachedObjects_,i=this._bindings[n];i!==void 0&&i.getValue(e,t)}setValue(e,t){const n=this._bindings;for(let i=this._targetGroup.nCachedObjects_,r=n.length;i!==r;++i)n[i].setValue(e,t)}bind(){const e=this._bindings;for(let t=this._targetGroup.nCachedObjects_,n=e.length;t!==n;++t)e[t].bind()}unbind(){const e=this._bindings;for(let t=this._targetGroup.nCachedObjects_,n=e.length;t!==n;++t)e[t].unbind()}}class bt{constructor(e,t,n){this.path=t,this.parsedPath=n||bt.parseTrackName(t),this.node=bt.findNode(e,this.parsedPath.nodeName),this.rootNode=e,this.getValue=this._getValue_unbound,this.setValue=this._setValue_unbound}static create(e,t,n){return e&&e.isAnimationObjectGroup?new bt.Composite(e,t,n):new bt(e,t,n)}static sanitizeNodeName(e){return e.replace(/\s/g,"_").replace(Bb,"")}static parseTrackName(e){const t=Wb.exec(e);if(t===null)throw new Error("PropertyBinding: Cannot parse trackName: "+e);const n={nodeName:t[2],objectName:t[3],objectIndex:t[4],propertyName:t[5],propertyIndex:t[6]},i=n.nodeName&&n.nodeName.lastIndexOf(".");if(i!==void 0&&i!==-1){const r=n.nodeName.substring(i+1);Xb.indexOf(r)!==-1&&(n.nodeName=n.nodeName.substring(0,i),n.objectName=r)}if(n.propertyName===null||n.propertyName.length===0)throw new Error("PropertyBinding: can not parse propertyName from trackName: "+e);return n}static findNode(e,t){if(t===void 0||t===""||t==="."||t===-1||t===e.name||t===e.uuid)return e;if(e.skeleton){const n=e.skeleton.getBoneByName(t);if(n!==void 0)return n}if(e.children){const n=function(r){for(let o=0;o<r.length;o++){const a=r[o];if(a.name===t||a.uuid===t)return a;const l=n(a.children);if(l)return l}return null},i=n(e.children);if(i)return i}return null}_getValue_unavailable(){}_setValue_unavailable(){}_getValue_direct(e,t){e[t]=this.targetObject[this.propertyName]}_getValue_array(e,t){const n=this.resolvedProperty;for(let i=0,r=n.length;i!==r;++i)e[t++]=n[i]}_getValue_arrayElement(e,t){e[t]=this.resolvedProperty[this.propertyIndex]}_getValue_toArray(e,t){this.resolvedProperty.toArray(e,t)}_setValue_direct(e,t){this.targetObject[this.propertyName]=e[t]}_setValue_direct_setNeedsUpdate(e,t){this.targetObject[this.propertyName]=e[t],this.targetObject.needsUpdate=!0}_setValue_direct_setMatrixWorldNeedsUpdate(e,t){this.targetObject[this.propertyName]=e[t],this.targetObject.matrixWorldNeedsUpdate=!0}_setValue_array(e,t){const n=this.resolvedProperty;for(let i=0,r=n.length;i!==r;++i)n[i]=e[t++]}_setValue_array_setNeedsUpdate(e,t){const n=this.resolvedProperty;for(let i=0,r=n.length;i!==r;++i)n[i]=e[t++];this.targetObject.needsUpdate=!0}_setValue_array_setMatrixWorldNeedsUpdate(e,t){const n=this.resolvedProperty;for(let i=0,r=n.length;i!==r;++i)n[i]=e[t++];this.targetObject.matrixWorldNeedsUpdate=!0}_setValue_arrayElement(e,t){this.resolvedProperty[this.propertyIndex]=e[t]}_setValue_arrayElement_setNeedsUpdate(e,t){this.resolvedProperty[this.propertyIndex]=e[t],this.targetObject.needsUpdate=!0}_setValue_arrayElement_setMatrixWorldNeedsUpdate(e,t){this.resolvedProperty[this.propertyIndex]=e[t],this.targetObject.matrixWorldNeedsUpdate=!0}_setValue_fromArray(e,t){this.resolvedProperty.fromArray(e,t)}_setValue_fromArray_setNeedsUpdate(e,t){this.resolvedProperty.fromArray(e,t),this.targetObject.needsUpdate=!0}_setValue_fromArray_setMatrixWorldNeedsUpdate(e,t){this.resolvedProperty.fromArray(e,t),this.targetObject.matrixWorldNeedsUpdate=!0}_getValue_unbound(e,t){this.bind(),this.getValue(e,t)}_setValue_unbound(e,t){this.bind(),this.setValue(e,t)}bind(){let e=this.node;const t=this.parsedPath,n=t.objectName,i=t.propertyName;let r=t.propertyIndex;if(e||(e=bt.findNode(this.rootNode,t.nodeName),this.node=e),this.getValue=this._getValue_unavailable,this.setValue=this._setValue_unavailable,!e){console.warn("THREE.PropertyBinding: No target node found for track: "+this.path+".");return}if(n){let u=t.objectIndex;switch(n){case"materials":if(!e.material){console.error("THREE.PropertyBinding: Can not bind to material as node does not have a material.",this);return}if(!e.material.materials){console.error("THREE.PropertyBinding: Can not bind to material.materials as node.material does not have a materials array.",this);return}e=e.material.materials;break;case"bones":if(!e.skeleton){console.error("THREE.PropertyBinding: Can not bind to bones as node does not have a skeleton.",this);return}e=e.skeleton.bones;for(let d=0;d<e.length;d++)if(e[d].name===u){u=d;break}break;case"map":if("map"in e){e=e.map;break}if(!e.material){console.error("THREE.PropertyBinding: Can not bind to material as node does not have a material.",this);return}if(!e.material.map){console.error("THREE.PropertyBinding: Can not bind to material.map as node.material does not have a map.",this);return}e=e.material.map;break;default:if(e[n]===void 0){console.error("THREE.PropertyBinding: Can not bind to objectName of node undefined.",this);return}e=e[n]}if(u!==void 0){if(e[u]===void 0){console.error("THREE.PropertyBinding: Trying to bind to objectIndex of objectName, but is undefined.",this,e);return}e=e[u]}}const o=e[i];if(o===void 0){const u=t.nodeName;console.error("THREE.PropertyBinding: Trying to update property for track: "+u+"."+i+" but it wasn't found.",e);return}let a=this.Versioning.None;this.targetObject=e,e.needsUpdate!==void 0?a=this.Versioning.NeedsUpdate:e.matrixWorldNeedsUpdate!==void 0&&(a=this.Versioning.MatrixWorldNeedsUpdate);let l=this.BindingType.Direct;if(r!==void 0){if(i==="morphTargetInfluences"){if(!e.geometry){console.error("THREE.PropertyBinding: Can not bind to morphTargetInfluences because node does not have a geometry.",this);return}if(!e.geometry.morphAttributes){console.error("THREE.PropertyBinding: Can not bind to morphTargetInfluences because node does not have a geometry.morphAttributes.",this);return}e.morphTargetDictionary[r]!==void 0&&(r=e.morphTargetDictionary[r])}l=this.BindingType.ArrayElement,this.resolvedProperty=o,this.propertyIndex=r}else o.fromArray!==void 0&&o.toArray!==void 0?(l=this.BindingType.HasFromToArray,this.resolvedProperty=o):Array.isArray(o)?(l=this.BindingType.EntireArray,this.resolvedProperty=o):this.propertyName=i;this.getValue=this.GetterByBindingType[l],this.setValue=this.SetterByBindingTypeAndVersioning[l][a]}unbind(){this.node=null,this.getValue=this._getValue_unbound,this.setValue=this._setValue_unbound}}bt.Composite=Yb;bt.prototype.BindingType={Direct:0,EntireArray:1,ArrayElement:2,HasFromToArray:3};bt.prototype.Versioning={None:0,NeedsUpdate:1,MatrixWorldNeedsUpdate:2};bt.prototype.GetterByBindingType=[bt.prototype._getValue_direct,bt.prototype._getValue_array,bt.prototype._getValue_arrayElement,bt.prototype._getValue_toArray];bt.prototype.SetterByBindingTypeAndVersioning=[[bt.prototype._setValue_direct,bt.prototype._setValue_direct_setNeedsUpdate,bt.prototype._setValue_direct_setMatrixWorldNeedsUpdate],[bt.prototype._setValue_array,bt.prototype._setValue_array_setNeedsUpdate,bt.prototype._setValue_array_setMatrixWorldNeedsUpdate],[bt.prototype._setValue_arrayElement,bt.prototype._setValue_arrayElement_setNeedsUpdate,bt.prototype._setValue_arrayElement_setMatrixWorldNeedsUpdate],[bt.prototype._setValue_fromArray,bt.prototype._setValue_fromArray_setNeedsUpdate,bt.prototype._setValue_fromArray_setMatrixWorldNeedsUpdate]];class qb{constructor(){this.isAnimationObjectGroup=!0,this.uuid=Rn(),this._objects=Array.prototype.slice.call(arguments),this.nCachedObjects_=0;const e={};this._indicesByUUID=e;for(let n=0,i=arguments.length;n!==i;++n)e[arguments[n].uuid]=n;this._paths=[],this._parsedPaths=[],this._bindings=[],this._bindingsIndicesByPath={};const t=this;this.stats={objects:{get total(){return t._objects.length},get inUse(){return this.total-t.nCachedObjects_}},get bindingsPerObject(){return t._bindings.length}}}add(){const e=this._objects,t=this._indicesByUUID,n=this._paths,i=this._parsedPaths,r=this._bindings,o=r.length;let a,l=e.length,u=this.nCachedObjects_;for(let d=0,m=arguments.length;d!==m;++d){const p=arguments[d],g=p.uuid;let _=t[g];if(_===void 0){_=l++,t[g]=_,e.push(p);for(let M=0,y=o;M!==y;++M)r[M].push(new bt(p,n[M],i[M]))}else if(_<u){a=e[_];const M=--u,y=e[M];t[y.uuid]=_,e[_]=y,t[g]=M,e[M]=p;for(let v=0,w=o;v!==w;++v){const S=r[v],T=S[M];let z=S[_];S[_]=T,z===void 0&&(z=new bt(p,n[v],i[v])),S[M]=z}}else e[_]!==a&&console.error("THREE.AnimationObjectGroup: Different objects with the same UUID detected. Clean the caches or recreate your infrastructure when reloading scenes.")}this.nCachedObjects_=u}remove(){const e=this._objects,t=this._indicesByUUID,n=this._bindings,i=n.length;let r=this.nCachedObjects_;for(let o=0,a=arguments.length;o!==a;++o){const l=arguments[o],u=l.uuid,d=t[u];if(d!==void 0&&d>=r){const m=r++,p=e[m];t[p.uuid]=d,e[d]=p,t[u]=m,e[m]=l;for(let g=0,_=i;g!==_;++g){const M=n[g],y=M[m],v=M[d];M[d]=y,M[m]=v}}}this.nCachedObjects_=r}uncache(){const e=this._objects,t=this._indicesByUUID,n=this._bindings,i=n.length;let r=this.nCachedObjects_,o=e.length;for(let a=0,l=arguments.length;a!==l;++a){const u=arguments[a],d=u.uuid,m=t[d];if(m!==void 0)if(delete t[d],m<r){const p=--r,g=e[p],_=--o,M=e[_];t[g.uuid]=m,e[m]=g,t[M.uuid]=p,e[p]=M,e.pop();for(let y=0,v=i;y!==v;++y){const w=n[y],S=w[p],T=w[_];w[m]=S,w[p]=T,w.pop()}}else{const p=--o,g=e[p];p>0&&(t[g.uuid]=m),e[m]=g,e.pop();for(let _=0,M=i;_!==M;++_){const y=n[_];y[m]=y[p],y.pop()}}}this.nCachedObjects_=r}subscribe_(e,t){const n=this._bindingsIndicesByPath;let i=n[e];const r=this._bindings;if(i!==void 0)return r[i];const o=this._paths,a=this._parsedPaths,l=this._objects,u=l.length,d=this.nCachedObjects_,m=new Array(u);i=r.length,n[e]=i,o.push(e),a.push(t),r.push(m);for(let p=d,g=l.length;p!==g;++p){const _=l[p];m[p]=new bt(_,e,t)}return m}unsubscribe_(e){const t=this._bindingsIndicesByPath,n=t[e];if(n!==void 0){const i=this._paths,r=this._parsedPaths,o=this._bindings,a=o.length-1,l=o[a],u=e[a];t[u]=n,o[n]=l,o.pop(),r[n]=r[a],r.pop(),i[n]=i[a],i.pop()}}}class Mm{constructor(e,t,n=null,i=t.blendMode){this._mixer=e,this._clip=t,this._localRoot=n,this.blendMode=i;const r=t.tracks,o=r.length,a=new Array(o),l={endingStart:gr,endingEnd:gr};for(let u=0;u!==o;++u){const d=r[u].createInterpolant(null);a[u]=d,d.settings=l}this._interpolantSettings=l,this._interpolants=a,this._propertyBindings=new Array(o),this._cacheIndex=null,this._byClipCacheIndex=null,this._timeScaleInterpolant=null,this._weightInterpolant=null,this.loop=ip,this._loopCount=-1,this._startTime=null,this.time=0,this.timeScale=1,this._effectiveTimeScale=1,this.weight=1,this._effectiveWeight=1,this.repetitions=1/0,this.paused=!1,this.enabled=!0,this.clampWhenFinished=!1,this.zeroSlopeAtStart=!0,this.zeroSlopeAtEnd=!0}play(){return this._mixer._activateAction(this),this}stop(){return this._mixer._deactivateAction(this),this.reset()}reset(){return this.paused=!1,this.enabled=!0,this.time=0,this._loopCount=-1,this._startTime=null,this.stopFading().stopWarping()}isRunning(){return this.enabled&&!this.paused&&this.timeScale!==0&&this._startTime===null&&this._mixer._isActiveAction(this)}isScheduled(){return this._mixer._isActiveAction(this)}startAt(e){return this._startTime=e,this}setLoop(e,t){return this.loop=e,this.repetitions=t,this}setEffectiveWeight(e){return this.weight=e,this._effectiveWeight=this.enabled?e:0,this.stopFading()}getEffectiveWeight(){return this._effectiveWeight}fadeIn(e){return this._scheduleFading(e,0,1)}fadeOut(e){return this._scheduleFading(e,1,0)}crossFadeFrom(e,t,n){if(e.fadeOut(t),this.fadeIn(t),n){const i=this._clip.duration,r=e._clip.duration,o=r/i,a=i/r;e.warp(1,o,t),this.warp(a,1,t)}return this}crossFadeTo(e,t,n){return e.crossFadeFrom(this,t,n)}stopFading(){const e=this._weightInterpolant;return e!==null&&(this._weightInterpolant=null,this._mixer._takeBackControlInterpolant(e)),this}setEffectiveTimeScale(e){return this.timeScale=e,this._effectiveTimeScale=this.paused?0:e,this.stopWarping()}getEffectiveTimeScale(){return this._effectiveTimeScale}setDuration(e){return this.timeScale=this._clip.duration/e,this.stopWarping()}syncWith(e){return this.time=e.time,this.timeScale=e.timeScale,this.stopWarping()}halt(e){return this.warp(this._effectiveTimeScale,0,e)}warp(e,t,n){const i=this._mixer,r=i.time,o=this.timeScale;let a=this._timeScaleInterpolant;a===null&&(a=i._lendControlInterpolant(),this._timeScaleInterpolant=a);const l=a.parameterPositions,u=a.sampleValues;return l[0]=r,l[1]=r+n,u[0]=e/o,u[1]=t/o,this}stopWarping(){const e=this._timeScaleInterpolant;return e!==null&&(this._timeScaleInterpolant=null,this._mixer._takeBackControlInterpolant(e)),this}getMixer(){return this._mixer}getClip(){return this._clip}getRoot(){return this._localRoot||this._mixer._root}_update(e,t,n,i){if(!this.enabled){this._updateWeight(e);return}const r=this._startTime;if(r!==null){const l=(e-r)*n;l<0||n===0?t=0:(this._startTime=null,t=n*l)}t*=this._updateTimeScale(e);const o=this._updateTime(t),a=this._updateWeight(e);if(a>0){const l=this._interpolants,u=this._propertyBindings;switch(this.blendMode){case yh:for(let d=0,m=l.length;d!==m;++d)l[d].evaluate(o),u[d].accumulateAdditive(a);break;case qa:default:for(let d=0,m=l.length;d!==m;++d)l[d].evaluate(o),u[d].accumulate(i,a)}}}_updateWeight(e){let t=0;if(this.enabled){t=this.weight;const n=this._weightInterpolant;if(n!==null){const i=n.evaluate(e)[0];t*=i,e>n.parameterPositions[1]&&(this.stopFading(),i===0&&(this.enabled=!1))}}return this._effectiveWeight=t,t}_updateTimeScale(e){let t=0;if(!this.paused){t=this.timeScale;const n=this._timeScaleInterpolant;if(n!==null){const i=n.evaluate(e)[0];t*=i,e>n.parameterPositions[1]&&(this.stopWarping(),t===0?this.paused=!0:this.timeScale=t)}}return this._effectiveTimeScale=t,t}_updateTime(e){const t=this._clip.duration,n=this.loop;let i=this.time+e,r=this._loopCount;const o=n===rp;if(e===0)return r===-1?i:o&&(r&1)===1?t-i:i;if(n===np){r===-1&&(this._loopCount=0,this._setEndings(!0,!0,!1));e:{if(i>=t)i=t;else if(i<0)i=0;else{this.time=i;break e}this.clampWhenFinished?this.paused=!0:this.enabled=!1,this.time=i,this._mixer.dispatchEvent({type:"finished",action:this,direction:e<0?-1:1})}}else{if(r===-1&&(e>=0?(r=0,this._setEndings(!0,this.repetitions===0,o)):this._setEndings(this.repetitions===0,!0,o)),i>=t||i<0){const a=Math.floor(i/t);i-=t*a,r+=Math.abs(a);const l=this.repetitions-r;if(l<=0)this.clampWhenFinished?this.paused=!0:this.enabled=!1,i=e>0?t:0,this.time=i,this._mixer.dispatchEvent({type:"finished",action:this,direction:e>0?1:-1});else{if(l===1){const u=e<0;this._setEndings(u,!u,o)}else this._setEndings(!1,!1,o);this._loopCount=r,this.time=i,this._mixer.dispatchEvent({type:"loop",action:this,loopDelta:a})}}else this.time=i;if(o&&(r&1)===1)return t-i}return i}_setEndings(e,t,n){const i=this._interpolantSettings;n?(i.endingStart=vr,i.endingEnd=vr):(e?i.endingStart=this.zeroSlopeAtStart?vr:gr:i.endingStart=to,t?i.endingEnd=this.zeroSlopeAtEnd?vr:gr:i.endingEnd=to)}_scheduleFading(e,t,n){const i=this._mixer,r=i.time;let o=this._weightInterpolant;o===null&&(o=i._lendControlInterpolant(),this._weightInterpolant=o);const a=o.parameterPositions,l=o.sampleValues;return a[0]=r,l[0]=t,a[1]=r+e,l[1]=n,this}}const Zb=new Float32Array(1);class jb extends ni{constructor(e){super(),this._root=e,this._initMemoryManager(),this._accuIndex=0,this.time=0,this.timeScale=1}_bindAction(e,t){const n=e._localRoot||this._root,i=e._clip.tracks,r=i.length,o=e._propertyBindings,a=e._interpolants,l=n.uuid,u=this._bindingsByRootAndName;let d=u[l];d===void 0&&(d={},u[l]=d);for(let m=0;m!==r;++m){const p=i[m],g=p.name;let _=d[g];if(_!==void 0)++_.referenceCount,o[m]=_;else{if(_=o[m],_!==void 0){_._cacheIndex===null&&(++_.referenceCount,this._addInactiveBinding(_,l,g));continue}const M=t&&t._propertyBindings[m].binding.parsedPath;_=new xm(bt.create(n,g,M),p.ValueTypeName,p.getValueSize()),++_.referenceCount,this._addInactiveBinding(_,l,g),o[m]=_}a[m].resultBuffer=_.buffer}}_activateAction(e){if(!this._isActiveAction(e)){if(e._cacheIndex===null){const n=(e._localRoot||this._root).uuid,i=e._clip.uuid,r=this._actionsByClip[i];this._bindAction(e,r&&r.knownActions[0]),this._addInactiveAction(e,i,n)}const t=e._propertyBindings;for(let n=0,i=t.length;n!==i;++n){const r=t[n];r.useCount++===0&&(this._lendBinding(r),r.saveOriginalState())}this._lendAction(e)}}_deactivateAction(e){if(this._isActiveAction(e)){const t=e._propertyBindings;for(let n=0,i=t.length;n!==i;++n){const r=t[n];--r.useCount===0&&(r.restoreOriginalState(),this._takeBackBinding(r))}this._takeBackAction(e)}}_initMemoryManager(){this._actions=[],this._nActiveActions=0,this._actionsByClip={},this._bindings=[],this._nActiveBindings=0,this._bindingsByRootAndName={},this._controlInterpolants=[],this._nActiveControlInterpolants=0;const e=this;this.stats={actions:{get total(){return e._actions.length},get inUse(){return e._nActiveActions}},bindings:{get total(){return e._bindings.length},get inUse(){return e._nActiveBindings}},controlInterpolants:{get total(){return e._controlInterpolants.length},get inUse(){return e._nActiveControlInterpolants}}}}_isActiveAction(e){const t=e._cacheIndex;return t!==null&&t<this._nActiveActions}_addInactiveAction(e,t,n){const i=this._actions,r=this._actionsByClip;let o=r[t];if(o===void 0)o={knownActions:[e],actionByRoot:{}},e._byClipCacheIndex=0,r[t]=o;else{const a=o.knownActions;e._byClipCacheIndex=a.length,a.push(e)}e._cacheIndex=i.length,i.push(e),o.actionByRoot[n]=e}_removeInactiveAction(e){const t=this._actions,n=t[t.length-1],i=e._cacheIndex;n._cacheIndex=i,t[i]=n,t.pop(),e._cacheIndex=null;const r=e._clip.uuid,o=this._actionsByClip,a=o[r],l=a.knownActions,u=l[l.length-1],d=e._byClipCacheIndex;u._byClipCacheIndex=d,l[d]=u,l.pop(),e._byClipCacheIndex=null;const m=a.actionByRoot,p=(e._localRoot||this._root).uuid;delete m[p],l.length===0&&delete o[r],this._removeInactiveBindingsForAction(e)}_removeInactiveBindingsForAction(e){const t=e._propertyBindings;for(let n=0,i=t.length;n!==i;++n){const r=t[n];--r.referenceCount===0&&this._removeInactiveBinding(r)}}_lendAction(e){const t=this._actions,n=e._cacheIndex,i=this._nActiveActions++,r=t[i];e._cacheIndex=i,t[i]=e,r._cacheIndex=n,t[n]=r}_takeBackAction(e){const t=this._actions,n=e._cacheIndex,i=--this._nActiveActions,r=t[i];e._cacheIndex=i,t[i]=e,r._cacheIndex=n,t[n]=r}_addInactiveBinding(e,t,n){const i=this._bindingsByRootAndName,r=this._bindings;let o=i[t];o===void 0&&(o={},i[t]=o),o[n]=e,e._cacheIndex=r.length,r.push(e)}_removeInactiveBinding(e){const t=this._bindings,n=e.binding,i=n.rootNode.uuid,r=n.path,o=this._bindingsByRootAndName,a=o[i],l=t[t.length-1],u=e._cacheIndex;l._cacheIndex=u,t[u]=l,t.pop(),delete a[r],Object.keys(a).length===0&&delete o[i]}_lendBinding(e){const t=this._bindings,n=e._cacheIndex,i=this._nActiveBindings++,r=t[i];e._cacheIndex=i,t[i]=e,r._cacheIndex=n,t[n]=r}_takeBackBinding(e){const t=this._bindings,n=e._cacheIndex,i=--this._nActiveBindings,r=t[i];e._cacheIndex=i,t[i]=e,r._cacheIndex=n,t[n]=r}_lendControlInterpolant(){const e=this._controlInterpolants,t=this._nActiveControlInterpolants++;let n=e[t];return n===void 0&&(n=new Gh(new Float32Array(2),new Float32Array(2),1,Zb),n.__cacheIndex=t,e[t]=n),n}_takeBackControlInterpolant(e){const t=this._controlInterpolants,n=e.__cacheIndex,i=--this._nActiveControlInterpolants,r=t[i];e.__cacheIndex=i,t[i]=e,r.__cacheIndex=n,t[n]=r}clipAction(e,t,n){const i=t||this._root,r=i.uuid;let o=typeof e=="string"?mo.findByName(i,e):e;const a=o!==null?o.uuid:e,l=this._actionsByClip[a];let u=null;if(n===void 0&&(o!==null?n=o.blendMode:n=qa),l!==void 0){const m=l.actionByRoot[r];if(m!==void 0&&m.blendMode===n)return m;u=l.knownActions[0],o===null&&(o=u._clip)}if(o===null)return null;const d=new Mm(this,o,t,n);return this._bindAction(d,u),this._addInactiveAction(d,a,r),d}existingAction(e,t){const n=t||this._root,i=n.uuid,r=typeof e=="string"?mo.findByName(n,e):e,o=r?r.uuid:e,a=this._actionsByClip[o];return a!==void 0&&a.actionByRoot[i]||null}stopAllAction(){const e=this._actions,t=this._nActiveActions;for(let n=t-1;n>=0;--n)e[n].stop();return this}update(e){e*=this.timeScale;const t=this._actions,n=this._nActiveActions,i=this.time+=e,r=Math.sign(e),o=this._accuIndex^=1;for(let u=0;u!==n;++u)t[u]._update(i,e,r,o);const a=this._bindings,l=this._nActiveBindings;for(let u=0;u!==l;++u)a[u].apply(o);return this}setTime(e){this.time=0;for(let t=0;t<this._actions.length;t++)this._actions[t].time=0;return this.update(e)}getRoot(){return this._root}uncacheClip(e){const t=this._actions,n=e.uuid,i=this._actionsByClip,r=i[n];if(r!==void 0){const o=r.knownActions;for(let a=0,l=o.length;a!==l;++a){const u=o[a];this._deactivateAction(u);const d=u._cacheIndex,m=t[t.length-1];u._cacheIndex=null,u._byClipCacheIndex=null,m._cacheIndex=d,t[d]=m,t.pop(),this._removeInactiveBindingsForAction(u)}delete i[n]}}uncacheRoot(e){const t=e.uuid,n=this._actionsByClip;for(const o in n){const a=n[o].actionByRoot,l=a[t];l!==void 0&&(this._deactivateAction(l),this._removeInactiveAction(l))}const i=this._bindingsByRootAndName,r=i[t];if(r!==void 0)for(const o in r){const a=r[o];a.restoreOriginalState(),this._removeInactiveBinding(a)}}uncacheAction(e,t){const n=this.existingAction(e,t);n!==null&&(this._deactivateAction(n),this._removeInactiveAction(n))}}class $h{constructor(e){this.value=e}clone(){return new $h(this.value.clone===void 0?this.value:this.value.clone())}}let Kb=0;class Jb extends ni{constructor(){super(),this.isUniformsGroup=!0,Object.defineProperty(this,"id",{value:Kb++}),this.name="",this.usage=so,this.uniforms=[]}add(e){return this.uniforms.push(e),this}remove(e){const t=this.uniforms.indexOf(e);return t!==-1&&this.uniforms.splice(t,1),this}setName(e){return this.name=e,this}setUsage(e){return this.usage=e,this}dispose(){return this.dispatchEvent({type:"dispose"}),this}copy(e){this.name=e.name,this.usage=e.usage;const t=e.uniforms;this.uniforms.length=0;for(let n=0,i=t.length;n<i;n++){const r=Array.isArray(t[n])?t[n]:[t[n]];for(let o=0;o<r.length;o++)this.uniforms.push(r[o].clone())}return this}clone(){return new this.constructor().copy(this)}}class $b extends nl{constructor(e,t,n=1){super(e,t),this.isInstancedInterleavedBuffer=!0,this.meshPerAttribute=n}copy(e){return super.copy(e),this.meshPerAttribute=e.meshPerAttribute,this}clone(e){const t=super.clone(e);return t.meshPerAttribute=this.meshPerAttribute,t}toJSON(e){const t=super.toJSON(e);return t.isInstancedInterleavedBuffer=!0,t.meshPerAttribute=this.meshPerAttribute,t}}class Qb{constructor(e,t,n,i,r){this.isGLBufferAttribute=!0,this.name="",this.buffer=e,this.type=t,this.itemSize=n,this.elementSize=i,this.count=r,this.version=0}set needsUpdate(e){e===!0&&this.version++}setBuffer(e){return this.buffer=e,this}setType(e,t){return this.type=e,this.elementSize=t,this}setItemSize(e){return this.itemSize=e,this}setCount(e){return this.count=e,this}}const Wd=new Ke;class Qh{constructor(e,t,n=0,i=1/0){this.ray=new Ar(e,t),this.near=n,this.far=i,this.camera=null,this.layers=new Ka,this.params={Mesh:{},Line:{threshold:1},LOD:{},Points:{threshold:1},Sprite:{}}}set(e,t){this.ray.set(e,t)}setFromCamera(e,t){t.isPerspectiveCamera?(this.ray.origin.setFromMatrixPosition(t.matrixWorld),this.ray.direction.set(e.x,e.y,.5).unproject(t).sub(this.ray.origin).normalize(),this.camera=t):t.isOrthographicCamera?(this.ray.origin.set(e.x,e.y,(t.near+t.far)/(t.near-t.far)).unproject(t),this.ray.direction.set(0,0,-1).transformDirection(t.matrixWorld),this.camera=t):console.error("THREE.Raycaster: Unsupported camera type: "+t.type)}setFromXRController(e){return Wd.identity().extractRotation(e.matrixWorld),this.ray.origin.setFromMatrixPosition(e.matrixWorld),this.ray.direction.set(0,0,-1).applyMatrix4(Wd),this}intersectObject(e,t=!0,n=[]){return th(e,this,n,t),n.sort(Xd),n}intersectObjects(e,t=!0,n=[]){for(let i=0,r=e.length;i<r;i++)th(e[i],this,n,t);return n.sort(Xd),n}}function Xd(s,e){return s.distance-e.distance}function th(s,e,t,n){if(s.layers.test(e.layers)&&s.raycast(e,t),n===!0){const i=s.children;for(let r=0,o=i.length;r<o;r++)th(i[r],e,t,!0)}}class nh{constructor(e=1,t=0,n=0){return this.radius=e,this.phi=t,this.theta=n,this}set(e,t,n){return this.radius=e,this.phi=t,this.theta=n,this}copy(e){return this.radius=e.radius,this.phi=e.phi,this.theta=e.theta,this}makeSafe(){return this.phi=Math.max(1e-6,Math.min(Math.PI-1e-6,this.phi)),this}setFromVector3(e){return this.setFromCartesianCoords(e.x,e.y,e.z)}setFromCartesianCoords(e,t,n){return this.radius=Math.sqrt(e*e+t*t+n*n),this.radius===0?(this.theta=0,this.phi=0):(this.theta=Math.atan2(e,n),this.phi=Math.acos(kt(t/this.radius,-1,1))),this}clone(){return new this.constructor().copy(this)}}class eS{constructor(e=1,t=0,n=0){return this.radius=e,this.theta=t,this.y=n,this}set(e,t,n){return this.radius=e,this.theta=t,this.y=n,this}copy(e){return this.radius=e.radius,this.theta=e.theta,this.y=e.y,this}setFromVector3(e){return this.setFromCartesianCoords(e.x,e.y,e.z)}setFromCartesianCoords(e,t,n){return this.radius=Math.sqrt(e*e+n*n),this.theta=Math.atan2(e,n),this.y=t,this}clone(){return new this.constructor().copy(this)}}const Yd=new se;class tS{constructor(e=new se(1/0,1/0),t=new se(-1/0,-1/0)){this.isBox2=!0,this.min=e,this.max=t}set(e,t){return this.min.copy(e),this.max.copy(t),this}setFromPoints(e){this.makeEmpty();for(let t=0,n=e.length;t<n;t++)this.expandByPoint(e[t]);return this}setFromCenterAndSize(e,t){const n=Yd.copy(t).multiplyScalar(.5);return this.min.copy(e).sub(n),this.max.copy(e).add(n),this}clone(){return new this.constructor().copy(this)}copy(e){return this.min.copy(e.min),this.max.copy(e.max),this}makeEmpty(){return this.min.x=this.min.y=1/0,this.max.x=this.max.y=-1/0,this}isEmpty(){return this.max.x<this.min.x||this.max.y<this.min.y}getCenter(e){return this.isEmpty()?e.set(0,0):e.addVectors(this.min,this.max).multiplyScalar(.5)}getSize(e){return this.isEmpty()?e.set(0,0):e.subVectors(this.max,this.min)}expandByPoint(e){return this.min.min(e),this.max.max(e),this}expandByVector(e){return this.min.sub(e),this.max.add(e),this}expandByScalar(e){return this.min.addScalar(-e),this.max.addScalar(e),this}containsPoint(e){return!(e.x<this.min.x||e.x>this.max.x||e.y<this.min.y||e.y>this.max.y)}containsBox(e){return this.min.x<=e.min.x&&e.max.x<=this.max.x&&this.min.y<=e.min.y&&e.max.y<=this.max.y}getParameter(e,t){return t.set((e.x-this.min.x)/(this.max.x-this.min.x),(e.y-this.min.y)/(this.max.y-this.min.y))}intersectsBox(e){return!(e.max.x<this.min.x||e.min.x>this.max.x||e.max.y<this.min.y||e.min.y>this.max.y)}clampPoint(e,t){return t.copy(e).clamp(this.min,this.max)}distanceToPoint(e){return this.clampPoint(e,Yd).distanceTo(e)}intersect(e){return this.min.max(e.min),this.max.min(e.max),this.isEmpty()&&this.makeEmpty(),this}union(e){return this.min.min(e.min),this.max.max(e.max),this}translate(e){return this.min.add(e),this.max.add(e),this}equals(e){return e.min.equals(this.min)&&e.max.equals(this.max)}}const qd=new O,xa=new O;class nS{constructor(e=new O,t=new O){this.start=e,this.end=t}set(e,t){return this.start.copy(e),this.end.copy(t),this}copy(e){return this.start.copy(e.start),this.end.copy(e.end),this}getCenter(e){return e.addVectors(this.start,this.end).multiplyScalar(.5)}delta(e){return e.subVectors(this.end,this.start)}distanceSq(){return this.start.distanceToSquared(this.end)}distance(){return this.start.distanceTo(this.end)}at(e,t){return this.delta(t).multiplyScalar(e).add(this.start)}closestPointToPointParameter(e,t){qd.subVectors(e,this.start),xa.subVectors(this.end,this.start);const n=xa.dot(xa);let r=xa.dot(qd)/n;return t&&(r=kt(r,0,1)),r}closestPointToPoint(e,t,n){const i=this.closestPointToPointParameter(e,t);return this.delta(n).multiplyScalar(i).add(this.start)}applyMatrix4(e){return this.start.applyMatrix4(e),this.end.applyMatrix4(e),this}equals(e){return e.start.equals(this.start)&&e.end.equals(this.end)}clone(){return new this.constructor().copy(this)}}const Zd=new O;class iS extends xt{constructor(e,t){super(),this.light=e,this.matrixAutoUpdate=!1,this.color=t,this.type="SpotLightHelper";const n=new at,i=[0,0,0,0,0,1,0,0,0,1,0,1,0,0,0,-1,0,1,0,0,0,0,1,1,0,0,0,0,-1,1];for(let o=0,a=1,l=32;o<l;o++,a++){const u=o/l*Math.PI*2,d=a/l*Math.PI*2;i.push(Math.cos(u),Math.sin(u),1,Math.cos(d),Math.sin(d),1)}n.setAttribute("position",new He(i,3));const r=new un({fog:!1,toneMapped:!1});this.cone=new ri(n,r),this.add(this.cone),this.update()}dispose(){this.cone.geometry.dispose(),this.cone.material.dispose()}update(){this.light.updateWorldMatrix(!0,!1),this.light.target.updateWorldMatrix(!0,!1),this.parent?(this.parent.updateWorldMatrix(!0),this.matrix.copy(this.parent.matrixWorld).invert().multiply(this.light.matrixWorld)):this.matrix.copy(this.light.matrixWorld),this.matrixWorld.copy(this.light.matrixWorld);const e=this.light.distance?this.light.distance:1e3,t=e*Math.tan(this.light.angle);this.cone.scale.set(t,t,e),Zd.setFromMatrixPosition(this.light.target.matrixWorld),this.cone.lookAt(Zd),this.color!==void 0?this.cone.material.color.set(this.color):this.cone.material.color.copy(this.light.color)}}const Fi=new O,Ma=new Ke,uc=new Ke;class rS extends ri{constructor(e){const t=bm(e),n=new at,i=[],r=[],o=new Oe(0,0,1),a=new Oe(0,1,0);for(let u=0;u<t.length;u++){const d=t[u];d.parent&&d.parent.isBone&&(i.push(0,0,0),i.push(0,0,0),r.push(o.r,o.g,o.b),r.push(a.r,a.g,a.b))}n.setAttribute("position",new He(i,3)),n.setAttribute("color",new He(r,3));const l=new un({vertexColors:!0,depthTest:!1,depthWrite:!1,toneMapped:!1,transparent:!0});super(n,l),this.isSkeletonHelper=!0,this.type="SkeletonHelper",this.root=e,this.bones=t,this.matrix=e.matrixWorld,this.matrixAutoUpdate=!1}updateMatrixWorld(e){const t=this.bones,n=this.geometry,i=n.getAttribute("position");uc.copy(this.root.matrixWorld).invert();for(let r=0,o=0;r<t.length;r++){const a=t[r];a.parent&&a.parent.isBone&&(Ma.multiplyMatrices(uc,a.matrixWorld),Fi.setFromMatrixPosition(Ma),i.setXYZ(o,Fi.x,Fi.y,Fi.z),Ma.multiplyMatrices(uc,a.parent.matrixWorld),Fi.setFromMatrixPosition(Ma),i.setXYZ(o+1,Fi.x,Fi.y,Fi.z),o+=2)}n.getAttribute("position").needsUpdate=!0,super.updateMatrixWorld(e)}dispose(){this.geometry.dispose(),this.material.dispose()}}function bm(s){const e=[];s.isBone===!0&&e.push(s);for(let t=0;t<s.children.length;t++)e.push.apply(e,bm(s.children[t]));return e}class sS extends Ie{constructor(e,t,n){const i=new ps(t,4,2),r=new ii({wireframe:!0,fog:!1,toneMapped:!1});super(i,r),this.light=e,this.color=n,this.type="PointLightHelper",this.matrix=this.light.matrixWorld,this.matrixAutoUpdate=!1,this.update()}dispose(){this.geometry.dispose(),this.material.dispose()}update(){this.light.updateWorldMatrix(!0,!1),this.color!==void 0?this.material.color.set(this.color):this.material.color.copy(this.light.color)}}const oS=new O,jd=new Oe,Kd=new Oe;class aS extends xt{constructor(e,t,n){super(),this.light=e,this.matrix=e.matrixWorld,this.matrixAutoUpdate=!1,this.color=n,this.type="HemisphereLightHelper";const i=new vi(t);i.rotateY(Math.PI*.5),this.material=new ii({wireframe:!0,fog:!1,toneMapped:!1}),this.color===void 0&&(this.material.vertexColors=!0);const r=i.getAttribute("position"),o=new Float32Array(r.count*3);i.setAttribute("color",new Rt(o,3)),this.add(new Ie(i,this.material)),this.update()}dispose(){this.children[0].geometry.dispose(),this.children[0].material.dispose()}update(){const e=this.children[0];if(this.color!==void 0)this.material.color.set(this.color);else{const t=e.geometry.getAttribute("color");jd.copy(this.light.color),Kd.copy(this.light.groundColor);for(let n=0,i=t.count;n<i;n++){const r=n<i/2?jd:Kd;t.setXYZ(n,r.r,r.g,r.b)}t.needsUpdate=!0}this.light.updateWorldMatrix(!0,!1),e.lookAt(oS.setFromMatrixPosition(this.light.matrixWorld).negate())}}class Sm extends ri{constructor(e=10,t=10,n=4473924,i=8947848){n=new Oe(n),i=new Oe(i);const r=t/2,o=e/t,a=e/2,l=[],u=[];for(let p=0,g=0,_=-a;p<=t;p++,_+=o){l.push(-a,0,_,a,0,_),l.push(_,0,-a,_,0,a);const M=p===r?n:i;M.toArray(u,g),g+=3,M.toArray(u,g),g+=3,M.toArray(u,g),g+=3,M.toArray(u,g),g+=3}const d=new at;d.setAttribute("position",new He(l,3)),d.setAttribute("color",new He(u,3));const m=new un({vertexColors:!0,toneMapped:!1});super(d,m),this.type="GridHelper"}dispose(){this.geometry.dispose(),this.material.dispose()}}class lS extends ri{constructor(e=10,t=16,n=8,i=64,r=4473924,o=8947848){r=new Oe(r),o=new Oe(o);const a=[],l=[];if(t>1)for(let m=0;m<t;m++){const p=m/t*(Math.PI*2),g=Math.sin(p)*e,_=Math.cos(p)*e;a.push(0,0,0),a.push(g,0,_);const M=m&1?r:o;l.push(M.r,M.g,M.b),l.push(M.r,M.g,M.b)}for(let m=0;m<n;m++){const p=m&1?r:o,g=e-e/n*m;for(let _=0;_<i;_++){let M=_/i*(Math.PI*2),y=Math.sin(M)*g,v=Math.cos(M)*g;a.push(y,0,v),l.push(p.r,p.g,p.b),M=(_+1)/i*(Math.PI*2),y=Math.sin(M)*g,v=Math.cos(M)*g,a.push(y,0,v),l.push(p.r,p.g,p.b)}}const u=new at;u.setAttribute("position",new He(a,3)),u.setAttribute("color",new He(l,3));const d=new un({vertexColors:!0,toneMapped:!1});super(u,d),this.type="PolarGridHelper"}dispose(){this.geometry.dispose(),this.material.dispose()}}const Jd=new O,ba=new O,$d=new O;class cS extends xt{constructor(e,t,n){super(),this.light=e,this.matrix=e.matrixWorld,this.matrixAutoUpdate=!1,this.color=n,this.type="DirectionalLightHelper",t===void 0&&(t=1);let i=new at;i.setAttribute("position",new He([-t,t,0,t,t,0,t,-t,0,-t,-t,0,-t,t,0],3));const r=new un({fog:!1,toneMapped:!1});this.lightPlane=new ln(i,r),this.add(this.lightPlane),i=new at,i.setAttribute("position",new He([0,0,0,0,0,1],3)),this.targetLine=new ln(i,r),this.add(this.targetLine),this.update()}dispose(){this.lightPlane.geometry.dispose(),this.lightPlane.material.dispose(),this.targetLine.geometry.dispose(),this.targetLine.material.dispose()}update(){this.light.updateWorldMatrix(!0,!1),this.light.target.updateWorldMatrix(!0,!1),Jd.setFromMatrixPosition(this.light.matrixWorld),ba.setFromMatrixPosition(this.light.target.matrixWorld),$d.subVectors(ba,Jd),this.lightPlane.lookAt(ba),this.color!==void 0?(this.lightPlane.material.color.set(this.color),this.targetLine.material.color.set(this.color)):(this.lightPlane.material.color.copy(this.light.color),this.targetLine.material.color.copy(this.light.color)),this.targetLine.lookAt(ba),this.targetLine.scale.z=$d.length()}}const Sa=new O,Bt=new Ja;class hS extends ri{constructor(e){const t=new at,n=new un({color:16777215,vertexColors:!0,toneMapped:!1}),i=[],r=[],o={};a("n1","n2"),a("n2","n4"),a("n4","n3"),a("n3","n1"),a("f1","f2"),a("f2","f4"),a("f4","f3"),a("f3","f1"),a("n1","f1"),a("n2","f2"),a("n3","f3"),a("n4","f4"),a("p","n1"),a("p","n2"),a("p","n3"),a("p","n4"),a("u1","u2"),a("u2","u3"),a("u3","u1"),a("c","t"),a("p","c"),a("cn1","cn2"),a("cn3","cn4"),a("cf1","cf2"),a("cf3","cf4");function a(_,M){l(_),l(M)}function l(_){i.push(0,0,0),r.push(0,0,0),o[_]===void 0&&(o[_]=[]),o[_].push(i.length/3-1)}t.setAttribute("position",new He(i,3)),t.setAttribute("color",new He(r,3)),super(t,n),this.type="CameraHelper",this.camera=e,this.camera.updateProjectionMatrix&&this.camera.updateProjectionMatrix(),this.matrix=e.matrixWorld,this.matrixAutoUpdate=!1,this.pointMap=o,this.update();const u=new Oe(16755200),d=new Oe(16711680),m=new Oe(43775),p=new Oe(16777215),g=new Oe(3355443);this.setColors(u,d,m,p,g)}setColors(e,t,n,i,r){const a=this.geometry.getAttribute("color");a.setXYZ(0,e.r,e.g,e.b),a.setXYZ(1,e.r,e.g,e.b),a.setXYZ(2,e.r,e.g,e.b),a.setXYZ(3,e.r,e.g,e.b),a.setXYZ(4,e.r,e.g,e.b),a.setXYZ(5,e.r,e.g,e.b),a.setXYZ(6,e.r,e.g,e.b),a.setXYZ(7,e.r,e.g,e.b),a.setXYZ(8,e.r,e.g,e.b),a.setXYZ(9,e.r,e.g,e.b),a.setXYZ(10,e.r,e.g,e.b),a.setXYZ(11,e.r,e.g,e.b),a.setXYZ(12,e.r,e.g,e.b),a.setXYZ(13,e.r,e.g,e.b),a.setXYZ(14,e.r,e.g,e.b),a.setXYZ(15,e.r,e.g,e.b),a.setXYZ(16,e.r,e.g,e.b),a.setXYZ(17,e.r,e.g,e.b),a.setXYZ(18,e.r,e.g,e.b),a.setXYZ(19,e.r,e.g,e.b),a.setXYZ(20,e.r,e.g,e.b),a.setXYZ(21,e.r,e.g,e.b),a.setXYZ(22,e.r,e.g,e.b),a.setXYZ(23,e.r,e.g,e.b),a.setXYZ(24,t.r,t.g,t.b),a.setXYZ(25,t.r,t.g,t.b),a.setXYZ(26,t.r,t.g,t.b),a.setXYZ(27,t.r,t.g,t.b),a.setXYZ(28,t.r,t.g,t.b),a.setXYZ(29,t.r,t.g,t.b),a.setXYZ(30,t.r,t.g,t.b),a.setXYZ(31,t.r,t.g,t.b),a.setXYZ(32,n.r,n.g,n.b),a.setXYZ(33,n.r,n.g,n.b),a.setXYZ(34,n.r,n.g,n.b),a.setXYZ(35,n.r,n.g,n.b),a.setXYZ(36,n.r,n.g,n.b),a.setXYZ(37,n.r,n.g,n.b),a.setXYZ(38,i.r,i.g,i.b),a.setXYZ(39,i.r,i.g,i.b),a.setXYZ(40,r.r,r.g,r.b),a.setXYZ(41,r.r,r.g,r.b),a.setXYZ(42,r.r,r.g,r.b),a.setXYZ(43,r.r,r.g,r.b),a.setXYZ(44,r.r,r.g,r.b),a.setXYZ(45,r.r,r.g,r.b),a.setXYZ(46,r.r,r.g,r.b),a.setXYZ(47,r.r,r.g,r.b),a.setXYZ(48,r.r,r.g,r.b),a.setXYZ(49,r.r,r.g,r.b),a.needsUpdate=!0}update(){const e=this.geometry,t=this.pointMap,n=1,i=1;Bt.projectionMatrixInverse.copy(this.camera.projectionMatrixInverse),Ht("c",t,e,Bt,0,0,-1),Ht("t",t,e,Bt,0,0,1),Ht("n1",t,e,Bt,-n,-i,-1),Ht("n2",t,e,Bt,n,-i,-1),Ht("n3",t,e,Bt,-n,i,-1),Ht("n4",t,e,Bt,n,i,-1),Ht("f1",t,e,Bt,-n,-i,1),Ht("f2",t,e,Bt,n,-i,1),Ht("f3",t,e,Bt,-n,i,1),Ht("f4",t,e,Bt,n,i,1),Ht("u1",t,e,Bt,n*.7,i*1.1,-1),Ht("u2",t,e,Bt,-n*.7,i*1.1,-1),Ht("u3",t,e,Bt,0,i*2,-1),Ht("cf1",t,e,Bt,-n,0,1),Ht("cf2",t,e,Bt,n,0,1),Ht("cf3",t,e,Bt,0,-i,1),Ht("cf4",t,e,Bt,0,i,1),Ht("cn1",t,e,Bt,-n,0,-1),Ht("cn2",t,e,Bt,n,0,-1),Ht("cn3",t,e,Bt,0,-i,-1),Ht("cn4",t,e,Bt,0,i,-1),e.getAttribute("position").needsUpdate=!0}dispose(){this.geometry.dispose(),this.material.dispose()}}function Ht(s,e,t,n,i,r,o){Sa.set(i,r,o).unproject(n);const a=e[s];if(a!==void 0){const l=t.getAttribute("position");for(let u=0,d=a.length;u<d;u++)l.setXYZ(a[u],Sa.x,Sa.y,Sa.z)}}const wa=new yn;class uS extends ri{constructor(e,t=16776960){const n=new Uint16Array([0,1,1,2,2,3,3,0,4,5,5,6,6,7,7,4,0,4,1,5,2,6,3,7]),i=new Float32Array(8*3),r=new at;r.setIndex(new Rt(n,1)),r.setAttribute("position",new Rt(i,3)),super(r,new un({color:t,toneMapped:!1})),this.object=e,this.type="BoxHelper",this.matrixAutoUpdate=!1,this.update()}update(e){if(e!==void 0&&console.warn("THREE.BoxHelper: .update() has no longer arguments."),this.object!==void 0&&wa.setFromObject(this.object),wa.isEmpty())return;const t=wa.min,n=wa.max,i=this.geometry.attributes.position,r=i.array;r[0]=n.x,r[1]=n.y,r[2]=n.z,r[3]=t.x,r[4]=n.y,r[5]=n.z,r[6]=t.x,r[7]=t.y,r[8]=n.z,r[9]=n.x,r[10]=t.y,r[11]=n.z,r[12]=n.x,r[13]=n.y,r[14]=t.z,r[15]=t.x,r[16]=n.y,r[17]=t.z,r[18]=t.x,r[19]=t.y,r[20]=t.z,r[21]=n.x,r[22]=t.y,r[23]=t.z,i.needsUpdate=!0,this.geometry.computeBoundingSphere()}setFromObject(e){return this.object=e,this.update(),this}copy(e,t){return super.copy(e,t),this.object=e.object,this}dispose(){this.geometry.dispose(),this.material.dispose()}}class dS extends ri{constructor(e,t=16776960){const n=new Uint16Array([0,1,1,2,2,3,3,0,4,5,5,6,6,7,7,4,0,4,1,5,2,6,3,7]),i=[1,1,1,-1,1,1,-1,-1,1,1,-1,1,1,1,-1,-1,1,-1,-1,-1,-1,1,-1,-1],r=new at;r.setIndex(new Rt(n,1)),r.setAttribute("position",new He(i,3)),super(r,new un({color:t,toneMapped:!1})),this.box=e,this.type="Box3Helper",this.geometry.computeBoundingSphere()}updateMatrixWorld(e){const t=this.box;t.isEmpty()||(t.getCenter(this.position),t.getSize(this.scale),this.scale.multiplyScalar(.5),super.updateMatrixWorld(e))}dispose(){this.geometry.dispose(),this.material.dispose()}}class fS extends ln{constructor(e,t=1,n=16776960){const i=n,r=[1,-1,0,-1,1,0,-1,-1,0,1,1,0,-1,1,0,-1,-1,0,1,-1,0,1,1,0],o=new at;o.setAttribute("position",new He(r,3)),o.computeBoundingSphere(),super(o,new un({color:i,toneMapped:!1})),this.type="PlaneHelper",this.plane=e,this.size=t;const a=[1,1,0,-1,1,0,-1,-1,0,1,1,0,-1,-1,0,1,-1,0],l=new at;l.setAttribute("position",new He(a,3)),l.computeBoundingSphere(),this.add(new Ie(l,new ii({color:i,opacity:.2,transparent:!0,depthWrite:!1,toneMapped:!1})))}updateMatrixWorld(e){this.position.set(0,0,0),this.scale.set(.5*this.size,.5*this.size,1),this.lookAt(this.plane.normal),this.translateZ(-this.plane.constant),super.updateMatrixWorld(e)}dispose(){this.geometry.dispose(),this.material.dispose(),this.children[0].geometry.dispose(),this.children[0].material.dispose()}}const Qd=new O;let Ea,dc;class pS extends xt{constructor(e=new O(0,0,1),t=new O(0,0,0),n=1,i=16776960,r=n*.2,o=r*.2){super(),this.type="ArrowHelper",Ea===void 0&&(Ea=new at,Ea.setAttribute("position",new He([0,0,0,0,1,0],3)),dc=new Wt(0,.5,1,5,1),dc.translate(0,-.5,0)),this.position.copy(t),this.line=new ln(Ea,new un({color:i,toneMapped:!1})),this.line.matrixAutoUpdate=!1,this.add(this.line),this.cone=new Ie(dc,new ii({color:i,toneMapped:!1})),this.cone.matrixAutoUpdate=!1,this.add(this.cone),this.setDirection(e),this.setLength(n,r,o)}setDirection(e){if(e.y>.99999)this.quaternion.set(0,0,0,1);else if(e.y<-.99999)this.quaternion.set(1,0,0,0);else{Qd.set(e.z,0,-e.x).normalize();const t=Math.acos(e.y);this.quaternion.setFromAxisAngle(Qd,t)}}setLength(e,t=e*.2,n=t*.2){this.line.scale.set(1,Math.max(1e-4,e-t),1),this.line.updateMatrix(),this.cone.scale.set(n,t,n),this.cone.position.y=e,this.cone.updateMatrix()}setColor(e){this.line.material.color.set(e),this.cone.material.color.set(e)}copy(e){return super.copy(e,!1),this.line.copy(e.line),this.cone.copy(e.cone),this}dispose(){this.line.geometry.dispose(),this.line.material.dispose(),this.cone.geometry.dispose(),this.cone.material.dispose()}}class mS extends ri{constructor(e=1){const t=[0,0,0,e,0,0,0,0,0,0,e,0,0,0,0,0,0,e],n=[1,0,0,1,.6,0,0,1,0,.6,1,0,0,0,1,0,.6,1],i=new at;i.setAttribute("position",new He(t,3)),i.setAttribute("color",new He(n,3));const r=new un({vertexColors:!0,toneMapped:!1});super(i,r),this.type="AxesHelper"}setColors(e,t,n){const i=new Oe,r=this.geometry.attributes.color.array;return i.set(e),i.toArray(r,0),i.toArray(r,3),i.set(t),i.toArray(r,6),i.toArray(r,9),i.set(n),i.toArray(r,12),i.toArray(r,15),this.geometry.attributes.color.needsUpdate=!0,this}dispose(){this.geometry.dispose(),this.material.dispose()}}class gS{constructor(){this.type="ShapePath",this.color=new Oe,this.subPaths=[],this.currentPath=null}moveTo(e,t){return this.currentPath=new lo,this.subPaths.push(this.currentPath),this.currentPath.moveTo(e,t),this}lineTo(e,t){return this.currentPath.lineTo(e,t),this}quadraticCurveTo(e,t,n,i){return this.currentPath.quadraticCurveTo(e,t,n,i),this}bezierCurveTo(e,t,n,i,r,o){return this.currentPath.bezierCurveTo(e,t,n,i,r,o),this}splineThru(e){return this.currentPath.splineThru(e),this}toShapes(e){function t(v){const w=[];for(let S=0,T=v.length;S<T;S++){const z=v[S],N=new Sr;N.curves=z.curves,w.push(N)}return w}function n(v,w){const S=w.length;let T=!1;for(let z=S-1,N=0;N<S;z=N++){let A=w[z],k=w[N],D=k.x-A.x,C=k.y-A.y;if(Math.abs(C)>Number.EPSILON){if(C<0&&(A=w[N],D=-D,k=w[z],C=-C),v.y<A.y||v.y>k.y)continue;if(v.y===A.y){if(v.x===A.x)return!0}else{const X=C*(v.x-A.x)-D*(v.y-A.y);if(X===0)return!0;if(X<0)continue;T=!T}}else{if(v.y!==A.y)continue;if(k.x<=v.x&&v.x<=A.x||A.x<=v.x&&v.x<=k.x)return!0}}return T}const i=ti.isClockWise,r=this.subPaths;if(r.length===0)return[];let o,a,l;const u=[];if(r.length===1)return a=r[0],l=new Sr,l.curves=a.curves,u.push(l),u;let d=!i(r[0].getPoints());d=e?!d:d;const m=[],p=[];let g=[],_=0,M;p[_]=void 0,g[_]=[];for(let v=0,w=r.length;v<w;v++)a=r[v],M=a.getPoints(),o=i(M),o=e?!o:o,o?(!d&&p[_]&&_++,p[_]={s:new Sr,p:M},p[_].s.curves=a.curves,d&&_++,g[_]=[]):g[_].push({h:a,p:M[0]});if(!p[0])return t(r);if(p.length>1){let v=!1,w=0;for(let S=0,T=p.length;S<T;S++)m[S]=[];for(let S=0,T=p.length;S<T;S++){const z=g[S];for(let N=0;N<z.length;N++){const A=z[N];let k=!0;for(let D=0;D<p.length;D++)n(A.p,p[D].p)&&(S!==D&&w++,k?(k=!1,m[D].push(A)):v=!0);k&&m[S].push(A)}}w>0&&v===!1&&(g=m)}let y;for(let v=0,w=p.length;v<w;v++){l=p[v].s,u.push(l),y=g[v];for(let S=0,T=y.length;S<T;S++)l.holes.push(y[S].h)}return u}}class vS extends qt{constructor(e=1,t=1,n=1,i={}){console.warn('THREE.WebGLMultipleRenderTargets has been deprecated and will be removed in r172. Use THREE.WebGLRenderTarget and set the "count" parameter to enable MRT.'),super(e,t,{...i,count:n}),this.isWebGLMultipleRenderTargets=!0}get texture(){return this.textures}}typeof __THREE_DEVTOOLS__<"u"&&__THREE_DEVTOOLS__.dispatchEvent(new CustomEvent("register",{detail:{revision:Xa}}));typeof window<"u"&&(window.__THREE__?console.warn("WARNING: Multiple instances of Three.js being imported."):window.__THREE__=Xa);const ef=Object.freeze(Object.defineProperty({__proto__:null,ACESFilmicToneMapping:ah,AddEquation:zi,AddOperation:Wf,AdditiveAnimationBlendMode:yh,AdditiveBlending:za,AgXToneMapping:lh,AlphaFormat:Kf,AlwaysCompare:fp,AlwaysDepth:Ff,AlwaysStencilFunc:Yc,AmbientLight:fm,AnimationAction:Mm,AnimationClip:mo,AnimationLoader:xb,AnimationMixer:jb,AnimationObjectGroup:qb,AnimationUtils:gb,ArcCurve:Vp,ArrayCamera:Ip,ArrowHelper:pS,AttachedBindMode:yc,Audio:ym,AudioAnalyser:Fb,AudioContext:Zh,AudioListener:Nb,AudioLoader:Ib,AxesHelper:mS,BackSide:_n,BasicDepthPacking:sp,BasicShadowMap:Cg,BatchedMesh:Bp,Bone:Ih,BooleanKeyframeTrack:Rr,Box2:tS,Box3:yn,Box3Helper:dS,BoxGeometry:zt,BoxHelper:uS,BufferAttribute:Rt,BufferGeometry:at,BufferGeometryLoader:_m,ByteType:qf,Cache:_i,Camera:Ja,CameraHelper:hS,CanvasTexture:kM,CapsuleGeometry:ol,CatmullRomCurve3:Hp,CineonToneMapping:oh,CircleGeometry:al,ClampToEdgeWrapping:Bn,Clock:jh,Color:Oe,ColorKeyframeTrack:Wh,ColorManagement:wt,CompressedArrayTexture:BM,CompressedCubeTexture:zM,CompressedTexture:rl,CompressedTextureLoader:Mb,ConeGeometry:ll,ConstantAlphaFactor:Nf,ConstantColorFactor:Lf,CubeCamera:bp,CubeReflectionMapping:Mi,CubeRefractionMapping:Hi,CubeTexture:xo,CubeTextureLoader:bb,CubeUVReflectionMapping:us,CubicBezierCurve:Nh,CubicBezierCurve3:Gp,CubicInterpolant:om,CullFaceBack:gc,CullFaceFront:gf,CullFaceFrontBack:Ag,CullFaceNone:mf,Curve:Yn,CurvePath:Xp,CustomBlending:_f,CustomToneMapping:Xf,CylinderGeometry:Wt,Cylindrical:eS,Data3DTexture:Sh,DataArrayTexture:ja,DataTexture:Vi,DataTextureLoader:Sb,DataUtils:Uv,DecrementStencilOp:zg,DecrementWrapStencilOp:Vg,DefaultLoadingManager:cm,DepthFormat:Mr,DepthStencilFormat:as,DepthTexture:Ah,DetachedBindMode:Yf,DirectionalLight:qh,DirectionalLightHelper:cS,DiscreteInterpolant:am,DisplayP3ColorSpace:Za,DodecahedronGeometry:cl,DoubleSide:Tn,DstAlphaFactor:Af,DstColorFactor:Rf,DynamicCopyUsage:nv,DynamicDrawUsage:Kg,DynamicReadUsage:Qg,EdgesGeometry:Yp,EllipseCurve:sl,EqualCompare:cp,EqualDepth:zf,EqualStencilFunc:Xg,EquirectangularReflectionMapping:js,EquirectangularRefractionMapping:Ks,Euler:xn,EventDispatcher:ni,ExtrudeGeometry:ul,FileLoader:Si,Float16BufferAttribute:Hv,Float32BufferAttribute:He,FloatType:zn,Fog:tl,FogExp2:el,FramebufferTexture:FM,FrontSide:xi,Frustum:Mo,GLBufferAttribute:Qb,GLSL1:rv,GLSL3:qc,GreaterCompare:hp,GreaterDepth:Vf,GreaterEqualCompare:dp,GreaterEqualDepth:kf,GreaterEqualStencilFunc:jg,GreaterStencilFunc:qg,GridHelper:Sm,Group:rs,HalfFloatType:Fn,HemisphereLight:hm,HemisphereLightHelper:aS,IcosahedronGeometry:dl,ImageBitmapLoader:Pb,ImageLoader:go,ImageUtils:_p,IncrementStencilOp:Bg,IncrementWrapStencilOp:kg,InstancedBufferAttribute:hs,InstancedBufferGeometry:vm,InstancedInterleavedBuffer:$b,InstancedMesh:Fp,Int16BufferAttribute:kv,Int32BufferAttribute:Vv,Int8BufferAttribute:Fv,IntType:dh,InterleavedBuffer:nl,InterleavedBufferAttribute:Er,Interpolant:wo,InterpolateDiscrete:Qs,InterpolateLinear:eo,InterpolateSmooth:Oa,InvertStencilOp:Hg,KeepStencilOp:dr,KeyframeTrack:qn,LOD:Op,LatheGeometry:So,Layers:Ka,LessCompare:lp,LessDepth:Bf,LessEqualCompare:Mh,LessEqualDepth:Zs,LessEqualStencilFunc:Yg,LessStencilFunc:Wg,Light:Xi,LightProbe:gm,Line:ln,Line3:nS,LineBasicMaterial:un,LineCurve:Oh,LineCurve3:Wp,LineDashedMaterial:im,LineLoop:zp,LineSegments:ri,LinearDisplayP3ColorSpace:_o,LinearFilter:Xt,LinearInterpolant:Gh,LinearMipMapLinearFilter:Lg,LinearMipMapNearestFilter:Ig,LinearMipmapLinearFilter:Qn,LinearMipmapNearestFilter:Ws,LinearSRGBColorSpace:wi,LinearToneMapping:rh,LinearTransfer:no,Loader:bn,LoaderUtils:eh,LoadingManager:Xh,LoopOnce:np,LoopPingPong:rp,LoopRepeat:ip,LuminanceAlphaFormat:Qf,LuminanceFormat:$f,MOUSE:hr,Material:hn,MaterialLoader:_l,MathUtils:pp,Matrix3:ut,Matrix4:Ke,MaxEquation:bf,Mesh:Ie,MeshBasicMaterial:ii,MeshDepthMaterial:Qa,MeshDistanceMaterial:Ch,MeshLambertMaterial:Vh,MeshMatcapMaterial:nm,MeshNormalMaterial:tm,MeshPhongMaterial:Qp,MeshPhysicalMaterial:$p,MeshStandardMaterial:kh,MeshToonMaterial:em,MinEquation:Mf,MirroredRepeatWrapping:$s,MixOperation:Gf,MultiplyBlending:_c,MultiplyOperation:vo,NearestFilter:nn,NearestMipMapLinearFilter:Pg,NearestMipMapNearestFilter:Rg,NearestMipmapLinearFilter:ts,NearestMipmapNearestFilter:hh,NeutralToneMapping:ch,NeverCompare:ap,NeverDepth:Uf,NeverStencilFunc:Gg,NoBlending:kn,NoColorSpace:pi,NoToneMapping:yi,NormalAnimationBlendMode:qa,NormalBlending:xr,NotEqualCompare:up,NotEqualDepth:Hf,NotEqualStencilFunc:Zg,NumberKeyframeTrack:fo,Object3D:xt,ObjectLoader:Cb,ObjectSpaceNormalMap:op,OctahedronGeometry:vi,OneFactor:wf,OneMinusConstantAlphaFactor:Of,OneMinusConstantColorFactor:Df,OneMinusDstAlphaFactor:Cf,OneMinusDstColorFactor:Pf,OneMinusSrcAlphaFactor:Va,OneMinusSrcColorFactor:Tf,OrthographicCamera:bo,P3Primaries:ro,PCFShadowMap:ih,PCFSoftShadowMap:vf,PMREMGenerator:Zc,Path:lo,PerspectiveCamera:en,Plane:$n,PlaneGeometry:Cr,PlaneHelper:fS,PointLight:dm,PointLightHelper:sS,Points:kp,PointsMaterial:Lh,PolarGridHelper:lS,PolyhedronGeometry:Wi,PositionalAudio:Ub,PropertyBinding:bt,PropertyMixer:xm,QuadraticBezierCurve:Uh,QuadraticBezierCurve3:Fh,Quaternion:Dt,QuaternionKeyframeTrack:ms,QuaternionLinearInterpolant:lm,RED_GREEN_RGTC2_Format:Wc,RED_RGTC1_Format:tp,REVISION:Xa,RGBADepthPacking:xh,RGBAFormat:Cn,RGBAIntegerFormat:_h,RGBA_ASTC_10x10_Format:Bc,RGBA_ASTC_10x5_Format:Oc,RGBA_ASTC_10x6_Format:Uc,RGBA_ASTC_10x8_Format:Fc,RGBA_ASTC_12x10_Format:zc,RGBA_ASTC_12x12_Format:kc,RGBA_ASTC_4x4_Format:Ac,RGBA_ASTC_5x4_Format:Cc,RGBA_ASTC_5x5_Format:Rc,RGBA_ASTC_6x5_Format:Pc,RGBA_ASTC_6x6_Format:Ic,RGBA_ASTC_8x5_Format:Lc,RGBA_ASTC_8x6_Format:Dc,RGBA_ASTC_8x8_Format:Nc,RGBA_BPTC_Format:Na,RGBA_ETC2_EAC_Format:Tc,RGBA_PVRTC_2BPPV1_Format:Sc,RGBA_PVRTC_4BPPV1_Format:bc,RGBA_S3TC_DXT1_Format:Ia,RGBA_S3TC_DXT3_Format:La,RGBA_S3TC_DXT5_Format:Da,RGBFormat:Jf,RGB_BPTC_SIGNED_Format:Vc,RGB_BPTC_UNSIGNED_Format:Hc,RGB_ETC1_Format:wc,RGB_ETC2_Format:Ec,RGB_PVRTC_2BPPV1_Format:Mc,RGB_PVRTC_4BPPV1_Format:xc,RGB_S3TC_DXT1_Format:Pa,RGFormat:ep,RGIntegerFormat:vh,RawShaderMaterial:zh,Ray:Ar,Raycaster:Qh,Rec709Primaries:io,RectAreaLight:pm,RedFormat:mh,RedIntegerFormat:gh,ReinhardToneMapping:sh,RenderTarget:yp,RepeatWrapping:Js,ReplaceStencilOp:Fg,ReverseSubtractEquation:xf,RingGeometry:fl,SIGNED_RED_GREEN_RGTC2_Format:Xc,SIGNED_RED_RGTC1_Format:Gc,SRGBColorSpace:Un,SRGBTransfer:It,Scene:Rh,ShaderChunk:mt,ShaderLib:Xn,ShaderMaterial:tn,ShadowMaterial:Jp,Shape:Sr,ShapeGeometry:pl,ShapePath:gS,ShapeUtils:ti,ShortType:Zf,Skeleton:il,SkeletonHelper:rS,SkinnedMesh:Up,Source:_r,Sphere:cn,SphereGeometry:ps,Spherical:nh,SphericalHarmonics3:mm,SplineCurve:Bh,SpotLight:um,SpotLightHelper:iS,Sprite:Np,SpriteMaterial:Ph,SrcAlphaFactor:ka,SrcAlphaSaturateFactor:If,SrcColorFactor:Ef,StaticCopyUsage:tv,StaticDrawUsage:so,StaticReadUsage:$g,StereoCamera:Lb,StreamCopyUsage:iv,StreamDrawUsage:Jg,StreamReadUsage:ev,StringKeyframeTrack:Pr,SubtractEquation:yf,SubtractiveBlending:vc,TOUCH:ur,TangentSpaceNormalMap:Gi,TetrahedronGeometry:ml,Texture:Vt,TextureLoader:wb,TorusGeometry:gi,TorusKnotGeometry:gl,Triangle:An,TriangleFanDrawMode:Og,TriangleStripDrawMode:Ng,TrianglesDrawMode:Dg,TubeGeometry:vl,UVMapping:Ya,Uint16BufferAttribute:wh,Uint32BufferAttribute:Eh,Uint8BufferAttribute:Bv,Uint8ClampedBufferAttribute:zv,Uniform:$h,UniformsGroup:Jb,UniformsLib:Ne,UniformsUtils:yo,UnsignedByteType:bi,UnsignedInt248Type:ds,UnsignedInt5999Type:jf,UnsignedIntType:wr,UnsignedShort4444Type:fh,UnsignedShort5551Type:ph,UnsignedShortType:uh,VSMShadowMap:Jn,Vector2:se,Vector3:O,Vector4:Ct,VectorKeyframeTrack:po,VideoTexture:UM,WebGL3DRenderTarget:Tv,WebGLArrayRenderTarget:Ev,WebGLCoordinateSystem:ei,WebGLCubeRenderTarget:Sp,WebGLMultipleRenderTargets:vS,WebGLRenderTarget:qt,WebGLRenderer:Lp,WebGLUtils:Pp,WebGPUCoordinateSystem:oo,WireframeGeometry:Kp,WrapAroundEnding:to,ZeroCurvatureEnding:gr,ZeroFactor:Sf,ZeroSlopeEnding:vr,ZeroStencilOp:Ug,createCanvasElement:gp},Symbol.toStringTag,{value:"Module"})),wm={name:"CopyShader",uniforms:{tDiffuse:{value:null},opacity:{value:1}},vertexShader:`

		varying vec2 vUv;

		void main() {

			vUv = uv;
			gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

		}`,fragmentShader:`

		uniform float opacity;

		uniform sampler2D tDiffuse;

		varying vec2 vUv;

		void main() {

			vec4 texel = texture2D( tDiffuse, vUv );
			gl_FragColor = opacity * texel;


		}`};class gs{constructor(){this.isPass=!0,this.enabled=!0,this.needsSwap=!0,this.clear=!1,this.renderToScreen=!1}setSize(){}render(){console.error("THREE.Pass: .render() must be implemented in derived pass.")}dispose(){}}const _S=new bo(-1,1,1,-1,0,1);class yS extends at{constructor(){super(),this.setAttribute("position",new He([-1,3,0,-1,-1,0,3,-1,0],3)),this.setAttribute("uv",new He([0,2,0,0,2,0],2))}}const xS=new yS;class eu{constructor(e){this._mesh=new Ie(xS,e)}dispose(){this._mesh.geometry.dispose()}render(e){e.render(this._mesh,_S)}get material(){return this._mesh.material}set material(e){this._mesh.material=e}}class Em extends gs{constructor(e,t){super(),this.textureID=t!==void 0?t:"tDiffuse",e instanceof tn?(this.uniforms=e.uniforms,this.material=e):e&&(this.uniforms=yo.clone(e.uniforms),this.material=new tn({name:e.name!==void 0?e.name:"unspecified",defines:Object.assign({},e.defines),uniforms:this.uniforms,vertexShader:e.vertexShader,fragmentShader:e.fragmentShader})),this.fsQuad=new eu(this.material)}render(e,t,n){this.uniforms[this.textureID]&&(this.uniforms[this.textureID].value=n.texture),this.fsQuad.material=this.material,this.renderToScreen?(e.setRenderTarget(null),this.fsQuad.render(e)):(e.setRenderTarget(t),this.clear&&e.clear(e.autoClearColor,e.autoClearDepth,e.autoClearStencil),this.fsQuad.render(e))}dispose(){this.material.dispose(),this.fsQuad.dispose()}}class tf extends gs{constructor(e,t){super(),this.scene=e,this.camera=t,this.clear=!0,this.needsSwap=!1,this.inverse=!1}render(e,t,n){const i=e.getContext(),r=e.state;r.buffers.color.setMask(!1),r.buffers.depth.setMask(!1),r.buffers.color.setLocked(!0),r.buffers.depth.setLocked(!0);let o,a;this.inverse?(o=0,a=1):(o=1,a=0),r.buffers.stencil.setTest(!0),r.buffers.stencil.setOp(i.REPLACE,i.REPLACE,i.REPLACE),r.buffers.stencil.setFunc(i.ALWAYS,o,4294967295),r.buffers.stencil.setClear(a),r.buffers.stencil.setLocked(!0),e.setRenderTarget(n),this.clear&&e.clear(),e.render(this.scene,this.camera),e.setRenderTarget(t),this.clear&&e.clear(),e.render(this.scene,this.camera),r.buffers.color.setLocked(!1),r.buffers.depth.setLocked(!1),r.buffers.color.setMask(!0),r.buffers.depth.setMask(!0),r.buffers.stencil.setLocked(!1),r.buffers.stencil.setFunc(i.EQUAL,1,4294967295),r.buffers.stencil.setOp(i.KEEP,i.KEEP,i.KEEP),r.buffers.stencil.setLocked(!0)}}class MS extends gs{constructor(){super(),this.needsSwap=!1}render(e){e.state.buffers.stencil.setLocked(!1),e.state.buffers.stencil.setTest(!1)}}class bS{constructor(e,t){if(this.renderer=e,this._pixelRatio=e.getPixelRatio(),t===void 0){const n=e.getSize(new se);this._width=n.width,this._height=n.height,t=new qt(this._width*this._pixelRatio,this._height*this._pixelRatio,{type:Fn}),t.texture.name="EffectComposer.rt1"}else this._width=t.width,this._height=t.height;this.renderTarget1=t,this.renderTarget2=t.clone(),this.renderTarget2.texture.name="EffectComposer.rt2",this.writeBuffer=this.renderTarget1,this.readBuffer=this.renderTarget2,this.renderToScreen=!0,this.passes=[],this.copyPass=new Em(wm),this.copyPass.material.blending=kn,this.clock=new jh}swapBuffers(){const e=this.readBuffer;this.readBuffer=this.writeBuffer,this.writeBuffer=e}addPass(e){this.passes.push(e),e.setSize(this._width*this._pixelRatio,this._height*this._pixelRatio)}insertPass(e,t){this.passes.splice(t,0,e),e.setSize(this._width*this._pixelRatio,this._height*this._pixelRatio)}removePass(e){const t=this.passes.indexOf(e);t!==-1&&this.passes.splice(t,1)}isLastEnabledPass(e){for(let t=e+1;t<this.passes.length;t++)if(this.passes[t].enabled)return!1;return!0}render(e){e===void 0&&(e=this.clock.getDelta());const t=this.renderer.getRenderTarget();let n=!1;for(let i=0,r=this.passes.length;i<r;i++){const o=this.passes[i];if(o.enabled!==!1){if(o.renderToScreen=this.renderToScreen&&this.isLastEnabledPass(i),o.render(this.renderer,this.writeBuffer,this.readBuffer,e,n),o.needsSwap){if(n){const a=this.renderer.getContext(),l=this.renderer.state.buffers.stencil;l.setFunc(a.NOTEQUAL,1,4294967295),this.copyPass.render(this.renderer,this.writeBuffer,this.readBuffer,e),l.setFunc(a.EQUAL,1,4294967295)}this.swapBuffers()}tf!==void 0&&(o instanceof tf?n=!0:o instanceof MS&&(n=!1))}}this.renderer.setRenderTarget(t)}reset(e){if(e===void 0){const t=this.renderer.getSize(new se);this._pixelRatio=this.renderer.getPixelRatio(),this._width=t.width,this._height=t.height,e=this.renderTarget1.clone(),e.setSize(this._width*this._pixelRatio,this._height*this._pixelRatio)}this.renderTarget1.dispose(),this.renderTarget2.dispose(),this.renderTarget1=e,this.renderTarget2=e.clone(),this.writeBuffer=this.renderTarget1,this.readBuffer=this.renderTarget2}setSize(e,t){this._width=e,this._height=t;const n=this._width*this._pixelRatio,i=this._height*this._pixelRatio;this.renderTarget1.setSize(n,i),this.renderTarget2.setSize(n,i);for(let r=0;r<this.passes.length;r++)this.passes[r].setSize(n,i)}setPixelRatio(e){this._pixelRatio=e,this.setSize(this._width,this._height)}dispose(){this.renderTarget1.dispose(),this.renderTarget2.dispose(),this.copyPass.dispose()}}class SS extends gs{constructor(e,t,n=null,i=null,r=null){super(),this.scene=e,this.camera=t,this.overrideMaterial=n,this.clearColor=i,this.clearAlpha=r,this.clear=!0,this.clearDepth=!1,this.needsSwap=!1,this._oldClearColor=new Oe}render(e,t,n){const i=e.autoClear;e.autoClear=!1;let r,o;this.overrideMaterial!==null&&(o=this.scene.overrideMaterial,this.scene.overrideMaterial=this.overrideMaterial),this.clearColor!==null&&(e.getClearColor(this._oldClearColor),e.setClearColor(this.clearColor)),this.clearAlpha!==null&&(r=e.getClearAlpha(),e.setClearAlpha(this.clearAlpha)),this.clearDepth==!0&&e.clearDepth(),e.setRenderTarget(this.renderToScreen?null:n),this.clear===!0&&e.clear(e.autoClearColor,e.autoClearDepth,e.autoClearStencil),e.render(this.scene,this.camera),this.clearColor!==null&&e.setClearColor(this._oldClearColor),this.clearAlpha!==null&&e.setClearAlpha(r),this.overrideMaterial!==null&&(this.scene.overrideMaterial=o),e.autoClear=i}}class ki extends gs{constructor(e,t,n,i){super(),this.renderScene=t,this.renderCamera=n,this.selectedObjects=i!==void 0?i:[],this.visibleEdgeColor=new Oe(1,1,1),this.hiddenEdgeColor=new Oe(.1,.04,.02),this.edgeGlow=0,this.usePatternTexture=!1,this.edgeThickness=1,this.edgeStrength=3,this.downSampleRatio=2,this.pulsePeriod=0,this._visibilityCache=new Map,this.resolution=e!==void 0?new se(e.x,e.y):new se(256,256);const r=Math.round(this.resolution.x/this.downSampleRatio),o=Math.round(this.resolution.y/this.downSampleRatio);this.renderTargetMaskBuffer=new qt(this.resolution.x,this.resolution.y),this.renderTargetMaskBuffer.texture.name="OutlinePass.mask",this.renderTargetMaskBuffer.texture.generateMipmaps=!1,this.depthMaterial=new Qa,this.depthMaterial.side=Tn,this.depthMaterial.depthPacking=xh,this.depthMaterial.blending=kn,this.prepareMaskMaterial=this.getPrepareMaskMaterial(),this.prepareMaskMaterial.side=Tn,this.prepareMaskMaterial.fragmentShader=d(this.prepareMaskMaterial.fragmentShader,this.renderCamera),this.renderTargetDepthBuffer=new qt(this.resolution.x,this.resolution.y,{type:Fn}),this.renderTargetDepthBuffer.texture.name="OutlinePass.depth",this.renderTargetDepthBuffer.texture.generateMipmaps=!1,this.renderTargetMaskDownSampleBuffer=new qt(r,o,{type:Fn}),this.renderTargetMaskDownSampleBuffer.texture.name="OutlinePass.depthDownSample",this.renderTargetMaskDownSampleBuffer.texture.generateMipmaps=!1,this.renderTargetBlurBuffer1=new qt(r,o,{type:Fn}),this.renderTargetBlurBuffer1.texture.name="OutlinePass.blur1",this.renderTargetBlurBuffer1.texture.generateMipmaps=!1,this.renderTargetBlurBuffer2=new qt(Math.round(r/2),Math.round(o/2),{type:Fn}),this.renderTargetBlurBuffer2.texture.name="OutlinePass.blur2",this.renderTargetBlurBuffer2.texture.generateMipmaps=!1,this.edgeDetectionMaterial=this.getEdgeDetectionMaterial(),this.renderTargetEdgeBuffer1=new qt(r,o,{type:Fn}),this.renderTargetEdgeBuffer1.texture.name="OutlinePass.edge1",this.renderTargetEdgeBuffer1.texture.generateMipmaps=!1,this.renderTargetEdgeBuffer2=new qt(Math.round(r/2),Math.round(o/2),{type:Fn}),this.renderTargetEdgeBuffer2.texture.name="OutlinePass.edge2",this.renderTargetEdgeBuffer2.texture.generateMipmaps=!1;const a=4,l=4;this.separableBlurMaterial1=this.getSeperableBlurMaterial(a),this.separableBlurMaterial1.uniforms.texSize.value.set(r,o),this.separableBlurMaterial1.uniforms.kernelRadius.value=1,this.separableBlurMaterial2=this.getSeperableBlurMaterial(l),this.separableBlurMaterial2.uniforms.texSize.value.set(Math.round(r/2),Math.round(o/2)),this.separableBlurMaterial2.uniforms.kernelRadius.value=l,this.overlayMaterial=this.getOverlayMaterial();const u=wm;this.copyUniforms=yo.clone(u.uniforms),this.materialCopy=new tn({uniforms:this.copyUniforms,vertexShader:u.vertexShader,fragmentShader:u.fragmentShader,blending:kn,depthTest:!1,depthWrite:!1}),this.enabled=!0,this.needsSwap=!1,this._oldClearColor=new Oe,this.oldClearAlpha=1,this.fsQuad=new eu(null),this.tempPulseColor1=new Oe,this.tempPulseColor2=new Oe,this.textureMatrix=new Ke;function d(m,p){const g=p.isPerspectiveCamera?"perspective":"orthographic";return m.replace(/DEPTH_TO_VIEW_Z/g,g+"DepthToViewZ")}}dispose(){this.renderTargetMaskBuffer.dispose(),this.renderTargetDepthBuffer.dispose(),this.renderTargetMaskDownSampleBuffer.dispose(),this.renderTargetBlurBuffer1.dispose(),this.renderTargetBlurBuffer2.dispose(),this.renderTargetEdgeBuffer1.dispose(),this.renderTargetEdgeBuffer2.dispose(),this.depthMaterial.dispose(),this.prepareMaskMaterial.dispose(),this.edgeDetectionMaterial.dispose(),this.separableBlurMaterial1.dispose(),this.separableBlurMaterial2.dispose(),this.overlayMaterial.dispose(),this.materialCopy.dispose(),this.fsQuad.dispose()}setSize(e,t){this.renderTargetMaskBuffer.setSize(e,t),this.renderTargetDepthBuffer.setSize(e,t);let n=Math.round(e/this.downSampleRatio),i=Math.round(t/this.downSampleRatio);this.renderTargetMaskDownSampleBuffer.setSize(n,i),this.renderTargetBlurBuffer1.setSize(n,i),this.renderTargetEdgeBuffer1.setSize(n,i),this.separableBlurMaterial1.uniforms.texSize.value.set(n,i),n=Math.round(n/2),i=Math.round(i/2),this.renderTargetBlurBuffer2.setSize(n,i),this.renderTargetEdgeBuffer2.setSize(n,i),this.separableBlurMaterial2.uniforms.texSize.value.set(n,i)}changeVisibilityOfSelectedObjects(e){const t=this._visibilityCache;function n(i){i.isMesh&&(e===!0?i.visible=t.get(i):(t.set(i,i.visible),i.visible=e))}for(let i=0;i<this.selectedObjects.length;i++)this.selectedObjects[i].traverse(n)}changeVisibilityOfNonSelectedObjects(e){const t=this._visibilityCache,n=[];function i(o){o.isMesh&&n.push(o)}for(let o=0;o<this.selectedObjects.length;o++)this.selectedObjects[o].traverse(i);function r(o){if(o.isMesh||o.isSprite){let a=!1;for(let l=0;l<n.length;l++)if(n[l].id===o.id){a=!0;break}if(a===!1){const l=o.visible;(e===!1||t.get(o)===!0)&&(o.visible=e),t.set(o,l)}}else(o.isPoints||o.isLine)&&(e===!0?o.visible=t.get(o):(t.set(o,o.visible),o.visible=e))}this.renderScene.traverse(r)}updateTextureMatrix(){this.textureMatrix.set(.5,0,0,.5,0,.5,0,.5,0,0,.5,.5,0,0,0,1),this.textureMatrix.multiply(this.renderCamera.projectionMatrix),this.textureMatrix.multiply(this.renderCamera.matrixWorldInverse)}render(e,t,n,i,r){if(this.selectedObjects.length>0){e.getClearColor(this._oldClearColor),this.oldClearAlpha=e.getClearAlpha();const o=e.autoClear;e.autoClear=!1,r&&e.state.buffers.stencil.setTest(!1),e.setClearColor(16777215,1),this.changeVisibilityOfSelectedObjects(!1);const a=this.renderScene.background;if(this.renderScene.background=null,this.renderScene.overrideMaterial=this.depthMaterial,e.setRenderTarget(this.renderTargetDepthBuffer),e.clear(),e.render(this.renderScene,this.renderCamera),this.changeVisibilityOfSelectedObjects(!0),this._visibilityCache.clear(),this.updateTextureMatrix(),this.changeVisibilityOfNonSelectedObjects(!1),this.renderScene.overrideMaterial=this.prepareMaskMaterial,this.prepareMaskMaterial.uniforms.cameraNearFar.value.set(this.renderCamera.near,this.renderCamera.far),this.prepareMaskMaterial.uniforms.depthTexture.value=this.renderTargetDepthBuffer.texture,this.prepareMaskMaterial.uniforms.textureMatrix.value=this.textureMatrix,e.setRenderTarget(this.renderTargetMaskBuffer),e.clear(),e.render(this.renderScene,this.renderCamera),this.renderScene.overrideMaterial=null,this.changeVisibilityOfNonSelectedObjects(!0),this._visibilityCache.clear(),this.renderScene.background=a,this.fsQuad.material=this.materialCopy,this.copyUniforms.tDiffuse.value=this.renderTargetMaskBuffer.texture,e.setRenderTarget(this.renderTargetMaskDownSampleBuffer),e.clear(),this.fsQuad.render(e),this.tempPulseColor1.copy(this.visibleEdgeColor),this.tempPulseColor2.copy(this.hiddenEdgeColor),this.pulsePeriod>0){const l=.625+Math.cos(performance.now()*.01/this.pulsePeriod)*.75/2;this.tempPulseColor1.multiplyScalar(l),this.tempPulseColor2.multiplyScalar(l)}this.fsQuad.material=this.edgeDetectionMaterial,this.edgeDetectionMaterial.uniforms.maskTexture.value=this.renderTargetMaskDownSampleBuffer.texture,this.edgeDetectionMaterial.uniforms.texSize.value.set(this.renderTargetMaskDownSampleBuffer.width,this.renderTargetMaskDownSampleBuffer.height),this.edgeDetectionMaterial.uniforms.visibleEdgeColor.value=this.tempPulseColor1,this.edgeDetectionMaterial.uniforms.hiddenEdgeColor.value=this.tempPulseColor2,e.setRenderTarget(this.renderTargetEdgeBuffer1),e.clear(),this.fsQuad.render(e),this.fsQuad.material=this.separableBlurMaterial1,this.separableBlurMaterial1.uniforms.colorTexture.value=this.renderTargetEdgeBuffer1.texture,this.separableBlurMaterial1.uniforms.direction.value=ki.BlurDirectionX,this.separableBlurMaterial1.uniforms.kernelRadius.value=this.edgeThickness,e.setRenderTarget(this.renderTargetBlurBuffer1),e.clear(),this.fsQuad.render(e),this.separableBlurMaterial1.uniforms.colorTexture.value=this.renderTargetBlurBuffer1.texture,this.separableBlurMaterial1.uniforms.direction.value=ki.BlurDirectionY,e.setRenderTarget(this.renderTargetEdgeBuffer1),e.clear(),this.fsQuad.render(e),this.fsQuad.material=this.separableBlurMaterial2,this.separableBlurMaterial2.uniforms.colorTexture.value=this.renderTargetEdgeBuffer1.texture,this.separableBlurMaterial2.uniforms.direction.value=ki.BlurDirectionX,e.setRenderTarget(this.renderTargetBlurBuffer2),e.clear(),this.fsQuad.render(e),this.separableBlurMaterial2.uniforms.colorTexture.value=this.renderTargetBlurBuffer2.texture,this.separableBlurMaterial2.uniforms.direction.value=ki.BlurDirectionY,e.setRenderTarget(this.renderTargetEdgeBuffer2),e.clear(),this.fsQuad.render(e),this.fsQuad.material=this.overlayMaterial,this.overlayMaterial.uniforms.maskTexture.value=this.renderTargetMaskBuffer.texture,this.overlayMaterial.uniforms.edgeTexture1.value=this.renderTargetEdgeBuffer1.texture,this.overlayMaterial.uniforms.edgeTexture2.value=this.renderTargetEdgeBuffer2.texture,this.overlayMaterial.uniforms.patternTexture.value=this.patternTexture,this.overlayMaterial.uniforms.edgeStrength.value=this.edgeStrength,this.overlayMaterial.uniforms.edgeGlow.value=this.edgeGlow,this.overlayMaterial.uniforms.usePatternTexture.value=this.usePatternTexture,r&&e.state.buffers.stencil.setTest(!0),e.setRenderTarget(n),this.fsQuad.render(e),e.setClearColor(this._oldClearColor,this.oldClearAlpha),e.autoClear=o}this.renderToScreen&&(this.fsQuad.material=this.materialCopy,this.copyUniforms.tDiffuse.value=n.texture,e.setRenderTarget(null),this.fsQuad.render(e))}getPrepareMaskMaterial(){return new tn({uniforms:{depthTexture:{value:null},cameraNearFar:{value:new se(.5,.5)},textureMatrix:{value:null}},vertexShader:`#include <morphtarget_pars_vertex>
				#include <skinning_pars_vertex>

				varying vec4 projTexCoord;
				varying vec4 vPosition;
				uniform mat4 textureMatrix;

				void main() {

					#include <skinbase_vertex>
					#include <begin_vertex>
					#include <morphtarget_vertex>
					#include <skinning_vertex>
					#include <project_vertex>

					vPosition = mvPosition;

					vec4 worldPosition = vec4( transformed, 1.0 );

					#ifdef USE_INSTANCING

						worldPosition = instanceMatrix * worldPosition;

					#endif
					
					worldPosition = modelMatrix * worldPosition;

					projTexCoord = textureMatrix * worldPosition;

				}`,fragmentShader:`#include <packing>
				varying vec4 vPosition;
				varying vec4 projTexCoord;
				uniform sampler2D depthTexture;
				uniform vec2 cameraNearFar;

				void main() {

					float depth = unpackRGBAToDepth(texture2DProj( depthTexture, projTexCoord ));
					float viewZ = - DEPTH_TO_VIEW_Z( depth, cameraNearFar.x, cameraNearFar.y );
					float depthTest = (-vPosition.z > viewZ) ? 1.0 : 0.0;
					gl_FragColor = vec4(0.0, depthTest, 1.0, 1.0);

				}`})}getEdgeDetectionMaterial(){return new tn({uniforms:{maskTexture:{value:null},texSize:{value:new se(.5,.5)},visibleEdgeColor:{value:new O(1,1,1)},hiddenEdgeColor:{value:new O(1,1,1)}},vertexShader:`varying vec2 vUv;

				void main() {
					vUv = uv;
					gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
				}`,fragmentShader:`varying vec2 vUv;

				uniform sampler2D maskTexture;
				uniform vec2 texSize;
				uniform vec3 visibleEdgeColor;
				uniform vec3 hiddenEdgeColor;

				void main() {
					vec2 invSize = 1.0 / texSize;
					vec4 uvOffset = vec4(1.0, 0.0, 0.0, 1.0) * vec4(invSize, invSize);
					vec4 c1 = texture2D( maskTexture, vUv + uvOffset.xy);
					vec4 c2 = texture2D( maskTexture, vUv - uvOffset.xy);
					vec4 c3 = texture2D( maskTexture, vUv + uvOffset.yw);
					vec4 c4 = texture2D( maskTexture, vUv - uvOffset.yw);
					float diff1 = (c1.r - c2.r)*0.5;
					float diff2 = (c3.r - c4.r)*0.5;
					float d = length( vec2(diff1, diff2) );
					float a1 = min(c1.g, c2.g);
					float a2 = min(c3.g, c4.g);
					float visibilityFactor = min(a1, a2);
					vec3 edgeColor = 1.0 - visibilityFactor > 0.001 ? visibleEdgeColor : hiddenEdgeColor;
					gl_FragColor = vec4(edgeColor, 1.0) * vec4(d);
				}`})}getSeperableBlurMaterial(e){return new tn({defines:{MAX_RADIUS:e},uniforms:{colorTexture:{value:null},texSize:{value:new se(.5,.5)},direction:{value:new se(.5,.5)},kernelRadius:{value:1}},vertexShader:`varying vec2 vUv;

				void main() {
					vUv = uv;
					gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
				}`,fragmentShader:`#include <common>
				varying vec2 vUv;
				uniform sampler2D colorTexture;
				uniform vec2 texSize;
				uniform vec2 direction;
				uniform float kernelRadius;

				float gaussianPdf(in float x, in float sigma) {
					return 0.39894 * exp( -0.5 * x * x/( sigma * sigma))/sigma;
				}

				void main() {
					vec2 invSize = 1.0 / texSize;
					float sigma = kernelRadius/2.0;
					float weightSum = gaussianPdf(0.0, sigma);
					vec4 diffuseSum = texture2D( colorTexture, vUv) * weightSum;
					vec2 delta = direction * invSize * kernelRadius/float(MAX_RADIUS);
					vec2 uvOffset = delta;
					for( int i = 1; i <= MAX_RADIUS; i ++ ) {
						float x = kernelRadius * float(i) / float(MAX_RADIUS);
						float w = gaussianPdf(x, sigma);
						vec4 sample1 = texture2D( colorTexture, vUv + uvOffset);
						vec4 sample2 = texture2D( colorTexture, vUv - uvOffset);
						diffuseSum += ((sample1 + sample2) * w);
						weightSum += (2.0 * w);
						uvOffset += delta;
					}
					gl_FragColor = diffuseSum/weightSum;
				}`})}getOverlayMaterial(){return new tn({uniforms:{maskTexture:{value:null},edgeTexture1:{value:null},edgeTexture2:{value:null},patternTexture:{value:null},edgeStrength:{value:1},edgeGlow:{value:1},usePatternTexture:{value:0}},vertexShader:`varying vec2 vUv;

				void main() {
					vUv = uv;
					gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
				}`,fragmentShader:`varying vec2 vUv;

				uniform sampler2D maskTexture;
				uniform sampler2D edgeTexture1;
				uniform sampler2D edgeTexture2;
				uniform sampler2D patternTexture;
				uniform float edgeStrength;
				uniform float edgeGlow;
				uniform bool usePatternTexture;

				void main() {
					vec4 edgeValue1 = texture2D(edgeTexture1, vUv);
					vec4 edgeValue2 = texture2D(edgeTexture2, vUv);
					vec4 maskColor = texture2D(maskTexture, vUv);
					vec4 patternColor = texture2D(patternTexture, 6.0 * vUv);
					float visibilityFactor = 1.0 - maskColor.g > 0.0 ? 1.0 : 0.5;
					vec4 edgeValue = edgeValue1 + edgeValue2 * edgeGlow;
					vec4 finalColor = edgeStrength * maskColor.r * edgeValue;
					if(usePatternTexture)
						finalColor += + visibilityFactor * (1.0 - maskColor.r) * (1.0 - patternColor.r);
					gl_FragColor = finalColor;
				}`,blending:za,depthTest:!1,depthWrite:!1,transparent:!0})}}ki.BlurDirectionX=new se(1,0);ki.BlurDirectionY=new se(0,1);const wS={name:"OutputShader",uniforms:{tDiffuse:{value:null},toneMappingExposure:{value:1}},vertexShader:`
		precision highp float;

		uniform mat4 modelViewMatrix;
		uniform mat4 projectionMatrix;

		attribute vec3 position;
		attribute vec2 uv;

		varying vec2 vUv;

		void main() {

			vUv = uv;
			gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

		}`,fragmentShader:`
	
		precision highp float;

		uniform sampler2D tDiffuse;

		#include <tonemapping_pars_fragment>
		#include <colorspace_pars_fragment>

		varying vec2 vUv;

		void main() {

			gl_FragColor = texture2D( tDiffuse, vUv );

			// tone mapping

			#ifdef LINEAR_TONE_MAPPING

				gl_FragColor.rgb = LinearToneMapping( gl_FragColor.rgb );

			#elif defined( REINHARD_TONE_MAPPING )

				gl_FragColor.rgb = ReinhardToneMapping( gl_FragColor.rgb );

			#elif defined( CINEON_TONE_MAPPING )

				gl_FragColor.rgb = OptimizedCineonToneMapping( gl_FragColor.rgb );

			#elif defined( ACES_FILMIC_TONE_MAPPING )

				gl_FragColor.rgb = ACESFilmicToneMapping( gl_FragColor.rgb );

			#elif defined( AGX_TONE_MAPPING )

				gl_FragColor.rgb = AgXToneMapping( gl_FragColor.rgb );

			#elif defined( NEUTRAL_TONE_MAPPING )

				gl_FragColor.rgb = NeutralToneMapping( gl_FragColor.rgb );

			#endif

			// color space

			#ifdef SRGB_TRANSFER

				gl_FragColor = sRGBTransferOETF( gl_FragColor );

			#endif

		}`};class ES extends gs{constructor(){super();const e=wS;this.uniforms=yo.clone(e.uniforms),this.material=new zh({name:e.name,uniforms:this.uniforms,vertexShader:e.vertexShader,fragmentShader:e.fragmentShader}),this.fsQuad=new eu(this.material),this._outputColorSpace=null,this._toneMapping=null}render(e,t,n){this.uniforms.tDiffuse.value=n.texture,this.uniforms.toneMappingExposure.value=e.toneMappingExposure,(this._outputColorSpace!==e.outputColorSpace||this._toneMapping!==e.toneMapping)&&(this._outputColorSpace=e.outputColorSpace,this._toneMapping=e.toneMapping,this.material.defines={},wt.getTransfer(this._outputColorSpace)===It&&(this.material.defines.SRGB_TRANSFER=""),this._toneMapping===rh?this.material.defines.LINEAR_TONE_MAPPING="":this._toneMapping===sh?this.material.defines.REINHARD_TONE_MAPPING="":this._toneMapping===oh?this.material.defines.CINEON_TONE_MAPPING="":this._toneMapping===ah?this.material.defines.ACES_FILMIC_TONE_MAPPING="":this._toneMapping===lh?this.material.defines.AGX_TONE_MAPPING="":this._toneMapping===ch&&(this.material.defines.NEUTRAL_TONE_MAPPING=""),this.material.needsUpdate=!0),this.renderToScreen===!0?(e.setRenderTarget(null),this.fsQuad.render(e)):(e.setRenderTarget(t),this.clear&&e.clear(e.autoClearColor,e.autoClearDepth,e.autoClearStencil),this.fsQuad.render(e))}dispose(){this.material.dispose(),this.fsQuad.dispose()}}const TS={name:"FXAAShader",uniforms:{tDiffuse:{value:null},resolution:{value:new se(1/1024,1/512)}},vertexShader:`

		varying vec2 vUv;

		void main() {

			vUv = uv;
			gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

		}`,fragmentShader:`
		precision highp float;

		uniform sampler2D tDiffuse;

		uniform vec2 resolution;

		varying vec2 vUv;

		// FXAA 3.11 implementation by NVIDIA, ported to WebGL by Agost Biro (biro@archilogic.com)

		//----------------------------------------------------------------------------------
		// File:        es3-keplerFXAAassetsshaders/FXAA_DefaultES.frag
		// SDK Version: v3.00
		// Email:       gameworks@nvidia.com
		// Site:        http://developer.nvidia.com/
		//
		// Copyright (c) 2014-2015, NVIDIA CORPORATION. All rights reserved.
		//
		// Redistribution and use in source and binary forms, with or without
		// modification, are permitted provided that the following conditions
		// are met:
		//  * Redistributions of source code must retain the above copyright
		//    notice, this list of conditions and the following disclaimer.
		//  * Redistributions in binary form must reproduce the above copyright
		//    notice, this list of conditions and the following disclaimer in the
		//    documentation and/or other materials provided with the distribution.
		//  * Neither the name of NVIDIA CORPORATION nor the names of its
		//    contributors may be used to endorse or promote products derived
		//    from this software without specific prior written permission.
		//
		// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ''AS IS'' AND ANY
		// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
		// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
		// PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
		// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
		// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
		// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
		// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
		// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
		// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
		// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
		//
		//----------------------------------------------------------------------------------

		#ifndef FXAA_DISCARD
			//
			// Only valid for PC OpenGL currently.
			// Probably will not work when FXAA_GREEN_AS_LUMA = 1.
			//
			// 1 = Use discard on pixels which don't need AA.
			//     For APIs which enable concurrent TEX+ROP from same surface.
			// 0 = Return unchanged color on pixels which don't need AA.
			//
			#define FXAA_DISCARD 0
		#endif

		/*--------------------------------------------------------------------------*/
		#define FxaaTexTop(t, p) texture2D(t, p, -100.0)
		#define FxaaTexOff(t, p, o, r) texture2D(t, p + (o * r), -100.0)
		/*--------------------------------------------------------------------------*/

		#define NUM_SAMPLES 5

		// assumes colors have premultipliedAlpha, so that the calculated color contrast is scaled by alpha
		float contrast( vec4 a, vec4 b ) {
			vec4 diff = abs( a - b );
			return max( max( max( diff.r, diff.g ), diff.b ), diff.a );
		}

		/*============================================================================

									FXAA3 QUALITY - PC

		============================================================================*/

		/*--------------------------------------------------------------------------*/
		vec4 FxaaPixelShader(
			vec2 posM,
			sampler2D tex,
			vec2 fxaaQualityRcpFrame,
			float fxaaQualityEdgeThreshold,
			float fxaaQualityinvEdgeThreshold
		) {
			vec4 rgbaM = FxaaTexTop(tex, posM);
			vec4 rgbaS = FxaaTexOff(tex, posM, vec2( 0.0, 1.0), fxaaQualityRcpFrame.xy);
			vec4 rgbaE = FxaaTexOff(tex, posM, vec2( 1.0, 0.0), fxaaQualityRcpFrame.xy);
			vec4 rgbaN = FxaaTexOff(tex, posM, vec2( 0.0,-1.0), fxaaQualityRcpFrame.xy);
			vec4 rgbaW = FxaaTexOff(tex, posM, vec2(-1.0, 0.0), fxaaQualityRcpFrame.xy);
			// . S .
			// W M E
			// . N .

			bool earlyExit = max( max( max(
					contrast( rgbaM, rgbaN ),
					contrast( rgbaM, rgbaS ) ),
					contrast( rgbaM, rgbaE ) ),
					contrast( rgbaM, rgbaW ) )
					< fxaaQualityEdgeThreshold;
			// . 0 .
			// 0 0 0
			// . 0 .

			#if (FXAA_DISCARD == 1)
				if(earlyExit) FxaaDiscard;
			#else
				if(earlyExit) return rgbaM;
			#endif

			float contrastN = contrast( rgbaM, rgbaN );
			float contrastS = contrast( rgbaM, rgbaS );
			float contrastE = contrast( rgbaM, rgbaE );
			float contrastW = contrast( rgbaM, rgbaW );

			float relativeVContrast = ( contrastN + contrastS ) - ( contrastE + contrastW );
			relativeVContrast *= fxaaQualityinvEdgeThreshold;

			bool horzSpan = relativeVContrast > 0.;
			// . 1 .
			// 0 0 0
			// . 1 .

			// 45 deg edge detection and corners of objects, aka V/H contrast is too similar
			if( abs( relativeVContrast ) < .3 ) {
				// locate the edge
				vec2 dirToEdge;
				dirToEdge.x = contrastE > contrastW ? 1. : -1.;
				dirToEdge.y = contrastS > contrastN ? 1. : -1.;
				// . 2 .      . 1 .
				// 1 0 2  ~=  0 0 1
				// . 1 .      . 0 .

				// tap 2 pixels and see which ones are "outside" the edge, to
				// determine if the edge is vertical or horizontal

				vec4 rgbaAlongH = FxaaTexOff(tex, posM, vec2( dirToEdge.x, -dirToEdge.y ), fxaaQualityRcpFrame.xy);
				float matchAlongH = contrast( rgbaM, rgbaAlongH );
				// . 1 .
				// 0 0 1
				// . 0 H

				vec4 rgbaAlongV = FxaaTexOff(tex, posM, vec2( -dirToEdge.x, dirToEdge.y ), fxaaQualityRcpFrame.xy);
				float matchAlongV = contrast( rgbaM, rgbaAlongV );
				// V 1 .
				// 0 0 1
				// . 0 .

				relativeVContrast = matchAlongV - matchAlongH;
				relativeVContrast *= fxaaQualityinvEdgeThreshold;

				if( abs( relativeVContrast ) < .3 ) { // 45 deg edge
					// 1 1 .
					// 0 0 1
					// . 0 1

					// do a simple blur
					return mix(
						rgbaM,
						(rgbaN + rgbaS + rgbaE + rgbaW) * .25,
						.4
					);
				}

				horzSpan = relativeVContrast > 0.;
			}

			if(!horzSpan) rgbaN = rgbaW;
			if(!horzSpan) rgbaS = rgbaE;
			// . 0 .      1
			// 1 0 1  ->  0
			// . 0 .      1

			bool pairN = contrast( rgbaM, rgbaN ) > contrast( rgbaM, rgbaS );
			if(!pairN) rgbaN = rgbaS;

			vec2 offNP;
			offNP.x = (!horzSpan) ? 0.0 : fxaaQualityRcpFrame.x;
			offNP.y = ( horzSpan) ? 0.0 : fxaaQualityRcpFrame.y;

			bool doneN = false;
			bool doneP = false;

			float nDist = 0.;
			float pDist = 0.;

			vec2 posN = posM;
			vec2 posP = posM;

			int iterationsUsed = 0;
			int iterationsUsedN = 0;
			int iterationsUsedP = 0;
			for( int i = 0; i < NUM_SAMPLES; i++ ) {
				iterationsUsed = i;

				float increment = float(i + 1);

				if(!doneN) {
					nDist += increment;
					posN = posM + offNP * nDist;
					vec4 rgbaEndN = FxaaTexTop(tex, posN.xy);
					doneN = contrast( rgbaEndN, rgbaM ) > contrast( rgbaEndN, rgbaN );
					iterationsUsedN = i;
				}

				if(!doneP) {
					pDist += increment;
					posP = posM - offNP * pDist;
					vec4 rgbaEndP = FxaaTexTop(tex, posP.xy);
					doneP = contrast( rgbaEndP, rgbaM ) > contrast( rgbaEndP, rgbaN );
					iterationsUsedP = i;
				}

				if(doneN || doneP) break;
			}


			if ( !doneP && !doneN ) return rgbaM; // failed to find end of edge

			float dist = min(
				doneN ? float( iterationsUsedN ) / float( NUM_SAMPLES - 1 ) : 1.,
				doneP ? float( iterationsUsedP ) / float( NUM_SAMPLES - 1 ) : 1.
			);

			// hacky way of reduces blurriness of mostly diagonal edges
			// but reduces AA quality
			dist = pow(dist, .5);

			dist = 1. - dist;

			return mix(
				rgbaM,
				rgbaN,
				dist * .5
			);
		}

		void main() {
			const float edgeDetectionQuality = .2;
			const float invEdgeDetectionQuality = 1. / edgeDetectionQuality;

			gl_FragColor = FxaaPixelShader(
				vUv,
				tDiffuse,
				resolution,
				edgeDetectionQuality, // [0,1] contrast needed, otherwise early discard
				invEdgeDetectionQuality
			);

		}
	`};function AS(s,e,t){let n=null,i=null,r=null;(function(){n=new bS(s);const l=new SS(e,t);n.addPass(l),i=new ki(new se(window.innerWidth,window.innerHeight),e,t),i.edgeStrength=1,i.edgeGlow=4,i.edgeThickness=1,n.addPass(i),r=new Em(TS),r.uniforms.resolution.value.set(1/window.innerWidth,1/window.innerHeight),n.addPass(r);const u=new ES;n.addPass(u),addEventListener("objectSelected",o,!1)})();function o(a){if(a.detail.mesh){if(i.selectedObjects&&i.selectedObjects[0]!==a.detail.mesh){let l=a.detail.mesh;i.selectedObjects=[l]}}else i.selectedObjects=[]}return{resize(a,l){n.setSize(a,l),r.uniforms.resolution.value.set(1/a,1/l)},render(){n&&n.render()}}}function CS(){let s,e,t,n=[],i=[],r,o={position:new O,rotation:new xn,scale:new O,materialColor:new Oe};addEventListener("sceneChanged",M,!1),addEventListener("objectSelected",a,!1),addEventListener("objectChanged",l,!1);function a(y){const v=y.detail.mesh;v==null?o.active=!1:(o.position.copy(v.position),o.scale.copy(v.scale),o.rotation.copy(v.rotation),o.materialColor.copy(v.material.color),o.active=!0);const w=y.detail.mesh?y.detail.mesh.myId:null;w?localStorage.setItem("selectedId",JSON.stringify(w)):localStorage.removeItem("selectedId")}function l(y){const v=y.detail.mesh,w={myId:v.myId,id:v.id};v.position.equals(o.position)||v.position.manhattanDistanceTo(o.position)>.001&&(w.type="position",w.position={oldValue:o.position.clone(),newValue:v.position.clone()},o.position.copy(v.position)),v.rotation.equals(o.rotation)||(w.type="rotation",w.rotation={oldValue:o.rotation.clone(),newValue:v.rotation.clone()},o.rotation.copy(v.rotation)),v.scale.equals(o.scale)||(w.type="scale",w.scale={oldValue:o.scale.clone(),newValue:v.scale.clone()},o.scale.copy(v.scale)),v.material.color.equals(o.materialColor)||(w.type="materialColor",w.materialColor={oldValue:o.materialColor.clone(),newValue:v.material.color.clone()},o.materialColor.copy(v.material.color));const S=new CustomEvent("sceneChanged",{detail:{changes:w}});dispatchEvent(S)}function u(y){s=new en(70,window.innerWidth/window.innerHeight,.01,100),s.position.set(0,2,2.1)}function d(y){if(e=new Rh,y.materials)for(let[S,T]of Object.entries(y.materials)){let z=T.type,N=new ef[z];N.color=new Oe().fromArray(T.color),N.myId=T.myId,i[N.myId]=N}else{let S=new Vh({color:34816});S.myId=0,i.push(S)}if(y.meshes)for(let[S,T]of Object.entries(y.meshes)){let z=T.type,N=new ef[z](...T.parameters),A=new Ie(N,i[T.materialId]);T.position&&A.position.fromArray(T.position),T.rotation&&A.rotation.set(...T.rotation),T.scale&&A.scale.fromArray(T.scale),A.myId=T.myId,n.push(A),e.add(A)}y.camera;const v=new Sm(100,100);e.add(v);const w=new qh(16777215,3);w.position.set(-1,2,4),e.add(w)}function m(y){t=new Lp({antialias:!0}),t.setClearColor(64,1),t.setSize(window.innerWidth,window.innerHeight),t.setAnimationLoop(g),t.domElement.id="renderCanvas",document.body.appendChild(t.domElement)}function p(y){document.getElementById("1").style.display="none",u(),d(y),m(),r=AS(t,e,s)}function g(){r.render()}function _(){s.aspect=window.innerWidth/window.innerHeight,s.updateProjectionMatrix(),t.setSize(window.innerWidth,window.innerHeight),r.resize(window.innerWidth,window.innerHeight)}function M(){let y=[],v=[];n.length+1;let w=[],S=0;for(let[z,N]of Object.entries(n)){let A={};A.myId=N.myId,A.type=N.geometry.type,A.position=N.position.toArray().map(X=>Math.round(X*1e3)/1e3),A.rotation=N.rotation.toArray().map(X=>isNaN(X)?X:Math.round(X*1e3)/1e3),A.scale=N.scale.toArray().map(X=>Math.round(X*1e3)/1e3);const k=Object.values(N.geometry.parameters);A.parameters=k;let D={};D.type=N.material.type,D.color=N.material.color.toArray();let C=N.material.uuid;typeof w[C]>"u"&&(w[C]=S),A.materialId=S,D.myId=S++,y.push(A),v.push(D)}let T={};T.meshes=y,T.materials=v,localStorage.setItem("scene",JSON.stringify(T))}return{get properties(){return[s,t,e,n]},get camera(){return s},get renderer(){return t},get scene(){return e},get meshes(){return n},setup:p,onWindowResize:_}}const Vs=CS(),cr=new Qh,an=new O,Bi=new O,Ot=new Dt,nf={X:new O(1,0,0),Y:new O(0,1,0),Z:new O(0,0,1)},fc={type:"change"},rf={type:"mouseDown"},sf={type:"mouseUp",mode:null},of={type:"objectChange"};class RS extends xt{constructor(e,t){super(),t===void 0&&(console.warn('THREE.TransformControls: The second parameter "domElement" is now mandatory.'),t=document),this.isTransformControls=!0,this.visible=!1,this.domElement=t,this.domElement.style.touchAction="none";const n=new OS;this._gizmo=n,this.add(n);const i=new US;this._plane=i,this.add(i);const r=this;function o(w,S){let T=S;Object.defineProperty(r,w,{get:function(){return T!==void 0?T:S},set:function(z){T!==z&&(T=z,i[w]=z,n[w]=z,r.dispatchEvent({type:w+"-changed",value:z}),r.dispatchEvent(fc))}}),r[w]=S,i[w]=S,n[w]=S}o("camera",e),o("object",void 0),o("enabled",!0),o("axis",null),o("mode","translate"),o("translationSnap",null),o("rotationSnap",null),o("scaleSnap",null),o("space","world"),o("size",1),o("dragging",!1),o("showX",!0),o("showY",!0),o("showZ",!0);const a=new O,l=new O,u=new Dt,d=new Dt,m=new O,p=new Dt,g=new O,_=new O,M=new O,y=0,v=new O;o("worldPosition",a),o("worldPositionStart",l),o("worldQuaternion",u),o("worldQuaternionStart",d),o("cameraPosition",m),o("cameraQuaternion",p),o("pointStart",g),o("pointEnd",_),o("rotationAxis",M),o("rotationAngle",y),o("eye",v),this._offset=new O,this._startNorm=new O,this._endNorm=new O,this._cameraScale=new O,this._parentPosition=new O,this._parentQuaternion=new Dt,this._parentQuaternionInv=new Dt,this._parentScale=new O,this._worldScaleStart=new O,this._worldQuaternionInv=new Dt,this._worldScale=new O,this._positionStart=new O,this._quaternionStart=new Dt,this._scaleStart=new O,this._getPointer=PS.bind(this),this._onPointerDown=LS.bind(this),this._onPointerHover=IS.bind(this),this._onPointerMove=DS.bind(this),this._onPointerUp=NS.bind(this),this.domElement.addEventListener("pointerdown",this._onPointerDown),this.domElement.addEventListener("pointermove",this._onPointerHover),this.domElement.addEventListener("pointerup",this._onPointerUp)}updateMatrixWorld(e){this.object!==void 0&&(this.object.updateMatrixWorld(),this.object.parent===null?console.error("TransformControls: The attached 3D object must be a part of the scene graph."):this.object.parent.matrixWorld.decompose(this._parentPosition,this._parentQuaternion,this._parentScale),this.object.matrixWorld.decompose(this.worldPosition,this.worldQuaternion,this._worldScale),this._parentQuaternionInv.copy(this._parentQuaternion).invert(),this._worldQuaternionInv.copy(this.worldQuaternion).invert()),this.camera.updateMatrixWorld(),this.camera.matrixWorld.decompose(this.cameraPosition,this.cameraQuaternion,this._cameraScale),this.camera.isOrthographicCamera?this.camera.getWorldDirection(this.eye).negate():this.eye.copy(this.cameraPosition).sub(this.worldPosition).normalize(),super.updateMatrixWorld(e)}pointerHover(e){if(this.object===void 0||this.dragging===!0)return;e!==null&&cr.setFromCamera(e,this.camera);const t=pc(this._gizmo.picker[this.mode],cr);t?this.axis=t.object.name:this.axis=null}pointerDown(e){if(!(this.object===void 0||this.dragging===!0||e!=null&&e.button!==0)&&this.axis!==null){e!==null&&cr.setFromCamera(e,this.camera);const t=pc(this._plane,cr,!0);t&&(this.object.updateMatrixWorld(),this.object.parent.updateMatrixWorld(),this._positionStart.copy(this.object.position),this._quaternionStart.copy(this.object.quaternion),this._scaleStart.copy(this.object.scale),this.object.matrixWorld.decompose(this.worldPositionStart,this.worldQuaternionStart,this._worldScaleStart),this.pointStart.copy(t.point).sub(this.worldPositionStart)),this.dragging=!0,rf.mode=this.mode,this.dispatchEvent(rf)}}pointerMove(e){const t=this.axis,n=this.mode,i=this.object;let r=this.space;if(n==="scale"?r="local":(t==="E"||t==="XYZE"||t==="XYZ")&&(r="world"),i===void 0||t===null||this.dragging===!1||e!==null&&e.button!==-1)return;e!==null&&cr.setFromCamera(e,this.camera);const o=pc(this._plane,cr,!0);if(o){if(this.pointEnd.copy(o.point).sub(this.worldPositionStart),n==="translate")this._offset.copy(this.pointEnd).sub(this.pointStart),r==="local"&&t!=="XYZ"&&this._offset.applyQuaternion(this._worldQuaternionInv),t.indexOf("X")===-1&&(this._offset.x=0),t.indexOf("Y")===-1&&(this._offset.y=0),t.indexOf("Z")===-1&&(this._offset.z=0),r==="local"&&t!=="XYZ"?this._offset.applyQuaternion(this._quaternionStart).divide(this._parentScale):this._offset.applyQuaternion(this._parentQuaternionInv).divide(this._parentScale),i.position.copy(this._offset).add(this._positionStart),this.translationSnap&&(r==="local"&&(i.position.applyQuaternion(Ot.copy(this._quaternionStart).invert()),t.search("X")!==-1&&(i.position.x=Math.round(i.position.x/this.translationSnap)*this.translationSnap),t.search("Y")!==-1&&(i.position.y=Math.round(i.position.y/this.translationSnap)*this.translationSnap),t.search("Z")!==-1&&(i.position.z=Math.round(i.position.z/this.translationSnap)*this.translationSnap),i.position.applyQuaternion(this._quaternionStart)),r==="world"&&(i.parent&&i.position.add(an.setFromMatrixPosition(i.parent.matrixWorld)),t.search("X")!==-1&&(i.position.x=Math.round(i.position.x/this.translationSnap)*this.translationSnap),t.search("Y")!==-1&&(i.position.y=Math.round(i.position.y/this.translationSnap)*this.translationSnap),t.search("Z")!==-1&&(i.position.z=Math.round(i.position.z/this.translationSnap)*this.translationSnap),i.parent&&i.position.sub(an.setFromMatrixPosition(i.parent.matrixWorld))));else if(n==="scale"){if(t.search("XYZ")!==-1){let a=this.pointEnd.length()/this.pointStart.length();this.pointEnd.dot(this.pointStart)<0&&(a*=-1),Bi.set(a,a,a)}else an.copy(this.pointStart),Bi.copy(this.pointEnd),an.applyQuaternion(this._worldQuaternionInv),Bi.applyQuaternion(this._worldQuaternionInv),Bi.divide(an),t.search("X")===-1&&(Bi.x=1),t.search("Y")===-1&&(Bi.y=1),t.search("Z")===-1&&(Bi.z=1);i.scale.copy(this._scaleStart).multiply(Bi),this.scaleSnap&&(t.search("X")!==-1&&(i.scale.x=Math.round(i.scale.x/this.scaleSnap)*this.scaleSnap||this.scaleSnap),t.search("Y")!==-1&&(i.scale.y=Math.round(i.scale.y/this.scaleSnap)*this.scaleSnap||this.scaleSnap),t.search("Z")!==-1&&(i.scale.z=Math.round(i.scale.z/this.scaleSnap)*this.scaleSnap||this.scaleSnap))}else if(n==="rotate"){this._offset.copy(this.pointEnd).sub(this.pointStart);const a=20/this.worldPosition.distanceTo(an.setFromMatrixPosition(this.camera.matrixWorld));let l=!1;t==="XYZE"?(this.rotationAxis.copy(this._offset).cross(this.eye).normalize(),this.rotationAngle=this._offset.dot(an.copy(this.rotationAxis).cross(this.eye))*a):(t==="X"||t==="Y"||t==="Z")&&(this.rotationAxis.copy(nf[t]),an.copy(nf[t]),r==="local"&&an.applyQuaternion(this.worldQuaternion),an.cross(this.eye),an.length()===0?l=!0:this.rotationAngle=this._offset.dot(an.normalize())*a),(t==="E"||l)&&(this.rotationAxis.copy(this.eye),this.rotationAngle=this.pointEnd.angleTo(this.pointStart),this._startNorm.copy(this.pointStart).normalize(),this._endNorm.copy(this.pointEnd).normalize(),this.rotationAngle*=this._endNorm.cross(this._startNorm).dot(this.eye)<0?1:-1),this.rotationSnap&&(this.rotationAngle=Math.round(this.rotationAngle/this.rotationSnap)*this.rotationSnap),r==="local"&&t!=="E"&&t!=="XYZE"?(i.quaternion.copy(this._quaternionStart),i.quaternion.multiply(Ot.setFromAxisAngle(this.rotationAxis,this.rotationAngle)).normalize()):(this.rotationAxis.applyQuaternion(this._parentQuaternionInv),i.quaternion.copy(Ot.setFromAxisAngle(this.rotationAxis,this.rotationAngle)),i.quaternion.multiply(this._quaternionStart).normalize())}this.dispatchEvent(fc),this.dispatchEvent(of)}}pointerUp(e){e!==null&&e.button!==0||(this.dragging&&this.axis!==null&&(sf.mode=this.mode,this.dispatchEvent(sf)),this.dragging=!1,this.axis=null)}dispose(){this.domElement.removeEventListener("pointerdown",this._onPointerDown),this.domElement.removeEventListener("pointermove",this._onPointerHover),this.domElement.removeEventListener("pointermove",this._onPointerMove),this.domElement.removeEventListener("pointerup",this._onPointerUp),this.traverse(function(e){e.geometry&&e.geometry.dispose(),e.material&&e.material.dispose()})}attach(e){return this.object=e,this.visible=!0,this}detach(){return this.object=void 0,this.visible=!1,this.axis=null,this}reset(){this.enabled&&this.dragging&&(this.object.position.copy(this._positionStart),this.object.quaternion.copy(this._quaternionStart),this.object.scale.copy(this._scaleStart),this.dispatchEvent(fc),this.dispatchEvent(of),this.pointStart.copy(this.pointEnd))}getRaycaster(){return cr}getMode(){return this.mode}setMode(e){this.mode=e}setTranslationSnap(e){this.translationSnap=e}setRotationSnap(e){this.rotationSnap=e}setScaleSnap(e){this.scaleSnap=e}setSize(e){this.size=e}setSpace(e){this.space=e}}function PS(s){if(this.domElement.ownerDocument.pointerLockElement)return{x:0,y:0,button:s.button};{const e=this.domElement.getBoundingClientRect();return{x:(s.clientX-e.left)/e.width*2-1,y:-(s.clientY-e.top)/e.height*2+1,button:s.button}}}function IS(s){if(this.enabled)switch(s.pointerType){case"mouse":case"pen":this.pointerHover(this._getPointer(s));break}}function LS(s){this.enabled&&(document.pointerLockElement||this.domElement.setPointerCapture(s.pointerId),this.domElement.addEventListener("pointermove",this._onPointerMove),this.pointerHover(this._getPointer(s)),this.pointerDown(this._getPointer(s)))}function DS(s){this.enabled&&this.pointerMove(this._getPointer(s))}function NS(s){this.enabled&&(this.domElement.releasePointerCapture(s.pointerId),this.domElement.removeEventListener("pointermove",this._onPointerMove),this.pointerUp(this._getPointer(s)))}function pc(s,e,t){const n=e.intersectObject(s,!0);for(let i=0;i<n.length;i++)if(n[i].object.visible||t)return n[i];return!1}const Ta=new xn,Lt=new O(0,1,0),af=new O(0,0,0),lf=new Ke,Aa=new Dt,Fa=new Dt,Kn=new O,cf=new Ke,Hs=new O(1,0,0),pr=new O(0,1,0),Gs=new O(0,0,1),Ca=new O,Fs=new O,Bs=new O;class OS extends xt{constructor(){super(),this.isTransformControlsGizmo=!0,this.type="TransformControlsGizmo";const e=new ii({depthTest:!1,depthWrite:!1,fog:!1,toneMapped:!1,transparent:!0}),t=new un({depthTest:!1,depthWrite:!1,fog:!1,toneMapped:!1,transparent:!0}),n=e.clone();n.opacity=.15;const i=t.clone();i.opacity=.5;const r=e.clone();r.color.setHex(16711680);const o=e.clone();o.color.setHex(65280);const a=e.clone();a.color.setHex(255);const l=e.clone();l.color.setHex(16711680),l.opacity=.5;const u=e.clone();u.color.setHex(65280),u.opacity=.5;const d=e.clone();d.color.setHex(255),d.opacity=.5;const m=e.clone();m.opacity=.25;const p=e.clone();p.color.setHex(16776960),p.opacity=.25,e.clone().color.setHex(16776960);const _=e.clone();_.color.setHex(7895160);const M=new Wt(0,.04,.1,12);M.translate(0,.05,0);const y=new zt(.08,.08,.08);y.translate(0,.04,0);const v=new at;v.setAttribute("position",new He([0,0,0,1,0,0],3));const w=new Wt(.0075,.0075,.5,3);w.translate(0,.25,0);function S(J,ce){const de=new gi(J,.0075,3,64,ce*Math.PI*2);return de.rotateY(Math.PI/2),de.rotateX(Math.PI/2),de}function T(){const J=new at;return J.setAttribute("position",new He([0,0,0,1,1,1],3)),J}const z={X:[[new Ie(M,r),[.5,0,0],[0,0,-Math.PI/2]],[new Ie(M,r),[-.5,0,0],[0,0,Math.PI/2]],[new Ie(w,r),[0,0,0],[0,0,-Math.PI/2]]],Y:[[new Ie(M,o),[0,.5,0]],[new Ie(M,o),[0,-.5,0],[Math.PI,0,0]],[new Ie(w,o)]],Z:[[new Ie(M,a),[0,0,.5],[Math.PI/2,0,0]],[new Ie(M,a),[0,0,-.5],[-Math.PI/2,0,0]],[new Ie(w,a),null,[Math.PI/2,0,0]]],XYZ:[[new Ie(new vi(.1,0),m.clone()),[0,0,0]]],XY:[[new Ie(new zt(.15,.15,.01),d.clone()),[.15,.15,0]]],YZ:[[new Ie(new zt(.15,.15,.01),l.clone()),[0,.15,.15],[0,Math.PI/2,0]]],XZ:[[new Ie(new zt(.15,.15,.01),u.clone()),[.15,0,.15],[-Math.PI/2,0,0]]]},N={X:[[new Ie(new Wt(.2,0,.6,4),n),[.3,0,0],[0,0,-Math.PI/2]],[new Ie(new Wt(.2,0,.6,4),n),[-.3,0,0],[0,0,Math.PI/2]]],Y:[[new Ie(new Wt(.2,0,.6,4),n),[0,.3,0]],[new Ie(new Wt(.2,0,.6,4),n),[0,-.3,0],[0,0,Math.PI]]],Z:[[new Ie(new Wt(.2,0,.6,4),n),[0,0,.3],[Math.PI/2,0,0]],[new Ie(new Wt(.2,0,.6,4),n),[0,0,-.3],[-Math.PI/2,0,0]]],XYZ:[[new Ie(new vi(.2,0),n)]],XY:[[new Ie(new zt(.2,.2,.01),n),[.15,.15,0]]],YZ:[[new Ie(new zt(.2,.2,.01),n),[0,.15,.15],[0,Math.PI/2,0]]],XZ:[[new Ie(new zt(.2,.2,.01),n),[.15,0,.15],[-Math.PI/2,0,0]]]},A={START:[[new Ie(new vi(.01,2),i),null,null,null,"helper"]],END:[[new Ie(new vi(.01,2),i),null,null,null,"helper"]],DELTA:[[new ln(T(),i),null,null,null,"helper"]],X:[[new ln(v,i.clone()),[-1e3,0,0],null,[1e6,1,1],"helper"]],Y:[[new ln(v,i.clone()),[0,-1e3,0],[0,0,Math.PI/2],[1e6,1,1],"helper"]],Z:[[new ln(v,i.clone()),[0,0,-1e3],[0,-Math.PI/2,0],[1e6,1,1],"helper"]]},k={XYZE:[[new Ie(S(.5,1),_),null,[0,Math.PI/2,0]]],X:[[new Ie(S(.5,.5),r)]],Y:[[new Ie(S(.5,.5),o),null,[0,0,-Math.PI/2]]],Z:[[new Ie(S(.5,.5),a),null,[0,Math.PI/2,0]]],E:[[new Ie(S(.75,1),p),null,[0,Math.PI/2,0]]]},D={AXIS:[[new ln(v,i.clone()),[-1e3,0,0],null,[1e6,1,1],"helper"]]},C={XYZE:[[new Ie(new ps(.25,10,8),n)]],X:[[new Ie(new gi(.5,.1,4,24),n),[0,0,0],[0,-Math.PI/2,-Math.PI/2]]],Y:[[new Ie(new gi(.5,.1,4,24),n),[0,0,0],[Math.PI/2,0,0]]],Z:[[new Ie(new gi(.5,.1,4,24),n),[0,0,0],[0,0,-Math.PI/2]]],E:[[new Ie(new gi(.75,.1,2,24),n)]]},X={X:[[new Ie(y,r),[.5,0,0],[0,0,-Math.PI/2]],[new Ie(w,r),[0,0,0],[0,0,-Math.PI/2]],[new Ie(y,r),[-.5,0,0],[0,0,Math.PI/2]]],Y:[[new Ie(y,o),[0,.5,0]],[new Ie(w,o)],[new Ie(y,o),[0,-.5,0],[0,0,Math.PI]]],Z:[[new Ie(y,a),[0,0,.5],[Math.PI/2,0,0]],[new Ie(w,a),[0,0,0],[Math.PI/2,0,0]],[new Ie(y,a),[0,0,-.5],[-Math.PI/2,0,0]]],XY:[[new Ie(new zt(.15,.15,.01),d),[.15,.15,0]]],YZ:[[new Ie(new zt(.15,.15,.01),l),[0,.15,.15],[0,Math.PI/2,0]]],XZ:[[new Ie(new zt(.15,.15,.01),u),[.15,0,.15],[-Math.PI/2,0,0]]],XYZ:[[new Ie(new zt(.1,.1,.1),m.clone())]]},Y={X:[[new Ie(new Wt(.2,0,.6,4),n),[.3,0,0],[0,0,-Math.PI/2]],[new Ie(new Wt(.2,0,.6,4),n),[-.3,0,0],[0,0,Math.PI/2]]],Y:[[new Ie(new Wt(.2,0,.6,4),n),[0,.3,0]],[new Ie(new Wt(.2,0,.6,4),n),[0,-.3,0],[0,0,Math.PI]]],Z:[[new Ie(new Wt(.2,0,.6,4),n),[0,0,.3],[Math.PI/2,0,0]],[new Ie(new Wt(.2,0,.6,4),n),[0,0,-.3],[-Math.PI/2,0,0]]],XY:[[new Ie(new zt(.2,.2,.01),n),[.15,.15,0]]],YZ:[[new Ie(new zt(.2,.2,.01),n),[0,.15,.15],[0,Math.PI/2,0]]],XZ:[[new Ie(new zt(.2,.2,.01),n),[.15,0,.15],[-Math.PI/2,0,0]]],XYZ:[[new Ie(new zt(.2,.2,.2),n),[0,0,0]]]},H={X:[[new ln(v,i.clone()),[-1e3,0,0],null,[1e6,1,1],"helper"]],Y:[[new ln(v,i.clone()),[0,-1e3,0],[0,0,Math.PI/2],[1e6,1,1],"helper"]],Z:[[new ln(v,i.clone()),[0,0,-1e3],[0,-Math.PI/2,0],[1e6,1,1],"helper"]]};function K(J){const ce=new xt;for(const de in J)for(let Q=J[de].length;Q--;){const pe=J[de][Q][0].clone(),ve=J[de][Q][1],Ue=J[de][Q][2],Je=J[de][Q][3],vt=J[de][Q][4];pe.name=de,pe.tag=vt,ve&&pe.position.set(ve[0],ve[1],ve[2]),Ue&&pe.rotation.set(Ue[0],Ue[1],Ue[2]),Je&&pe.scale.set(Je[0],Je[1],Je[2]),pe.updateMatrix();const ae=pe.geometry.clone();ae.applyMatrix4(pe.matrix),pe.geometry=ae,pe.renderOrder=1/0,pe.position.set(0,0,0),pe.rotation.set(0,0,0),pe.scale.set(1,1,1),ce.add(pe)}return ce}this.gizmo={},this.picker={},this.helper={},this.add(this.gizmo.translate=K(z)),this.add(this.gizmo.rotate=K(k)),this.add(this.gizmo.scale=K(X)),this.add(this.picker.translate=K(N)),this.add(this.picker.rotate=K(C)),this.add(this.picker.scale=K(Y)),this.add(this.helper.translate=K(A)),this.add(this.helper.rotate=K(D)),this.add(this.helper.scale=K(H)),this.picker.translate.visible=!1,this.picker.rotate.visible=!1,this.picker.scale.visible=!1}updateMatrixWorld(e){const n=(this.mode==="scale"?"local":this.space)==="local"?this.worldQuaternion:Fa;this.gizmo.translate.visible=this.mode==="translate",this.gizmo.rotate.visible=this.mode==="rotate",this.gizmo.scale.visible=this.mode==="scale",this.helper.translate.visible=this.mode==="translate",this.helper.rotate.visible=this.mode==="rotate",this.helper.scale.visible=this.mode==="scale";let i=[];i=i.concat(this.picker[this.mode].children),i=i.concat(this.gizmo[this.mode].children),i=i.concat(this.helper[this.mode].children);for(let r=0;r<i.length;r++){const o=i[r];o.visible=!0,o.rotation.set(0,0,0),o.position.copy(this.worldPosition);let a;if(this.camera.isOrthographicCamera?a=(this.camera.top-this.camera.bottom)/this.camera.zoom:a=this.worldPosition.distanceTo(this.cameraPosition)*Math.min(1.9*Math.tan(Math.PI*this.camera.fov/360)/this.camera.zoom,7),o.scale.set(1,1,1).multiplyScalar(a*this.size/4),o.tag==="helper"){o.visible=!1,o.name==="AXIS"?(o.visible=!!this.axis,this.axis==="X"&&(Ot.setFromEuler(Ta.set(0,0,0)),o.quaternion.copy(n).multiply(Ot),Math.abs(Lt.copy(Hs).applyQuaternion(n).dot(this.eye))>.9&&(o.visible=!1)),this.axis==="Y"&&(Ot.setFromEuler(Ta.set(0,0,Math.PI/2)),o.quaternion.copy(n).multiply(Ot),Math.abs(Lt.copy(pr).applyQuaternion(n).dot(this.eye))>.9&&(o.visible=!1)),this.axis==="Z"&&(Ot.setFromEuler(Ta.set(0,Math.PI/2,0)),o.quaternion.copy(n).multiply(Ot),Math.abs(Lt.copy(Gs).applyQuaternion(n).dot(this.eye))>.9&&(o.visible=!1)),this.axis==="XYZE"&&(Ot.setFromEuler(Ta.set(0,Math.PI/2,0)),Lt.copy(this.rotationAxis),o.quaternion.setFromRotationMatrix(lf.lookAt(af,Lt,pr)),o.quaternion.multiply(Ot),o.visible=this.dragging),this.axis==="E"&&(o.visible=!1)):o.name==="START"?(o.position.copy(this.worldPositionStart),o.visible=this.dragging):o.name==="END"?(o.position.copy(this.worldPosition),o.visible=this.dragging):o.name==="DELTA"?(o.position.copy(this.worldPositionStart),o.quaternion.copy(this.worldQuaternionStart),an.set(1e-10,1e-10,1e-10).add(this.worldPositionStart).sub(this.worldPosition).multiplyScalar(-1),an.applyQuaternion(this.worldQuaternionStart.clone().invert()),o.scale.copy(an),o.visible=this.dragging):(o.quaternion.copy(n),this.dragging?o.position.copy(this.worldPositionStart):o.position.copy(this.worldPosition),this.axis&&(o.visible=this.axis.search(o.name)!==-1));continue}o.quaternion.copy(n),this.mode==="translate"||this.mode==="scale"?(o.name==="X"&&Math.abs(Lt.copy(Hs).applyQuaternion(n).dot(this.eye))>.99&&(o.scale.set(1e-10,1e-10,1e-10),o.visible=!1),o.name==="Y"&&Math.abs(Lt.copy(pr).applyQuaternion(n).dot(this.eye))>.99&&(o.scale.set(1e-10,1e-10,1e-10),o.visible=!1),o.name==="Z"&&Math.abs(Lt.copy(Gs).applyQuaternion(n).dot(this.eye))>.99&&(o.scale.set(1e-10,1e-10,1e-10),o.visible=!1),o.name==="XY"&&Math.abs(Lt.copy(Gs).applyQuaternion(n).dot(this.eye))<.2&&(o.scale.set(1e-10,1e-10,1e-10),o.visible=!1),o.name==="YZ"&&Math.abs(Lt.copy(Hs).applyQuaternion(n).dot(this.eye))<.2&&(o.scale.set(1e-10,1e-10,1e-10),o.visible=!1),o.name==="XZ"&&Math.abs(Lt.copy(pr).applyQuaternion(n).dot(this.eye))<.2&&(o.scale.set(1e-10,1e-10,1e-10),o.visible=!1)):this.mode==="rotate"&&(Aa.copy(n),Lt.copy(this.eye).applyQuaternion(Ot.copy(n).invert()),o.name.search("E")!==-1&&o.quaternion.setFromRotationMatrix(lf.lookAt(this.eye,af,pr)),o.name==="X"&&(Ot.setFromAxisAngle(Hs,Math.atan2(-Lt.y,Lt.z)),Ot.multiplyQuaternions(Aa,Ot),o.quaternion.copy(Ot)),o.name==="Y"&&(Ot.setFromAxisAngle(pr,Math.atan2(Lt.x,Lt.z)),Ot.multiplyQuaternions(Aa,Ot),o.quaternion.copy(Ot)),o.name==="Z"&&(Ot.setFromAxisAngle(Gs,Math.atan2(Lt.y,Lt.x)),Ot.multiplyQuaternions(Aa,Ot),o.quaternion.copy(Ot))),o.visible=o.visible&&(o.name.indexOf("X")===-1||this.showX),o.visible=o.visible&&(o.name.indexOf("Y")===-1||this.showY),o.visible=o.visible&&(o.name.indexOf("Z")===-1||this.showZ),o.visible=o.visible&&(o.name.indexOf("E")===-1||this.showX&&this.showY&&this.showZ),o.material._color=o.material._color||o.material.color.clone(),o.material._opacity=o.material._opacity||o.material.opacity,o.material.color.copy(o.material._color),o.material.opacity=o.material._opacity,this.enabled&&this.axis&&(o.name===this.axis||this.axis.split("").some(function(l){return o.name===l}))&&(o.material.color.setHex(16776960),o.material.opacity=1)}super.updateMatrixWorld(e)}}class US extends Ie{constructor(){super(new Cr(1e5,1e5,2,2),new ii({visible:!1,wireframe:!0,side:Tn,transparent:!0,opacity:.1,toneMapped:!1})),this.isTransformControlsPlane=!0,this.type="TransformControlsPlane"}updateMatrixWorld(e){let t=this.space;switch(this.position.copy(this.worldPosition),this.mode==="scale"&&(t="local"),Ca.copy(Hs).applyQuaternion(t==="local"?this.worldQuaternion:Fa),Fs.copy(pr).applyQuaternion(t==="local"?this.worldQuaternion:Fa),Bs.copy(Gs).applyQuaternion(t==="local"?this.worldQuaternion:Fa),Lt.copy(Fs),this.mode){case"translate":case"scale":switch(this.axis){case"X":Lt.copy(this.eye).cross(Ca),Kn.copy(Ca).cross(Lt);break;case"Y":Lt.copy(this.eye).cross(Fs),Kn.copy(Fs).cross(Lt);break;case"Z":Lt.copy(this.eye).cross(Bs),Kn.copy(Bs).cross(Lt);break;case"XY":Kn.copy(Bs);break;case"YZ":Kn.copy(Ca);break;case"XZ":Lt.copy(Bs),Kn.copy(Fs);break;case"XYZ":case"E":Kn.set(0,0,0);break}break;case"rotate":default:Kn.set(0,0,0)}Kn.length()===0?this.quaternion.copy(this.cameraQuaternion):(cf.lookAt(an.set(0,0,0),Kn,Lt),this.quaternion.setFromRotationMatrix(cf)),super.updateMatrixWorld(e)}}const hf={type:"change"},mc={type:"start"},uf={type:"end"},Ra=new Ar,df=new $n,FS=Math.cos(70*pp.DEG2RAD);class BS extends ni{constructor(e,t){super(),this.object=e,this.domElement=t,this.domElement.style.touchAction="none",this.enabled=!0,this.target=new O,this.cursor=new O,this.minDistance=0,this.maxDistance=1/0,this.minZoom=0,this.maxZoom=1/0,this.minTargetRadius=0,this.maxTargetRadius=1/0,this.minPolarAngle=0,this.maxPolarAngle=Math.PI,this.minAzimuthAngle=-1/0,this.maxAzimuthAngle=1/0,this.enableDamping=!1,this.dampingFactor=.05,this.enableZoom=!0,this.zoomSpeed=1,this.enableRotate=!0,this.rotateSpeed=1,this.enablePan=!0,this.panSpeed=1,this.screenSpacePanning=!0,this.keyPanSpeed=7,this.zoomToCursor=!1,this.autoRotate=!1,this.autoRotateSpeed=2,this.keys={LEFT:"ArrowLeft",UP:"ArrowUp",RIGHT:"ArrowRight",BOTTOM:"ArrowDown"},this.mouseButtons={LEFT:hr.ROTATE,MIDDLE:hr.DOLLY,RIGHT:hr.PAN},this.touches={ONE:ur.ROTATE,TWO:ur.DOLLY_PAN},this.target0=this.target.clone(),this.position0=this.object.position.clone(),this.zoom0=this.object.zoom,this._domElementKeyEvents=null,this.getPolarAngle=function(){return a.phi},this.getAzimuthalAngle=function(){return a.theta},this.getDistance=function(){return this.object.position.distanceTo(this.target)},this.listenToKeyEvents=function(V){V.addEventListener("keydown",Re),this._domElementKeyEvents=V},this.stopListenToKeyEvents=function(){this._domElementKeyEvents.removeEventListener("keydown",Re),this._domElementKeyEvents=null},this.saveState=function(){n.target0.copy(n.target),n.position0.copy(n.object.position),n.zoom0=n.object.zoom},this.reset=function(){n.target.copy(n.target0),n.object.position.copy(n.position0),n.object.zoom=n.zoom0,n.object.updateProjectionMatrix(),n.dispatchEvent(hf),n.update(),r=i.NONE},this.update=function(){const V=new O,fe=new Dt().setFromUnitVectors(e.up,new O(0,1,0)),G=fe.clone().invert(),re=new O,ne=new Dt,Te=new O,Pe=2*Math.PI;return function(St=null){const Et=n.object.position;V.copy(Et).sub(n.target),V.applyQuaternion(fe),a.setFromVector3(V),n.autoRotate&&r===i.NONE&&X(D(St)),n.enableDamping?(a.theta+=l.theta*n.dampingFactor,a.phi+=l.phi*n.dampingFactor):(a.theta+=l.theta,a.phi+=l.phi);let Tt=n.minAzimuthAngle,ct=n.maxAzimuthAngle;isFinite(Tt)&&isFinite(ct)&&(Tt<-Math.PI?Tt+=Pe:Tt>Math.PI&&(Tt-=Pe),ct<-Math.PI?ct+=Pe:ct>Math.PI&&(ct-=Pe),Tt<=ct?a.theta=Math.max(Tt,Math.min(ct,a.theta)):a.theta=a.theta>(Tt+ct)/2?Math.max(Tt,a.theta):Math.min(ct,a.theta)),a.phi=Math.max(n.minPolarAngle,Math.min(n.maxPolarAngle,a.phi)),a.makeSafe(),n.enableDamping===!0?n.target.addScaledVector(d,n.dampingFactor):n.target.add(d),n.target.sub(n.cursor),n.target.clampLength(n.minTargetRadius,n.maxTargetRadius),n.target.add(n.cursor);let Kt=!1;if(n.zoomToCursor&&N||n.object.isOrthographicCamera)a.radius=pe(a.radius);else{const At=a.radius;a.radius=pe(a.radius*u),Kt=At!=a.radius}if(V.setFromSpherical(a),V.applyQuaternion(G),Et.copy(n.target).add(V),n.object.lookAt(n.target),n.enableDamping===!0?(l.theta*=1-n.dampingFactor,l.phi*=1-n.dampingFactor,d.multiplyScalar(1-n.dampingFactor)):(l.set(0,0,0),d.set(0,0,0)),n.zoomToCursor&&N){let At=null;if(n.object.isPerspectiveCamera){const Pn=V.length();At=pe(Pn*u);const si=Pn-At;n.object.position.addScaledVector(T,si),n.object.updateMatrixWorld(),Kt=!!si}else if(n.object.isOrthographicCamera){const Pn=new O(z.x,z.y,0);Pn.unproject(n.object);const si=n.object.zoom;n.object.zoom=Math.max(n.minZoom,Math.min(n.maxZoom,n.object.zoom/u)),n.object.updateProjectionMatrix(),Kt=si!==n.object.zoom;const Yi=new O(z.x,z.y,0);Yi.unproject(n.object),n.object.position.sub(Yi).add(Pn),n.object.updateMatrixWorld(),At=V.length()}else console.warn("WARNING: OrbitControls.js encountered an unknown camera type - zoom to cursor disabled."),n.zoomToCursor=!1;At!==null&&(this.screenSpacePanning?n.target.set(0,0,-1).transformDirection(n.object.matrix).multiplyScalar(At).add(n.object.position):(Ra.origin.copy(n.object.position),Ra.direction.set(0,0,-1).transformDirection(n.object.matrix),Math.abs(n.object.up.dot(Ra.direction))<FS?e.lookAt(n.target):(df.setFromNormalAndCoplanarPoint(n.object.up,n.target),Ra.intersectPlane(df,n.target))))}else if(n.object.isOrthographicCamera){const At=n.object.zoom;n.object.zoom=Math.max(n.minZoom,Math.min(n.maxZoom,n.object.zoom/u)),At!==n.object.zoom&&(n.object.updateProjectionMatrix(),Kt=!0)}return u=1,N=!1,Kt||re.distanceToSquared(n.object.position)>o||8*(1-ne.dot(n.object.quaternion))>o||Te.distanceToSquared(n.target)>o?(n.dispatchEvent(hf),re.copy(n.object.position),ne.copy(n.object.quaternion),Te.copy(n.target),!0):!1}}(),this.dispose=function(){n.domElement.removeEventListener("contextmenu",Ce),n.domElement.removeEventListener("pointerdown",Fe),n.domElement.removeEventListener("pointercancel",B),n.domElement.removeEventListener("wheel",ue),n.domElement.removeEventListener("pointermove",Ge),n.domElement.removeEventListener("pointerup",B),n.domElement.getRootNode().removeEventListener("keydown",me,{capture:!0}),n._domElementKeyEvents!==null&&(n._domElementKeyEvents.removeEventListener("keydown",Re),n._domElementKeyEvents=null)};const n=this,i={NONE:-1,ROTATE:0,DOLLY:1,PAN:2,TOUCH_ROTATE:3,TOUCH_PAN:4,TOUCH_DOLLY_PAN:5,TOUCH_DOLLY_ROTATE:6};let r=i.NONE;const o=1e-6,a=new nh,l=new nh;let u=1;const d=new O,m=new se,p=new se,g=new se,_=new se,M=new se,y=new se,v=new se,w=new se,S=new se,T=new O,z=new se;let N=!1;const A=[],k={};function D(V){return V!==null?2*Math.PI/60*n.autoRotateSpeed*V:2*Math.PI/60/60*n.autoRotateSpeed}function C(V){const fe=Math.abs(V*.01);return Math.pow(.95,n.zoomSpeed*fe)}function X(V){l.theta-=V}function Y(V){l.phi-=V}const H=function(){const V=new O;return function(G,re){V.setFromMatrixColumn(re,0),V.multiplyScalar(-G),d.add(V)}}(),K=function(){const V=new O;return function(G,re){n.screenSpacePanning===!0?V.setFromMatrixColumn(re,1):(V.setFromMatrixColumn(re,0),V.crossVectors(n.object.up,V)),V.multiplyScalar(G),d.add(V)}}(),J=function(){const V=new O;return function(G,re){const ne=n.domElement;if(n.object.isPerspectiveCamera){const Te=n.object.position;V.copy(Te).sub(n.target);let Pe=V.length();Pe*=Math.tan(n.object.fov/2*Math.PI/180),H(2*G*Pe/ne.clientHeight,n.object.matrix),K(2*re*Pe/ne.clientHeight,n.object.matrix)}else n.object.isOrthographicCamera?(H(G*(n.object.right-n.object.left)/n.object.zoom/ne.clientWidth,n.object.matrix),K(re*(n.object.top-n.object.bottom)/n.object.zoom/ne.clientHeight,n.object.matrix)):(console.warn("WARNING: OrbitControls.js encountered an unknown camera type - pan disabled."),n.enablePan=!1)}}();function ce(V){n.object.isPerspectiveCamera||n.object.isOrthographicCamera?u/=V:(console.warn("WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled."),n.enableZoom=!1)}function de(V){n.object.isPerspectiveCamera||n.object.isOrthographicCamera?u*=V:(console.warn("WARNING: OrbitControls.js encountered an unknown camera type - dolly/zoom disabled."),n.enableZoom=!1)}function Q(V,fe){if(!n.zoomToCursor)return;N=!0;const G=n.domElement.getBoundingClientRect(),re=V-G.left,ne=fe-G.top,Te=G.width,Pe=G.height;z.x=re/Te*2-1,z.y=-(ne/Pe)*2+1,T.set(z.x,z.y,1).unproject(n.object).sub(n.object.position).normalize()}function pe(V){return Math.max(n.minDistance,Math.min(n.maxDistance,V))}function ve(V){m.set(V.clientX,V.clientY)}function Ue(V){Q(V.clientX,V.clientX),v.set(V.clientX,V.clientY)}function Je(V){_.set(V.clientX,V.clientY)}function vt(V){p.set(V.clientX,V.clientY),g.subVectors(p,m).multiplyScalar(n.rotateSpeed);const fe=n.domElement;X(2*Math.PI*g.x/fe.clientHeight),Y(2*Math.PI*g.y/fe.clientHeight),m.copy(p),n.update()}function ae(V){w.set(V.clientX,V.clientY),S.subVectors(w,v),S.y>0?ce(C(S.y)):S.y<0&&de(C(S.y)),v.copy(w),n.update()}function we(V){M.set(V.clientX,V.clientY),y.subVectors(M,_).multiplyScalar(n.panSpeed),J(y.x,y.y),_.copy(M),n.update()}function ke(V){let fe=!1;switch(V.code){case n.keys.UP:V.ctrlKey||V.metaKey||V.shiftKey?Y(2*Math.PI*n.rotateSpeed/n.domElement.clientHeight):J(0,n.keyPanSpeed),fe=!0;break;case n.keys.BOTTOM:V.ctrlKey||V.metaKey||V.shiftKey?Y(-2*Math.PI*n.rotateSpeed/n.domElement.clientHeight):J(0,-n.keyPanSpeed),fe=!0;break;case n.keys.LEFT:V.ctrlKey||V.metaKey||V.shiftKey?X(2*Math.PI*n.rotateSpeed/n.domElement.clientHeight):J(n.keyPanSpeed,0),fe=!0;break;case n.keys.RIGHT:V.ctrlKey||V.metaKey||V.shiftKey?X(-2*Math.PI*n.rotateSpeed/n.domElement.clientHeight):J(-n.keyPanSpeed,0),fe=!0;break}fe&&(V.preventDefault(),n.update())}function Ee(V){if(A.length===1)m.set(V.pageX,V.pageY);else{const fe=Qe(V),G=.5*(V.pageX+fe.x),re=.5*(V.pageY+fe.y);m.set(G,re)}}function nt(V){if(A.length===1)_.set(V.pageX,V.pageY);else{const fe=Qe(V),G=.5*(V.pageX+fe.x),re=.5*(V.pageY+fe.y);_.set(G,re)}}function be(V){const fe=Qe(V),G=V.pageX-fe.x,re=V.pageY-fe.y,ne=Math.sqrt(G*G+re*re);v.set(0,ne)}function Z(V){n.enableZoom&&be(V),n.enablePan&&nt(V)}function st(V){n.enableZoom&&be(V),n.enableRotate&&Ee(V)}function le(V){if(A.length==1)p.set(V.pageX,V.pageY);else{const G=Qe(V),re=.5*(V.pageX+G.x),ne=.5*(V.pageY+G.y);p.set(re,ne)}g.subVectors(p,m).multiplyScalar(n.rotateSpeed);const fe=n.domElement;X(2*Math.PI*g.x/fe.clientHeight),Y(2*Math.PI*g.y/fe.clientHeight),m.copy(p)}function xe(V){if(A.length===1)M.set(V.pageX,V.pageY);else{const fe=Qe(V),G=.5*(V.pageX+fe.x),re=.5*(V.pageY+fe.y);M.set(G,re)}y.subVectors(M,_).multiplyScalar(n.panSpeed),J(y.x,y.y),_.copy(M)}function he(V){const fe=Qe(V),G=V.pageX-fe.x,re=V.pageY-fe.y,ne=Math.sqrt(G*G+re*re);w.set(0,ne),S.set(0,Math.pow(w.y/v.y,n.zoomSpeed)),ce(S.y),v.copy(w);const Te=(V.pageX+fe.x)*.5,Pe=(V.pageY+fe.y)*.5;Q(Te,Pe)}function Ae(V){n.enableZoom&&he(V),n.enablePan&&xe(V)}function ge(V){n.enableZoom&&he(V),n.enableRotate&&le(V)}function Fe(V){n.enabled!==!1&&(A.length===0&&(n.domElement.setPointerCapture(V.pointerId),n.domElement.addEventListener("pointermove",Ge),n.domElement.addEventListener("pointerup",B)),!je(V)&&(Xe(V),V.pointerType==="touch"?Le(V):I(V)))}function Ge(V){n.enabled!==!1&&(V.pointerType==="touch"?tt(V):$(V))}function B(V){switch(lt(V),A.length){case 0:n.domElement.releasePointerCapture(V.pointerId),n.domElement.removeEventListener("pointermove",Ge),n.domElement.removeEventListener("pointerup",B),n.dispatchEvent(uf),r=i.NONE;break;case 1:const fe=A[0],G=k[fe];Le({pointerId:fe,pageX:G.x,pageY:G.y});break}}function I(V){let fe;switch(V.button){case 0:fe=n.mouseButtons.LEFT;break;case 1:fe=n.mouseButtons.MIDDLE;break;case 2:fe=n.mouseButtons.RIGHT;break;default:fe=-1}switch(fe){case hr.DOLLY:if(n.enableZoom===!1)return;Ue(V),r=i.DOLLY;break;case hr.ROTATE:if(V.ctrlKey||V.metaKey||V.shiftKey){if(n.enablePan===!1)return;Je(V),r=i.PAN}else{if(n.enableRotate===!1)return;ve(V),r=i.ROTATE}break;case hr.PAN:if(V.ctrlKey||V.metaKey||V.shiftKey){if(n.enableRotate===!1)return;ve(V),r=i.ROTATE}else{if(n.enablePan===!1)return;Je(V),r=i.PAN}break;default:r=i.NONE}r!==i.NONE&&n.dispatchEvent(mc)}function $(V){switch(r){case i.ROTATE:if(n.enableRotate===!1)return;vt(V);break;case i.DOLLY:if(n.enableZoom===!1)return;ae(V);break;case i.PAN:if(n.enablePan===!1)return;we(V);break}}function ue(V){n.enabled===!1||n.enableZoom===!1||r!==i.NONE||(V.preventDefault(),n.dispatchEvent(mc),_e(V),n.dispatchEvent(uf))}function _e(V){Q(V.clientX,V.clientY);var fe=n.object;const G=V.offsetX/V.srcElement.clientWidth*2-1,re=-(V.offsetY/V.srcElement.clientHeight)*2+1;let ne=new O(G,re,0);ne.unproject(fe),ne.sub(fe.position);const Te=C(V.deltaY);V.deltaY<0?(fe.position.addVectors(fe.position,ne.setLength(Te)),n.target.addVectors(n.target,ne.setLength(Te))):V.deltaY>0&&(fe.position.subVectors(fe.position,ne.setLength(Te)),n.target.subVectors(n.target,ne.setLength(Te))),fe.updateProjectionMatrix()}function me(V){V.key==="Control"&&n.domElement.getRootNode().addEventListener("keyup",Ye,{passive:!0,capture:!0})}function Ye(V){V.key==="Control"&&n.domElement.getRootNode().removeEventListener("keyup",Ye,{passive:!0,capture:!0})}function Re(V){n.enabled===!1||n.enablePan===!1||ke(V)}function Le(V){switch(ze(V),A.length){case 1:switch(n.touches.ONE){case ur.ROTATE:if(n.enableRotate===!1)return;Ee(V),r=i.TOUCH_ROTATE;break;case ur.PAN:if(n.enablePan===!1)return;nt(V),r=i.TOUCH_PAN;break;default:r=i.NONE}break;case 2:switch(n.touches.TWO){case ur.DOLLY_PAN:if(n.enableZoom===!1&&n.enablePan===!1)return;Z(V),r=i.TOUCH_DOLLY_PAN;break;case ur.DOLLY_ROTATE:if(n.enableZoom===!1&&n.enableRotate===!1)return;st(V),r=i.TOUCH_DOLLY_ROTATE;break;default:r=i.NONE}break;default:r=i.NONE}r!==i.NONE&&n.dispatchEvent(mc)}function tt(V){switch(ze(V),r){case i.TOUCH_ROTATE:if(n.enableRotate===!1)return;le(V),n.update();break;case i.TOUCH_PAN:if(n.enablePan===!1)return;xe(V),n.update();break;case i.TOUCH_DOLLY_PAN:if(n.enableZoom===!1&&n.enablePan===!1)return;Ae(V),n.update();break;case i.TOUCH_DOLLY_ROTATE:if(n.enableZoom===!1&&n.enableRotate===!1)return;ge(V),n.update();break;default:r=i.NONE}}function Ce(V){n.enabled!==!1&&V.preventDefault()}function Xe(V){A.push(V.pointerId)}function lt(V){delete k[V.pointerId];for(let fe=0;fe<A.length;fe++)if(A[fe]==V.pointerId){A.splice(fe,1);return}}function je(V){for(let fe=0;fe<A.length;fe++)if(A[fe]==V.pointerId)return!0;return!1}function ze(V){let fe=k[V.pointerId];fe===void 0&&(fe=new se,k[V.pointerId]=fe),fe.set(V.pageX,V.pageY)}function Qe(V){const fe=V.pointerId===A[0]?A[1]:A[0];return k[fe]}n.domElement.addEventListener("contextmenu",Ce),n.domElement.addEventListener("pointerdown",Fe),n.domElement.addEventListener("pointercancel",B),n.domElement.addEventListener("wheel",ue,{passive:!1}),n.domElement.getRootNode().addEventListener("keydown",me,{passive:!0,capture:!0}),this.update()}}class zS{constructor(e){this.scene=e,this.undoStack=[],this.redoStack=[],this.listenForChanges(),this.restoreUndoRedo()}listenForChanges(){addEventListener("sceneChanged",e=>this.saveState(e.detail)),addEventListener("undo",()=>this.undo()),addEventListener("redo",()=>this.redo()),addEventListener("keyup",e=>this.onKeyUp(e))}restoreUndoRedo(){if(!this.ctrlKey){let e=localStorage.getItem("undoStack");e&&(this.undoStack=JSON.parse(e),e=localStorage.getItem("redoStack"),this.redoStack=JSON.parse(e))}}saveUndoRedo(){localStorage.setItem("undoStack",JSON.stringify(this.undoStack)),localStorage.setItem("redoStack",JSON.stringify(this.redoStack))}onKeyUp(e){this.ctrlKey=e.ctrlKey,e.ctrlKey&&(e.key.toLowerCase()==="z"?this.undo():e.key.toLowerCase()==="y"&&this.redo())}saveState(e){e.changes&&(this.undoStack.push(e.changes),this.redoStack.length=0,this.saveUndoRedo())}undo(){if(this.undoStack.length===0)return;const e=this.undoStack.pop();this.redoStack.push(e),this.restoreState(e,"oldValue"),this.saveUndoRedo()}redo(){if(this.redoStack.length===0)return;const e=this.redoStack.pop();this.undoStack.push(e),this.restoreState(e,"newValue"),this.saveUndoRedo()}getCurrentState(e){const t=this.getObjectById(e.myId);return t?{type:e.type,id:t.id,position:t.position.clone(),rotation:t.rotation.clone(),scale:t.scale.clone(),color:t.material?t.material.color.clone():null}:null}restoreState(e,t){const n=this.getObjectById(e.myId);if(n)switch(e.type){case"position":n.position.copy(e.position[t]);break;case"rotation":n.rotation.copy(e.rotation[t]);break;case"scale":n.scale.copy(e.scale[t]);break;case"materialColor":n.material&&e.materialColor&&n.material.color.copy(e.materialColor[t]);break}}getObjectById(e){return this.scene.getObjectByProperty("myId",e)}}function Tm(s){const e=localStorage.getItem(s);return e?JSON.parse(e):(console.log("No data found in localStorage for the given key."),null)}addEventListener("orbitControlChanged",s=>{localStorage.setItem("orbitControl",JSON.stringify(s.detail.orbitControlData))});function kS(){const s=new se;let e=!1,t=null,n=null,i=!1,r=!1;const o=new $n;let a=null,l=null;const u=new Qh;let d,m,p,g,_=!1;addEventListener("beforeunload",M,!1);function M(Y){e&&(localStorage.removeItem("selectedId"),localStorage.removeItem("transformControl"),localStorage.removeItem("orbitControl"),localStorage.removeItem("scene"),localStorage.removeItem("redoStack"),localStorage.removeItem("undoStack"))}function y(Y){[d,m,p,g]=Y.properties,new zS(p);const H=Tm("orbitControl");l=new BS(d,m.domElement),H&&(d.position.fromArray(H.position),l.target.fromArray(H.target)),l.addEventListener("end",()=>{const ce={target:l.target.toArray().map(Q=>Math.round(Q*1e3)/1e3),position:d.position.toArray().map(Q=>Math.round(Q*1e3)/1e3)},de=new CustomEvent("orbitControlChanged",{detail:{orbitControlData:ce}});dispatchEvent(de)});const K=localStorage.getItem("orbitControl");if(K){const ce=JSON.parse(K);l.target.fromArray(ce.target),l.update()}a=new RS(d,m.domElement),a.addEventListener("change",S),a.addEventListener("dragging-changed",T),a.setMode("translate"),a.enabled=!1,p.add(a),z();const J=localStorage.getItem("selectedId");if(J){const ce=JSON.parse(J),de=p.getObjectByProperty("myId",ce);C(de);const Q=localStorage.getItem("transformControl");Q&&(a.attach(de),a.setMode(JSON.parse(Q)),a.enabled=!0)}}function v(Y){Y.shiftKey,e=Y.ctrlKey,Y.altKey}function w(Y){Y.shiftKey,e=Y.ctrlKey,Y.altKey}function S(Y){let H=a.object||t;if(a.enabled||(H=null),H&&i){const K=new CustomEvent("objectMoving",{detail:{mesh:H}});dispatchEvent(K)}}function T(Y){l.enabled=!Y.value}function z(){addEventListener("pointermove",N),addEventListener("mousedown",A),addEventListener("mouseup",k),addEventListener("keyup",w),addEventListener("keydown",v),addEventListener("dblclick",D)}function N(Y){if(s.x=Y.clientX/window.innerWidth*2-1,s.y=-(Y.clientY/window.innerHeight)*2+1,i&&(r=!0),!l.enabled){if(a.enabled){_=!0;return}if(i&&n){const H=new O;u.setFromCamera(s,d),u.ray.intersectPlane(o,H);const K=n.clone().sub(H);t.position.sub(K),n=H,_=!0;const J=new CustomEvent("objectMoving",{detail:{mesh:t}});dispatchEvent(J)}}}function A(Y){if(Y.target.id!=="renderCanvas")return;i=!0,u.setFromCamera(s,d);const H=u.intersectObjects(g);H.length>0?(C(H[0].object),n=H[0].point,X(),l.enabled=!1):a.enabled||(n=null,l.enabled=!0)}function k(Y){if(Y.target.id==="renderCanvas"){if(!r&&!n&&C(null),i=!1,r=!1,_){_=!1;const H=new CustomEvent("objectChanged",{detail:{mesh:t}});dispatchEvent(H)}l.enabled=!0}}function D(Y){if(Y.target.id!=="renderCanvas")return;if(u.setFromCamera(s,d),u.intersectObjects(g).length>0&&t)if(a.attach(t),!a.enabled)a.setMode("translate"),a.enabled=!0,n=null;else switch(a.mode){case"translate":a.setMode("rotate");break;case"rotate":a.setMode("scale");break;case"scale":a.detach(),a.enabled=!1,a.setMode("translate");break}a.enabled?localStorage.setItem("transformControl",JSON.stringify(a.mode)):localStorage.removeItem("transformControl")}function C(Y){if(t!==Y||t==null&&Y==null){t=Y;const H=new CustomEvent("objectSelected",{detail:{mesh:Y}});dispatchEvent(H),a.enabled=!1,a.detach(),a.setMode("scale")}}function X(){const Y=new O;d.getWorldDirection(Y),o.setFromNormalAndCoplanarPoint(Y,n)}return{setup:y,orbitControl:l}}const ff=kS();function VS(){const s=document.getElementById("info");function e(g){m(g.detail.mesh),removeEventListener("objectSelected",e),addEventListener("objectSelected",p,!1)}addEventListener("objectSelected",e,!1),addEventListener("objectMoving",p,!1);let t;function n(g){return{position:g.position,rotation:g.rotation,scale:g.scale,materialColor:g.material.color,geometryType:g.geometry.type}}function i(g,_){return function(M){t[g][_]=parseFloat(M.target.value);const y=new CustomEvent("objectChanged",{detail:{mesh:t}});dispatchEvent(y)}}function r(){return function(g){t.material.color.set(g.target.value)}}function o(){return function(g){const _=new CustomEvent("objectChanged",{detail:{mesh:t}});dispatchEvent(_)}}function a(){document.querySelectorAll("input[data-prop]").forEach(g=>{const[_,M]=g.getAttribute("data-prop").split("-");_==="color"?(g.addEventListener("input",r()),g.addEventListener("change",o())):g.addEventListener("input",i(_,M))})}function l(g,_,M){const y=document.createElement("tr"),v=document.createElement("td");v.className="propertyName",v.textContent=g,y.appendChild(v);const w="0.1";return M.forEach(S=>{const T=S.toLowerCase(),z=_[T]!==void 0?_[T].toFixed(2):0,N=document.createElement("td"),A=document.createElement("label");A.setAttribute("for",`${g}-${T}`),A.textContent=`${S}: `;const k=document.createElement("input");k.type="number",k.id=`${g}-${T}`,k.dataset.prop=`${g}-${T}`,k.value=z,k.step=w,N.appendChild(A),N.appendChild(k),y.appendChild(N)}),y}function u(g,_,M){M.forEach(y=>{const v=y.toLowerCase(),w=document.getElementById(`${g}-${v}`);w&&(w.value=_[v]!==void 0?_[v].toFixed(2):0)})}function d(g){const _=document.getElementById("color");_&&(_.value=`#${g.getHexString()}`)}function m(g){t=g;const _=n(t);s.textContent="";const M=document.createElement("strong");M.textContent="Properties",s.appendChild(M);const y=document.createElement("table");y.className="blueTable";const v=document.createElement("tbody");if(_.position&&v.appendChild(l("position",_.position,["X","Y","Z"])),_.rotation&&v.appendChild(l("rotation",_.rotation,["X","Y","Z"])),_.scale&&v.appendChild(l("scale",_.scale,["X","Y","Z"])),_.materialColor){const w=document.createElement("tr"),S=document.createElement("td");S.textContent="Material Color",w.appendChild(S);const T=document.createElement("td");T.setAttribute("colspan","3");const z=document.createElement("label");z.setAttribute("for","color"),z.textContent="Color: ";const N=document.createElement("input");N.type="color",N.id="color",N.dataset.prop="color",N.value=`#${_.materialColor.getHexString()}`,T.appendChild(z),T.appendChild(N),w.appendChild(T),v.appendChild(w)}if(_.geometryType){const w=document.createElement("tr"),S=document.createElement("td");S.textContent="Geometry Type",w.appendChild(S);const T=document.createElement("td");T.setAttribute("colspan","3"),T.textContent=_.geometryType,w.appendChild(T),v.appendChild(w)}y.appendChild(v),s.appendChild(y),document.getElementById("1").style.display="block",a()}function p(g){if(g.detail.mesh){t=g.detail.mesh;const _=n(t);_.position&&u("position",_.position,["X","Y","Z"]),_.rotation&&u("rotation",_.rotation,["X","Y","Z"]),_.scale&&u("scale",_.scale,["X","Y","Z"]),_.materialColor&&d(_.materialColor),document.getElementById("1").style.display="block"}else document.getElementById("1").style.display="none"}return{updateInfoDiv:p,initializeInfoDiv:m}}VS();Tg.setup();const pf=Tm("scene");pf?(Vs.setup(pf),ff.setup(Vs)):fetch("./assets/default-scene.json").then(s=>s.json()).then(function(s){Vs.setup(s),ff.setup(Vs)});window.addEventListener("resize",Vs.onWindowResize);
//# sourceMappingURL=index-DdRMChwS.js.map
