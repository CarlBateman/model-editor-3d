# Model Editor 3D Three.js

## Live view
https://carlbateman.gitlab.io/model-editor-3d/

## Setup

### Install dependencies (only the first time)
Download and install [Node.js](https://nodejs.org/en/download/) (if not already installed).
`npm install`

### In terminal:
Navigate to the project root:
```
npm init -y

npm install vite

npm install three
```

### Run the local server
`npm run dev`

### Build for production in the dist/ directory
``npm run build``
