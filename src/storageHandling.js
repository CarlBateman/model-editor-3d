﻿function recoverFromLocalStorage(key) {
	const jsonString = localStorage.getItem(key);
	if (jsonString) {
		// Parse the JSON string back to an object
		return JSON.parse(jsonString);
	} else {
		console.log("No data found in localStorage for the given key.");
		return null;
	}
}

addEventListener("orbitControlChanged", (event) => {
	localStorage.setItem("orbitControl", JSON.stringify(event.detail.orbitControlData));
});



export { recoverFromLocalStorage };