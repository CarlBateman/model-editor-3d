import * as THREE from "three";
import { TransformControls } from "three/examples/jsm/controls/TransformControls";
import { OrbitControls } from "../custom/OrbitControl.js";
import { HistoryManager } from "./historyManager";
import { recoverFromLocalStorage } from "./storageHandling";

function createUserInteraction() {
	const pointer = new THREE.Vector2();
	let shiftKey = false;
	let ctrlKey = false;
	let altKey = false;
	let currentSelection = null;
	let grabPoint = null;
	let mouseDown = false;
	let dragging = false;

	const plane = new THREE.Plane();
	let transformControl = null;
	let orbitControl = null;
	const raycaster = new THREE.Raycaster();

	let camera, renderer, scene, meshes;
	let objectChanged = false;

	addEventListener('beforeunload', clearLocalStorage, false);

	function clearLocalStorage(event) {
		if (ctrlKey) {
			localStorage.removeItem("selectedId");
			localStorage.removeItem("transformControl");
			localStorage.removeItem("orbitControl");
			localStorage.removeItem("scene");
			localStorage.removeItem("redoStack");
			localStorage.removeItem("undoStack");
		}
	}


	function setup(controller) {
		[camera, renderer, scene, meshes] = controller.properties;
		const historyManager = new HistoryManager(scene);

		const json = recoverFromLocalStorage("orbitControl");

		orbitControl = new OrbitControls(camera, renderer.domElement);
		if (json) {
			camera.position.fromArray(json.position);
			orbitControl.target.fromArray(json.target);
		} else {
			fetch('./assets/default-scene.json')
				.then(
					(response) => response.json())
				.then(function (json) {
					if (json.orbitControl) {
						camera.position.fromArray(json.orbitControl.position);
						orbitControl.target.fromArray(json.orbitControl.target);
					}
				});
		}

		orbitControl.addEventListener('end', () => {
			const orbitControlData = {
				target: orbitControl.target.toArray().map(x => Math.round(x * 1000) / 1000),
				position: camera.position.toArray().map(x => Math.round(x * 1000) / 1000),
			};
			const orbitControlChanged = new CustomEvent("orbitControlChanged", { detail: { orbitControlData } });
			dispatchEvent(orbitControlChanged);
		});

		const jsonString = localStorage.getItem("orbitControl");
		if (jsonString) {
			const json = JSON.parse(jsonString);
			orbitControl.target.fromArray(json.target);
			orbitControl.update();
		}

		transformControl = new TransformControls(camera, renderer.domElement);
		transformControl.addEventListener("change", onTransformChanging);
		transformControl.addEventListener("dragging-changed", onTransformDraggingChanged);
		transformControl.setMode("translate");
		transformControl.enabled = false;
		scene.add(transformControl);

		addEventListeners();

		// get selected mesh entry for local storage
		const jsonId = localStorage.getItem("selectedId");
		if (jsonId) {
			const selectedId = JSON.parse(jsonId);

			// can't use object id, as value not guaranteed
			const mesh = scene.getObjectByProperty("myId", selectedId);
			updateSelection(mesh);

			const jsonTransformMode = localStorage.getItem("transformControl");
			if (jsonTransformMode) {
				transformControl.attach(mesh);
				transformControl.setMode(JSON.parse(jsonTransformMode));
				transformControl.enabled = true;
			}

		}
	}

	function onKeyDown(event) {
		shiftKey = event.shiftKey;
		ctrlKey = event.ctrlKey;
		altKey = event.altKey;
	}

	function onKeyUp(event) {
		shiftKey = event.shiftKey;
		ctrlKey = event.ctrlKey;
		altKey = event.altKey;



		if (event.ctrlKey) {
			if (event.key.toLowerCase() === 'z' || event.key.toLowerCase() === 'y') {
				transformControl.enabled = false;
				transformControl.detach();
				transformControl.setMode("scale");
				localStorage.removeItem("transformControl");
			}
		}
	}

	function onTransformChanging(event) {
		let mesh = transformControl.object || currentSelection;
		if (!transformControl.enabled) mesh = null;
		if (mesh && mouseDown) {
			const objectMovingEvent = new CustomEvent("objectMoving", { detail: { mesh } });
			dispatchEvent(objectMovingEvent);
		}
	}

	function onTransformDraggingChanged(event) {
		orbitControl.enabled = !event.value;
	}

	function addEventListeners() {
		addEventListener("pointermove", onPointerMove);
		addEventListener("mousedown", onMouseDown);
		addEventListener("mouseup", onMouseUp);
		addEventListener("keyup", onKeyUp);
		addEventListener("keydown", onKeyDown);
		addEventListener("dblclick", onDoubleClick);
	}

	function onPointerMove(event) {
		pointer.x = (event.clientX / window.innerWidth) * 2 - 1;
		pointer.y = -(event.clientY / window.innerHeight) * 2 + 1;

		if (mouseDown) {
			dragging = true;
		}

		if (orbitControl.enabled) {
			return;
		}

		if (transformControl.enabled) {
			objectChanged = true;
			return;
		}

		// Dragging: Move object along with cursor
		if (mouseDown && grabPoint) {
			const newGrabPoint = new THREE.Vector3();
			raycaster.setFromCamera(pointer, camera);
			raycaster.ray.intersectPlane(plane, newGrabPoint);

			const change = grabPoint.clone().sub(newGrabPoint);
			currentSelection.position.sub(change);
			grabPoint = newGrabPoint;

			objectChanged = true;

			const objectMovingEvent = new CustomEvent("objectMoving", { detail: { mesh: currentSelection } });
			dispatchEvent(objectMovingEvent);
		}
	}

	function onMouseDown(event) {
		if (event.target.id !== "renderCanvas") return;

		mouseDown = true;

		// Cast a ray to check for intersections with objects in the scene
		raycaster.setFromCamera(pointer, camera);
		const intersectedObjects = raycaster.intersectObjects(meshes);

		if (intersectedObjects.length > 0) {
			// An object is selected
			updateSelection(intersectedObjects[0].object);

			grabPoint = intersectedObjects[0].point;
			initialiseGrabPoint();

			orbitControl.enabled = false;
		} else if (!transformControl.enabled) {
			grabPoint = null;
			orbitControl.enabled = true;
		}
			}

	function onMouseUp(event) {
		if (event.target.id !== "renderCanvas") return;

		if (!dragging && !grabPoint) {
			updateSelection(null);
		}

		mouseDown = false;
		dragging = false;

		if (objectChanged) {
			objectChanged = false;

			const customEvent = new CustomEvent("objectChanged", { detail: { mesh: currentSelection } });
			dispatchEvent(customEvent);
		}

		orbitControl.enabled = true;
	}

	// Function to handle the double-click transform control cycling
	function onDoubleClick(event) {
		if (event.target.id !== "renderCanvas") return;

		// Cast a ray to check if the double-click happened on an object
		raycaster.setFromCamera(pointer, camera);
		const intersectedObjects = raycaster.intersectObjects(meshes);

		if (intersectedObjects.length > 0) {
			// If an object is selected, attach and cycle through transform control modes
			if (currentSelection) {
				transformControl.attach(currentSelection);

				// Cycle through transform modes: translate -> rotate -> scale -> disabled
				if (!transformControl.enabled) {
					transformControl.setMode("translate");
					transformControl.enabled = true;
					grabPoint = null;
				} else {
					switch (transformControl.mode) {
						case "translate":
							transformControl.setMode("rotate");
							break;
						case "rotate":
							transformControl.setMode("scale");
							break;
						case "scale":
							transformControl.detach();
							transformControl.enabled = false;
							transformControl.setMode("translate"); // Reset for next cycle
							break;
					}
				}
			}
		}
		if (transformControl.enabled) {
			localStorage.setItem("transformControl", JSON.stringify(transformControl.mode));
		} else {
			localStorage.removeItem("transformControl");
		}
	}

	function updateSelection(mesh) {
		if (currentSelection !== mesh || (currentSelection == null && mesh == null)) {
			currentSelection = mesh;
			const objectSelectedEvent = new CustomEvent("objectSelected", { detail: { mesh } });
			dispatchEvent(objectSelectedEvent);

			transformControl.enabled = false;
			transformControl.detach();
			transformControl.setMode("scale");
		}
	}

	function initialiseGrabPoint() {
		const fwd = new THREE.Vector3();
		camera.getWorldDirection(fwd);

		plane.setFromNormalAndCoplanarPoint(fwd, grabPoint);
	}

	return { setup, orbitControl };
}

const userInteraction = createUserInteraction();
export { userInteraction };
