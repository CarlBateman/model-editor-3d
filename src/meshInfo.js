function MakeMeshInfo() {
	const infoDiv = document.getElementById("info");

	function initInfo(e) {
		initializeInfoDiv(e.detail.mesh);

		removeEventListener("objectSelected", initInfo);

		addEventListener("objectSelected", updateInfoDiv, false);
	}

	addEventListener("objectSelected", initInfo, false);
	addEventListener("objectMoving", updateInfoDiv, false);

	let currMesh;

	function getMeshProperties(currMesh) {
		return {
			position: currMesh.position,
			rotation: currMesh.rotation,
			scale: currMesh.scale,
			materialColor: currMesh.material.color,
			geometryType: currMesh.geometry.type,
		};
	}

	function createPropertyEventHandler(property, axis) {
		return function (event) {
			currMesh[property][axis] = parseFloat(event.target.value);

			const customEvent = new CustomEvent("objectChanged", { detail: { mesh: currMesh } });
			dispatchEvent(customEvent);
		};
	}

	function createColorChangingEventHandler() {
		return function (event) {
			currMesh.material.color.set(event.target.value);
		};
	}

	function createColorChangedEventHandler() {
		return function (event) {
			const customEvent = new CustomEvent("objectChanged", { detail: { mesh: currMesh } });
			dispatchEvent(customEvent);
		};
	}

	function setEventHandlers() {
		document.querySelectorAll("input[data-prop]").forEach((input) => {
			const [property, axis] = input.getAttribute("data-prop").split("-");
			if (property === "color") {
				input.addEventListener("input", createColorChangingEventHandler());
				input.addEventListener("change", createColorChangedEventHandler());
			} else {
				input.addEventListener("input", createPropertyEventHandler(property, axis));
			}
		});
	}

	function generateTableRow(propertyName, property, labels) {
		const row = document.createElement("tr");

		const propertyNameCell = document.createElement("td");
		propertyNameCell.className = "propertyName";
		propertyNameCell.textContent = propertyName;
		row.appendChild(propertyNameCell);

		const stepSize = propertyName === "rotation" ? "0.1" : "0.1";

		labels.forEach((label) => {
			const axis = label.toLowerCase();
			const value = property[axis] !== undefined ? property[axis].toFixed(2) : 0;

			const cell = document.createElement("td");
			const labelElement = document.createElement("label");
			labelElement.setAttribute("for", `${propertyName}-${axis}`);
			labelElement.textContent = `${label}: `;

			const input = document.createElement("input");
			input.type = "number";
			input.id = `${propertyName}-${axis}`;
			input.dataset.prop = `${propertyName}-${axis}`;
			input.value = value;

			// Apply the step size based on property name
			input.step = stepSize;

			cell.appendChild(labelElement);
			cell.appendChild(input);
			row.appendChild(cell);
		});

		return row;
	}

	function updateInputValue(propertyName, property, labels) {
		labels.forEach((label) => {
			const axis = label.toLowerCase();
			const input = document.getElementById(`${propertyName}-${axis}`);
			if (input) {
				input.value = property[axis] !== undefined ? property[axis].toFixed(2) : 0;
			}
		});
	}

	function updateColorInputValue(color) {
		const input = document.getElementById("color");
		if (input) {
			input.value = `#${color.getHexString()}`;
		}
	}

	function initializeInfoDiv(mesh) {
		currMesh = mesh;
		const properties = getMeshProperties(currMesh);

		// Clear existing content in infoDiv
		infoDiv.textContent = "";

		const title = document.createElement("strong");
		title.textContent = "Properties";
		infoDiv.appendChild(title);

		const table = document.createElement("table");
		table.className = "blueTable";

		// Create the tbody element
		const tbody = document.createElement("tbody");

		if (properties.position) {
			tbody.appendChild(generateTableRow("position", properties.position, ["X", "Y", "Z"]));
		}
		if (properties.rotation) {
			tbody.appendChild(generateTableRow("rotation", properties.rotation, ["X", "Y", "Z"]));
		}
		if (properties.scale) {
			tbody.appendChild(generateTableRow("scale", properties.scale, ["X", "Y", "Z"]));
		}
		if (properties.materialColor) {
			const row = document.createElement("tr");

			const colorLabelCell = document.createElement("td");
			colorLabelCell.textContent = "Material Color";
			row.appendChild(colorLabelCell);

			const colorInputCell = document.createElement("td");
			colorInputCell.setAttribute("colspan", "3");

			const colorLabel = document.createElement("label");
			colorLabel.setAttribute("for", "color");
			colorLabel.textContent = "Color: ";

			const colorInput = document.createElement("input");
			colorInput.type = "color";
			colorInput.id = "color";
			colorInput.dataset.prop = "color";
			colorInput.value = `#${properties.materialColor.getHexString()}`;

			colorInputCell.appendChild(colorLabel);
			colorInputCell.appendChild(colorInput);
			row.appendChild(colorInputCell);

			tbody.appendChild(row);
		}
		if (properties.geometryType) {
			const row = document.createElement("tr");

			const geometryLabelCell = document.createElement("td");
			geometryLabelCell.textContent = "Geometry Type";
			row.appendChild(geometryLabelCell);

			const geometryValueCell = document.createElement("td");
			geometryValueCell.setAttribute("colspan", "3");
			geometryValueCell.textContent = properties.geometryType;

			row.appendChild(geometryValueCell);
			tbody.appendChild(row);
		}

		// Append tbody to the table
		table.appendChild(tbody);

		// Append table to infoDiv
		infoDiv.appendChild(table);

		document.getElementById("1").style.display = "block";

		setEventHandlers();
	}

	function updateInfoDiv(e) {
		if (e.detail.mesh) {
			currMesh = e.detail.mesh;
			const properties = getMeshProperties(currMesh);
			if (properties.position) {
				updateInputValue("position", properties.position, ["X", "Y", "Z"]);
			}
			if (properties.rotation) {
				updateInputValue("rotation", properties.rotation, ["X", "Y", "Z"]);
			}
			if (properties.scale) {
				updateInputValue("scale", properties.scale, ["X", "Y", "Z"]);
			}
			if (properties.materialColor) {
				updateColorInputValue(properties.materialColor);
			}
			document.getElementById("1").style.display = "block";
		} else {
			document.getElementById("1").style.display = "none";
		}
	}

	return { updateInfoDiv, initializeInfoDiv };
}

const meshinfo = MakeMeshInfo();

export { meshinfo };
