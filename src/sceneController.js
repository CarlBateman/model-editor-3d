import * as THREE from "three";
import { MakeSelectionHighlighter } from "./highlightSelection";

function makeSceneController() {
  let camera, scene, renderer;
  let meshes = [];
  let materials = [];
  let selectionHighlighter;
  let prevSelectionState = {
    position: new THREE.Vector3(),
    rotation: new THREE.Euler(),
    scale: new THREE.Vector3(),
    materialColor: new THREE.Color(),
  };

  addEventListener("sceneChanged", updateSceneJson, false);
  addEventListener("objectSelected", updateSelection, false);
  addEventListener("objectChanged", updatePrevSelection, false);

  function updateSelection(event) {
    // update selection previous details
    const mesh = event.detail.mesh;
    if (mesh == null) {
      prevSelectionState.active = false;
    } else {
      prevSelectionState.position.copy(mesh.position);
      prevSelectionState.scale.copy(mesh.scale);
      prevSelectionState.rotation.copy(mesh.rotation);
      prevSelectionState.materialColor.copy(mesh.material.color);
      prevSelectionState.active = true;
    }

    /////////////////
    // update selected mesh id
    const myId = event.detail.mesh ? event.detail.mesh.myId : null;
    if (myId) {
      localStorage.setItem("selectedId", JSON.stringify(myId));
    } else {
      localStorage.removeItem("selectedId");
    }
  }

  function updatePrevSelection(event) {
    const currentSelection = event.detail.mesh;
    const changedProperties = { myId: currentSelection.myId, id: currentSelection.id };

    // Check each property against its initial value
    if (!currentSelection.position.equals(prevSelectionState.position)) {
      // ignore small changes - can be caused by mouse "wobble"
			if (currentSelection.position.manhattanDistanceTo(prevSelectionState.position) > 0.001) {
				changedProperties.type = "position";
				changedProperties.position = {
					oldValue: prevSelectionState.position.clone(),
					newValue: currentSelection.position.clone()
				};
				prevSelectionState.position.copy(currentSelection.position);
			}
		}

    if (!currentSelection.rotation.equals(prevSelectionState.rotation)) {
      changedProperties.type = "rotation";
      changedProperties.rotation = {
        oldValue: prevSelectionState.rotation.clone(),
        newValue: currentSelection.rotation.clone()
      };
      prevSelectionState.rotation.copy(currentSelection.rotation);
    }

    if (!currentSelection.scale.equals(prevSelectionState.scale)) {
      changedProperties.type = "scale";
      changedProperties.scale = {
        oldValue: prevSelectionState.scale.clone(),
        newValue: currentSelection.scale.clone()
      };
      prevSelectionState.scale.copy(currentSelection.scale);
    }

    if (!currentSelection.material.color.equals(prevSelectionState.materialColor)) {
      changedProperties.type = "materialColor";
      changedProperties.materialColor = {
        oldValue: prevSelectionState.materialColor.clone(),
        newValue: currentSelection.material.color.clone()
      };
      prevSelectionState.materialColor.copy(currentSelection.material.color);
    }

    const objectMovedEvent = new CustomEvent("sceneChanged", {
      detail: { changes: changedProperties }
    });
    dispatchEvent(objectMovedEvent);
  
  }

  function setupCamera(json) {
    camera = new THREE.PerspectiveCamera(
      70,
      window.innerWidth / window.innerHeight,
      0.01,
      100
    );
    camera.position.set(0, 2, 2.1);
    //camera.lookAt(new THREE.Vector3(0, 0, 0));
  }

  function setupScene(json) {
    scene = new THREE.Scene();

    if (json.materials) {
      for (let [key, value] of Object.entries(json.materials)) {
        let materialType = value.type;
        let material = new THREE[materialType]();
        material.color = new THREE.Color().fromArray(value.color);
        material.myId = value.myId;
        materials[material.myId] = material;
      }
    } else {
      let material = new THREE.MeshLambertMaterial({ color: 0x008800 });
      material.myId = 0;
      materials.push(material);
    }

    if (json.meshes) {
      for (let [key, value] of Object.entries(json.meshes)) {
        let geometryType = value.type;
        let geometry = new THREE[geometryType](...value.parameters);
        let mesh = new THREE.Mesh(geometry, materials[value.materialId]);

        if (value.position) {
          mesh.position.fromArray(value.position);
        }
        if (value.rotation) {
          mesh.rotation.set(...value.rotation);
        }
        if (value.scale) {
          mesh.scale.fromArray(value.scale);
        }

        mesh.myId = value.myId;
        meshes.push(mesh);

        scene.add(mesh);
      }
    }

    if (json.camera) {
      //camera.position.fromArray(json.camera.position);
      //debugger;

      //camera.position.set(...json.camera.position);
      //camera.quaternion.fromArray(json.camera.quaternion);
      //camera.rotation.fromArray(json.camera.rotation);
    }

    const gridHelper = new THREE.GridHelper(100, 26);
    scene.add(gridHelper);

    const light = new THREE.DirectionalLight(0xffffff, 3);
    light.position.set(-1, 2, 4);
    scene.add(light);

  }

  function setupRenderer() {
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setClearColor(0x000040, 1);
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setAnimationLoop(animation);
    renderer.domElement.id = "renderCanvas";
    document.body.appendChild(renderer.domElement);
  }

  function setup(json) {
    // TODO don't set display = "none" here
    document.getElementById("1").style.display = "none";

    setupCamera(json);
    setupScene(json);
    setupRenderer();
    selectionHighlighter = MakeSelectionHighlighter(renderer, scene, camera);

  }

  function addMesh(response) {
    const material = new THREE.MeshBasicMaterial({
      side: THREE.DoubleSide,
      map: response.texture,
    });
    const geometry = new THREE.PlaneGeometry(0.5, 0.5 * response.ratio);
    const mesh = new THREE.Mesh(geometry, material);
    scene.add(mesh);
    meshes.push(mesh);
  }

  function animation() {
    //renderer.render(scene, camera);
    selectionHighlighter.render();
  }

  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth, window.innerHeight);
    selectionHighlighter.resize(window.innerWidth, window.innerHeight);
  }

  function debugShowPoint(pos) {
    const geometry = new THREE.SphereGeometry(0.01);
    const material = new THREE.MeshBasicMaterial({ side: THREE.DoubleSide });
    const mesh = new THREE.Mesh(geometry, material);
    mesh.position.set(...pos);
    scene.add(mesh);
  }

  function updateSceneJson() {
    let sceneMeshes = [];
    let sceneMaterials = [];

    let meshId = meshes.length + 1;
    let materialUuids = [];
    let materialId = 0;
    for (let [key, value] of Object.entries(meshes)) {
      let mesh = {};
      mesh.myId = value.myId;
      mesh.type = value.geometry.type;
      mesh.position = value.position.toArray().map(x => ((Math.round(x * 1000) / 1000)));
			mesh.rotation = value.rotation.toArray().map(x => (isNaN(x) ? x :  (Math.round(x * 1000) / 1000)));
      mesh.scale = value.scale.toArray().map(x => ((Math.round(x * 1000) / 1000)));
      const parameters = Object.values(value.geometry.parameters);
      mesh.parameters = parameters;

      let material = {};
      material.type = value.material.type;
      material.color = value.material.color.toArray();
      let idx = value.material.uuid;
      if (typeof materialUuids[idx] === 'undefined') {
        materialUuids[idx] = materialId;
      }
      mesh.materialId = materialId;
      material.myId = materialId++;

      sceneMeshes.push(mesh);
      sceneMaterials.push(material);
    }

    let sceneData = {};
    sceneData.meshes = sceneMeshes;
    sceneData.materials = sceneMaterials;

    localStorage.setItem("scene", JSON.stringify(sceneData));
  }

  return {
    get properties() { return [camera, renderer, scene, meshes]; },
    get camera() { return camera; },
    get renderer() { return renderer; },
    get scene() { return scene; },
    get meshes() { return meshes; },
    setup,
    onWindowResize
  }
}

const sceneController = makeSceneController();

export { sceneController };