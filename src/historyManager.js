﻿class HistoryManager {
	constructor(scene) {
		this.scene = scene;
		this.undoStack = [];
		this.redoStack = [];

		this.listenForChanges();
		this.restoreUndoRedo();

	}

	listenForChanges() {
		addEventListener("sceneChanged", (event) =>
			this.saveState(event.detail)
		);
		addEventListener("undo", () => this.undo());
		addEventListener("redo", () => this.redo());

		addEventListener("keyup", (event) => this.onKeyUp(event));
	}

	restoreUndoRedo() {
		if (!this.ctrlKey) {
			let json = localStorage.getItem("undoStack");
			if (json) {
				this.undoStack = JSON.parse(json);

				json = localStorage.getItem("redoStack");
				this.redoStack = JSON.parse(json);
			}
		}
	}

	saveUndoRedo() {
		localStorage.setItem("undoStack", JSON.stringify(this.undoStack));
		localStorage.setItem("redoStack", JSON.stringify(this.redoStack));
	}

	onKeyUp(event) {
		this.ctrlKey = event.ctrlKey;

		if (event.ctrlKey) {
			if (event.key.toLowerCase() === 'z') {
				this.undo();
			} else if (event.key.toLowerCase() === 'y') {
				this.redo();
			}
		}
	}

	// Save the current state to the undo stack and clear the redo stack
	saveState(detail) {
		if (detail.changes) {
			this.undoStack.push(detail.changes);
			this.redoStack.length = 0;  // Clear redo stack on new action

			this.saveUndoRedo();
		}
	}

	// Restore a previous state from the undo stack
	undo() {
		if (this.undoStack.length === 0) return;
		const prevState = this.undoStack.pop();
		this.redoStack.push(prevState);  // Save current state to redo stack
		this.restoreState(prevState, "oldValue");

		this.saveUndoRedo();
	}

	// Restore the next state from the redo stack
	redo() {
		if (this.redoStack.length === 0) return;
		const nextState = this.redoStack.pop();
		this.undoStack.push(nextState);  // Save current state to undo stack
		this.restoreState(nextState, "newValue");

		this.saveUndoRedo();
	}

	// Get the current state of the mesh for saving to history stacks
	getCurrentState(state) {
		const mesh = this.getObjectById(state.myId);
		if (!mesh) return null;

		return {
			type: state.type,
			id: mesh.id,
			position: mesh.position.clone(),
			rotation: mesh.rotation.clone(),
			scale: mesh.scale.clone(),
			color: mesh.material ? mesh.material.color.clone() : null
		};
	}

	// Restore the state of an mesh based on saved history
	restoreState(state, value) {
		const mesh = this.getObjectById(state.myId);
		if (!mesh) return;

		switch (state.type) {
			case "position":
				mesh.position.copy(state.position[value]);
				break;
			case "rotation":
				mesh.rotation.copy(state.rotation[value]);
				break;
			case "scale":
				mesh.scale.copy(state.scale[value]);
				break;
			case "materialColor":
				if (mesh.material && state.materialColor) {
					mesh.material.color.copy(state.materialColor[value]);
				}
				break;
		}
		const objectMovingEvent = new CustomEvent("objectMoving", { detail: { mesh } });
		dispatchEvent(objectMovingEvent);
		const objectSelectedEvent = new CustomEvent("objectSelected", { detail: { mesh } });
		dispatchEvent(objectSelectedEvent);
	}

	getObjectById(id) {
		return this.scene.getObjectByProperty("myId", id);
	}
}

//const historyManager = new HistoryManager();
export { HistoryManager };
