import { drag } from "./interact";
import { sceneController } from "./sceneController";
import { userInteraction } from "./userInteraction";
import { meshinfo } from "./meshInfo";
import { recoverFromLocalStorage } from "./storageHandling";

drag.setup();

const json = recoverFromLocalStorage("scene");

if (json) {
	sceneController.setup(json);
	userInteraction.setup(sceneController);
} else {
	fetch('./assets/default-scene.json')
		.then(
			(response) => response.json())
		.then(function (json) {
			sceneController.setup(json);
			userInteraction.setup(sceneController);
		});
}

window.addEventListener("resize", sceneController.onWindowResize);
